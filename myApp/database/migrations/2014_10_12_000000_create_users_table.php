<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->default('null');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('first_name')->default('null');
            $table->string('last_name')->default('null');
            $table->string('phone_number')->default('null');
            $table->string('username')->default('null');
            $table->date('dob')->nullable();
            $table->text('address');
            $table->string('profile_img')->default('null');
            $table->tinyInteger('gender')->comment('1 male, 2 female')->default(0);
            $table->smallInteger('nationality')->default(0);
            $table->string('facebook_id')->default('null');
            $table->tinyInteger('nf_comment')->default(0);
            $table->tinyInteger('nf_game')->default(0);
            $table->tinyInteger('nf_attendance')->default(0);
            $table->tinyInteger('nf_newsletter')->default(0);
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('user_role')->comment('1 Admin, 2 User, 3 Company, 4 Advertiser');
            $table->tinyInteger('is_cancel')->default(0);;
            $table->boolean('active')->default(false);
            $table->rememberToken();
            $table->string('activation_token')->default('null');
            $table->string('api_token')->default('null');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users');
    }

}
