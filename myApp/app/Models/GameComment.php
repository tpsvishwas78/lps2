<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GameComment extends Model
{
    protected $fillable = [
        'user_id','game_id','comment','name','email','comment_id'
    ];
}
