<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class YourBooking extends Model
{
    protected $table='your_booking';
    protected $fillable = [
        'first_name','last_name','user_id','email','phone_number','venue_id','date','message','image','recurring_booking'
    ];
}
