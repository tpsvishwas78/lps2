<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdvertiseSession extends Model
{
    protected $table='advertise_session';
    protected $fillable = [
        'first_name','last_name','email','user_id','phone_number','venue_id','sport_id','date','time','message','image','is_recurring_booking','no_of_player','signature'
    ];
}
