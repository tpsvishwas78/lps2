<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RepeatGames extends Model
{
    protected $table='repeat_games';
    public $timestamps = false;
    
    protected $fillable = [
        'game_id','game_start_date'
    ];
}
