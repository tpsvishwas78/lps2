<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JoinLeague extends Model
{
    protected $table='join_a_league';
    protected $fillable = [
        'banner_title','banner','title','description'
    ];
}
