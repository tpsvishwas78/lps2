<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SportTurnamentCategory extends Model
{
    protected $table='sportsturnaments_category';
    protected $fillable = [
        'name','slug','status','image'
    ];
}
