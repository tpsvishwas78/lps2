<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MstNationality extends Model
{
    protected $table='mst_nationality';
}
