<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KidsBlogs extends Model
{
    protected $table='kids_blogs';
    protected $fillable = [
        'title','slug','description','image','category_id'
    ];
}
