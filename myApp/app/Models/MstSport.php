<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MstSport extends Model
{
    protected $table='mst_sports';
    public $timestamps = false;

    protected $fillable = [
        'name','description','image','sport_order','main_id','slug'
    ];
}
