<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MstVenue extends Model {

    protected $table = 'mst_venue';
    protected $fillable = [
        'location', 'image', 'slug', 'name', 'address', 'description', 'postcode', 'latitude', 'longitude'
    ];


}
