<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GameImages extends Model
{
    protected $table='game_images';
    public $timestamps = false;

    protected $fillable = [
        'game_id','image'
    ];
}
