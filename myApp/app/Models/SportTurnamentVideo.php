<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SportTurnamentVideo extends Model
{
    protected $table='sportsturnaments_videos';
    public $timestamps = false;
    protected $fillable = [
        'title','cp_id','video'
    ];
}
