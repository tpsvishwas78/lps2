<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChildrenPartyFacility extends Model
{
    protected $table = 'cp_venue_facility';
    public $timestamps = false;
    protected $fillable = [
        'cp_id', 'vf_id'
    ];
}
