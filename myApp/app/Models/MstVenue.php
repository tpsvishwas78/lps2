<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MstVenue extends Model
{

    protected $table = 'mst_venue';
    protected $fillable = [
        'location', 'image', 'slug', 'name', 'address', 'description', 'postcode', 'latitude', 'longitude'
    ];

    public function venue()
    {
        return $this->hasOne('App\Phone');
    }

    public function games()
    {
        return $this->hasMany('App\Models\MstGame', 'venue_id', 'id');
    }

    public static function getSession($id = null)
    {
        if ($id) {
            $upcominggames = \DB::table('mst_games')
                ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
                ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
                ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
                ->where('mst_games.status', 1)
                ->where('mst_games.venue_id', $id)
                ->where('repeat_games.game_start_date', '>=', date('Y-m-d'))
                ->orderBy('repeat_games.game_start_date', 'ASC')
                ->take(2)
                ->get();

            return $upcominggames;
        } else {
            return array();
        }
    }
}
