<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GetInvolved extends Model
{
    protected $table='get_involved';
    protected $fillable = [
        'first_name','last_name','email','phone_number','venue','available_days','time','sport_id','comments'
    ];
}
