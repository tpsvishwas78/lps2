<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuickLinksSlider extends Model
{
    protected $table='quick_links_slider';
    
    protected $fillable = [
        'image','title','description','link','status'
    ];
}
