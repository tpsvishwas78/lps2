<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SiteSetting extends Model
{
    protected $table='site_setting';
    public $timestamps = false;
    
    protected $fillable = [
        'website_name','header_logo','footer_logo','favicon_icon','app_store_link','play_store_link','facebook_url','twitter_url','instagram_url','copyright','website_email','sendmail_email','phone_number','address','city','latitude','longitude','website_title'
    ];
}
