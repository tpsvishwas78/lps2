<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GamePayment extends Model
{
    protected $table='game_payment';

    protected $fillable = [
        'game_id','user_id','game_author_id','order_code','paid_amount','game_amount','vat_amount','commission_amount','currency','payment_status','payment_type','customer_order_code'
    ];
}
