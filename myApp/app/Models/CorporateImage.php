<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CorporateImage extends Model
{
    protected $table='corporate_images';
    public $timestamps = false;
    protected $fillable = [
        'cp_id','image'
    ];
}
