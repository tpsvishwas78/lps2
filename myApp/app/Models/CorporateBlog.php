<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CorporateBlog extends Model
{
    protected $table='corporate_blogs';
    protected $fillable = [
        'title','slug','description','image','category_id'
    ];
}
