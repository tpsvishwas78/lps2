<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlockBooking extends Model
{
    protected $table='block_booking';
    protected $fillable = [
        'first_name','last_name','email','phone_number','venue','ideal_days','time','sport_id','comments'
    ];
}
