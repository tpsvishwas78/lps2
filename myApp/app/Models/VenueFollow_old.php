<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VenueFollow extends Model
{
    protected $table='venue_followers';
    public $timestamps = false;
    protected $fillable = [
        'venue_id','user_id'
    ];
}
