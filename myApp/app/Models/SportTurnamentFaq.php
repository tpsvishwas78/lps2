<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SportTurnamentFaq extends Model
{
    protected $table='sportsturnaments_faq';
    public $timestamps = false;
    protected $fillable = [
        'cp_id','title'
    ];
}
