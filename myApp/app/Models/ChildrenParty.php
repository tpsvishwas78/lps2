<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChildrenParty extends Model
{
    protected $table='children_party';
    protected $fillable = [
        'title','slug', 'venue_title', 'venue_address','feature_image','video','timing_description','description','opening_hours','status','short_description','category','box_title'
    ];
}
