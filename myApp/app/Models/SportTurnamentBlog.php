<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SportTurnamentBlog extends Model
{
    protected $table='sportsturnaments_blogs';
    protected $fillable = [
        'title','slug','description','image','category_id'
    ];
}
