<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomeBanner extends Model
{
    protected $table='home_banners';
    
    protected $fillable = [
        'banner','title_one','title_two','title_three','button_link','banner_type','status'
    ];
}
