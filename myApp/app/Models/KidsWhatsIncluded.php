<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KidsWhatsIncluded extends Model
{
    protected $table='kids_whats_included';
    public $timestamps = false;

    protected $fillable = [
        'kid','title','description'
    ];
}
