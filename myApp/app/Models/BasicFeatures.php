<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BasicFeatures extends Model
{
    protected $table='basic_features';
    public $timestamps = false;
    
    protected $fillable = [
        'title','status'
    ];
}
