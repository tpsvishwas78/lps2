<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommonVideo extends Model
{
    protected $table='common_video';
    public $timestamps = false;

    protected $fillable = [
        'video','description','status'
    ];
}
