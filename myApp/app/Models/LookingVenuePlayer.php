<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LookingVenuePlayer extends Model
{
    protected $table='looking_venue_players';
}
