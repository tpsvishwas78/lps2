<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MstGame extends Model {

    protected $table = 'mst_games';
    protected $fillable = ['user_id', 'slug', 'title', 'featured_img', 'description', 'game_date', 'game_start_time', 'game_finish_time', 'gender', 'payment', 'team_limit', 'venue_id', 'is_private', 'price', 'currency', 'is_refund', 'refund_changed_rsvp', 'refund_days', 'sport_id', 'is_admin'];

    public function venue() {
        return $this->hasOne('App\Models\MstVenue', 'id', 'venue_id')->select(['id','location','image','name','address','description','latitude','longitude']);
    }

}
