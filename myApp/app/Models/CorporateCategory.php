<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CorporateCategory extends Model
{
    protected $table='corporate_category';
    protected $fillable = [
        'name','slug','status','image'
    ];
}
