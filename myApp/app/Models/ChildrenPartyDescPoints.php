<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChildrenPartyDescPoints extends Model
{
    protected $table = 'cp_desc_points';
}
