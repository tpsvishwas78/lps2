<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CorporateVideo extends Model
{
    protected $table='corporate_videos';
    public $timestamps = false;
    protected $fillable = [
        'title','cp_id','video'
    ];
}
