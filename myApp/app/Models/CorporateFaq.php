<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CorporateFaq extends Model
{
    protected $table='corporate_faq';
    public $timestamps = false;
    protected $fillable = [
        'cp_id','title'
    ];
}
