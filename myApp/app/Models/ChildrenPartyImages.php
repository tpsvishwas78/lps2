<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChildrenPartyImages extends Model
{
    protected $table = 'cp_images';
    public $timestamps = false;
    protected $fillable = [
        'cp_id', 'image'
    ];
}
