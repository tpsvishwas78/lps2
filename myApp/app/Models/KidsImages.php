<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KidsImages extends Model
{
    protected $table='kids_gallery';
    public $timestamps = false;

    protected $fillable = [
        'kid','image'
    ];
}
