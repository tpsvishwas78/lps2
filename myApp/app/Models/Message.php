<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table='message';

    protected $fillable = [
        'message','receiver_id','sender_id','is_read'
    ];
}
