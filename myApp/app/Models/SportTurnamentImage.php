<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SportTurnamentImage extends Model
{
    protected $table='sportsturnaments_images';
    public $timestamps = false;
    protected $fillable = [
        'cp_id','image'
    ];
}
