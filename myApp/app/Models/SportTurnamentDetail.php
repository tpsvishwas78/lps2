<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SportTurnamentDetail extends Model
{
    protected $table='sportsturnaments_details';
    protected $fillable = [
        'title','feature_image','banner','category_id','main_video','description','sec_title','sec_description','layout'
    ];
}
