<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    protected $table='pages';
    public $timestamps = false;

    protected $fillable = [
        'name','description','menu_id'
    ];
}
