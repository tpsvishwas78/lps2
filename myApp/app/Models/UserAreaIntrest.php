<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAreaIntrest extends Model
{
    protected $table='user_area_intrest';
    public $timestamps = false;
    protected $fillable = [
        'area_intrest_id','user_id'
    ];
}
