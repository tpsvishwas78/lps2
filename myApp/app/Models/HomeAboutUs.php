<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomeAboutUs extends Model
{
    protected $table='home_about_us';
    public $timestamps = false;
    
    protected $fillable = [
        'video','description','status'
    ];
}
