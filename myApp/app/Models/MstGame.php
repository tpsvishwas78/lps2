<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MstGame extends Model {

    protected $table = 'mst_games';
    protected $fillable = ['user_id', 'slug', 'title', 'featured_img', 'description', 'game_date', 'game_start_time', 'game_finish_time', 'gender', 'payment', 'team_limit', 'venue_id', 'is_private', 'price', 'currency', 'is_refund', 'refund_changed_rsvp', 'refund_days', 'sport_id', 'is_admin','end_date','is_repeat','days'];

    public function venue() {
        return $this->hasOne('App\Models\MstVenue', 'id', 'venue_id')->select(['id','location','image','name','address','description','latitude','longitude','postcode']);
    }
    public function comment() {
        return $this->hasMany('App\Models\GameComment', 'game_id', 'id')->select(['game_comments.id','user_id','game_id','comment','game_comments.name','game_comments.email','game_comments.created_at','users.profile_img'])->join('users', 'users.id', '=', 'game_comments.user_id');
    }
    public function teams() {
        return $this->hasMany('App\Models\GameSpots', 'game_id', 'id')->join('users', 'users.id', '=', 'team_spots.user_id')->select(['team_spots.id','user_id','game_id','users.first_name as name','profile_img','team']);
    }
    public function slider() {
        return $this->hasMany('App\Models\GameImages', 'game_id', 'MID');
    }


}
