<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JoinLeagueBlock extends Model
{
    protected $table='join_league_block';
    protected $fillable = [
        'title','description','image'
    ];
}
