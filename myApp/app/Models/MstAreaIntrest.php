<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MstAreaIntrest extends Model
{
    protected $table='mst_area_intrest';

    protected $fillable = [
        'name','description'
    ];
}
