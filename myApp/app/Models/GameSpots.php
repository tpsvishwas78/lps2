<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GameSpots extends Model
{
    protected $table='team_spots';
    public $timestamps = false;

    protected $fillable = [
        'game_id','user_id','team'
    ];
    public function usergames() {
        return $this->hasOne('App\Models\MstGame', 'id', 'game_id');
    }
    public function user() {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
}
