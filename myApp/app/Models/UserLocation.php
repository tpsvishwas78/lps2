<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLocation extends Model
{
    protected $table='user_location';
    public $timestamps = false;
    protected $fillable = [
        'location','address','latitude','longitude','user_id'
    ];
}
