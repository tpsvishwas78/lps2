<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MstVenueFacility extends Model
{
    protected $table = 'mst_venue_facility';
    protected $fillable = [
        'facility', 'icon', 'status'
    ];
}
