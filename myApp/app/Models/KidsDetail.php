<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KidsDetail extends Model
{
    protected $table='kids_details';

    protected $fillable = [
        'title','description','image','status','category_id','secound_title','secound_description'
    ];
}
