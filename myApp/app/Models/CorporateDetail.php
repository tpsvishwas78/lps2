<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CorporateDetail extends Model
{
    protected $table='corporate_details';
    protected $fillable = [
        'title','feature_image','banner_description','banner','category_id','main_video','description','sec_title','sec_description','layout'
    ];
}
