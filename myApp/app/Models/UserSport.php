<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSport extends Model
{
    protected $table='user_sports';
    public $timestamps = false;
    protected $fillable = [
        'sport_id','user_id'
    ];
}
