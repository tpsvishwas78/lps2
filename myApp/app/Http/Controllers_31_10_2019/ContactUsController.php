<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Mail\ContactMail;
use App\Models\SiteSetting;

class ContactUsController extends Controller
{
    public function index()
    {
        return view('contactus');
    }
    public function submitContact(Request $req)
    {
        $setting=SiteSetting::first();
        $req->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'mobile_number' => 'required|digits:11',
            'message' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ]);

        $contactdata=array(
            'first_name' => $req->first_name,
            'last_name' => $req->last_name,
            'email' => $req->email,
            'phone_number' => $req->mobile_number,
            'message' => $req->message,
        );
       
        $send=Mail::to(config('app.sendmail'))->send(new ContactMail($contactdata));

        //if($send){
            return back()->with('success', 'Successfully send your request.');
        // }else{
        //     return back()->with('warning', 'Failed Please Try Again.');
        // }
        
    }

    public function sendmail()
    {
        $contactdata=array(
            'first_name' => 'test',
            'last_name' => 'test',
            'email' => 'test@gmail.com',
            'phone_number' => '99999990999',
            'message' => 'test message',
        );
        $toEmail = "tpsvishwas78@gmail.com";
        Mail::to($toEmail)->send(new ContactMail($contactdata));
    }
    
}
