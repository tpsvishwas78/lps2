<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pages;
use App\Models\Menus;

class PagesController extends Controller
{
    public function staticpages()
    {
        $pages=\DB::table('menus')
            ->join('pages', 'menus.id', '=', 'pages.menu_id')
            ->select('*','menus.name as menu_name','menus.id as menu_id','menus.slug as menu_slug','pages.name as name','pages.id as id')
            ->where('menus.status',1)
            ->get()->toArray();

        return response()->json([
            'status' => 'success',
            'pages' => $pages
            ]);
    }
}
