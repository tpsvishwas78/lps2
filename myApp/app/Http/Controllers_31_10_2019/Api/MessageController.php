<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Message;
Use Auth;
use App\User;

class MessageController extends Controller
{
    public function allmessage(Request $request)
    {
        $messages=Message::where('receiver_id',Auth::id())
        ->orWhere('sender_id',Auth::id())
        ->orderBy('id','ASC')
        ->get()->toArray();
        
        // $data=array();
        $i=0;
        foreach ($messages as $key => $message) {
            if(Auth::id()==$message['receiver_id']){
                $user=User::where('id',$message['sender_id'])->first();
                $data[$message['sender_id']]['userdata']=$user;
                $data[$message['sender_id']]['lastmessage']=$message;
                $data[$message['sender_id']]['chat'][]=$message;

                // $data['userdata']=$user;
                // $data['lastmessage']=$message;
                // $data[0]=(array)$message;
            }else{
                $user=User::where('id',$message['sender_id'])->first();
                $data[$message['receiver_id']]['userdata']=$user;
                $data[$message['receiver_id']]['lastmessage']=$message;
                $data[$message['receiver_id']]['chat'][]=$message;
            }

            // if(Auth::id()==$message['receiver_id']){
            //     $user=User::where('id',$message['sender_id'])->first();
            //     $data[]['userdata']=$user;
            //     $data[]['lastmessage']=$message;
            //     $data[]['chat'][]=$message;
            // }else{
            //     $user=User::where('id',$message['sender_id'])->first();
            //     $data[]['userdata']=$user;
            //     $data[]['lastmessage']=$message;
            //     $data[]['chat'][]=$message;
            // }
            $i++;
        }

    //  print_r(gettype($data));die;
    return collect([
        'response' => 'success',
        'comments' => $data,
    ]);
        return response()->json([
            'message' => $data,
            'status' => 'success'
        ]);
    }

    public function sendmessage(Request $request)
    {
        Message::create([
            'message'=>$request->message,
            'sender_id'=>$request->sender_id,
            'receiver_id'=>$request->receiver_id,
        ]);
        $message=Message::where('receiver_id',$request->receiver_id)->where('sender_id',$request->sender_id)->orderBy('id','DESC')->first();
        return response()->json([
            'data' => $message,
            'status' => 'success'
        ]);
    }
    public function readmessage(Request $request)
    {
        Message::where('receiver_id',$request->receiver_id)->update([
            'is_read'=>1,
        ]);
        return response()->json([
            'status' => 'success'
        ]);
    }
}
