<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\MstVenue;
use App\Models\VenueFollow;
use Auth;
use Illuminate\Http\Request;
use App\Models\GameSpots;
use App\Models\MstGame;
use App\Models\MstSport;

class VenueController extends Controller
{

    public function allvenue(Request $request)
    {
        $Venues = MstVenue::select('mst_venue.id','venue_followers.user_id' ,'venue_followers.id as Fid','location', 'image', 'name', 'postcode', 'address', 'description', 'latitude', 'longitude')
            ->with('games')
            ->leftJoin('venue_followers', 'mst_venue.id', '=', 'venue_followers.venue_id')
            ->Where('status', 1)
            ->get()
            ->toArray();
        return response()->json([
            'data' => $Venues,
            'status' => 'success',
        ]);
    }

    public function myvenues()
    {
        $Venues = VenueFollow::with('myvenue')->Where('venue_followers.user_id', Auth::id())->get();
        return response()->json([
            'data' => $Venues,
            'status' => 'success',
        ]);
    }
     public function venuegames($id) {
        $query = MstGame::select('mst_games.id as MID', 'repeat_games.id as id', 'user_id', 'title', 'featured_img', 'description', 'mst_games.game_date as Mgame_date', 'repeat_games.game_start_date as game_date', 'game_start_time', 'game_finish_time', 'team_limit', 'payment', 'price', 'is_refund', 'currency', 'refund_days', 'venue_id')
                ->with('venue')
                ->with('comment')
                ->with('teams')
                ->with('slider')
                ->rightJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
                ->where('venue_id', $id);
        $query->orderBy('repeat_games.game_start_date', 'asc');
        $games = $query->get()->toArray();

        return response()->json([
                    'data' => $games,
                    'status' => 'success',
        ]);
    }
}
