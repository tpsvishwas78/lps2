<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\IntrestMail;
use App\Models\GamePayment;
use App\Models\GameSpots;
use App\Models\HomeBanner;
use App\Models\MstGame;
use App\Models\MstSport;
use App\Models\MstVenue;
use App\Notifications\GameNotify;
use App\User;
use Auth;
use GuzzleHttp\json_decode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Notification;

class GamesController extends Controller
{

    public function allgames(Request $request)
    {

        $query = MstGame::select('mst_games.id as MID', 'repeat_games.id as id', 'user_id', 'title', 'featured_img', 'description', 'mst_games.game_date as Mgame_date', 'repeat_games.game_start_date as game_date', 'game_start_time', 'game_finish_time', 'team_limit', 'payment', 'price', 'is_refund', 'currency', 'refund_days', 'venue_id', 'gender')
            ->with('venue')
            ->with('comment')
            ->with('teams')
            ->with('slider')
            ->rightJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
            ->where('status', '1');
        if ($request->sport != 'null' && $request->has('sport') && $request->sport) {
            $findvalue = MstSport::whereid($request->input('sport'))->first();
             if ($findvalue && $findvalue->name == 'Football') {
                $getall = MstSport::where('name','like', '%' . $findvalue->name . '%')->get('id')->toArray();
                $query->whereIn('sport_id', $getall);
            } else {
                $query->where('sport_id', $request->input('sport'));
            }
        }
        if ($request->has('gender') && $request->gender != 'null' && $request->gender) {

            if ($request->input('gender') == '-1') {
                $query->whereIn('gender', array(2, 3));
            } else {
                $query->where('gender', $request->input('gender'));

            }

        }
        if ($request->FromDate && $request->ToDate && $request->has('FromDate') && $request->FromDate != 'null' && $request->has('ToDate') && $request->ToDate != 'null') {
            $from = explode(' ', $request->input('FromDate'));
            $to = explode(' ', $request->input('ToDate'));
            $query->whereBetween('repeat_games.game_start_date', [date("Y-m-d", strtotime($from[0])), date("Y-m-d", strtotime($to[0]))]);

        }
        if ($request->location != 'null' && $request->location && $request->has('location')) {

            $getall1 = MstVenue::where('status', 1)
                ->where(
                    function ($q2) use ($request) {
                        $q2->orWhere('location', 'like', '%' . $request->location . '%');
                        $q2->orWhere('postcode', 'like', '%' . $request->location . '%');
                    }
                )
                ->get('id')->toArray();
            $query->whereIn('venue_id', $getall1);
            $query->orWhere('title', 'like', '%' . $request->location . '%');

        }
        $query->where('game_start_date', '>=', date("Y-m-d"));
        $query->orderBy('repeat_games.game_start_date', 'asc');

        $games = $query->limit(100)->get()->toArray();

        return response()->json([
            'data' => $games,
            'status' => 'success',
        ]);
    }

    public function myGames(Request $request,$type=null)
    {
        //type 1 = mygames and type 2 = my join games 
      
            if($type ==1){
                $query = MstGame::select('mst_games.id as MID', 'repeat_games.id as id', 'user_id', 'title', 'featured_img', 'description', 'mst_games.game_date as Mgame_date', 'repeat_games.game_start_date as game_date', 'game_start_time', 'game_finish_time', 'team_limit', 'payment', 'price', 'is_refund', 'currency', 'refund_days', 'venue_id', 'gender')
                ->with('venue')
                ->with('comment')
                ->with('teams')
                ->with('slider')
                ->rightJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
                ->where('status', '1');
                $query->where('user_id',  Auth::id());
                $query->where('game_start_date', '>=', date("Y-m-d"));
                $query->orderBy('repeat_games.game_start_date', 'asc');
                $games = $query->limit(100)->get()->toArray();
              
            // $games = GameSpots::with('usergames')->Where('team_spots.user_id', Auth::id())->get();
            return response()->json([
                'data' => $games,
                'status' => 'success',
            ]);
            }else{
                $query = MstGame::select('mst_games.id as MID', 'repeat_games.id as id', 'user_id', 'title', 'featured_img', 'description', 'mst_games.game_date as Mgame_date', 'repeat_games.game_start_date as game_date', 'game_start_time', 'game_finish_time', 'team_limit', 'payment', 'price', 'is_refund', 'currency', 'refund_days', 'venue_id', 'gender')
              
               
            ->with('venue')
            ->with('comment')
            ->with('teams')
            ->with('slider')
            ->rightJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
            // ->join('team_spots','repeat_games.id','='.'team_spots.game_id')
            ->where('status', '1');
            // ->where('team_spots.user_id', Auth::id());

            $query->where('game_start_date', '>=', date("Y-m-d"));
            $query->orderBy('repeat_games.game_start_date', 'asc');
            $games = $query->limit(5)->get()->toArray();
          
        // $games = GameSpots::with('usergames')->Where('team_spots.user_id', Auth::id())->get();
        return response()->json([
            'data' => $games,
            'status' => 'success',
        ]);
                 
               
            }
           
           
    }

    public function sports(Request $request)
    {
        $sports = MstSport::Where('status', 1)->orderBy('sport_order', 'asc')->get();
        return response()->json([
            'data' => $sports,
            'status' => 'success',
        ]);
    }

    public function homeslider(Request $request)
    {
        $banner = HomeBanner::Where('status', 1)->get();
        return response()->json([
            'data' => $banner,
            'status' => 'success',
        ]);
    }

    public function joingame(Request $request)
    {
        $key = config('worldpay.sandbox.client');

        header('Content-Type: application/json');
        $ch = curl_init('https://api.worldpay.com/v1/tokens');
        $array = [
            'reusable' => true,
            'paymentMethod' => [
                'name' => $request->name,
                'expiryMonth' => $request->expmonth,
                'expiryYear' => $request->expyear,
                'cardNumber' => $request->cardName,
                'type' => 'Card',
                'cvc' => $request->cvc,
            ],
            'clientKey' => $key,
        ];
        $post = json_encode($array);
        // print_r($post);die;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = json_decode(curl_exec($ch));
        curl_close($ch);
        if (!isset($result->token) && $result->token != '') {
            return response()->json([
                'data' => 'Something went wrong try again',
                'status' => 'success',
            ]);
        }
        $userid = Auth::user()->id;
        $token =  $result->token;
        $total = $request->totalprice * 100;
        $key = config('worldpay.sandbox.service');
        $worldPay = new \Alvee\WorldPay\lib\Worldpay($key);
     
        $billing_address = array(
            'address1' => '',
            'address2' => '',
            'address3' => '',
            'postalCode' => '',
            'city' => '',
            'state' => '',
            'countryCode' => 'GB',
        );
        // $billing_address = array(
        //     'address1'    => '15 Chipping Vale',
        //     'address2'    => 'Emerson Valley',
        //     'address3'    => 'Milton Keynes',
        //     'postalCode'  => 'MK4 2JW',
        //     'city'        => 'Milton Keynes',
        //     'state'       => 'UK',
        //     'countryCode' => 'GB',
        // );
        try {

            $response = $worldPay->createOrder(array(
                'token' => $token,
                'amount'            => (int) 1,
                // 'amount' => (int) ($total),
                'currencyCode' => 'GBP',
                'name' => $request->cardName,
                'billingAddress' => $billing_address,
                'orderDescription' => 'Joined Game',
                'customerOrderCode' => time(),
            ));
          

            if ($response['paymentStatus'] === 'SUCCESS') {
                $worldpayOrderCode = $response['orderCode'];
                $paidamout = $response['amount'] / 100;
                $commission = $request->game_amount * 10 / 100;
                // echo "<pre>";
                // print_r($response);
                // dd($request->all());
                
                GamePayment::create([
                    'game_id' => $request->game_id,
                    'user_id' => $userid,
                    'game_author_id' => $request->game_author_id,
                    'order_code' => $response['orderCode'],
                    'paid_amount' => $paidamout,
                    'game_amount' => $request->game_amount,
                    'vat_amount' => $request->vat_amount,
                    'commission_amount' => $commission,
                    'currency' => $response['currencyCode'],
                    'payment_status' => $response['paymentStatus'],
                    'payment_type' => $response['paymentResponse']['cardType'],
                    'customer_order_code' => $response['customerOrderCode'],
                ]);
               
                $already = GameSpots::where('user_id', $userid)->where('game_id', $request->game_id)->first();
                $checkteam = GameSpots::where('game_id', $request->game_id)->orderBy('id', 'DESC')->first();

               
                $MyteamRow = '';
                if ($already) {
                    GameSpots::where('user_id', $userid)->where('game_id', $request->game_id)->update([
                        'status' => 1,
                    ]);
                    $MyteamRow = $already;
                    
                    return response()->json([
                        'data' => $MyteamRow,
                        'status' => 'error',
                        'message' => 'You are already join this game',
                    ]);
                    exit;
                } else {
                    if (isset($checkteam->team) && $checkteam->team == 1) {
                        $team = 2;
                    } else {
                        $team = 1;
                    }
                   
                 $gameSp =   GameSpots::create([
                        'game_id' => $request->game_id,
                        'user_id' => $userid,
                        'team' => $team,
                    ]);
                   
                    $MyteamRow = GameSpots::select('game_id', 'team_spots.id', 'users.name', 'profile_img', 'team', 'user_id')->join('users', 'users.id', '=', 'team_spots.user_id')->where('user_id', $userid)->where('game_id', $request->game_id)->first();
                }
              
              
                $gameOwner = User::where('id', $request->game_author_id)->first();
                $gameJoiner = User::where('id', $userid)->first();
              
                $game = MstGame::where('id', $request->game_pid)->first();
             

                $detailsOwner = [
                    'greeting' => 'Hi ' . $gameOwner->first_name . ' ' . $gameOwner->last_name,
                    'body' => $gameJoiner->first_name . ' ' . $gameJoiner->last_name . ' has joined your created game ' . $game->title,
                    'thanks' => 'Thank you for using ' . url(''),
                    'actionText' => 'View Game',
                    'actionURL' => url('') . '/' . 'games/' . $game->slug . '/' . $request->game_id,
                    'game_id' => $game->id,
                    'game_cid' => $request->game_id,
                    'user_id' => $userid,
                    'message' => ' has joined your created game',
                ];
              

                Notification::send($gameOwner, new GameNotify($detailsOwner));

                $detailsJoiner = [
                    'greeting' => 'Hi ' . $gameJoiner->first_name . ' ' . $gameJoiner->last_name,
                    'body' => ' You have joined a game ' . $game->title,
                    'thanks' => 'Thank you for using ' . url(''),
                    'actionText' => 'View Game',
                    'actionURL' => url('') . '/' . 'games/' . $game->slug . '/' . $request->game_id,
                    'game_id' => $game->id,
                    'game_cid' => $request->game_id,
                    'user_id' => $userid,
                    'message' => ' You have joined a game ',
                ];

                Notification::send($gameJoiner, new GameNotify($detailsJoiner));

                $admin = User::where('email', 'info@letsplaysports.co.uk')->first();

                Notification::send($admin, new GameNotify($detailsJoiner));
             
                return response()->json([
                                    'data' => $MyteamRow,
                                    'status' => 'success',
                                    'message' => 'Game join successfully',
                ],200);exit;
                    // return redirect('thankyou/' . $request->game_slug . '/' . $request->game_id)->with('success', 'Successfully Joined Game!');
                
            } else {

                // The card has been declined
                throw new \Alvee\WorldPay\lib\WorldpayException(print_r($response, true));
            }
        } catch (Alvee\WorldPay\lib\WorldpayException $e) {
            //$error = $e->getMessage();
            return response()->json([
                'status' => 'error',
                'message' => 'The card has been declined',
            ],200);exit;
            // return redirect('paynow/' . $request->game_slug . '/' . $request->game_id)->with('error',  'Unknown error');

            // echo 'Error code: ' . $e->getCustomCode() . '
            //     HTTP status code:' . $e->getHttpStatusCode() . '
            //     Error description: ' . $e->getDescription() . '
            //     Error message: ' . $e->getMessage();

            // The card has been declined
        } catch (Exception $e) {
           
           // dd($e);
           return response()->json([
                'status' => 'error',
                'message' => 'The card has been declined',
           ],200);exit;
            // return redirect('paynow/' . $request->game_slug . '/' . $request->game_id)->with('error',  'Unknown error');

            // echo 'Error message: <pre>' . $e->getMessage();
        }
    }
    // public function joingame(Request $request)
    // {
    //     $key = config('worldpay.sandbox.client');

    //     header('Content-Type: application/json');
    //     $ch = curl_init('https://api.worldpay.com/v1/tokens');
    //     $array = [
    //         'reusable' => true,
    //         'paymentMethod' => [
    //             'name' => $request->name,
    //             'expiryMonth' => $request->expmonth,
    //             'expiryYear' => $request->expyear,
    //             'cardNumber' => $request->cardName,
    //             'type' => 'Card',
    //             'cvc' => $request->cvc,
    //         ],
    //         'clientKey' => $key,
    //     ];
    //     $post = json_encode($array);
    //     // print_r($post);die;
    //     curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //     curl_setopt($ch, CURLOPT_POST, 1);
    //     curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    //     curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    //     $result = json_decode(curl_exec($ch));
    //     curl_close($ch);
    //     if (!isset($result->token) && $result->token != '') {
    //         return response()->json([
    //             'data' => 'Something went wrong try again',
    //             'status' => 'success',
    //         ]);
    //     }
    //     $userid = Auth::user()->id;
    //     $token = $result->token;
    //     $total = $request->totalprice * 100;
    //     $key = config('worldpay.sandbox.service');
    //     $worldPay = new \Alvee\WorldPay\lib\Worldpay($key);

    //     $billing_address = array(
    //         'address1' => '',
    //         'address2' => '',
    //         'address3' => '',
    //         'postalCode' => '',
    //         'city' => '',
    //         'state' => '',
    //         'countryCode' => 'GB',
    //     );
    //     // $billing_address = array(
    //     //     'address1'    => '15 Chipping Vale',
    //     //     'address2'    => 'Emerson Valley',
    //     //     'address3'    => 'Milton Keynes',
    //     //     'postalCode'  => 'MK4 2JW',
    //     //     'city'        => 'Milton Keynes',
    //     //     'state'       => 'UK',
    //     //     'countryCode' => 'GB',
    //     // );

    //     try {

    //         $response = $worldPay->createOrder(array(
    //             'token' => $token,
    //             //'amount'            => (int)($total . "00"),
    //             'amount' => (int) ($total),
    //             'currencyCode' => 'GBP',
    //             'name' => $request->cardName,
    //             'billingAddress' => $billing_address,
    //             'orderDescription' => 'Joined Game',
    //             'customerOrderCode' => time(),
    //         ));

    //         if ($response['paymentStatus'] === 'SUCCESS') {
    //             $worldpayOrderCode = $response['orderCode'];
    //             $paidamout = $response['amount'] / 100;
    //             $commission = $request->game_amount * 10 / 100;
    //             // echo "<pre>";
    //             // print_r($response);
    //             // dd($request->all());

    //             GamePayment::create([
    //                 'game_id' => $request->game_id,
    //                 'user_id' => $userid,
    //                 'game_author_id' => $request->game_author_id,
    //                 'order_code' => $response['orderCode'],
    //                 'paid_amount' => $paidamout,
    //                 'game_amount' => $request->game_amount,
    //                 'vat_amount' => $request->vat_amount,
    //                 'commission_amount' => $commission,
    //                 'currency' => $response['currencyCode'],
    //                 'payment_status' => $response['paymentStatus'],
    //                 'payment_type' => $response['paymentResponse']['cardType'],
    //                 'customer_order_code' => $response['customerOrderCode'],
    //             ]);

    //             $already = GameSpots::select('game_id', 'team_spots.id', 'users.name', 'profile_img', 'team', 'user_id')->join('users', 'users.id', '=', 'team_spots.user_id')->where('user_id', $userid)->where('game_id', $request->game_id)->first();
    //             $checkteam = GameSpots::where('game_id', $request->game_id)->orderBy('id', 'DESC')->first();
    //             $MyteamRow = '';
    //             if ($already) {
    //                 GameSpots::where('user_id', $userid)->where('game_id', $request->game_id)->update([
    //                     'status' => 1,
    //                 ]);
    //                 $MyteamRow = $already;
    //             } else {
    //                 if ($checkteam->team == 1) {
    //                     $team = 2;
    //                 } else {
    //                     $team = 1;
    //                 }
    //                 $JoinGame = GameSpots::create([
    //                     'game_id' => $request->game_id,
    //                     'user_id' => $userid,
    //                     'team' => $team,
    //                 ]);
    //                 $MyteamRow = GameSpots::select('game_id', 'team_spots.id', 'users.name', 'profile_img', 'team', 'user_id')->join('users', 'users.id', '=', 'team_spots.user_id')->where('user_id', $userid)->where('game_id', $request->game_id)->first();
    //             }

    //             $gameOwner = User::where('id', $request->game_author_id)->first();
    //             $gameJoiner = User::where('id', $userid)->first();
    //             $game = MstGame::where('id', $request->game_id)->first();
    //             $detailsOwner = [
    //                 'greeting' => 'Hi ' . $gameOwner->first_name . ' ' . $gameOwner->last_name,
    //                 'body' => $gameJoiner->first_name . ' ' . $gameJoiner->last_name . ' has joined your created game ' . $game->title,
    //                 'thanks' => 'Thank you for using ' . url(''),
    //                 'actionText' => 'View Game',
    //                 'actionURL' => url('') . '/' . 'games/' . $game->slug,
    //                 'game_id' => $game->id,
    //                 'user_id' => $userid,
    //                 'message' => ' has joined your created game',
    //             ];

    //             Notification::send($gameOwner, new GameNotify($detailsOwner));

    //             $detailsJoiner = [
    //                 'greeting' => 'Hi ' . $gameJoiner->first_name . ' ' . $gameJoiner->last_name,
    //                 'body' => ' You have joined a game ' . $game->title,
    //                 'thanks' => 'Thank you for using ' . url(''),
    //                 'actionText' => 'View Game',
    //                 'actionURL' => url('') . '/' . 'games/' . $game->slug,
    //                 'game_id' => $game->id,
    //                 'user_id' => $userid,
    //                 'message' => ' You have joined a game ',
    //             ];

    //             Notification::send($gameJoiner, new GameNotify($detailsJoiner));
    //             return response()->json([
    //                 'data' => $MyteamRow,
    //                 'status' => 'success',
    //                 'message' => 'Game join successfully',
    //             ]);
    //         } else {

    //             // The card has been declined
    //             throw new \Alvee\WorldPay\lib\WorldpayException(print_r($response, true));
    //         }
    //     } catch (Alvee\WorldPay\lib\WorldpayException $e) {
    //         echo 'Error code: ' . $e->getCustomCode() . '
    //             HTTP status code:' . $e->getHttpStatusCode() . '
    //             Error description: ' . $e->getDescription() . '
    //             Error message: ' . $e->getMessage();

    //         // The card has been declined
    //     } catch (Exception $e) {
    //         // The card has been declined
    //         echo 'Error message: ' . $e->getMessage();
    //     }
    // }

    public function submitIntrest(Request $req)
    {

        $req->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'mobile_number' => 'required|digits:10',
            'location' => 'required',
            'message' => 'required',
        ]);

        $intrestdata = array(
            'first_name' => $req->first_name,
            'last_name' => $req->last_name,
            'email' => $req->email,
            'phone_number' => $req->mobile_number,
            'location' => $req->location,
            'message' => $req->message,
        );

        $send = Mail::to(config('app.sendmail'))->send(new IntrestMail($intrestdata));
        return response()->json([
            'data' => $send,
            'status' => 'success',
            'message' => 'Successfully send your intrest.',
        ]);
    }

    public function token(Request $request)
    {
        return $request;
    }

}
