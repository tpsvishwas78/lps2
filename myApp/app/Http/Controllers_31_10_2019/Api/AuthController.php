<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Notifications\SignupActivate;

class AuthController extends Controller {

    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function signup(Request $request) {
        $validator = Validator::make($request->all(), [
                    'name' => 'required|string',
                    'email' => 'required|string|email|unique:users',
                    'password' => 'required|string',
                    'phone_number' => ['required', 'string', 'digits:10'],
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'error' => $validator->errors()], 200);
        }

        $user = new User([
            'name' => $request->name,
            'username' => $request->name,
            'first_name' => $request->name,
            'email' => $request->email,
            //            'password' => bcrypt($request->password),
            'phone_number' => $request->phone_number,
            'user_role' => 2,
            'password' => Hash::make($request->password),
            'activation_token' => str_random(60)
        ]);
        $user->save();
        
        $user->profile_img = Self::baseImg($request->profile_img, 'uploads/profile/', $user->id, $request);
        $user->save();
        $user->notify(new SignupActivate($user));
        return response()->json(['status' => 'success', 'user' => $user]);
    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request) {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean',
        ]);
        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials)) {
            return response()->json([
                        'message' => 'Please enter valid email and password',
                        'status' => 'error',
                            ], 200);
        } else {
            $credentials['active'] = 1;
            $credentials['deleted_at'] = NULL;
            if (!Auth::attempt($credentials))
                return response()->json([
                            'message' => 'Unauthorized Verify your account',
                            'status' => 'error',
                                ], 200);
        }


        $user = $request->user();


        $tokenResult = $user->createToken('letesplayapp');

        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        return response()->json([
                    'access_token' => $tokenResult->accessToken,
                    'token_type' => 'Bearer',
                    'status' => 'success',
                    'user' => $user,
                    'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString()
        ]);
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request) {
        $request->user()->token()->revoke();
        return response()->json([
                    'message' => 'Successfully logged out',
                    'status' => 'success'
        ]);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request) {
        return response()->json($request->user());
    }

    public function signupActivate($token) {
        $user = User::where('activation_token', $token)->first();
        if (!$user) {
            return response()->json([
                        'message' => 'This activation token is invalid.'
                            ], 404);
        }
        $user->active = true;
        $user->activation_token = '';
        // $user->email_verified_at = new date();
        $user->save();
        return 'Activation sucessfully';
    }

    public function profileupdate(Request $request) {

        $validator = Validator::make($request->all(), [
                    'name' => 'required|string',
                    'username' => 'required|string',
                    'email' => 'required|string|email|unique:users,email,' . Auth::id(),
                    'address' => 'required|string',
                    'gender' => 'required|int',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'error' => $validator->errors()], 200);
        }



        $user = User::where('id', Auth::id())->first();
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->gender = $request->gender;
        $user->save();

        return response()->json([
                    'message' => 'Successfully logged out',
                    'status' => 'success',
                    'data' => $user,
        ]);
    }

    public function changepassword(Request $request) {

        $validator = Validator::make($request->all(), [
                    'password' => 'required|string',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'error' => $validator->errors()], 200);
        }
        $user = User::where('id', Auth::id())->first();
        $user->password = Hash::make($request->password);
        $user->save();

        return response()->json([
                    'message' => 'Password Changed successfully',
                    'status' => 'success',
                    'data' => $user,
        ]);
    }

    public function notifiupdate(Request $request) {


        $user = User::where('id', Auth::id())->first();
        if ($request->nf_comment != '1') {
            $user->nf_comment = ($request->nf_comment == 'true') ? 1 : 0;
        }
        if ($request->nf_game != '1') {
            $user->nf_game = ($request->nf_game == 'true') ? 1 : 0;
        }
        if ($request->nf_attendance != '1') {
            $user->nf_attendance = ($request->nf_attendance == 'true') ? 1 : 0;
        }
        if ($request->nf_newsletter != '1') {
            $user->nf_newsletter = ($request->nf_newsletter == 'true') ? 1 : 0;
        }
        $user->save();

        return response()->json([
                    'message' => 'Setting Update successfully',
                    'status' => 'success',
                    'data' => $user,
        ]);
    }

    function baseImg($img, $imgpath, $name, $req) {

        if ($img && $img != 'null' && strlen($img) > 100) {
            $img = str_replace('data:image/jpg;base64,', '', $img);
            $img = str_replace('data:image/png;base64,', '', $img);
            $img = str_replace('data:image/jpeg;base64,', '', $img);
            $img = str_replace(' ', '+', $img);
            $data = base64_decode($img);

            $imagePath = $imgpath;
            $fname = $name . '_' . ".jpg";
            $filename = $fname;
            if (file_put_contents('upload/images/' . $filename, $data)) {
                $col = $imagePath . $fname;
            }
            return $filename;
        } else {
            return $imgpath . $name . '_' . ".jpg";
        }
    }

}
