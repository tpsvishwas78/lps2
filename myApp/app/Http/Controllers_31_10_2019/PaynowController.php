<?php

namespace App\Http\Controllers;

use App\Models\GamePayment;
use App\Models\GameSpots;
use App\Models\MstGame;
use App\Notifications\GameNotify;
use App\User;
use Auth;
use Exception;
use Illuminate\Http\Request;
use Notification;

class PaynowController extends Controller
{
    public function index($slug = '', $id = 0)
    {
        //$game = MstGame::where('slug', $slug)->first();
        $game = \DB::table('mst_games')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
            ->select('*', 'mst_games.id as gid', 'repeat_games.id as id')
            ->where('mst_games.slug', '=', $slug)
            ->where('repeat_games.id', $id)
            ->first();
        return view('paynow')->with('game', $game);
    }

    public function charge(Request $request)
    {
        $userid = Auth::user()->id;
        $token = $request->input('token');
        $total = $request->totalprice * 100;
        $key = config('worldpay.live.service');
        $worldPay = new \Alvee\WorldPay\lib\Worldpay($key);

        $billing_address = array(
            'address1' => '',
            'address2' => '',
            'address3' => '',
            'postalCode' => '',
            'city' => '',
            'state' => '',
            'countryCode' => 'GB',
        );
        // $billing_address = array(
        //     'address1'    => '15 Chipping Vale',
        //     'address2'    => 'Emerson Valley',
        //     'address3'    => 'Milton Keynes',
        //     'postalCode'  => 'MK4 2JW',
        //     'city'        => 'Milton Keynes',
        //     'state'       => 'UK',
        //     'countryCode' => 'GB',
        // );

        try {

            $response = $worldPay->createOrder(array(
                'token' => $token,
                'amount'            => (int) 1,
                // 'amount' => (int) ($total),
                'currencyCode' => 'GBP',
                'name' => $request->cardName,
                'billingAddress' => $billing_address,
                'orderDescription' => 'Joined Game',
                'customerOrderCode' => time(),
            ));

            if ($response['paymentStatus'] === 'SUCCESS') {
                $worldpayOrderCode = $response['orderCode'];
                $paidamout = $response['amount'] / 100;
                $commission = $request->game_amount * 10 / 100;
                // echo "<pre>";
                // print_r($response);
                // dd($request->all());

                GamePayment::create([
                    'game_id' => $request->game_id,
                    'user_id' => $userid,
                    'game_author_id' => $request->game_author_id,
                    'order_code' => $response['orderCode'],
                    'paid_amount' => $paidamout,
                    'game_amount' => $request->game_amount,
                    'vat_amount' => $request->vat_amount,
                    'commission_amount' => $commission,
                    'currency' => $response['currencyCode'],
                    'payment_status' => $response['paymentStatus'],
                    'payment_type' => $response['paymentResponse']['cardType'],
                    'customer_order_code' => $response['customerOrderCode'],
                ]);

                @$already = GameSpots::where('user_id', $userid)->where('game_id', $request->game_id)->first();
                @$checkteam = GameSpots::where('game_id', $request->game_id)->orderBy('id', 'DESC')->first();

                if (@$already) {
                    GameSpots::where('user_id', $userid)->where('game_id', $request->game_id)->update([
                        'status' => 1,
                    ]);
                } else {
                    if (@$checkteam->team == 1) {
                        $team = 2;
                    } else {
                        $team = 1;
                    }
                    GameSpots::create([
                        'game_id' => $request->game_id,
                        'user_id' => $userid,
                        'team' => $team,
                    ]);
                }

                $gameOwner = User::where('id', $request->game_author_id)->first();
                $gameJoiner = User::where('id', $userid)->first();
                $game = MstGame::where('id', $request->game_pid)->first();
                $detailsOwner = [
                    'greeting' => 'Hi ' . $gameOwner->first_name . ' ' . $gameOwner->last_name,
                    'body' => $gameJoiner->first_name . ' ' . $gameJoiner->last_name . ' has joined your created game ' . $game->title,
                    'thanks' => 'Thank you for using ' . url(''),
                    'actionText' => 'View Game',
                    'actionURL' => url('') . '/' . 'games/' . $game->slug . '/' . $request->game_id,
                    'game_id' => $game->id,
                    'game_cid' => $request->game_id,
                    'user_id' => $userid,
                    'message' => ' has joined your created game',
                ];

                Notification::send($gameOwner, new GameNotify($detailsOwner));

                $detailsJoiner = [
                    'greeting' => 'Hi ' . $gameJoiner->first_name . ' ' . $gameJoiner->last_name,
                    'body' => ' You have joined a game ' . $game->title,
                    'thanks' => 'Thank you for using ' . url(''),
                    'actionText' => 'View Game',
                    'actionURL' => url('') . '/' . 'games/' . $game->slug . '/' . $request->game_id,
                    'game_id' => $game->id,
                    'game_cid' => $request->game_id,
                    'user_id' => $userid,
                    'message' => ' You have joined a game ',
                ];

                Notification::send($gameJoiner, new GameNotify($detailsJoiner));

                $admin = User::where('email', 'info@letsplaysports.co.uk')->first();

                Notification::send($admin, new GameNotify($detailsJoiner));

               
                    return redirect('thankyou/' . $request->game_slug . '/' . $request->game_id)->with('success', 'Successfully Joined Game!');
                
            } else {

                // The card has been declined
                throw new \Alvee\WorldPay\lib\WorldpayException(print_r($response, true));
            }
        } catch (Alvee\WorldPay\lib\WorldpayException $e) {
            //$error = $e->getMessage();
            return redirect('paynow/' . $request->game_slug . '/' . $request->game_id)->with('error',  'Unknown error');

            // echo 'Error code: ' . $e->getCustomCode() . '
            //     HTTP status code:' . $e->getHttpStatusCode() . '
            //     Error description: ' . $e->getDescription() . '
            //     Error message: ' . $e->getMessage();

            // The card has been declined
        } catch (Exception $e) {
           
           // dd($e);
            return redirect('paynow/' . $request->game_slug . '/' . $request->game_id)->with('error',  'Unknown error');

            // echo 'Error message: <pre>' . $e->getMessage();
        }
    }

    public function thankyou($slug = null, $id = null)
    {
        if ($slug && $id) {
            $data['title'] = $slug;
            $data['id'] = $id;
            $data['message'] = 'Successfully Joined Game!';
            return view('thankyou')->with('data', $data); 
        } else {
            return view('gamesdashboard');
        }
    }
}
