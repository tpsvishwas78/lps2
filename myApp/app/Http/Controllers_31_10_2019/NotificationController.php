<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use Auth;

class NotificationController extends Controller
{
    public function notifyread(Request $req)
    {
        $user= Auth::user();
        $user->unreadNotifications->markAsRead(); 
        echo 'done';
    }
}
