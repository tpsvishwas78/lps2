<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MstVenue;
use App\Models\VenueFollow;
use App\Models\MstGame;
use App\Models\GameSpots;
use Auth;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth' => 'verified']);
    }
    
    public function index()
    {
        $userid=Auth::user()->id;
        $joingames=GameSpots::where('user_id',$userid)->where('status',1)->get();
        //$games=MstGame::where('user_id',$userid)->orderBy('game_date','ASC')->get();
        $games=\DB::table('mst_games')
        ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
        ->select('*', 'mst_games.id as id', 'repeat_games.id as rid')
        ->where('mst_games.user_id','=',$userid)
        ->where('mst_games.status',1)
        ->get();
        return view('dashboard')->with('games',$games)->with('joingames',$joingames);
    }
    public function venues(Request $req)
    {
        if ($req->search && $req->search!='') {
            $venues=MstVenue::where('status',1)->where('name','LIKE', '%'.$req->search.'%')->orWhere('location','LIKE', '%'.$req->search.'%')->get();
        } else {
            $venues=MstVenue::where('status',1)->get();
        }
        
        return view('venues')->with('venues',$venues);
    }

    public function follow(Request $req)
    {
        $userid=Auth::user()->id;
        if($req->follow=='Follow'){
            VenueFollow::create([
                'user_id'=>$userid,
                'venue_id'=>$req->id,
            ]);
            $result['follow'] = 'Following';
        }else{
            VenueFollow::where('venue_id',$req->id)->where('user_id',$userid)->delete();
            $result['follow'] = 'Follow';
        }
        $followes=VenueFollow::where('venue_id',$req->id)->count();

        $result['followes']=$followes;

        echo json_encode($result);
    }

    public function payment()
    {
        return view('payment');
    }
    
}
