<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CorporateCategory;
use App\Models\CorporateDetail;
use App\Models\CorporateImage;
use App\Models\CorporateVideo;
use App\Models\CorporateBlog;
use App\Models\CorporateFaq;

class CorporateController extends Controller
{
    public function addcorporatecatgory($id=0)
    {
        $data = array();
        if($id>0){
            $data=CorporateCategory::where('id',$id)->first();
        }
        return view('admin.addcorporatecatgory')->with('edit',$data);
    }
    public function allcorporatecatgory()
    {
        $allcorporatecatgory = CorporateCategory::get();
        return view('admin.allcorporatecatgory')
        ->with('allcorporatecatgory', $allcorporatecatgory);
    }
    public function submitCorporatecatgory(Request $req)
    {
        $req->validate([

            'name' => 'required',
            'image' => (isset($req->id) and $req->id > 0) ? '' : 'required',

        ]);
        
        if(isset($req->id) and $req->id != ''){
            $data=CorporateCategory::where('id',$req->id)->first();
            if($req->image){
                if($data->image){
                    @unlink("upload\images\/".$data->image);
                }
                $imageName = time().'.'.$req->image->getClientOriginalExtension();
                $req->image->move('upload/images', $imageName);
            }else{
                $imageName=$data->image;
            }

            $slug = str_slug($req['name'], "-");
            CorporateCategory::where('id',$req->id)->update([
                'name' => $req['name'],
                'slug' => $slug,
                'image' => $imageName,
                'status' => $req['status'],
            ]);
            return redirect('admin/allcorporatecatgory')->with('success', 'Category updated successfully.');
        }

        if($req->image){
            $imageName = time().'.'.$req->image->getClientOriginalExtension();
            $req->image->move('upload/images', $imageName);
        }else{
            $imageName='';
        }
        $slug = str_slug($req['name'], "-");
        CorporateCategory::create([
            'name' => $req['name'],
            'slug' => $slug,
            'image' => $imageName,
            'status' => $req['status'],
        ]);
        return redirect('admin/allcorporatecatgory')->with('success', 'Category created successfully.');
    }
    public function corporatecatgorydelete($id=0)
    {
        CorporateCategory::where('id',$id)->delete();
        return back()->with('success', 'Category Deleted Successfully.');
    }

    public function corporatecatgorystatus($id=0)
    {
        $data=CorporateCategory::where('id',$id)->first();
        if($data->status==1){
            CorporateCategory::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            CorporateCategory::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Category status changed successfully.');
    }

    
    public function addcorporatedetails($id=0)
    {
        $data = array();
        if($id>0){
            $data=CorporateDetail::where('id',$id)->first();
        }
        $allcorporatecatgory = CorporateCategory::get();
        return view('admin.addcorporatedetails')->with('edit',$data)->with('allcorporatecatgory',$allcorporatecatgory);
    }

    public function submitCorporate(Request $req)
    {
        $req->validate([

            'category' => 'required',
            'layout' => 'required',
            'title' => 'required',
            'video' => 'required',
            'banner' => (isset($req->id) and $req->id > 0) ? '' : 'required',
            'feature_image' => (isset($req->id) and $req->id > 0) ? '' : 'required',
            //'description' => 'required',
            'secound_title' => 'required',
            'secound_description' => 'required',

        ]);

        if($req->id){
            $data=CorporateDetail::where('id',$req->id)->first();
            if($req->banner){
                if($data->banner){
                    @unlink("upload\images\/".$data->banner);
                }
                $imageName = time().'.'.$req->banner->getClientOriginalExtension();
                $req->banner->move('upload/images', $imageName);
            }else{
                $imageName=$data->banner;
            }

            if($req->feature_image){
                if($data->feature_image){
                    @unlink("upload\images\/".$data->feature_image);
                }
                $feature = time().'.'.$req->feature_image->getClientOriginalExtension();
                $req->feature_image->move('upload/images', $feature);
            }else{
                $feature=$data->feature_image;
            }
            
            $slug = str_slug($req['title'], "-");
            CorporateDetail::where('id',$req->id)->update([
                'title' => $req['title'],
                'layout' => $req['layout'],
                'banner_description' => $req['banner_description'],
                'banner' => $imageName,
                'feature_image' => $feature,
                'description' => $req['description'],
                'category_id' => $req['category'],
                'main_video' => $req['video'],
                'sec_title' => $req['secound_title'],
                'sec_description' => $req['secound_description'],
            ]);

            if($req->hasfile('gallary'))
             {
                foreach($req->file('gallary') as $image)
                {
                    $name=$image->getClientOriginalName();
                    $image->move('upload/images', $name);
                    CorporateImage::create([
                        'cp_id'=>$req->id,
                        'image'=>$name,
                    ]);
                }
             }

             if($req->has('videotitle'))
             {
                foreach($req->videotitle as $key => $title)
                {
                  if($title && $req->videos[$key]){
                    CorporateVideo::create([
                        'cp_id'=>$req->id,
                        'title'=>$title,
                        'video'=>$req->videos[$key],
                    ]);
                  }
                    
                }
             }
             if($req->has('faq'))
             {
                foreach($req->faq as $key => $title)
                {
                    if($title){
                    CorporateFaq::create([
                        'cp_id'=>$req->id,
                        'title'=>$title,
                    ]);
                    }
                  
                    
                }
             }
            return redirect('admin/allcorporatedetails')->with('success', 'Corporate updated successfully.');

        }else{
            if($req->banner){
                $imageName = time().'.'.$req->banner->getClientOriginalExtension();
                $req->banner->move('upload/images', $imageName);
            }else{
                $imageName='';
            }
            if($req->feature_image){
                $feature = time().'.'.$req->feature_image->getClientOriginalExtension();
                $req->feature_image->move('upload/images', $feature);
            }else{
                $feature='';
            }
            
            $slug = str_slug($req['title'], "-");
            $mid=CorporateDetail::create([
                'title' => $req['title'],
                'layout' => $req['layout'],
                'banner_description' => $req['banner_description'],
                'banner' => $imageName,
                'feature_image' => $feature,
                'description' => $req['description'],
                'category_id' => $req['category'],
                'main_video' => $req['video'],
                'sec_title' => $req['secound_title'],
                'sec_description' => $req['secound_description'],
            ]);
            
             if($req->hasfile('gallary'))
             {
                foreach($req->file('gallary') as $image)
                {
                    $name=$image->getClientOriginalName();
                    $image->move('upload/images', $name);
                    CorporateImage::create([
                        'cp_id'=>$mid->id,
                        'image'=>$name,
                    ]);
                }
             }

             if($req->has('videotitle'))
             {
                foreach($req->videotitle as $key => $title)
                {
                    if($title && $req->videos[$key]){
                    CorporateVideo::create([
                        'cp_id'=>$mid->id,
                        'title'=>$title,
                        'video'=>$req->videos[$key],
                    ]);
                    }
                }
             }
             if($req->has('faq'))
             {
                foreach($req->faq as $key => $title)
                {
                    if($title){
                        CorporateFaq::create([
                            'cp_id'=>$mid->id,
                            'title'=>$title,
                        ]);
                    }
                    
                  
                    
                }
             }
        }
        
        return back()->with('success', 'Corporate Created successfully.');
    }
    public function allcorporatedetails()
    {
        $allcorporatedetails = CorporateDetail::get();
        return view('admin.allcorporatedetails')
        ->with('allcorporatedetails', $allcorporatedetails);
    }
    public function addcorporatedetailsdelete($id=0)
    {
        CorporateDetail::where('id',$id)->delete();
        return back()->with('success', 'Corporate Deleted Successfully.');
    }
    public function deletecorporategalleryimage(Request $req)
    {
        $img=CorporateImage::where('id',$req->id)->first();
        @unlink("upload\images\/".$img->image);
        CorporateImage::where('id',$req->id)->delete();
        echo 1;
        //return back()->with('success', 'Corporate gallery image deleted successfully.');
    }
    public function deletecorporatevideo(Request $req)
    {
        
        CorporateVideo::where('id',$req->id)->delete();
        echo 1;
        //return back()->with('success', 'Corporate video deleted successfully.');
    }

    public function addcorporateblog($id=0)
    {
        $data = array();
        if($id>0){
            $data=CorporateBlog::where('id',$id)->first();
        }
        $allcorporatecatgory = CorporateCategory::get();
        return view('admin.addcorporateblog')->with('edit',$data)->with('allcorporatecatgory',$allcorporatecatgory);
    }
    public function allcorporateblog()
    {
        $allcorporateblog = CorporateBlog::get();
        return view('admin.allcorporateblog')
        ->with('allcorporateblogs', $allcorporateblog);
    }

    public function submitCorporateblog(Request $req)
    {
        $req->validate([

            'name' => 'required',
            'category' => 'required',
            'description' => 'required',
            'image' => (isset($req->id) and $req->id > 0) ? '' : 'required',

        ]);
        
        if(isset($req->id) and $req->id != ''){
            $data=CorporateBlog::where('id',$req->id)->first();
            if($req->image){
                if($data->image){
                    @unlink("upload\images\/".$data->image);
                }
                $imageName = time().'.'.$req->image->getClientOriginalExtension();
                $req->image->move('upload/images', $imageName);
            }else{
                $imageName=$data->image;
            }

            $slug = str_slug($req['name'], "-").'-'.$req->id;
            CorporateBlog::where('id',$req->id)->update([
                'title' => $req['name'],
                'category_id' => $req['category'],
                'slug' => $slug,
                'image' => $imageName,
                'description' => $req['description'],
            ]);
            return redirect('admin/allcorporateblog')->with('success', 'Blog updated successfully.');
        }

        if($req->image){
            $imageName = time().'.'.$req->image->getClientOriginalExtension();
            $req->image->move('upload/images', $imageName);
        }else{
            $imageName='';
        }
        $data=CorporateBlog::orderBy('id','DESC')->first();
        if($data){
            $slug = str_slug($req['name'], "-").'-'.$data->id;
        }else{
            $slug = str_slug($req['name'], "-");
        }
        CorporateBlog::create([
            'title' => $req['name'],
            'category_id' => $req['category'],
            'slug' => $slug,
            'image' => $imageName,
            'description' => $req['description'],
        ]);
        return redirect('admin/allcorporateblog')->with('success', 'Blog created successfully.');
    }
    public function corporateblogdelete($id=0)
    {
        CorporateBlog::where('id',$id)->delete();
        return back()->with('success', 'Blog Deleted Successfully.');
    }
    public function deletecorporatefaq(Request $req)
    {
        CorporateFaq::where('id',$req->id)->delete();
        echo 1;
        //return back()->with('success', 'FAQ Deleted Successfully.');
    }
    
    
}
