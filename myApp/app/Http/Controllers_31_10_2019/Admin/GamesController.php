<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MstGame;
use App\Models\MstSport;
use App\Models\GameImages;
use App\Models\RepeatGames;
use Auth;
use DateTime;
use DatePeriod;
use DateInterval;


class GamesController extends Controller
{
    public function allusergame()
    {
        $games=\DB::table('mst_games')
        ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
        ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
        ->where('is_admin',0)
        ->get();
        return view('admin.allusergame')->with('games',$games);
    }
    public function alladmingame()
    {
        $games=\DB::table('mst_games')
        ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
        ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
        ->where('is_admin',1)
        ->get();
        return view('admin.alladmingame')->with('games',$games);
    }
    public function addgame($id=0)
    {
        $data=array();
        if($id>0){
            $data=MstGame::where('id',$id)->first();
        }
        $sports=MstSport::where('status',1)->get();
        return view('admin.addgame')->with('edit',$data)->with('sports',$sports);
    }
    public function creategame(Request $req)
    {
        $user_id=Auth::guard('admin')->user()->id;
        $req->validate([
            'title' => ['required'],
            'venue' => ['required'],
            'sport' => ['required'],
            'game_date' => ['required'],
            'game_start_time' => ['required'],
            'game_finish_time' => ['required'],
            'gender' => ['required'],
            'payment' => ['required'],
            'team_limit' => ['required'],
        ]);

        if($req['game_date']){
            $game_date = date('Y-m-d', strtotime($req['game_date']));
            $end_date = date('Y-m-d', strtotime($req['end_date']));
        }else{
            $game_date = NULL;
            $end_date = NULL;
        }

        if($req->id){
            $data=MstGame::where('id',$req->id)->first();
            if($req->featured_img){
                if($data->featured_img){
                    @unlink("upload\images\/".$data->featured_img);
                }
                $imageName = time().'.'.$req->featured_img->getClientOriginalExtension();
                $req->featured_img->move('upload/images', $imageName);
            }else{
                $imageName=$data->featured_img;
            }
            $slug = str_slug($req['title'], "-");
            MstGame::where('id',$req->id)->update([
                'title' => $req['title'],
                'slug' => $slug,
                'sport_id' => $req['sport'],
                'featured_img' => $imageName,
                'description' => $req['description'],
                'game_date' => $game_date,
                'game_start_time' => $req['game_start_time'],
                'game_finish_time' => $req['game_finish_time'],
                'gender' => $req['gender'],
                'payment' => $req['payment'],
                'team_limit' => $req['team_limit'],
                'venue_id' => $req['venue_id'],
                'is_private' => $req['is_private'],
                'price' => $req['price'],
                'currency' => $req['currency'],
                'is_refund' => $req['is_refund'],
                'refund_changed_rsvp' => $req['refund_changed_rsvp'],
                'refund_days' => $req['refund_days'],
                'is_repeat' => (isset($req->repeat_check)) ? $req['is_repeat'] : 0,
                'end_date' => $end_date,
                'days' => serialize($req['days']),
            ]);
            
            if($req['repeat_check']){
            if($req->has('days'))
            {
                

                    RepeatGames::where('game_id',$req->id)->where('game_start_date','>=',date('Y-m-d'))->delete();

                    $start = new DateTime($game_date);
                    $end = new DateTime($end_date);
                    $end = $end->modify('+1 day');
                    $interval = DateInterval::createFromDateString('1 day');
                    $period = new DatePeriod($start, $interval, $end);
                    
                    /* days output */
                    foreach ($period as $date) {
                        $day = $date->format("N");
                        
                        if (in_array($day, $req['days']))
                            {
                                if($date->format("Y-m-d") >= date('Y-m-d')){
                                    RepeatGames::create([
                                        'game_id'=>$req->id,
                                        'game_start_date'=>$date->format("Y-m-d"),
                                    ]);
                                }
                                
                            }
                    }
                }
                   
                }else{
                    if(!isset($req['repeat_check'])){
                        $datecheck=RepeatGames::where('game_id',$req->id)->where('game_start_date','=',$game_date)->first();
                        if(!$datecheck){
                            RepeatGames::create([
                                'game_id'=>$req->id,
                                'game_start_date'=>$game_date,
                            ]);
                        }
                        
                     }
                }

            if($req->hasfile('gallary'))
             {
                foreach($req->file('gallary') as $image)
                {
                    $name=$image->getClientOriginalName();
                    $image->move('upload/images', $name);
                    GameImages::create([
                        'game_id'=>$req->id,
                        'image'=>$name,
                    ]);
                }
             }
            return redirect('admin/alladmingame')->with('success', 'Game updated successfully.');

        }else{
            if($req->featured_img){
                $imageName = time().'.'.$req->featured_img->getClientOriginalExtension();
                $req->featured_img->move('upload/images', $imageName);
            }else{
                $imageName='';
            }
            $slug = str_slug($req['title'], "-");
            $mid=MstGame::create([
                'user_id' => $user_id,
                'is_admin' => 1,
                'title' => $req['title'],
                'slug' => $slug,
                'sport_id' => $req['sport'],
                'featured_img' => $imageName,
                'description' => $req['description'],
                'game_date' => $game_date,
                'game_start_time' => $req['game_start_time'],
                'game_finish_time' => $req['game_finish_time'],
                'gender' => $req['gender'],
                'payment' => $req['payment'],
                'team_limit' => $req['team_limit'],
                'venue_id' => $req['venue_id'],
                'is_private' => $req['is_private'],
                'price' => $req['price'],
                'currency' => $req['currency'],
                'is_refund' => $req['is_refund'],
                'refund_changed_rsvp' => $req['refund_changed_rsvp'],
                'refund_days' => $req['refund_days'],
                'is_repeat' => $req['is_repeat'],
                'end_date' => $end_date,
                'status' => 1,
                'days' => serialize($req['days']),
            ]);

            if($req->has('days'))
            {
                if($req['is_repeat']){
                    $start = new DateTime($game_date);
                    $end = new DateTime($end_date);
                    $end = $end->modify('+1 day');
                    $interval = DateInterval::createFromDateString('1 day');
                    $period = new DatePeriod($start, $interval, $end);
                    
                    /* days output */
                    foreach ($period as $date) {
                        $day = $date->format("N");
                        if (in_array($day, $req['days']))
                            {
                                RepeatGames::create([
                                    'game_id'=>$mid->id,
                                    'game_start_date'=>$date->format("Y-m-d"),
                                ]);
                            }
                    }
                }
                   
                }else{
                    if(!isset($req['is_repeat'])){
                        RepeatGames::create([
                            'game_id'=>$mid->id,
                            'game_start_date'=>$game_date,
                        ]);
                     }
                }
            
            if($req->hasfile('gallary'))
             {
                foreach($req->file('gallary') as $image)
                {
                    $name=$image->getClientOriginalName();
                    $image->move('upload/images', $name);
                    GameImages::create([
                        'game_id'=>$mid->id,
                        'image'=>$name,
                    ]);
                }
             }
        }
        
        return back()->with('success', 'Game Created successfully.');
    }
    public function gamedelete($id=0)
    {
        MstGame::where('id',$id)->delete();
        RepeatGames::where('game_id',$id)->delete();
        return back()->with('success', 'Game Deleted Successfully.');
    }

    public function gamedeletegalleryimage(Request $req)
    {
        $img=GameImages::where('id',$req->id)->first();
        @unlink("upload\images\/".$img->image);
        GameImages::where('id',$req->id)->delete();

        echo 1;
        
        //return back()->with('success', 'Venue gallery image deleted successfully.');
    }
    public function gamestatus($id=0)
    {
        $data=MstGame::where('id',$id)->first();
        if($data->status==1){
            MstGame::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            MstGame::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Game status changed successfully.');
    }
}
