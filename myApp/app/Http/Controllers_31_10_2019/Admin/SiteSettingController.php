<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SiteSetting;

class SiteSettingController extends Controller
{
    public function index()
    {
        $data = array();

        $data=SiteSetting::first();
    
        return view('admin.sitesetting')->with('edit',$data);
    }
    public function submitSiteSetting(Request $req)
    {
        if(isset($req->id) and $req->id != ''){
            $setting=SiteSetting::where('id',$req->id)->first();
            if($req->header_logo){
                if($setting->header_logo){
                    @unlink("upload\images\/".$setting->header_logo);
                }
                $header_logo = time().'.'.$req->header_logo->getClientOriginalExtension();
                $req->header_logo->move('upload/images', $header_logo);
            }else{
                $header_logo=$setting->header_logo;
            }

            if($req->footer_logo){
                if($setting->footer_logo){
                    @unlink("upload\images\/".$setting->footer_logo);
                }
                $footer_logo = time().'.'.$req->footer_logo->getClientOriginalExtension();
                $req->footer_logo->move('upload/images', $footer_logo);
            }else{
                $footer_logo=$setting->footer_logo;
            }

            if($req->favicon_icon){
                if($setting->favicon_icon){
                    @unlink("upload\images\/".$setting->favicon_icon);
                }
                $favicon_icon = time().'.'.$req->favicon_icon->getClientOriginalExtension();
                $req->favicon_icon->move('upload/images', $favicon_icon);
            }else{
                $favicon_icon=$setting->favicon_icon;
            }

            SiteSetting::where('id',$req->id)->update([
                'website_name' => $req['website_name'],
                'website_title' => $req['website_title'],
                'header_logo' => $header_logo,
                'footer_logo' => $footer_logo,
                'favicon_icon' => $favicon_icon,
                'app_store_link' => $req['app_store_link'],
                'play_store_link' => $req['play_store_link'],
                'facebook_url' => $req['facebook_url'],
                'twitter_url' => $req['twitter_url'],
                'instagram_url' => $req['instagram_url'],
                'copyright' => $req['copyright'],
                'website_email' => $req['website_email'],
                'sendmail_email' => $req['sendmail_email'],
                'phone_number' => $req['phone_number'],
                'address' => $req['address'],
                'city' => $req['city'],
                'latitude' => $req['latitude'],
                'longitude' => $req['longitude'],
            ]);
            return redirect('admin/sitesetting')->with('success', 'Updated successfully.');
        }
        if($req->header_logo){
            $header_logo = time().'.'.$req->header_logo->getClientOriginalExtension();
            $req->header_logo->move('upload/images', $header_logo);
        }else{
            $header_logo='';
        }
        if($req->footer_logo){
            $footer_logo = time().'.'.$req->footer_logo->getClientOriginalExtension();
            $req->footer_logo->move('upload/images', $footer_logo);
        }else{
            $footer_logo='';
        }
        if($req->favicon_icon){
            $favicon_icon = time().'.'.$req->favicon_icon->getClientOriginalExtension();
            $req->favicon_icon->move('upload/images', $favicon_icon);
        }else{
            $favicon_icon='';
        }
        SiteSetting::create([
            'website_name' => $req['website_name'],
            'website_title' => $req['website_title'],
            'header_logo' => $header_logo,
            'footer_logo' => $footer_logo,
            'favicon_icon' => $favicon_icon,
            'app_store_link' => $req['app_store_link'],
            'play_store_link' => $req['play_store_link'],
            'facebook_url' => $req['facebook_url'],
            'twitter_url' => $req['twitter_url'],
            'instagram_url' => $req['instagram_url'],
            'copyright' => $req['copyright'],
            'website_email' => $req['website_email'],
            'sendmail_email' => $req['sendmail_email'],
            'phone_number' => $req['phone_number'],
            'address' => $req['address'],
            'city' => $req['city'],
            'latitude' => $req['latitude'],
            'longitude' => $req['longitude'],
        ]);
        return redirect('admin/sitesetting')->with('success', 'Updated successfully.');
    }
}
