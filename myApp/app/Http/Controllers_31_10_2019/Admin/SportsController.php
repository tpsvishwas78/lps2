<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MstSport;

class SportsController extends Controller
{
    public function index()
    {
        $sports = MstSport::get();
        return view('admin.allsports')
        ->with('sports', $sports);
    }
    public function addsport($id=0)
    {
        $sports=MstSport::where('status',1)->get();
        $data = array();
        if($id>0){
            $data=MstSport::where('id',$id)->first();
        }
        return view('admin.addsport')->with('edit',$data)->with('sports',$sports);
    }
    public function submitSport(Request $req)
    {
        //dd($req->image);
        $req->validate([

            'name' => 'required',

        ]);
        if(isset($req->id) and $req->id != ''){
            $sport=MstSport::where('id',$req->id)->first();
            if($req->image){
                if($sport->image){
                    @unlink("upload\images\/".$sport->image);
                }
                $imageName = time().'.'.$req->image->getClientOriginalExtension();
                $req->image->move('upload/images', $imageName);
            }else{
                $imageName=$sport->image;
            }
            MstSport::where('id',$req->id)->update([
                'name' => $req['name'],
                'description' => $req['description'],
                'image' => $imageName,
                'sport_order' => $req['sport_order'],
                'main_id' => $req['main_id'],
            ]);
            return redirect('admin/allsports')->with('success', 'Sport updated successfully.');
        }
        if($req->image){
            $imageName = time().'.'.$req->image->getClientOriginalExtension();
            $req->image->move('upload/images', $imageName);
        }else{
            $imageName='';
        }
        
        MstSport::create([
            'name' => $req['name'],
            'description' => $req['description'],
            'image' => $imageName,
            'sport_order' => $req['sport_order'],
            'main_id' => $req['main_id'],
        ]);
        return back()->with('success', 'Sport created successfully.');
    }
    public function sportdelete($id=0)
    {
        MstSport::where('id',$id)->delete();
        return back()->with('success', 'Sport Deleted Successfully.');
    }
    public function sportstatus($id=0)
    {
        $data=MstSport::where('id',$id)->first();
        if($data->status==1){
            MstSport::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            MstSport::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Sport status changed successfully.');
    }
}
