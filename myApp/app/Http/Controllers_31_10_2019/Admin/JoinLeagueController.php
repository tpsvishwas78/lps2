<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\JoinLeague;
use App\Models\JoinLeagueBlock;

class JoinLeagueController extends Controller
{
    public function joinleague()
    {
        $data=JoinLeague::orderBy('id','DESC')->first();
        return view('admin.joinleague')->with('edit',$data);
    }
    public function submitJoinLeague(Request $req)
    {
        $req->validate([
            'banner_title' => 'required',
            'title' => 'required',
            'description' => 'required',
            'banner' => (isset($req->id) and $req->id > 0) ? '' : 'required',
        ]);
        
        if(isset($req->id) and $req->id != ''){
            $data=JoinLeague::where('id',$req->id)->first();
            if($req->banner){
                if($data->banner){
                    @unlink("upload\images\/".$data->banner);
                }
                $imageName = time().'.'.$req->banner->getClientOriginalExtension();
                $req->banner->move('upload/images', $imageName);
            }else{
                $imageName=$data->banner;
            }

            JoinLeague::where('id',$req->id)->update([
                'banner_title' => $req['banner_title'],
                'title' => $req['title'],
                'banner' => $imageName,
                'description' => $req['description'],
            ]);
            return redirect('admin/joinleague')->with('success', 'Join a league updated successfully.');
        }

        if($req->banner){
            $imageName = time().'.'.$req->banner->getClientOriginalExtension();
            $req->banner->move('upload/images', $imageName);
        }else{
            $imageName='';
        }
        JoinLeague::create([
            'banner_title' => $req['banner_title'],
            'title' => $req['title'],
            'banner' => $imageName,
            'description' => $req['description'],
        ]);
        return redirect('admin/joinleague')->with('success', 'Join a league updated successfully.');
    }
    public function addjoinleagueblock($id=0)
    {
        $data = array();
        if($id>0){
            $data=JoinLeagueBlock::where('id',$id)->first();
        }
        return view('admin.addjoinleagueblock')->with('edit',$data);
    }
    public function alljoinleagueblock()
    {
        $alljoinleagueblock = JoinLeagueBlock::get();
        return view('admin.alljoinleagueblock')
        ->with('alljoinleagueblock', $alljoinleagueblock);
    }

    public function submitJoinLeagueBlock(Request $req)
    {
        $req->validate([

            'title' => 'required',
            'description' => 'required',
            'image' => (isset($req->id) and $req->id > 0) ? '' : 'required',

        ]);
        
        if(isset($req->id) and $req->id != ''){
            $data=JoinLeagueBlock::where('id',$req->id)->first();
            if($req->image){
                if($data->image){
                    @unlink("upload\images\/".$data->image);
                }
                $imageName = time().'.'.$req->image->getClientOriginalExtension();
                $req->image->move('upload/images', $imageName);
            }else{
                $imageName=$data->image;
            }

            JoinLeagueBlock::where('id',$req->id)->update([
                'title' => $req['title'],
                'image' => $imageName,
                'description' => $req['description'],
            ]);
            return redirect('admin/alljoinleagueblock')->with('success', 'Block updated successfully.');
        }

        if($req->image){
            $imageName = time().'.'.$req->image->getClientOriginalExtension();
            $req->image->move('upload/images', $imageName);
        }else{
            $imageName='';
        }
        
        JoinLeagueBlock::create([
            'title' => $req['title'],
            'image' => $imageName,
            'description' => $req['description'],
        ]);
        return redirect('admin/alljoinleagueblock')->with('success', 'Block created successfully.');
    }
    public function joinleagueblockdelete($id=0)
    {
        JoinLeagueBlock::where('id',$id)->delete();
        return back()->with('success', 'Block Deleted Successfully.');
    }
}
