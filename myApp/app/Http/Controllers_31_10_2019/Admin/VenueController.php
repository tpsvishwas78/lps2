<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\MstVenue;

class VenueController extends Controller
{
    public function index()
    {
        $venues = MstVenue::get();
        return view('admin.allvenues')
        ->with('venues', $venues);
    }

    public function addvenue($id=0)
    {
        $data = array();
        if($id>0){
            $data=MstVenue::where('id',$id)->first();
        }
        return view('admin.addvenue')->with('edit',$data);
    }

    public function submitVenue(Request $req)
    {        
        $req->validate([

            'location' => 'required',
            'name' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'postcode' => 'required',

        ]);
        
        if(isset($req->id) and $req->id != ''){
            $venue=MstVenue::where('id',$req->id)->first();
            if($req->image){
                if($venue->image){
                    @unlink("upload\images\/".$venue->image);
                }
                $imageName = time().'.'.$req->image->getClientOriginalExtension();
                $req->image->move('upload/images', $imageName);
            }else{
                $imageName=$venue->image;
            }
            $slug = str_slug($req['name'], "-").'-'.$req->id;
            MstVenue::where('id',$req->id)->update([
                'location' => $req['location'],
                'name' => $req['name'],
                'slug' => $slug,
                'image' => $imageName,
                'address' => $req['address'],
                'description' => $req['description'],
                'postcode' => $req['postcode'],
                'latitude' => $req['latitude'],
                'longitude' => $req['longitude'],
            ]);
            return redirect('admin/allvenues')->with('success', 'Venue updated successfully.');
        }
        if($req->image){
            $imageName = time().'.'.$req->image->getClientOriginalExtension();
            $req->image->move('upload/images', $imageName);
        }else{
            $imageName='';
        }
        $venue=MstVenue::orderBy('id', 'DESC')->first();
        $slug = str_slug($req['name'], "-").'-'.($venue->id+1);
        MstVenue::create([
            'location' => $req['location'],
            'name' => $req['name'],
            'slug' => $slug,
            'image' => $imageName,
            'address' => $req['address'],
            'description' => $req['description'],
            'postcode' => $req['postcode'],
            'latitude' => $req['latitude'],
            'longitude' => $req['longitude'],
        ]);
        return redirect('admin/allvenues')->with('success', 'Venue created successfully.');
    }

    public function venuedelete($id=0)
    {
        MstVenue::where('id',$id)->delete();
        return back()->with('success', 'Venue Deleted Successfully.');
    }

    public function venuestatus($id=0)
    {
        $data=MstVenue::where('id',$id)->first();
        if($data->status==1){
            MstVenue::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            MstVenue::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Venue status changed successfully.');
    }
}
