<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SportTurnamentCategory;
use App\Models\SportTurnamentDetail;
use App\Models\SportTurnamentImage;
use App\Models\SportTurnamentVideo;
use App\Models\SportTurnamentBlog;
use App\Models\SportTurnamentFaq;

class SportsTurnamentsController extends Controller
{
    public function addsportsturnamentscatgory($id=0)
    {
        $data = array();
        if($id>0){
            $data=SportTurnamentCategory::where('id',$id)->first();
        }
        return view('admin.addsportsturnamentscatgory')->with('edit',$data);
    }
    public function allsportsturnamentscatgory()
    {
        $allsportsturnamentscatgory = SportTurnamentCategory::get();
        return view('admin.allsportsturnamentscatgory')
        ->with('allsportsturnamentscatgory', $allsportsturnamentscatgory);
    }
    public function submitsportsturnamentscatgory(Request $req)
    {
        $req->validate([

            'name' => 'required',
            'image' => (isset($req->id) and $req->id > 0) ? '' : 'required',

        ]);
        
        if(isset($req->id) and $req->id != ''){
            $data=SportTurnamentCategory::where('id',$req->id)->first();
            if($req->image){
                if($data->image){
                    @unlink("upload\images\/".$data->image);
                }
                $imageName = time().'.'.$req->image->getClientOriginalExtension();
                $req->image->move('upload/images', $imageName);
            }else{
                $imageName=$data->image;
            }

            $slug = str_slug($req['name'], "-");
            SportTurnamentCategory::where('id',$req->id)->update([
                'name' => $req['name'],
                'slug' => $slug,
                'image' => $imageName,
                'status' => $req['status'],
            ]);
            return redirect('admin/allsportsturnamentscatgory')->with('success', 'Category updated successfully.');
        }

        if($req->image){
            $imageName = time().'.'.$req->image->getClientOriginalExtension();
            $req->image->move('upload/images', $imageName);
        }else{
            $imageName='';
        }
        $slug = str_slug($req['name'], "-");
        SportTurnamentCategory::create([
            'name' => $req['name'],
            'slug' => $slug,
            'image' => $imageName,
            'status' => $req['status'],
        ]);
        return redirect('admin/allsportsturnamentscatgory')->with('success', 'Category created successfully.');
    }
    public function sportsturnamentscatgorydelete($id=0)
    {
        SportTurnamentCategory::where('id',$id)->delete();
        return back()->with('success', 'Category Deleted Successfully.');
    }

    public function sportsturnamentscatgorystatus($id=0)
    {
        $data=SportTurnamentCategory::where('id',$id)->first();
        if($data->status==1){
            SportTurnamentCategory::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            SportTurnamentCategory::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Category status changed successfully.');
    }

    
    public function addsportsturnamentsdetails($id=0)
    {
        $data = array();
        if($id>0){
            $data=SportTurnamentDetail::where('id',$id)->first();
        }
        $allsportsturnamentscatgory = SportTurnamentCategory::get();
        return view('admin.addsportsturnamentsdetails')->with('edit',$data)->with('allsportsturnamentscatgory',$allsportsturnamentscatgory);
    }

    public function submitsportsturnaments(Request $req)
    {
        $req->validate([

            'category' => 'required',
            'layout' => 'required',
            'title' => 'required',
            'video' => 'required',
            'banner' => (isset($req->id) and $req->id > 0) ? '' : 'required',
            'feature_image' => (isset($req->id) and $req->id > 0) ? '' : 'required',
            //'description' => 'required',
            'secound_title' => 'required',
            'secound_description' => 'required',

        ]);

        if($req->id){
            $data=SportTurnamentDetail::where('id',$req->id)->first();
            if($req->banner){
                if($data->banner){
                    @unlink("upload\images\/".$data->banner);
                }
                $imageName = time().'.'.$req->banner->getClientOriginalExtension();
                $req->banner->move('upload/images', $imageName);
            }else{
                $imageName=$data->banner;
            }

            if($req->feature_image){
                if($data->feature_image){
                    @unlink("upload\images\/".$data->feature_image);
                }
                $feature = time().'.'.$req->feature_image->getClientOriginalExtension();
                $req->feature_image->move('upload/images', $feature);
            }else{
                $feature=$data->feature_image;
            }
            
            $slug = str_slug($req['title'], "-");
            SportTurnamentDetail::where('id',$req->id)->update([
                'title' => $req['title'],
                'layout' => $req['layout'],
                'banner' => $imageName,
                'feature_image' => $feature,
                'description' => $req['description'],
                'category_id' => $req['category'],
                'main_video' => $req['video'],
                'sec_title' => $req['secound_title'],
                'sec_description' => $req['secound_description'],
            ]);

            if($req->hasfile('gallary'))
             {
                foreach($req->file('gallary') as $image)
                {
                    $name=$image->getClientOriginalName();
                    $image->move('upload/images', $name);
                    SportTurnamentImage::create([
                        'cp_id'=>$req->id,
                        'image'=>$name,
                    ]);
                }
             }

             if($req->has('videotitle'))
             {
                foreach($req->videotitle as $key => $title)
                {
                  if($title && $req->videos[$key]){
                    SportTurnamentVideo::create([
                        'cp_id'=>$req->id,
                        'title'=>$title,
                        'video'=>$req->videos[$key],
                    ]);
                  }
                    
                }
             }
             if($req->has('faq'))
             {
                foreach($req->faq as $key => $title)
                {
                    if($title){
                    SportTurnamentFaq::create([
                        'cp_id'=>$req->id,
                        'title'=>$title,
                    ]);
                    }
                  
                    
                }
             }
            return redirect('admin/allsportsturnamentsdetails')->with('success', 'sportsturnaments updated successfully.');

        }else{
            if($req->banner){
                $imageName = time().'.'.$req->banner->getClientOriginalExtension();
                $req->banner->move('upload/images', $imageName);
            }else{
                $imageName='';
            }
            if($req->feature_image){
                $feature = time().'.'.$req->feature_image->getClientOriginalExtension();
                $req->feature_image->move('upload/images', $feature);
            }else{
                $feature='';
            }
            
            $slug = str_slug($req['title'], "-");
            $mid=SportTurnamentDetail::create([
                'title' => $req['title'],
                'layout' => $req['layout'],
                'banner' => $imageName,
                'feature_image' => $feature,
                'description' => $req['description'],
                'category_id' => $req['category'],
                'main_video' => $req['video'],
                'sec_title' => $req['secound_title'],
                'sec_description' => $req['secound_description'],
            ]);
            
             if($req->hasfile('gallary'))
             {
                foreach($req->file('gallary') as $image)
                {
                    $name=$image->getClientOriginalName();
                    $image->move('upload/images', $name);
                    SportTurnamentImage::create([
                        'cp_id'=>$mid->id,
                        'image'=>$name,
                    ]);
                }
             }

             if($req->has('videotitle'))
             {
                foreach($req->videotitle as $key => $title)
                {
                    if($title && $req->videos[$key]){
                    SportTurnamentVideo::create([
                        'cp_id'=>$mid->id,
                        'title'=>$title,
                        'video'=>$req->videos[$key],
                    ]);
                    }
                }
             }
             if($req->has('faq'))
             {
                foreach($req->faq as $key => $title)
                {
                    if($title){
                        SportTurnamentFaq::create([
                            'cp_id'=>$mid->id,
                            'title'=>$title,
                        ]);
                    }
                    
                  
                    
                }
             }
        }
        
        return back()->with('success', 'sportsturnaments Created successfully.');
    }
    public function allsportsturnamentsdetails()
    {
        $allsportsturnamentsdetails = SportTurnamentDetail::get();
        return view('admin.allsportsturnamentsdetails')
        ->with('allsportsturnamentsdetails', $allsportsturnamentsdetails);
    }
    public function addsportsturnamentsdetailsdelete($id=0)
    {
        SportTurnamentDetail::where('id',$id)->delete();
        return back()->with('success', 'sportsturnaments Deleted Successfully.');
    }
    public function deletesportsturnamentsgalleryimage(Request $req)
    {
        $img=SportTurnamentImage::where('id',$req->id)->first();
        @unlink("upload\images\/".$img->image);
        SportTurnamentImage::where('id',$req->id)->delete();
        echo 1;
        //return back()->with('success', 'sportsturnaments gallery image deleted successfully.');
    }
    public function deletesportsturnamentsvideo(Request $req)
    {
        SportTurnamentVideo::where('id',$req->id)->delete();
        echo 1;
        //return back()->with('success', 'sportsturnaments video deleted successfully.');
    }

    public function addsportsturnamentsblog($id=0)
    {
        $data = array();
        if($id>0){
            $data=SportTurnamentBlog::where('id',$id)->first();
        }
        $allsportsturnamentscatgory = SportTurnamentCategory::get();
        return view('admin.addsportsturnamentsblog')->with('edit',$data)->with('allsportsturnamentscatgory',$allsportsturnamentscatgory);
    }
    public function allsportsturnamentsblog()
    {
        $allsportsturnamentsblog = SportTurnamentBlog::get();
        return view('admin.allsportsturnamentsblog')
        ->with('allsportsturnamentsblogs', $allsportsturnamentsblog);
    }

    public function submitsportsturnamentsblog(Request $req)
    {
        $req->validate([

            'name' => 'required',
            'category' => 'required',
            'description' => 'required',
            'image' => (isset($req->id) and $req->id > 0) ? '' : 'required',

        ]);
        
        if(isset($req->id) and $req->id != ''){
            $data=SportTurnamentBlog::where('id',$req->id)->first();
            if($req->image){
                if($data->image){
                    @unlink("upload\images\/".$data->image);
                }
                $imageName = time().'.'.$req->image->getClientOriginalExtension();
                $req->image->move('upload/images', $imageName);
            }else{
                $imageName=$data->image;
            }

            $slug = str_slug($req['name'], "-").'-'.$req->id;
            SportTurnamentBlog::where('id',$req->id)->update([
                'title' => $req['name'],
                'category_id' => $req['category'],
                'slug' => $slug,
                'image' => $imageName,
                'description' => $req['description'],
            ]);
            return redirect('admin/allsportsturnamentsblog')->with('success', 'Blog updated successfully.');
        }

        if($req->image){
            $imageName = time().'.'.$req->image->getClientOriginalExtension();
            $req->image->move('upload/images', $imageName);
        }else{
            $imageName='';
        }
        $data=SportTurnamentBlog::orderBy('id','DESC')->first();
        if($data){
            $slug = str_slug($req['name'], "-").'-'.$data->id;
        }else{
            $slug = str_slug($req['name'], "-");
        }
        SportTurnamentBlog::create([
            'title' => $req['name'],
            'category_id' => $req['category'],
            'slug' => $slug,
            'image' => $imageName,
            'description' => $req['description'],
        ]);
        return redirect('admin/allsportsturnamentsblog')->with('success', 'Blog created successfully.');
    }
    public function sportsturnamentsblogdelete($id=0)
    {
        SportTurnamentBlog::where('id',$id)->delete();
        return back()->with('success', 'Blog Deleted Successfully.');
    }
    public function deletesportsturnamentsfaq(Request $req)
    {
        SportTurnamentFaq::where('id',$req->id)->delete();
        echo 1;
       // return back()->with('success', 'FAQ Deleted Successfully.');
    }
    
}
