<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\HomeBanner;
use App\Models\HomeAboutUs;
use App\Models\BasicFeatures;
use App\Models\CommonVideo;

class HomeCmsController extends Controller
{
    public function index()
    {
        $banners = HomeBanner::get();
        return view('admin.allhomebanners')->with('banners',$banners);
    }
    public function addhomebanners($id=0)
    {
        $data = array();
        if($id>0){
            $data=HomeBanner::where('id',$id)->first();
        }
        return view('admin.addhomebanners')->with('edit',$data);
    }
    public function submitHomeBanner(Request $req)
    {
        $req->validate([
            'banner_type'=>'required',
            //'banner'=>'required',
            'title_one'=>'required',
            'title_two'=>'required',
            'title_three'=>'required',
            'status'=>'required'
            ]);

            if(isset($req->id) and $req->id != ''){
                $banner=HomeBanner::where('id',$req->id)->first();
                if($req->banner){
                    if($banner->banner){
                        @unlink("upload\banners\/".$banner->banner);
                    }
                    $imageName = time().'.'.$req->banner->getClientOriginalExtension();
                    $req->banner->move('upload/banners', $imageName);
                }else{
                    $imageName=$banner->banner;
                }
                HomeBanner::where('id',$req->id)->update([
                    'title_one' => $req['title_one'],
                    'title_two' => $req['title_two'],
                    'banner' => $imageName,
                    'title_three' => $req['title_three'],
                    'button_link' => $req['button_link'],
                    'banner_type' => $req['banner_type'],
                    'status' => $req['status'],
                ]);
                return redirect('admin/allhomebanners')->with('success', 'Banner updated successfully.');
            }
            if($req->banner){
                $imageName = time().'.'.$req->banner->getClientOriginalExtension();
                $req->banner->move('upload/banners', $imageName);
            }else{
                $imageName='';
            }
            HomeBanner::create([
                'title_one' => $req['title_one'],
                    'title_two' => $req['title_two'],
                    'banner' => $imageName,
                    'title_three' => $req['title_three'],
                    'button_link' => $req['button_link'],
                    'banner_type' => $req['banner_type'],
                    'status' => $req['status'],
            ]);
            return redirect('admin/addhomebanners')->with('success', 'Banner created successfully.');
    }
    public function bannerdelete($id=0)
    {
        HomeBanner::where('id',$id)->delete();
        return back()->with('success', 'Banner Deleted Successfully.');
    }

    public function bannerstatus($id=0)
    {
        $data=HomeBanner::where('id',$id)->first();
        if($data->status==1){
            HomeBanner::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            HomeBanner::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Banner status changed successfully.');
    }

    public function homeaboutus()
    {
        $data = array();

            $data=HomeAboutUs::first();
        
        return view('admin.homeaboutus')->with('edit',$data);
    }
    public function submitHomeAbout(Request $req)
    {
        $req->validate([
            'description'=>'required',
            ]);

            if(isset($req->id) and $req->id != ''){
                $video=HomeAboutUs::where('id',$req->id)->first();
                if($req->video){
                    if($video->video){
                        @unlink("upload\banners\/".$video->video);
                    }
                    $imageName = time().'.'.$req->video->getClientOriginalExtension();
                    $req->video->move('upload/banners', $imageName);
                }else{
                    $imageName=$video->video;
                }
                HomeAboutUs::where('id',$req->id)->update([
                    'video' => $imageName,
                    'description' => $req['description'],
                    'status' => $req['status'],
                ]);
                return redirect('admin/homeaboutus')->with('success', 'updated successfully.');
            }
            if($req->video){
                $imageName = time().'.'.$req->video->getClientOriginalExtension();
                $req->video->move('upload/banners', $imageName);
            }else{
                $imageName='';
            }
            HomeAboutUs::create([
                'video' => $imageName,
                    'description' => $req['description'],
                    'status' => $req['status'],
            ]);
            return redirect('admin/homeaboutus')->with('success', 'updated successfully.');
    }

    public function allbasicfeatures()
    {
        $features = BasicFeatures::get();
        return view('admin.allbasicfeatures')->with('features',$features);
    }
    public function addbasicfeatures($id=0)
    {
        $data = array();
        if($id>0){
            $data=BasicFeatures::where('id',$id)->first();
        }
        return view('admin.addbasicfeatures')->with('edit',$data);
    }
    public function submitBasicFeatures(Request $req)
    {
        $req->validate([
            'title'=>'required',
            'status'=>'required'
            ]);

            if(isset($req->id) and $req->id != ''){
               
                BasicFeatures::where('id',$req->id)->update([
                    'title' => $req['title'],
                    'status' => $req['status'],
                ]);
                return redirect('admin/allbasicfeatures')->with('success', 'updated successfully.');
            }
           
            BasicFeatures::create([
                'title' => $req['title'],
                'status' => $req['status'],
            ]);
            return redirect('admin/addbasicfeatures')->with('success', 'Added successfully.');
    }
    public function basicfeaturedelete($id=0)
    {
        BasicFeatures::where('id',$id)->delete();
        return back()->with('success', 'Deleted Successfully.');
    }

    public function basicfeaturestatus($id=0)
    {
        $data=BasicFeatures::where('id',$id)->first();
        if($data->status==1){
            BasicFeatures::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            BasicFeatures::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Status changed successfully.');
    }

    public function commonvideo()
    {
        $edit=CommonVideo::first();
        return view('admin.commonvideo')->with('edit',$edit);
    }

    public function submitCommonVideo(Request $req)
    {
        if($req->id==''){
            $req->validate([
                'video'=>'required'
            ]);
        }
       
        
        
        if($req->id){
            $edit=CommonVideo::where('id',$req->id)->first();
            if($edit->video){
                @unlink("upload\video\/".$edit->video);
            }
            if($req->video){
                $videoName = time().'.'.$req->video->getClientOriginalExtension();
                $req->video->move('upload/video', $videoName);
            }else{
                $videoName=$edit->video;
            }
            CommonVideo::where('id',$req->id)->update([
                'video'=>$videoName,
                'description'=>$req->description,
                'status'=>$req->status
            ]);
        }else{
            if($req->video){
                $videoName = time().'.'.$req->video->getClientOriginalExtension();
                $req->video->move('upload/video', $videoName);
            }else{
                $videoName='';
            }
            CommonVideo::create([
                'video'=>$videoName,
                'description'=>$req->description,
                'status'=>$req->status
            ]);
        }

        return back()->with('success', 'Added successfully.');
    }
}
