<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ChildrenPartyCategory;
use App\Models\MstVenueFacility;
use App\Models\ChildrenPartyFacility;
use App\Models\ChildrenPartyImages;
use App\Models\KidsDetail;
use App\Models\KidsImages;
use App\Models\KidsWhatsIncluded;
use App\Models\KidsBlogs;
use App\Models\ChildrenParty as ChildrenPartyModel;

class ChildrenParty extends Controller
{
    public function addCategory($id=0)
    {
        $data = array();
        if($id>0){
            $data=ChildrenPartyCategory::where('id',$id)->first();
        }
        return view('admin.addchildrenpartycategory')->with('edit',$data);
    }

    public function submitAddCategory(Request $req)
    {        
        $req->validate([
            'name' => 'required',
            'layout' => 'required',
            'description' => 'required',
            'image' => (isset($req->id) and $req->id > 0) ? '' : 'required',
            'banner' => (isset($req->id) and $req->id > 0) ? '' : 'required',
        ]);
        
        if(isset($req->id) and $req->id != ''){
            $venue=ChildrenPartyCategory::where('id',$req->id)->first();
            
            if($req->image){
                
                if($venue->image){
                    @unlink("upload\images\/".$venue->image);
                }
                
                $imageName = time().'.'.$req->image->getClientOriginalExtension();
                $req->image->move('upload/images', $imageName);
            }else{
                $imageName=$venue->image;
            }

            if($req->banner){
                if($venue->banner){
                    @unlink("upload\images\/".$venue->banner);
                }
                $bannerName = time().'.'.$req->banner->getClientOriginalExtension();
                $req->banner->move('upload/images', $bannerName);
            }else{
                $bannerName=$venue->banner;
            }
            
            $slug = str_slug($req['name'], "-");
            ChildrenPartyCategory::where('id',$req->id)->update([
                'name' => $req['name'],
                'layout' => $req['layout'],
                'slug' => $slug,
                'image' => $imageName,
                'banner' => $bannerName,
                'description' => $req['description'],
            ]);
            return redirect('admin/all-children-party-category')->with('success', 'Kid Party Section updated successfully.');
        }
        if($req->image){
            $imageName = time().'.'.$req->image->getClientOriginalExtension();
            $req->image->move('upload/images', $imageName);
        }else{
            $imageName='';
        }
        if($req->banner){
            $bannerName = time().'.'.$req->banner->getClientOriginalExtension();
            $req->banner->move('upload/images', $bannerName);
        }else{
            $bannerName='';
        }
        $venue=ChildrenPartyCategory::orderBy('id', 'DESC')->first();
        $slug = str_slug($req['name'], "-");
        ChildrenPartyCategory::create([
            'name' => $req['name'],
            'layout' => $req['layout'],
            'slug' => $slug,
            'image' => $imageName,
            'banner' => $bannerName,
            'description' => $req['description'],
            'status' => 1
        ]);
        return redirect('admin/add-children-party-category')->with('success', 'Kid Party Section created successfully.');
    }

    public function showAllChildCategory()
    {
        $venues = ChildrenPartyCategory::get();
        return view('admin.child_party_category_list')
        ->with('venues', $venues);
    }

    public function category_change_status($id=0)
    {
        $data=ChildrenPartyCategory::where('id',$id)->first();
        if($data->status==1){
            ChildrenPartyCategory::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            ChildrenPartyCategory::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Children party category status changed successfully.');
    }

    public function delete_category($id=0)
    {
        ChildrenPartyCategory::where('id',$id)->delete();
        return back()->with('success', 'Children party category Deleted Successfully.');
    }

    public function addParty($id=0)
    {
        $data = array();
        if($id>0){
            $data=ChildrenPartyModel::where('id',$id)->first();
        }

        $category = ChildrenPartyCategory::get();
        $facility = MstVenueFacility::where('status', 1)->get();
        $cp_images = ChildrenPartyImages::get();
        return view('admin.add_childrenparty')
        ->with('category',$category)
        ->with('facility',$facility)
        ->with('cp_images',$cp_images)
        ->with('edit',$data)
        ;
    }
    public function allParty()
    {
        $parties=ChildrenPartyModel::get();
        return view('admin.allparties')->with('parties',$parties);
    }
    public function partydelete($id=0)
    {
        ChildrenPartyModel::where('id',$id)->delete();
        return back()->with('success', 'Venue patry deleted successfully.');
    }
    public function deletegalleryimage(Request $req)
    {
        $img=ChildrenPartyImages::where('id',$req->id)->first();
        @unlink("upload\images\/".$img->image);
        ChildrenPartyImages::where('id',$req->id)->delete();
        echo 1;
        //return back()->with('success', 'Venue gallery image deleted successfully.');
    }
    public function partystatus($id=0)
    {
        $data=ChildrenPartyModel::where('id',$id)->first();
        if($data->status==1){
            ChildrenPartyModel::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            ChildrenPartyModel::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Venue patry status changed successfully.');
    }

    public function submitAddPartyVenue(Request $req)
    {        
        $req->validate([
            'category' => 'required',
            'title' => 'required',
            'venue_title' => 'required',
            'venue_address' => 'required',
            'postcode' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'timing_description' => 'required',
            'short_description' => 'required',
            //'video' => (isset($req->id) and $req->id > 0) ? '' : 'required',
            'box_title' => 'required',
            'description' => 'required',
            'opening_hours' => 'required',
            'feature_image' => (isset($req->id) and $req->id > 0) ? '' : 'required',
        ]);
        
        if(isset($req->id) and $req->id != ''){
            $venue=ChildrenPartyModel::where('id',$req->id)->first();
            if($req->feature_image){
                if($venue->feature_image){
                    @unlink("upload\images\/".$venue->feature_image);
                }
                $imageName = time().'.'.$req->feature_image->getClientOriginalExtension();
                $req->feature_image->move('upload/images', $imageName);
            }else{
                $imageName=$venue->feature_image;
            }
            if($req->video){
                if($venue->video){
                    @unlink("upload\video\/".$venue->video);
                }
                $video = time().'.'.$req->video->getClientOriginalExtension();
                $req->video->move('upload/video', $video);
            }else{
                $video=$venue->video;
            }
            $slug = str_slug($req['title'], "-").'-'.$req->id;
            ChildrenPartyModel::where('id',$req->id)->update([
                'category' => $req['category'],
                'title' => $req['title'],
                'box_title' => $req['box_title'],
                'slug' => $slug,
                'feature_image' => $imageName,
                'video' => $video,
                'venue_title' => $req['venue_title'],
                'venue_address' => $req['venue_address'],
                'short_description' => $req['short_description'],
                'timing_description' => $req['timing_description'],
                'opening_hours' => $req['opening_hours'],
                'description' => $req['description'],
                'postcode' => $req['postcode'],
                'latitude' => $req['latitude'],
                'longitude' => $req['longitude'],
            ]);
            if($req->hasfile('gallary'))
            {
               foreach($req->file('gallary') as $image)
               {
                   $name=$image->getClientOriginalName();
                   $image->move('upload/images', $name);
                   ChildrenPartyImages::create([
                       'cp_id'=>$req->id,
                       'image'=>$name,
                   ]);
               }
            }
            if($req->facility)
             {
                ChildrenPartyFacility::where('cp_id',$req->id)->delete();
                foreach($req->facility as $facility)
                {
                    
                    ChildrenPartyFacility::create([
                        'cp_id'=>$req->id,
                        'vf_id'=>$facility,
                    ]);
                }
             }
            return redirect('admin/all-children-party')->with('success', 'Children party venues updated successfully.');
        }
        if($req->feature_image){
            $imageName = time().'.'.$req->feature_image->getClientOriginalExtension();
            $req->feature_image->move('upload/images', $imageName);
        }else{
            $imageName='';
        }
        if($req->video){
            $video = time().'.'.$req->video->getClientOriginalExtension();
            $req->video->move('upload/images', $video);
        }else{
            $video='';
        }
        $venue=ChildrenPartyModel::orderBy('id', 'DESC')->first();
        $slug = str_slug($req['title'], "-").'-'.$venue->id;
        $cid=ChildrenPartyModel::create([
                'category' => $req['category'],
                'title' => $req['title'],
                'box_title' => $req['box_title'],
                'slug' => $slug,
                'feature_image' => $imageName,
                'video' => $video,
                'venue_title' => $req['venue_title'],
                'venue_address' => $req['venue_address'],
                'short_description' => $req['short_description'],
                'timing_description' => $req['timing_description'],
                'opening_hours' => $req['opening_hours'],
                'description' => $req['description'],
                'postcode' => $req['postcode'],
                'latitude' => $req['latitude'],
                'longitude' => $req['longitude'],
            ]);
            if($req->hasfile('gallary'))
             {
                foreach($req->file('gallary') as $image)
                {
                    $name=$image->getClientOriginalName();
                    $image->move('upload/images', $name);
                    ChildrenPartyImages::create([
                        'cp_id'=>$cid->id,
                        'image'=>$name,
                    ]);
                }
             }
             

             if($req->facility)
             {
                foreach($req->facility as $facility)
                {
                    
                    ChildrenPartyFacility::create([
                        'cp_id'=>$cid->id,
                        'vf_id'=>$facility,
                    ]);
                }
             }
        return redirect('admin/add-children-party')->with('success', 'Children party venues created successfully.');
    }
    
    public function addfacilities($id=0)
    {
        $data = array();
        if($id>0){
            $data=MstVenueFacility::where('id',$id)->first();
        }
        return view('admin.addfacilities')->with('edit',$data);
    }
    
    public function submitFacilities(Request $req)
    {        
        
        $req->validate([
            'facility' => 'required',
            'icon' => (isset($req->id) and $req->id > 0) ? '' : 'required',
        ]);
        
        if(isset($req->id) and $req->id != ''){
            $icon=MstVenueFacility::where('id',$req->id)->first();
            
            if($req->icon){
                
                if($icon->icon){
                    @unlink("upload\images\/".$icon->icon);
                }
                
                $imageName = time().'.'.$req->icon->getClientOriginalExtension();
                $req->icon->move('upload/images', $imageName);
            }else{
                $imageName=$icon->icon;
            }
            
            MstVenueFacility::where('id',$req->id)->update([
                'facility' => $req['facility'],
                'icon' => $imageName,
            ]);
            return redirect('admin/allfacilities')->with('success', 'Venue facilities updated successfully.');
        }
        if($req->icon){
            $imageName = time().'.'.$req->icon->getClientOriginalExtension();
            $req->icon->move('upload/images', $imageName);
        }else{
            $imageName='';
        }
        MstVenueFacility::create([
            'facility' => $req['facility'],
            'icon' => $imageName,
            'status' => 1
        ]);
        return back()->with('success', 'Venue facilities created successfully.');
    }
    public function allfacilities()
    {
        $facilities = MstVenueFacility::get();
        return view('admin.allfacilities')
        ->with('facilities', $facilities);
    }
    
    public function facilitiesdelete($id=0)
    {
        MstVenueFacility::where('id',$id)->delete();
        return back()->with('success', 'Venue facilities deleted successfully.');
    }
    public function facilitiestatus($id=0)
    {
        $data=MstVenueFacility::where('id',$id)->first();
        if($data->status==1){
            MstVenueFacility::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            MstVenueFacility::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Venue facilities status changed successfully.');
    }
    public function kids_party($id=0)
    {
        $data = array();
        if($id>0){
            $data=KidsDetail::where('id',$id)->first();
        }
        $allcorporatecatgory=ChildrenPartyCategory::where('status',1)->where('layout',1)->get();
        return view('admin.addkidsdetails')->with('edit',$data)->with('allcorporatecatgory',$allcorporatecatgory);
    }
    
    public function kids_party_details($id=0)
    {
        $data = array();
        if($id>0){
            $data=KidsDetail::where('id',$id)->first();
        }
        $allcorporatecatgory=ChildrenPartyCategory::where('status',1)->where('layout',2)->get();
        return view('admin.addkidspartydetails')->with('edit',$data)->with('allcorporatecatgory',$allcorporatecatgory);
    }
    

    public function submitKidsDeials(Request $req)
    {        
        $req->validate([
            'name' => 'required',
            'category' => 'required',
            'description' => 'required',
            'image' => (isset($req->id) and $req->id > 0) ? '' : 'required',
        ]);
        
        if(isset($req->id) and $req->id != ''){
            $venue=KidsDetail::where('id',$req->id)->first();
            
            if($req->image){
                
                if($venue->image){
                    @unlink("upload\images\/".$venue->image);
                }
                
                $imageName = time().'.'.$req->image->getClientOriginalExtension();
                $req->image->move('upload/images', $imageName);
            }else{
                $imageName=$venue->image;
            }
            
            KidsDetail::where('id',$req->id)->update([
                'title' => $req['name'],
                'image' => $imageName,
                'description' => $req['description'],
                'status' => $req['status'],
                'category_id' => $req['category'],
            ]);
            return redirect('admin/all-kids-party-details')->with('success', 'Kids party updated successfully.');
        }
        if($req->image){
            $imageName = time().'.'.$req->image->getClientOriginalExtension();
            $req->image->move('upload/images', $imageName);
        }else{
            $imageName='';
        }
        KidsDetail::create([
            'title' => $req['name'],
            'image' => $imageName,
            'description' => $req['description'],
            'status' => $req['status'],
            'category_id' => $req['category'],
        ]);
        return redirect('admin/add-kids-party-details')->with('success', 'Kids party created successfully.');
    }

    public function all_kids_party()
    {
        $kids = KidsDetail::get();
        return view('admin.allkidspartydetails')
        ->with('allcorporateblogs', $kids);
    }

    public function kidstatus($id=0)
    {
        $data=KidsDetail::where('id',$id)->first();
        if($data->status==1){
            KidsDetail::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            KidsDetail::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Kids party status changed successfully.');
    }

    public function kiddelete($id=0)
    {
        KidsDetail::where('id',$id)->delete();
        return back()->with('success', 'Kids party Deleted Successfully.');
    }
    public function choose_layout()
    {
        return view('admin.choose_layout');
    }
    public function submitKidsDetailLayoutTwo(Request $req)
    {
        $req->validate([

            'category' => 'required',
            'title' => 'required',
            'image' => (isset($req->id) and $req->id > 0) ? '' : 'required',
            'description' => 'required',
            'secound_title' => 'required',
            'secound_description' => 'required',

        ]);

        if($req->id){
            $data=KidsDetail::where('id',$req->id)->first();

            if($req->image){
                if($data->image){
                    @unlink("upload\images\/".$data->image);
                }
                $feature = time().'.'.$req->image->getClientOriginalExtension();
                $req->image->move('upload/images', $feature);
            }else{
                $feature=$data->image;
            }
            
            $slug = str_slug($req['title'], "-");
            KidsDetail::where('id',$req->id)->update([
                'title' => $req['title'],
                'image' => $feature,
                'description' => $req['description'],
                'category_id' => $req['category'],
                'secound_title' => $req['secound_title'],
                'secound_description' => $req['secound_description'],
                'status'=>1
            ]);

            if($req->hasfile('gallary'))
             {
                foreach($req->file('gallary') as $image)
                {
                    $name=$image->getClientOriginalName();
                    $image->move('upload/images', $name);
                    KidsImages::create([
                        'kid'=>$req->id,
                        'image'=>$name,
                    ]);
                }
             }

             if($req->has('videotitle'))
             {
                foreach($req->videotitle as $key => $title)
                {
                  if($title && $req->videos[$key]){
                    KidsWhatsIncluded::create([
                        'kid'=>$req->id,
                        'title'=>$title,
                        'description'=>$req->videos[$key],
                    ]);
                  }
                    
                }
             }
            return redirect('admin/all-kids-party-details')->with('success', 'Kid Party Details updated successfully.');

        }else{
            
            if($req->image){
                $feature = time().'.'.$req->image->getClientOriginalExtension();
                $req->image->move('upload/images', $feature);
            }else{
                $feature='';
            }
            
            $slug = str_slug($req['title'], "-");
            $mid=KidsDetail::create([
                'title' => $req['title'],
                'image' => $feature,
                'description' => $req['description'],
                'category_id' => $req['category'],
                'secound_title' => $req['secound_title'],
                'secound_description' => $req['secound_description'],
            ]);
            
             if($req->hasfile('gallary'))
             {
                foreach($req->file('gallary') as $image)
                {
                    $name=$image->getClientOriginalName();
                    $image->move('upload/images', $name);
                    KidsImages::create([
                        'kid'=>$mid->id,
                        'image'=>$name,
                    ]);
                }
             }

             if($req->has('videotitle'))
             {
                foreach($req->videotitle as $key => $title)
                {
                    if($title && $req->videos[$key]){
                        KidsWhatsIncluded::create([
                        'kid'=>$mid->id,
                        'title'=>$title,
                        'description'=>$req->videos[$key],
                    ]);
                    }
                }
             }
        }
        
        return back()->with('success', 'Kid Party Details Created successfully.');
    }
    public function deletekidsgalleryimage(Request $req)
    {
        KidsImages::where('id',$req->id)->delete();
        echo 1;
        //return back()->with('success', 'Kid Party Details gallery image deleted successfully.');
    }
    public function deletekidsvideo(Request $req)
    {
        KidsWhatsIncluded::where('id',$req->id)->delete();
        echo 1;
        //return back()->with('success', 'Kid Party Details Tabs deleted successfully.');
    }

    public function addkidsblog($id=0)
    {
        $data = array();
        if($id>0){
            $data=KidsBlogs::where('id',$id)->first();
        }
        $allcorporatecatgory = ChildrenPartyCategory::where('status',1)->where('layout',2)->get();
        return view('admin.addkidsblog')->with('edit',$data)->with('allcorporatecatgory',$allcorporatecatgory);
    }
    public function allkidsblog()
    {
        $allkidsblog = KidsBlogs::get();
        return view('admin.allkidsblog')
        ->with('allcorporateblogs', $allkidsblog);
    }

    public function submitKidsBlogs(Request $req)
    {
        $req->validate([

            'name' => 'required',
            'category' => 'required',
            'description' => 'required',
            'image' => (isset($req->id) and $req->id > 0) ? '' : 'required',

        ]);
        
        if(isset($req->id) and $req->id != ''){
            $data=KidsBlogs::where('id',$req->id)->first();
            if($req->image){
                if($data->image){
                    @unlink("upload\images\/".$data->image);
                }
                $imageName = time().'.'.$req->image->getClientOriginalExtension();
                $req->image->move('upload/images', $imageName);
            }else{
                $imageName=$data->image;
            }

            $slug = str_slug($req['name'], "-").'-'.$req->id;
            KidsBlogs::where('id',$req->id)->update([
                'title' => $req['name'],
                'category_id' => $req['category'],
                'slug' => $slug,
                'image' => $imageName,
                'description' => $req['description'],
            ]);
            return redirect('admin/allkidsblog')->with('success', 'Featured Block updated successfully.');
        }

        if($req->image){
            $imageName = time().'.'.$req->image->getClientOriginalExtension();
            $req->image->move('upload/images', $imageName);
        }else{
            $imageName='';
        }
        $data=KidsBlogs::orderBy('id','DESC')->first();
        if($data){
            $slug = str_slug($req['name'], "-").'-'.$data->id;
        }else{
            $slug = str_slug($req['name'], "-");
        }
        
        KidsBlogs::create([
            'title' => $req['name'],
            'category_id' => $req['category'],
            'slug' => $slug,
            'image' => $imageName,
            'description' => $req['description'],
        ]);
        return redirect('admin/allkidsblog')->with('success', 'Featured Block created successfully.');
    }
    public function deletekidsblog($id=0)
    {
        KidsBlogs::where('id',$id)->delete();
        return back()->with('success', 'Featured Block Deleted Successfully.');
    }

}
