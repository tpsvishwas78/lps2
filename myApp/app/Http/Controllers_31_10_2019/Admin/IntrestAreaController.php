<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MstAreaIntrest;

class IntrestAreaController extends Controller
{
    public function index()
    {
        $areas = MstAreaIntrest::get();
        return view('admin.allarea')
        ->with('areas', $areas);
    }
    public function addarea($id=0)
    {
        $data = array();
        if($id>0){
            $data=MstAreaIntrest::where('id',$id)->first();
        }
        return view('admin.addarea')->with('edit',$data);
    }
    public function submitArea(Request $req)
    {
        $req->validate([

            'name' => 'required',

        ]);
        if(isset($req->id) and $req->id != ''){
            MstAreaIntrest::where('id',$req->id)->update([
                'name' => $req['name'],
                'description' => $req['description'],
            ]);
            return redirect('admin/allarea')->with('success', 'Area updated successfully.');
        }
        MstAreaIntrest::create([
            'name' => $req['name'],
            'description' => $req['description'],
        ]);
        return back()->with('success', 'Area created successfully.');
    }

    public function areadelete($id=0)
    {
        MstAreaIntrest::where('id',$id)->delete();
        return back()->with('success', 'Area Deleted Successfully.');
    }

    public function areastatus($id=0)
    {
        $data=MstAreaIntrest::where('id',$id)->first();
        if($data->status==1){
            MstAreaIntrest::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            MstAreaIntrest::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Area status changed successfully.');
    }
}
