<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use App\Models\MstVenue;
use App\Models\VenueFollow;
Use Auth;

class VenueController extends Controller
{



    public function allvenue(Request $request)
    {
        $Venues =  MstVenue::select('id', 'location', 'image', 'name', 'postcode', 'address', 'description', 'latitude', 'longitude')->Where('status', 1)->get();
        return response()->json([
            'data' => $Venues,
            'status' => 'success'
        ]);
    }

    public function myvenues(){
        $Venues =  VenueFollow::with('myvenue')->Where('venue_followers.user_id', Auth::id())->get();
        return response()->json([
            'data' => $Venues,
            'status' => 'success'
        ]);
    }
}
