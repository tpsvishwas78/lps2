<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MstSport;
use App\Models\MstNationality;
use App\Models\MstAreaIntrest;
use App\Models\User;
use App\Models\userSport;
use App\Models\UserAreaIntrest;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    public function edit($id)
    {
        $data['sports']=MstSport::where('status',1)->get();
        $data['nationality']=MstNationality::where('status',1)->get();
        $data['areas']=MstAreaIntrest::where('status',1)->get();

        $user_intrest = UserAreaIntrest::select('area_intrest_id')->where('user_id', $id)->get();
        $userInt = array();
    
        foreach ($user_intrest as $key => $value) {
            $userInt[] = $value->area_intrest_id;
        }

        $user_sport = userSport::select('sport_id')->where('user_id', $id)->get();
        $userSport = array();
        foreach ($user_sport as $key => $value) {
            $userSport[] = $value->sport_id;
        }
        
        return view('editprofile')->with('data',$data)->with('user_intrest', $userInt)->with('user_sport', $userSport);
    }

    public function updateprofile(Request $req)
    {
        $userdata=Auth::user();
        $user=User::where('id',$userdata->id)->first();
        $req->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'username' => 'required',
            'email' => 'required',
            'phone_number' => 'required',
        ]);

        if($req->image){
            if($user->profile_img){
                unlink("upload\images\/".$user->profile_img);
            }
           
            $imageName = time().'.'.$req->image->getClientOriginalExtension();
            $req->image->move('upload/images', $imageName);
        }else{
            $imageName=$user->profile_img;
        }
        if($req['dob']){
            $dob = date('Y-m-d', strtotime($req['dob']));
        }else{
            $dob = NULL;
        }
        
        $mydata=User::where('id',$user->id)->update([
            'first_name' => $req['first_name'],
            'last_name' => $req['last_name'],
            'username' => $req['username'],
            'phone_number' => $req['phone_number'],
            'profile_img' => $imageName,
            'address' => $req['address'],
            'gender' => $req['gender'],
            'dob' => $dob,
            'nationality' => $req['nationality'],
        ]);

        $sports=$req['sports'];
        userSport::where('user_id',$user->id)->delete();
        if($sports){
            foreach ($sports as $key => $value) {
                userSport::create([
                    'sport_id' => $value,
                    'user_id' => $user->id,
                ]);
            }
        }
        

        $areas=$req['area'];
        UserAreaIntrest::where('user_id',$user->id)->delete();
        if($areas){
            foreach ($areas as $key => $value) {
                UserAreaIntrest::create([
                    'area_intrest_id' => $value,
                    'user_id' => $user->id,
                ]);
            }
        }
        

        return back()->with('success', 'Updated successfully.');
    }

    public function changepassword(Request $req)
    {
        $userdata=Auth::user();
        $user=User::where('id',$userdata->id)->first();
        $req->validate([
            'password' => 'required',
            'confirm_password' => 'required',
        ]);
    }

}
