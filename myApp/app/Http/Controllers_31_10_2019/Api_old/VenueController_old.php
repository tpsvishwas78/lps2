<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use App\Models\MstVenue;

class VenueController extends Controller
{



    public function allvenue(Request $request)
    {
        $Venues =  MstVenue::select('id', 'location', 'image', 'name', 'postcode', 'address', 'description', 'latitude', 'longitude')->Where('status', 1)->get();
        return response()->json([
            'data' => $Venues,
            'status' => 'success'
        ]);
    }
}
