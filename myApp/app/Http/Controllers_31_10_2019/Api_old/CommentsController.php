<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\GameComment;
Use Auth;
use App\User;

class CommentsController extends Controller
{
    public function usercomments(Request $req)
    {
       
        $validator = Validator::make($req->all(), [
            'name'=>'required',
            'email'=>'required',
            'comment'=>'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'error' => $validator->errors()], 200);
        }
        
        GameComment::create([
            'game_id'=>$req->game_id,
            'name'=>$req->name,
            'email'=>$req->email,
            'comment'=>$req->comment,
            'user_id' => Auth::id(),
        ]);
        $comment=GameComment::where('game_id',$req->game_id)->orderBy('id','DESC')->first();
        $user=User::where('id',$comment->user_id)->first();
        $comment['profile_img']=$user->profile_img;

        return response()->json([
            'status' => 'success',
            'comment' => $comment
            ]);
        
    }
}
