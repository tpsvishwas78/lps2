<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MstGame;

class GamesController extends Controller {

    public function allgames(Request $request) {

        $games = MstGame::select('id', 'user_id', 'title', 'featured_img', 'description', 'game_date', 'game_start_time', 'game_finish_time', 'team_limit', 'payment', 'price', 'is_refund', 'currency', 'refund_days','venue_id')
                ->with('venue')
                ->where('status', '1')
                ->get()->toArray();
        return response()->json([
                    'data' => $games,
                    'status' => 'success'
        ]);
    }

}
