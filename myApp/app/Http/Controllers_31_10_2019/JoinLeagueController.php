<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\HomeBanner;
use App\Models\SiteSetting;
use App\Models\MstSport;
use App\Models\MstGame;
use App\Models\JoinLeague;
use App\Models\JoinLeagueBlock;

class JoinLeagueController extends Controller
{
    public function index(Request $req)
    {
        if($req->search && $req->date=='' && $req->sport==''){

            $search=$req->search;

            $games=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
            ->where('mst_games.status',1)
            ->where('mst_games.game_date','>=',date('Y-m-d'))
            ->where('mst_venue.location','LIKE', '%'.$search.'%')
            ->orWhere('mst_venue.postcode','LIKE', '%'.$search.'%')
            ->orWhere('mst_games.title','LIKE', '%'.$search.'%')
            ->limit(10)
            ->get();

            $gamescount=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
            ->where('mst_games.status',1)
            ->where('mst_games.game_date','>=',date('Y-m-d'))
            ->where('mst_venue.location','LIKE', '%'.$search.'%')
            ->orWhere('mst_venue.postcode','LIKE', '%'.$search.'%')
            ->orWhere('mst_games.title','LIKE', '%'.$search.'%')
            ->count();
        }elseif($req->search==''){
            $games=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
            ->where('mst_games.status',1)
            ->where('mst_games.game_date','>=',date('Y-m-d'))
            ->where('mst_games.game_date','LIKE', '%'.$req->date.'%')
            ->where('mst_games.sport_id','LIKE', '%'.$req->sport.'%')
            ->limit(10)
            ->get();
            $gamescount=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
            ->where('mst_games.status',1)
            ->where('mst_games.game_date','>=',date('Y-m-d'))
            ->where('mst_games.game_date','LIKE', '%'.$req->date.'%')
            ->where('mst_games.sport_id','LIKE', '%'.$req->sport.'%')
            ->count();

        }elseif( $req->search && $req->date && $req->sport ){

            $search=$req->search;

            $games=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
            ->where('mst_games.status',1)
            ->where('mst_games.game_date','>=',date('Y-m-d'))
            ->where('mst_games.game_date','LIKE', '%'.$req->date.'%')
            ->where('mst_games.sport_id','LIKE', '%'.$req->sport.'%')
            ->where('mst_venue.location','LIKE', '%'.$search.'%')
            ->orWhere('mst_venue.postcode','LIKE', '%'.$search.'%')
            ->orWhere('mst_games.title','LIKE', '%'.$search.'%')
            ->limit(10)
            ->get();
            $gamescount=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
            ->where('mst_games.status',1)
            ->where('mst_games.game_date','>=',date('Y-m-d'))
            ->where('mst_games.game_date','LIKE', '%'.$req->date.'%')
            ->where('mst_games.sport_id','LIKE', '%'.$req->sport.'%')
            ->where('mst_venue.location','LIKE', '%'.$search.'%')
            ->orWhere('mst_venue.postcode','LIKE', '%'.$search.'%')
            ->orWhere('mst_games.title','LIKE', '%'.$search.'%')
            ->count();
        }else{
            
        $games=\DB::table('mst_games')
        ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
        ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
        ->where('mst_games.status',1)
        ->where('mst_games.game_date','>=',date('Y-m-d'))
        ->limit(10)
        ->get();
        $gamescount=\DB::table('mst_games')
        ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
        ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
        ->where('mst_games.status',1)
        ->where('mst_games.game_date','>=',date('Y-m-d'))
        ->count();
        }

        $banners=HomeBanner::where('status',1)->get();
        $setting=SiteSetting::first();
        
        $sports=MstSport::where('status',1)->get();
        $joinleague=JoinLeague::orderBy('id','DESC')->first();
        $allblocks=JoinLeagueBlock::get();
        return view('joinleague')->with('games',$games)->with('sports',$sports)->with('banners',$banners)->with('setting',$setting)->with('gamescount',$gamescount)->with('joinleague',$joinleague)->with('allblocks',$allblocks);
    }

    public function joinleagueloadmore(Request $req)
    {
        $id = $req->id;
        if($req->search && $req->date=='' && $req->sport==''){

            $search=$req->search;

            $games=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
            ->where('mst_games.id','>',$id)
            ->where('mst_games.status',1)
            ->where('mst_games.game_date','>=',date('Y-m-d'))
            ->where('mst_venue.location','LIKE', '%'.$search.'%')
            ->orWhere('mst_venue.postcode','LIKE', '%'.$search.'%')
            ->orWhere('mst_games.title','LIKE', '%'.$search.'%')
            ->limit(10)
            ->get();
        }elseif($req->search==''){
            $games=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
            ->where('mst_games.id','>',$id)
            ->where('mst_games.status',1)
            ->where('mst_games.game_date','>=',date('Y-m-d'))
            ->where('mst_games.game_date','LIKE', '%'.$req->date.'%')
            ->where('mst_games.sport_id','LIKE', '%'.$req->sport.'%')
            ->limit(10)
            ->get();

        }elseif( $req->search && $req->date && $req->sport ){

            $search=$req->search;

            $games=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
            ->where('mst_games.id','>',$id)
            ->where('mst_games.status',1)
            ->where('mst_games.game_date','>=',date('Y-m-d'))
            ->where('mst_games.game_date','LIKE', '%'.$req->date.'%')
            ->where('mst_games.sport_id','LIKE', '%'.$req->sport.'%')
            ->where('mst_venue.location','LIKE', '%'.$search.'%')
            ->orWhere('mst_venue.postcode','LIKE', '%'.$search.'%')
            ->orWhere('mst_games.title','LIKE', '%'.$search.'%')
            ->limit(10)
            ->get();
        }else{
            
        $games=\DB::table('mst_games')
        ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
        ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
        ->where('mst_games.id','>',$id)
        ->where('mst_games.status',1)
        ->where('mst_games.game_date','>=',date('Y-m-d'))
        ->limit(10)
        ->get();
        }

        if(!$games->isEmpty()){
            $html= view('loadmoregames', compact('games'))->render();
        }else{
            $html='';
        }
        echo $html;
    }
}
