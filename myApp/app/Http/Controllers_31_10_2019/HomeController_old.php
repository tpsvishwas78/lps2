<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MstSport;
use App\Models\MstNationality;
use App\Models\MstAreaIntrest;
use App\Models\User;
use App\Models\UserSport;
use App\Models\UserAreaIntrest;
use App\Models\UserLocation;
Use Auth;
use Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index()
    {
        dd('a');
        // $userdata=Auth::user();
        // $user=User::where('id',$userdata->id)->first();

        // $nationality=MstNationality::where('id',$user->nationality)->first();
        // $areas = MstAreaIntrest::leftJoin('user_area_intrest', function($join) {
        //     $join->on('mst_area_intrest.id', '=', 'user_area_intrest.area_intrest_id');
        //   })
        //   ->where('user_area_intrest.user_id',$user->id)
        //   ->get();
          
        //return view('home')->with('nationality',$nationality)->with('areas',$areas);
    }
    public function edit()
    {
        $data['sports']=MstSport::where('status',1)->get();
        $data['nationality']=MstNationality::where('status',1)->get();
        $data['areas']=MstAreaIntrest::where('status',1)->get();
        $location=UserLocation::where('user_id',Auth::user()->id)->first();

        $user_intrest = UserAreaIntrest::select('area_intrest_id')->where('user_id', Auth::user()->id)->get();
        $userInt = array();
    
        foreach ($user_intrest as $key => $value) {
            $userInt[] = $value->area_intrest_id;
        }

        $user_sport = UserSport::select('sport_id')->where('user_id', Auth::user()->id)->get();
        $userSport = array();
        foreach ($user_sport as $key => $value) {
            $userSport[] = $value->sport_id;
        }
        
        return view('editprofile')->with('data',$data)->with('user_intrest', $userInt)->with('user_sport', $userSport)->with('location', $location);
    }

    public function updateprofile(Request $req)
    {
        
        $userdata=Auth::user();
        $user=User::where('id',$userdata->id)->first();
        //dd($req->all());
        $req->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'username' => 'required',
            'email' => 'required',
            'phone_number' => 'required',
        ]);
        
        if($req->image){
            if($user->profile_img){
                @unlink("upload\images\/".$user->profile_img);
            }
           
            $imageName = time().'.'.$req->image->getClientOriginalExtension();
            $req->image->move('upload/images', $imageName);
            
        }else{
            $imageName=$user->profile_img;
        }
        if($req['dob']){
            $dob = date('Y-m-d', strtotime($req['dob']));
        }else{
            $dob = NULL;
        }
        
        
        $mydata=User::where('id',$user->id)->update([
            'first_name' => $req['first_name'],
            'last_name' => $req['last_name'],
            'username' => $req['username'],
            'phone_number' => $req['phone_number'],
            'profile_img' => $imageName,
            'address' => $req['address'],
            'gender' => $req['gender'],
            'dob' => $dob,
            'nationality' => $req['nationality'],
        ]);

        $sports=$req['sports'];
        
        if($sports){
            UserSport::where('user_id',$user->id)->delete();
            foreach ($sports as $key => $value) {
                UserSport::create([
                    'sport_id' => $value,
                    'user_id' => $user->id,
                ]);
            }
        }
        

        $areas=$req['area'];
        
        if($areas){
            UserAreaIntrest::where('user_id',$user->id)->delete();
            foreach ($areas as $key => $value) {
                UserAreaIntrest::create([
                    'area_intrest_id' => $value,
                    'user_id' => $user->id,
                ]);
            }
        }
        

        return back()->with('success', 'Updated successfully.');
    }

    public function changepassword(Request $req)
    {
        
        $user_id=Auth::user()->id;
        $req->validate([
            'password' => ['required', 'string', 'min:8','confirmed'],
        ]);

        
        $obj_user = User::find($user_id);
        $obj_user->password = Hash::make($req['password']);
        $obj_user->save();

        return redirect()->back()->with('success', 'Password Changed Successfully!');
        
    }

    public function setnotify(Request $req)
    {

        $user_id=Auth::user()->id;

        if($req['nf_comment']){
            $nf_comment=1;
        }else{
            $nf_comment=0;
        }

        if($req['nf_game']){
            $nf_game=1;
        }else{
            $nf_game=0;
        }

        if($req['nf_attendance']){
            $nf_attendance=1;
        }else{
            $nf_attendance=0;
        }

        if($req['nf_newsletter']){
            $nf_newsletter=1;
        }else{
            $nf_newsletter=0;
        }

        $mydata=User::where('id',$user_id)->update([
            'nf_comment' => $nf_comment,
            'nf_game' => $nf_game,
            'nf_attendance' => $nf_attendance,
            'nf_newsletter' => $nf_newsletter,
        ]);
        return redirect()->back()->with('success', 'Notification Setting Updated Successfully!');
    }

    public function addlocation(Request $req)
    {
        $user_id=Auth::user()->id;
        $req->validate([
            'location' => ['required'],
            'address' => ['required'],
        ]);

        $location=UserLocation::where('user_id',$user_id)->first();
        if($location){
            UserLocation::where('user_id',$user_id)->update([
                'location' => $req['location'],
                'address' => $req['address'],
                'latitude' => $req['latitude'],
                'longitude' => $req['longitude'],
            ]);
        }else{
            UserLocation::create([
                'location' => $req['location'],
                'address' => $req['address'],
                'latitude' => $req['latitude'],
                'longitude' => $req['longitude'],
                'user_id' => $user_id,
            ]);
        }

        return redirect()->back()->with('success', 'Successfully added your location!');

        
    }

   
}