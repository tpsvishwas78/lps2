<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\YourBooking;
use App\Models\AdvertiseSession;
use App\Models\MstSport;
use Auth;

class AdvertiseYourSessionController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth' => 'verified']);
    }
    public function index()
    {
        return view('advertise-your-session');
    }

    public function yourbooking()
    {
        return view('yourbooking');
    }
    public function yoursession()
    {
        $sports=MstSport::where('status',1)->get();
        return view('yoursession')->with('sports',$sports);
    }

    public function submitBooking(Request $req)
    {
        $userid=Auth::user()->id;
        $req->validate([
            'first_name'=>'required|alpha',
            'last_name'=>'required|alpha',
            'email'=>'required|email',
            'mobile_number'=>'required|digits:10',
            'venue' => 'required',
            'date' => 'required',
            'recurring_booking' => 'required',
            'message' => 'required',
            'image' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ]);

            $date = date('Y-m-d', strtotime($req['date']));

            if($req->image){
                $imageName = time().'.'.$req->image->getClientOriginalExtension();
                $req->image->move('upload/images', $imageName);
            }else{
                $imageName='';
            }

        YourBooking::create([
            'first_name'=>$req->first_name,
            'last_name'=>$req->last_name,
            'email'=>$req->email,
            'user_id'=>$userid,
            'phone_number'=>$req->mobile_number,
            'venue_id' => $req->venue_id,
            'recurring_booking' => $req->recurring_booking,
            'date' => $date,
            'message' => $req->message,
            'image' => $imageName,
        ]);

        return back()->with('success', 'Successfully send your booking request.');
    }

    public function submitSession(Request $req)
    {
        $userid=Auth::user()->id;
            $req->validate([
            'first_name'=>'required|alpha',
            'last_name'=>'required|alpha',
            'email'=>'required|email',
            'mobile_number'=>'required|digits:10',
            'venue' => 'required',
            'date' => 'required',
            'recurring_booking' => 'required',
            'sport' => 'required',
            'time' => 'required',
            'no_of_player' => 'required|numeric',
            'message' => 'required',
            'image' => 'required',
            'signature' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ]);

            $date = date('Y-m-d', strtotime($req['date']));

            if($req->image){
                $imageName = time().'.'.$req->image->getClientOriginalExtension();
                $req->image->move('upload/images', $imageName);
            }else{
                $imageName='';
            }

            $data_uri = $req->signature;
        $encoded_image = explode(",", $data_uri)[1];
        $decoded_image = base64_decode($encoded_image);
        $signature=time().'.png';
        \File::put('upload/signature/'.$signature, $decoded_image);

            AdvertiseSession::create([
            'first_name'=>$req->first_name,
            'last_name'=>$req->last_name,
            'email'=>$req->email,
            'user_id'=>$userid,
            'phone_number'=>$req->mobile_number,
            'venue_id' => $req->venue_id,
            'is_recurring_booking' => $req->recurring_booking,
            'sport_id' => $req->sport,
            'time' => $req->time,
            'no_of_player' => $req->no_of_player,
            'date' => $date,
            'message' => $req->message,
            'image' => $imageName,
            'signature' => $signature,
        ]);

        return back()->with('success', 'Successfully send your session request.');
    }
}
