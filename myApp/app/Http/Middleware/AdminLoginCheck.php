<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class AdminLoginCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd(Auth::user()->user_role);
        if(Auth::guard('admin')->check() and Auth::guard('admin')->user()->user_role == 1)
        {
            return redirect('admin/dashboard');
        }elseif(Auth::guard('admin')->check()){
            return redirect('admin/login');
        }
        return $next($request);
    }
}
