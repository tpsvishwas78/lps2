<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MstSport;
use App\Models\MstAreaIntrest;
use App\Models\QuickLinksSlider;
use App\Models\BlockBooking;
use App\Models\GetInvolved;
use App\Models\CorporateCategory;
use App\Models\CorporateDetail;
use App\Models\CorporateBlog;
use App\Models\CorporateFaq;
use Notification;
use App\Notifications\NewBlockBookingMail;
use App\User;

class CorporateController extends Controller
{
    public function team_building()
    {
        return view('team_building');
    }
    public function blockbooking()
    {
        $sports=MstSport::where('status',1)->get();
        $areas=MstAreaIntrest::where('status',1)->get();
        return view('blockbooking')->with('sports',$sports)->with('areas',$areas);
    }
    public function submitblockboking(Request $req)
    {
        $req->validate([
            'first_name'=>'required|alpha',
            'last_name'=>'required|alpha',
            'email'=>'required|email',
            'mobile_number'=>'required|digits:11',
            'venue_name' => 'required',
            'date' => 'required',
            'time' => 'required',
            'sport' => 'required',
            'comments' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ]);

        $date = date('Y-m-d', strtotime($req['date']));

        BlockBooking::create([
            'first_name'=>$req->first_name,
            'last_name'=>$req->last_name,
            'email'=>$req->email,
            'phone_number'=>$req->mobile_number,
            'venue' => $req->venue_name,
            'time' => $req->time,
            'ideal_days' => $date,
            'sport_id' => $req->sport,
            'comments' => $req->comments,
        ]);

        $admin = User::where('email', 'info@letsplaysports.co.uk')->first();
        
        //$admin = User::where('email', 'tpsvishwas78@gmail.com')->first();
        $sport=MstSport::where('id',$req->sport)->first();
        
        // $html='<p>First Name : '.$req->first_name.'</p>';
        // $html.='<p>Last Name : '.$req->last_name.'</p>';
        // $html.='<p>Email : '.$req->email.'</p>';
        // $html.='<p>Phone Numbe : '.$req->mobile_number.'</p>';
        // $html.='<p>Venue : '.$req->venue_name.'</p>';
        // $html.='<p>Sport : '.$req->sport.'</p>';
        // $html.='<p>Ideal Booking Date : '.$date.'</p>';
        // $html.='<p>Time : '.$req->time.'</p>';
        // $html.='<p>Comments : '.$req->comments.'</p>';
        $detailsJoiner = [
            'greeting' => 'Hello Admin', 
            'body' => ' New block booking by '. $req->first_name.' '.$req->last_name.' just submitted ',
            'first_name'=>'First Name : '. $req->first_name,
            'last_name'=>'Last Name : '. $req->last_name,
            'email'=> 'Email : '.$req->email,
            'mobile_number'=>'Phone Numbe : '. $req->mobile_number, 
            'venue_name'=>'Venue : '. $req->venue_name,
            'sport'=>'Sport : '. $sport->name,
            'date'=>'Ideal Booking Date : '. $date, 
            'time'=>'Time : '. $req->time,
            'comments'=>'Comments : '.$req->comments ,
            'thanks' => 'Thank you for using ' . url(''),
            'actionText' => 'Go To Archive',
            'actionURL' => url('/admin/manageblockbooking'),
            'message' => ' New Block Booking! ', 
        ];
        Notification::send($admin, new NewBlockBookingMail($detailsJoiner));
        
        return back()->with('success', 'THANK YOU FOR YOUR MESSAGE! WE WILL GET BACK TO YOU AS SOON AS POSSIBLE!');
    }
    public function submitgetinvolved(Request $req)
    {
        
        $req->validate([
            'first_name'=>'required|alpha',
            'last_name'=>'required|alpha',
            'email'=>'required|email',
            'mobile_number'=>'required|digits:10',
            'venue' => 'required',
            'date' => 'required',
            'time' => 'required',
            'sport' => 'required',
            'comments' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ]);

        $date = date('Y-m-d', strtotime($req['date']));

        GetInvolved::create([
            'first_name'=>$req->first_name,
            'last_name'=>$req->last_name,
            'email'=>$req->email,
            'phone_number'=>$req->mobile_number,
            'venue' => $req->venue,
            'time' => $req->time,
            'available_days' => $date,
            'sport_id' => $req->sport,
            'comments' => $req->comments,
        ]);

        return back()->with('success', 'Successfully send your details.');
    }
    public function one_day_tournament()
    {
        $sliders=QuickLinksSlider::where('status',1)->where('title','NOT LIKE', '%corporate%')->get();
        return view('one_day_tournament')->with('sliders',$sliders);
    }
    public function corporate_detail($slug='')
    {
        
        $cate=CorporateCategory::where('slug',$slug)->first();
       
        $detail=CorporateDetail::where('category_id',$cate->id)->first();
        if(@$detail->layout==1){
            $blogs=CorporateBlog::where('category_id',$cate->id)->get();
            return view('corporate_detail')->with('detail',$detail)->with('blogs',$blogs);
        }else{
            $faqs=CorporateFaq::where('cp_id',$detail->id)->get();
            return view('corporate_detail_layout_two')->with('detail',$detail)->with('faqs',$faqs);
        }
        
    }
    public function corporate()
    {
        $categories=CorporateCategory::where('status',1)->get();
        return view('corporate')->with('categories',$categories);
    }
    public function get_involved()
    {
        $sports=MstSport::where('status',1)->get();
        $areas=MstAreaIntrest::where('status',1)->get();
        return view('get_involved')->with('sports',$sports)->with('areas',$areas);
    }

    public function corporate_sports_days()
    {
        return view('corporate_sports_days');
    }
    public function inflatable_fun_days()
    {
        return view('inflatable_fun_days');
    }
    public function its_a_knockout_fun_days()
    {
        return view('its_a_knockout_fun_days');
    }
    public function office_summer_party()
    {
        return view('office_summer_party');
    }
    public function office_christmas_party()
    {
        return view('office_christmas_party');
    }
    public function sports_tournaments()
    {
        return view('sports_tournaments');
    }

    
    
}
