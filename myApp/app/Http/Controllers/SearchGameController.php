<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\HomeBanner;
use App\Models\SiteSetting;
use App\Models\MstSport;
use App\Models\MstGame;

class SearchGameController extends Controller
{
    
    public function index(Request $req, $slug='')
    {
        
        $bannerShow = false;        
            $gamesquery=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id');

             if (($req->has('near_latitude') && $req->near_latitude != 'null' && $req->near_latitude) && ($req->has('near_longitude') && $req->near_longitude != 'null' && $req->near_longitude)) {
                
                $gamesquery->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus','mst_venue.latitude as latitude','mst_venue.longitude as longitude',
                \DB::raw("get_distance('".$req->near_latitude.",".$req->near_longitude."', mst_venue.latitude,mst_venue.longitude  ) as distance") );

            }else{
                $gamesquery->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus','mst_venue.latitude as latitude','mst_venue.longitude as longitude'
            );
            }
            
            if ($req->sport != 'null' && $req->has('sport') && $req->sport or $slug != '') {
                if($slug == ''){
                    $findvalues = MstSport::where('main_id',$req->sport)->get();
                }else{
                    $slugid = MstSport::where('slug',$slug)->first(); 
                    $findvalues = MstSport::where('main_id',$slugid->id)->get();                  
                    $bannerShow = $slugid->id;
                }
                
                $mysports=array();
                if(!$findvalues->isEmpty()){
                    foreach ($findvalues as $key => $findvalue) {
                        $mysports[]=$findvalue->id;
                    } 
                    $mysports[]=$findvalue->main_id; 
                }
                else{ 
                    $slugid = MstSport::where('slug',$slug)->first(); 
                    //$mysports[]=$req->sport;
                    $mysports[]=$slugid->id;
                }
                $gamesquery->whereIn('sport_id', $mysports);
                
            }
            
            if ($req->has('gender') && $req->gender != 'null' && $req->gender) {

                if ($req->input('gender') == '2') {
                    $gamesquery->whereIn('gender', array(2, 1));
                } elseif ($req->input('gender') == '3') {
                    $gamesquery->whereIn('gender', array(3, 1));
                } else {
                    $gamesquery->where('gender', $req->input('gender'));
    
                }
    
            }
            if ($req->has('date') && $req->date != 'null' && $req->date) {
                    $gamesquery->where('repeat_games.game_start_date','LIKE', '%'.$req->date.'%');
            }
            
            $gamesquery->where(function ($query) use ($req) {
                    if($req->search && ($req->near_latitude=='' && $req->near_longitude == '' )){
                        
                        $query->where('mst_venue.location','LIKE', '%'.$req->search.'%')
                     ->orWhere('mst_venue.postcode','LIKE', '%'.$req->search.'%')
                     ->orWhere('mst_games.title','LIKE', '%'.$req->search.'%');
                    }                
            });
            $gamesquery->where('mst_games.status', '=', 1);
            $gamesquery->where('repeat_games.game_start_date', '>=', date('Y-m-d'));

            if (($req->has('near_latitude') && $req->near_latitude != 'null' && $req->near_latitude) && ($req->has('near_longitude') && $req->near_longitude != 'null' && $req->near_longitude)) {

            $gamesquery->where(\DB::raw("get_distance('".$req->near_latitude.",".$req->near_longitude."', mst_venue.latitude,mst_venue.longitude  )"), '<=', 8);
               
            }
             
            $gamesquery->orderBy('repeat_games.game_start_date','ASC');
            $games = $gamesquery->paginate(10);
            

            $gamescountquery=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id');

            if (($req->has('near_latitude') && $req->near_latitude != 'null' && $req->near_latitude) && ($req->has('near_longitude') && $req->near_longitude != 'null' && $req->near_longitude)) {
                
                $gamescountquery->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus','mst_venue.latitude as latitude','mst_venue.longitude as longitude',
                \DB::raw("get_distance('".$req->near_latitude.",".$req->near_longitude."', mst_venue.latitude,mst_venue.longitude  ) as distance") );

              }else{
                $gamescountquery->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus','mst_venue.latitude as latitude','mst_venue.longitude as longitude'
            );
            }

            // if ($req->sport != 'null' && $req->has('sport') && $req->sport) {
            //     $findvalues = MstSport::where('main_id',$req->sport)->get();
            //     $mysports=array();
            //     if(!$findvalues->isEmpty()){
            //         foreach ($findvalues as $key => $findvalue) {
            //             $mysports[]=$findvalue->id;
            //         }
            //         $mysports[]=$findvalue->main_id;
            //     }
            //     else{
            //         $mysports[]=$req->sport;
            //     }
            //     $gamescountquery->whereIn('sport_id', $mysports);
                
            // }
            if ($req->sport != 'null' && $req->has('sport') && $req->sport or $slug != '') {
                if($slug == ''){
                    $findvalues = MstSport::where('main_id',$req->sport)->get();
                }else{
                    $slugid = MstSport::where('slug',$slug)->first(); 
                    $findvalues = MstSport::where('main_id',$slugid->id)->get();                  
                    $bannerShow = $slugid->id;
                }
                
                $mysports=array();
                if(!$findvalues->isEmpty()){
                    foreach ($findvalues as $key => $findvalue) {
                        $mysports[]=$findvalue->id;
                    } 
                    $mysports[]=$findvalue->main_id; 
                }
                else{ 
                    $slugid = MstSport::where('slug',$slug)->first(); 
                    //$mysports[]=$req->sport;
                    $mysports[]=$slugid->id;
                }
                $gamesquery->whereIn('sport_id', $mysports);
                
            }
            if ($req->has('gender') && $req->gender != 'null' && $req->gender) {

                if ($req->input('gender') == '2') {
                    $gamesquery->whereIn('gender', array(2, 1));
                } elseif ($req->input('gender') == '3') {
                    $gamesquery->whereIn('gender', array(3, 1));
                } else {
                    $gamesquery->where('gender', $req->input('gender'));
    
                }
    
            }
            if ($req->has('date') && $req->date != 'null' && $req->date) {
                    $gamescountquery->where('repeat_games.game_start_date','LIKE', '%'.$req->date.'%');
            }
            
            $gamescountquery->where(function ($query) use ($req) {
                if($req->search && ($req->near_latitude=='' && $req->near_longitude == '' )){
                        $query->where('mst_venue.location','LIKE', '%'.$req->search.'%')
                     ->orWhere('mst_venue.postcode','LIKE', '%'.$req->search.'%')
                     ->orWhere('mst_games.title','LIKE', '%'.$req->search.'%');
                    }                
            });
            $gamescountquery->where('mst_games.status', '=', 1);
            $gamescountquery->where('repeat_games.game_start_date', '>=', date('Y-m-d'));
            if (($req->has('near_latitude') && $req->near_latitude != 'null' && $req->near_latitude) && ($req->has('near_longitude') && $req->near_longitude != 'null' && $req->near_longitude)) {

                $gamescountquery->where(\DB::raw("get_distance('".$req->near_latitude.",".$req->near_longitude."', mst_venue.latitude,mst_venue.longitude  )"), '<=', 8);
                   
                }
            $gamescountquery->orderBy('repeat_games.game_start_date','ASC');
            $gamescount = $gamescountquery->count();
            

            $allgamesquery=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id');

            if (($req->has('near_latitude') && $req->near_latitude != 'null' && $req->near_latitude) && ($req->has('near_longitude') && $req->near_longitude != 'null' && $req->near_longitude)) {
                
                $allgamesquery->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus','mst_venue.latitude as latitude','mst_venue.longitude as longitude',
                \DB::raw("get_distance('".$req->near_latitude.",".$req->near_longitude."', mst_venue.latitude,mst_venue.longitude  ) as distance") );

               
              }else{
                $allgamesquery->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus','mst_venue.latitude as latitude','mst_venue.longitude as longitude'
            );
            }

            // if ($req->sport != 'null' && $req->has('sport') && $req->sport) {
            //     $findvalues = MstSport::where('main_id',$req->sport)->get();
            //     $mysports=array();
            //     if(!$findvalues->isEmpty()){
            //         foreach ($findvalues as $key => $findvalue) {
            //             $mysports[]=$findvalue->id;
            //         }
            //         $mysports[]=$findvalue->main_id;
            //     }
            //     else{
            //         $mysports[]=$req->sport;
            //     }
            //     $allgamesquery->whereIn('sport_id', $mysports);
                
            // }
            if ($req->sport != 'null' && $req->has('sport') && $req->sport or $slug != '') {
                if($slug == ''){
                    $findvalues = MstSport::where('main_id',$req->sport)->get();
                }else{
                    $slugid = MstSport::where('slug',$slug)->first(); 
                    $findvalues = MstSport::where('main_id',$slugid->id)->get();                  
                    $bannerShow = $slugid->id;
                }
                
                $mysports=array();
                if(!$findvalues->isEmpty()){
                    foreach ($findvalues as $key => $findvalue) {
                        $mysports[]=$findvalue->id;
                    } 
                    $mysports[]=$findvalue->main_id; 
                }
                else{ 
                    $slugid = MstSport::where('slug',$slug)->first(); 
                    //$mysports[]=$req->sport;
                    $mysports[]=$slugid->id;
                }
                $gamesquery->whereIn('sport_id', $mysports);
                
            }
            if ($req->has('gender') && $req->gender != 'null' && $req->gender) {

                if ($req->input('gender') == '2') {
                    $gamesquery->whereIn('gender', array(2, 1));
                } elseif ($req->input('gender') == '3') {
                    $gamesquery->whereIn('gender', array(3, 1));
                } else {
                    $gamesquery->where('gender', $req->input('gender'));
    
                }
    
            }
            if ($req->has('date') && $req->date != 'null' && $req->date) {
                    $allgamesquery->where('repeat_games.game_start_date','LIKE', '%'.$req->date.'%');
            }
            
            $allgamesquery->where(function ($query) use ($req) {
                if($req->search && ($req->near_latitude=='' && $req->near_longitude == '' )){
                        $query->where('mst_venue.location','LIKE', '%'.$req->search.'%')
                     ->orWhere('mst_venue.postcode','LIKE', '%'.$req->search.'%')
                     ->orWhere('mst_games.title','LIKE', '%'.$req->search.'%');
                    }                
            });
            $allgamesquery->where('mst_games.status', '=', 1);
            $allgamesquery->where('repeat_games.game_start_date', '>=', date('Y-m-d'));
            if (($req->has('near_latitude') && $req->near_latitude != 'null' && $req->near_latitude) && ($req->has('near_longitude') && $req->near_longitude != 'null' && $req->near_longitude)) {

                $allgamesquery->where(\DB::raw("get_distance('".$req->near_latitude.",".$req->near_longitude."', mst_venue.latitude,mst_venue.longitude  )"), '<=', 8);
                   
                }
            $allgamesquery->orderBy('repeat_games.game_start_date','ASC');
            $allgames = $allgamesquery->get();
        

        $banners=HomeBanner::where('status',1)->get();
        $setting=SiteSetting::first();
        
        $sports=MstSport::where('status',1)->orderBy('sport_order','ASC')->get();
        if($slug!=''){
            $selectedsport = MstSport::where('slug',$slug)->first();
        }else{
            $selectedsport=array();
        }
        

        return view('searchgame')->with('games',$games)
        ->with('sports',$sports)
        ->with('banners',$banners)
        ->with('bannerShow',$bannerShow)
        ->with('selectedsport',$selectedsport)
        ->with('setting',$setting)->with('gamescount',$gamescount)->with('allgames',$allgames);
    }
















































    public function searchgameloadmore(Request $req)
    {
        $gamesquery=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id');

             if (($req->has('near_latitude') && $req->near_latitude != 'null' && $req->near_latitude) && ($req->has('near_longitude') && $req->near_longitude != 'null' && $req->near_longitude)) {
                
                $gamesquery->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus','mst_venue.latitude as latitude','mst_venue.longitude as longitude',
                \DB::raw("get_distance('".$req->near_latitude.",".$req->near_longitude."', mst_venue.latitude,mst_venue.longitude  ) as distance") );

               
              }else{
                $gamesquery->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus','mst_venue.latitude as latitude','mst_venue.longitude as longitude'
            );
            } 
            
            if ($req->sport != 'null' && $req->has('sport') && $req->sport) {
                $findvalues = MstSport::where('main_id',$req->sport)->get();
                $mysports=array();
                if(!$findvalues->isEmpty()){
                    foreach ($findvalues as $key => $findvalue) {
                        $mysports[]=$findvalue->id;
                    }
                    $mysports[]=$findvalue->main_id;
                }
                else{
                    $mysports[]=$req->sport;
                }
                $gamesquery->whereIn('sport_id', $mysports);
                
            }
            
            if ($req->has('gender') && $req->gender != 'null' && $req->gender) {

                if ($req->input('gender') == '2') {
                    $gamesquery->whereIn('gender', array(2, 1));
                } elseif ($req->input('gender') == '3') {
                    $gamesquery->whereIn('gender', array(3, 1));
                } else {
                    $gamesquery->where('gender', $req->input('gender'));
    
                }
    
            }
            if ($req->has('date') && $req->date != 'null' && $req->date) {
                    $gamesquery->where('repeat_games.game_start_date','LIKE', '%'.$req->date.'%');
            }
            
            $gamesquery->where(function ($query) use ($req) {
                if($req->search && ($req->near_latitude=='' && $req->near_longitude == '' )){
                        $query->where('mst_venue.location','LIKE', '%'.$req->search.'%')
                     ->orWhere('mst_venue.postcode','LIKE', '%'.$req->search.'%')
                     ->orWhere('mst_games.title','LIKE', '%'.$req->search.'%');
                    }                
            });
            $gamesquery->where('mst_games.status', '=', 1);
            $gamesquery->where('repeat_games.game_start_date', '>=', date('Y-m-d'));

            if (($req->has('near_latitude') && $req->near_latitude != 'null' && $req->near_latitude) && ($req->has('near_longitude') && $req->near_longitude != 'null' && $req->near_longitude)) {

            $gamesquery->where(\DB::raw("get_distance('".$req->near_latitude.",".$req->near_longitude."', mst_venue.latitude,mst_venue.longitude  )"), '<=', 8);
               
            }
             
            $gamesquery->orderBy('repeat_games.game_start_date','ASC');
            $games = $gamesquery->paginate(10);

        if(!$games->isEmpty()){
            $html= view('loadmoregames', compact('games'))->render();
        }else{
            $html='';
        }
        echo $html;
    }

    //women games

    public function woomengame(Request $req)
    {
        $gamesquery=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id');

            if (($req->has('near_latitude') && $req->near_latitude != 'null' && $req->near_latitude) && ($req->has('near_longitude') && $req->near_longitude != 'null' && $req->near_longitude)) {
                
                $gamesquery->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus','mst_venue.latitude as latitude','mst_venue.longitude as longitude',
                \DB::raw("get_distance('".$req->near_latitude.",".$req->near_longitude."', mst_venue.latitude,mst_venue.longitude  ) as distance") );

               
              }else{
                $gamesquery->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus','mst_venue.latitude as latitude','mst_venue.longitude as longitude'
            );
            } 

            if ($req->sport != 'null' && $req->has('sport') && $req->sport) {
                $findvalues = MstSport::where('main_id',$req->sport)->get();
                $mysports=array();
                if(!$findvalues->isEmpty()){
                    foreach ($findvalues as $key => $findvalue) {
                        $mysports[]=$findvalue->id;
                    }
                    $mysports[]=$findvalue->main_id;
                }
                else{
                    $mysports[]=$req->sport;
                }
                $gamesquery->whereIn('sport_id', $mysports);
                
            }
            
            
            if ($req->has('date') && $req->date != 'null' && $req->date) {
                    $gamesquery->where('repeat_games.game_start_date','LIKE', '%'.$req->date.'%');
            }
            
            $gamesquery->where(function ($query) use ($req) {
                if($req->search && ($req->near_latitude=='' && $req->near_longitude == '' )){
                        $query->where('mst_venue.location','LIKE', '%'.$req->search.'%')
                     ->orWhere('mst_venue.postcode','LIKE', '%'.$req->search.'%')
                     ->orWhere('mst_games.title','LIKE', '%'.$req->search.'%');
                    }                
            });
            $gamesquery->where('mst_games.status', '=', 1);
            $gamesquery->where('mst_games.gender', '=', 3);
            $gamesquery->where('repeat_games.game_start_date', '>=', date('Y-m-d'));
            if (($req->has('near_latitude') && $req->near_latitude != 'null' && $req->near_latitude) && ($req->has('near_longitude') && $req->near_longitude != 'null' && $req->near_longitude)) {

                $gamesquery->where(\DB::raw("get_distance('".$req->near_latitude.",".$req->near_longitude."', mst_venue.latitude,mst_venue.longitude  )"), '<=', 8);
                   
                }
            $gamesquery->orderBy('repeat_games.game_start_date','ASC');
            $games = $gamesquery->paginate(10);

            $gamescountquery=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id');

            if (($req->has('near_latitude') && $req->near_latitude != 'null' && $req->near_latitude) && ($req->has('near_longitude') && $req->near_longitude != 'null' && $req->near_longitude)) {
                
                $gamescountquery->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus','mst_venue.latitude as latitude','mst_venue.longitude as longitude',
                \DB::raw("get_distance('".$req->near_latitude.",".$req->near_longitude."', mst_venue.latitude,mst_venue.longitude  ) as distance") );

               
              }else{
                $gamescountquery->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus','mst_venue.latitude as latitude','mst_venue.longitude as longitude'
            );
            } 
            if ($req->sport != 'null' && $req->has('sport') && $req->sport) {
                $findvalues = MstSport::where('main_id',$req->sport)->get();
                $mysports=array();
                if(!$findvalues->isEmpty()){
                    foreach ($findvalues as $key => $findvalue) {
                        $mysports[]=$findvalue->id;
                    }
                    $mysports[]=$findvalue->main_id;
                }
                else{
                    $mysports[]=$req->sport;
                }
                $gamescountquery->whereIn('sport_id', $mysports);
                
            }
           
            if ($req->has('date') && $req->date != 'null' && $req->date) {
                    $gamescountquery->where('repeat_games.game_start_date','LIKE', '%'.$req->date.'%');
            }
            
            $gamescountquery->where(function ($query) use ($req) {
                if($req->search && ($req->near_latitude=='' && $req->near_longitude == '' )){
                        $query->where('mst_venue.location','LIKE', '%'.$req->search.'%')
                     ->orWhere('mst_venue.postcode','LIKE', '%'.$req->search.'%')
                     ->orWhere('mst_games.title','LIKE', '%'.$req->search.'%');
                    }                
            });
            $gamescountquery->where('mst_games.status', '=', 1);
            $gamescountquery->where('mst_games.gender', '=', 3);
            $gamescountquery->where('repeat_games.game_start_date', '>=', date('Y-m-d'));
            if (($req->has('near_latitude') && $req->near_latitude != 'null' && $req->near_latitude) && ($req->has('near_longitude') && $req->near_longitude != 'null' && $req->near_longitude)) {

                $gamescountquery->where(\DB::raw("get_distance('".$req->near_latitude.",".$req->near_longitude."', mst_venue.latitude,mst_venue.longitude  )"), '<=', 8);
                   
                }
            $gamescountquery->orderBy('repeat_games.game_start_date','ASC');
            $gamescount = $gamescountquery->count();
            

            $allgamesquery=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id');

            if (($req->has('near_latitude') && $req->near_latitude != 'null' && $req->near_latitude) && ($req->has('near_longitude') && $req->near_longitude != 'null' && $req->near_longitude)) {
                
                $allgamesquery->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus','mst_venue.latitude as latitude','mst_venue.longitude as longitude',
                \DB::raw("get_distance('".$req->near_latitude.",".$req->near_longitude."', mst_venue.latitude,mst_venue.longitude  ) as distance") );

               
              }else{
                $allgamesquery->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus','mst_venue.latitude as latitude','mst_venue.longitude as longitude'
            );
            } 
            if ($req->sport != 'null' && $req->has('sport') && $req->sport) {
                $findvalues = MstSport::where('main_id',$req->sport)->get();
                $mysports=array();
                if(!$findvalues->isEmpty()){
                    foreach ($findvalues as $key => $findvalue) {
                        $mysports[]=$findvalue->id;
                    }
                    $mysports[]=$findvalue->main_id;
                }
                else{
                    $mysports[]=$req->sport;
                }
                $allgamesquery->whereIn('sport_id', $mysports);
                
            }
           
            if ($req->has('date') && $req->date != 'null' && $req->date) {
                    $allgamesquery->where('repeat_games.game_start_date','LIKE', '%'.$req->date.'%');
            }
            
            $allgamesquery->where(function ($query) use ($req) {
                if($req->search && ($req->near_latitude=='' && $req->near_longitude == '' )){
                        $query->where('mst_venue.location','LIKE', '%'.$req->search.'%')
                     ->orWhere('mst_venue.postcode','LIKE', '%'.$req->search.'%')
                     ->orWhere('mst_games.title','LIKE', '%'.$req->search.'%');
                    }                
            });
            $allgamesquery->where('mst_games.status', '=', 1);
            $allgamesquery->where('mst_games.gender', '=', 3);
            $allgamesquery->where('repeat_games.game_start_date', '>=', date('Y-m-d'));
            if (($req->has('near_latitude') && $req->near_latitude != 'null' && $req->near_latitude) && ($req->has('near_longitude') && $req->near_longitude != 'null' && $req->near_longitude)) {

                $allgamesquery->where(\DB::raw("get_distance('".$req->near_latitude.",".$req->near_longitude."', mst_venue.latitude,mst_venue.longitude  )"), '<=', 8);
                   
                }
            $allgamesquery->orderBy('repeat_games.game_start_date','ASC');
            $allgames = $allgamesquery->get();

        $banners=HomeBanner::where('status',1)->get();
        $setting=SiteSetting::first();
        
        $sports=MstSport::where('status',1)->orderBy('sport_order','ASC')->get();

        return view('woomengame')->with('games',$games)->with('sports',$sports)->with('banners',$banners)->with('setting',$setting)->with('gamescount',$gamescount)->with('allgames',$allgames);
    }
    public function woomenloadmore(Request $req)
    { 
        $gamesquery=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id');
            if (($req->has('near_latitude') && $req->near_latitude != 'null' && $req->near_latitude) && ($req->has('near_longitude') && $req->near_longitude != 'null' && $req->near_longitude)) {
                
                $gamesquery->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus','mst_venue.latitude as latitude','mst_venue.longitude as longitude',
                \DB::raw("get_distance('".$req->near_latitude.",".$req->near_longitude."', mst_venue.latitude,mst_venue.longitude  ) as distance") );
               
              }else{
                $gamesquery->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus','mst_venue.latitude as latitude','mst_venue.longitude as longitude'
            );
            } 

            if ($req->sport != 'null' && $req->has('sport') && $req->sport) {
                $findvalues = MstSport::where('main_id',$req->sport)->get();
                $mysports=array();
                if(!$findvalues->isEmpty()){
                    foreach ($findvalues as $key => $findvalue) {
                        $mysports[]=$findvalue->id;
                    }
                    $mysports[]=$findvalue->main_id;
                }
                else{
                    $mysports[]=$req->sport;
                }
                $gamesquery->whereIn('sport_id', $mysports);
                
            }
            
            
            if ($req->has('date') && $req->date != 'null' && $req->date) {
                    $gamesquery->where('repeat_games.game_start_date','LIKE', '%'.$req->date.'%');
            }
            
            $gamesquery->where(function ($query) use ($req) {
                if($req->search && ($req->near_latitude=='' && $req->near_longitude == '' )){
                        $query->where('mst_venue.location','LIKE', '%'.$req->search.'%')
                     ->orWhere('mst_venue.postcode','LIKE', '%'.$req->search.'%')
                     ->orWhere('mst_games.title','LIKE', '%'.$req->search.'%');
                    }                
            });
            $gamesquery->where('mst_games.status', '=', 1);
            $gamesquery->where('mst_games.gender', '=', 3);
            $gamesquery->where('repeat_games.game_start_date', '>=', date('Y-m-d'));
            if (($req->has('near_latitude') && $req->near_latitude != 'null' && $req->near_latitude) && ($req->has('near_longitude') && $req->near_longitude != 'null' && $req->near_longitude)) {

                $gamesquery->where(\DB::raw("get_distance('".$req->near_latitude.",".$req->near_longitude."', mst_venue.latitude,mst_venue.longitude  )"), '<=', 8);
                   
                }
            $gamesquery->orderBy('repeat_games.game_start_date','ASC');
            $games = $gamesquery->paginate(10);
         
        
        
        if(!$games->isEmpty()){
            $html= view('loadmoregames', compact('games'))->render();
        }else{
            $html='';
        }
        echo $html;
    }
}
