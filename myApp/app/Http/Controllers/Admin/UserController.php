<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;

class UserController extends Controller
{
    public function index()
    {
        $users = User::where('user_role','!=', 1)->get();
        return view('admin.users')
        ->with('users', $users);
    }
    public function userstatus($id=0)
    {
        $data=User::where('id',$id)->first();
        if($data->status==1){
            User::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            User::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'User status changed successfully.');
    }
}
