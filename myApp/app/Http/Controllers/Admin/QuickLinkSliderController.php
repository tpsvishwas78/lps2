<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\QuickLinksSlider;

class QuickLinkSliderController extends Controller
{
    
    public function index()
    {
        $sliders = QuickLinksSlider::get();
        return view('admin.allsliders')->with('sliders',$sliders);
    }
    public function addslider($id=0)
    {
        $data = array();
        if($id>0){
            $data=QuickLinksSlider::where('id',$id)->first();
        }
        return view('admin.addslider')->with('edit',$data);
    }
    public function submitHomeSlider(Request $req)
    {
        $req->validate([
            'title'=>'required',
            'description'=>'required',
            'link'=>'required',
            'status'=>'required'
            ]);

            if(isset($req->id) and $req->id != ''){
                $slider=QuickLinksSlider::where('id',$req->id)->first();
                if($req->image){
                    if($slider->image){
                        @unlink("upload\sliders\/".$slider->image);
                    }
                    $imageName = time().'.'.$req->image->getClientOriginalExtension();
                    $req->image->move('upload/sliders', $imageName);
                }else{
                    $imageName=$slider->image;
                }
                QuickLinksSlider::where('id',$req->id)->update([
                    'title' => $req['title'],
                    'description' => $req['description'],
                    'image' => $imageName,
                    'link' => $req['link'],
                    'status' => $req['status'],
                ]);
                return redirect('admin/allsliders')->with('success', 'Slider updated successfully.');
            }
            if($req->image){
                $imageName = time().'.'.$req->image->getClientOriginalExtension();
                $req->image->move('upload/sliders', $imageName);
            }else{
                $imageName='';
            }
            QuickLinksSlider::create([
                'title' => $req['title'],
                'description' => $req['description'],
                'image' => $imageName,
                'link' => $req['link'],
                'status' => $req['status'],
            ]);
            return redirect('admin/addslider')->with('success', 'Slider created successfully.');
    }
    public function sliderdelete($id=0)
    {
        QuickLinksSlider::where('id',$id)->delete();
        return back()->with('success', 'Slider Deleted Successfully.');
    }
    public function sliderstatus($id=0)
    {
        $data=QuickLinksSlider::where('id',$id)->first();
        if($data->status==1){
            QuickLinksSlider::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            QuickLinksSlider::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Slider status changed successfully.');
    }
        
    
}
