<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\YourBooking;
use App\Models\AdvertiseSession;
use App\Models\BlockBooking;
use App\Models\GetInvolved;

class AdvertiseYourSessionController extends Controller
{
    public function listbooking()
    {
        $bookings = YourBooking::get();
       return view('admin.listbooking')->with('bookings',$bookings);
    }
    public function listsession()
    {
        $sessions = AdvertiseSession::get();
       return view('admin.listsession')->with('sessions',$sessions);
    }
    public function bookingdetail($id=0)
    {
        $booking = YourBooking::where('id',$id)->first();
       return view('admin.bookingdetail')->with('booking',$booking);
    }
    public function sessiondetail($id=0)
    {
        $session = AdvertiseSession::where('id',$id)->first();
       return view('admin.sessiondetail')->with('session',$session);
    }
    public function manageblockbooking()
    {
        $blockbookings = BlockBooking::get();        
       return view('admin.blockbookings')->with('blockbookings',$blockbookings);
    }
    public function blockbookingdetail($id=0)
    {
        $booking = BlockBooking::where('id',$id)->first();
       return view('admin.blockbookingdetail')->with('booking',$booking);
    }

    public function managegetinvolved()
    {
        $getinvolved = GetInvolved::get();
       return view('admin.getinvolved')->with('getinvolved',$getinvolved);
    }
    public function getinvolveddetail($id=0)
    {
        $getinvolved = GetInvolved::where('id',$id)->first();
       return view('admin.getinvolveddetail')->with('booking',$getinvolved);
    }

    
    
}
