<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index(){
        return view('admin.login');
    }

    public function loginSubmit(Request $req)
    {
        if (Auth::guard('admin')->attempt(['email' => $req->email, 'password' => $req->password, 'status' => 1, 'user_role' => 1])) {
            return redirect()->intended('admin/dashboard');
        }
        return back()->withErrors(['Login username or password not match']);
    }
    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('admin/login');
    }
}
