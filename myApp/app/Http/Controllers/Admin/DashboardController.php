<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
Use Auth;
use Hash;

class DashboardController extends Controller
{
    public function index()
    {
        return view('admin.dashboard');
    }
    public function profile()
    {
        $user=Auth::guard('admin')->user();
        $data=User::where('id',$user->id)->first();
        
        return view('admin.profile')->with('userdata',$data);
    }

    public function updateprofile(Request $req)
    {
        
        $user=Auth::guard('admin')->user();
        $data=User::where('id',$user->id)->first();

        $req->validate([

            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'username' => 'required',

        ]);
        
        if($req->image){
            unlink("upload\images\/".$data->profile_img);
            $imageName = time().'.'.$req->image->getClientOriginalExtension();
            $req->image->move('upload/images', $imageName);
        }else{
            $imageName=$data->profile_img;
        }

        User::where('id',$user->id)->update([
            'first_name' => $req['first_name'],
            'last_name' => $req['last_name'],
            'email' => $req['email'],
            'username' => $req['username'],
            'profile_img' => $imageName,
        ]);

        return back()->with('success', 'Updated successfully.');
        
    }
    public function changepassword(Request $req)
    {
       
            $request_data = $req->All();
            
            $validator = $this->admin_credential_rules($request_data);
            if($validator->fails())
            {
            return redirect()->back()->with('errors', $validator->getMessageBag());
            }
            else
            {  
            $current_password = Auth::guard('admin')->User()->password;        

            if(Hash::check($request_data['old_password'], $current_password))
            {           
                $user_id = Auth::guard('admin')->User()->id;                       
                $obj_user = User::find($user_id);
                $obj_user->password = Hash::make($request_data['new_password']);
                $obj_user->save(); 
                // $req->session()->flash('alert-success', 'Password Changed Successfully!');
                // return redirect('admin/profile');
                return redirect()->back()->with('success', 'Password Changed Successfully!');
            }
            else
            {           
                // $req->session()->flash('current-password', 'Please enter correct current password');
                // return redirect()->back();
                return redirect()->back()->with('warning', 'Please enter correct current password!');
            }
            } 
        
        return view('admin.changepassword');
    }
    public function admin_credential_rules(array $data)
    {
        $messages = [
            'old_password.required' => 'Please enter your current password',
            'new_password.required' => 'Please enter new password',
            'confirm_password.required' => 'Please enter confirm password',
            'confirm_password.same' => 'New and confirm password does not match',
        ];

        $validator = \Validator::make($data, [
            'old_password' => 'required',
            'new_password' => 'required|same:new_password',
            'confirm_password' => 'required|same:new_password',     
        ], $messages);

        return $validator;
    } 
}
