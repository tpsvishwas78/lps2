<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Menus;
use App\Models\Pages;

class MenuAndPagesController extends Controller
{
    public function allmenu()
    {
        $menus = Menus::get();
        return view('admin.allmenu')
        ->with('menus', $menus);
    }
    public function addmenu($id=0)
    {
        
        $data = array();
        if($id>0){
            $data=Menus::where('id',$id)->first();
        }
        return view('admin.addmenu')->with('edit',$data);
    }
    public function submitMenu(Request $req)
    {
        $req->validate([

            'name' => 'required',

        ]);
        
        if(isset($req->id) and $req->id != ''){
            
            $slug = str_slug($req['name'], "-");
            Menus::where('id',$req->id)->update([
                'name' => $req['name'],
                'slug' => $slug,
                'status' => $req['status'],
            ]);
            return redirect('admin/allmenu')->with('success', 'Menu updated successfully.');
        }
        
        $slug = str_slug($req['name'], "-");
        Menus::create([
            'name' => $req['name'],
            'slug' => $slug,
            'status' => $req['status'],
        ]);
        return redirect('admin/allmenu')->with('success', 'Add created successfully.');
    }

    public function menudelete($id=0)
    {
        Menus::where('id',$id)->delete();
        return back()->with('success', 'Menu Deleted Successfully.');
    }

    public function menustatus($id=0)
    {
        $data=Menus::where('id',$id)->first();
        if($data->status==1){
            Menus::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            Menus::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Menu status changed successfully.');
    }
    
    public function allpage()
    {
        $pages = Pages::get();
        return view('admin.allpage')
        ->with('pages', $pages);
    }
    public function addpage($id=0)
    {
        
        $data = array();
        if($id>0){
            $data=Pages::where('id',$id)->first();
        }
        $menus = Menus::get();
        return view('admin.addpage')->with('edit',$data)->with('menus',$menus);
    }
    public function submitPage(Request $req)
    {
        
        $req->validate([

            'name' => 'required',
            'description' => 'required',
            'menu' => 'required',

        ]);
        
        if(isset($req->id) and $req->id != ''){
            
            Pages::where('id',$req->id)->update([
                'name' => $req['name'],
                'description' => $req['description'],
                'menu_id' => $req['menu'],
            ]);
            return redirect('admin/allpage')->with('success', 'Page updated successfully.');
        }
        
        Pages::create([
            'name' => $req['name'],
            'description' => $req['description'],
            'menu_id' => $req['menu'],
        ]);
        return redirect('admin/allpage')->with('success', 'Page created successfully.');
    }

    public function pagedelete($id=0)
    {
        Pages::where('id',$id)->delete();
        return back()->with('success', 'Page Deleted Successfully.');
    }
    
}
