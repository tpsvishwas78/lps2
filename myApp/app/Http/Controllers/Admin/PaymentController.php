<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
Use Auth;
use Hash;

class PaymentController extends Controller
{
    function index(Request $req){

        $sport = $req->sports ? $req->sports : null;
        $gender = $req->gender ? $req->gender : null;
        $date = $req->date ? date('Y-m-d', strtotime($req->date)) : null;

        $res = \DB::table('mst_games')
        ->select(
            'mst_games.*', 
            'mst_venue.location',
            'mst_venue.address',
            'repeat_games.game_start_date',
            'game_payment.game_author_id',
            'game_payment.order_code',
            'game_payment.paid_amount',
            'game_payment.game_amount',
            'game_payment.vat_amount',
            'game_payment.commission_amount',
            'game_payment.payment_status',
            'game_payment.payment_type',
            'game_payment.customer_order_code',
            'mst_sports.name as sport_name',
            \DB::raw('concat(owner.first_name," ",owner.last_name) as author_name'),
            \DB::raw('concat(buyer.first_name," ",buyer.last_name) as buyer_name')
        )
        ->join('mst_sports', 'mst_sports.id', '=', 'mst_games.sport_id')
        ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
        ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
        ->join('game_payment', 'game_payment.game_id', '=', 'repeat_games.id')
        ->join('users AS owner', 'game_payment.game_author_id', '=', 'owner.id')
        ->join('users AS buyer', 'game_payment.user_id', '=', 'buyer.id')
        ->when($sport, function ($query) use ($sport) {
            return $query->where('sport_id', $sport);
        })
        ->when($gender, function ($query) use ($gender) {
            return $query->where('mst_games.gender', $gender);
        })
        ->when($date, function ($query) use ($date) {
            return $query->where('game_start_date', $date);
        })
        // ->when($phone, function ($query) use ($phone) {
        //     return $query->where('phone', 'LIKE', '%'.$phone . '%');
        // })
        // ->when($dob, function ($query) use ($dob) {
        //     return $query->where('dob', $dob);
        // })        
        ->get();
        //->toArray();
        //->toSql();
        $sports = \DB::table('mst_sports')->get();
        //dd($res);
        return view('admin.payments')
        ->with('games', $res)
        ->with('sports', $sports)
        ;
    }
}