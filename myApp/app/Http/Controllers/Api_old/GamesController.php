<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\GamePayment;
use App\Models\GameSpots;
use App\Models\MstGame;
use App\Notifications\GameNotify;
use App\User;
use Auth;
use function GuzzleHttp\json_decode;
use Illuminate\Http\Request;
use Notification;

class GamesController extends Controller
{

    public function allgames(Request $request)
    {

        $games = MstGame::select('id', 'user_id', 'title', 'featured_img', 'description', 'game_date', 'game_start_time', 'game_finish_time', 'team_limit', 'payment', 'price', 'is_refund', 'currency', 'refund_days', 'venue_id')
            ->with('venue')
            ->with('comment')
            ->with('teams')
            ->with('slider')
            ->where('status', '1')
            ->get()->toArray();
        return response()->json([
            'data' => $games,
            'status' => 'success',
        ]);
    }

    public function myGames(Request $request)
    {
        $games = GameSpots::with('usergames')->Where('team_spots.user_id', Auth::id())->get();
        return response()->json([
            'data' => $games,
            'status' => 'success',
        ]);
    }

    public function joingame(Request $request)
    {
        $key = config('worldpay.sandbox.client');

        header('Content-Type: application/json');
        $ch = curl_init('https://api.worldpay.com/v1/tokens');
        $array = [
            'reusable' => true,
            'paymentMethod' => [
                'name' => $request->name,
                'expiryMonth' => $request->expmonth,
                'expiryYear' => $request->expyear,
                'cardNumber' => $request->cardName,
                'type' => 'Card',
                'cvc' => $request->cvc,
            ],

            'clientKey' => $key,
        ];
        $post = json_encode($array);
        // print_r($post);die;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = json_decode(curl_exec($ch));
        curl_close($ch);
        if (!isset($result->token) && $result->token != '') {
            return response()->json([
                'data' => 'Something went wrong try again',
                'status' => 'success',
            ]);
        }
        $userid = Auth::user()->id;
        $token = $result->token;
        $total = $request->totalprice * 100;
        $key = config('worldpay.sandbox.service');
        $worldPay = new \Alvee\WorldPay\lib\Worldpay($key);

        $billing_address = array(
            'address1' => '',
            'address2' => '',
            'address3' => '',
            'postalCode' => '',
            'city' => '',
            'state' => '',
            'countryCode' => 'GB',
        );
        // $billing_address = array(
        //     'address1'    => '15 Chipping Vale',
        //     'address2'    => 'Emerson Valley',
        //     'address3'    => 'Milton Keynes',
        //     'postalCode'  => 'MK4 2JW',
        //     'city'        => 'Milton Keynes',
        //     'state'       => 'UK',
        //     'countryCode' => 'GB',
        // );

        try {

            $response = $worldPay->createOrder(array(
                'token' => $token,
                //'amount'            => (int)($total . "00"),
                'amount' => (int) ($total),
                'currencyCode' => 'GBP',
                'name' => $request->cardName,
                'billingAddress' => $billing_address,
                'orderDescription' => 'Joined Game',
                'customerOrderCode' => time(),
            ));

            if ($response['paymentStatus'] === 'SUCCESS') {
                $worldpayOrderCode = $response['orderCode'];
                $paidamout = $response['amount'] / 100;
                $commission = $request->game_amount * 10 / 100;
                // echo "<pre>";
                // print_r($response);
                // dd($request->all());

                GamePayment::create([
                    'game_id' => $request->game_id,
                    'user_id' => $userid,
                    'game_author_id' => $request->game_author_id,
                    'order_code' => $response['orderCode'],
                    'paid_amount' => $paidamout,
                    'game_amount' => $request->game_amount,
                    'vat_amount' => $request->vat_amount,
                    'commission_amount' => $commission,
                    'currency' => $response['currencyCode'],
                    'payment_status' => $response['paymentStatus'],
                    'payment_type' => $response['paymentResponse']['cardType'],
                    'customer_order_code' => $response['customerOrderCode'],
                ]);

                $already = GameSpots::select('game_id', 'team_spots.id', 'users.name', 'profile_img', 'team', 'user_id')->join('users', 'users.id', '=', 'team_spots.user_id')->where('user_id', $userid)->where('game_id', $request->game_id)->first();
                $checkteam = GameSpots::where('game_id', $request->game_id)->orderBy('id', 'DESC')->first();
                $MyteamRow = '';
                if ($already) {
                    GameSpots::where('user_id', $userid)->where('game_id', $request->game_id)->update([
                        'status' => 1,
                    ]);
                    $MyteamRow = $already;
                } else {
                    if ($checkteam->team == 1) {
                        $team = 2;
                    } else {
                        $team = 1;
                    }
                    $JoinGame = GameSpots::create([
                        'game_id' => $request->game_id,
                        'user_id' => $userid,
                        'team' => $team,
                    ]);
                    $MyteamRow = GameSpots::select('game_id', 'team_spots.id', 'users.name', 'profile_img', 'team', 'user_id')->join('users', 'users.id', '=', 'team_spots.user_id')->where('user_id', $userid)->where('game_id', $request->game_id)->first();

                }

                $gameOwner = User::where('id', $request->game_author_id)->first();
                $gameJoiner = User::where('id', $userid)->first();
                $game = MstGame::where('id', $request->game_id)->first();
                $detailsOwner = [
                    'greeting' => 'Hi ' . $gameOwner->first_name . ' ' . $gameOwner->last_name,
                    'body' => $gameJoiner->first_name . ' ' . $gameJoiner->last_name . ' has joined your created game ' . $game->title,
                    'thanks' => 'Thank you for using ' . url(''),
                    'actionText' => 'View Game',
                    'actionURL' => url('') . '/' . 'games/' . $game->slug,
                    'game_id' => $game->id,
                    'user_id' => $userid,
                    'message' => ' has joined your created game',
                ];

                Notification::send($gameOwner, new GameNotify($detailsOwner));

                $detailsJoiner = [
                    'greeting' => 'Hi ' . $gameJoiner->first_name . ' ' . $gameJoiner->last_name,
                    'body' => ' You have joined a game ' . $game->title,
                    'thanks' => 'Thank you for using ' . url(''),
                    'actionText' => 'View Game',
                    'actionURL' => url('') . '/' . 'games/' . $game->slug,
                    'game_id' => $game->id,
                    'user_id' => $userid,
                    'message' => ' You have joined a game ',
                ];

                Notification::send($gameJoiner, new GameNotify($detailsJoiner));
                return response()->json([
                    'data' => $MyteamRow,
                    'status' => 'success',
                    'message' => 'Game join successfully',
                ]);

            } else {

                // The card has been declined
                throw new \Alvee\WorldPay\lib\WorldpayException(print_r($response, true));
            }
        } catch (Alvee\WorldPay\lib\WorldpayException $e) {
            echo 'Error code: ' . $e->getCustomCode() . '
                HTTP status code:' . $e->getHttpStatusCode() . '
                Error description: ' . $e->getDescription() . '
                Error message: ' . $e->getMessage();

            // The card has been declined
        } catch (Exception $e) {
            // The card has been declined
            echo 'Error message: ' . $e->getMessage();
        }
    }

    public function token(Request $request)
    {
        return $request;
    }

}
