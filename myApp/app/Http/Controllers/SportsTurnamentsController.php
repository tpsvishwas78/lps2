<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SportTurnamentCategory;
use App\Models\SportTurnamentDetail;
use App\Models\SportTurnamentImage;
use App\Models\SportTurnamentVideo;
use App\Models\SportTurnamentBlog;
use App\Models\SportTurnamentFaq;

class SportsTurnamentsController extends Controller
{
    public function index()
    {
        $categories=SportTurnamentCategory::where('status',1)->get();
        return view('sports_tournaments')->with('categories',$categories);
    }
    public function sports_tournaments_detail($slug='')
    {
        
        $cate=SportTurnamentCategory::where('slug',$slug)->first();
       
        $detail=SportTurnamentDetail::where('category_id',$cate->id)->first();
        if(@$detail->layout==1){
            $blogs=SportTurnamentBlog::where('category_id',$cate->id)->get();
            return view('sports_tournaments_detail')->with('detail',$detail)->with('blogs',$blogs);
        }else{
            $faqs=SportTurnamentFaq::where('cp_id',@$detail->id)->get();
            return view('sports_tournaments_detail_layout_two')->with('detail',$detail)->with('faqs',$faqs);
        }
        
    }
}
