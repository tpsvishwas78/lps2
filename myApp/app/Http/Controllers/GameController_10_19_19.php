<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Models\MstGame;
use App\Models\GameSpots;
use App\Models\UserLocation;
use App\Models\MstSport;
use App\Models\GameComment;
use App\Models\RepeatGames;
use App\Mail\InviteMail;
use Auth;
use Notification;
use App\Notifications\GameNotify;
Use App\User;

class GameController extends Controller
{
    public function index(Request $req,$type=null)
    {
       
        $userid=Auth::user()->id;
        @$location=UserLocation::where('user_id',$userid)->first();
        if($req->search && $req->date=='' && $req->sport==''){

            if($req->search){
                $search=$req->search;
            }else{
                $search=@$location->location;
            }

            $games=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
            ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
            ->where([
                ['mst_games.status', '=', 1],
                ['repeat_games.game_start_date', '>=', date('Y-m-d')],
            ])
            ->where(function ($query) use ($req) {
                $query->where('mst_venue.location','LIKE', '%'.$req->search.'%')
                 ->orWhere('mst_venue.postcode','LIKE', '%'.$req->search.'%')
                 ->orWhere('mst_games.title','LIKE', '%'.$req->search.'%');
            })
            ->orderBy('repeat_games.game_start_date','ASC')
            //->limit(10)
            ->paginate(10);
            $gamescount=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
            ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
            ->where([
                ['mst_games.status', '=', 1],
                ['repeat_games.game_start_date', '>=', date('Y-m-d')],
            ])
            ->where(function ($query) use ($req) {
                $query->where('mst_venue.location','LIKE', '%'.$req->search.'%')
                 ->orWhere('mst_venue.postcode','LIKE', '%'.$req->search.'%')
                 ->orWhere('mst_games.title','LIKE', '%'.$req->search.'%');
            })
            ->count();
        }
        elseif($req->search==''){

            if($req->search){
                $search=$req->search;
            }else{
                $search=@$location->location;
            }

            $games=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
            ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
            ->where('mst_games.status',1)
            ->where('repeat_games.game_start_date','>=',date('Y-m-d'))
            ->where('repeat_games.game_start_date','LIKE', '%'.$req->date.'%')
            ->where('mst_games.sport_id','LIKE', '%'.$req->sport.'%')
            ->orderBy('repeat_games.game_start_date','ASC')
            //->limit(10)
            ->paginate(10);
            $gamescount=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
            ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
            ->where('mst_games.status',1)
            ->where('repeat_games.game_start_date','>=',date('Y-m-d'))
            ->where('repeat_games.game_start_date','LIKE', '%'.$req->date.'%')
            ->where('mst_games.sport_id','LIKE', '%'.$req->sport.'%')
            ->count();
        }elseif( $req->search && $req->date=='' && $req->sport ){

            $search=$req->search;

            $games=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
            ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
            ->where([
                ['mst_games.status', '=', 1],
                ['repeat_games.game_start_date', '>=', date('Y-m-d')],
                ['mst_games.sport_id','LIKE', '%'.$req->sport.'%'],
            ])
            ->where(function ($query) use ($req) {
                $query->where('mst_venue.location','LIKE', '%'.$req->search.'%')
                 ->orWhere('mst_venue.postcode','LIKE', '%'.$req->search.'%')
                 ->orWhere('mst_games.title','LIKE', '%'.$req->search.'%');
            })
            
            ->orderBy('repeat_games.game_start_date','ASC')
            ->paginate(10);
            $gamescount=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
            ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
            ->where([
                ['mst_games.status', '=', 1],
                ['repeat_games.game_start_date', '>=', date('Y-m-d')],
                ['repeat_games.game_start_date','LIKE', '%'.$req->date.'%'],
                ['mst_games.sport_id','LIKE', '%'.$req->sport.'%'],
            ])
            ->where(function ($query) use ($req) {
                $query->where('mst_venue.location','LIKE', '%'.$req->search.'%')
                 ->orWhere('mst_venue.postcode','LIKE', '%'.$req->search.'%')
                 ->orWhere('mst_games.title','LIKE', '%'.$req->search.'%');
            })
            ->count();
        }elseif( $req->search && $req->date && $req->sport=='' ){

            $search=$req->search;

            $games=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
            ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
            ->where([
                ['mst_games.status', '=', 1],
                ['repeat_games.game_start_date', '>=', date('Y-m-d')],
                ['repeat_games.game_start_date','LIKE', '%'.$req->date.'%'],
            ])
            ->where(function ($query) use ($req) {
                $query->where('mst_venue.location','LIKE', '%'.$req->search.'%')
                 ->orWhere('mst_venue.postcode','LIKE', '%'.$req->search.'%')
                 ->orWhere('mst_games.title','LIKE', '%'.$req->search.'%');
            })
            
            ->orderBy('repeat_games.game_start_date','ASC')
            ->paginate(10);
            $gamescount=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
            ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
            ->where([
                ['mst_games.status', '=', 1],
                ['repeat_games.game_start_date', '>=', date('Y-m-d')],
                ['repeat_games.game_start_date','LIKE', '%'.$req->date.'%'],
                ['mst_games.sport_id','LIKE', '%'.$req->sport.'%'],
            ])
            ->where(function ($query) use ($req) {
                $query->where('mst_venue.location','LIKE', '%'.$req->search.'%')
                 ->orWhere('mst_venue.postcode','LIKE', '%'.$req->search.'%')
                 ->orWhere('mst_games.title','LIKE', '%'.$req->search.'%');
            })
            ->count();
        }
        elseif( $req->search && $req->date && $req->sport ){

            $search=$req->search;

            $games=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
            ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
            ->where([
                ['mst_games.status', '=', 1],
                ['repeat_games.game_start_date', '>=', date('Y-m-d')],
                ['repeat_games.game_start_date','LIKE', '%'.$req->date.'%'],
                ['mst_games.sport_id','LIKE', '%'.$req->sport.'%'],
            ])
            ->where(function ($query) use ($req) {
                $query->where('mst_venue.location','LIKE', '%'.$req->search.'%')
                 ->orWhere('mst_venue.postcode','LIKE', '%'.$req->search.'%')
                 ->orWhere('mst_games.title','LIKE', '%'.$req->search.'%');
            })
            
            ->orderBy('repeat_games.game_start_date','ASC')
            ->paginate(10);
            $gamescount=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
            ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
            ->where([
                ['mst_games.status', '=', 1],
                ['repeat_games.game_start_date', '>=', date('Y-m-d')],
                ['repeat_games.game_start_date','LIKE', '%'.$req->date.'%'],
                ['mst_games.sport_id','LIKE', '%'.$req->sport.'%'],
            ])
            ->where(function ($query) use ($req) {
                $query->where('mst_venue.location','LIKE', '%'.$req->search.'%')
                 ->orWhere('mst_venue.postcode','LIKE', '%'.$req->search.'%')
                 ->orWhere('mst_games.title','LIKE', '%'.$req->search.'%');
            })
            ->count();
        }
        elseif(@$location){
            
            $games=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
            ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
            ->where('mst_games.status',1)
            ->where('repeat_games.game_start_date','>=',date('Y-m-d'))
            ->where('mst_venue.location','LIKE', '%'.@$location->location.'%')
            ->orderBy('repeat_games.game_start_date','ASC')
            ->paginate(10);
            $gamescount=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
            ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
            ->where('mst_games.status',1)
            ->where('repeat_games.game_start_date','>=',date('Y-m-d'))
            ->where('mst_venue.location','LIKE', '%'.@$location->location.'%')
            ->count();
        }else{
            
        $games=\DB::table('mst_games')
        ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
        ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
            ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
        ->where('mst_games.status',1)
        ->where('repeat_games.game_start_date','>=',date('Y-m-d'))
        ->orderBy('repeat_games.game_start_date','ASC')
        ->paginate(10);
        $gamescount=\DB::table('mst_games')
        ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
        ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
            ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
        ->where('mst_games.status',1)
        ->where('repeat_games.game_start_date','>=',date('Y-m-d'))
        ->count();
        }

        
        
        $sports=MstSport::where('status',1)->get();
        return view('games')->with('games',$games)->with('sports',$sports)->with('gamescount',$gamescount)->with('type',$type);
    }
    public function usergameloadmore(Request $req)
    {
        $id = $req->id;
        $userid=Auth::user()->id;
        @$location=UserLocation::where('user_id',$userid)->first();
        if($req->search || $req->date || $req->sport){

            if($req->search){
                $search=$req->search;
            }else{
                $search=@$location->location;
            }

            $games=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
            ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
            ->where([
                ['mst_games.status', '=', 1],
                ['repeat_games.game_start_date', '>=', date('Y-m-d')],
                ['repeat_games.game_start_date','LIKE', '%'.$req->date.'%'],
                ['mst_games.sport_id','LIKE', '%'.$req->sport.'%'],
            ])
            ->where(function ($query) use ($req) {
                $query->where('mst_venue.location','LIKE', '%'.$req->search.'%')
                 ->orWhere('mst_venue.postcode','LIKE', '%'.$req->search.'%')
                 ->orWhere('mst_games.title','LIKE', '%'.$req->search.'%');
            })
            
            ->orderBy('repeat_games.game_start_date','ASC')
            ->paginate(10);
            
        }
        elseif($location){
            $games=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
            ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
            ->where('mst_games.status',1)
            ->where('repeat_games.game_start_date','>=',date('Y-m-d'))
            ->where('mst_venue.location','LIKE', '%'.$location->location.'%')
            ->orderBy('repeat_games.game_start_date','ASC')
            ->paginate(10);
            
        }else{
        $games=\DB::table('mst_games')
        ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
        ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
            ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
        ->where('mst_games.status',1)
        ->where('repeat_games.game_start_date','>=',date('Y-m-d'))
        ->orderBy('repeat_games.game_start_date','ASC')
        ->paginate(10);
       
        }

        if(!$games->isEmpty()){
            $html= view('loadmoregames', compact('games'))->render();
        }else{
            $html='';
        }
        echo $html;
    }
    
    public function gamedetail($slug='',$id=0)
    {
        // $game=MstGame::where('slug',$slug)->first();
        // $repeat=RepeatGames::where('id',$id)->first();

        $game=\DB::table('mst_games')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
            ->select('*', 'mst_games.id as gid', 'repeat_games.id as id')
            ->where('mst_games.slug','=',$slug)
            ->where('repeat_games.id',$id)
            ->first();
        
        $comments=GameComment::where('game_id',@$game->gid)->where('comment_id',NULL)->get();

        return view('game_detail')->with('game',$game)->with('comments',$comments);
    }
    public function joingame(Request $req)
    {
        $user_id=Auth::user('')->id;
        $already=GameSpots::where('user_id',$user_id)->where('game_id',$req->game_id)->first();
        if($already){
            GameSpots::where('user_id',$user_id)->where('game_id',$req->game_id)->update([
                'status' => 1,
            ]);
        }else{
            GameSpots::create([
                'game_id'=>$req->game_id,
                'user_id' => $user_id,
            ]);
        }
        
        return back()->with('success', 'You are in.');
    }
    public function leavegame(Request $req)
    {
        $user=Auth::user();
        GameSpots::where('user_id',$user->id)->where('game_id',$req->game_id)->update([
            'status' => 0,
        ]);

                
                $game=MstGame::where('id',$req->game_id)->first();
                $gameOwner=User::where('id',$game->user_id)->first();
                $detailsOwner = [
                    'greeting' => 'Hi '.$gameOwner->first_name.' '.$gameOwner->last_name,
                    'body' => $user->first_name.' '.$user->last_name.' has left your game '.$game->title,
                    'thanks' => 'Thank you for using '.url(''),
                    'actionText' => 'View Game',
                    'actionURL' => url('').'/'.'games/'.$game->slug.'/'.$game->game_cid,
                    'game_id' => $game->id,
                    'game_cid' => $req->game_cid,
                    'user_id' => $user->id,
                    'message' => ' has left your game',
                ];
          
                Notification::send($gameOwner, new GameNotify($detailsOwner));
        return back()->with('success', 'You are out.');
    }
    public function gamecomment(Request $req)
    {
        $req->validate([
            'name'=>'required',
            'email'=>'required',
            'comment'=>'required'
        ]);
        GameComment::create([
            'game_id'=>$req->game_id,
            'name'=>$req->name,
            'email'=>$req->email,
            'comment'=>$req->comment,
            'comment_id'=>$req->comment_id,
            'user_id' => $req->userid,
        ]);
        return back()->with('success', 'Thanks for your comment.');
        
    }
    public function sendinvitation(Request $req)
    {
        $invitedata=array(
            'email' => $req->email,
            'gameurl' => $req->gameurl,
            'gametitle' => $req->gametitle,
            'gamevenue' => $req->gamevenue,
        );
        $toEmail = $req->email;
        Mail::to($toEmail)->send(new InviteMail($invitedata));
        return back()->with('success', 'Successfully send you invitation.');
    }
    public function worldpay()
    {
        
        return view('worldpay');
    }

}
