<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Models\MstSport;
use App\Models\MstAreaIntrest;
use App\Models\QuickLinksSlider;
use App\Models\ChildrenPartyCategory;
use App\Models\BlockBooking;
use App\Models\GetInvolved;
use App\Models\ChildrenParty;
use App\Mail\IntrestMail;
use App\Models\KidsDetail;

class ChildrenController extends Controller
{
    public function index()
    {
        $parties=ChildrenPartyCategory::where('status',1)->get();
        $sliders=QuickLinksSlider::where('status',1)->where('title','NOT LIKE', '%corporate%')->get();
        return view('children')->with('sliders',$sliders)->with('parties',$parties);
    }
    public function children_venues(Request $req)
    {
        $sliders=QuickLinksSlider::where('status',1)->where('title','NOT LIKE', '%corporate%')->get();

        if($req->sortby){
            if($req->sortby=='ASC'){
                $orderby='ASC';
            }else{
                $orderby='DESC';
            }
            
        }else{
            $orderby='ASC';
        }
        if ($req->search) {
            $parties=\DB::table('children_party')
            ->select('*')
            ->where([
                ['status', '=', 1],
                ['category', '=', 1],
            ])
            ->where(function ($query) use ($req) {
                $query->where('venue_address','LIKE', '%'.$req->search.'%')
                 ->orWhere('postcode','LIKE', '%'.$req->search.'%')
                 ->orWhere('venue_title','LIKE', '%'.$req->search.'%')
                 ->orWhere('title','LIKE', '%'.$req->search.'%');
            })
            ->orderBy('title',$orderby)
            ->get();
            
        } else {
            $parties=ChildrenParty::where('status',1)->where('category',1)->orderBy('title',$orderby)->get();
        }

        

        return view('children_venues')->with('sliders',$sliders)->with('parties',$parties);
    }
    public function children_coaching(Request $req)
    {
        if($req->sortby){
            if($req->sortby=='ASC'){
                $orderby='ASC';
            }else{
                $orderby='DESC';
            }
            
        }else{
            $orderby='ASC';
        }
        if ($req->search) {
            $parties=\DB::table('children_party')
            ->select('*')
            ->where([
                ['status', '=', 1],
                ['category', '=', 2],
            ])
            ->where(function ($query) use ($req) {
                $query->where('venue_address','LIKE', '%'.$req->search.'%')
                 ->orWhere('postcode','LIKE', '%'.$req->search.'%')
                 ->orWhere('venue_title','LIKE', '%'.$req->search.'%')
                 ->orWhere('title','LIKE', '%'.$req->search.'%');
            })
            ->orderBy('title',$orderby)
            ->get();
            
        } else {
            $parties=ChildrenParty::where('status',1)->where('category',2)->orderBy('title',$orderby)->get();
        }
        return view('children_coaching')->with('parties',$parties);
    }
    public function kids_language(Request $req)
    {
        if($req->sortby){
            if($req->sortby=='ASC'){
                $orderby='ASC';
            }else{
                $orderby='DESC';
            }
            
        }else{
            $orderby='ASC';
        }
        if ($req->search) {
            $parties=\DB::table('children_party')
            ->select('*')
            ->where([
                ['status', '=', 1],
                ['category', '=', 3],
            ])
            ->where(function ($query) use ($req) {
                $query->where('venue_address','LIKE', '%'.$req->search.'%')
                 ->orWhere('postcode','LIKE', '%'.$req->search.'%')
                 ->orWhere('venue_title','LIKE', '%'.$req->search.'%')
                 ->orWhere('title','LIKE', '%'.$req->search.'%');
            })
            ->orderBy('title',$orderby)
            ->get();
            
        } else {
            $parties=ChildrenParty::where('status',1)->where('category',3)->orderBy('title',$orderby)->get();
        }
        return view('kids_language')->with('parties',$parties);
    }
    public function children_detail($slug='')
    {
        $party=ChildrenParty::where('slug',$slug)->first();
        $sliders=QuickLinksSlider::where('status',1)->where('title','NOT LIKE', '%corporate%')->get();
        return view('children_detail')->with('sliders',$sliders)->with('party',$party);
    }
    

    public function register_your_intrest()
    {
        return view('register_your_intrest');
    }

    public function submitIntrest(Request $req)
    {
        $req->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'mobile_number' => 'required|digits:11',
            'location' => 'required',
            'message' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ]);

        $intrestdata=array(
            'first_name' => $req->first_name,
            'last_name' => $req->last_name,
            'email' => $req->email,
            'phone_number' => $req->mobile_number,
            'location' => $req->location,
            'message' => $req->message,
        );
        
        $send=Mail::to(config('app.sendmail'))->send(new IntrestMail($intrestdata));
        return back()->with('success', 'THANK YOU FOR YOUR MESSAGE! WE WILL GET BACK TO YOU AS SOON AS POSSIBLE!.');
        
    }

    public function kids_deayils($slug='')
    {
        $cat=ChildrenPartyCategory::where('slug',$slug)->first();
        
        if($cat->layout==1){
            $party=KidsDetail::where('category_id',$cat->id)->where('status',1)->get();
            return view('kids_details')->with('party',$party)->with('cat',$cat);
        }else{
            $party=KidsDetail::where('category_id',$cat->id)->first();
            return view('kids_details_two')->with('party',$party)->with('cat',$cat);
        }
        
    }
    

}
