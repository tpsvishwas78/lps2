<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\MstVenue;
use App\Models\MstGame;
use App\Models\GameImages;
use App\Models\GameSpots;
use App\Models\MstSport;
use App\Models\VenueFollow;
use App\Models\RepeatGames;
use Notification;
use App\Notifications\GameNotify;
use App\Notifications\GameAdminNotifyMail;
Use Auth;
Use App\User;
use DateTime;
use DatePeriod;
use DateInterval;
use Illuminate\Support\Facades\Redirect;


class StartGameController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth' => 'verified']);
    }
    public function index($id=0)
    {
        if($id>0){
            $edit=MstGame::where('id',$id)->first(); 
        }else{
            $edit=array();
        }
        $sports=MstSport::where('status',1)->get();
       return view('startgame')->with('sports',$sports)->with('edit',$edit);
    }
    public function creategame(Request $req)
    {
        $oldgame=MstGame::orderBy('id', 'DESC')->first();
        

        $user_id=Auth::user()->id;
        $req->validate([
            'title' => ['required'],
            'venue' => ['required'],
            'sport' => ['required'],
            'game_date' => ['required'],
            'game_start_time' => ['required'],
            'game_finish_time' => ['required'],
            'gender' => ['required'],
            'payment' => ['required'],
            'price' => ['required','regex:/^\d+(\.\d{1,2})?$/'],
            'team_limit' => ['required'],
        ]);

        if($req->id){
            $data=MstGame::where('id',$req->id)->first();
            
            if($req['game_date']){
                $game_date = date('Y-m-d', strtotime($req['game_date']));
                $end_date = date('Y-m-d', strtotime($req['end_date']));
            }else{
                $game_date = NULL;
                $end_date = NULL;
            }
    
            if($req->featured_img){
                if($data->featured_img){
                    @unlink("upload\images\/".$data->featured_img);
                }
                $imageName = time().'.'.$req->featured_img->getClientOriginalExtension();
                $req->featured_img->move('upload/images', $imageName);
            }else{
                $imageName=$data->featured_img;
            }
            //$slug = str_slug($req['title'], "-").'-'.($oldgame->id+1);
            MstGame::where('id',$req->id)->update([
                'user_id' => $user_id,
                'title' => $req['title'],
                //'slug' => $slug,
                'sport_id' => $req['sport'],
                'featured_img' => $imageName,
                'description' => $req['description'],
                'game_date' => $game_date,
                'game_start_time' => $req['game_start_time'],
                'game_finish_time' => $req['game_finish_time'],
                'gender' => $req['gender'],
                'payment' => $req['payment'],
                'team_limit' => $req['team_limit'],
                'venue_id' => $req['venue_id'],
                'is_private' => $req['is_private'],
                'price' => $req['price'],
                'currency' => $req['currency'],
                'is_refund' => $req['is_refund'],
                'refund_changed_rsvp' => $req['refund_changed_rsvp'],
                'refund_days' => $req['refund_days'],
                'is_repeat' => (isset($req->repeat_check)) ? $req['is_repeat'] : 0,
                'end_date' => $end_date,
                'days' => serialize($req['days']),
            ]);
            if($req['repeat_check']){
                if($req->has('days'))
                {
                    
    
                        RepeatGames::where('game_id',$req->id)->where('game_start_date','>=',date('Y-m-d'))->delete();
    
                        $start = new DateTime($game_date);
                        $end = new DateTime($end_date);
                        $end = $end->modify('+1 day');
                        $interval = DateInterval::createFromDateString('1 day');
                        $period = new DatePeriod($start, $interval, $end);
                        
                        /* days output */
                        foreach ($period as $date) {
                            $day = $date->format("N");
                            
                            if (in_array($day, $req['days']))
                                {
                                    if($date->format("Y-m-d") >= date('Y-m-d')){
                                        RepeatGames::create([
                                            'game_id'=>$req->id,
                                            'game_start_date'=>$date->format("Y-m-d"),
                                        ]);
                                    }
                                    
                                }
                        }
                    }
                       
                    }else{
                        if(!isset($req['repeat_check'])){
                            $datecheck=RepeatGames::where('game_id',$req->id)->where('game_start_date','=',$game_date)->first();
                            if(!$datecheck){
                                RepeatGames::create([
                                    'game_id'=>$req->id,
                                    'game_start_date'=>$game_date,
                                ]);
                            }
                            
                         }
                    }
            
            if($req->hasfile('gallary'))
             {
                foreach($req->file('gallary') as $image)
                {
                    $name=$image->getClientOriginalName();
                    $image->move('upload/images', $name);
                    GameImages::create([
                        'game_id'=>$req->id,
                        'image'=>$name,
                    ]);
                }
             }
    
            return redirect('mygames')->with('success', 'Game updated successfully.');
        }else{
            if($req['game_date']){
                $game_date = date('Y-m-d', strtotime($req['game_date']));
                $end_date = date('Y-m-d', strtotime($req['end_date']));
            }else{
                $game_date = NULL;
                $end_date = NULL;
            }
    
            if($req->featured_img){
                $imageName = time().'.'.$req->featured_img->getClientOriginalExtension();
                $req->featured_img->move('upload/images', $imageName);
            }else{
                $imageName='';
            }
            $slug = str_slug($req['title'], "-").'-'.($oldgame->id+1);
            $mid=MstGame::create([
                'user_id' => $user_id,
                'title' => $req['title'],
                'slug' => $slug,
                'sport_id' => $req['sport'],
                'featured_img' => $imageName,
                'description' => $req['description'],
                'game_date' => $game_date,
                'game_start_time' => $req['game_start_time'],
                'game_finish_time' => $req['game_finish_time'],
                'gender' => $req['gender'],
                'payment' => $req['payment'],
                'team_limit' => $req['team_limit'],
                'venue_id' => $req['venue_id'],
                'is_private' => $req['is_private'],
                'price' => $req['price'],
                'currency' => $req['currency'],
                'is_refund' => $req['is_refund'],
                'refund_changed_rsvp' => $req['refund_changed_rsvp'],
                'refund_days' => $req['refund_days'],
                'is_repeat' => $req['is_repeat'],
                'end_date' => $end_date,
                'days' => serialize($req['days']),
            ]);
            if($req->has('days'))
            {
                $start = new DateTime($game_date);
                $end = new DateTime($end_date);
                $end = $end->modify('+1 day');
                $interval = DateInterval::createFromDateString('1 day');
                $period = new DatePeriod($start, $interval, $end);
                
                /* days output */
                foreach ($period as $date) {
                    $day = $date->format("N");
                    if (in_array($day, $req['days']))
                        {
                            RepeatGames::create([
                                'game_id'=>$mid->id,
                                'game_start_date'=>$date->format("Y-m-d"),
                            ]);
                        }
                }
                   
                }else{
                    RepeatGames::create([
                        'game_id'=>$mid->id,
                        'game_start_date'=>$game_date,
                    ]);
                }
            
            
            
            // GameSpots::create([
            //     'game_id'=>$mid->id,
            //     'user_id' => $user_id,
            //     'team' => 1
            // ]);
            
            if($req->hasfile('gallary'))
             {
                foreach($req->file('gallary') as $image)
                {
                    $name=$image->getClientOriginalName();
                    $image->move('upload/images', $name);
                    GameImages::create([
                        'game_id'=>$mid->id,
                        'image'=>$name,
                    ]);
                }
             }
    
            //  $venes=VenueFollow::where('venue_id',$req['venue_id'])->get();
            //  foreach ($venes as $key => $vene) {
                 
            //     $notifyuser=User::where('id',$vene->user_id)->first();
            //     if($notifyuser){
            //         $details = [
            //             'greeting' => 'Hi '.$notifyuser->first_name.' '.$notifyuser->last_name,
            //             'body' => 'created a new game on a venue you are following',
            //             'thanks' => 'Thank you for using '.url(''),
            //             'actionText' => 'View Game',
            //             'actionURL' => url('').'/'.'games/'.$slug,
            //             'game_id' => $mid->id,
            //             'user_id' => $user_id,
            //             'message' => 'created a new game on a venue you are following',
            //         ];
              
            //         Notification::send($notifyuser, new GameNotify($details));
            //     }
                
            //  }
            //  $gamedata=array(
            //     'game_id' => $mid->id,
            // );
             //$send=Mail::to('lpsgamesbox@gmail.com')->send(new GameAdminNotifyMail($gamedata));
            
             $gameuser=User::where('id',$user_id)->first();
             $repeat=RepeatGames::where('game_id',$mid->id)->orderBy('game_start_date')->first();
             $details = [
                            'greeting' => 'Hi Admin',
                            'body' => $gameuser->first_name.' '.$gameuser->last_name.' created a new game.',
                            'thanks' => 'New game added waiting for approval.',
                            'actionText' => 'View Game',
                            'actionURL' => url('').'/'.'games/'.$slug.'/'.$repeat->id,
                            'message' => 'New game added waiting for approval.',
                        ];
                  
                        Notification::route('mail', config('app.sendmail'))->notify(new GameAdminNotifyMail($details));
             
            
    
            return back()->with('success', 'Game Created successfully.');
        }

        
    }
    public function sendNotification()
    {
        $user = Auth::user();
        
        $details = [
            'greeting' => 'Hi Tapas',
            'body' => 'created a new game on a venue you are following',
            'thanks' => 'Thank you for using ItSolutionStuff.com tuto!',
            'actionText' => 'View My Site',
            'actionURL' => url('/'),
            'game_id' => 1,
            'user_id' => 1,
            'message' => 'created a new game on a venue you are following',
        ];
  
        //Notification::send($user, new GameNotify($details));
        Notification::route('mail', 'tpsvishwas78@gmail.com')->notify(new GameNotify($details));
        dd('done');
    }
    
    public function gamedeletegalleryimage($id=0)
    {
        GameImages::where('id',$id)->delete();
        return back()->with('success', 'Venue gallery image deleted successfully.');
    }
    
    public function checkdate()
    {

$start = new DateTime('2019-09-01');
$end = new DateTime('2019-10-13');
$end = $end->modify('+1 day');
$interval = DateInterval::createFromDateString('1 day');
$period = new DatePeriod($start, $interval, $end);

$days = $start->diff($end, true)->days;

$sundays = intval($days / 7) + ($start->format('N') + $days % 7 >= 7);
/* sundays output */
foreach ($period as $date) {
    echo  $day = $date->format("N");
    echo $date->format("D Y-m-d") . "</br>";
    }
       dd($days);
    }


    public function gamedelete(Request $request,$id=null){
        if($id){
            $team = GameSpots::where('game_id',$id)->where('user_id',Auth::user()->id)->count();
           
            if($team > 0){
                  $request->session()->flash('error', 'This game is not deteted Because some users are already join this game');
                  return Redirect::back();
            }else{
               
                MstGame::where('id',$id)->delete();
                RepeatGames::where('game_id',$id)->delete();
                $request->session()->flash('success', 'Game deleted Successfully!!');
                return Redirect::back();
            }
        }else{
            $request->session()->flash('success', 'Oops Someting went wrong please try again');
            return Redirect::back();
        }
    }
    
}
