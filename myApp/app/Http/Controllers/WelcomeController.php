<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\HomeBanner;
use App\Models\QuickLinksSlider;
use App\Models\HomeAboutUs;
use App\Models\BasicFeatures;
use App\Models\SiteSetting;
use App\Models\MstGame;
use App\Models\MstVenue;

class WelcomeController extends Controller
{
    public function index()
    {
 
        $banners=HomeBanner::where('status',1)->get();
        $sliders=QuickLinksSlider::where('status',1)->get();
        $aboutus=HomeAboutUs::where('status',1)->first();
        $features=BasicFeatures::where('status',1)->get();
        $upcomingcount=MstGame::where('status',1)->where('game_date','>=',date('Y-m-d'))->count();
        $playedcount=MstGame::where('status',1)->where('game_date','<',date('Y-m-d'))->count();
        $venuescount=MstVenue::where('status',1)->count();
        $venues=MstVenue::where('status',1)->offset(0)->limit(3)->get();
        $setting=SiteSetting::first();

        $upcominggames=\DB::table('mst_games')
        ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
        ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
        ->where('mst_games.status',1)
        ->where('mst_games.game_date','>=',date('Y-m-d'))
        ->orderBy('game_date','ASC')
        ->offset(0)->limit(3)
        ->get();

        return view('welcome')->with('banners',$banners)->with('sliders',$sliders)->with('aboutus',$aboutus)->with('features',$features)->with('setting',$setting)->with('upcominggames',$upcominggames)->with('upcomingcount',$upcomingcount)->with('playedcount',$playedcount)->with('venuescount',$venuescount)->with('venues',$venues);
    }
}
