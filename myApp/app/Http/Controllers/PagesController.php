<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pages;
use App\Models\Menus;

class PagesController extends Controller
{
    public function index($slug='')
    {
        $menu=Menus::where('slug',$slug)->first();
        $page=Pages::where('menu_id',$menu->id)->first();
        return view('pages')->with('page',$page);
    }
}
