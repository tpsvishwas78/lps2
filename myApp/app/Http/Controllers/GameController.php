<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Models\MstGame;
use App\Models\GameSpots;
use App\Models\UserLocation;
use App\Models\MstSport;
use App\Models\GameComment;
use App\Models\RepeatGames;
use App\Mail\InviteMail;
use Auth;
use Notification;
use App\Notifications\GameNotify;
use App\User;
use App\Models\HomeBanner;
use App\Models\SiteSetting;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;

class GameController extends Controller
{
    public function index(Request $req, $type = null)
    { 
        // print_r($req->cookie('GameDetail'));die;
        if($req->cookie('GameDetail') !=''){
            \Cookie::queue(
                \Cookie::forget('GameDetail')
            );
          return  Redirect::to($req->cookie('GameDetail'));
        }
        $userid = Auth::user()->id;
        @$location = UserLocation::where('user_id', $userid)->first();
       
        $gamesquery=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id');

             if (($req->has('near_latitude') && $req->near_latitude != 'null' && $req->near_latitude) && ($req->has('near_longitude') && $req->near_longitude != 'null' && $req->near_longitude)) {
                
                $gamesquery->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus','mst_venue.latitude as latitude','mst_venue.longitude as longitude',
                \DB::raw("get_distance('".$req->near_latitude.",".$req->near_longitude."', mst_venue.latitude,mst_venue.longitude  ) as distance") );

              }else{
                $gamesquery->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus','mst_venue.latitude as latitude','mst_venue.longitude as longitude'
            );
            } 
            
            if ($req->sport != 'null' && $req->has('sport') && $req->sport) {
                $findvalues = MstSport::where('main_id',$req->sport)->get();
                $mysports=array();
                if(!$findvalues->isEmpty()){
                    foreach ($findvalues as $key => $findvalue) {
                        $mysports[]=$findvalue->id;
                    }
                    $mysports[]=$findvalue->main_id;
                }
                else{
                    $mysports[]=$req->sport;
                }
                $gamesquery->whereIn('sport_id', $mysports);
                
            }
            
            if ($req->has('gender') && $req->gender != 'null' && $req->gender) {

                if ($req->input('gender') == '2') {
                    $gamesquery->whereIn('gender', array(2, 1));
                } elseif ($req->input('gender') == '3') {
                    $gamesquery->whereIn('gender', array(3, 1));
                } else {
                    $gamesquery->where('gender', $req->input('gender'));
    
                }
    
            }
            if ($req->has('date') && $req->date != 'null' && $req->date) {
                    $gamesquery->where('repeat_games.game_start_date','LIKE', '%'.$req->date.'%');
            }
            
            $gamesquery->where(function ($query) use ($req) {
                    if($req->search && ($req->near_latitude=='' && $req->near_longitude == '' )){
                        
                        $query->where('mst_venue.location','LIKE', '%'.$req->search.'%')
                     ->orWhere('mst_venue.postcode','LIKE', '%'.$req->search.'%')
                     ->orWhere('mst_games.title','LIKE', '%'.$req->search.'%');
                    }                
            });
            $gamesquery->where('mst_games.status', '=', 1);
            $gamesquery->where('repeat_games.game_start_date', '>=', date('Y-m-d'));

            if (($req->has('near_latitude') && $req->near_latitude != 'null' && $req->near_latitude) && ($req->has('near_longitude') && $req->near_longitude != 'null' && $req->near_longitude)) {

            $gamesquery->where(\DB::raw("get_distance('".$req->near_latitude.",".$req->near_longitude."', mst_venue.latitude,mst_venue.longitude  )"), '<=', 8);
               
            }
             
            $gamesquery->orderBy('repeat_games.game_start_date','ASC');
            $games = $gamesquery->paginate(10);
            

            $gamescountquery=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id');

            if (($req->has('near_latitude') && $req->near_latitude != 'null' && $req->near_latitude) && ($req->has('near_longitude') && $req->near_longitude != 'null' && $req->near_longitude)) {
                
                $gamescountquery->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus','mst_venue.latitude as latitude','mst_venue.longitude as longitude',
                \DB::raw("get_distance('".$req->near_latitude.",".$req->near_longitude."', mst_venue.latitude,mst_venue.longitude  ) as distance") );

              }else{
                $gamescountquery->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus','mst_venue.latitude as latitude','mst_venue.longitude as longitude'
            );
            }

            if ($req->sport != 'null' && $req->has('sport') && $req->sport) {
                $findvalues = MstSport::where('main_id',$req->sport)->get();
                $mysports=array();
                if(!$findvalues->isEmpty()){
                    foreach ($findvalues as $key => $findvalue) {
                        $mysports[]=$findvalue->id;
                    }
                    $mysports[]=$findvalue->main_id;
                }
                else{
                    $mysports[]=$req->sport;
                }
                $gamescountquery->whereIn('sport_id', $mysports);
                
            }
            if ($req->has('gender') && $req->gender != 'null' && $req->gender) {

                if ($req->input('gender') == '2') {
                    $gamesquery->whereIn('gender', array(2, 1));
                } elseif ($req->input('gender') == '3') {
                    $gamesquery->whereIn('gender', array(3, 1));
                } else {
                    $gamesquery->where('gender', $req->input('gender'));
    
                }
    
            }
            if ($req->has('date') && $req->date != 'null' && $req->date) {
                    $gamescountquery->where('repeat_games.game_start_date','LIKE', '%'.$req->date.'%');
            }
            
            $gamescountquery->where(function ($query) use ($req) {
                if($req->search && ($req->near_latitude=='' && $req->near_longitude == '' )){
                        $query->where('mst_venue.location','LIKE', '%'.$req->search.'%')
                     ->orWhere('mst_venue.postcode','LIKE', '%'.$req->search.'%')
                     ->orWhere('mst_games.title','LIKE', '%'.$req->search.'%');
                    }                
            });
            $gamescountquery->where('mst_games.status', '=', 1);
            $gamescountquery->where('repeat_games.game_start_date', '>=', date('Y-m-d'));
            if (($req->has('near_latitude') && $req->near_latitude != 'null' && $req->near_latitude) && ($req->has('near_longitude') && $req->near_longitude != 'null' && $req->near_longitude)) {

                $gamescountquery->where(\DB::raw("get_distance('".$req->near_latitude.",".$req->near_longitude."', mst_venue.latitude,mst_venue.longitude  )"), '<=', 8);
                   
                }
            $gamescountquery->orderBy('repeat_games.game_start_date','ASC');
            $gamescount = $gamescountquery->count();
            

            $allgamesquery=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id');

            if (($req->has('near_latitude') && $req->near_latitude != 'null' && $req->near_latitude) && ($req->has('near_longitude') && $req->near_longitude != 'null' && $req->near_longitude)) {
                
                $allgamesquery->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus','mst_venue.latitude as latitude','mst_venue.longitude as longitude',
                \DB::raw("get_distance('".$req->near_latitude.",".$req->near_longitude."', mst_venue.latitude,mst_venue.longitude  ) as distance") );

               
              }else{
                $allgamesquery->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus','mst_venue.latitude as latitude','mst_venue.longitude as longitude'
            );
            }

            if ($req->sport != 'null' && $req->has('sport') && $req->sport) {
                $findvalues = MstSport::where('main_id',$req->sport)->get();
                $mysports=array();
                if(!$findvalues->isEmpty()){
                    foreach ($findvalues as $key => $findvalue) {
                        $mysports[]=$findvalue->id;
                    }
                    $mysports[]=$findvalue->main_id;
                }
                else{
                    $mysports[]=$req->sport;
                }
                $allgamesquery->whereIn('sport_id', $mysports);
                
            }
            if ($req->has('gender') && $req->gender != 'null' && $req->gender) {

                if ($req->input('gender') == '2') {
                    $gamesquery->whereIn('gender', array(2, 1));
                } elseif ($req->input('gender') == '3') {
                    $gamesquery->whereIn('gender', array(3, 1));
                } else {
                    $gamesquery->where('gender', $req->input('gender'));
    
                }
    
            }
            if ($req->has('date') && $req->date != 'null' && $req->date) {
                    $allgamesquery->where('repeat_games.game_start_date','LIKE', '%'.$req->date.'%');
            }
            
            $allgamesquery->where(function ($query) use ($req) {
                if($req->search && ($req->near_latitude=='' && $req->near_longitude == '' )){
                        $query->where('mst_venue.location','LIKE', '%'.$req->search.'%')
                     ->orWhere('mst_venue.postcode','LIKE', '%'.$req->search.'%')
                     ->orWhere('mst_games.title','LIKE', '%'.$req->search.'%');
                    }                
            });
            $allgamesquery->where('mst_games.status', '=', 1);
            $allgamesquery->where('repeat_games.game_start_date', '>=', date('Y-m-d'));
            if (($req->has('near_latitude') && $req->near_latitude != 'null' && $req->near_latitude) && ($req->has('near_longitude') && $req->near_longitude != 'null' && $req->near_longitude)) {

                $allgamesquery->where(\DB::raw("get_distance('".$req->near_latitude.",".$req->near_longitude."', mst_venue.latitude,mst_venue.longitude  )"), '<=', 8);
                   
                }
            $allgamesquery->orderBy('repeat_games.game_start_date','ASC');
            $allgames = $allgamesquery->get();

        $banners=HomeBanner::where('status',1)->get();
        $setting=SiteSetting::first();

        $sports = MstSport::where('status', 1)->orderBy('sport_order','ASC')->get();


        $myjoinedgames=\DB::table('mst_games')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
            ->leftJoin('game_payment', 'game_payment.game_id', '=', 'repeat_games.id')
            ->select('*', 'mst_games.id as id', 'repeat_games.id as rid')
            ->where('mst_games.status',1)
            ->where('game_payment.user_id',$userid)
            ->orderBy('game_start_date','DESC')
            ->get();

        $mygames=\DB::table('mst_games')
            ->join('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
            //->leftJoin('game_payment', 'game_payment.game_id', '=', 'repeat_games.id')
            ->select('*', 'mst_games.id as id', 'repeat_games.id as rid')
            ->where('mst_games.user_id','=',$userid)
            ->where('mst_games.status',1)            
            ->orderBy('game_start_date','DESC')
            ->get();
        
        // dump($myjoinedgames);
        // dd($mygames);
            

        return view('games')
        ->with('games', $games)
        ->with('allgames', $allgames)
        ->with('sports', $sports)
        ->with('gamescount', $gamescount)
        ->with('myjoinedgames', $myjoinedgames)
        ->with('mygames', $mygames)
        ->with('type', $type);
    }


    public function usergameloadmore(Request $req)
    {
        $gamesquery=\DB::table('mst_games')
            ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id');

             if (($req->has('near_latitude') && $req->near_latitude != 'null' && $req->near_latitude) && ($req->has('near_longitude') && $req->near_longitude != 'null' && $req->near_longitude)) {
                
                $gamesquery->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus','mst_venue.latitude as latitude','mst_venue.longitude as longitude',
                \DB::raw("get_distance('".$req->near_latitude.",".$req->near_longitude."', mst_venue.latitude,mst_venue.longitude  ) as distance") );

               
              }else{
                $gamesquery->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus','mst_venue.latitude as latitude','mst_venue.longitude as longitude'
            );
            } 
            
            if ($req->sport != 'null' && $req->has('sport') && $req->sport) {
                $findvalues = MstSport::where('main_id',$req->sport)->get();
                $mysports=array();
                if(!$findvalues->isEmpty()){
                    foreach ($findvalues as $key => $findvalue) {
                        $mysports[]=$findvalue->id;
                    }
                    $mysports[]=$findvalue->main_id;
                }
                else{
                    $mysports[]=$req->sport;
                }
                $gamesquery->whereIn('sport_id', $mysports);
                
            }
            
            if ($req->has('gender') && $req->gender != 'null' && $req->gender) {

                if ($req->input('gender') == '2') {
                    $gamesquery->whereIn('gender', array(2, 1));
                } elseif ($req->input('gender') == '3') {
                    $gamesquery->whereIn('gender', array(3, 1));
                } else {
                    $gamesquery->where('gender', $req->input('gender'));
    
                }
    
            }
            if ($req->has('date') && $req->date != 'null' && $req->date) {
                    $gamesquery->where('repeat_games.game_start_date','LIKE', '%'.$req->date.'%');
            }
            
            $gamesquery->where(function ($query) use ($req) {
                if($req->search && ($req->near_latitude=='' && $req->near_longitude == '' )){
                        $query->where('mst_venue.location','LIKE', '%'.$req->search.'%')
                     ->orWhere('mst_venue.postcode','LIKE', '%'.$req->search.'%')
                     ->orWhere('mst_games.title','LIKE', '%'.$req->search.'%');
                    }                
            });
            $gamesquery->where('mst_games.status', '=', 1);
            $gamesquery->where('repeat_games.game_start_date', '>=', date('Y-m-d'));

            if (($req->has('near_latitude') && $req->near_latitude != 'null' && $req->near_latitude) && ($req->has('near_longitude') && $req->near_longitude != 'null' && $req->near_longitude)) {

            $gamesquery->where(\DB::raw("get_distance('".$req->near_latitude.",".$req->near_longitude."', mst_venue.latitude,mst_venue.longitude  )"), '<=', 8);
               
            }
             
            $gamesquery->orderBy('repeat_games.game_start_date','ASC');
            $games = $gamesquery->paginate(10);

        if (!$games->isEmpty()) {
            $html = view('loadmoregames', compact('games'))->render();
        } else {
            $html = '';
        }
        echo $html;
    }

    public function gamedetail($slug = '', $id = 0)
    {
        // $game=MstGame::where('slug',$slug)->first();
        // $repeat=RepeatGames::where('id',$id)->first();

        $game = \DB::table('mst_games')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
            ->select('*', 'mst_games.id as gid', 'repeat_games.id as id')
            ->where('mst_games.slug', '=', $slug)
            ->where('repeat_games.id', $id)
            ->first();

        $comments = GameComment::where('game_id', @$game->gid)->where('comment_id', NULL)->get();

      
            return view('game_detail')->with('game', $game)->with('comments', $comments);
       
    }
    public function joingame(Request $req)
    {
        $user_id = Auth::user('')->id;
        $already = GameSpots::where('user_id', $user_id)->where('game_id', $req->game_id)->first();
        if ($already) {
            GameSpots::where('user_id', $user_id)->where('game_id', $req->game_id)->update([
                'status' => 1,
            ]);
        } else {
            GameSpots::create([
                'game_id' => $req->game_id,
                'user_id' => $user_id,
            ]);
        }

        return back()->with('success', 'You are in.');
    }
    public function leavegame(Request $req)
    {
        $user = Auth::user();
        GameSpots::where('user_id', $user->id)->where('game_id', $req->game_id)->update([
            'status' => 0,
        ]);


        $game = MstGame::where('id', $req->game_id)->first();
        $gameOwner = User::where('id', $game->user_id)->first();
        $detailsOwner = [
            'greeting' => 'Hi ' . $gameOwner->first_name . ' ' . $gameOwner->last_name,
            'body' => $user->first_name . ' ' . $user->last_name . ' has left your game ' . $game->title,
            'thanks' => 'Thank you for using ' . url(''),
            'actionText' => 'View Game',
            'actionURL' => url('') . '/' . 'games/' . $game->slug . '/' . $game->game_cid,
            'game_id' => $game->id,
            'game_cid' => $req->game_cid,
            'user_id' => $user->id,
            'message' => ' has left your game',
        ];

        Notification::send($gameOwner, new GameNotify($detailsOwner));
        return back()->with('success', 'You are out.');
    }
    public function gamecomment(Request $req)
    {
        $req->validate([
            'name' => 'required',
            'email' => 'required',
            'comment' => 'required'
        ]);
        GameComment::create([
            'game_id' => $req->game_id,
            'name' => $req->name,
            'email' => $req->email,
            'comment' => $req->comment,
            'comment_id' => $req->comment_id,
            'user_id' => $req->userid,
        ]);
        return back()->with('success', 'Thanks for your comment.');
    }
    public function sendinvitation(Request $req)
    {
        $invitedata = array(
            'email' => $req->email,
            'gameurl' => $req->gameurl,
            'gametitle' => $req->gametitle,
            'gamevenue' => $req->gamevenue,
        );
        $toEmail = $req->email;
        Mail::to($toEmail)->send(new InviteMail($invitedata));
        return back()->with('success', 'Successfully send you invitation.');
    }
    public function worldpay()
    {

        return view('worldpay');
    }

    public function setcookie(Request $request){
    

        $minutes = 10;
        $response = Response(array('status'=>'success'));
        $response->withCookie(cookie('GameDetail', $request['Url'], $minutes));
        return $response;
        
    }

    public function getCookie(Request $request) {
        $value = $request->cookie($request->name);
        return $value;
     }
}
