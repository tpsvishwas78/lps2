<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Notifications\NewUserSignupAdminMail;
use Notification;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => ['required', 'alpha', 'max:255'],
            'last_name' => ['required', 'alpha', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'mobile_number' => ['required', 'string', 'digits:11'],
            'password' => ['required', 'string', 'min:8','regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!@$#%^&*()-+?><:]).*$/'],
            'g-recaptcha-response' => ['required','captcha']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        

        $fullname=$data['first_name'].' '.$data['last_name'];
        $username = str_slug($fullname, "-");

        $check=User::where('username',$username)->first();

        if($check){
            $username =$username.'-'.$check->id;
        }else{
            $username =$username;
        }
        
        $user = User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'username' => $username,
            'email' => $data['email'],
            'phone_number' => $data['mobile_number'],
            'password' => Hash::make($data['password']),
            'user_role' => 2,
            'active' => 1,
        ]);
        $user->sendEmailVerificationNotification();

        
        $admin = User::where('email', 'info@letsplaysports.co.uk')->first();
        $detailsJoiner = [
            'greeting' => 'Hi Admin',
            'body' => ' New user '. $data['first_name'].' '.$data['last_name'].' just signup ',
            'thanks' => 'Thank you for using ' . url(''),
            'actionText' => 'View Game',            
            'message' => ' New user have signup! ',
        ];
        
        Notification::send($admin, new NewUserSignupAdminMail($detailsJoiner));
        

        return $user;
    }
}
