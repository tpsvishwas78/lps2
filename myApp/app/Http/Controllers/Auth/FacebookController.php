<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Socialite;
use Exception;
use Auth;
use App\User;

class FacebookController extends Controller
{
    
    const facebookScope = [
        'email',
    ];
    /**
     * Initialize Facebook fields to override
     */
    const facebookFields = [
        'first_name', // Default
        'last_name', // Default
        'name', // Default
        'email', // Default
    ];
    

    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->fields(self::facebookFields)->scopes(self::facebookScope)->redirect();
    }
    public function handleFacebookCallback()
    {

        try {
            $user = Socialite::driver('facebook')->fields(self::facebookFields)->user();
            $first_name = $user->user['first_name'];
            $last_name = $user->user['last_name'];
            $email = $user->getEmail();
            $facebook_id = $user->getId();
            $profile_img = $user->avatar_original;
            $username = str_slug($user->getName(), "-");
        
            

        $img=time().'.png';
        file_put_contents('upload/images/'.$img, file_get_contents($profile_img));

        $check=User::where('username',$username)->first();

        if($check){
            $username =$username.'-'.$check->id;
        }else{
            $username =$username;
        }
        
        $create=array(
            'first_name' => $first_name,
            'last_name' => $last_name,
            'username' => $username,
            'email' => $email,
            'profile_img' =>$img,
            'facebook_id' =>$facebook_id,
            'user_role' => 2,
            'email_verified_at' => date('Y-m-d H:i:s'),
        );

            $userModel = new User;
            
            $createdUser = $userModel->addNew($create);
            
            Auth::loginUsingId($createdUser->id);
            
            return redirect()->route('dashboard');


        } catch (Exception $e) {
            //dd($e);
            return redirect('auth/facebook');


        }
    }
}
