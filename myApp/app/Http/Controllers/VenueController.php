<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MstVenue;
use App\Models\MstGame;
use Auth;

class VenueController extends Controller
{
    public function index($slug='')
    {
        @$userid=Auth::user()->id;
       if($slug==''){
        return redirect('');
       }
       $venue=MstVenue::where('slug',$slug)->first();
       
       $upcominggames=\DB::table('mst_games')
       ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
       ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
       ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
        ->where('mst_games.status',1)
        ->where('mst_games.venue_id',$venue->id)
        ->where('repeat_games.game_start_date', '>=', date('Y-m-d'))
        ->orderBy('repeat_games.game_start_date','ASC')
        ->get();

        $playedgames=\DB::table('mst_games')
        ->join('mst_venue', 'mst_games.venue_id', '=', 'mst_venue.id')
            ->leftJoin('repeat_games', 'mst_games.id', '=', 'repeat_games.game_id')
            ->select('*', 'mst_venue.id as vid', 'mst_games.id as id', 'repeat_games.id as rid', 'mst_games.slug as slug', 'mst_venue.slug as vslug', 'mst_venue.description as vdescription', 'mst_games.description as description', 'mst_games.status as status', 'mst_venue.status as vstatus')
        ->where('mst_games.status',1)
        ->where('mst_games.venue_id',$venue->id)
        ->where('repeat_games.game_start_date', '<', date('Y-m-d'))
        ->orderBy('repeat_games.game_start_date','ASC')
        ->get();

        $followes=\DB::table('venue_followers')->where('venue_id',$venue->id)->count();
        if(@$userid){
            $mefollow=\DB::table('venue_followers')->where('user_id',$userid)->where('venue_id',$venue->id)->count();
        }else{
            $mefollow=0;
        }
        

        return view('venuedetail')->with('venue',$venue)->with('upcominggames',$upcominggames)->with('playedgames',$playedgames)->with('followes',$followes)->with('mefollow',$mefollow);
    }

    public function autocomplete(Request $req)
    {
        $query = $req['query'];
        if($query )
        {
        $venues=MstVenue::where('location', 'LIKE', "%{$query}%")->orWhere('name', 'LIKE', "%{$query}%")->get();

        $output = '<ul class="dropdown-menu" style="display:block; position:relative;width: 100%;">';
            foreach($venues as $row)
                {
                $output .= '
                <li lat='.$row->latitude.' lng='.$row->longitude.' vid='.$row->id.'><a href="javascript:void(0);">'.$row->name.'</a></li>
                ';
                }
        $output .= '</ul>';
        echo $output;
        }
    }
    public function addvenue()
    {
        return view('add-new-venue');
    }
    public function submitVenue(Request $req)
    {        
        $req->validate([

            'location' => 'required',
            'name' => 'required',
            'address' => 'required',
            'postcode' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',

        ]);
        
        $slug = str_slug($req['name'], "-");
        MstVenue::create([
            'location' => $req['location'],
            'name' => $req['name'],
            'slug' => $slug,
            'address' => $req['address'],
            'description' => $req['description'],
            'postcode' => $req['postcode'],
            'latitude' => $req['latitude'],
            'longitude' => $req['longitude'],
        ]);
        return redirect('add-venue')->with('success', 'Venue created successfully.');
    }
}
