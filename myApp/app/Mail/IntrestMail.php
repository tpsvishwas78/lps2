<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class IntrestMail extends Mailable
{
    use Queueable, SerializesModels;
    public $intrestdata;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($intrestdata)
    {
        $this->intrestdata = $intrestdata;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.intrestmail', array('intrestdata', $this->intrestdata));
    }
}
