<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InviteMail extends Mailable
{
    use Queueable, SerializesModels;
    public $invitedata;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($invitedata)
    {
        $this->invitedata = $invitedata;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.invitemail', array('invitedata', $this->invitedata));
    }
}
