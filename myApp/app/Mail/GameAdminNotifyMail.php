<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class GameAdminNotifyMail extends Mailable
{
    use Queueable, SerializesModels;
    public $gamedata;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($gamedata)
    {
        $this->gamedata = $gamedata;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.gameadminmail', array('gamedata', $this->gamedata));
    }
}
