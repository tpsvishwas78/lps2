<?php

namespace App;

use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail as MustVerifyEmailContract;

class User extends Authenticatable implements MustVerifyEmailContract {

    use HasApiTokens,
        Notifiable,
        SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * 
     */
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'first_name', 'last_name', 'username', 'email', 'password', 'user_role', 'phone_number', 'facebook_id', 'profile_img', 'active', 'activation_token','email_verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'activation_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];

    public function addNew($input) {
        $check = static::where('facebook_id', $input['facebook_id'])->first();


        if (is_null($check)) {
            return static::create($input);
        }

        return $check;
    }

}
