<?php
require_once 'admin.php';
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index')->name('');

Auth::routes(['verify' => true]);

Route::group(['middleware'=>['UserAuth', 'PreventBackHistory']], function(){ 
    Route::get('/sendnotification', 'StartGameController@sendNotification')->name('sendNotification');
    Route::get('/startgame/{id?}', 'StartGameController@index')->name('startgame');
    Route::post('/creategame', 'StartGameController@creategame')->name('creategame');
    Route::get('/gamedeletegalleryimage/{id?}', 'StartGameController@gamedeletegalleryimage')->name('gamedeletegalleryimage');
    Route::get('/checkdate', 'StartGameController@checkdate')->name('checkdate');
    
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('/payment', 'DashboardController@payment')->name('payment');
    Route::get('/venues', 'DashboardController@venues')->name('venues');
    Route::post('/venue/follow', 'DashboardController@follow')->name('follow');
    Route::get('/games', 'GameController@index')->name('games');

    Route::get('/advertise-your-session', 'AdvertiseYourSessionController@index')->name('advertise-your-session');
    Route::get('/your-booking', 'AdvertiseYourSessionController@yourbooking')->name('yourbooking');
    Route::get('/your-session', 'AdvertiseYourSessionController@yoursession')->name('yoursession');
    Route::post('/submitBooking', 'AdvertiseYourSessionController@submitBooking')->name('submitBooking');
    Route::post('/submitSession', 'AdvertiseYourSessionController@submitSession')->name('submitSession');

    Route::post('/notifyread', 'NotificationController@notifyread')->name('notifyread');

    //world pay routes
Route::get('/paynow/{slug?}/{id?}', 'PaynowController@index')->name('index');
Route::post('/charge', 'PaynowController@charge')->name('charge');
    
});

Route::get('auth/facebook', 'Auth\FacebookController@redirectToFacebook');
Route::get('auth/facebook/callback', 'Auth\FacebookController@handleFacebookCallback');

Route::post('/venue/autocomplete', 'VenueController@autocomplete')->name('autocomplete');

Route::get('/add-venue', 'VenueController@addvenue')->name('addvenue');
Route::post('/submitVenue', 'VenueController@submitVenue')->name('submitVenue');

Route::get('/profile', 'ProfileController@index')->name('profile');
Route::get('/profile/edit', 'ProfileController@edit')->name('edit');
Route::post('/profile/updateprofile', 'ProfileController@updateprofile')->name('updateprofile');
Route::post('/profile/changepassword', 'ProfileController@changepassword')->name('changepassword');
Route::post('/profile/setnotify', 'ProfileController@setnotify')->name('setnotify');
Route::post('/profile/addlocation', 'ProfileController@addlocation')->name('addlocation');

Route::get('/contactus', 'ContactUsController@index')->name('contactus');
Route::get('/sendmail', 'ContactUsController@sendmail')->name('sendmail');
Route::post('/submitContact', 'ContactUsController@submitContact')->name('submitContact');

Route::get('/games/{slug?}/{id?}', 'GameController@gamedetail')->name('gamedetail');
Route::post('/joingame', 'GameController@joingame')->name('joingame');
Route::post('/leavegame', 'GameController@leavegame')->name('leavegame');
Route::post('/gamecomment', 'GameController@gamecomment')->name('gamecomment');
Route::post('/usergameloadmore', 'GameController@usergameloadmore')->name('usergameloadmore');

Route::get('/searchgame', 'SearchGameController@index')->name('searchgame');
Route::get('/womengames', 'SearchGameController@woomengame')->name('woomengame');
Route::post('/woomenloadmore', 'SearchGameController@woomenloadmore')->name('woomenloadmore');
Route::post('/searchgameloadmore', 'SearchGameController@searchgameloadmore')->name('searchgameloadmore');

Route::get('/joinleague', 'JoinLeagueController@index')->name('joinleague');
Route::post('/joinleagueloadmore', 'JoinLeagueController@joinleagueloadmore')->name('joinleagueloadmore');

Route::get('/venue/{id?}', 'VenueController@index')->name('index');
Route::get('/kids-parties', 'ChildrenController@index')->name('children');
Route::get('/children-venues', 'ChildrenController@children_venues')->name('children_venues');
Route::get('/children-coaching', 'ChildrenController@children_coaching')->name('children_coaching');
Route::get('/kids-5-a-side-leagues', 'ChildrenController@kids_language')->name('kids_language');
Route::get('/children-detail/{id?}', 'ChildrenController@children_detail')->name('children_detail');
Route::get('/register-your-interest', 'ChildrenController@register_your_intrest')->name('register_your_intrest');
Route::get('/children/register-your-interest', 'ChildrenController@register_your_intrest')->name('children_register_your_intrest');
Route::post('/submitIntrest', 'ChildrenController@submitIntrest')->name('submitIntrest');
Route::get('/kids/{id?}', 'ChildrenController@kids_deayils')->name('kids_deayils');

Route::get('/team-building', 'CorporateController@team_building')->name('team_building');
Route::get('/blockbooking', 'CorporateController@blockbooking')->name('blockbooking');
Route::post('/submitblockboking', 'CorporateController@submitblockboking')->name('submitblockboking');
Route::post('/submitgetinvolved', 'CorporateController@submitgetinvolved')->name('submitgetinvolved');
//Route::get('/sports-tournament', 'CorporateController@one_day_tournament')->name('one_day_tournament');
//Route::get('/corporate-detail', 'CorporateController@corporate_detail')->name('corporate_detail');
Route::get('/get-involved', 'CorporateController@get_involved')->name('get_involved');
Route::get('/corporate-sports-days', 'CorporateController@corporate_sports_days')->name('corporate_sports_days');
Route::get('/inflatable-fun-days', 'CorporateController@inflatable_fun_days')->name('inflatable_fun_days');
Route::get('/its-a-knockout-fun-days', 'CorporateController@its_a_knockout_fun_days')->name('its_a_knockout_fun_days');
Route::get('/office-summer-party', 'CorporateController@office_summer_party')->name('office_summer_party');
Route::get('/office-christmas-party', 'CorporateController@office_christmas_party')->name('office_christmas_party');
Route::get('/corporate', 'CorporateController@corporate')->name('corporate');
Route::get('/corporate/{id?}', 'CorporateController@corporate_detail')->name('corporate_detail');

Route::get('/sports-tournaments', 'SportsTurnamentsController@index')->name('sports_tournaments');
Route::get('/sports-tournaments/{id?}', 'SportsTurnamentsController@sports_tournaments_detail')->name('sports_tournaments_detail');

Route::get('/worldpay', 'GameController@worldpay')->name('worldpay');
Route::post('/sendinvitation', 'GameController@sendinvitation')->name('sendinvitation');

Route::get('/pages/{id?}', 'PagesController@index')->name('index');



Route::get('/worldpay', function () {
    return view('vendor/alvee/worldpay');
});



