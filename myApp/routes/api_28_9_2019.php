<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::group(['prefix' => 'auth'], function () {
  Route::post('login', 'Api\AuthController@login');
  Route::post('signup', 'Api\AuthController@signup');
  Route::get('signup/activate/{token}', 'Api\AuthController@signupActivate');

  Route::group(['middleware' => 'auth:api'], function () {
    Route::get('logout', 'Api\AuthController@logout');
    Route::get('user', 'Api\AuthController@user');
    Route::get('games', 'Api\GamesController@allgames');
    Route::get('venues', 'Api\VenueController@allvenue');
    Route::get('mygames', 'Api\GamesController@myGames');
    Route::get('myvenues', 'Api\VenueController@myvenues');
    Route::post('profileupdate', 'Api\AuthController@profileupdate');
    Route::post('changepassword', 'Api\AuthController@changepassword');
    Route::post('notifiupdate', 'Api\AuthController@notifiupdate');
    Route::get('allmessage', 'Api\MessageController@allmessage');
    Route::post('sendmessage', 'Api\MessageController@sendmessage');
    Route::post('readmessage', 'Api\MessageController@readmessage');
    Route::post('usercomments', 'Api\CommentsController@usercomments');
    Route::get('staticpages', 'Api\PagesController@staticpages');
    Route::post('tokens', 'Api\GamesController@token');
    Route::post('joingame', 'Api\GamesController@joingame');
  });
});

Route::get('profile/edit', 'HomeController@edit');
