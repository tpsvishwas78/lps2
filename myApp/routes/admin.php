<?php

Route::group(['middleware' => 'AdminLoginCheck'], function(){
    Route::get('/admin/login', 'Admin\LoginController@index')->name('admin/login');
    Route::post('/admin/loginSubmit', 'Admin\LoginController@loginSubmit')->name('admin/loginSubmit');
});

Route::group(['middleware'=>['AdminAuth', 'PreventBackHistory']], function(){ 
    Route::get('/admin/logout', 'Admin\LoginController@logout')->name('admin/logout');
    Route::get('/admin/dashboard', 'Admin\DashboardController@index')->name('admin/dashboard');
    Route::get('/admin/profile', 'Admin\DashboardController@profile')->name('admin/profile');
    Route::post('/admin/updateprofile', 'Admin\DashboardController@updateprofile')->name('admin/updateprofile');
    Route::post('/admin/changepassword', 'Admin\DashboardController@changepassword')->name('admin/changepassword');
    Route::get('/admin/allusers', 'Admin\UserController@index')->name('admin/allusers');
    Route::get('/admin/allvenues', 'Admin\VenueController@index')->name('admin/allvenues');
    Route::get('/admin/userstatus/{id?}', 'Admin\UserController@userstatus')->name('admin/userstatus');
    Route::get('/admin/addvenue/{id?}', 'Admin\VenueController@addvenue')->name('admin/addvenue');
    Route::post('/admin/submitVenue', 'Admin\VenueController@submitVenue')->name('admin/submitVenue');
    Route::get('/admin/venuedelete/{id?}', 'Admin\VenueController@venuedelete')->name('admin/venuedelete');
    Route::get('/admin/venuestatus/{id?}', 'Admin\VenueController@venuestatus')->name('admin/venuestatus');
    Route::get('/admin/allarea', 'Admin\IntrestAreaController@index')->name('admin/allarea');
    Route::get('/admin/addarea/{id?}', 'Admin\IntrestAreaController@addarea')->name('admin/addarea');
    Route::post('/admin/submitArea', 'Admin\IntrestAreaController@submitArea')->name('admin/submitArea');
    Route::get('/admin/areadelete/{id?}', 'Admin\IntrestAreaController@areadelete')->name('admin/areadelete');
    Route::get('/admin/areastatus/{id?}', 'Admin\IntrestAreaController@areastatus')->name('admin/areastatus');
    Route::get('/admin/allsports', 'Admin\SportsController@index')->name('admin/allsports');
    Route::get('/admin/addsport/{id?}', 'Admin\SportsController@addsport')->name('admin/addsport');
    Route::post('/admin/submitSport', 'Admin\SportsController@submitSport')->name('admin/submitSport');
    Route::get('/admin/sportdelete/{id?}', 'Admin\SportsController@sportdelete')->name('admin/sportdelete');
    Route::get('/admin/sportstatus/{id?}', 'Admin\SportsController@sportstatus')->name('admin/sportstatus');
    Route::get('/admin/allhomebanners', 'Admin\HomeCmsController@index')->name('admin/allhomebanners');
    Route::get('/admin/addhomebanners/{id?}', 'Admin\HomeCmsController@addhomebanners')->name('admin/addhomebanners');
    Route::post('/admin/submitHomeBanner', 'Admin\HomeCmsController@submitHomeBanner')->name('admin/submitHomeBanner');
    Route::get('/admin/bannerstatus/{id?}', 'Admin\HomeCmsController@bannerstatus')->name('admin/bannerstatus');
    Route::get('/admin/bannerdelete/{id?}', 'Admin\HomeCmsController@bannerdelete')->name('admin/bannerdelete');
    Route::get('/admin/homeaboutus', 'Admin\HomeCmsController@homeaboutus')->name('admin/homeaboutus');
    Route::post('/admin/submitHomeAbout', 'Admin\HomeCmsController@submitHomeAbout')->name('admin/submitHomeAbout');
    Route::get('/admin/commonvideo', 'Admin\HomeCmsController@commonvideo')->name('admin/commonvideo');
    Route::post('/admin/submitCommonVideo', 'Admin\HomeCmsController@submitCommonVideo')->name('admin/submitCommonVideo');

    Route::get('/admin/allbasicfeatures', 'Admin\HomeCmsController@allbasicfeatures')->name('admin/allbasicfeatures');
    Route::get('/admin/addbasicfeatures/{id?}', 'Admin\HomeCmsController@addbasicfeatures')->name('admin/addbasicfeatures');
    Route::post('/admin/submitBasicFeatures', 'Admin\HomeCmsController@submitBasicFeatures')->name('admin/submitBasicFeatures');
    Route::get('/admin/basicfeaturestatus/{id?}', 'Admin\HomeCmsController@basicfeaturestatus')->name('admin/basicfeaturestatus');
    Route::get('/admin/basicfeaturedelete/{id?}', 'Admin\HomeCmsController@basicfeaturedelete')->name('admin/basicfeaturedelete');

    Route::get('/admin/allsliders', 'Admin\QuickLinkSliderController@index')->name('admin/allsliders');
    Route::get('/admin/addslider/{id?}', 'Admin\QuickLinkSliderController@addslider')->name('admin/addslider');
    Route::post('/admin/submitHomeSlider', 'Admin\QuickLinkSliderController@submitHomeSlider')->name('admin/submitHomeSlider');
    Route::get('/admin/sliderstatus/{id?}', 'Admin\QuickLinkSliderController@sliderstatus')->name('admin/sliderstatus');
    Route::get('/admin/sliderdelete/{id?}', 'Admin\QuickLinkSliderController@sliderdelete')->name('admin/sliderdelete');
    Route::get('/admin/sitesetting', 'Admin\SiteSettingController@index')->name('admin/sitesetting');
    Route::post('/admin/submitSiteSetting', 'Admin\SiteSettingController@submitSiteSetting')->name('admin/submitSiteSetting');

    Route::get('/admin/add-children-party-category/{id?}', 'Admin\ChildrenParty@addCategory')->name('admin/addCategory');
    Route::post('/admin/submit-children-party-category/{id?}', 'Admin\ChildrenParty@submitAddCategory')->name('admin/addCategory');
    Route::post('/admin/submitAddPartyVenue', 'Admin\ChildrenParty@submitAddPartyVenue')->name('admin/submitAddPartyVenue');
    
    Route::get('/admin/all-children-party-category', 'Admin\ChildrenParty@showAllChildCategory')->name('admin/showAllChildCategory');
    Route::get('/admin/children-party-category-change-status/{id?}', 'Admin\ChildrenParty@category_change_status')->name('admin/children-party-category-change_status');
    Route::get('/admin/delete-children-party-category/{id?}', 'Admin\ChildrenParty@delete_category')->name('admin/children-party-category-delete');
    Route::get('/admin/addfacilities/{id?}', 'Admin\ChildrenParty@addfacilities')->name('admin/addfacilities');
    Route::get('/admin/allfacilities', 'Admin\ChildrenParty@allfacilities')->name('admin/allfacilities');
    Route::post('/admin/submitFacilities', 'Admin\ChildrenParty@submitFacilities')->name('admin/submitFacilities');
    Route::get('/admin/facilitiesdelete/{id?}', 'Admin\ChildrenParty@facilitiesdelete')->name('admin/facilitiesdelete');
    Route::get('/admin/facilitiestatus/{id?}', 'Admin\ChildrenParty@facilitiestatus')->name('admin/facilitiestatus');
    Route::get('/admin/add-children-party/{id?}', 'Admin\ChildrenParty@addParty')->name('admin/addPartyVenue');
    Route::get('/admin/all-children-party', 'Admin\ChildrenParty@allParty')->name('admin/allParty');
    Route::get('/admin/partystatus/{id?}', 'Admin\ChildrenParty@partystatus')->name('admin/partystatus');
    Route::get('/admin/partydelete/{id?}', 'Admin\ChildrenParty@partydelete')->name('admin/partydelete');
    Route::post('/admin/deletegalleryimage/{id?}', 'Admin\ChildrenParty@deletegalleryimage')->name('admin/deletegalleryimage');

    Route::get('/admin/add-kids-party-details/{id?}', 'Admin\ChildrenParty@kids_party')->name('admin/kids_party');
    Route::get('/admin/add-party-details/{id?}', 'Admin\ChildrenParty@kids_party_details')->name('admin/kids_party_details');
    Route::get('/admin/all-kids-party-details', 'Admin\ChildrenParty@all_kids_party')->name('admin/all_kids_party');
    Route::get('/admin/choose-kids-party-layout', 'Admin\ChildrenParty@choose_layout')->name('admin/choose_layout');
    Route::post('/admin/submitKidsDeials', 'Admin\ChildrenParty@submitKidsDeials')->name('admin/submitKidsDeials');
    Route::post('/admin/submitKidsDetailLayoutTwo', 'Admin\ChildrenParty@submitKidsDetailLayoutTwo')->name('admin/submitKidsDetailLayoutTwo');
    Route::post('/admin/deletekidsgalleryimage/{id?}', 'Admin\ChildrenParty@deletekidsgalleryimage')->name('admin/deletekidsgalleryimage');
    Route::post('/admin/deletekidsvideo/{id?}', 'Admin\ChildrenParty@deletekidsvideo')->name('admin/deletekidsvideo');
    Route::get('/admin/addkidsblog/{id?}', 'Admin\ChildrenParty@addkidsblog')->name('admin/addkidsblog');
    Route::get('/admin/allkidsblog', 'Admin\ChildrenParty@allkidsblog')->name('admin/allkidsblog');
    
    Route::get('/admin/kidstatus/{id?}', 'Admin\ChildrenParty@kidstatus')->name('admin/kidstatus');
    Route::get('/admin/kiddelete/{id?}', 'Admin\ChildrenParty@kiddelete')->name('admin/kiddelete');
    Route::post('/admin/submitKidsBlogs', 'Admin\ChildrenParty@submitKidsBlogs')->name('admin/submitKidsBlogs');
    Route::get('/admin/deletekidsblog/{id?}', 'Admin\ChildrenParty@deletekidsblog')->name('admin/deletekidsblog');
    
    Route::get('/admin/listbooking', 'Admin\AdvertiseYourSessionController@listbooking')->name('admin/listbooking');
    Route::get('/admin/listsession', 'Admin\AdvertiseYourSessionController@listsession')->name('admin/listsession');
    Route::get('/admin/bookingdetail/{id?}', 'Admin\AdvertiseYourSessionController@bookingdetail')->name('admin/bookingdetail');
    Route::get('/admin/sessiondetail/{id?}', 'Admin\AdvertiseYourSessionController@sessiondetail')->name('admin/sessiondetail');

    Route::get('/admin/manageblockbooking', 'Admin\AdvertiseYourSessionController@manageblockbooking')->name('admin/manageblockbooking');
    Route::get('/admin/blockbookingdetail/{id?}', 'Admin\AdvertiseYourSessionController@blockbookingdetail')->name('admin/blockbookingdetail');

    Route::get('/admin/managegetinvolved', 'Admin\AdvertiseYourSessionController@managegetinvolved')->name('admin/managegetinvolved');
    Route::get('/admin/getinvolveddetail/{id?}', 'Admin\AdvertiseYourSessionController@getinvolveddetail')->name('admin/getinvolveddetail');
    
    Route::get('/admin/addmenu/{id?}', 'Admin\MenuAndPagesController@addmenu')->name('admin/addmenu');
    Route::post('/admin/submitMenu', 'Admin\MenuAndPagesController@submitMenu')->name('admin/submitMenu');
    Route::get('/admin/allmenu', 'Admin\MenuAndPagesController@allmenu')->name('admin/allmenu');
    Route::get('/admin/menudelete/{id?}', 'Admin\MenuAndPagesController@menudelete')->name('admin/menudelete');
    Route::get('/admin/menustatus/{id?}', 'Admin\MenuAndPagesController@menustatus')->name('admin/menustatus');

    Route::get('/admin/addpage/{id?}', 'Admin\MenuAndPagesController@addpage')->name('admin/addpage');
    Route::post('/admin/submitPage', 'Admin\MenuAndPagesController@submitPage')->name('admin/submitPage');
    Route::get('/admin/allpage', 'Admin\MenuAndPagesController@allpage')->name('admin/allpage');
    Route::get('/admin/pagedelete/{id?}', 'Admin\MenuAndPagesController@pagedelete')->name('admin/pagedelete');

    Route::get('/admin/addgame/{id?}', 'Admin\GamesController@addgame')->name('admin/addgame');
    Route::get('/admin/alladmingame', 'Admin\GamesController@alladmingame')->name('admin/alladmingame');
    Route::get('/admin/allusergame', 'Admin\GamesController@allusergame')->name('admin/allusergame');
    Route::post('/admin/creategame', 'Admin\GamesController@creategame')->name('admin/creategame');
    Route::get('/admin/gamedelete/{id?}', 'Admin\GamesController@gamedelete')->name('admin/gamedelete');
    Route::get('/admin/gamestatus/{id?}', 'Admin\GamesController@gamestatus')->name('admin/gamestatus');
    Route::post('/admin/gamedeletegalleryimage/{id?}', 'Admin\GamesController@gamedeletegalleryimage')->name('admin/gamedeletegalleryimage');

    Route::get('/admin/addcorporatecatgory/{id?}', 'Admin\CorporateController@addcorporatecatgory')->name('admin/addcorporatecatgory');
    Route::get('/admin/allcorporatecatgory', 'Admin\CorporateController@allcorporatecatgory')->name('admin/allcorporatecatgory');
    Route::post('/admin/submitCorporatecatgory', 'Admin\CorporateController@submitCorporatecatgory')->name('admin/submitCorporatecatgory');
    Route::get('/admin/corporatecatgorydelete/{id?}', 'Admin\CorporateController@corporatecatgorydelete')->name('admin/corporatecatgorydelete');
    Route::get('/admin/corporatecatgorystatus/{id?}', 'Admin\CorporateController@corporatecatgorystatus')->name('admin/corporatecatgorystatus');

    Route::get('/admin/addcorporatedetails/{id?}', 'Admin\CorporateController@addcorporatedetails')->name('admin/addcorporatedetails');
    Route::post('/admin/submitCorporate', 'Admin\CorporateController@submitCorporate')->name('admin/submitCorporate');
    Route::get('/admin/allcorporatedetails', 'Admin\CorporateController@allcorporatedetails')->name('admin/allcorporatedetails');
    Route::get('/admin/addcorporatedetailsdelete/{id?}', 'Admin\CorporateController@addcorporatedetailsdelete')->name('admin/addcorporatedetailsdelete');
    Route::post('/admin/deletecorporategalleryimage/{id?}', 'Admin\CorporateController@deletecorporategalleryimage')->name('admin/deletecorporategalleryimage');
    Route::post('/admin/deletecorporatevideo/{id?}', 'Admin\CorporateController@deletecorporatevideo')->name('admin/deletecorporatevideo');

    Route::get('/admin/addcorporateblog/{id?}', 'Admin\CorporateController@addcorporateblog')->name('admin/addcorporateblog');
    Route::get('/admin/allcorporateblog', 'Admin\CorporateController@allcorporateblog')->name('admin/allcorporateblog');
    Route::post('/admin/submitCorporateblog', 'Admin\CorporateController@submitCorporateblog')->name('admin/submitCorporateblog');
    Route::get('/admin/corporateblogdelete/{id?}', 'Admin\CorporateController@corporateblogdelete')->name('admin/corporateblogdelete');
    Route::post('/admin/deletecorporatefaq/{id?}', 'Admin\CorporateController@deletecorporatefaq')->name('admin/deletecorporatefaq');

    // sports turnaments routes
    
    Route::get('/admin/addsportsturnamentscatgory/{id?}', 'Admin\SportsTurnamentsController@addsportsturnamentscatgory')->name('admin/addsportsturnamentscatgory');
    Route::get('/admin/allsportsturnamentscatgory', 'Admin\SportsTurnamentsController@allsportsturnamentscatgory')->name('admin/allsportsturnamentscatgory');
    Route::post('/admin/submitsportsturnamentscatgory', 'Admin\SportsTurnamentsController@submitsportsturnamentscatgory')->name('admin/submitsportsturnamentscatgory');
    Route::get('/admin/sportsturnamentscatgorydelete/{id?}', 'Admin\SportsTurnamentsController@sportsturnamentscatgorydelete')->name('admin/sportsturnamentscatgorydelete');
    Route::get('/admin/sportsturnamentscatgorystatus/{id?}', 'Admin\SportsTurnamentsController@sportsturnamentscatgorystatus')->name('admin/sportsturnamentscatgorystatus');

    Route::get('/admin/addsportsturnamentsdetails/{id?}', 'Admin\SportsTurnamentsController@addsportsturnamentsdetails')->name('admin/addsportsturnamentsdetails');
    Route::post('/admin/submitsportsturnaments', 'Admin\SportsTurnamentsController@submitsportsturnaments')->name('admin/submitsportsturnaments');
    Route::get('/admin/allsportsturnamentsdetails', 'Admin\SportsTurnamentsController@allsportsturnamentsdetails')->name('admin/allsportsturnamentsdetails');
    Route::get('/admin/addsportsturnamentsdetailsdelete/{id?}', 'Admin\SportsTurnamentsController@addsportsturnamentsdetailsdelete')->name('admin/addsportsturnamentsdetailsdelete');
    Route::post('/admin/deletesportsturnamentsgalleryimage/{id?}', 'Admin\SportsTurnamentsController@deletesportsturnamentsgalleryimage')->name('admin/deletesportsturnamentsgalleryimage');
    Route::post('/admin/deletesportsturnamentsvideo/{id?}', 'Admin\SportsTurnamentsController@deletesportsturnamentsvideo')->name('admin/deletesportsturnamentsvideo');

    Route::get('/admin/addsportsturnamentsblog/{id?}', 'Admin\SportsTurnamentsController@addsportsturnamentsblog')->name('admin/addsportsturnamentsblog');
    Route::get('/admin/allsportsturnamentsblog', 'Admin\SportsTurnamentsController@allsportsturnamentsblog')->name('admin/allsportsturnamentsblog');
    Route::post('/admin/submitsportsturnamentsblog', 'Admin\SportsTurnamentsController@submitsportsturnamentsblog')->name('admin/submitsportsturnamentsblog');
    Route::get('/admin/sportsturnamentsblogdelete/{id?}', 'Admin\SportsTurnamentsController@sportsturnamentsblogdelete')->name('admin/sportsturnamentsblogdelete');
    Route::post('/admin/deletesportsturnamentsfaq/{id?}', 'Admin\SportsTurnamentsController@deletesportsturnamentsfaq')->name('admin/deletesportsturnamentsfaq');
    
    Route::get('/admin/joinleague', 'Admin\JoinLeagueController@joinleague')->name('admin/joinleague');
    Route::post('/admin/submitJoinLeague', 'Admin\JoinLeagueController@submitJoinLeague')->name('admin/submitJoinLeague');
    Route::get('/admin/addjoinleagueblock/{id?}', 'Admin\JoinLeagueController@addjoinleagueblock')->name('admin/addjoinleagueblock');
    Route::get('/admin/alljoinleagueblock', 'Admin\JoinLeagueController@alljoinleagueblock')->name('admin/alljoinleagueblock');
    Route::post('/admin/submitJoinLeagueBlock', 'Admin\JoinLeagueController@submitJoinLeagueBlock')->name('admin/submitJoinLeagueBlock');
    Route::get('/admin/joinleagueblockdelete/{id?}', 'Admin\JoinLeagueController@joinleagueblockdelete')->name('admin/joinleagueblockdelete');

    Route::get('/admin/payments', 'Admin\PaymentController@index')->name('admin/payments');    
    Route::post('/admin/payments', 'Admin\PaymentController@index')->name('admin/payments');    
    
    
    
});






?>