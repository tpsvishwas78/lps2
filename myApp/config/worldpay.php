<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Default Server is sandbox
    |--------------------------------------------------------------------------
    |
    | This option controls the server "sandbox" and "live" that will be used on
    | requests.
    |
    | Supported: "sandbox", "live"
    |
    */

    'server' => "sandbox",

    /*
    |--------------------------------------------------------------------------
    | Sandbox Key
    |--------------------------------------------------------------------------
    |
    | Sandbox account credentials from developer portal set the service and
    | client key
    |
    */

    'sandbox' => array(
        'service' => 'T_S_1c1d5e33-4cd7-499b-b921-c0c01752f857',
        'client'  => 'T_C_2f4c3d06-2257-4ece-b8ce-6ef6a35b8c93',
    ),

    /*
    |--------------------------------------------------------------------------
    | Live Key
    |--------------------------------------------------------------------------
    |
    | live account credentials from developer portal set the service and
    | client key
    |
    */

    'live' => array(
        'service' => 'L_S_fd54b46b-d22a-40e3-9b2c-c8ec71ed7620',
        'client'  => 'L_C_2d50b2d1-5c0e-4dff-ab01-52268d549a44',
    ),

];
