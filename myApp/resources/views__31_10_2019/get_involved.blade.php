@extends('layouts.master')
@section('content')

<!--BANNER-SECTION-START-->
<section class="banner-detail">
		<div  class="hedding-children">
       <h1>get involved </h1>
       </div>
    </section>
	<!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
	<!--available-at-section-start-->
	<section class="available-at">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="search-fields">
						<form>
							<div class="select-coaching">
									<select>
											<option>Coaching</option>
											<option>Kids 5 a side leagues</option>
									</select>
							</div>
							<div class="selectcity-town">
								<input type="text" name="" placeholder="SEARCH BY TOWN/CITY/POSTCODE">
							</div>
							<button  class="search"><img src="{{ url('') }}/images/icons/search.png">SEARCH </button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--available-at-section-end-->
	<div class="clearfix"></div>
	<!--BOOKING-SECTION-START-->
	<section class="form-contact">
		<div class="container">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<div class="heading-contact-form">
						<h1>Get Involved</h1>
						<p>Looking to play sports. Well lets get you started by filling in the form below</p>
					</div>
					@if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
                @php
                    Session::forget('success');
				@endphp
				</div>
				@endif
				<div class="contact-us-form">
					<form class="comment" action="{{ url('') }}/submitgetinvolved" method="POST" enctype="multipart/form-data">
						@csrf
						<div class="col-sm-6 p-0">
								<div class="form-group">
									<label class="control-label">Firs Name</label>
									<input type="text" name="first_name"  class="form-control" placeholder="Enter Your First  Name" value="{{ old('first_name') }}">
									@if ($errors->has('first_name'))
							<span class="text-danger">{{ $errors->first('first_name') }}</span>
						@endif
									</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label class="control-label">Last Name</label>
									<input type="text" name="last_name" class="form-control" placeholder="Enter Your Last Name" value="{{ old('last_name') }}">
									@if ($errors->has('last_name'))
							<span class="text-danger">{{ $errors->first('last_name') }}</span>
						@endif
									</div>
							</div>
							<div class="col-sm-6 p-0">
								<div class="form-group">
									<label class="control-label">Your Email</label>
									<input type="text" name="email" class="form-control" placeholder="Enter Your Email" value="{{ old('email') }}">
									@if ($errors->has('email'))
									<span class="text-danger">{{ $errors->first('email') }}</span>
								@endif	
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label class="control-label">Mobile Number</label>
									<input type="text" name="mobile_number" class="form-control" placeholder="Enter Your Mobile Number" value="{{ old('mobile_number') }}">
									@if ($errors->has('mobile_number'))
									<span class="text-danger">{{ $errors->first('mobile_number') }}</span>
								@endif
									</div>
							</div>
							<div class="col-sm-6 p-0">
								<div class="form-group">
									<label class="control-label">Venue</label>
									<input type="text" name="venue_name" class="form-control" placeholder="Select vanue" autocomplete="off" id="venue" style="margin-bottom: 0px;">
									<input type="hidden" id="venue_id" name="venue">
									@if ($errors->has('venue'))
									<span class="text-danger">{{ $errors->first('venue') }}</span>
								@endif
								<div id="venueList">
								</div>
									</div>
							</div>
					<div class="col-sm-6">
						
							<div class="form-group datepicker">
									<label class="control-label">Available Days</label>
									<input autocomplete="off" type="text" name="date" class="form-control" placeholder="Available Days" value="{{ old('date') }}">
									@if ($errors->has('date'))
									<span class="text-danger">{{ $errors->first('date') }}</span>
								@endif	
								</div>
					</div>
					<div class="col-sm-6 p-0">
							<div class="form-group timepicker">
								<label class="control-label">Time</label>
								<input type="text" autocomplete="off" name="time" class="form-control" placeholder="Select Your Time">
								@if ($errors->has('time'))
								<span class="text-danger">{{ $errors->first('time') }}</span>
							@endif
								</div>
						</div>
						<div class="col-sm-6">
								<div class="form-group">
									<label class="control-label">Sport</label>
									<select name="sport" id="sport" class="form-control">
										<option value="">Select</option>
										@foreach ($sports as $sport)
										<option value="{{ $sport->id }}">{{ $sport->name }}</option>
										@endforeach
										
									</select>
									@if ($errors->has('sport'))
									<span class="text-danger">{{ $errors->first('sport') }}</span>
								@endif	
								</div>
							</div>
					<div class="col-sm-12 pl-0">
						<div class="form-group">
							<label class="control-label">Comments</label>
							<textarea class="form-control textarea" name="comments" placeholder="Enter Your Comments"></textarea>
							@if ($errors->has('comments'))
									<span class="text-danger">{{ $errors->first('comments') }}</span>
							@endif
						</div>
					</div>
					<div class="captcha contact-captcha">
							{!! NoCaptcha::renderJs() !!}
							{!! NoCaptcha::display() !!}
							@if ($errors->has('g-recaptcha-response'))
							<span class="text-danger">{{ $errors->first('g-recaptcha-response') }}</span>
						@endif
				</div>
				<div style="text-align: center;">
					<button class="submit">SUBMIT</button>
				</div>
				</form>
				</div>
				</div>
			</div>
		</div>
	</section>
	<!--BOOKING-SECTION-END-->
	<script>
		$(document).ready(function(){
				
				$('#venue').keyup(function(){ 
					   var query = $(this).val();
					   //console.log('query',query);
					   if(query != '')
					   {
						var _token = $('input[name="_token"]').val();
						$.ajax({
						 url:"{{ url('') }}/venue/autocomplete",
						 method:"POST",
						 data:{query:query, _token:_token},
						 success:function(data){
							 //console.log('data',data);
									$('#venueList').fadeIn();  
								   $('#venueList').html(data);
						 }
						});
					   }
				   });
			   
				   $(document).on('click', '.dropdown-menu li', function(){  
					   $('#venue').val($(this).text());
					  
					   var vid=$(this).attr('vid');
					 
					   $('#venue_id').val(vid);
					   $('#venueList').fadeOut();
					 
				   });  
			   
			   });
		</script>
@endsection