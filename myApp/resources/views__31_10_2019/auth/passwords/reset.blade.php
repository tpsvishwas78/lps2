<?php
@$setting=\DB::table('site_setting')->first();
?>
@extends('layouts.master')

@section('content')
<!--SIGN-IN-PAGE-START-->
<section class="sign-in">
		<div class="left-pannel">
			<img src="{{ url('') }}/images/sign-in.jpg">
		</div>
		<div class="right-pannel">
			<div class="signin-top-section">
					<a href="{{ url('') }}">
				<img class="back-arrow-sign" src="{{ url('') }}/images/icons/back-arrow.png">
					</a>
				<div class="sign-in-logo">
						<a href="{{ url('') }}">
							<img src="{{ url('') }}/upload/images/{{ @$setting->header_logo }}">
						</a>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="sign-in-form">
				<h1>Reset Password</h1>
				<p class="tag-line-heading">Please choose a new password to finish signinig in.</p>
				<form method="POST" action="{{ route('password.update') }}">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="form-group">
                                <label class="control-label">Email</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus placeholder="Enter Your Email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
					<div class="form-group">
					<label class="control-label">New Password</label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password"  placeholder="Enter Your New Password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
					</div>
					<div class="form-group">
					<label class="control-label">Confirm Password</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password"  placeholder="Confirm Your Password">
					</div>
					
					<div class="sinin-buttons">
						<button type="submit" class="sign-in-here">CHANGE PASSWORD</button>
					</div>
				</form>
			</div>
		</div>
	</section>
	<!--SIGN-IN-PAGE-END-->
@endsection
