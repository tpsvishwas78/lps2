<?php
@$setting=\DB::table('site_setting')->first();
?>
@extends('layouts.master')

@section('content')
<!--SIGN-IN-PAGE-START-->
<section class="sign-in">
		<div class="left-pannel">
			<img src="{{ url('') }}/images/sign-in.jpg">
		</div>
		<div class="right-pannel">
			<div class="signin-top-section">
					<a href="{{ url('') }}">
				<img class="back-arrow-sign" src="{{ url('') }}/images/icons/back-arrow.png">
					</a>
				<div class="sign-in-logo">
						<a href="{{ url('') }}">
							<img src="{{ url('') }}/upload/images/{{ @$setting->header_logo }}">
						</a>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="sign-up-form">
				<h1>Create New Account</h1>
				<p class="tag-line-heading">Use your email to create account... It's free</p>
                <form method="POST" action="{{ route('register') }}">
                        @csrf
					<div class="form-group">
					<label class="control-label">First Name</label>
                    <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" required autocomplete="first_name" autofocus placeholder="Enter Your First Name">

                    @error('first_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
					</div>
					<div class="form-group">
					<label class="control-label">Last Name</label>
                    <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" required autocomplete="last_name" autofocus  placeholder="Enter Your Last Name">

                                @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
					</div>
					<div class="form-group">
					<label class="control-label">Email Address</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Enter Your Email Address">

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
					</div>
					<div class="form-group">
					<label class="control-label">Mobile Number</label>
                    <input id="mobile_number" type="text" class="form-control @error('mobile_number') is-invalid @enderror" name="mobile_number" value="{{ old('mobile_number') }}" required autocomplete="mobile_number" placeholder="Enter Your Mobile Number">

                    @error('mobile_number')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
					</div>
					<div class="form-group last-field">
					<label class="control-label">Password</label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Enter Your Password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
									</span>
									<ul>
											<li style="color: #fff !important;">
											English uppercase characters (A – Z)
											</li>
											<li style="color: #fff !important;">
											English lowercase characters (a – z)
											</li>
											<li style="color: #fff !important;">
											Base 10 digits (0 – 9)
											</li>
											<li style="color: #fff !important;">
											Non-alphanumeric (For example: !,@, $, #, % etc)
											</li>
											<li style="color: #fff !important;">
											Unicode characters
											</li>
											</ul>
                                @enderror
					</div>
					
					<div class="tnc">
						<input class="styled-checkbox" id="styled-checkbox-1" required name="term_cond" type="checkbox" value="1">
    					<label for="styled-checkbox-1">I have read the <a target="_blank" href="{{ url('') }}/pages/terms-conditions" style="color:#fff;font-size: 1.4rem;">Terms & Conditions</a></label>
					</div>
					<div class="captcha">
					   {!! NoCaptcha::renderJs() !!}
                        {!! NoCaptcha::display() !!}
                        @if ($errors->has('g-recaptcha-response'))
                        <span class="text-danger">{{ $errors->first('g-recaptcha-response') }}</span>
                    @endif
					</div>
					<div class="sinin-buttons">
						<button type="submit" class="sign-in-here">SIGN UP NOW</button>
					</div>
				</form>
				<div class="accont-create">
					<p class="no-account">Have an account.<a href="{{ route('login') }}"> Sign In?</a></p>
				</div>
			</div>
		</div>
	</section>
	<!--SIGN-IN-PAGE-END-->
@endsection
