@extends('layouts.master')
@section('content')
<?php
$setting=\DB::table('site_setting')->first();
?>
<div class="clearfix"></div>
	<!--BANNER-SECTION-START-->
	<section class="banner-detail" style="background: url({{ url('') }}/images/Office-Summer-Party.jpg);">
       <h1 class="hedding-detail">Office Summer Party</h1>
    </section>
	<!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
	<!--crousel--and-video-section-start-->
	<section class="corporate-sports-day">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="col-sm-8">
						<div class="left-vide0-section">
							<div class="video-detail">
								<img src="http://localhost/lps2/upload/images/1567776148.jpg">
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="right-side-crousel">
							<!--crousel-->
							<div class="header" id="cover">
						<div class="carousel slide" data-ride="carousel" id="carousel">
						<div class="carousel-inner">
						<div class="item">
						
						<img class="responsive" alt="umpires" src="http://localhost/lps2/upload/images/1567776148.jpg"></div>
						<div class="item active">
						
						<img class="responsive" alt="baseballd" src="images/office-chrimasassa.jpg">
						</div>
						<div class="item">
						
						<img class="responsive" alt="fallball5" src="images/kdakdkd.jpg">
						</div>
						<div class="item">
						
						<img class="responsive" alt="fallball4" src="images/office-chrimasassa.jpg">
						</div>
						
						</div>
						<div class="clearfix">
						<div class="carousel-link">
						<div class="thumb" data-slide-to="0" data-target="#carousel">
						<img class="responsive" alt="umpires" src="http://localhost/lps2/upload/images/1567776148.jpg"></div>
						<div class="thumb" data-slide-to="1" data-target="#carousel">
						<img class="responsive" alt="baseballd" src="images/office-chrimasassa.jpg">
						</div>
						<div class="thumb" data-slide-to="2" data-target="#carousel">
						<img class="responsive" alt="fallball5" src="images/kdakdkd.jpg">
						</div>
						<div class="thumb" data-slide-to="3" data-target="#carousel">
						<img class="responsive" alt="fallball4" src="images/office-chrimasassa.jpg">
						</div>
						
						</div>
						</div>
						</div>
						</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="childpara"> <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p></div>
				</div>
				</div>
				</div>
				</div>
			</div>
		</div>
	</section>
<!--crousel--and-video-section-end-->
<!--section-key-facts-->
<section class="sports-challanges">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="head-challange">
						<h1>Key Facts </h1><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
					</div>
					<div class="tab-design-function">
						 <div class="col-sm-12 p-0">
						 	<h1 class="whats-included">Whats Included</h1>
				        <div class="col-sm-5 pl-0">
				         <!-- required for floating -->
				          <!-- Nav tabs -->
				          <ul class="nav nav-tabs tabs-left sideways">
					
						<li class="active"><a href="#home-0" data-toggle="tab" aria-expanded="false">
							<div class="drop-down-icon-filter-corporate"></div>Lorem Ipsum Included</a>
						</li>
							<li class=""><a href="#home-1" data-toggle="tab" aria-expanded="false">
									<div class="drop-down-icon-filter-corporate"></div>Lorem Ipsum Included</a>
								</li>
								<li class=""><a href="#home-2" data-toggle="tab" aria-expanded="true">
									<div class="drop-down-icon-filter-corporate"></div>Lorem Ipsum Included</a>
								</li>
								<li class=""><a href="#home-3" data-toggle="tab" aria-expanded="true">
									<div class="drop-down-icon-filter-corporate"></div>Lorem Ipsum Included</a>
								</li>
											            
				          </ul>
				        </div>
				        <div class="col-sm-7">
				          <!-- Tab panes -->
				          <div class="tab-content">
																
				            <div class="tab-pane active" id="home-0">
				            	<div class="content-info-lorem">
				            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
				            	</div>
				            </div>
															
				            <div class="tab-pane" id="home-1">
				            	<div class="content-info-lorem">
				            		<p>Lorem2 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
				            	</div>
				            </div>
															
				            <div class="tab-pane" id="home-2">
				            <div class="content-info-lorem">
				            		<p>Lorem3 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
				            	</div>
				            </div>
				              <div class="tab-pane" id="home-3">
				            <div class="content-info-lorem">
				            		<p>Lorem4 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
				            	</div>
				            </div>
							
				          </div>
				        </div>

				        <div class="clearfix"></div>

				      </div>
					</div>
					</div>
			</div>
		</div>
	</section>
	<div class="clearfix"></div>
<!--section-key-facts--end-->
<section class="serach-result">
		<div class="container">
			<div class="row">
				<!--game-listing-start-->
				<div class="col-sm-12">
					
					<div class="listing-games">
						<div class="design-list">
							<div class="col-sm-9">
								<div class="info-game">
									<p>Champions Experience</p>
									
									<div class="dis-game">Experience a series of fun games and challenges for 90 minutes, before the children move inside for food and presentations </div>
									<div class="dis-game">Experience a series of fun games and challenges for 90 minutes, before the children move inside for food and presentations </div>
								</div>
							</div>
							<div class="col-sm-3 p-0">
							<div class="game-img">
								<img src="images/img-list3.png">
							</div>
							</div>
						</div>
					</div>
					<div class="listing-games">
						<div class="design-list">
							<div class="col-sm-9">
								<div class="info-game">
									<p>Football Party</p>
									<div class="dis-game">Experience a series of fun games and challenges for 90 minutes, before the children move inside for food and presentations </div>
									<div class="dis-game">Experience a series of fun games and challenges for 90 minutes, before the children move inside for food and presentations </div>
								</div>
							</div>
							<div class="col-sm-3 p-0">
							<div class="game-img">
								<img src="images/img-list2.png">
							</div>
							</div>
						</div>
					</div>
					<div class="listing-games">
						<div class="design-list">
							<div class="col-sm-9">
								<div class="info-game">
									<p>Champions Experience</p>
									
									<div class="dis-game">Experience a series of fun games and challenges for 90 minutes, before the children move inside for food and presentations </div>
									<div class="dis-game">Experience a series of fun games and challenges for 90 minutes, before the children move inside for food and presentations </div>
								</div>
							</div>
							<div class="col-sm-3 p-0">
							<div class="game-img">
								<img src="images/img-list4.png">
							</div>
							</div>
						</div>
					</div>
				</div>
				<!--game-listing-end-->
			</div>
		</div>
	</section>
	<script>
		function initAutocomplete() {
			var input = document.getElementById('pac-input');
			//var searchBox = new google.maps.places.SearchBox(input);
			var options = {
			types: ['(cities)'],
			componentRestrictions: {country: 'GB'}//UK only
			};
			var autocomplete = new google.maps.places.Autocomplete(input,options);
          
        }
	</script>
@endsection