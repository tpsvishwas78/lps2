@extends('layouts.usermaster')

@section('content')

<!--BANNER-SECTION-START-->
<section class="banner-detail">
        <h1 class="hedding-detail">Payments</h1>
     </section>
     <!--BANNER-SECTION-END-->
     <div class="clearfix"></div>
     <!--account-setting-section-start-->
     <section class="account-setting-content">
         <div class="container">
             <div class="row">
                 <div class="col-sm-12 p-0">
                     @include('layouts.sidebar')
                     <div class="col-sm-8">
                         <div class="top-payment-content">
                             <div class="col-sm-4">
                                 <div class="balance">
                                     <h2>£0.00</h2>
                                     <p>Current Balance</p>
                                 </div>
                             </div>
                             <div class="col-sm-4">
                                 <div class="balance">
                                     <h2>GBP</h2>
                                     <p>User currency</p>
                                 </div>
                             </div>
                             <div class="col-sm-4">
                                 <div class="balance">
                                     <h2>Edit</h2>
                                     <p>Payment methods</p>
                                 </div>
                             </div>
                         </div>
                         <div class="clearfix"></div>
                         <div class="ammount-box">
                             <h2>*Topup Amount</h2>
                             <div class="box-vailue">
                                 <p>0</p>
                             </div>
                         </div>
                         <div class="clearfix"></div>
                       <div class="payment-accordion">
                           <h2>Your Payment Details</h2>
                           <form class="bs-example" action="">
                               <div class="panel-group" id="accordion">
                                 <div class="panel panel-default">
                                   <div class="panel-heading">
                                     <h4 class="panel-title">
                                         <input type='radio' id='r11' name='occupation' value='Working' required />
                                         <label for='r11' >
                                           <span> Debit/Credit Card</span>
                                          
                                           <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"></a>
                                         </label>
                                          <ul>
                                               <li><img src="{{ url('') }}/images/icons/visa.png"></li>
                                             <li><img src="{{ url('') }}/images/icons/master.png"></li>
                                           </ul>
                                     </h4>
                                   </div>
                                   <div id="collapseOne" class="panel-collapse collapse in">
                                     <div class="panel-body">
                                      <div class="edit-content">
                                         <div class="col-sm-12 ">
                                             <div class="form-group">
                                                 <label class="control-label">Card Holder Name</label>
                                                 <input type="text" name="regular" class="form-control" placeholder="Enter Your Name">
                                                 </div>
                                         </div>
                                         <div class="col-sm-12">
                                             <div class="form-group">
                                                 <label class="control-label">Card Number</label>
                                                 <input type="text" name="regular" class="form-control" placeholder="Enter Your Card Number">
                                                 </div>
                                         </div>
                                         <div class="col-sm-6">
                                             <div class="form-group">
                                                 <label class="control-label">Expiration Date</label>
                                                 <input type="text" name="regular" class="form-control" placeholder="09/20">
                                                 </div>
                                         </div>
                                             <div class="col-sm-6">
                                             <div class="form-group">
                                                 <label class="control-label">CVV</label>
                                                 <input type="text" name="regular" class="form-control" placeholder="09/20">
                                                 <img class="tool-tip-icon" src="{{ url('') }}/images/icons/cvv.png">
                                                 </div>
                                         </div>
                                         
                                     </div>
                                     <p class="save-details"><img src="{{ url('') }}/images/icons/save-details.png">Save my details for future payments.</p>
                                     <p class="save-details">Is it safe? Yes, all sensitive information are securely stored via Stripe, certified to PCI Service Provider Level 1</p>
                                     <button class="make-payment">MAKE PAYMENT</button>
                                     </div>
                                   </div>
                                 </div>
                                 <div class="panel panel-default">
                                   <div class="panel-heading">
                                     <h4 class=panel-title>
                                         <input type='radio' id='r12' name='occupation' value='Not-Working' required /> 
                                         <label for='r12'>
                                           <span>Net Banking</span>
                                           
                                           <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"></a>
                                         </label>
                                     </h4>
                                   </div>
                                   <div id="collapseTwo" class="panel-collapse collapse">
                                     <div class="panel-body">
                                      <div class="edit-content">
                                         <div class="col-sm-12 ">
                                             <div class="form-group">
                                                 <label class="control-label">Card Holder Name</label>
                                                 <input type="text" name="regular" class="form-control" placeholder="Enter Your Name">
                                                 </div>
                                         </div>
                                         <div class="col-sm-12">
                                             <div class="form-group">
                                                 <label class="control-label">Card Number</label>
                                                 <input type="text" name="regular" class="form-control" placeholder="Enter Your Card Number">
                                                 </div>
                                         </div>
                                         <div class="col-sm-6">
                                             <div class="form-group">
                                                 <label class="control-label">Expiration Date</label>
                                                 <input type="text" name="regular" class="form-control" placeholder="09/20">
                                                 </div>
                                         </div>
                                             <div class="col-sm-6">
                                             <div class="form-group">
                                                 <label class="control-label">CVV</label>
                                                 <input type="text" name="regular" class="form-control" placeholder="09/20">
                                                 <img class="tool-tip-icon" src="{{ url('') }}/images/icons/cvv.png">
                                                 </div>
                                         </div>
                                         
                                     </div>
                                     <p class="save-details"><img src="{{ url('') }}/images/icons/save-details.png">Save my details for future payments.</p>
                                     <p class="save-details">Is it safe? Yes, all sensitive information are securely stored via Stripe, certified to PCI Service Provider Level 1</p>
                                     <button class="make-payment">MAKE PAYMENT</button>
                                     </div>
                                   </div>
                                 </div>
 
                                 <div class="panel panel-default">
                                   <div class="panel-heading">
                                     <h4 class=panel-title>
                                         <input type='radio' id='r13' name='occupation' value='Not-Working' required />
                                         <label for='r13'>
                                           <span> Paypal</span>
                                          
                                           <a data-toggle="collapse" data-parent="#accordion" href="#collapsethree"></a>
                                         </label>
                                          <ul>
                                               <li><img src="{{ url('') }}/images/icons/pay-pall.png"></li>
                                           </ul>
                                     </h4>
                                   </div>
                                   <div id="collapsethree" class="panel-collapse collapse">
                                     <div class="panel-body">
                                      <div class="edit-content">
                                         <div class="col-sm-12 ">
                                             <div class="form-group">
                                                 <label class="control-label">Card Holder Name</label>
                                                 <input type="text" name="regular" class="form-control" placeholder="Enter Your Name">
                                                 </div>
                                         </div>
                                         <div class="col-sm-12">
                                             <div class="form-group">
                                                 <label class="control-label">Card Number</label>
                                                 <input type="text" name="regular" class="form-control" placeholder="Enter Your Card Number">
                                                 </div>
                                         </div>
                                         <div class="col-sm-6">
                                             <div class="form-group">
                                                 <label class="control-label">Expiration Date</label>
                                                 <input type="text" name="regular" class="form-control" placeholder="09/20">
                                                 </div>
                                         </div>
                                             <div class="col-sm-6">
                                             <div class="form-group">
                                                 <label class="control-label">CVV</label>
                                                 <input type="text" name="regular" class="form-control" placeholder="09/20">
                                                 <img class="tool-tip-icon" src="{{ url('') }}/images/icons/cvv.png">
                                                 </div>
                                         </div>
                                         
                                     </div>
                                     <p class="save-details"><img src="{{ url('') }}/images/icons/save-details.png">Save my details for future payments.</p>
                                     <p class="save-details">Is it safe? Yes, all sensitive information are securely stored via Stripe, certified to PCI Service Provider Level 1</p>
                                     <button class="make-payment">MAKE PAYMENT</button>
                                     </div>
                                   </div>
                                 </div>
 
                                  <div class="panel panel-default">
                                   <div class="panel-heading">
                                     <h4 class=panel-title>
                                          <input type='radio' id='r14' name='occupation' value='Not-Working' required />
                                         <label for='r14'>
                                          <span> WorldPay</span>
                                          
                                           <a data-toggle="collapse" data-parent="#accordion" href="#collapsefour"></a>
                                         </label>
                                          <ul>
                                               <li><img src="{{ url('') }}/images/icons/word-pay.png"></li>
                                           </ul>
                                     </h4>
                                   </div>
                                   <div id="collapsefour" class="panel-collapse collapse">
                                     <div class="panel-body">
                                      <div class="edit-content">
                                         <div class="col-sm-12 ">
                                             <div class="form-group">
                                                 <label class="control-label">Card Holder Name</label>
                                                 <input type="text" name="regular" class="form-control" placeholder="Enter Your Name">
                                                 </div>
                                         </div>
                                         <div class="col-sm-12">
                                             <div class="form-group">
                                                 <label class="control-label">Card Number</label>
                                                 <input type="text" name="regular" class="form-control" placeholder="Enter Your Card Number">
                                                 </div>
                                         </div>
                                         <div class="col-sm-6">
                                             <div class="form-group">
                                                 <label class="control-label">Expiration Date</label>
                                                 <input type="text" name="regular" class="form-control" placeholder="09/20">
                                                 </div>
                                         </div>
                                             <div class="col-sm-6">
                                             <div class="form-group">
                                                 <label class="control-label">CVV</label>
                                                 <input type="text" name="regular" class="form-control" placeholder="09/20">
                                                 <img class="tool-tip-icon" src="{{ url('') }}/images/icons/cvv.png">
                                                 </div>
                                         </div>
                                         
                                     </div>
                                     <p class="save-details"><img src="{{ url('') }}/images/icons/save-details.png">Save my details for future payments.</p>
                                     <p class="save-details">Is it safe? Yes, all sensitive information are securely stored via Stripe, certified to PCI Service Provider Level 1</p>
                                     <button class="make-payment">MAKE PAYMENT</button>
                                     </div>
                                   </div>
                                 </div>
                               </div>
                             </form>
                       </div>
                     </div>
                 </div>
             </div>
         </div>
     </section>
     <!--account-setting-section-end-->

@endsection