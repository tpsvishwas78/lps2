@extends('layouts.usermaster')

@section('content')

<?php
@$location=\DB::table('mst_venue')->where('id',$edit->venue_id)->first();
@$images=\DB::table('game_images')->where('game_id',$edit->id)->get();
?>
<!--HEADER-SECTION-END-->
<div class="clearfix"></div>
<!--BANNER-SECTION-START-->
<section class="banner-detail">
<h1 class="hedding-detail">Start a Game</h1>
</section>
<!--BANNER-SECTION-END-->
<div class="clearfix"></div>
<!--account-setting-section-start-->
<section class="account-setting-content">
<div class="container">
<div class="row">
<div class="col-sm-12 p-0">
@include('layouts.sidebar')
<div class="col-sm-8">
        
        @if(Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
            @php
                Session::forget('success');
            @endphp
            </div>
            @endif
{{-- <div class="ground-map">
        <div id="map"></div>
</div> --}}
<div class="content-profile">
<div class="pro-info">
<h2>Start a game</h2>

<form class="edit-content" action="{{url('')}}/creategame" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="col-sm-12 p-0">
        <div class="form-group">
            <label class="control-label">Select Sport</label>
            <select class="form-control select" name="sport" id="sport">
                <option value="">Select</option>
                @foreach ($sports as $sport)
                <option @if (@$edit->sport_id==$sport->id)
                    selected
                @endif value="{{ $sport->id }}">{{ $sport->name }}</option>
                @endforeach
            </select>
            @if ($errors->has('sport'))
            <span class="text-danger">{{ $errors->first('sport') }}</span>
        @endif
            </div>
    </div>
<div class="col-sm-12 p-0">
    <div class="form-group">
        <label class="control-label">Title</label>
        <input type="text" name="title" class="form-control" placeholder="Add Title" value="{{ @$edit->title }}">
        @if ($errors->has('title'))
                        <span class="text-danger">{{ $errors->first('title') }}</span>
        @endif
        </div>
</div>
<div class="col-sm-12 p-0">
    <div class="form-group">
        <label class="control-label">Featured Image</label>
        <input type="file" name="featured_img" class="form-control">
        @if (@$edit->featured_img)
            <img src="{{ url('') }}/upload/images/{{ @$edit->featured_img }}" alt="" width="200">
        @endif
        </div>
</div>
<div class="col-sm-12 p-0">
    <div class="form-group">
        <label class="control-label">Description</label>
        <textarea name="description" class="form-control" placeholder="Enter Description">{{ @$edit->description }}</textarea>
    </div>
</div>
<div class="col-sm-12 p-0">
    <div class="form-group datepicker">
        <label class="control-label">Date</label>
        <input type="text" name="game_date" class="form-control" placeholder="Enter Your date" autocomplete="off" value="{{ @$edit->game_date }}">
        @if ($errors->has('game_date'))
        <span class="text-danger">{{ $errors->first('game_date') }}</span>
    @endif
        </div>
</div>
<div class="col-sm-6 p-0">
    <div class="form-group rangestart">
        <label class="control-label">Start Time</label>
        <input type="text" name="game_start_time" class="form-control" placeholder="Enter Your Start Time" autocomplete="off" value="{{ @$edit->game_start_time }}">
        @if ($errors->has('game_start_time'))
        <span class="text-danger">{{ $errors->first('game_start_time') }}</span>
    @endif
        </div>
</div>
<div class="col-sm-6 pr-0">
        <div class="form-group rangeend">
            <label class="control-label">Finish Time</label>
            <input type="text" name="game_finish_time" class="form-control" placeholder="Enter Your Fininsh Time" autocomplete="off"  value="{{ @$edit->game_finish_time }}">
            @if ($errors->has('game_finish_time'))
            <span class="text-danger">{{ $errors->first('game_finish_time') }}</span>
        @endif
            </div>
    </div>
    
    <div class="can-not-find">
        <div class="intrest-checkbox">
            <input class="styled-checkbox" @if (@$edit->is_repeat > 0)
                checked
            @endif id="styled-checkbox-repeat" name="repeat_check" type="checkbox" value="1">
            <label for="styled-checkbox-repeat">Repeat event</label>
        </div>
    </div>
    <div  id="repeat_option" @if (@$edit->is_repeat > 0)
            style="display:block;"
        @else style="display:none;" @endif>
    <div class="col-sm-12 p-0">
        <div class="form-group">
            <label class="control-label"></label>
            <select class="form-control select" name="is_repeat" id="is_repeat">
                <option @if (@$edit->is_repeat==1) selected @endif value="1">Every week</option>
                {{-- <option @if (@$edit->is_repeat==2) selected @endif value="2">Every 2 weeks</option>
                <option @if (@$edit->is_repeat==3) selected @endif value="3">Every month</option> --}}
            </select>
           
            </div>
    </div>
    <div class="col-sm-12 p-0">
        <div class="form-group">
            <label>Select which days</label>
            <div class="btn-group " data-toggle="buttons">
                
                <label class="btn btn-default @if (@in_array(1,unserialize(@$edit->days)))
                        active
                    @endif">
                    <input type="checkbox" @if (@in_array(1,unserialize(@$edit->days)))
                    checked
                @endif name="days[]" value="1">Mon
                </label>
                <label class="btn btn-default @if (@in_array(2,unserialize(@$edit->days)))
                        active
                    @endif">
                    <input type="checkbox" @if (@in_array(2,unserialize(@$edit->days)))
                    checked
                @endif name="days[]" value="2"> Tue
                </label>
                <label class="btn btn-default @if (@in_array(3,unserialize(@$edit->days)))
                        active
                    @endif">
                    <input type="checkbox" @if (@in_array(3,unserialize(@$edit->days)))
                    checked
                @endif name="days[]" value="3"> Wed
                </label>
                <label class="btn btn-default @if (@in_array(4,unserialize(@$edit->days)))
                        active
                    @endif">
                    <input type="checkbox" @if (@in_array(4,unserialize(@$edit->days)))
                    checked
                @endif name="days[]" value="4"> Thur
                </label>
                <label class="btn btn-default @if (@in_array(5,unserialize(@$edit->days)))
                        active
                    @endif">
                    <input type="checkbox" @if (@in_array(5,unserialize(@$edit->days)))
                    checked
                @endif name="days[]" value="5"> Fri
                </label>
                <label class="btn btn-default @if (@in_array(6,unserialize(@$edit->days)))
                        active
                    @endif">
                    <input type="checkbox" @if (@in_array(6,unserialize(@$edit->days)))
                    checked
                @endif name="days[]" value="6"> Sat
                </label>
                <label class="btn btn-default @if (@in_array(7,unserialize(@$edit->days)))
                        active
                    @endif">
                    <input type="checkbox" @if (@in_array(7,unserialize(@$edit->days)))
                    checked
                @endif name="days[]" value="7"> Sun
                </label>
            </div>
           
            </div>
    </div>

    <div class="col-sm-12 p-0">
        <div class="form-group datepicker">
            <label class="control-label">Set an end date</label>
            <input type="text" name="end_date" class="form-control" placeholder="Enter Your end date" autocomplete="off" value="{{ @$edit->end_date }}">
            
        </div>
    </div>
</div>


    <div class="col-sm-12 p-0">
    <div class="form-group">
        <label class="control-label">Venue</label>
        <input style="margin-bottom: 0px;" type="text" name="venue" id="venue" class="form-control" placeholder="Enter Venue" autocomplete="off" value="{{ @$location->name }}">
        <input type="hidden" id="venue_id" name="venue_id"  value="{{ @$location->id }}">
        <input type="hidden" id="latitude" name="latitude"  value="{{ @$location->latitude }}">
        <input type="hidden" id="longitude" name="longitude"  value="{{ @$location->longitude }}">
        @if ($errors->has('venue'))
        <span class="text-danger">{{ $errors->first('venue') }}</span>
    @endif

    <div id="venueList"></div>
        </div>
        
</div>
<div class="clearfix"></div>
<div class="can-not-find">
    <p>Cannot find what you're looking for? <a target="_blank" href="{{ url('') }}/add-venue">Add it</a></p>
    <div class="intrest-checkbox">
        <input class="styled-checkbox" @if (@$edit->is_private)
            checked
        @endif id="styled-checkbox-10" name="is_private" type="checkbox" value="1">
        <label for="styled-checkbox-10">Private (Only invited players can join)</label>
    </div>
</div>
<div class="col-sm-12 p-0">
    <div class="form-group">
        <label class="control-label">Team players limit</label>
        <select class="form-control select" name="team_limit" id="team_limit">
            <option value="">Select</option>
            <option @if (@$edit->team_limit==1) selected @endif value="1">1</option>
            <option @if (@$edit->team_limit==2) selected @endif value="2">2</option>
            <option @if (@$edit->team_limit==3) selected @endif value="3">3</option>
            <option @if (@$edit->team_limit==4) selected @endif value="4">4</option>
            <option @if (@$edit->team_limit==5) selected @endif value="5">5</option>
            <option @if (@$edit->team_limit==6) selected @endif value="6">6</option>
            <option @if (@$edit->team_limit==7) selected @endif value="7">7</option>
            <option @if (@$edit->team_limit==8) selected @endif value="8">8</option>
            <option @if (@$edit->team_limit==9) selected @endif value="9">9</option>
            <option @if (@$edit->team_limit==10) selected @endif value="10">10</option>
            <option @if (@$edit->team_limit==11) selected @endif value="11">11</option>
            <option @if (@$edit->team_limit==12) selected @endif value="12">12</option>
            <option @if (@$edit->team_limit==13) selected @endif value="13">13</option>
            <option @if (@$edit->team_limit==14) selected @endif value="14">14</option>
            <option @if (@$edit->team_limit==15) selected @endif value="15">15</option>
            <option @if (@$edit->team_limit==16) selected @endif value="16">16</option>
            <option @if (@$edit->team_limit==17) selected @endif value="17">17</option>
            <option @if (@$edit->team_limit==18) selected @endif value="18">18</option>
            <option @if (@$edit->team_limit==19) selected @endif value="19">19</option>
            <option @if (@$edit->team_limit==20) selected @endif value="20">20</option>
            <option @if (@$edit->team_limit==21) selected @endif value="21">21</option>
            <option @if (@$edit->team_limit==22) selected @endif value="22">22</option>
        </select>
        @if ($errors->has('team_limit'))
        <span class="text-danger">{{ $errors->first('team_limit') }}</span>
    @endif
        </div>
</div>
<div class="gender-select">
    <h2 class="gender-hedding">Gender options</h2>
    <ul>
        <li>
            <input id="gender1" type="radio" name="gender" value="1" @if (@$edit->gender==1) checked @endif>
            <label for="gender1">Mixed</label>
        </li>
        <li>
            <input id="gender2" type="radio" name="gender" value="3" @if (@$edit->gender==3) checked @endif>
            <label for="gender2">Women</label>
        </li>
        <li>
            <input id="gender3" type="radio" name="gender" value="2" @if (@$edit->gender==2) checked @endif>
            <label for="gender3">Men</label>
        </li>
    </ul>
    @if ($errors->has('gender'))
        <span class="text-danger">{{ $errors->first('gender') }}</span>
    @endif
    <h2 class="gender-hedding">Payment</h2>
    <ul>
        <li>
            <input id="payment1" type="radio" name="payment" value="1" @if (@$edit->payment==1) checked @else checked @endif >
            <label for="payment1">Online</label>
        </li>
        <li>
            <input id="payment2" type="radio" name="payment" value="2">
            <label for="payment2">Cash</label>
        </li>
        {{-- <li>
            <input id="payment3" checked type="radio" name="payment" value="3">
            <label for="payment3">Free</label>
        </li> --}}
    </ul>
    @if ($errors->has('payment'))
        <span class="text-danger">{{ $errors->first('payment') }}</span>
    @endif

</div>

<div class="col-sm-12 p-0">
        <div class="form-group">
                <div class="payment-fields">
                        <div class="form-group currency optional payment-box">
                            
                            <label class="control-label">Price per player</label>
                            
                                <div class="input-group">
                                        <input type="text" class="form-control" name="price" value="{{ @$edit->price }}">
                                        
                                        <span class="input-group-btn">
                                          <select class="btn" name="currency">
                                                <option @if (@$edit->currency=='GBP')
                                                    
                                                @endif value="GBP">GBP</option>
                                                {{-- <option value="EUR">EUR</option>
                                                <option value="USD">USD</option>  --}}
                                          </select>
                                        </span>
                                        
                                      </div>
                                      @if ($errors->has('price'))
                                      <span class="text-danger">{{ $errors->first('price') }}</span>
                                  @endif     
                            {{-- <p class="help-block">You will receive <span class="js-currency font-bold">£</span><span class="js-price font-bold">0</span> after fees.</p> --}}
                        </div>
                        <div class="refund-fields">
                            <div class="form-group boolean optional game_refund_cancelled">
                                <div class="checkbox">
                                    <label>
                                        <input class="boolean optional" @if (@$edit->is_refund)
                                            checked
                                        @endif type="checkbox" value="1" name="is_refund" id="is_refund"> Refund if the game is cancelled
                                    </label>
                                </div>
                            </div>
                            <div class="form-group boolean optional game_refund_changed_rsvp">
                                <div class="checkbox">
                                    <label>
                                        <input class="boolean optional" @if (@$edit->refund_changed_rsvp)
                                        checked
                                    @endif type="checkbox" value="1" name="refund_changed_rsvp" id="refund_changed_rsvp"> Refund if RSVP changed 
                                        <input value="{{ @$edit->refund_days }}" class="numeric integer optional" type="text" name="refund_days" id="refund_days"> days before game
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
</div>
<input type="hidden" name="id" value="{{ @$edit->id }}">
<div class="col-sm-12 p-0">
    <div class="form-group">
        <h2 class="gender-hedding">Venue Images</h2>
        <div class="controls">
           
            <div class="entry input-group col-xs-3">
              
           
              <input class="btn btn-default" name="gallary[]" type="file">
              <span class="input-group-btn">
            <button class="btn btn-success btn-add" type="button">
                              <span class="glyphicon glyphicon-plus"></span>
              </button>
              </span>
            </div>
            @if (@$images)
            @foreach (@$images as $image)
            <p>
                    <img width="100" src="{{ url('') }}/upload/images/{{ $image->image }}" alt="">
                    <a href="{{ url('/gamedeletegalleryimage') }}/{{ $image->id }}"><i class="fa fa-remove"></i></a>
                    </p>
            @endforeach
            @endif
            
         
        </div>
        
        </div>
</div>
<button type="submit" class="create-game">CREATE GAME</button>
</form>
</div>
</div>
</div>
</div>
</div>
</section>
<!--account-setting-section-end-->

<script>
    $(document).on('click', '#styled-checkbox-repeat', function(e)
    {
        var check=$('input[name="repeat_check"]:checked').val();
        if(check){
            $('#repeat_option').show();
        }else{
            $('#repeat_option').hide();
        }
        
    });

    $(document).on('click', '#payment1', function(e)
    {
        $('.payment-box').show();
        $('.help-block').show();
        $('.refund-fields').show();
    });
    $(document).on('click', '#payment2', function(e)
    {
        $('.payment-box').show();
        $('.help-block').hide();
        $('.refund-fields').hide();
    });
    $(document).on('click', '#payment3', function(e)
    {
        $('.payment-box').hide();
        $('.help-block').hide();
        $('.refund-fields').hide();
    });

    $(function()
{
    $(document).on('click', '.btn-add', function(e)
    {
        e.preventDefault();

        var controlForm = $('.controls:first'),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="glyphicon glyphicon-minus"></span>');
    }).on('click', '.btn-remove', function(e)
    {
      $(this).parents('.entry:first').remove();

		e.preventDefault();
		return false;
	});
});
        $(document).ready(function(){
        
         $('#venue').keyup(function(){ 
                var query = $(this).val();
                //console.log('query',query);
                if(query != '')
                {
                 var _token = $('input[name="_token"]').val();
                 $.ajax({
                  url:"{{ url('') }}/venue/autocomplete",
                  method:"POST",
                  data:{query:query, _token:_token},
                  success:function(data){
                      //console.log('data',data);
                             $('#venueList').fadeIn();  
                            $('#venueList').html(data);
                  }
                 });
                }
            });
        
            $(document).on('click', '.dropdown-menu li', function(){  
                $('#venue').val($(this).text());
                var lat=$(this).attr('lat');
                var lng=$(this).attr('lng');
                var vid=$(this).attr('vid');
                $('#latitude').val(lat);
                $('#longitude').val(lng);
                $('#venue_id').val(vid);
                $('#venueList').fadeOut();
                initAutocomplete();
            });  
        
        });
        
        function initAutocomplete() {
            var latitude=$('#latitude').val();
            var longitude=$('#longitude').val();
            if(latitude && longitude){
                var lattt=parseFloat(latitude);
                var long=parseFloat(longitude);
            }else{
                var lattt= -25.363;
                var long= 131.044;
            }

            
           
          var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: lattt, lng: long},
            zoom: 13,
            mapTypeId: 'roadmap'
          });
		  var marker = new google.maps.Marker({map: map, position: {lat: lattt, lng: long}});
  
          
        }
        </script>
@endsection