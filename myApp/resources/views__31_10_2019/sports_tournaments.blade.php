@extends('layouts.master')
@section('content')
<?php
$setting=\DB::table('site_setting')->first();
?>
<div class="clearfix"></div>
	<!--BANNER-SECTION-START-->
	<section class="banner-detail" style="background: url({{ url('') }}/images/Sports-Tournaments.jpg);">
       <h1 class="hedding-detail">Sports Tournaments</h1>
    </section>
	<!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
	<!--CONTACT-US-SECTION-START-->
	
	<!--soft-play-section-start-->
	<section class="blocks-children">
		<div class="container">
			<div class="row">
		 <div class="col-sm-12">
			 
			
			@foreach ($categories as $category)
			<div class="col-sm-4">
				 <a href="{{ url('') }}/sports-tournaments/{{ $category->slug }}">
				<div class="block-img">
					 @if ($category->image)
					 <img src="{{url('')}}/upload/images/{{$category->image}}">
				 @else
					 <img src="{{url('')}}/images/oneday-img4.jpg">
				 @endif
					<div class="hover-deta-img team-build">
						<h2>{{ $category->name }}</h2>
						<!--<p>None of these really need content mate </p>-->
					</div>
				</div>
				 </a>
			</div>
			@endforeach
			
			
		 </div>
		</div>
		</div>
	</section>
  <!--soft-play-section-end-->

	
@endsection