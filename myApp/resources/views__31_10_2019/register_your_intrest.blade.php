@extends('layouts.master')
@section('content')
<?php
$setting=\DB::table('site_setting')->first();
?>
<div class="clearfix"></div>
	<!--BANNER-SECTION-START-->
	<section class="banner-detail">
       <h1 class="hedding-detail">Register your interest</h1>
    </section>
	<!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
	<!--CONTACT-US-SECTION-START-->
	
	<section class="form-contact">
		<div class="container">
			<div class="row">
					@if(Session::has('success'))
					<div class="alert alert-success">
						{{ Session::get('success') }}
						@php
							Session::forget('success');
						@endphp
						</div>
						@endif
				<div class="col-sm-10 col-sm-offset-1">
					<div class="heading-contact-form">
						<h1>Register your interest form</h1>
						<p>Please fill out the below form - we aim to respond within 24 hours </p>
					</div>
					<div class="contact-us-form">
                        <form class="comment" method="POST" action="{{ url('') }}/submitIntrest">
                            @csrf
						<div class="col-sm-6 p-0">
							<div class="form-group">
								<label class="control-label">First Name</label>
                                <input type="text" name="first_name" class="form-control" placeholder="Enter Your First  Name">
                                @if ($errors->has('first_name'))
                        <span class="text-danger">{{ $errors->first('first_name') }}</span>
                    @endif
								</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label">Last Name</label>
                                <input type="text" name="last_name" class="form-control" placeholder="Enter Your Last Name">
                                @if ($errors->has('last_name'))
                        <span class="text-danger">{{ $errors->first('last_name') }}</span>
                    @endif
								</div>
						</div>
						<div class="col-sm-6 p-0">
							<div class="form-group">
								<label class="control-label">Your Email</label>
                                <input type="text" name="email" class="form-control" placeholder="Enter Your Email">
                                @if ($errors->has('email'))
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                    @endif
								</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label">Mobile Number</label>
                                <input type="text" name="mobile_number" class="form-control" placeholder="Enter Your Mobile Number">
                                @if ($errors->has('mobile_number'))
                        <span class="text-danger">{{ $errors->first('mobile_number') }}</span>
                    @endif
								</div>
						</div>
						<div class="col-sm-12 pl-0">
								<div class="form-group">
									<label class="control-label">Location</label>
									<input type="text" name="location" id="pac-input" class="form-control" placeholder="Enter Your Location">
									@if ($errors->has('location'))
							<span class="text-danger">{{ $errors->first('location') }}</span>
						@endif
									</div>
							</div>
						<div class="col-sm-12 pl-0">
							<div class="form-group">
								<label class="control-label">	Your Message</label>
                                <textarea name="message" class="form-control textarea" placeholder="Enter Your Message"></textarea>
                                @if ($errors->has('message'))
                        <span class="text-danger">{{ $errors->first('message') }}</span>
                    @endif
								</div>
						</div>
						<div class="captcha contact-captcha">
    					{!! NoCaptcha::renderJs() !!}
                        {!! NoCaptcha::display() !!}
                        @if ($errors->has('g-recaptcha-response'))
                        <span class="text-danger">{{ $errors->first('g-recaptcha-response') }}</span>
                    @endif
					</div>
					<div style="text-align: center;">
						<button class="submit">SUBMIT</button>
					</div>
					</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--CONTACT-US-SECTION-END-->

	<script>
		function initAutocomplete() {
			var input = document.getElementById('pac-input');
			//var searchBox = new google.maps.places.SearchBox(input);
			var options = {
			types: ['(cities)'],
			componentRestrictions: {country: 'GB'}//UK only
			};
			var autocomplete = new google.maps.places.Autocomplete(input,options);
          
        }
	</script>
@endsection