@extends('layouts.master')
@section('content')

<!--BANNER-SECTION-START-->
<section class="banner-detail">
		<div  class="hedding-children">
       <h1>Corporate Sports Days</h1>
       </div>
    </section>
	<!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
	<!--crousel--and-video-section-start-->
	<section class="corporate-sports-day">
		<div class="container">
			<div  class="row">
				<div class="col-sm-12">
					<div class="col-sm-7">
						<div  class="left-vide0-section">
							<div class="video-detail">
								<img src="{{ url('') }}/images/img1.jpg">
								<div class="video-play-icon-cp">
									<img src="{{ url('') }}/images/icons/play-video.png">
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-5">
						<div class="right-side-crousel">
							<!--crousel-->
							<div class="header" id="cover">
						<div class="carousel slide" data-ride="carousel" id="carousel">
						<div class="carousel-inner">
						<div class="item active">
						
						<img class="responsive" alt="umpires" src="{{ url('') }}/images/detail-crousel-img.jpg"></div>
						<div class="item">
						
						<img class="responsive" alt="baseballd" src="{{ url('') }}/images/img-list1.png">
						</div>
						<div class="item">
						
						<img class="responsive" alt="fallball5" src="{{ url('') }}/images/img-list2.png">
						</div>
						<div class="item">
						
						<img class="responsive" alt="fallball4" src="{{ url('') }}/images/img-list3.png">
						</div>
						
						</div>
						<div class="clearfix">
						<div class="carousel-link">
						<div class="thumb" data-slide-to="0" data-target="#carousel">
						<img class="responsive" alt="umpires" src="{{ url('') }}/images/detail-crousel-img.jpg"></div>
						<div class="thumb" data-slide-to="1" data-target="#carousel">
						<img class="responsive" alt="baseballd" src="{{ url('') }}/images/img-list1.png">
						</div>
						<div class="thumb" data-slide-to="2" data-target="#carousel">
						<img class="responsive" alt="fallball5" src="{{ url('') }}/images/img-list2.png">
						</div>
						<div class="thumb" data-slide-to="3" data-target="#carousel">
						<img class="responsive" alt="fallball4" src="{{ url('') }}/images/img-list3.png">
						</div>
						
						</div>
						</div>
						</div>
						</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="info-cp"><p>Fun filled, fast paced action for your corporate event. We have a huge range of activities we can include within your sports day package. Please email us on info@letsplaysports.co.uk or call 020 3589 6019 for a quote.</p>

				</div>
			</div>
		</div>
	</section>
	<!--crousel--and-video-section-end-->
	<div class="clearfix"></div>
	<!--referess-section-strt-->
	<section class="refress-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
                     <div class="col-sm-2">
                     	<div class="img-and-info">
                     		<img src="{{ url('') }}/images/icons/icon-venu1.png">
                     		<p>Venue</p>
                     	</div>
                     </div>
                     <div class="col-sm-2">
                     	<div class="img-and-info">
                     		<img src="{{ url('') }}/images/icons/icon-venue2.png">
                     		<p>Referees</p>
                     	</div>
                     </div><div class="col-sm-2">
                     	<div class="img-and-info">
                     		<img src="{{ url('') }}/images/icons/icon-venue3.png">
                     		<p>Management</p>
                     	</div>
                     </div><div class="col-sm-2">
                     	<div class="img-and-info">
                     		<img src="{{ url('') }}/images/icons/icon-venue4.png">
                     		<p>Equipment</p>
                     	</div>
                     </div><div class="col-sm-2">
                     	<div class="img-and-info">
                     		<img src="{{ url('') }}/images/icons/icon-venue5.png">
                     		<p>Trophies</p>
                     	</div>
                     </div><div class="col-sm-2">
                     	<div class="img-and-info">
                     		<img src="{{ url('') }}/images/icons/icon-venue6.png">
                     		<p>Extras</p>
                     	</div>
                     </div>
				</div>
			</div>
		</div>
	</section>
	<!--referess-section-end-->
	<div class="clearfix"></div>
	<!--sports-day-challanges-start-->
	<section class="sports-challanges">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="head-challange">
						<h1>Sports Day Challanges</h1>
						<p>Not sure how to run the event and organise games? Well don't worry we can post and create fixtures for you and post them online 48 hours before kick off so your teams know exactly what time theyre playing and where. </p>
					</div>
					<div class="tab-design-function">
						 <div  class="col-sm-12 p-0">
       
				        <div class="col-sm-5"> <!-- required for floating -->
				          <!-- Nav tabs -->
				          <ul class="nav nav-tabs tabs-left sideways">
				            <li class="active"><a href="#home-v" data-toggle="tab">
								<div class="drop-down-icon-filter-corporate"></div>  Doegeball- finale event</a></li>
				            <li><a href="#profile-v" data-toggle="tab"><div class="drop-down-icon-filter-corporate"></div> Tug of War- finale event</a></li>
				            <li><a href="#messages-v" data-toggle="tab"> <div class="drop-down-icon-filter-corporate"></div> zorb football</a></li>
				            <li><a href="#settings-v" data-toggle="tab"><div class="drop-down-icon-filter-corporate"></div> Tunnel Ball</a></li>
				            <li><a href="#settings-v1" data-toggle="tab"><div class="drop-down-icon-filter-corporate"></div> Soccer Skillz</a></li>
				            <li><a href="#settings-v2" data-toggle="tab"><div class="drop-down-icon-filter-corporate"></div> Water Balloon Challenge</a></li>
				             <li><a href="#settings-v3" data-toggle="tab"><div class="drop-down-icon-filter-corporate"></div> Space Hopper Challenge</a></li>
				          </ul>
				        </div>

				        <div class="col-sm-7">
				          <!-- Tab panes -->
				          <div class="tab-content">
				            <div class="tab-pane active" id="home-v">
				            	<div class="video-detail">
								<img src="{{ url('') }}/images/img1.jpg">
								<div class="video-play-icon-cp">
									<img src="{{ url('') }}/images/icons/play-video.png">
								</div>
							</div>
				            </div>
				            <div class="tab-pane" id="profile-v">
				            	<div class="video-detail">
								<img src="{{ url('') }}/images/img2.jpg">
								<div class="video-play-icon-cp">
									<img src="{{ url('') }}/images/icons/play-video.png">
								</div>
							</div>
				            </div>
				            <div class="tab-pane" id="messages-v"><div class="video-detail">
								<img src="{{ url('') }}/images/img3.jpg">
								<div class="video-play-icon-cp">
									<img src="{{ url('') }}/images/icons/play-video.png">
								</div>
							</div></div>
				            <div class="tab-pane" id="settings-v">
				            	<div class="video-detail">
								<img src="{{ url('') }}/images/img1.jpg">
								<div class="video-play-icon-cp">
									<img src="{{ url('') }}/images/icons/play-video.png">
								</div>
							</div>
				            </div>
				            <div class="tab-pane" id="settings-v1">
				            	<div class="video-detail">
								<img src="{{ url('') }}/images/img4.jpg">
								<div class="video-play-icon-cp">
									<img src="{{ url('') }}/images/icons/play-video.png">
								</div>
							</div>
				            </div>
				            <div class="tab-pane" id="settings-v2">
				            	<div class="video-detail">
								<img src="{{ url('') }}/images/img1.jpg">
								<div class="video-play-icon-cp">
									<img src="{{ url('') }}/images/icons/play-video.png">
								</div>
							</div></div>
				            <div class="tab-pane" id="settings-v3">
				            	<div class="video-detail">
								<img src="{{ url('') }}/images/img2.jpg">
								<div class="video-play-icon-cp">
									<img src="{{ url('') }}/images/icons/play-video.png">
								</div>
							</div>
				            </div>

				          </div>
				        </div>

				        <div class="clearfix"></div>

				      </div>

				      
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--sports-day-challanges-start-->
	<div class="clearfix"></div>
	<!--last-section-start-->
	<section class="serach-result">
		<div class="container">
			<div class="row">
				<!--game-listing-start-->
				<div class="col-sm-12">
					<div class="listing-games">
						<div class="design-list">
							<div class="col-sm-9">
								<div class="info-game">
									<p>Location</p>
									
									<div class="dis-game">We are affiliated with Locations acorss the Uk and host any style tournament (5,6,7,11 a side) for as many people as you require. All you have to do is tell us where you wish the event to take place and the ideal times and we will give you a list of suitable options</div>
								</div>
							</div>
							<div class="col-sm-3 p-0">
							<div class="game-img">
								<img src="{{ url('') }}/images/img-list1.png">
							</div>
							</div>
						</div>
					</div>
					<div class="listing-games">
						<div class="design-list">
							<div class="col-sm-9">
								<div class="info-game">
									<p>Equipment</p>
									
									<div class="dis-game">Dont want to carry anything to the venue? Dont worry we will have everything ready for you from balls to bibs and any extras you may require for your event.s</div>
								</div>
							</div>
							<div class="col-sm-3 p-0">
							<div class="game-img">
								<img src="{{ url('') }}/images/img-list3.png">
							</div>
							</div>
						</div>
					</div>
					<div class="listing-games">
						<div class="design-list">
							<div class="col-sm-9">
								<div class="info-game">
									<p>Trophies</p>
									
									<div class="dis-game">Our packs include Trophies for winners and runners up along with medals. You can choose from standard 10" trophies to large 22" inch trophies so you can really spoil the winners and there is even a plate for the losers.
										</div>
								</div>
							</div>
							<div class="col-sm-3 p-0">
							<div class="game-img">
								<img src="{{ url('') }}/images/img-list2.png">
							</div>
							</div>
						</div>
					</div>
					<div class="listing-games">
						<div class="design-list">
							<div class="col-sm-9">
								<div class="info-game">
									<p>Event Management</p>
									
									<div class="dis-game">Not sure how to run the event and organise games? Well don't worry we can post and create fixtures for you and post them online 48 hours before kick off so your teams know exactly what time theyre playing and where. This also adds vital build up to what could be a long awaited tournament. On the day if you need a dedicated event manager to ensure smooth operation alongside referees then fear not, we can get anyone anywhere and make sure that all the bad boys and girls are kept in line. Download our brochure here </div>
								</div>
							</div>
							<div class="col-sm-3 p-0">
							<div class="game-img">
								<img src="{{ url('') }}/images/img-list4.png">
							</div>
							</div>
						</div>
					</div>
				</div>
				<!--game-listing-end-->
			</div>
		</div>
	</section>
	<!--last-section-end-->

@endsection