@extends('layouts.master')
@section('content')

<!--BANNER-SECTION-START-->
<section class="banner-detail" style="    background: url({{ url('') }}/images/Children-Party-Banner.jpg);">
		<div  class="hedding-children">
       <h1>{{ $cat->name }}</h1>
       </div>
    </section>
	<!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
	<!--soft-play-section-start-->
	  <section class="blocks-children">
	  	<div class="container">
	  		<div class="row">
	  	 <div class="col-sm-12">
            @foreach ($party as $category)
	  	 	<div class="col-sm-4">
	  	 		<div class="block-img">
                        @if ($category->image)
                        <img src="{{url('')}}/upload/images/{{$category->image}}">
                    @else
                        <img src="{{url('')}}/images/oneday-img4.jpg">
                    @endif
	  	 			
				   </div>
				   <div class="hover-deta-img-kids team-build">
					<h2>{{ $category->title }}</h2>
					<div class="kidspara">{!! $category->description !!} </div>
					<a href="{{ url('') }}/children/register-your-interest">
                        <button class="enquire-now">Enquire Now</button>
                        </a>
				</div> 
               </div>
               @endforeach
	  	 		  	 	
	  	 </div>
	  	</div>
	  	</div>
	  </section>
	<!--soft-play-section-end-->

@endsection