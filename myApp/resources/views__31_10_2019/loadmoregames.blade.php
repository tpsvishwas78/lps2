<?php
@$user = Auth::user();

use App\Models\MstVenue;

?>
@foreach ($games as $game)
<?php
if ($game->game_start_date) {
	@$day = date('d', strtotime($game->game_start_date));
	@$month = date('M', strtotime($game->game_start_date));
} else {
	@$day = date('d', strtotime($game->game_date));
	@$month = date('M', strtotime($game->game_date));
}
@$location = \DB::table('mst_venue')->where('id', $game->venue_id)->first();
@$spots = \DB::table('team_spots')->where('game_id', $game->id)->where('status', 1)->count();

@$followes = \DB::table('venue_followers')->where('venue_id', $game->venue_id)->count();
@$mefollow = \DB::table('venue_followers')->where('user_id', $user->id)->where('venue_id', $game->venue_id)->count();
?>
<style>
	button.follow1 {
		background-color: #000;
		color: #fff;
		border: none;
		padding: 2px 15px;
		font-weight: 500;
		border: 1px solid #e6e6e6;
		/* color: #575f64; */
		font-weight: 500;
	}
</style>
<div class="design-list">

	<div class="col-sm-2 col-xs-3 p-0">
		<div class="date-game">
			<h2>{{$day}}<span>{{$month}}</span></h2>
		</div>
	</div>
	<div class="col-sm-7 col-xs-9">
		<div class="info-game">
			<a href="{{ url('') }}/games/{{ $game->slug }}/{{ $game->rid }}">
				<p><img src="{{url('')}}/images/icons/tenis-ball.png">{{ $game->title }}</p>
			</a>
			<ul class="time-and-place">
				<li><img src="{{url('')}}/images/icons/watch-icon.png">{{$game->game_start_time}} - {{$game->game_finish_time}}</li>
				<li title="{{ $location->name }}"><img src="{{url('')}}/images/icons/location-icon.png">{!! str_limit($location->name, $limit = 50, $end = '') !!}</li>
			</ul>
			<div class="follow-venue">
				<ul>
					<li class="price-book">
						@if ($game->payment==1 || $game->payment==2)
						@if ($game->currency=='USD')
						${{ $game->price }}
						@else
						£{{ $game->price }}
						@endif
						@else
						Free
						@endif
					</li>
					{{-- <li class="number-player">{{ $spots }}/{{ $game->team_limit+$game->team_limit }}</li> --}}

					<li class="follow-button">
						<?php

						$venue = MstVenue::where('id', $game->venue_id)->first();
						?>
						@csrf
						<input type="hidden" value="{{ @$user->id }}" id="userid">
						@if ($mefollow > 0)
						<a href="{{ url('') }}/venue/{{ $venue->slug }}">
							<button class="follow setfollow">
								View Sessions
							</button>
						</a>
						<!-- <button id="follow{{ $game->venue_id }}" follow="Following" vid="{{ $game->venue_id }}" class="follow setfollow">Following Venue</button> -->
						@else
						<a href="{{ url('') }}/venue/{{ $venue->slug }}">
							<button class="follow setfollow">
								View Sessions
							</button>
						</a>
						<!-- <button id="follow{{ $game->venue_id }}" follow="Follow" vid="{{ $game->venue_id }}" class="follow setfollow">Follow Venue</button> -->
						@endif

						<?php
						if (Auth::id()) {
							?>
							<a href="{{ url('') }}/games/{{ $game->slug }}/{{ $game->rid }}">
								<button class="follow1 setfollow">
									Join Now
								</button>
							</a>
						<?php } ?>

						<!-- <span><span id="followcount{{ $game->venue_id }}">{{$followes}}</span> followers</span> -->
					</li>

				</ul>
			</div>
			<div class="dis-game">{!! str_limit($game->description, $limit = 45, $end = '') !!} </div>
		</div>
	</div>
	<div class="col-sm-3 col-xs-12 p-0">
		<div class="game-img">
			@if ($game->featured_img)
			<img src="{{url('')}}/upload/images/{{$game->featured_img}}">
			@else
			<img src="{{url('')}}/images/no-user-Image.png">
			@endif

		</div>
	</div>

</div>
@endforeach

<div class="load-more-button" id="remove-row">
	<button id="btn-more" data-id="3" class="load-more">LOAD MORE</button>
</div>