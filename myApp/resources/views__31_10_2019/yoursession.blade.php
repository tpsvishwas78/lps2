@extends('layouts.master')
@section('content')

<div class="clearfix"></div>
	<!--BANNER-SECTION-START-->
	<section class="banner-detail">
       <h1 class="hedding-detail">Rigister your session</h1>
    </section>
	<!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
<!--hidden-form-content2-start-->
<section class="session-form">
		<div class="container">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<div class="your-booking-info">
						<h1>Advertise Your Session</h1>
						<p>You will be part of the letsplaysports extended family. All player fees minus a 10% + VAT commision will be sent directly to your letsplaysports account. </p>
						<p>You will be provided with balls and bibs every week<br>You will be provided with official kit<br>You will be reponsible for your own venue costs.</p>
						<p>This is particularly good if you have 2-3 mates who want to play football at a certain venue and want to reach out to as many people as possible and play for free whilst running a good game of football. </p>
						<p>We ask for all LPS session managers not to think of this as a business but rather a community that brings people together with the aim of fun, sports and fitness.</p>
					</div>
					<div class="heading-contact-form">
						<h1>Fill Out The Form Below</h1>
						<p>To advertise your session online please fill out the form below, please note it can take up to 24 hours for admin approval. </p>
					</div>
					@if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
                @php
                    Session::forget('success');
				@endphp
				</div>
				@endif
					<div class="contact-us-form">
							<form class="comment" action="{{ url('') }}/submitSession" method="POST" enctype="multipart/form-data">
								@csrf
							<div class="col-sm-6 p-0">
								<div class="form-group">
									<label class="control-label">Firs Name</label>
									<input type="text" name="first_name"  class="form-control" placeholder="Enter Your First  Name" value="{{ old('first_name') }}">
									@if ($errors->has('first_name'))
							<span class="text-danger">{{ $errors->first('first_name') }}</span>
						@endif
									</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label class="control-label">Last Name</label>
									<input type="text" name="last_name" class="form-control" placeholder="Enter Your Last Name" value="{{ old('last_name') }}">
									@if ($errors->has('last_name'))
							<span class="text-danger">{{ $errors->first('last_name') }}</span>
						@endif
									</div>
							</div>
							<div class="col-sm-6 p-0">
								<div class="form-group">
									<label class="control-label">Your Email</label>
									<input type="text" name="email" class="form-control" placeholder="Enter Your Email" value="{{ old('email') }}">
									@if ($errors->has('email'))
									<span class="text-danger">{{ $errors->first('email') }}</span>
								@endif	
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label class="control-label">Mobile Number</label>
									<input type="text" name="mobile_number" class="form-control" placeholder="Enter Your Mobile Number" value="{{ old('mobile_number') }}">
									@if ($errors->has('mobile_number'))
									<span class="text-danger">{{ $errors->first('mobile_number') }}</span>
								@endif
									</div>
							</div>
							<div class="col-sm-6 p-0">
								<div class="form-group">
									<label class="control-label">Venue</label>
									<input type="text" name="venue" class="form-control" placeholder="Select vanue" autocomplete="off" id="venue" style="margin-bottom: 0px;">
									<input type="hidden" id="venue_id" name="venue_id">
									@if ($errors->has('venue'))
									<span class="text-danger">{{ $errors->first('venue') }}</span>
								@endif
								<div id="venueList">
								</div>
									</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group datepicker">
									<label class="control-label">Date</label>
									<input autocomplete="off" type="text" name="date" class="form-control" placeholder="Select Your Date" value="{{ old('date') }}">
									@if ($errors->has('date'))
									<span class="text-danger">{{ $errors->first('date') }}</span>
								@endif	
								</div>
							</div>
							<div class="col-sm-6 p-0">
									<div class="form-group">
										<label class="control-label">Sport</label>
										<select name="sport" id="sport" class="form-control">
											<option value="">Select</option>
											@foreach ($sports as $sport)
											<option value="{{ $sport->id }}">{{ $sport->name }}</option>
											@endforeach
											
										</select>
										@if ($errors->has('sport'))
										<span class="text-danger">{{ $errors->first('sport') }}</span>
									@endif	
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group timepicker">
										<label class="control-label">Time</label>
										<input type="text" autocomplete="off" name="time" class="form-control" placeholder="Select Your Time">
										@if ($errors->has('time'))
										<span class="text-danger">{{ $errors->first('time') }}</span>
									@endif
										</div>
								</div>
									<div class="col-sm-12 p-0">
									<div class="form-group">
										<label class="control-label">No of player needed</label>
										<input type="text" name="no_of_player" class="form-control" placeholder="0">
										@if ($errors->has('no_of_player'))
										<span class="text-danger">{{ $errors->first('no_of_player') }}</span>
									@endif
										</div>
								</div>
							<div class="gender-select">
									<h2 class="gender-hedding">Is this a recurring booking</h2>
		<ul>
			<li>
				<input id="gender1" type="radio" name="recurring_booking" value="1">
				<label for="gender1">Yes</label>
			</li>
			<li>
				<input id="gender2" type="radio" name="recurring_booking" value="0">
				<label for="gender2">No</label>
			</li>
			
		</ul>
		@if ($errors->has('recurring_booking'))
									<span class="text-danger">{{ $errors->first('recurring_booking') }}</span>
								@endif
								</div>
							<div class="col-sm-12 p-0">
								<div class="form-group">
									<label class="control-label">Description</label>
									<textarea name="message" class="form-control textarea" placeholder="Enter Your Message">{{ old('message') }}</textarea>
									@if ($errors->has('message'))
									<span class="text-danger">{{ $errors->first('message') }}</span>
								@endif	
								</div>
							</div>
							<div class="col-sm-12 p-0">
								<div class="choose-file">
									<label class="control-label">Image Upload</label>
									<input type="file" name="image" class="file" placeholder="Select Your Date">
									@if ($errors->has('image'))
									<span class="text-danger">{{ $errors->first('image') }}</span>
								@endif
									<button style="z-index: -1;" type="button" class="custom-upload">CHOOSE FILE</button>
								</div>
							</div>
							<div class="col-sm-12 p-0">
									<div class="form-control sign-here">
										<label class="control-label">	Sign Here</label>
										<canvas id="signature-pad" class="signature-pad" width=225 height=90></canvas>
										  <input type="hidden" id="mysignature" name="signature">
										  
										  @if ($errors->has('signature'))
										  <span class="text-danger">{{ $errors->first('signature') }}</span>
									  @endif
										</div>
										<button type="button" id="signature-clear" class="btn text-center">Undo</button>
								</div>
								
							<div class="captcha contact-captcha">
								{!! NoCaptcha::renderJs() !!}
								{!! NoCaptcha::display() !!}
								@if ($errors->has('g-recaptcha-response'))
								<span class="text-danger">{{ $errors->first('g-recaptcha-response') }}</span>
							@endif
						</div>
						<div style="text-align: center;">
							<button class="submit">SUBMIT</button>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--hidden-form-content2-end-->
	<script>
			$(document).ready(function(){
					
					$('#venue').keyup(function(){ 
						   var query = $(this).val();
						   //console.log('query',query);
						   if(query != '')
						   {
							var _token = $('input[name="_token"]').val();
							$.ajax({
							 url:"{{ url('') }}/venue/autocomplete",
							 method:"POST",
							 data:{query:query, _token:_token},
							 success:function(data){
								 //console.log('data',data);
										$('#venueList').fadeIn();  
									   $('#venueList').html(data);
							 }
							});
						   }
					   });
				   
					   $(document).on('click', '.dropdown-menu li', function(){  
						   $('#venue').val($(this).text());
						   var lat=$(this).attr('lat');
						   var lng=$(this).attr('lng');
						   var vid=$(this).attr('vid');
						   $('#latitude').val(lat);
						   $('#longitude').val(lng);
						   $('#venue_id').val(vid);
						   $('#venueList').fadeOut();
						   initAutocomplete();
					   });  
				   
				   });
			</script>
@endsection