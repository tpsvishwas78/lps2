@extends('layouts.master')
@section('content')

<div class="clearfix"></div>
	<!--BANNER-SECTION-START-->
	<section class="banner-detail">
       <h1 class="hedding-detail">Register your session</h1>
    </section>
	<!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
<!--Rigister-session-SECTION-START-->
<section class="Register-your-session">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="two-buttons-session">
						<div class="col-sm-10 col-sm-offset-1">
							<div class="col-sm-6">
								<div class="your-booking">
									{{-- <a href="{{ url('') }}/your-booking"> --}}
										<a href="{{ url('') }}/startgame">
									<img src="{{ url('') }}/images/icons/reg1.png">
                                    <h2>Looking for a  few players for  your booking</h2>
                                    <a>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="your-session">
									{{-- <a href="{{ url('') }}/your-session"> --}}
										<a href="{{ url('') }}/startgame">
									<img src="{{ url('') }}/images/icons/reg2.png">
                                    <h2>Set up your own session</h2>
                                </a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--Rigister-session-SECTION-END-->

@endsection