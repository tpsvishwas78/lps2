@extends('layouts.master')
@section('content')

<!--BANNER-SECTION-START-->
<section class="banner-detail">
		<div  class="hedding-children">
       <h1>Corporate Events</h1>
       </div>
    </section>
	<!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
	<!--soft-play-section-start-->
	  <section class="blocks-children">
	  	<div class="container">
	  		<div class="row">
	  	 <div class="col-sm-12">
            @foreach ($categories as $category)
	  	 	<div class="col-sm-4">
					<a href="{{ url('') }}/corporate/{{ $category->slug }}">
	  	 		<div class="block-img">
                        @if ($category->image)
                        <img src="{{url('')}}/upload/images/{{$category->image}}">
                    @else
                        <img src="{{url('')}}/images/oneday-img4.jpg">
                    @endif
	  	 			<div class="hover-deta-img team-build">
	  	 				<h2>{{ $category->name }}</h2>
	  	 				<!--<p>None of these really need content mate </p>-->
	  	 			</div>
				   </div>
					</a>
               </div>
			   @endforeach
			   <!-- <div class="col-sm-4">
					<a href="{{ url('') }}/sports-tournaments">
	  	 		<div class="block-img">
                        <img src="{{url('')}}/images/sports-tournaments-block.jpg">
	  	 			<div class="hover-deta-img team-build">
	  	 				<h2>Sports Tournaments</h2>
	  	 				<p>None of these really need content mate </p>
	  	 			</div>
				   </div>
					</a>
               </div> -->
	  	 		  	 	
	  	 </div>
	  	</div>
	  	</div>
	  </section>
	<!--soft-play-section-end-->

@endsection