<form id="paymentToken" name="paymentToken" method="post" action="https://secure-test.worldpay.com/wcc/purchase">

    <input type="hidden" name="instId" value="XXXXXXX"/>
    <input type="hidden" name="cartId" value="Kvc-23"/>
    <input type="hidden" name="testMode" value="Y"/>
    <input type="hidden" name="accId1" value="XXXXXXXX"/>
    <input type="hidden" name="currency" value="GBP"/>
    <input type="hidden" name="town" value="Town"/>
    <input type="hidden" name="authMode" value="A"/>
    
    <input type="hidden" name="MC_ad_id" id="MC_ad_id" value="Custom_Parameter"/>
    
    <input type="hidden" name="amount" value="123.45" />
    
    <input type="hidden" name="desc" value="Test Product Description"/>
    <input type="hidden" name="name" value="Test Product Name"/>
    <input type="hidden" name="address1" value="Address line 1"/>
    <input type="hidden" name="address2" value="Address line 2"/>
    <input type="hidden" name="address3" value="Address line 3"/>
    <input type="hidden" name="postcode" value="Postcode"/>
    <input type="hidden" name="tel" value="123456789"/>
    <input type="hidden" name="country" value="GB"/>
    
    <input type="submit" class="payButton"  value="Process Payment"/></div>
        
    </form>

