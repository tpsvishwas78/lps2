@extends('layouts.usermaster')

@section('content')
<?php
$user=Auth::user();
?>
<div class="clearfix"></div>
	<!--BANNER-SECTION-START-->
	<section class="banner-detail">
       <h1 class="hedding-detail">Venues</h1>
    </section>
	<!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
	<!--account-setting-section-start-->
	<section class="account-setting-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 p-0">
					@include('layouts.sidebar')
					<div class="col-sm-8">
					  <div class="col-sm-12 p-0">
					  	<div class="search-fields dashboard-game">
						<form method="GET" action="">
							
							<div class="selectcity-town">
								<input type="text" name="search" value="{{ Request::get('search') }}" placeholder="Type Keyword">
							</div>
							<button class="search"><img src="{{url('')}}/images/icons/search.png">SEARCH </button>
						</form>
					</div>
					<div class="map-sports">
							<div id="map"></div>
					</div>
					
					<div class="map-toggle-button">
						<p>SHOW MAP</p>
						<label class="switch">
						  <input class="switch-button" type="checkbox">
						  <span class="slider-button round"></span>
						</label>
					</div>
				</div>
				<div class="clearfix"></div>
					  <div class="listing-games">
						  <h3>Venues</h3>

						  @foreach ($venues as $venue)

						<?php 
						$followes=\DB::table('venue_followers')->where('venue_id',$venue->id)->count();
						$mefollow=\DB::table('venue_followers')->where('user_id',$user->id)->where('venue_id',$venue->id)->count();
						
						?>
						<div class="design-list">
							
							<div class="col-sm-9">
								<div class="info-game">
										<a href="{{ url('') }}/venue/{{ $venue->slug }}">
									<p>{{ $venue->name }}</p>
								</a>
									<p class="dis-game">{{$venue->location}} </p>
									<div class="follow">
										@csrf
										@if ($mefollow > 0)
										<button id="follow{{$venue->id}}" follow="Following" vid="{{$venue->id}}" class="follow setfollow">Following</button>
										@else
										<button id="follow{{$venue->id}}" follow="Follow" vid="{{$venue->id}}" class="follow setfollow">Follow</button>
										@endif
										
										
										<span><span id="followcount{{$venue->id}}">{{$followes}}</span> followers</span>
									</div>
								</div>
							</div>
							<div class="col-sm-3 p-0">
							<div class="game-img">
									<a href="{{ url('') }}/venue/{{ $venue->slug }}">
									@if ($venue->image)
										<img src="{{url('')}}/upload/images/{{$venue->image}}">
									@else
										<img src="{{url('')}}/images/no-user-Image.png">
									@endif
								</a>
								
							</div>
							</div>
						
						</div>
						@endforeach

					</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--account-setting-section-end-->
	
<script>

$(".setfollow").click(function(){
	var follow=$(this).attr('follow');
	var vid=$(this).attr('vid');
	console.log('follow',follow);

	var _token = $('input[name="_token"]').val();
                 $.ajax({
                  url:"{{ url('') }}/venue/follow",
                  method:"POST",
				  dataType:'Json',
                  data:{follow:follow,id:vid, _token:_token},
                  success:function(data){
					  console.log(data);
                      $('#follow'+vid).html(data.follow);
					  $('#follow'+vid).attr('follow',data.follow);
					  $('#followcount'+vid).html(data.followes);
                            
                  }
                 });
  
});
</script>

	  <script>
		  function initAutocomplete() {
    var locations = [
        @foreach ($venues as $venue)
		  ['{{$venue->name}}', {{$venue->latitude}}, {{$venue->longitude}}, 1],
		  @endforeach
        
    ];

    window.map = new google.maps.Map(document.getElementById('map'), {
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var bounds = new google.maps.LatLngBounds();

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map
        });

        bounds.extend(marker.position);

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));
    }

    map.fitBounds(bounds);

    var listener = google.maps.event.addListener(map, "idle", function () {
        map.setZoom(3);
        google.maps.event.removeListener(listener);
    });
}

	  </script>
    @endsection