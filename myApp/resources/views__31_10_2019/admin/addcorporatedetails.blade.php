@extends('admin.layouts.master')
@section('content')
<?php
@$images=\DB::table('corporate_images')->where('cp_id',@$edit->id)->get();
@$videos=\DB::table('corporate_videos')->where('cp_id',@$edit->id)->get();
@$faqs=\DB::table('corporate_faq')->where('cp_id',@$edit->id)->get();
?>
<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
    <div class="mr-auto">
    <h1>Add Event Details</h1>
    </div>
    </div>
</header>
<!--END PAGE HEADER -->
<section class="page-content container-fluid">

<div class="row">
    <div class="col-md-12">
<div class="card">
        
        <form  class="form-horizontal" action="{{ url('admin/submitCorporate') }}" method="POST" enctype="multipart/form-data">
            @csrf
    <div class="card-body">
            @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
                @php
                    Session::forget('success');
                @endphp
            </div>
            @endif

            <div class="form-body">
                    <div class="form-group row">
                            <label class="control-label text-right col-md-3">Choose Layout</label>
                            <div class="col-md-5">
                             <div class="row">
                            <div class="custom-control custom-radio custom-control-inline1 col-md-6">
                                    <input type="radio"   id="customRadioInline3" name="layout" class="custom-control-input chooselayout" value="1" 
                                    @if (@$edit)
                                        @if (@$edit->layout==1)
                                        checked
                                        @endif
                                    @else
                                    checked
                                    @endif>
                                    <label class="custom-control-label" for="customRadioInline3">Layout One</label>
                                    <img width="100" src="{{ url('') }}/images/corporate-corporate-sports-days-layout.png" alt="">
                                </div>
                            
                                <div class="custom-control custom-radio custom-control-inline1 col-md-6">
                                        <input type="radio" id="customRadioInline4" name="layout" class="custom-control-input chooselayout" value="2" @if (@$edit) @if (@$edit->layout==2)
                                        checked
                                        @endif @endif>
                                        <label class="custom-control-label" for="customRadioInline4">Layout Two</label>
                                        <img width="100" src="{{ url('') }}/images/office-christmas-party-layout.jpg" alt="">
                                </div>
                            </div>
                            </div>
                            @if ($errors->has('layout'))
                            <span class="text-danger">{{ $errors->first('layout') }}</span>
                        @endif
                        </div>
                    <div class="form-group row">
                            <label class="control-label text-right col-md-3">Select Category</label>
                            <div class="col-md-5">
                                <select required name="category" id="category" class="form-control" >
                                    <option value="">select</option>
                                    @foreach ($allcorporatecatgory as $allcorporatecat)
                                    <option @if (@$edit->category_id==$allcorporatecat->id) selected @endif value="{{ $allcorporatecat->id }}">{{ $allcorporatecat->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('category'))
                                <span class="text-danger">{{ $errors->first('category') }}</span>
                                @endif
                            </div>
                        </div>                  
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Title</label>
                    <div class="col-md-5">
                        <input required type="text" placeholder="Title" id="title" name="title" class="form-control" autocomplete="city-name" value="{{ old('title') ? old('title') : @$edit->title }}">
                        @if ($errors->has('title'))
                        <span class="text-danger">{{ $errors->first('title') }}</span>
                        @endif
                    </div>
                </div> 
                <div class="form-group row">
                        <label class="control-label text-right col-md-3">Feature Image</label>
                        <div class="col-md-5">
                        <input class="form-control"  type="file" name="feature_image" >
                        @if(isset($edit->feature_image))
                            <img src="{{ url('upload/images') }}/{{ $edit->feature_image }}" alt="" width="115">
                        @endif
                        @if ($errors->has('feature_image'))
                            <span class="text-danger">{{ $errors->first('feature_image') }}</span>
                        @endif
                        </div>
                    </div>              

                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Banner Image</label>
                    <div class="col-md-5">
                    <input class="form-control"  type="file" name="banner" >
                    @if(isset($edit->banner))
                        <img src="{{ url('upload/images') }}/{{ $edit->banner }}" alt="" width="115">
                    @endif
                    @if ($errors->has('banner'))
                        <span class="text-danger">{{ $errors->first('banner') }}</span>
                    @endif
                    </div>
                </div>
                <div class="form-group row">
                        <label class="control-label text-right col-md-3">Banner Description</label>
                        <div class="col-md-5">
                        <textarea placeholder="Banner Description" class="form-control" name="banner_description">@if(@$edit->banner_description){{ @$edit->banner_description }}@endif</textarea>
                        </div>
                    </div>

                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Main Video</label>
                    <div class="col-md-5">
                    <textarea required placeholder="Please copy youtube embeded video and paste here" class="form-control" name="video">@if(@$edit->main_video){{ @$edit->main_video }}@endif</textarea>
                    @if(@$edit->main_video)
                        {!! @$edit->main_video !!}
                    @endif
                   
                    @if ($errors->has('video'))
                        <span class="text-danger">{{ $errors->first('video') }}</span>
                    @endif
                    </div>
                </div>
                <div class="form-group row" id='mydescription'>
                    <label class="control-label text-right col-md-3">Description</label>
                    <div class="col-md-9">
                    <textarea required class="form-control cke_wrapper" id="editor1" name="description">{{ @$edit->description }}</textarea>
                    @if ($errors->has('description'))
                        <span class="text-danger">{{ $errors->first('description') }}</span>
                    @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Secound Title</label>
                    <div class="col-md-5">
                        <input type="text" required placeholder="Secound Title" id="secound_title" name="secound_title" class="form-control" autocomplete="city-name" value="{{ old('secound_title') ? old('secound_title') : @$edit->sec_title }}">
                        @if ($errors->has('secound_title'))
                        <span class="text-danger">{{ $errors->first('secound_title') }}</span>
                        @endif
                    </div>
                </div>    
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Secound Description</label>
                    <div class="col-md-9">
                    <textarea required class="form-control cke_wrapper" id="editor2" name="secound_description">{{ @$edit->sec_description }}</textarea>
                    @if ($errors->has('secound_description'))
                        <span class="text-danger">{{ $errors->first('secound_description') }}</span>
                    @endif
                    </div>
                </div>
                
                    
                    <div class="form-group row" id="mygallery">
                            <label class="control-label text-right col-md-3">Gallery Images</label>
                            <div class="col-md-5">
                                    <div class="controls">
               
                                            <div class="entry input-group col-xs-3">
                                              
                                           
                                              <input class="btn btn-default" name="gallary[]" type="file">
                                              <span class="input-group-btn">
                                            <button class="btn btn-success btn-add" type="button">
                                                              <span class="zmdi zmdi-plus zmdi-hc-fw"></span>
                                              </button>
                                              </span>
                                            </div>
                                         
                                        </div>
                        @if (@$images)
                            @foreach (@$images as $image)
                            <p>
                            <img width="100" src="{{ url('') }}/upload/images/{{ $image->image }}" alt="">
                            <a href="javascript:void(0);"><i id="{{ $image->id }}" class="zmdi zmdi-delete zmdi-hc-fw removeimg"></i></a>
                            </p>
                            @endforeach
                        @endif
                        
                                        
                            </div>
                        </div>

                        <div class="form-group row" id="myvideos">
                            <label class="control-label text-right col-md-3">Videos</label>
                            <div class="col-md-9">
                                    <div class="videocontrols">
               
                                            <div class="entry-video input-group col-xs-3">
                                              
                                                <input class="form-control" placeholder="Title" name="videotitle[]" type="text">
                                                <textarea placeholder="Please copy youtube embeded video and paste here" class="form-control" name="videos[]"></textarea>
                                              <span class="input-group-btn">
                                            <button class="btn btn-success btn-add-video" type="button">
                                                              <span class="zmdi zmdi-plus zmdi-hc-fw"></span>
                                              </button>
                                              </span>
                                            </div>
                                    </div>
                            </div>
                        </div>
                        @if (@$videos)
                        @foreach ($videos as $video)
                        <div class="form-group row myvideo">
                            <div class="col-md-12">
                                <div class="row">
                                <div class="col-md-2">{{ $video->title }}</div>
                                <div class="col-md-7">{!! $video->video !!}</div>
                                <div class="col-md-3"><a href="javascript:void(0);"><i id="{{ $video->id }}" class="zmdi zmdi-delete zmdi-hc-fw removevideo"></i></a></div>
                            </div>
                            </div>
                        </div>
                        @endforeach
                        @endif

                        <div class="form-group row" id="myfaq" style="display:none;">
                                <label class="control-label text-right col-md-3">FAQ</label>
                                <div class="col-md-9">
                                        <div class="faqcontrols">
                   
                                                <div class="entry-faq input-group col-xs-3">
                                                  
                                                    <textarea placeholder="FAQ Title" class="form-control" name="faq[]"></textarea>
                                                  <span class="input-group-btn">
                                                <button class="btn btn-success btn-add-faq" type="button">
                                                                  <span class="zmdi zmdi-plus zmdi-hc-fw"></span>
                                                  </button>
                                                  </span>
                                                </div>
                                        </div>
                                </div>
                            </div>
                            @if (@$faqs)
                        @foreach ($faqs as $faq)
                        <div class="form-group row myfaqs">
                            <div class="col-md-12">
                                <div class="row">
                                        <div class="col-md-3"></div>
                                <div class="col-md-6">{{ $faq->title }}</div>
                                <div class="col-md-3"><a href="javascript:void(0);"><i id="{{ $faq->id }}" class="zmdi zmdi-delete zmdi-hc-fw removefaq"></i></a></div>
                            </div>
                            </div>
                        </div>
                        @endforeach
                        @endif
 
                </div>
            
        </div>
        <div class="card-footer bg-light">
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="offset-sm-3 col-md-5">
                                <input type="hidden" name="id" value="{{@$edit->id}}">
                                <button type="submit" class="btn btn-primary btn-rounded">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>
</div>

</section>



<script src="{{ url('') }}/admin/vendor/ckeditor/ckeditor.js"></script>
{{-- <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script> --}}

<script>
    
    CKEDITOR.replace( 'editor1' );
    CKEDITOR.replace( 'editor2' );
   
    //CKEDITOR.replace( 'editor3' );
    $(function()
{
    $(document).on('click', '.removeimg', function(e)
    {

         var id=$(this).attr('id');
         $(this).closest('p').remove();
        var _token = $('input[name="_token"]').val();
                $.ajax({
                 url:"{{ url('') }}/admin/deletecorporategalleryimage",
                 method:"POST",
                 data:{id:id, _token:_token},
                 success:function(data){
                    
                 }
                });
        
    });
    $(document).on('click', '.removevideo', function(e)
    {

         var id=$(this).attr('id');
         $(this).closest('.myvideo').remove();
        var _token = $('input[name="_token"]').val();
                $.ajax({
                 url:"{{ url('') }}/admin/deletecorporatevideo",
                 method:"POST",
                 data:{id:id, _token:_token},
                 success:function(data){
                    
                 }
                });
        
    });
    $(document).on('click', '.removefaq', function(e)
    {

         var id=$(this).attr('id');
         $(this).closest('.myfaqs').remove();
        var _token = $('input[name="_token"]').val();
                $.ajax({
                 url:"{{ url('') }}/admin/deletecorporatefaq",
                 method:"POST",
                 data:{id:id, _token:_token},
                 success:function(data){
                    
                 }
                });
        
    });
    
    
    
    $(document).on('click', '.btn-add', function(e)
    {
        e.preventDefault();

        var controlForm = $('.controls:first'),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="zmdi zmdi-minus zmdi-hc-fw"></span>');
    }).on('click', '.btn-remove', function(e)
    {
      $(this).parents('.entry:first').remove();

		e.preventDefault();
		return false;
	});
});

$(function()
{
    $(document).on('click', '.btn-add-video', function(e)
    {
        e.preventDefault();

        var controlForm = $('.videocontrols:first'),
            currentEntry = $(this).parents('.entry-video:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry-video:not(:last) .btn-add-video')
            .removeClass('btn-add-video').addClass('btn-remove-video')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="zmdi zmdi-minus zmdi-hc-fw"></span>');
    }).on('click', '.btn-remove-video', function(e)
    {
      $(this).parents('.entry-video:first').remove();

		e.preventDefault();
		return false;
	});
});

$(function()
{
    $(document).on('click', '.btn-add-faq', function(e)
    {
        e.preventDefault();

        var controlForm = $('.faqcontrols:first'),
            currentEntry = $(this).parents('.entry-faq:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry-faq:not(:last) .btn-add-faq')
            .removeClass('btn-add-faq').addClass('btn-remove-faq')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="zmdi zmdi-minus zmdi-hc-fw"></span>');
    }).on('click', '.btn-remove-faq', function(e)
    {
      $(this).parents('.entry-faq:first').remove();

		e.preventDefault();
		return false;
	});
});
$("document").ready(function() {
    var layout=$('input[name="layout"]:checked').val();
  if(layout==1){
    $('#mydescription').show();
    $('#myvideos').show();
    $('#myfaq').hide();
  }else{
    $('#mydescription').hide();
    $('#myvideos').hide();
    $('#myfaq').show();
  }
     
});
$(".chooselayout").click(function(){
  var layout=$('input[name="layout"]:checked').val();
  if(layout==1){
    $('#mydescription').show();
    $('#myvideos').show();
    $('#myfaq').hide();
  }else{
    $('#mydescription').hide();
    $('#myvideos').hide();
    $('#myfaq').show();
  }
});
</script>

@endsection