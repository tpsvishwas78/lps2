@extends('admin.layouts.master')
@section('content')


<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h1>Manage All Games</h1>
        </div>
    </div>
</header>
<!--END PAGE HEADER -->

<section class="page-content container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                        @if(Session::has('success'))

                        <div class="alert alert-success">
                
                            {{ Session::get('success') }}
                
                            @php
                
                                Session::forget('success');
                
                            @endphp
                
                        </div>
                
                        @endif
                    <div class="card-body">
                        <table id="bs4-table" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Venue</th>
                                    <th>Price</th>
                                    <th>Start Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($games as $game)
                                <?php
@$repeat=\DB::table('repeat_games')->where('game_id',$game->id)->orderBy('game_start_date','DESC')->first();
                                ?>
                                <tr>
                                    <td>{{$game->title}}</td>
                                    <td>{{$game->name}}</td>
                                    <td>
                                        @if ($game->payment==3)
                                            Free
                                        @else
                                        @if ($game->currency=='USD')
                                        ${{ $game->price }} 
                                        @else
                                        £{{ $game->price }}
                                        @endif
                                        @endif
                                    </td>
                                    <td>{{$game->game_date}}</td>
                                    <td>
                                            @if ($game->status==1)
                                            <a onClick="return confirm('Are you sure you want to Deactive?');" style="color:#fff; padding-top:9px;" href="{{ url('admin/gamestatus') }}/{{ $game->id }}" class="btn btn-success btn-sm">Active</a>
                                            @elseif ($game->status==0)
                                            <a onClick="return confirm('Are you sure you want to Active?');" style="color:#fff; padding-top:9px;" href="{{ url('admin/gamestatus') }}/{{ $game->id }}" class="btn btn-danger btn-sm">Deactive</a>
                                            @else
                                            <a onClick="return confirm('Are you sure you want to Active?');" style="color:#fff; padding-top:9px;" href="{{ url('admin/gamestatus') }}/{{ $game->id }}" class="btn btn-info btn-sm">Pending Approval</a>
                                            @endif
                                            
                                        </td>
                                    <td>
                                            <a target="_blank" href="{{ url('') }}/games/{{ $game->slug }}/{{ @$repeat->id }}"><i class="zmdi zmdi-eye zmdi-hc-fw"></i></a>
                                    <a href="{{ url('admin/addgame') }}/{{ $game->id }}"><i class="zmdi zmdi-edit zmdi-hc-fw"></i></a>
                                    <a href="{{ url('admin/gamedelete') }}/{{ $game->id }}" onClick="return confirm('Are you sure you want to Delete?');"><i class="zmdi zmdi-delete zmdi-hc-fw"></i></a>
                                        
                                        </td>
                                </tr>
                                @endforeach
                            </tbody>
                            
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection