@extends('admin.layouts.master')
@section('content')


<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
    <div class="mr-auto">
    <h1>Add Sport</h1>
    </div>
    </div>
</header>
<!--END PAGE HEADER -->
<section class="page-content container-fluid">

<div class="row">
    <div class="col-md-12">
<div class="card">
        
        <form  class="form-horizontal" action="{{ url('admin/submitSport') }}" method="POST" enctype="multipart/form-data">
            @csrf
    <div class="card-body">
            @if(Session::has('success'))

            <div class="alert alert-success">
    
                {{ Session::get('success') }}
    
                @php
    
                    Session::forget('success');
    
                @endphp
    
            </div>
    
            @endif
            <div class="form-body">
                    <div class="form-group row">
                            <label class="control-label text-right col-md-3">Main Sport</label>
                            <div class="col-md-5">
                               <select required name="main_id" id="main_id" class="form-control">
                                   <option value="">Select</option>
                                   @foreach ($sports as $sport)
                                   <option @if (@$edit)
                                   @if ($edit->main_id==$sport->id)
                                   selected
                               @endif
                                   @endif  value="{{ $sport->id }}">{{ $sport->name }}</option>
                                   @endforeach
                                   
                               </select>
                               
                            </div>
                        </div>
                
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Name</label>
                    <div class="col-md-5">
                        <input type="text" placeholder="Name" id="name" name="name" class="form-control" autocomplete="city-name" value="{{ old('name') ? old('name') : @$edit->name }}">
                        @if ($errors->has('name'))
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Description</label>
                    <div class="col-md-5">
                    <textarea class="form-control" name="description">{{ @$edit->description }}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Image</label>
                    <div class="col-md-5">
                    <input class="form-control"  type="file" name="image" >
                    @if (@$edit->image)
                    <img width="200" src="{{ url('') }}/upload/images/{{ @$edit->image }}" alt="">
                @endif
                    </div>
                   
                </div>
                <div class="form-group row">
                        <label class="control-label text-right col-md-3">Sport Order</label>
                        <div class="col-md-5">
                            <input type="text" placeholder="Order" id="sport_order" name="sport_order" class="form-control" autocomplete="city-name" value="{{ old('sport_order') ? old('sport_order') : @$edit->sport_order }}">
                            @if ($errors->has('sport_order'))
                            <span class="text-danger">{{ $errors->first('sport_order') }}</span>
                        @endif
                        </div>
                    </div>
 
                </div>
            
        </div>
        <div class="card-footer bg-light">
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="offset-sm-3 col-md-5">
                                <input type="hidden" name="id" value="{{@$edit->id}}">
                                <button type="submit" class="btn btn-primary btn-rounded">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>
</div>
</section>


@endsection