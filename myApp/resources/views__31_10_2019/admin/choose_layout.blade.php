@extends('admin.layouts.master')
@section('content')


<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
    <div class="mr-auto">
    <h1>Choose Kids Party Details Page Laout</h1>
    </div>
    </div>
</header>
<!--END PAGE HEADER -->
<section class="page-content container-fluid">

<div class="row">
    <div class="col-md-12">
<div class="card">
        
        <form  class="form-horizontal" action="{{ url('admin/add-kids-party-details') }}" method="GET" enctype="multipart/form-data">
           
    <div class="card-body">
           
            <div class="form-body">               

                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Choose Layout</label>
                    <div class="col-md-5">
                     <div class="row">
                    <div class="custom-control custom-radio custom-control-inline1 col-md-6">
                            <input type="radio" id="customRadioInline3" name="layout" class="custom-control-input chooselayout" value="1" checked>
                            <label class="custom-control-label" for="customRadioInline3">Layout One</label>
                            <img src="{{ url('') }}/images/soft-play-hire-layout.jpg" alt="">
                        </div>
                    
                        <div class="custom-control custom-radio custom-control-inline1 col-md-6">
                                <input type="radio" id="customRadioInline4" name="layout" class="custom-control-input chooselayout" value="2">
                                <label class="custom-control-label" for="customRadioInline4">Layout Two</label>
                                <img src="{{ url('') }}/images/Sports-Party-layout.jpg" alt="">
                        </div>
                    </div>
                    </div>
                   
                </div>
 
                </div>
            
        </div>
        <div class="card-footer bg-light">
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="offset-sm-3 col-md-5">
                                <button type="submit" class="btn btn-primary btn-rounded">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>
</div>
</section>
<script>

$(".chooselayout").click(function(){
  var layout=$(this).val();
  if(layout==1){
    $('form').attr('action', "{{ url('admin/add-kids-party-details') }}" );
  }else{
    $('form').attr('action', "{{ url('admin/add-party-details') }}" );
  }
});
</script>
@endsection