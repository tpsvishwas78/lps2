@extends('admin.layouts.master')
@section('content')


<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
    <div class="mr-auto">
    <h1>Add Kid Party Details</h1>
    </div>
    </div>
</header>
<!--END PAGE HEADER -->
<section class="page-content container-fluid">

<div class="row">
    <div class="col-md-12">
<div class="card">
        
        <form  class="form-horizontal" action="{{ url('admin/submitKidsDeials') }}" method="POST" enctype="multipart/form-data">
            @csrf
    <div class="card-body">
            @if(Session::has('success'))

            <div class="alert alert-success">
    
                {{ Session::get('success') }}
    
                @php
    
                    Session::forget('success');
    
                @endphp
    
            </div>
    
            @endif
            <div class="form-body">
                    <div class="form-group row">
                            <label class="control-label text-right col-md-3">Select Category</label>
                            <div class="col-md-5">
                                <select name="category" id="category" class="form-control" >
                                    <option value="">select</option>
                                    @foreach ($allcorporatecatgory as $allcorporatecat)
                                    <option @if (@$edit->category_id==$allcorporatecat->id) selected @endif value="{{ $allcorporatecat->id }}">{{ $allcorporatecat->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('category'))
                                <span class="text-danger">{{ $errors->first('category') }}</span>
                                @endif
                            </div>
                        </div>  
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Title</label>
                    <div class="col-md-5">
                        <input type="text" placeholder="Name" id="name" name="name" class="form-control" autocomplete="city-name" value="{{ old('name') ? old('name') : @$edit->title }}">
                        @if ($errors->has('name'))
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    @endif
                    </div>
                </div>
                <div class="form-group row">
                        <label class="control-label text-right col-md-3">Image</label>
                        <div class="col-md-5">
                        <input class="form-control"  type="file" name="image" >
                        @if(isset($edit->image))
                            <img src="{{ url('upload/images') }}/{{ $edit->image }}" alt="" width="115">
                        @endif
                        @if ($errors->has('image'))
                            <span class="text-danger">{{ $errors->first('image') }}</span>
                        @endif
                        </div>
                    </div>
                    <div class="form-group row">
                            <label class="control-label text-right col-md-3">Description</label>
                            <div class="col-md-9">
                                <textarea class="form-control cke_wrapper" name="description"id="editor1" cols="30" rows="10">{{ old('description') ? old('description') : @$edit->description }}</textarea>
                                @if ($errors->has('description'))
                                <span class="text-danger">{{ $errors->first('description') }}</span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group row">
                                <label class="control-label text-right col-md-3">Status</label>
                                <div class="col-md-5">
                                <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio"   id="customRadioInline3" name="status" class="custom-control-input" value="1" 
                                        @if (@$edit)
                                            @if (@$edit->status==1)
                                            checked
                                            @endif
                                        @else
                                        checked
                                        @endif>
                                        <label class="custom-control-label" for="customRadioInline3">Active</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadioInline4" name="status" class="custom-control-input" value="0" @if (@$edit) @if (@$edit->status==0)
                                            checked
                                            @endif @endif>
                                            <label class="custom-control-label" for="customRadioInline4">Deactive</label>
                                    </div>
                                </div>
                                @if ($errors->has('status'))
                                <span class="text-danger">{{ $errors->first('status') }}</span>
                            @endif
                            </div>
 
                </div>
            
        </div>
        <div class="card-footer bg-light">
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="offset-sm-3 col-md-5">
                                <input type="hidden" name="id" value="{{@$edit->id}}">
                                <button type="submit" class="btn btn-primary btn-rounded">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>
</div>
</section>
<script src="{{ url('') }}/admin/vendor/ckeditor/ckeditor.js"></script>

<script>
    CKEDITOR.replace( 'editor1' );
    </script>

@endsection