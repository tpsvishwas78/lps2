@extends('admin.layouts.master')
@section('content')


<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
    <div class="mr-auto">
    <h1>Edit Kid Parties Section</h1>
    </div>
    </div>
</header>
<!--END PAGE HEADER -->

<section class="page-content container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                        @if(Session::has('success'))

                        <div class="alert alert-success">
                
                            {{ Session::get('success') }}
                
                            @php
                
                                Session::forget('success');
                
                            @endphp
                
                        </div>
                
                        @endif
                    <div class="card-body">
                        <table id="bs4-table" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Image</th>
                                    <th>Layout</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($venues as $venue)
                                <tr>                                
                                    <td>{{$venue->name}}</td>
                                    <td>{{ $venue->description }}</td>
                                    <td>
                                        <img src="{{url('upload/images')}}/{{ $venue->image }}" alt="" width="115">
                                        </td>
                                        <td>
                                            @if ($venue->layout==1)
                                                Layout One
                                            @else
                                            Layout Two
                                            @endif
                                        </td>
                                <td>
                                    @if ($venue->status==1)
                                    <a onClick="return confirm('Are you sure you want to Deactive?');" style="color:#fff; padding-top:9px;" href="{{ url('admin/children-party-category-change-status') }}/{{ $venue->id }}" class="btn btn-danger btn-sm">Deactive Now</a>
                                    @else
                                    <a onClick="return confirm('Are you sure you want to Active?');" style="color:#fff; padding-top:9px;" href="{{ url('admin/children-party-category-change-status') }}/{{ $venue->id }}" class="btn btn-success btn-sm">Active Now</a>
                                    @endif
                                    
                                </td>
                                    <td>
                                    <a href="{{ url('admin/add-children-party-category') }}/{{ $venue->id }}"><i class="zmdi zmdi-edit zmdi-hc-fw"></i></a>
                                    <a href="{{ url('admin/delete-children-party-category') }}/{{ $venue->id }}" onClick="return confirm('Are you sure you want to Delete?');"><i class="zmdi zmdi-delete zmdi-hc-fw"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection