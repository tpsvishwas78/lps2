@extends('admin.layouts.master')
@section('content')

<!--START PAGE HEADER -->
<header class="page-header">
        <div class="d-flex align-items-center">
        <div class="mr-auto">
        <h1>Manage All Users</h1>
        </div>
        </div>
        </header>
        <!--END PAGE HEADER -->
        <section class="page-content container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                                @if(Session::has('success'))

                                <div class="alert alert-success">
                        
                                    {{ Session::get('success') }}
                        
                                    @php
                        
                                        Session::forget('success');
                        
                                    @endphp
                        
                                </div>
                        
                                @endif
                            <div class="card-body">
                                <table id="bs4-table" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Mobile</th>
                                            <th>Added Date</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($users as $user)
                                        <tr>
                                        <td>{{$user->first_name}} {{$user->last_name}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>{{$user->phone_number}}</td>
                                            <td>{{$user->created_at}}</td>
                                            <td>
                                                    @if ($user->status==1)
                                                    <a onClick="return confirm('Are you sure you want to Deactive?');" style="color:#fff; padding-top:9px;" href="{{ url('admin/userstatus') }}/{{ $user->id }}" class="btn btn-danger btn-sm">Deactive Now</a>
                                                    @else
                                                    <a onClick="return confirm('Are you sure you want to Active?');" style="color:#fff; padding-top:9px;" href="{{ url('admin/userstatus') }}/{{ $user->id }}" class="btn btn-success btn-sm">Active Now</a>
                                                    @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    
                                </table>


                            </div>
                        </div>
                    </div>
                </div>
            </section>

@endsection