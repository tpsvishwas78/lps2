@extends('admin.layouts.master')
@section('content')
<?php

@$images=\DB::table('cp_images')->where('cp_id',@$edit->id)->get();
?>
<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
    <div class="mr-auto">
    <h1>Add Children Party Venues</h1>
    </div>
    </div>
</header>
<!--END PAGE HEADER -->
<section class="page-content container-fluid">

<div class="row">
    <div class="col-md-12">
<div class="card">
        
        <form  class="form-horizontal" action="{{ url('admin/submitAddPartyVenue') }}" method="POST" enctype="multipart/form-data">
            @csrf
    <div class="card-body">
            @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
                @php
                    Session::forget('success');
                @endphp
            </div>
            @endif

            <div class="form-body">
                    <div class="form-group row">
                            <label class="control-label text-right col-md-3">Select Category</label>
                            <div class="col-md-5">
                                <select name="category" id="category" class="form-control" >
                                    <option value="">select</option>
                                    <option @if (@$edit->category==1) selected @endif value="1">Children’s Parties Venues</option>
                                    <option @if (@$edit->category==2) selected @endif value="2">Children’s Coaching</option>
                                    <option @if (@$edit->category==3) selected @endif value="3">Kids 5a Side Language</option>
                                </select>
                                @if ($errors->has('category'))
                                <span class="text-danger">{{ $errors->first('category') }}</span>
                                @endif
                            </div>
                        </div>                  
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Title</label>
                    <div class="col-md-5">
                        <input type="text" placeholder="Title" id="title" name="title" class="form-control" autocomplete="city-name" value="{{ old('title') ? old('title') : @$edit->title }}">
                        @if ($errors->has('title'))
                        <span class="text-danger">{{ $errors->first('title') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Venue Title</label>
                    <div class="col-md-5">
                    <input type="text" placeholder="Venue Title" id="venue_title" name="venue_title" class="form-control" autocomplete="venue_title" value="{{ old('venue_title') ? old('venue_title') : @$edit->venue_title }}">
                    @if ($errors->has('venue_title'))
                    <span class="text-danger">{{ $errors->first('venue_title') }}</span>
                    @endif
                    </div>
                </div>
                <div class="form-group row">
                        <label class="control-label text-right col-md-3">Venue Address</label>
                        <div class="col-md-5">
                        <input type="text" placeholder="Venue Address" id="txtPlaces" name="venue_address" class="form-control" autocomplete="off" value="{{ old('venue_address') ? old('venue_address') : @$edit->venue_address }}" autofocus>
                        {{-- <input type="hidden" name="latitude" id="latitude" value="{{ old('latitude') ? old('latitude') : @$edit->latitude }}">
                        <input type="hidden" name="longitude" id="longitude" value="{{ old('longitude') ? old('longitude') : @$edit->longitude }}"> --}}
                        @if ($errors->has('venue_address'))
                    <span class="text-danger">{{ $errors->first('venue_address') }}</span>
                    @endif
                        </div>
                </div>

                <div class="form-group row">
                        <label class="control-label text-right col-md-3">Latitude</label>
                        <div class="col-md-5">
                        <input type="text" placeholder="Latitude" class="form-control" name="latitude" id="latitude" value="{{ old('latitude') ? old('latitude') : @$edit->latitude }}">
                        @if ($errors->has('latitude'))
                    <span class="text-danger">{{ $errors->first('latitude') }}</span>
                    @endif
                        </div>
                </div>

                <div class="form-group row">
                        <label class="control-label text-right col-md-3">Longitude</label>
                        <div class="col-md-5">
                        <input type="text" class="form-control" placeholder="Longitude" name="longitude" id="longitude" value="{{ old('longitude') ? old('longitude') : @$edit->longitude }}">
                        @if ($errors->has('longitude'))
                    <span class="text-danger">{{ $errors->first('longitude') }}</span>
                    @endif
                        </div>
                </div>

                <div class="form-group row">
                        <label class="control-label text-right col-md-3">Postcode</label>
                        <div class="col-md-5">
                        <input type="text" placeholder="Postcode"  id="postcode" name="postcode" class="form-control" autocomplete="off" value="{{ old('postcode') ? old('postcode') : @$edit->postcode }}" >
                        @if ($errors->has('postcode'))
                    <span class="text-danger">{{ $errors->first('postcode') }}</span>
                    @endif
                        </div>
                </div>

                <div class="form-group row">
                        <label class="control-label text-right col-md-3">Venue Facility</label>
                        <div class="col-md-5">
                        @foreach ($facility as $item)
<?php
@$check=\DB::table('cp_venue_facility')->where('cp_id',@$edit->id)->where('vf_id',$item->id)->first();

?>
                            <p>
                                <input type="checkbox" id="fac_{{ $item->id }}" name="facility[]" 
                                value="{{$item->id}}" @if (@$check->vf_id==$item->id)
                                    checked
                                @endif>
                                <label for="fac_{{$item->id}}">{{$item->facility}}</label>                                
                            </p>
                        @endforeach
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Feature Image</label>
                    <div class="col-md-5">
                    <input class="form-control"  type="file" name="feature_image" >
                    @if(isset($edit->feature_image))
                        <img src="{{ url('upload/images') }}/{{ $edit->feature_image }}" alt="" width="115">
                    @endif
                    @if ($errors->has('feature_image'))
                        <span class="text-danger">{{ $errors->first('feature_image') }}</span>
                    @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Video</label>
                    <div class="col-md-5">
                    <input class="form-control" type="file" name="video" >
                    @if(@$edit->video)
                        <video width="100%" height="auto" controls>
                                <source src="{{ url('') }}/upload/video/{{ @$edit->video }}" type="video/mp4">
                                <source src="{{ url('') }}/upload/video/{{ @$edit->video }}" type="video/ogg">
                              Your browser does not support the video tag.
                              </video>
                    @endif
                    @if ($errors->has('video'))
                        <span class="text-danger">{{ $errors->first('video') }}</span>
                    @endif
                    </div>
                </div>
                <div class="form-group row">
                        <label class="control-label text-right col-md-3">Short Description</label>
                        <div class="col-md-9">
                        <textarea class="form-control"  name="short_description">{{ @$edit->short_description }}</textarea>
                        @if ($errors->has('short_description'))
                        <span class="text-danger">{{ $errors->first('short_description') }}</span>
                    @endif
                        </div>
                    </div>
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Timing Description</label>
                    <div class="col-md-9">
                    <textarea class="form-control cke_wrapper" id="editor2" name="timing_description">
                        @if (@$edit->timing_description)
                            {{ @$edit->timing_description }}
                            @else
                            <p>Training Date and Time: Tuesdays 4:15-5:30pm | Training Location: Chobham Academy, E15 | Match Day: Sundays (kick offs between 10-2pm) Match Location Home: Weavers Fields, E2 | Match Location Away: Dependant on away team</p>
                        @endif
                            

                    </textarea>
                    @if ($errors->has('timing_description'))
                        <span class="text-danger">{{ $errors->first('timing_description') }}</span>
                    @endif
                    </div>
                </div>
                <div class="form-group row">
                        <label class="control-label text-right col-md-3">Box Title</label>
                        <div class="col-md-5">
                            <input type="text" placeholder="Box Title" id="box_title" name="box_title" class="form-control" autocomplete="city-name" value="{{ old('box_title') ? old('box_title') : @$edit->box_title }}">
                            @if ($errors->has('box_title'))
                            <span class="text-danger">{{ $errors->first('box_title') }}</span>
                            @endif
                        </div>
                    </div>
                <div class="form-group row">
                        <label class="control-label text-right col-md-3">Description</label>
                        <div class="col-md-9">
                        <textarea class="form-control cke_wrapper" id="editor1" name="description">
                                @if (@$edit->description)
                                {{ @$edit->description }}
                                @else
                                <p>Experience a series of fun games and challenges for 90 minutes, before the children move inside for food and presentations</p>
                                <ul>
                                        <li>point 1</li>
                                        <li>point 2</li>
                                        <li>point 3 ...</li>
                                        
                                    </ul>
                            @endif
                            
                                    
                        </textarea>
                        @if ($errors->has('description'))
                        <span class="text-danger">{{ $errors->first('description') }}</span>
                    @endif
                        </div>
                    </div>
                    <div class="form-group row">
                            <label class="control-label text-right col-md-3">Opening Hours</label>
                            <div class="col-md-9">
                            <textarea class="form-control cke_wrapper" id="editor3" name="opening_hours">
                                    @if (@$edit->opening_hours)
                                    {{ @$edit->opening_hours }}
                                    @else
                                    <table>
                                            <tbody>
                                                <tr>
                                                    <td>Monday</td>
                                                    <td>09:00 - 23:00</td>
                                                </tr>
                                                <tr>
                                                    <td>Tuesday</td>
                                                    <td>09:00 - 23:00</td>
                                                </tr>
                                                <tr>
                                                    <td>Wednesdau</td>
                                                    <td>09:00 - 23:00</td>
                                                </tr>
                                                <tr>
                                                    <td>Thursday</td>
                                                    <td>09:00 - 23:00</td>
                                                </tr>
                                                <tr>
                                                    <td>Friday</td>
                                                    <td>09:00 - 23:00</td>
                                                </tr>
                                                <tr>
                                                    <td>Saturday</td>
                                                    <td>09:00 - 23:00</td>
                                                </tr>
                                                <tr>
                                                    <td>Sunday</td>
                                                    <td>09:00 - 23:00</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                @endif
                                
                                </textarea>
                                @if ($errors->has('opening_hours'))
                        <span class="text-danger">{{ $errors->first('opening_hours') }}</span>
                    @endif
                            </div>
                        </div>
                    <div class="form-group row">
                            <label class="control-label text-right col-md-3">Venue Images</label>
                            <div class="col-md-5">
                                    <div class="controls">
               
                                            <div class="entry input-group col-xs-3">
                                              
                                           
                                              <input class="btn btn-default" name="gallary[]" type="file">
                                              <span class="input-group-btn">
                                            <button class="btn btn-success btn-add" type="button">
                                                              <span class="zmdi zmdi-plus zmdi-hc-fw"></span>
                                              </button>
                                              </span>
                                            </div>
                                         
                                        </div>
                        @if (@$images)
                            @foreach (@$images as $image)
                            <p>
                            <img width="100" src="{{ url('') }}/upload/images/{{ $image->image }}" alt="">
                            <a href="javascript:void(0);"><i id="{{ $image->id }}" class="zmdi zmdi-delete zmdi-hc-fw removeimg"></i></a>
                            </p>
                            @endforeach
                        @endif
                        
                                        
                            </div>
                        </div>

                
 
                </div>
            
        </div>
        <div class="card-footer bg-light">
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="offset-sm-3 col-md-5">
                                <input type="hidden" name="id" value="{{@$edit->id}}">
                                <button type="submit" class="btn btn-primary btn-rounded">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>
</div>

</section>



<script src="{{ url('') }}/admin/vendor/ckeditor/ckeditor.js"></script>

<script>
    CKEDITOR.replace( 'editor1' );
    CKEDITOR.replace( 'editor2' );
    CKEDITOR.replace( 'editor3' );
    $(function()
{
    $(document).on('click', '.removeimg', function(e)
    {

         var id=$(this).attr('id');
         $(this).closest('p').remove();
        var _token = $('input[name="_token"]').val();
                $.ajax({
                 url:"{{ url('') }}/admin/deletegalleryimage",
                 method:"POST",
                 data:{id:id, _token:_token},
                 success:function(data){
                    
                 }
                });
        
    });
    $(document).on('click', '.btn-add', function(e)
    {
        e.preventDefault();

        var controlForm = $('.controls:first'),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="zmdi zmdi-minus zmdi-hc-fw"></span>');
    }).on('click', '.btn-remove', function(e)
    {
      $(this).parents('.entry:first').remove();

		e.preventDefault();
		return false;
	});
});

</script>

@endsection