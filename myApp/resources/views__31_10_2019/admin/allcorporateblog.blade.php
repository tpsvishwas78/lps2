@extends('admin.layouts.master')
@section('content')


<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
    <div class="mr-auto">
    <h1>Edit Featured Blocks</h1>
    </div>
    </div>
</header>
<!--END PAGE HEADER -->

<section class="page-content container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                        @if(Session::has('success'))

                        <div class="alert alert-success">
                
                            {{ Session::get('success') }}
                
                            @php
                
                                Session::forget('success');
                
                            @endphp
                
                        </div>
                
                        @endif
                    <div class="card-body">
                        <table id="bs4-table" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Category</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($allcorporateblogs as $allcorporateblog)
                                <?php
                    @$category=\DB::table('corporate_category')->where('id',$allcorporateblog->category_id)->first();
                                ?>
                                <tr>
                                        <td>
                                                @if ($allcorporateblog->image)
                                                <img width="70" src="{{ url('') }}/upload/images/{{ $allcorporateblog->image }}" alt=""> 
                                             @else
                                             <img width="70" src="{{ url('') }}/images/no-user-Image.png" alt="">  
                                             @endif
                                        </td>
                                    <td>{{$allcorporateblog->title}}</td>
                                    <td>{{ @$category->name }}</td>
                                    <td>
                                    <a href="{{ url('admin/addcorporateblog') }}/{{ $allcorporateblog->id }}"><i class="zmdi zmdi-edit zmdi-hc-fw"></i></a>
                                         <a href="{{ url('admin/corporateblogdelete') }}/{{ $allcorporateblog->id }}" onClick="return confirm('Are you sure you want to Delete?');"><i class="zmdi zmdi-delete zmdi-hc-fw"></i></a></td>
                                </tr>
                                @endforeach
                            </tbody>
                            
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection