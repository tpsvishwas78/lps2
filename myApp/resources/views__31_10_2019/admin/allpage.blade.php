@extends('admin.layouts.master')
@section('content')


<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
    <div class="mr-auto">
    <h1>Manage All Pages</h1>
    </div>
    </div>
</header>
<!--END PAGE HEADER -->

<section class="page-content container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                        @if(Session::has('success'))

                        <div class="alert alert-success">
                
                            {{ Session::get('success') }}
                
                            @php
                
                                Session::forget('success');
                
                            @endphp
                
                        </div>
                
                        @endif
                    <div class="card-body">
                        <table id="bs4-table" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Menu</th>
                                    <th>Page Link</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($pages as $page)
                                <?php
                    $menu=\DB::table('menus')->where('id',$page->menu_id)->first();
                                ?>
                                <tr>
                                    <td>{{ $page->name }}</td>
                                <td>
                                        {{ $menu->name }}
                                    
                                </td>
                                <td>
                                    <a target="_blank" href="{{ url('') }}/pages/{{ $menu->slug }}">{{ url('') }}/pages/{{ $menu->slug }}</a>
                                        
                                    
                                </td>
                                    <td>
                                    <a href="{{ url('admin/addpage') }}/{{ $page->id }}"><i class="zmdi zmdi-edit zmdi-hc-fw"></i></a>
                                         <a href="{{ url('admin/pagedelete') }}/{{ $page->id }}" onClick="return confirm('Are you sure you want to Delete?');"><i class="zmdi zmdi-delete zmdi-hc-fw"></i></a></td>
                                </tr>
                                @endforeach
                            </tbody>
                            
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection