@extends('admin.layouts.master')
@section('content')


<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
    <div class="mr-auto">
    <h1>Session Details</h1>
    </div>
    </div>
</header>
<!--END PAGE HEADER -->
<?php
$location=\DB::table('mst_venue')->where('id',$session->venue_id)->first();
$sport=\DB::table('mst_sports')->where('id',$session->sport_id)->first();
?>
<section class="page-content container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                        
                    <div class="card-body">
                            <ul class="list-reset p-t-10">
                                    <li class="p-b-10">
                                        <span class="w-150 d-inline-block">First Name</span>
                                        <span>{{ $session->first_name }}</span>
                                    </li>
                                    <li class="p-b-10">
                                            <span class="w-150 d-inline-block">Last Name</span>
                                            <span>{{ $session->last_name }}</span>
                                    </li>
                                    <li class="p-b-10">
                                            <span class="w-150 d-inline-block">Email</span>
                                            <span>{{ $session->email }}</span>
                                    </li>
                                    <li class="p-b-10">
                                            <span class="w-150 d-inline-block">Phone Number</span>
                                            <span>{{ $session->phone_number }}</span>
                                    </li>
                                    <li class="p-b-10">
                                            <span class="w-150 d-inline-block">Venue</span>
                                            <span>{{ $location->name }}</span>
                                    </li>
                                    <li class="p-b-10">
                                            <span class="w-150 d-inline-block">Sport</span>
                                            <span>{{ $sport->name }}</span>
                                    </li>
                                    <li class="p-b-10">
                                            <span class="w-150 d-inline-block">No of playes</span>
                                            <span>{{ $session->no_of_player }}</span>
                                    </li>
                                    <li class="p-b-10">
                                            <span class="w-150 d-inline-block">Recurring Booking</span>
                                            <span>
                                               @if ($session->is_recurring_booking==1)
                                                   Yes
                                               @else
                                                   No
                                               @endif
                                            </span>
                                    </li>
                                    <li class="p-b-10">
                                            <span class="w-150 d-inline-block">Session Date</span>
                                            <span>{{ date('M d, Y',strtotime($session->date)) }}</span>
                                    </li>
                                    <li class="p-b-10">
                                            <span class="w-150 d-inline-block">Session Time</span>
                                            <span>{{ $session->time }}</span>
                                    </li>
                                    <li class="p-b-10">
                                            <span class="w-150 d-inline-block">Message</span>
                                            <span>{{ $session->message }}</span>
                                    </li>
                                    <li class="p-b-10">
                                            <span class="w-150 d-inline-block">Uploaded Image</span>
                                            <span> 
                                                <a href="{{ url('') }}/upload/images/{{ $session->image }}" target="_blank">
                                                <img width="200" src="{{ url('') }}/upload/images/{{ $session->image }}" alt=""> 
                                            </a>
                                            </span>
                                    </li>
                                    <li class="p-b-10">
                                            <span class="w-150 d-inline-block">Signature</span>
                                            <span> 
                                                <a href="{{ url('') }}/upload/signature/{{ $session->signature }}" target="_blank">
                                                <img width="200" src="{{ url('') }}/upload/signature/{{ $session->signature }}" alt=""> 
                                            </a>
                                            </span>
                                    </li>
                                    
                                </ul>


                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection