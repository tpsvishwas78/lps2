@extends('admin.layouts.master')
@section('content')


<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
    <div class="mr-auto">
    <h1>Edit Events</h1>
    </div>
    </div>
</header>
<!--END PAGE HEADER -->

<section class="page-content container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                        @if(Session::has('success'))

                        <div class="alert alert-success">
                
                            {{ Session::get('success') }}
                
                            @php
                
                                Session::forget('success');
                
                            @endphp
                
                        </div>
                
                        @endif
                    <div class="card-body">
                        <table id="bs4-table" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($allcorporatecatgory as $allcorporatecat)
                                <tr>
                                        <td>
                                                @if ($allcorporatecat->image)
                                                <img width="70" src="{{ url('') }}/upload/images/{{ $allcorporatecat->image }}" alt=""> 
                                             @else
                                             <img width="70" src="{{ url('') }}/images/no-user-Image.png" alt="">  
                                             @endif
                                        </td>
                                    <td>{{$allcorporatecat->name}}</td>
                                <td>
                                    @if ($allcorporatecat->status==1)
                                    <a onClick="return confirm('Are you sure you want to Deactive?');" style="color:#fff; padding-top:9px;" href="{{ url('admin/corporatecatgorystatus') }}/{{ $allcorporatecat->id }}" class="btn btn-danger btn-sm">Deactive Now</a>
                                    @else
                                    <a onClick="return confirm('Are you sure you want to Active?');" style="color:#fff; padding-top:9px;" href="{{ url('admin/corporatecatgorystatus') }}/{{ $allcorporatecat->id }}" class="btn btn-success btn-sm">Active Now</a>
                                    @endif
                                    
                                </td>
                                    <td>
                                    <a href="{{ url('admin/addcorporatecatgory') }}/{{ $allcorporatecat->id }}"><i class="zmdi zmdi-edit zmdi-hc-fw"></i></a>
                                         <a href="{{ url('admin/corporatecatgorydelete') }}/{{ $allcorporatecat->id }}" onClick="return confirm('Are you sure you want to Delete?');"><i class="zmdi zmdi-delete zmdi-hc-fw"></i></a></td>
                                </tr>
                                @endforeach
                            </tbody>
                            
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection