<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Admin | Sign In</title>
	<!-- ================== GOOGLE FONTS ==================-->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500" rel="stylesheet">
	<!-- ======================= GLOBAL VENDOR STYLES ========================-->
	<link rel="stylesheet" href="{{ url('') }}/admin/css/vendor/bootstrap.css">
	<link rel="stylesheet" href="{{ url('') }}/admin/vendor/metismenu/dist/metisMenu.css">
	<link rel="stylesheet" href="{{ url('') }}/admin/vendor/switchery-npm/index.css">
	<link rel="stylesheet" href="{{ url('') }}/admin/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
	<!-- ======================= LINE AWESOME ICONS ===========================-->
	<link rel="stylesheet" href="{{ url('') }}/admin/css/icons/line-awesome.min.css">
	<!-- ======================= DRIP ICONS ===================================-->
	<link rel="stylesheet" href="{{ url('') }}/admin/css/icons/dripicons.min.css">
	<!-- ======================= MATERIAL DESIGN ICONIC FONTS =================-->
	<link rel="stylesheet" href="{{ url('') }}/admin/css/icons/material-design-iconic-font.min.css">
	<!-- ======================= GLOBAL COMMON STYLES ============================-->
	<link rel="stylesheet" href="{{ url('') }}/admin/css/common/main.bundle.css">
	<!-- ======================= LAYOUT TYPE ===========================-->
	<link rel="stylesheet" href="{{ url('') }}/admin/css/layouts/vertical/core/main.css">
	<!-- ======================= MENU TYPE ===========================================-->
	<link rel="stylesheet" href="{{ url('') }}/admin/css/layouts/vertical/menu-type/default.css">
	<!-- ======================= THEME COLOR STYLES ===========================-->
	<link rel="stylesheet" href="{{ url('') }}/admin/css/layouts/vertical/themes/theme-a.css">
</head>

<body>
	<div class="container">
		<form class="sign-in-form" action="{{ url('admin/loginSubmit') }}" method="POST">
            @csrf
			<div class="card">
				<div class="card-body">
					<a href="#" class="brand text-center d-block m-b-20">
						<img src="{{ url('') }}/admin/img/qt-logo@2x.png" alt="LPS" />
					</a>
                    <h5 class="sign-in-heading text-center m-b-20">Sign in to your account</h5>
                    @if ($errors->any())
                            <div class="text-danger">{{ implode('', $errors->all(':message')) }}</div>
                            @endif
					<div class="form-group">
						<label for="inputEmail" class="sr-only">Email address</label>
						<input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" required="">
					</div>

					<div class="form-group">
						<label for="inputPassword" class="sr-only">Password</label>
						<input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required="">
					</div>
					
					<button class="btn btn-primary btn-rounded btn-floating btn-lg btn-block" type="submit">Sign In</button>
				</div>

			</div>
		</form>
	</div>

	<!-- ================== GLOBAL VENDOR SCRIPTS ==================-->
	<script src="{{ url('') }}/admin/vendor/modernizr/modernizr.custom.js"></script>
	<script src="{{ url('') }}/admin/vendor/jquery/dist/jquery.min.js"></script>
	<script src="{{ url('') }}/admin/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
	<script src="{{ url('') }}/admin/vendor/js-storage/js.storage.js"></script>
	<script src="{{ url('') }}/admin/vendor/js-cookie/src/js.cookie.js"></script>
	<script src="{{ url('') }}/admin/vendor/pace/pace.js"></script>
	<script src="{{ url('') }}/admin/vendor/metismenu/dist/metisMenu.js"></script>
	<script src="{{ url('') }}/admin/vendor/switchery-npm/index.js"></script>
	<script src="{{ url('') }}/admin/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
	<!-- ================== GLOBAL APP SCRIPTS ==================-->
	<script src="{{ url('') }}/admin/js/global/app.js"></script>

</body>

</html>
