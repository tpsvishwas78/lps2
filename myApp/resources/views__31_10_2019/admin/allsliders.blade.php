@extends('admin.layouts.master')
@section('content')


<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
    <div class="mr-auto">
    <h1>Manage All Sliders</h1>
    </div>
    </div>
</header>
<!--END PAGE HEADER -->

<section class="page-content container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                        @if(Session::has('success'))

                        <div class="alert alert-success">
                
                            {{ Session::get('success') }}
                
                            @php
                
                                Session::forget('success');
                
                            @endphp
                
                        </div>
                
                        @endif
                    <div class="card-body">
                        <table id="bs4-table" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Link</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($sliders as $slider)
                                <tr>
                                    <td><img width="100px" src="{{ url('') }}/upload/sliders/{{ $slider->image }}"></td>
                                <td>{{$slider->title}}</td>
                                    <td>{{ $slider->description }}</td>
                                    <td>{{ $slider->link }}</td>
                                <td>
                                    @if ($slider->status==1)
                                    <a onClick="return confirm('Are you sure you want to Deactive?');" style="color:#fff; padding-top:9px;" href="{{ url('admin/sliderstatus') }}/{{ $slider->id }}" class="btn btn-danger btn-sm">Deactive Now</a>
                                    @else
                                    <a onClick="return confirm('Are you sure you want to Active?');" style="color:#fff; padding-top:9px;" href="{{ url('admin/sliderstatus') }}/{{ $slider->id }}" class="btn btn-success btn-sm">Active Now</a>
                                    @endif
                                    
                                </td>
                                    <td>
                                    <a href="{{ url('admin/addslider') }}/{{ $slider->id }}"><i class="zmdi zmdi-edit zmdi-hc-fw"></i></a>
                                         <a href="{{ url('admin/sliderdelete') }}/{{ $slider->id }}" onClick="return confirm('Are you sure you want to Delete?');"><i class="zmdi zmdi-delete zmdi-hc-fw"></i></a></td>
                                </tr>
                                @endforeach
                            </tbody>
                            
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection