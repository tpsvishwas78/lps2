@extends('admin.layouts.master')
@section('content')

<?php
$location=\DB::table('mst_venue')->count();
$games=\DB::table('mst_games')->count();
$sports=\DB::table('mst_sports')->count();
$users=\DB::table('users')->count();
?>

<!--START PAGE HEADER -->
<header class="page-header">
<div class="d-flex align-items-center">
<div class="mr-auto">
<h1>Dashboard</h1>
</div>
</div>
</header>
<!--END PAGE HEADER -->
<!--START PAGE CONTENT -->
<section class="page-content container-fluid">
<div class="row">
<div class="col-12">
<div class="card">
<div class="row m-0 col-border-xl">
<div class="col-md-12 col-lg-6 col-xl-3">
<div class="card-body">
<div class="icon-rounded icon-rounded-primary float-left m-r-20">
<i class="icon dripicons-user-group"></i>
</div>
<h5 class="card-title m-b-5 counter" data-count="{{ $users }}">0</h5>
<h6 class="text-muted m-t-10">
Users
</h6>
</div>
</div>
<div class="col-md-12 col-lg-6 col-xl-3">
<div class="card-body">
<div class="icon-rounded icon-rounded-accent float-left m-r-20">
        <i class="icon dripicons-gaming"></i>
</div>
<h5 class="card-title m-b-5  counter" data-count="{{ $games }}">0</h5>
<h6 class="text-muted m-t-10">
Games
</h6>

</div>
</div>
<div class="col-md-12 col-lg-6 col-xl-3">
<div class="card-body">
<div class="icon-rounded icon-rounded-info float-left m-r-20">
        <i class="icon dripicons-location"></i>
</div>
<h5 class="card-title m-b-5 counter" data-count="{{ $location }}">0</h5>
<h6 class="text-muted m-t-10">
venues
</h6>

</div>
</div>
<div class="col-md-12 col-lg-6 col-xl-3">
<div class="card-body">
<div class="icon-rounded icon-rounded-success float-left m-r-20">
        <i class="icon dripicons-basketball"></i>
</div>
<h5 class="card-title m-b-5  counter" data-count="{{ $sports }}">0</h5>
<h6 class="text-muted m-t-10">
Sports
</h6>

</div>
</div>
</div>
</div>
</div>
</div>
</section>


@endsection
