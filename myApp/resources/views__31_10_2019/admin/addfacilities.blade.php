@extends('admin.layouts.master')
@section('content')


<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
    <div class="mr-auto">
    <h1>Add Sport</h1>
    </div>
    </div>
</header>
<!--END PAGE HEADER -->
<section class="page-content container-fluid">

<div class="row">
    <div class="col-md-12">
<div class="card">
        
        <form  class="form-horizontal" action="{{ url('admin/submitFacilities') }}" method="POST" enctype="multipart/form-data">
            @csrf
    <div class="card-body">
            @if(Session::has('success'))

            <div class="alert alert-success">
    
                {{ Session::get('success') }}
    
                @php
    
                    Session::forget('success');
    
                @endphp
    
            </div>
    
            @endif
            <div class="form-body">
                
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Facility Title</label>
                    <div class="col-md-5">
                        <input type="text" placeholder="Facility Title" id="facility" name="facility" class="form-control" autocomplete="city-name" value="{{ old('facility') ? old('facility') : @$edit->facility }}">
                        @if ($errors->has('facility'))
                        <span class="text-danger">{{ $errors->first('facility') }}</span>
                    @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Icon</label>
                    <div class="col-md-5">
                    <input class="form-control"  type="file" name="icon" >
                    @if (@$edit->icon)
                    <img width="200" src="{{ url('') }}/upload/images/{{ @$edit->icon }}" alt="">
                @endif
                @if ($errors->has('icon'))
                        <span class="text-danger">{{ $errors->first('icon') }}</span>
                    @endif
                    </div>
                   
                </div>
                
 
                </div>
            
        </div>
        <div class="card-footer bg-light">
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="offset-sm-3 col-md-5">
                                <input type="hidden" name="id" value="{{@$edit->id}}">
                                <button type="submit" class="btn btn-primary btn-rounded">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>
</div>
</section>


@endsection