@extends('admin.layouts.master')
@section('content')


<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
    <div class="mr-auto">
    <h1>Add Page</h1>
    </div>
    </div>
</header>
<!--END PAGE HEADER -->
<section class="page-content container-fluid">

<div class="row">
    <div class="col-md-12">
<div class="card">
        
        <form  class="form-horizontal" action="{{ url('admin/submitPage') }}" method="POST">
            @csrf
    <div class="card-body">
            @if(Session::has('success'))

            <div class="alert alert-success">
    
                {{ Session::get('success') }}
    
                @php
    
                    Session::forget('success');
    
                @endphp
    
            </div>
    
            @endif
            <div class="form-body">
                    <div class="form-group row">
                            <label class="control-label text-right col-md-3">Menu</label>
                            <div class="col-md-5">
                               <select name="menu" id="menu" class="form-control">
                                   <option value="">Select</option>
                                   @foreach ($menus as $menu)
                                   <option @if (@$edit)
                                   @if ($edit->menu_id==$menu->id)
                                   selected
                               @endif
                                   @endif  value="{{ $menu->id }}">{{ $menu->name }}</option>
                                   @endforeach
                                   
                               </select>
                                @if ($errors->has('menu'))
                                <span class="text-danger">{{ $errors->first('menu') }}</span>
                            @endif
                            </div>
                        </div>
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Name</label>
                    <div class="col-md-5">
                        <input type="text" placeholder="Name" id="name" name="name" class="form-control" autocomplete="city-name" value="{{ old('name') ? old('name') : @$edit->name }}">
                        @if ($errors->has('name'))
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    @endif
                    </div>
                </div>
                <div class="form-group row">
                        <label class="control-label text-right col-md-3">Description</label>
                        <div class="col-md-9">
                            <textarea id="editor1" class="cke_wrapper" name="description">{{ @$edit->description }}</textarea>
                            @if ($errors->has('description'))
                            <span class="text-danger">{{ $errors->first('description') }}</span>
                        @endif
                        </div>
                </div>

                </div>
            
        </div>
        <div class="card-footer bg-light">
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="offset-sm-3 col-md-5">
                                <input type="hidden" name="id" value="{{@$edit->id}}">
                                <button type="submit" class="btn btn-primary btn-rounded">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>
</div>
</section>
<script src="{{ url('') }}/admin/vendor/ckeditor/ckeditor.js"></script>
<script>
        CKEDITOR.replace( 'editor1' );
    </script>
@endsection