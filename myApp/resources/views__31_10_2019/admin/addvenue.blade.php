@extends('admin.layouts.master')
@section('content')


<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
    <div class="mr-auto">
    <h1>Add Venue</h1>
    </div>
    </div>
</header>
<!--END PAGE HEADER -->
<section class="page-content container-fluid">

<div class="row">
    <div class="col-md-12">
<div class="card">
        
        <form  class="form-horizontal" action="{{ url('admin/submitVenue') }}" method="POST" enctype="multipart/form-data">
            @csrf
    <div class="card-body">
            @if(Session::has('success'))

            <div class="alert alert-success">
    
                {{ Session::get('success') }}
    
                @php
    
                    Session::forget('success');
    
                @endphp
    
            </div>
    
            @endif
            <div class="form-body">
                    <div class="form-group row">
                            <label class="control-label text-right col-md-3">Name</label>
                            <div class="col-md-5">
                                <input type="text" placeholder="Name" id="name1" name="name" class="form-control" autocomplete="city-name" value="{{ old('name') ? old('name') : @$edit->name }}">
                                @if ($errors->has('name'))
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            @endif
                            </div>
                        </div>
                        <div class="form-group row">
                                <label class="control-label text-right col-md-3">Address</label>
                                <div class="col-md-5">
                                    <input type="text" placeholder="address" name="address" id="address1" class="form-control" autocomplete="address" value="{{ old('address') ? old('address') : @$edit->address }}">
                                    @if ($errors->has('address'))
                                    <span class="text-danger">{{ $errors->first('address') }}</span>
                                @endif
                                </div>
                            </div>
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Location</label>
                    <div class="col-md-5">
                        <input type="text" name="location" id="txtPlaces" placeholder="Enter Location" class="form-control" autocomplete="given-location" 
                        value="{{ old('location') ? old('location') : @$edit->location }}"  autofocus>
                        
                        
                        @if ($errors->has('location'))
                        <span class="text-danger">{{ $errors->first('location') }}</span>
                    @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Latitude</label>
                    <div class="col-md-5">
                        <input type="text" class="form-control" placeholder="Latitude" name="latitude" id="latitude" value="{{ old('latitude') ? old('latitude') : @$edit->latitude }}">
                        @if ($errors->has('latitude'))
                        <span class="text-danger">{{ $errors->first('latitude') }}</span>
                    @endif
                    </div>
                </div>
                 <div class="form-group row">
                    <label class="control-label text-right col-md-3">Longitude</label>
                    <div class="col-md-5">
                        <input type="text" class="form-control" placeholder="Longitude" name="longitude" id="longitude" value="{{ old('longitude') ? old('longitude') : @$edit->longitude }}">
                        @if ($errors->has('longitude'))
                        <span class="text-danger">{{ $errors->first('longitude') }}</span>
                    @endif
                    </div>
                </div>
               
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Postcode</label>
                    <div class="col-md-5">
                        <input type="text" placeholder="Postcode" id="postcode" name="postcode" class="form-control" autocomplete="city-postcode" value="{{ old('postcode') ? old('postcode') : @$edit->postcode }}">
                        @if ($errors->has('postcode'))
                        <span class="text-danger">{{ $errors->first('postcode') }}</span>
                    @endif
                    </div>
                </div>
                

                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Description</label>
                    <div class="col-md-5">
                    <textarea class="form-control" name="description">{{@$edit->description}}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Image</label>
                    <div class="col-md-5">
                    <input class="form-control"  type="file" name="image" >
                    @if (@$edit->image)
                    <img width="200" src="{{ url('') }}/upload/images/{{ @$edit->image }}" alt="">
                @endif
                    </div>
                </div>
 
                </div>
            
        </div>
        <div class="card-footer bg-light">
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="offset-sm-3 col-md-5">
                                <input type="hidden" name="id" value="{{@$edit->id}}">
                                <button type="submit" class="btn btn-primary btn-rounded">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>
</div>
</section>
@endsection