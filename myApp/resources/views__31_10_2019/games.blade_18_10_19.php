@extends('layouts.usermaster')

@section('content')
<?php
@$user=Auth::user();
@$mylocation=\DB::table('user_location')->where('user_id',$user->id)->first();
?>
<div class="clearfix"></div>
	<!--BANNER-SECTION-START-->
	<section class="banner-detail">
       <h1 class="hedding-detail">Games</h1>
    </section>
    
	<!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
	<!--account-setting-section-start-->
	<section class="account-setting-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 p-0">
					@include('layouts.sidebar')
					<div class="col-sm-8">
					  <div class="col-sm-12 p-0">
					  	<div class="search-fields dashboard-game">
						<form>
							
							<div class="selectcity-town">
								<input type="text" name="search" value="{{ @$_GET['search'] }}" id="pac-input" placeholder="SEARCH BY TOWN/CITY/POSTCODE">
							</div>
							<button class="search"><img src="{{url('')}}/images/icons/search.png">SEARCH </button>
						</form>
					</div>
					<div class="map-sports">
                            <div id="map"></div>
					</div>
					<div class="date-and-filter">
						<div class="select-date datefilter">
							<div class="custom-date-design">
								<img src="{{url('')}}/images/icons/cal.png">
								<input autocomplete="off" type="text" name="date" id="date" class="datebox" placeholder="Select dates" date="{{ @$_GET['date'] }}" value="{{ @$_GET['date'] }}">
								<img src="{{url('')}}/images/icons/drop-down-icon.png">
							</div>
						</div>
						<div class="filter_vailue">
							<select name="sports" id="sports" class="selectfilter sportfilter">
								<option value="">Filter Sport</option>
								@foreach ($sports as $sport)
									<option @if (@$_GET['sport']==$sport->id)
										selected
									@endif value="{{ $sport->id }}">{{ $sport->name }}</option>
								@endforeach
							</select>
							
						</div>
					</div>
					<div class="map-toggle-button">
						<p>SHOW MAP</p>
						<label class="switch">
						  <input class="switch-button" type="checkbox">
						  <span class="slider-button round"></span>
						</label>
					</div>
				</div>
				<div class="clearfix"></div>
					  <div class="listing-games" id="load-data">
					
						 
						@if (isset($games[0]->id))
                        @foreach ($games as $game)
                       <?php
                            if(@$game->game_start_date){
								$day=date('d',strtotime(@$game->game_start_date));
							   $month=date('M',strtotime(@$game->game_start_date));
							}else{
								$day=date('d',strtotime(@$game->game_date));
							   $month=date('M',strtotime(@$game->game_date));
							}
							$location=\DB::table('mst_venue')->where('id',$game->venue_id)->first();
							$spots=\DB::table('team_spots')->where('game_id',$game->id)->where('status',1)->count();

						$followes=\DB::table('venue_followers')->where('venue_id',$game->venue_id)->count();
						$mefollow=\DB::table('venue_followers')->where('user_id',$user->id)->where('venue_id',$game->venue_id)->count();
                       ?>
						<div class="design-list">
							
							<div class="col-sm-2 col-xs-3 p-0">
								<div class="date-game">
									<h2>{{$day}}<span>{{$month}}</span></h2>
								</div>
							</div>
							<div class="col-sm-7 col-xs-9">
								<div class="info-game">
										<a href="{{ url('') }}/games/{{ $game->slug }}/{{ $game->rid }}">
									<p><img src="{{url('')}}/images/icons/tenis-ball.png">{{ $game->title }}</p>
								</a>
									<ul class="time-and-place">
										<li><img src="{{url('')}}/images/icons/watch-icon.png">{{$game->game_start_time}} - {{$game->game_finish_time}}</li>
                                    <li title="{{ $location->name }}"><img src="{{url('')}}/images/icons/location-icon.png">{!! str_limit($location->name, $limit = 20, $end = '') !!}</li>
									</ul>
									<div class="follow-venue">
										<ul>
											 <li class="price-book">
                                                 @if ($game->payment==3)
												 Free
                                                 @else
												 @if ($game->currency=='USD')
												 ${{ $game->price }} 
											 @else
											 £{{ $game->price }}
											 @endif
                                                 @endif
                                                </li>
											 <li class="number-player">{{ $spots }}/{{ $game->team_limit+$game->team_limit }}</li>

											 <li class="follow-button">
										@csrf
										@if ($mefollow > 0)
										<button id="follow{{ $game->venue_id }}" follow="Following" vid="{{ $game->venue_id }}" class="follow setfollow">Following Venue</button>
										@else
										<button id="follow{{ $game->venue_id }}" follow="Follow" vid="{{ $game->venue_id }}" class="follow setfollow">Follow Venue</button>
										@endif
										
										
										
										@if ($followes>0)
                                        <span><span id="followcount{{ $game->venue_id }}">{{ $followes }}</span> followers</span>
                                        @endif
											 </li>
											 
										</ul>
									</div>
									<div class="dis-game">{!! str_limit($game->description, $limit = 45, $end = '') !!} </div>
								</div>
							</div>
							<div class="col-sm-3 col-xs-12 p-0">
							<div class="game-img">
								<a href="{{ url('') }}/games/{{ $game->slug }}/{{ $game->rid }}">
                                @if ($game->featured_img)
                                    <img src="{{url('')}}/upload/images/{{$game->featured_img}}">
                                @else
                                    <img src="{{url('')}}/images/no-user-Image.png">
                                @endif
								</a>
							</div>
							</div>
						
                        </div>
						@endforeach
						@else
						<div class="alert alert-warning m-sm">
							We couldn't find any games based on your requirements.
							You can try the following options:
							<ul>
							<li><a href="{{ url('') }}/startgame">Start a Game</a></li>
							<li><a href="{{ url('') }}/profile/edit">Modify your location ({{ @$mylocation->location }})</a></li>
							<li>
							<div class="text-black">Change the search terms</div>
							</li>
							</ul>
							</div>
						@endif
						@if ($gamescount>10)
                       <div class="load-more-button" id="remove-row">
                                <button id="btn-more" data-id="2" class="load-more">LOAD MORE</button>
                            </div> 
                        @endif
						
					
					</div>
					</div>
				</div>
			</div>
		</div>
	</section>
    <!--account-setting-section-end-->
	<script>
			$(document).ready(function(){
			   $(document).on('click','#btn-more',function(){
				   var id = $(this).data('id');
				   $("#btn-more").html("Loading....");
				   $.ajax({
					   url : '{{ url("/usergameloadmore") }}',
					   method : "POST",
					   data : {page:id, _token:"{{csrf_token()}}",search:"{{ @$_GET['search'] }}" , date:"{{ @$_GET['date'] }}" ,sport:"{{ @$_GET['sport'] }}" },
					   dataType : "text",
					   success : function (data)
					   {
						   //console.log('data',data);
						  if(data != '') 
						  {
							  $('#remove-row').remove();
							  //$("#btn-more").html("Load More");
							  $('#load-data').append(data);
							  var page=parseFloat(id)+parseFloat(1);
                            $('#btn-more').data('id',page);
						  }
						  else
						  {
							  //$('#btn-more').html("No Data");
							  $('#remove-row').remove();
						  }
					   }
				   });
			   });  
			}); 
			</script> 
<script>
  
	$(".sportfilter").change(function(){
		var sport=$(this).val();
		var date=$('#date').attr('date');
		var search=$('#pac-input').val();
		if(date){
			date=date;
		}else{
			date='';
		}
		window.history.pushState({}, '', 'games?search='+search+'&date='+date+'&sport='+sport);
		location.reload();
	});

			$(".setfollow").click(function(){
				var follow=$(this).attr('follow');
				var vid=$(this).attr('vid');
				//console.log('follow',follow);
			
				var _token = $('input[name="_token"]').val();
							 $.ajax({
							  url:"{{ url('') }}/venue/follow",
							  method:"POST",
							  dataType:'Json',
							  data:{follow:follow,id:vid, _token:_token},
							  success:function(data){
								  //console.log(data);
								  $('#follow'+vid).html(data.follow + ' Venue');
								  $('#follow'+vid).attr('follow',data.follow);
								  $('#followcount'+vid).html(data.followes);
										
							  }
							 });
			  
			});
			</script>
    <script>
            function initAutocomplete() {
      var locations = [
          @foreach ($games as $game)
          <?php  
          $location=\DB::table('mst_venue')->where('id',$game->venue_id)->first();
          ?>
            ['{{$location->name}}', {{$location->latitude}}, {{$location->longitude}}, 1],
            @endforeach
          
      ];
  
      window.map = new google.maps.Map(document.getElementById('map'), {
          mapTypeId: google.maps.MapTypeId.ROADMAP
	  });
	  
	  var input = document.getElementById('pac-input');
			//var searchBox = new google.maps.places.SearchBox(input);
			var options = {
			// types: ['(cities)'],
			componentRestrictions: {country: 'GB'}//UK only
			};
			var autocomplete = new google.maps.places.Autocomplete(input,options);
  
      var infowindow = new google.maps.InfoWindow();
  
      var bounds = new google.maps.LatLngBounds();
  
      for (i = 0; i < locations.length; i++) {
          marker = new google.maps.Marker({
              position: new google.maps.LatLng(locations[i][1], locations[i][2]),
              map: map
          });
  
          bounds.extend(marker.position);
  
          google.maps.event.addListener(marker, 'click', (function (marker, i) {
              return function () {
                  infowindow.setContent(locations[i][0]);
                  infowindow.open(map, marker);
              }
          })(marker, i));
      }
  
      map.fitBounds(bounds);
  
      var listener = google.maps.event.addListener(map, "idle", function () {
          map.setZoom(3);
          google.maps.event.removeListener(listener);
      });
  }
  
        </script>
    @endsection