@extends('layouts.master')
@section('content')
<?php
@$user_id = Auth::user()->id;
?>
<!--BANNER-SECTION-START-->
<section class="banner-detail">
    <h1 class="hedding-detail">{{ $venue->name }}</h1>
</section>
{{-- <section class="venue-description">
         <div class="container">
             <div class="row">
                 
             </div>
         </div>
 
     </section> --}}
<!--BANNER-SECTION-END-->
<div class="clearfix"></div>
<!--account-setting-section-start-->
<section class="account-setting-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 p-0">
                <div class="col-sm-6">
                    <div class="listing-games">
                        <h3>{{ $venue->name }}

                            @if ($mefollow > 0)
                            <!-- <button style="float: right;" id="follow{{ $venue->id }}" follow="Following" vid="{{ $venue->id }}" class="follow setfollow">Following Venue</button> -->
                            @else
                            <!-- <button style="float: right;" id="follow{{ $venue->id }}" follow="Follow" vid="{{ $venue->id }}" class="follow setfollow">Follow Venue</button> -->
                            @endif
                            <!-- <a class="pull-right" href="{{ url('') }}/venue/{{ $venue->slug }}">
											<button class="follow setfollow">
													View Sessions
											</button>
										</a> -->
                        </h3>
                    </div>
                    <div class="map-section">
                        <div class="map">
                            <div class="location">
                                @csrf
                                <p><img src="{{ url('') }}/images/icons/dash-location.png"> {{ $venue->location }}</p>
                                <p><img style="width: 30px; padding-right: 10px;" src="{{ url('') }}/images/icons/icon-venue3.png"><span id="followcount{{ $venue->id }}">{{$followes}}</span> Followers</p>
                            </div>
                            <div id="map"></div>
                        </div>
                        <div class="venu-tabbar">
                            <!-- Bootstrap CSS -->
                            <!-- jQuery first, then Bootstrap JS. -->
                            <!-- Nav tabs -->

                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item active">
                                    <a class="nav-link active" href="#profile" role="tab" data-toggle="tab">Play games<span class="badge">{{ count($upcominggames) }}</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#buzz" role="tab" data-toggle="tab">Past games<span class="badge">{{ count($playedgames) }}</span></a>
                                </li>

                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="profile">
                                    <div class="venue-content-here">
                                        <ul>

                                            @foreach ($upcominggames as $upcominggame)

                                            <?php
                                            @$spots = \DB::table('team_spots')->where('game_id', $upcominggame->id)->where('status', 1)->count();
                                            @$organizer = \DB::table('users')->where('id', $upcominggame->user_id)->first();
                                            @$sport = \DB::table('mst_sports')->where('id', $upcominggame->sport_id)->first();
                                            ?>
                                            <li>
                                                <a href="{{ url('') }}/games/{{ $upcominggame->slug }}/{{ $upcominggame->rid }}">
                                                    <div class="col-sm-3 p-0">
                                                        <div class="time-section">
                                                            <div class="time">
                                                                <h2>{{ $upcominggame->game_start_time }}</h2>
                                                            </div>
                                                            <div class="day">
                                                                <p>{{ date('D d M', strtotime(@$upcominggame->game_start_date)) }}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <h3>{{ $upcominggame->title }}</h3>
                                                        <div class="img-text">
                                                            <div class="img-upcome">
                                                                @if (@$organizer->profile_img && $organizer->profile_img!='null')
                                                                <img src="{{ url('') }}/upload/images/{{$organizer->profile_img}}" width="20" alt="{{$organizer->first_name}}">
                                                                @else
                                                                <img src="{{ url('') }}/images/avtar.png" width="20">
                                                                @endif
                                                            </div>
                                                            <p>Aded by {{ @$organizer->first_name }} {{ @$organizer->last_name }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2 p-0">
                                                        <p class="price-up">
                                                            @if (@$upcominggame->payment==1)
                                                            @if ($upcominggame->currency=='USD')
                                                            ${{ $upcominggame->price }}
                                                            @else
                                                            £{{ $upcominggame->price }}
                                                            @endif

                                                            @elseif(@$upcominggame->payment==2)
                                                            @if ($upcominggame->currency=='USD')
                                                            ${{ $upcominggame->price }}
                                                            @else
                                                            £{{ $upcominggame->price }}
                                                            @endif
                                                            @else
                                                            Free
                                                            @endif
                                                        </p>
                                                        {{-- <p class="team-up">{{ @$spots }}/{{ @$upcominggame->team_limit*2 }}</p> --}}
                                                    </div>
                                                </a>
                                            </li>

                                            @endforeach

                                        </ul>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="buzz">
                                    <div class="venue-content-here">
                                        <ul>
                                            @foreach ($playedgames as $playedgame)

                                            <?php
                                            @$spots = \DB::table('team_spots')->where('game_id', $playedgame->id)->where('status', 1)->count();
                                            @$organizer = \DB::table('users')->where('id', $playedgame->user_id)->first();
                                            @$sport = \DB::table('mst_sports')->where('id', $playedgame->sport_id)->first();
                                            ?>
                                            <li>
                                                <a href="{{ url('') }}/games/{{ $playedgame->slug }}/{{ $playedgame->rid }}">
                                                    <div class="col-sm-3 p-0">
                                                        <div class="time-section">
                                                            <div class="time">
                                                                <h2>{{ $playedgame->game_start_time }}</h2>
                                                            </div>
                                                            <div class="day">
                                                                <p>{{ date('D d M', strtotime(@$playedgame->game_start_date)) }}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <h3>{{ $playedgame->title }}</h3>
                                                        <div class="img-text">
                                                            <div class="img-upcome">
                                                                @if (@$organizer->profile_img && $organizer->profile_img!='null')
                                                                <img src="{{ url('') }}/upload/images/{{$organizer->profile_img}}" width="20" alt="{{$organizer->first_name}}">
                                                                @else
                                                                <img src="{{ url('') }}/images/avtar.png" width="20">
                                                                @endif
                                                            </div>
                                                            <p> Aded by {{ @$organizer->first_name }} {{ @$organizer->last_name }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2 p-0">
                                                        <p class="price-up">
                                                            @if (@$playedgame->payment==1)
                                                            @if ($playedgame->currency=='USD')
                                                            ${{ $playedgame->price }}
                                                            @else
                                                            £{{ $playedgame->price }}
                                                            @endif

                                                            @elseif(@$playedgame->payment==2)
                                                            @if ($playedgame->currency=='USD')
                                                            ${{ $playedgame->price }}
                                                            @else
                                                            £{{ $playedgame->price }}
                                                            @endif
                                                            @else
                                                            Free
                                                            @endif
                                                        </p>
                                                        {{-- <p class="team-up">{{ @$spots }}/{{ @$playedgame->team_limit*2 }}</p> --}}
                                                    </div>
                                                </a>
                                            </li>

                                            @endforeach

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="discription">
                        <h2>Description</h2>
                        <p><img style="width: 100%;" src="{{ url('') }}/upload/images/{{ $venue->image }}" alt=""></p>
                        <p>{{ $venue->description }}</p>
                    </div>
                </div>
            </div>
        </div>
</section>
<input type="hidden" id="userid" name="userid" value="{{ @$user_id }}">
<!--account-setting-section-end-->
<script>
    $(".setfollow").click(function() {
        var follow = $(this).attr('follow');
        var vid = $(this).attr('vid');
        var userid = $('#userid').val();
        if (userid) {
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: "{{ url('') }}/venue/follow",
                method: "POST",
                dataType: 'Json',
                data: {
                    follow: follow,
                    id: vid,
                    _token: _token
                },
                success: function(data) {
                    //console.log(data);
                    $('#follow' + vid).html(data.follow + ' Venue');
                    $('#follow' + vid).attr('follow', data.follow);
                    $('#followcount' + vid).html(data.followes);
                }
            });
        } else {
            alert('Need to login first!');
        }





    });


    function initAutocomplete() {
        var latitude = {{
                $venue->latitude
            }};
        var longitude = {{
                $venue->longitude
            }};
        if (latitude && longitude) {
            var lattt = parseFloat(latitude);
            var long = parseFloat(longitude);
        } else {
            var lattt = -25.363;
            var long = 131.044;
        }
        var html = '<a target="_blank" href="http://maps.google.co.uk/maps?daddr=' + lattt + ',' + long + '">';
        html += '<p>{{ @$venue->name }} </p>';
        html += '<p>{{  @$venue->address }}  </p>';
        html += '<p>{{  @$venue->postcode }} </p> ';
        html += '</a>';
        var locations = [

            [html, lattt, long, 1],
        ];

        var map = new google.maps.Map(document.getElementById('map'), {
            center: {
                lat: lattt,
                lng: long
            },
            zoom: 13,
            mapTypeId: 'roadmap'
        });
        var infowindow = new google.maps.InfoWindow();

        var bounds = new google.maps.LatLngBounds();
        //var marker = new google.maps.Marker({map: map, position: {lat: lattt, lng: long}});
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(lattt, long),
            map: map
        });

        bounds.extend(marker.position);

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infowindow.setContent(locations[0][0]);
                infowindow.open(map, marker);
            }
        })(marker, 0));


    }
</script>
@endsection