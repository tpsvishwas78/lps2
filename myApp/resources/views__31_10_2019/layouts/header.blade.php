<?php
@$user = Auth::user();
@$setting = \DB::table('site_setting')->first();
@$sports = \DB::table('mst_sports')->where('status', 1)->orderBy('sport_order', 'ASC')->get();
@$corporates = \DB::table('corporate_category')->where('status', 1)->get();
?>
<!--HEADER-SECTION-START-->
<section class="header_section">
  <div class="container">
    <div class="top-header">
      <div class="left-header">
        <a class="navbar-brand" href="mailto:{{ @$setting->website_email }}"><img class="mail-icon" src="{{ url('') }}/images/icons/mail.png">{{ @$setting->website_email }}</a>
      </div>
      <div class="logo">
        <a href="{{ url('') }}">
          <img src="{{ url('') }}/upload/images/{{ @$setting->header_logo }}">
        </a>
      </div>
      <div class="right-header">
        @if (Route::has('login'))
        @auth
        <div class="login-signup mr-auto">
          <li><a href="{{ url('') }}/dashboard">{{ @$user->first_name}}</a></li>
          <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"> Logout</a></li>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
        </div>
        @else
        <div class="login-signup mr-auto">
          <li><a href="{{ route('register') }}">Sign up</a></li>
          <li><a href="{{ route('login') }}">Sign in</a></li>
        </div>
        @endauth
        @endif
        <ul class="navbar-nav phone-nav">
          <li class="nav-item andro-phone">
            <a target="_blank" class="nav-link " href="{{ @$setting->play_store_link }}"><img src="{{ url('') }}/images/icons/android.png"></a>
          </li>
          <li class="nav-item i-phone">
            <a target="_blank" class="nav-link" href="{{ @$setting->app_store_link }}"><img src="{{ url('') }}/images/icons/apple.png"></a>
          </li>

        </ul>
        {{-- <a class="navbar-brand" href="mailto:{{ @$setting->website_email }}"><img class="mail-icon" src="{{ url('') }}/images/icons/mail.png"></a> --}}

      </div>
    </div>

    <nav class="navbar navbar-default">

      <!-- Brand/logo -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#example-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>

      <!-- Collapsible Navbar -->
      <div class="collapse navbar-collapse" id="example-1">


        <ul class="navbar-nav menu-header  mt-2 mt-lg-0">
          <li class="nav-item active">
            <a class="nav-link" href="{{ url('') }}">Home</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="{{ url('') }}/searchgame" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Choose your sport
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              @foreach (@$sports as $sport)
              <a class="dropdown-item" href="{{ url('') }}/searchgame?search=&date=&gender=&sport={{ $sport->id  }}">
                {{ $sport->name }}
              </a>
              @endforeach
            </div>
          </li>


          {{-- <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="{{ url('') }}/woomengame" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Womens Only </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            @foreach ($sports as $sport)
            <a class="dropdown-item" href="{{ url('') }}/woomengame?search=&date=&sport={{ $sport->id  }}">{{ $sport->name }}</a>
            @endforeach
          </div>
          </li> --}}
          <li class="nav-item">
            <a class="nav-link" href="{{ url('') }}/womengames
                      ">Womens Only </a>

          </li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Children's Zone</a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="{{ url('') }}/kids-parties">Kids Parties</a>
              <a class="dropdown-item" href="{{ url('') }}/children-venues">Party Venues</a>
              <a class="dropdown-item" href="{{ url('') }}/children-coaching">Children’s Coaching</a>
              <a class="dropdown-item" href="{{ url('') }}/kids-5-a-side-leagues">Kids 5a Side Leagues</a>
              {{-- <a class="dropdown-item" href="{{ url('') }}/register-your-intrest">Soft Play </a>
              <a class="dropdown-item" href="{{ url('') }}/register-your-intrest">Inflatable Hire </a> --}}
              {{-- <a class="dropdown-item" href="#">Kids football All Clubs</a>
                        <a class="dropdown-item" href="#">Soft Play Hire </a>
                        <a class="dropdown-item" href="#">All sports </a> --}}
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="{{ url('') }}/corporate" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Corporate</a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              {{-- <a class="dropdown-item" href="{{ url('') }}/team-building">Team Building Day</a>
              <a class="dropdown-item" href="{{ url('') }}/blockbooking">Block Booking</a>
              <a class="dropdown-item" href="{{ url('') }}/sports-tournament">Sports Tournaments</a>
              <a class="dropdown-item" href="{{ url('') }}/get-involved">Get Involved</a> --}}
              {{-- <a class="dropdown-item" href="{{ url('') }}/corporate-sports-days">Corporate Sports Days</a>
              <a class="dropdown-item" href="{{ url('') }}/inflatable-fun-days">Inflatable Fun Days</a>
              <a class="dropdown-item" href="{{ url('') }}/its-a-knockout-fun-days">Its A Knockout Fun Days</a>
              <a class="dropdown-item" href="{{ url('') }}/office-summer-party">Office Summer Party</a>
              <a class="dropdown-item" href="{{ url('') }}/office-christmas-party">Office Christmas Party</a>
              <a class="dropdown-item" href="{{ url('') }}/sports-tournaments">Sports Tournaments</a>
              <a class="dropdown-item" href="{{ url('') }}/blockbooking">Block Booking</a> --}}
              @foreach ($corporates as $corporate)
              <a class="dropdown-item" href="{{ url('') }}/corporate/{{ @$corporate->slug }}">{{ @$corporate->name }}</a>
              @endforeach
              {{-- <a class="dropdown-item" href="{{ url('') }}/sports-tournaments">Sports Tournaments</a> --}}
              <a class="dropdown-item" href="{{ url('') }}/blockbooking">Block Booking</a>
            </div>
          </li>
          <li class="nav-item">
            @if (Route::has('login'))
            @auth
            <a class="nav-link" href="{{ url('') }}/advertise-your-session"> Advertise Your Session </a>

            @else
            <a class="nav-link" href="{{ route('login') }}"> Advertise Your Session </a>
            @endauth
            @endif
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('') }}/contactus"> Contact us </a>
          </li>

        </ul>
        <a class="navbar-brand pull-right phone-number-contact" href="tel:{{ @$setting->phone_number }}"><img src="{{ url('') }}/images/icons/phone.png">{{ @$setting->phone_number }}</a>

      </div>
  </div>

  </nav>
</section>
<!--HEADER-SECTION-END-->