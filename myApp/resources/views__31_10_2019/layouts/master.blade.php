<?php
@$setting=\DB::table('site_setting')->first();
if(@$setting->website_title){
  $title=@$setting->website_title;
}else{
  $title='Lets play sports - play football, netball and sports near you';
}
$controllername=request()->route()->getAction();
$pagename=$controllername['as'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>{{ $title }}</title>
	<!-- META TAGS -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- FAV ICON(BROWSER TAB ICON) -->
	
	<!-- GOOGLE FONT -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i|Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
 
	<!-- FONTAWESOME ICONS -->
	<link rel="stylesheet" href="{{ url('') }}/css/font-awesome.min.css">
    <!-- ALL CSS FILES -->
    <link href="{{ url('') }}/css/semantic.min.css" rel="stylesheet">
    <link href="{{ url('') }}/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{ url('') }}/css/owl.carousel.min.css" rel="stylesheet">
	<!-- RESPONSIVE.CSS ONLY FOR MOBILE AND TABLET VIEWS -->
  <link href="{{ url('') }}/css/style.css" rel="stylesheet">
  
@if ($pagename=='children' || $pagename=='children_venues' || $pagename=='children_coaching' || $pagename=='kids_language' || $pagename=='children_detail'|| $pagename=='kids_deayils' || $pagename=='children_register_your_intrest')
<link href="https://fonts.googleapis.com/css?family=Caveat+Brush&display=swap" rel="stylesheet">
<link href="{{ url('') }}/css/children.css" rel="stylesheet">
@endif


	<link href="{{ url('') }}/css/responsive.css" rel="stylesheet">
	<script src="{{ url('') }}/js/jquery.min.js"></script>
  <script src="{{ url('') }}/js/owl.carousel.min.js"></script>
	
</head>

<body>
        @if (request()->segment(1) != 'login' && request()->segment(1) != 'register' && request()->segment(1) != 'password')
        @include('layouts.header')
        @endif
        @yield('content')
        @if (request()->segment(1) != 'login' && request()->segment(1) != 'register' && request()->segment(1) != 'password')
        @include('layouts.footer')
        @endif
        

    <!--SCRIPT FILES-->
	{{-- <script src="{{ url('') }}/js/jquery.min.js"></script> --}}
	<script src="{{ url('') }}/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="{{ url('') }}/js/custom.js"></script>
	<script src="{{ url('') }}/js/semantic.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCinqHaojRbM2o13DIpNjZV5OIovI5kyrs&libraries=places&callback=initAutocomplete"
	async defer></script>
	
  <script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
    
<script>
           

        var dateToday = new Date();
        $('.datepicker').calendar({
          type: 'date',
          minDate: dateToday,
        });
        $('.timepicker').calendar({
          type: 'time',
        });
	$('.datefilter').calendar({
          type: 'date',
          minDate: dateToday,
          onChange: function (date, text) {
            var year = date.getFullYear();
            var month = date.getMonth() + 1;
            var day = date.getDate();
            if (month < 10) {
                month = '0' + month;
            }
            if (day < 10) {
                day = '0' + day;
            }

            // everything combined
           var mydate = year + '-' + month + '-' + day;
           $('#date').attr('date',mydate);
           var sport=$('#sports').val();
           var gender=$('#gender').val();
           var search=$('#pac-input').val();
           var near_latitude=$('#near_latitude').val();
                var near_longitude=$('#near_longitude').val();
           window.history.pushState({}, '', 'searchgame?search='+search+'&date='+mydate+'&sport='+sport+'&gender='+gender+'&near_latitude='+near_latitude+'&near_longitude='+near_longitude);
           location.reload();
           
  },
        });

        $('.datefilterwomen').calendar({
          type: 'date',
          minDate: dateToday,
          onChange: function (date, text) {
            var year = date.getFullYear();
            var month = date.getMonth() + 1;
            var day = date.getDate();
            if (month < 10) {
                month = '0' + month;
            }
            if (day < 10) {
                day = '0' + day;
            }

            // everything combined
           var mydate = year + '-' + month + '-' + day;
           $('#date').attr('date',mydate);
           var sport=$('#sports').val();
           var gender=$('#gender').val();
           var search=$('#pac-input').val();
           var near_latitude=$('#near_latitude').val();
                var near_longitude=$('#near_longitude').val();
           window.history.pushState({}, '', 'womengames?search='+search+'&date='+mydate+'&sport='+sport+'&gender='+gender+'&near_latitude='+near_latitude+'&near_longitude='+near_longitude);
           location.reload();
           
  },
        });

        
        var canvas = document.querySelector("canvas");

// Adjust canvas coordinate space taking into account pixel ratio,
// to make it look crisp on mobile devices.
// This also causes canvas to be cleared.
function resizeCanvas() {
    // When zoomed out to less than 100%, for some very strange reason,
    // some browsers report devicePixelRatio as less than 1
    // and only part of the canvas is cleared then.
    var ratio =  Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
}

window.onresize = resizeCanvas;
resizeCanvas();

var signaturePad = new SignaturePad(canvas, {
  backgroundColor: 'rgb(255, 255, 255)' // necessary for saving image as JPEG; can be removed is only saving as PNG or SVG
});



$("#signature-pad").click(function(){
  var dataURL = canvas.toDataURL();
$('#mysignature').val(dataURL+'mysignature');
  console.log('signaturePad',dataURL);
});
$("#signature-clear").click(function (event) {
var data = signaturePad.toData();

if (data) {
data.pop(); // remove the last dot or line
signaturePad.fromData(data);
$('#mysignature').val('');
}
});

//signaturePad.toDataURL("data:image/png;base64,signature");


	</script>
</body>
</html>