<?php
@$setting=\DB::table('site_setting')->first();
@$menus=\DB::table('menus')->where('status',1)->get();
?>
<!--FOOTER-START-->
<footer>
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="col-sm-6 col-xs-12">
						<div class="logo-foot">
							<a href="{{ url('') }}">
							<img src="{{ url('') }}/upload/images/{{ @$setting->footer_logo }}">
						</a>
						</div>
						<ul class="social">
							<li><a target="_blank" href="{{ @$setting->facebook_url }}"><img src="{{ url('') }}/images/icons/fb-footer.png"> <span>FACEBOOK</span></a></li>
							<li><a target="_blank"  href="{{ @$setting->twitter_url }}"><img src="{{ url('') }}/images/icons/tw-footer.png"> <span>Twitter</span></a></li>
							<li><a target="_blank"  href="{{ @$setting->instagram_url }}"><img src="{{ url('') }}/images/icons/insta-footer.png"> <span>Instagram</span></a></li>
						</ul>
					</div>
					<div class="col-sm-3 col-xs-6">
						<div class="footer-menu">
							<h3 class="menu-heading">Navigation</h3>
							<ul>
								<li><a href="{{ url('') }}">Home</a></li>
								<li><a href="{{ url('') }}/searchgame">Choose your sport</a></li>
								<li><a href="{{ url('') }}/womengames">Womens Only</a></li>
								<li><a href="{{ url('') }}/kids-parties">Kids Parties</a></li>
								<li><a href="{{ url('') }}/corporate">Corporate</a></li>
								<li><a href="{{ url('') }}/advertise-your-session">Advertise your session</a></li>
								<li><a href="{{ url('') }}/contactus">Contact Us</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3 col-xs-6">
						<div class="footer-menu">
							<h3 class="menu-heading">LEGAL</h3>
							<ul>
								{{-- <li><a href="#">PRIVACY POLICY</a></li>
								<li><a href="#">TERMS & CONDITIONS</a></li> --}}
								@foreach ($menus as $menu)
								@if ($menu->slug!='about-us')
								<li><a href="{{ url('') }}/pages/{{ $menu->slug }}">{{ $menu->name }}</a></li>
								@endif
								
								@endforeach
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="bottom-footer">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<p>{!! @$setting->copyright !!}</p>
						<div class="buttons-playstore">
							<a target="_blank" href="{{ @$setting->app_store_link }}">
							<button class="ios"><img src="{{ url('') }}/images/icons/apple.png">Apple Store</button>
						</a>
						<a target="_blank" href="{{ @$setting->play_store_link }}">
							<button class="android"><img src="{{ url('') }}/images/icons/android.png">Play Store</button>
						</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!--FOOTER-END-->