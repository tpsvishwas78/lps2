<?php
$user=Auth::user();

$location=\DB::table('user_location')->where('user_id',$user->id)->first();

?>
<div class="col-sm-4">
        <div class="profile-info">
            <div class="name-img-profile">
                <div class="img-pro">
                        @if ($user->profile_img && $user->profile_img!='null')
                        <img src="{{ url('') }}/upload/images/{{$user->profile_img}}" class="w-50 rounded-circle" alt="{{$user->first_name}}">
                        @else
                        <img src="{{ url('') }}/images/avtar.png" class="w-50 rounded-circle" alt="{{$user->first_name}}">
                        @endif
                </div>
                <p class="name-user">{{$user->first_name}} {{$user->last_name}}</p>
            </div>
            
            <div class="basic-info">
                <p>Basic</p>
                <ul>
                    <li><a href="{{ url('') }}/profile"><img src="{{ url('') }}/images/icons/profile-icon.png"><p class="text-basic">Profile</p></a></li>
                    <li><a href="{{ url('') }}/startgame"><img src="{{ url('') }}/images/icons/add-icon.png"><p class="text-basic">Start a Game</p></a></li>
                    <li><a href="{{ url('') }}/mygames"><img src="{{ url('') }}/images/icons/add-icon.png"><p class="text-basic">My Games</p></a></li>

                    <li><a href="{{ url('') }}/join_games"><img src="{{ url('') }}/images/icons/add-icon.png"><p class="text-basic">My Joined Games</p></a></li>                    <!-- <li><a href="{{ url('') }}/myjoingames"><img src="{{ url('') }}/images/icons/add-icon.png"><p class="text-basic">My Join Games</p></a></li> -->
                <li>
                    <img src="{{ url('') }}/images/icons/dash-location.png"><p class="text-basic">Based in 
                    
                   {{@$location->location}}</p></li>
                    <li><a href="#"><img src="{{ url('') }}/images/icons/when-where-online.png"><p class="text-basic">Current balance £0.0</p></a></li>
                </ul>
            </div>
        </div>
       
        <div class="profile-info">
            
            <div class="basic-info">
                <p>My Upcoming Games</p>
                <span>You don't have a next game. You can look for game from <a class="link" href="{{ url('') }}/games">Here<img src="{{ url('') }}/images/icons/right-arrow.png"></a></span>
            </div>
        </div>
    </div>