@extends('layouts.master')
@section('content')
<?php
$setting=\DB::table('site_setting')->first();
?>
<div class="clearfix"></div>
	<!--BANNER-SECTION-START-->
	<section class="banner-detail">
       <h1 class="hedding-detail">Contact us</h1>
    </section>
	<!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
	<!--CONTACT-US-SECTION-START-->
	<section class="contact-map">
			<div id="map"></div>
		<div class="locate-card">
				<h2>{{ @$setting->city }}</h2>
				<p>{{ @$setting->address }}</p>
				<p>Tel:{{ @$setting->phone_number }}</p>
				<button class="contact-map getcontactform">Contact</button>
			</div>
	</section>
	<section class="form-contact">
		<div class="container">
			<div class="row">
					@if(Session::has('success'))
					<div class="alert alert-success">
						{{ Session::get('success') }}
						@php
							Session::forget('success');
						@endphp
						</div>
						@endif
				<div class="col-sm-10 col-sm-offset-1">
					<div class="heading-contact-form">
						<h1>Get in Touch With Us</h1>
						<p>Please fill out the below form - we aim to respond within 24 hours </p>
					</div>
					<div class="contact-us-form">
                        <form class="comment" method="POST" action="{{ url('') }}/submitContact">
                            @csrf
						<div class="col-sm-6 p-0">
							<div class="form-group">
								<label class="control-label">First Name</label>
                                <input type="text" name="first_name" class="form-control" placeholder="Enter Your First  Name">
                                @if ($errors->has('first_name'))
                        <span class="text-danger">{{ $errors->first('first_name') }}</span>
                    @endif
								</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label">Last Name</label>
                                <input type="text" name="last_name" class="form-control" placeholder="Enter Your Last Name">
                                @if ($errors->has('last_name'))
                        <span class="text-danger">{{ $errors->first('last_name') }}</span>
                    @endif
								</div>
						</div>
						<div class="col-sm-6 p-0">
							<div class="form-group">
								<label class="control-label">Your Email</label>
                                <input type="text" name="email" class="form-control" placeholder="Enter Your Email">
                                @if ($errors->has('email'))
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                    @endif
								</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label">Mobile Number</label>
                                <input type="text" name="mobile_number" class="form-control" placeholder="Enter Your Mobile Number">
                                @if ($errors->has('mobile_number'))
                        <span class="text-danger">{{ $errors->first('mobile_number') }}</span>
                    @endif
								</div>
						</div>
						<div class="col-sm-12 pl-0">
							<div class="form-group">
								<label class="control-label">	Your Message</label>
                                <textarea name="message" class="form-control textarea" placeholder="Enter Your Message"></textarea>
                                @if ($errors->has('message'))
                        <span class="text-danger">{{ $errors->first('message') }}</span>
                    @endif
								</div>
						</div>
						<div class="captcha contact-captcha">
    					{!! NoCaptcha::renderJs() !!}
                        {!! NoCaptcha::display() !!}
                        @if ($errors->has('g-recaptcha-response'))
                        <span class="text-danger">{{ $errors->first('g-recaptcha-response') }}</span>
                    @endif
					</div>
					<div style="text-align: center;">
						<button class="submit">SUBMIT</button>
					</div>
					</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--CONTACT-US-SECTION-END-->

	<script>
		$(".getcontactform").click(function(){   
     $('html, body').animate({
         scrollTop: $(".form-contact").offset().top
     }, 1500);
});
		function initAutocomplete() {
            var latitude={{ @$setting->latitude }};
            var longitude={{ @$setting->longitude }};
            if(latitude && longitude){
                var lattt=parseFloat(latitude);
                var long=parseFloat(longitude);
            }else{
                var lattt= -25.363;
                var long= 131.044;
            }

            
           
          var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: lattt, lng: long},
            zoom: 13,
            mapTypeId: 'roadmap'
          });
		  var marker = new google.maps.Marker({map: map, position: {lat: lattt, lng: long}});
  
          
        }
	</script>
@endsection