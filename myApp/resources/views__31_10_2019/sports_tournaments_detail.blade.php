@extends('layouts.master')
@section('content')
<?php
@$images=\DB::table('sportsturnaments_images')->where('cp_id',@$detail->id)->get();
@$videos=\DB::table('sportsturnaments_videos')->where('cp_id',@$detail->id)->get();
?>
<!--BANNER-SECTION-START-->
<section class="banner-detail" style="background: url({{ url('') }}/upload/images/{{ @$detail->banner }});">
		<div  class="hedding-children">
       <h1>{{ @$detail->title }}</h1>
       </div>
    </section>
	<!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
	<!--crousel--and-video-section-start-->
	<section class="corporate-sports-day">
		<div class="container">
			<div  class="row">
				<div class="col-sm-12">
					<div class="col-sm-7">
						<div  class="left-vide0-section">
							<div class="video-detail">
									{!! @$detail->main_video !!}
								{{-- <div class="video-play-icon-cp">
									<img src="{{ url('') }}/images/icons/play-video.png">
								</div> --}}
							</div>
						</div>
					</div>
					<div class="col-sm-5">
						<div class="right-side-crousel">
							<!--crousel-->
							<div class="header" id="cover">
									<div class="carousel slide" data-ride="carousel" id="carousel">
											<div class='carousel-inner'>
													<div class='item active'>
													
													<img class='responsive' alt='umpires' src='{{ url('') }}/upload/images/{{ @$detail->feature_image }}'/>
												</div>
													@if (@$images)
														
													
													@foreach ($images as $image)
														
													
													<div class='item'>
													
													<img class='responsive' alt='fallball4' src='{{ url('') }}/upload/images/{{ $image->image }}'/>
													</div>
								
													@endforeach
													@endif
													
													</div>
									<div class="clearfix">
											<div class='carousel-link'>
													<div class='thumb' data-slide-to='0' data-target='#carousel'>
													<img class='responsive' alt='umpires' src='{{ url('') }}/upload/images/{{ @$detail->feature_image }}'/>
												</div>
												@foreach ($images as $key=> $thumb)
													<div class='thumb' data-slide-to='{{ $key+1 }}' data-target='#carousel'>
													<img class='responsive' alt='baseballd' src='{{ url('') }}/upload/images/{{ $thumb->image }}'/>
													</div>
													@endforeach
													</div>
													
									</div>
									</div>
									</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="info-cp col-md-12">{!! @$detail->description !!}</div>

				</div>
			</div>
		</div>
	</section>
	<!--crousel--and-video-section-end-->
	<div class="clearfix"></div>
	<!--referess-section-strt-->
	<section class="refress-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
                     <div class="col-sm-2">
                     	<div class="img-and-info">
                     		<img src="{{ url('') }}/images/icons/icon-venu1.png">
                     		<p>Venue</p>
                     	</div>
                     </div>
                     <div class="col-sm-2">
                     	<div class="img-and-info">
                     		<img src="{{ url('') }}/images/icons/icon-venue2.png">
                     		<p>Referees</p>
                     	</div>
                     </div><div class="col-sm-2">
                     	<div class="img-and-info">
                     		<img src="{{ url('') }}/images/icons/icon-venue3.png">
                     		<p>Management</p>
                     	</div>
                     </div><div class="col-sm-2">
                     	<div class="img-and-info">
                     		<img src="{{ url('') }}/images/icons/icon-venue4.png">
                     		<p>Equipment</p>
                     	</div>
                     </div><div class="col-sm-2">
                     	<div class="img-and-info">
                     		<img src="{{ url('') }}/images/icons/icon-venue5.png">
                     		<p>Trophies</p>
                     	</div>
                     </div><div class="col-sm-2">
                     	<div class="img-and-info">
                     		<img src="{{ url('') }}/images/icons/icon-venue6.png">
                     		<p>Extras</p>
                     	</div>
                     </div>
				</div>
			</div>
		</div>
	</section>
	<!--referess-section-end-->
	<div class="clearfix"></div>
	<!--sports-day-challanges-start-->
	<section class="sports-challanges">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="head-challange">
						<h1>{{ @$detail->sec_title }}</h1>
						<p>{!! @$detail->sec_description !!}</p>
					</div>
					@if (@$videos)
					<div class="tab-design-function">
						 <div  class="col-sm-12 p-0">
       
				        <div class="col-sm-5"> <!-- required for floating -->
				          <!-- Nav tabs -->
				          <ul class="nav nav-tabs tabs-left sideways">
					
							@foreach ($videos as $key => $video)
							<li @if ($key==0)
							class="active"
							@endif ><a href="#home-{{ $key }}" data-toggle="tab">
									<div class="drop-down-icon-filter-corporate"></div>  {{ $video->title }}</a>
								</li>
							@endforeach
				            
				          </ul>
				        </div>

				        <div class="col-sm-7">
				          <!-- Tab panes -->
				          <div class="tab-content">
								@foreach ($videos as $key => $video)
								
				            <div class="tab-pane @if ($key==0)
							active @endif" id="home-{{ $key }}">
				            	<div class="video-detail">
								{!! $video->video !!}
								{{-- <div class="video-play-icon-cp">
									<img src="{{ url('') }}/images/icons/play-video.png">
								</div> --}}
							</div>
				            </div>
							@endforeach

				          </div>
				        </div>

				        <div class="clearfix"></div>

				      </div>

				      
					</div>

					@endif
				</div>
			</div>
		</div>
	</section>
	<!--sports-day-challanges-start-->
	<div class="clearfix"></div>
	<!--last-section-start-->
	<section class="serach-result">
		<div class="container">
			<div class="row">
				<!--game-listing-start-->
				<div class="col-sm-12">
@foreach ($blogs as $blog)
<div class="listing-games">
		<div class="design-list">
			<div class="col-sm-9">
				<div class="info-game">
					<p>{{ $blog->title }}</p>
					
					<div class="dis-game">{{ $blog->description }}</div>
				</div>
			</div>
			<div class="col-sm-3 p-0">
			<div class="game-img">
				<img src="{{ url('') }}/upload/images/{{ $blog->image }}">
			</div>
			</div>
		</div>
	</div>
@endforeach
					
					
					
				</div>
				<!--game-listing-end-->
			</div>
		</div>
	</section>
	<!--last-section-end-->

@endsection