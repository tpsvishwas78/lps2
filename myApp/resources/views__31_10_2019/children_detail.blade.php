@extends('layouts.master')
@section('content')

<?php
@$facilities=\DB::table('cp_venue_facility')->where('cp_id',$party->id)->get();
@$images=\DB::table('cp_images')->where('cp_id',$party->id)->get();
?>
<!--BANNER-SECTION-START-->
<section class="banner-detail">
		<div  class="hedding-children">
       <h1>{{ @$party->title }}</h1>
       {{-- <h3>AT lets play sports BIRMINGHAM INDOOR</h3> --}}
       </div>
    </section>
	<!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
	<!--available-at-section-start-->
	{{-- <section class="available-at">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h2 class="available-hedding">Available At <br>BIRMINGHAM INDOOR</h2>
				</div>
			</div>
		</div> --}}
	</section>
	<!--available-at-section-end-->
	<div class="clearfix"></div>
	<!--SLIDER-SECTION-START-->
	{{-- <section class="slider">
			<div>
			

			<div class="full-width-crousel">
				<div id="owl-demo" class="owl-carousel owl-theme">
						@if(@$sliders)
						@foreach ($sliders as $key=> $slider)
				  <a href="{{ $slider->link }}" class="item">
				  	<div class="col-md-12 col-sm-12 col-xs-12 p-0">
							<img src="{{ url('') }}/upload/sliders/{{ $slider->image }}" class="img-responsive">
							<div class="hover-deta-img-crousel">
								 <h2>{{ $slider->title }}</h2>
								 <p>{{ $slider->description }}</p>
							 </div>
						</div>
					</a>
					@endforeach
					@endif
			
				 
				</div>
			</div>
	</section> --}}
	<!--SLIDER-SECTION-END-->
	<div class="clearfix"></div>
	<!--slide-select-section-start-->
	<section class="search-detail">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="col-sm-8">
						<div class="left-detail-section">
							<!--crousel-start-->
							<div class="header" id="cover">
						<div class="carousel slide" data-ride="carousel" id="carousel">
								<div class='carousel-inner'>
										<div class='item active'>
										
										<img class='responsive' alt='umpires' src='{{ url('') }}/upload/images/{{ @$party->feature_image }}'/>
									</div>
										@if (@$images)
											
										
										@foreach ($images as $image)
											
										
										<div class='item'>
										
										<img class='responsive' alt='fallball4' src='{{ url('') }}/upload/images/{{ $image->image }}'/>
										</div>
					
										@endforeach
										@endif
										
										</div>
						<div class="clearfix">
								<div class='carousel-link'>
										<div class='thumb' data-slide-to='0' data-target='#carousel'>
										<img class='responsive' alt='umpires' src='{{ url('') }}/upload/images/{{ @$party->feature_image }}'/>
									</div>
									@foreach ($images as $key=> $thumb)
										<div class='thumb' data-slide-to='{{ $key+1 }}' data-target='#carousel'>
										<img class='responsive' alt='baseballd' src='{{ url('') }}/upload/images/{{ $thumb->image }}'/>
										</div>
										@endforeach
										</div>
										
						</div>
						</div>
						</div>
						<!--crousel-end-->
					</div>

				</div>
				<div class="col-sm-4">
						<div class="when-and-where">
							<h2>{{ @$party->venue_title }}</h2>
							<div class="addres-vanue">
								<div class="col-sm-1 col-xs-2 p-0">
										<img src="{{ url('') }}/images/icons/when-where-location.png">
									</div>
									<div class="col-sm-11 col-xs-10">
										<h4>Address</h4>
										<p>{{ @$party->venue_address }}</p>
									</div>
							</div>
							<div class="clearfix"></div>
							<div class="vanue-facilities">
								<h3>Venue Facilities</h3>
								<ul>
									@foreach (@$facilities as $facility)
									<?php
@$icon=\DB::table('mst_venue_facility')->where('id',$facility->vf_id)->first();
									?>
									
									<li>
										<img src="{{ url('') }}/upload/images/{{ $icon->icon }}">
										<span>{{ $icon->facility }}</span>
									</li>
									@endforeach
									
								</ul>
							</div>
							<a href="{{ url('') }}/children/register-your-interest">
							<button class="pay-now">ENQUIRE NOW</button>
							</a>
						</div>
					</div>
			</div>
	</div></div>
</section>
	<!--slide-select-section-start-->
	<!--video-vanue-section-start-->
	<section class="video-vanue" style="min-height: 400px;">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					{{-- <div class="watch-video">
							<video width="100%" height="auto" controls>
									<source src="{{ url('') }}/upload/video/{{ @$party->video }}" type="video/mp4">
									<source src="{{ url('') }}/upload/video/{{ @$party->video }}" type="video/ogg">
								  Your browser does not support the video tag.
								  </video>
						<div class="play-video-icon" play='play'>
							<img src="{{ url('') }}/images/icons/play-video.png">
						</div>
					</div> --}}
					
				</div>
			</div>
		</div>
	</section>
	<!--video-vanue-section-end-->
	<!--referess-section-start-->
	<section class="refress-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
                     <div class="col-sm-2 col-xs-6">
                     	<div class="img-and-info">
                     		<img src="{{ url('') }}/images/icons/icon-venu1.png">
                     		<p>Venue</p>
                     	</div>
                     </div>
                     <div class="col-sm-2 col-xs-6">
                     	<div class="img-and-info">
                     		<img src="{{ url('') }}/images/icons/icon-venue2.png">
                     		<p>Referees</p>
                     	</div>
                     </div><div class="col-sm-2 col-xs-6">
                     	<div class="img-and-info">
                     		<img src="{{ url('') }}/images/icons/icon-venue3.png">
                     		<p>Management</p>
                     	</div>
                     </div><div class="col-sm-2 col-xs-6">
                     	<div class="img-and-info">
                     		<img src="{{ url('') }}/images/icons/icon-venue4.png">
                     		<p>Equipment</p>
                     	</div>
                     </div><div class="col-sm-2 col-xs-6">
                     	<div class="img-and-info">
                     		<img src="{{ url('') }}/images/icons/icon-venue5.png">
                     		<p>Trophies</p>
                     	</div>
                     </div><div class="col-sm-2 col-xs-6">
                     	<div class="img-and-info">
                     		<img src="{{ url('') }}/images/icons/icon-venue6.png">
                     		<p>Extras</p>
                     	</div>
                     </div>
				</div>
			</div>
		</div>
	</section>
	<!--referess-section-end-->
	<div class="container">
			<div class="row">
				<div class="col-sm-12">
	<div class="childpara"><div class="col-sm-12"> {!! @$party->timing_description !!}</div></div>
</div>
</div>
</div>
	
	<!--opening-hougher-section-start-->
	<section class="opening-time">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="col-sm-8">
						<div class="football-party-section">
							<h2>{{ @$party->box_title }}</h2>
							{!! @$party->description !!}
						</div>
					</div>
					<div class="col-sm-4">
						<!--<div class="opening-hours">
							<h2>OPENING HOURS</h2>
							{!! @$party->opening_hours !!}
						</div>-->
						<div class="opening-hours" style="text-align:center; padding-top:50px;">
							<a href="{{ url('') }}/children/register-your-interest"><img src="{{ url('') }}/images/interest-img.png" alt="Register your interest"></a>
							
							</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--opening-hougher-section-end-->

<script>

$(".watch-video").click(function(){
	var play=$('.play-video-icon').attr('play');
	if(play=='play'){
		$('.play-video-icon').attr('play','stop');
		$('.play-video-icon').show();
	}else{
		$('.play-video-icon').attr('play','play');
		$('.play-video-icon').hide();
	}
});
</script>
@endsection