
@extends('layouts.usermaster')

@section('content')
<?php
$user=Auth::user();
?>
<div class="clearfix"></div>
<!--BANNER-SECTION-START-->
<section class="banner-detail">
<h1 class="hedding-detail">Account Settings</h1>
</section>
<!--BANNER-SECTION-END-->
<div class="clearfix"></div>
<!--account-setting-section-start-->
<section class="account-setting-content">
<div class="container">
<div class="row">
<div class="col-sm-12 p-0">
@include('layouts.sidebar')
<div class="col-sm-8">
<div class="right-account-setting">
<h2 class="headding-account">Account Settings</h2>
@if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
                @php
                    Session::forget('success');
				@endphp
				</div>
				@endif
<div id="exTab1">	
<ul  class="nav nav-pills">
	<li class="active">
<a  href="#1a" data-toggle="tab">Profile</a>
	</li>
	<li><a href="#2a" data-toggle="tab">Password</a>
	</li>
	<li><a href="#3a" data-toggle="tab">Notification</a>
	</li>
<li><a href="#4a" data-toggle="tab">Location</a>
	</li>
</ul>

	<div class="tab-content clearfix">
		<div class="tab-pane active" id="1a">
			<form class="content-profile" action="{{ url('profile/updateprofile') }}" method="POST" enctype="multipart/form-data">
				@csrf
			<!--<div class="edit-profile-img">
				<div class="profile-img-edit">
						@if ($user->profile_img && $user->profile_img!='null')
						<img src="{{ url('') }}/upload/images/{{ $user->profile_img }}" class="w-50 rounded-circle" alt="{{$user->first_name}}">
						@else
						<img src="{{ url('') }}/images/avtar.png" class="w-50 rounded-circle" alt="{{$user->first_name}}">
						@endif
					</div>-->
<!--profile-img-->
<div class="small-12 medium-2 large-2 columns">
     <div class="circle">
       <!-- User Profile Image -->
       
	   @if ($user->profile_img && $user->profile_img!='null')
	   <img class="profile-pic"  src="{{ url('') }}/upload/images/{{ $user->profile_img }}" alt="{{$user->first_name}}">
	   @else
	   <img class="profile-pic" src="{{ url('') }}/images/avtar.png">
	   	   @endif
       <!-- Default Image -->
       <!-- <i class="fa fa-user fa-5x"></i> -->
     </div>
     
<!--end-profile-img-->

					<div class="upload-btn-wrapper">
						<button class="edit-profile-button upload-button"><img src="{{ url('') }}/images/icons/camera.png">UPLOAD PHOTO</button>
						<input class="file-upload" type="file" name="image" accept="image/*">
						</div>
			</div>
			<div class="pro-info">
				<h2>Edit Your Profile</h2>
				<p>Please fill out the fields below</p>
				<div class="edit-content">
					<div class="col-sm-6 p-0">
						<div class="form-group">
							<label class="control-label">First Name</label>
						<input type="text" name="first_name" class="form-control" placeholder="Enter Your First  Name" value="{{$user->first_name}}">
						@if ($errors->has('first_name'))
                        <span class="text-danger">{{ $errors->first('first_name') }}</span>
                    @endif
							</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label class="control-label">Last Name</label>
							<input type="text" name="last_name" class="form-control" placeholder="Enter Your Last Name" value="{{$user->last_name}}">
							@if ($errors->has('last_name'))
                        <span class="text-danger">{{ $errors->first('last_name') }}</span>
                    @endif
							</div>
					</div>
					<div class="col-sm-12 p-0">
						<div class="form-group">
							<label class="control-label">User Name</label>
							<input type="text" name="username" class="form-control" placeholder="Enter Your Username" value="{{$user->username}}">
							@if ($errors->has('username'))
                        <span class="text-danger">{{ $errors->first('username') }}</span>
                    @endif
							</div>
					</div>
					
				</div>
			</div>

			<div class="pro-info">
				<h2>Contact Information</h2>
				<p>Please fill out the fields below</p>
				<div class="edit-content">
					<div class="col-sm-6 p-0">
						<div class="form-group">
							<label class="control-label">Email Address</label>
							<input type="text" readonly name="email" class="form-control" placeholder="Enter Your Email" value="{{$user->email}}">
							@if ($errors->has('email'))
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                    @endif
							</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label class="control-label">Mobile Number</label>
							<input type="text" name="mobile_number" class="form-control" placeholder="Enter Your Mobile Number" value="{{$user->phone_number}}">
							@if ($errors->has('mobile_number'))
                        <span class="text-danger">{{ $errors->first('mobile_number') }}</span>
                    @endif
							</div>
					</div>
					<div class="col-sm-12 p-0">
						<div class="form-group">
							<label class="control-label">Address</label>
							<textarea name="address" class="form-control" placeholder="Enter Your Address" >{{$user->address}}</textarea>
							</div>
					</div>
					
				</div>
			</div>

			<div class="pro-info">
				<h2>Basic Information</h2>
				<p>Please fill out the fields below</p>
				<div class="edit-content">
					<div class="col-sm-6 p-0">
						<div class="form-group profiledatepicker">
							<label class="control-label">Birthday</label>
							<input type="text" autocomplete="off" name="dob" class="form-control" placeholder="Birthday" value="{{$user->dob}}">
							</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label class="control-label">Gender</label>
							
							<select class="form-control" name="gender">
								<option value="0">Select</option>
								<option value="1" @if ($user->gender==1) selected @endif>Male</option>
								<option value="2" @if ($user->gender==2) selected @endif>Female</option>
							</select>
							</div>
					</div>
					<div class="col-sm-12 p-0">
						<div class="form-group">
							<label class="control-label">Nationality</label>
							<select class="form-control" name="nationality">
									<option value="0">Select</option>
									@foreach ($data['nationality'] as $item)
							<option @if ($user->gender==$item->id) selected @endif value="{{$item->id}}">{{$item->name}}</option>
									@endforeach
								</select>
							</div>
					</div>
					
				</div>
			</div>

				<div class="pro-info">
				<h2>Area Of Interest</h2>
				<p>Please specif your areas of interest to ensure you are kept up to date with new games</p>
				<h3>Sports:</h3>
				<div class="edit-content">
						@foreach ($data['sports'] as $sport)
						<div class="intrest-checkbox">
							<input class="styled-checkbox" name="sports[]" id="styled-checkbox-{{$sport->id}}" type="checkbox" value="{{$sport->id}}" {{ (in_array($sport->id, $user_sport) ? 'checked' : '')}}>
								<label for="styled-checkbox-{{$sport->id}}">{{$sport->name}}</label>
							</div>
						@endforeach
				</div>
				<h3>Area:</h3>
				<br>
				<div class="drop-down-box">
					<dl class="dropdown-area"> 

							<dt>
							<a href="javascript:void(0)">
								@if (!$user_intrest)
								<span class="hida">Select</span> 
								@endif
								   
								<p class="multiSel">
										@foreach ($data['areas'] as $area)
										@if (in_array($area->id, $user_intrest))
										<span title="{{$area->name}},">{{$area->name}},</span>
										@endif
										
										@endforeach
								</p>  
							</a>
							</dt>
							
							<dd>
								<div class="mutliSelect">
								<ul style="{{ ($user_intrest) ? 'display:block;' : '' }}">
										@foreach ($data['areas'] as $area)
										<li><input id="styled-checkbox-{{ $area->id+100 }}" class="styled-checkbox" type="checkbox" name="area[]" title="{{$area->name}}" value="{{$area->id}}" 
										{{ (in_array($area->id, $user_intrest) ? 'checked' : '')}}
											/><label for="styled-checkbox-{{ $area->id+100 }}">{{$area->name}}</label></li>
										@endforeach
									</ul>
								</div>
							</dd>
						</dl>
						<button class="update-setting">UPDATE SETTINGS</button>
				</div>
			</div>
		</form>
		</div>
		<div class="tab-pane" id="2a">
			<div class="content-profile">
			<div class="pro-info">
				<h2>Change Your Password</h2>
				<p>Please select a memorable password</p>
				<form class="edit-content" action="{{ url('profile/changepassword') }}" method="POST">
					@csrf
					<div class="col-sm-12 p-0">
						<div class="form-group">
							<label class="control-label">New Password </label>
							<input type="password" name="password" class="form-control" placeholder="Password">
							@if ($errors->has('password'))
                        <span class="text-danger">{{ $errors->first('password') }}</span>
                    @endif
							</div>
					</div>
					<div class="col-sm-12 p-0">
						<div class="form-group">
							<label class="control-label">Confirm Password</label>
							<input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password">
							@if ($errors->has('password_confirmation'))
                        <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
                    		@endif
							</div>
					</div>
					<button type="submit" class="update-setting">Change My Password</button>
				</form>
				
				<br>
				{{-- <p class="not-happy">Not Happy? Cancel my account</p> --}}
			</div>
		</div>
	</div>
<div class="tab-pane" id="3a">
	<div class="content-profile">
			<div class="pro-info">
			<form class="edit-content notification-deta" action="{{ url('profile/setnotify') }}" method="POST">
				@csrf
					<div class="intrest-checkbox">
						<input class="styled-checkbox" @if ($user->nf_comment==1) checked @endif  name="nf_comment" id="styled-checkbox-1a" type="checkbox" value="1">
						<label for="styled-checkbox-1a">Email me when a new comment is added in a game I am attending </label>
					</div>
					<div class="intrest-checkbox">
						<input class="styled-checkbox" @if ($user->nf_game==1) checked @endif name="nf_game" id="styled-checkbox-2a" type="checkbox" value="1">
						<label for="styled-checkbox-2a">Email me when a game is added in a venue I am following</label>
					</div><div class="intrest-checkbox">
						<input class="styled-checkbox" @if ($user->nf_attendance==1) checked @endif name="nf_attendance" id="styled-checkbox-3a" type="checkbox" value="1">
						<label for="styled-checkbox-3a">If I am the event host, email me any changes in the attendance list</label>
					</div><div class="intrest-checkbox">
						<input class="styled-checkbox" @if ($user->nf_newsletter==1) checked @endif  name="nf_newsletter" id="styled-checkbox-4a" type="checkbox" value="1">
						<label for="styled-checkbox-4a">Weekly newsletter subscription</label>
					</div> 	 	
					<button type="submit" class="update-setting">UPDATE NOTIFICATION</button>
				</form>	
			
			</div>
	</div>
		</div>
	<div class="tab-pane" id="4a">
	<div class="content-profile">
			<div class="pro-info">
				<h2>Enter Your Location</h2>
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. copy</p>
				<form class="edit-content" action="{{ url('profile/addlocation') }}" method="POST">
					@csrf
					<div class="col-sm-12 p-0">
						<div class="form-group">
							<label class="control-label">Location </label>
						<input type="text" id="pac-input" name="location" class="form-control" placeholder="Enter Your Location" value="{{ @$location->location }}">
							@if ($errors->has('location'))
                        <span class="text-danger">{{ $errors->first('location') }}</span>
                    @endif
							</div>
					</div>
					<div class="col-sm-12 p-0">
						<div class="form-group">
							<label class="control-label">Address</label>
							<input readonly type="text" id="location_address" name="address" class="form-control" placeholder="Address"  value="{{ @$location->address }}">
							<input type="hidden" id="location_latitude" name="latitude"  value="{{ @$location->latitude }}">
							<input type="hidden" id="location_longitude" name="longitude"  value="{{ @$location->longitude }}">
							@if ($errors->has('address'))
                        <span class="text-danger">{{ $errors->first('address') }}</span>
                    @endif
							</div>
					</div>
					<div class="clearfix"></div>
				<div class="map-location">
						<div id="map"></div>
				</div>
				<button type="submit" class="update-setting">update Location</button>
				</form>
				
			</div>
		</div>
		</div>
	</div>
</div>

</div>
</div>
</div>
</div>
</div>
</section>
<!--account-setting-section-end-->

<script>
        // This example adds a search box to a map, using the Google Place Autocomplete
        // feature. People can enter geographical searches. The search box will return a
        // pick list containing a mix of places and predicted search terms.
  
        // This example requires the Places library. Include the libraries=places
        // parameter when you first load the API. For example:
        // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
  
        function initAutocomplete() {
			<?php
			if(@$location->latitude && @$location->longitude){
				$latitude=@$location->latitude;
				$longitude=@$location->longitude;
			}else{
				$latitude = 51.5073509;
				$longitude = -0.12775829999998223;
			}
			?>
          var map = new google.maps.Map(document.getElementById('map'), {

            center: {lat: {{ $latitude}}, lng: {{ $longitude }} },
            zoom: 13,
            mapTypeId: 'roadmap'
          });
		  var marker = new google.maps.Marker({map: map, position: {lat: {{ $latitude }}, lng: {{ $longitude }} }});
  
          // Create the search box and link it to the UI element.
		  var input = document.getElementById('pac-input');
			//var searchBox = new google.maps.places.SearchBox(input);
			var options = {
			// types: ['(cities)'],
			componentRestrictions: {country: 'GB'}//UK only
			};
			var autocomplete = new google.maps.places.Autocomplete(input,options);
          //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
		  
          // Bias the SearchBox results towards current map's viewport.
		  autocomplete.bindTo('bounds', map);
		  
		autocomplete.addListener('place_changed', function() {
          var places = autocomplete.getPlace();
          var lat = places.geometry.location.lat();
                var lng = places.geometry.location.lng();
                var address = places.formatted_address;
                    $('#location_latitude').val(lat);
                    $('#location_longitude').val(lng);
                    $('#location_address').val(address);

          // If the place has a geometry, then present it on a map.
          if (places.geometry.viewport) {
            map.fitBounds(places.geometry.viewport);
          } else {
            map.setCenter(places.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(places.geometry.location);
          marker.setVisible(true);

          

          
        });

        
      }
          
  
      </script>
@endsection

