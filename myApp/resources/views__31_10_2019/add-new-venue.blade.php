@extends('layouts.master')
@section('content')
<?php
$setting=\DB::table('site_setting')->first();
?>
<div class="clearfix"></div>
	<!--BANNER-SECTION-START-->
	<section class="banner-detail">
       <h1 class="hedding-detail">Add New Venue</h1>
    </section>
	<!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
	<!--CONTACT-US-SECTION-START-->
	
	<section class="form-contact">
		<div class="container">
			<div class="row">
					@if(Session::has('success'))
					<div class="alert alert-success">
						{{ Session::get('success') }}
						@php
							Session::forget('success');
						@endphp
						</div>
						@endif
				<div class="col-sm-10 col-sm-offset-1">
					<div class="heading-contact-form">
						<h1>Add New Venue</h1>
					</div>
					<div class="contact-us-form">
                        <form class="comment" method="POST" action="{{ url('submitVenue') }}">
                            @csrf
						<div class="col-sm-6 p-0">
							<div class="form-group">
								<label class="control-label">Name</label>
                                <input type="text" placeholder="Name" id="name1" name="name" class="form-control" autocomplete="city-name" value="{{ old('name') ? old('name') : @$edit->name }}">
                                @if ($errors->has('name'))
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            @endif
								</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label">Address</label>
                                <input type="text" placeholder="address" name="address" id="address1" class="form-control" autocomplete="address" value="{{ old('address') ? old('address') : @$edit->address }}">
                                    @if ($errors->has('address'))
                                    <span class="text-danger">{{ $errors->first('address') }}</span>
                                @endif
								</div>
						</div>
						<div class="col-sm-6 p-0">
							<div class="form-group">
								<label class="control-label">Location</label>
                                <input type="text" name="location" id="txtPlaces" placeholder="Enter Location" class="form-control" autocomplete="given-location" 
                                value="{{ old('location') ? old('location') : @$edit->location }}"  autofocus>
                               
                                
                                @if ($errors->has('location'))
                                <span class="text-danger">{{ $errors->first('location') }}</span>
                            @endif
								</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label">Postcode</label>
                                <input type="text" placeholder="Postcode" id="postcode" name="postcode" class="form-control" autocomplete="city-postcode" value="{{ old('postcode') ? old('postcode') : @$edit->postcode }}">
                        @if ($errors->has('postcode'))
                        <span class="text-danger">{{ $errors->first('postcode') }}</span>
                    @endif
								</div>
						</div>
						<div class="col-sm-6 p-0">
								<div class="form-group">
									<label class="control-label">Latitude</label>
									<input type="text" class="form-control" name="latitude" id="latitude" value="{{ old('latitude') ? old('latitude') : @$edit->latitude }}">
							@if ($errors->has('latitude'))
							<span class="text-danger">{{ $errors->first('latitude') }}</span>
						@endif
									</div>
							</div>
							<div class="col-sm-6">
									<div class="form-group">
										<label class="control-label">Longitude</label>
										<input type="text" class="form-control" name="longitude" id="longitude" value="{{ old('longitude') ? old('longitude') : @$edit->longitude }}">
								@if ($errors->has('longitude'))
								<span class="text-danger">{{ $errors->first('longitude') }}</span>
							@endif
										</div>
								</div>
						<div class="col-sm-12 pl-0">
								<div class="form-group">
									<label class="control-label">Description</label>
									<textarea class="form-control" name="description">{{@$edit->description}}</textarea>
									</div>
							</div>
						
					<div style="text-align: center;">
						<button class="submit">SUBMIT</button>
					</div>
					</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--CONTACT-US-SECTION-END-->

	<script>
		function initAutocomplete() {
          var options = {
			//types: ['(cities)'],
			componentRestrictions: {country: 'GB'}//UK only
			};
            var places = new google.maps.places.Autocomplete(document.getElementById('txtPlaces'),options);
            google.maps.event.addListener(places, 'place_changed', function () {
                var lat = places.getPlace().geometry.location.lat();
                var lng = places.getPlace().geometry.location.lng();
                var address = places.getPlace().formatted_address;
                var name = places.getPlace().name;
                    $('#latitude').val(lat);
                    $('#longitude').val(lng);
                    $('#address').val(address);
                    $('#name').val(name);
                   var place = places.getPlace();
                   for (var i = 0; i < place.address_components.length; i++) {
      for (var j = 0; j < place.address_components[i].types.length; j++) {
        if (place.address_components[i].types[j] == "postal_code") {
          $('#postcode').val(place.address_components[i].long_name);
        }
      }
    }
    
            });
        };
	</script>
@endsection