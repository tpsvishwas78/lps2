@extends('layouts.master')
@section('content')
	<div class="clearfix"></div>
	<!--BANNER-SECTION-START-->
	<section class="banner">
<div class="carousel fade-carousel slide" data-ride="carousel" data-interval="4000" id="bs-carousel">
  <!-- Overlay -->
  
  <!-- Wrapper for slides -->
  <div class="carousel-inner">
	  @if(@$banners)
	  @foreach ($banners as $key=> $banner)
	  @if ($banner->banner_type==2)
	  <div class="item slides @if ($key==0)  active  @endif">
			<div class="slide-2">
				<video width="100%" height="auto"  autoplay muted loop>
				<source src="{{ url('') }}/upload/banners/{{ $banner->banner }}" type="video/mp4">
				<source src="{{ url('') }}/upload/banners/{{ $banner->banner }}" type="video/ogg">
			  Your browser does not support the video tag.
			  </video>
			</div>
		  <div class="hero content-carousel-item">
			<hgroup>
				{{--  <div class="play_icon">
					<img src="{{ url('') }}/images/icons/play.png">
				</div>  --}}
				<h2>{{ $banner->title_one }}</h1>        
				<h1>{{ $banner->title_two }}<br> <span>{{ $banner->title_three }}</span></h1>
				<a href="{{ $banner->button_link }}" class="findmore">Join Mailing List</a>
			</hgroup>
		  </div>
		</div> 
	  @else
	  <div class="item slides @if ($key==0)  active  @endif">
			<div class="slide-2" style="background-image:url({{ url('') }}/upload/banners/{{ $banner->banner }}); height: 500px;">
				
			</div>
		  <div class="hero content-carousel-item">
			<hgroup>
				{{--  <div class="play_icon">
					<img src="{{ url('') }}/images/icons/play.png">
				</div>  --}}
				<h2>{{ $banner->title_one }}</h1>        
					<h1>{{ $banner->title_two }}<br> <span>{{ $banner->title_three }}</span></h1>
					<a href="{{ $banner->button_link }}" class="findmore">Join Mailing List</a>
			</hgroup>
		  </div>
		</div> 
	  @endif
	  
	  @endforeach
	  @endif
  </div> 
   <a class="left carousel-control" href="#bs-carousel" data-slide="prev">
         <span class="glyphicon glyphicon-chevron-left"><img src="{{ url('') }}/images/icons/prev.png"></span>
      </a>
      <a class="right carousel-control" href="#bs-carousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"><img src="{{ url('') }}/images/icons/next.png"></span>
      </a>
</div>
</section>
	<!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
	<!--SEARCH-SECTION-START-->
	<section class="search">
		<nav class="navbar navbar-light bg-light justify-content-between">
			<div class="container">
				<div class="col-sm-12">
			<a class="navbar-brand">FIND YOUR SPORT</a>
			<form class="form-inline" action="{{ url('') }}/searchgame">
				<input class="form-control mr-sm-2" type="text" name="search" placeholder="TOWN/CITY/POSTCODE" aria-label="Search" id="pac-input">
				<input class="form-control mr-sm-2" type="hidden" name="near_latitude" id="near_latitude">
				<input class="form-control mr-sm-2" type="hidden" name="near_longitude" id="near_longitude">
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit"><img src="{{ url('') }}/images/icons/search.png">Search Games</button>
			</form>
		</div>
		</nav>
	</section>
	<!--SEARCH-SECTION-END-->
	<div class="clearfix"></div>
	<!--SLIDER-SECTION-START-->
	<section class="slider">
			<div>
			

			<div class="full-width-crousel">
				<div id="owl-demo" class="owl-carousel owl-theme">
						@if(@$sliders)
						@foreach ($sliders as $key=> $slider)
						<?php
		if ($slider->link=='' || $slider->link=='#') {
			$link=url('')."/register-your-interest";
		} else {
			$link=$slider->link;
		}
		
						?>
				  <a href="{{ $link }}" class="item">
				  	<div class="col-md-12 col-sm-12 col-xs-12 p-0">
							<img src="{{ url('') }}/upload/sliders/{{ $slider->image }}" class="img-responsive">
							<div class="hover-deta-img-crousel">
								 <h2>{{ $slider->title }}</h2>
								 <p>{{ $slider->description }}</p>
							 </div>
						</div>
					</a>
					@endforeach
					@endif
			
				 
				</div>
			</div>
	</section>
	<!--SLIDER-SECTION-END-->
	<!--add-section-->
	{{-- <section class="add">
		<div class="container">
			<div class="img-add-here">
				<img src="images/add-img.jpg">
			</div>
		</div>
	</section> --}}
	<!--add-section-end-->
	<div class="clearfix"></div>
	<!--ABOUTUS-SECTION-START-->
	@if (@$aboutus)
		
	<section class="about_us">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="abot-us-content text-center">
						<h1>ABOUT US</h1>
						<!--<div class="video-section">
								{{-- <video width="100%" height="auto" controls>
										<source src="{{ url('') }}/upload/banners/{{ $aboutus->video }}" type="video/mp4">
										<source src="{{ url('') }}/upload/banners/{{ $aboutus->video }}" type="video/ogg">
									  Your browser does not support the video tag.
									  </video> --}}
									  <img src="{{ url('') }}/images/about-img.jpg">
							  <div class="play-video">
								<img src="{{ url('') }}/images/icons/play-video.png">
							</div>  
					    </div>-->
					    <p>{{ $aboutus->description }}</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	@endif
	<!--ABOUTUS-SECTION-END-->
	<div class="clearfix"></div>
	<!--WHY-JOIN-US-SECTION-START-->
	<section class="why-join-us">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="col-sm-6">
						<div class="image-phone">
							<img  src="{{ url('') }}/images/find-your-sport.png">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="why-join">
							<h1>WHY JOIN US</h1>
							<p>A sneak peak of our basic features </p>
						</div>
						<ul class="join-resion">
							@if (@$features)
								@foreach ($features as $feature)
								<li>
									{{ $feature->title }}
									</li>
								@endforeach
							@endif
							
							
						</ul>
						<a href="{{ url('') }}/pages/about-us" class="learn-more">LEARN MORE ABOUT US</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--WHY-JOIN-US-SECTION-END-->
	<div class="clearfix"></div>
	{{-- <section class="add">
		<div class="container">
			<div class="img-add-here">
				<img src="images/add-img2.jpg">
			</div>
		</div>
	</section> --}}
	<!--BASIC-STATUS-SECTION-START-->
	<!--<section class="basic-status">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 col-lg-offset-1 col-sm-12">
					<div class="basicstatus-hedding text-center">
						<h1>BASIC STATS FROM OUR WEB APP</h1>
						<p>Join our community</p>
					</div>
					<div class="col-sm-4">
						<div class="total-game">
							<h2>Total games played</h2>
							<h1>{{ $playedcount }}</h1>
							<p>OVERALL</p>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="content-status">
							<div class="head-section-status">
								<h2>Upcoming games <br> ({{ $upcomingcount }})</h2>
							</div>
							<ul>
								
								@foreach ($upcominggames as $upcominggame)
								
								<?php
@$spots=\DB::table('team_spots')->where('game_id',$upcominggame->id)->where('status',1)->count();
@$organizer=\DB::table('users')->where('id',$upcominggame->user_id)->first();
@$sport=\DB::table('mst_sports')->where('id',$upcominggame->sport_id)->first();
								?>
								<li>
									<a href="{{ url('') }}/games/{{ $upcominggame->slug }}">
									<div class="col-sm-3 col-xs-3 p-0">
										<div class="time-section">
											<div class="time">
												<h2>{{ $upcominggame->game_start_time }}</h2>
											</div>
											<div class="day">
												<p>{{ date('D', strtotime(@$upcominggame->game_date)) }}</p>
											</div>
										</div>
									</div>
									<div class="col-sm-7 col-xs-6">
										<h3>{{ $upcominggame->name }}</h3>
										<div class="img-text">
											<div class="img-upcome">
												
													@if (@$organizer->profile_img && $organizer->profile_img!='null')
													<img src="{{ url('') }}/upload/images/{{$organizer->profile_img}}" width="20" alt="{{$organizer->first_name}}">
													@else
													<img src="{{ url('') }}/images/avtar.png" width="20" >
													@endif
											</div>
											<p>{{ $upcominggame->team_limit }} a side {{ @$sport->name }} by {{ @$organizer->first_name }} {{ @$organizer->last_name }}</p>
									    </div>
									</div>
									<div class="col-sm-2 col-xs-3 p-0">
										<p class="price-up">
									@if (@$upcominggame->payment==1)
										@if ($upcominggame->currency=='USD')
                                            ${{ $upcominggame->price }}
                                        @else
                                       		 £{{ $upcominggame->price }}
										@endif

									@elseif(@$upcominggame->payment==2)
										@if ($upcominggame->currency=='USD')
                                            ${{ $upcominggame->price }}
                                        @else
                                        	£{{ $upcominggame->price }}
										@endif
									@else
										Free
										@endif
										</p>
										<p class="team-up">{{ @$spots }}/{{ @$upcominggame->team_limit*2 }}</p>
									</div>
								</a>
								</li>
							
								@endforeach
							</ul>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="content-status">
							<div class="head-section-status">
								<h2>Registered venues <br> ({{ $venuescount }})</h2>
							</div>
							<ul>
								@foreach ($venues as $venue)
								<?php
							$followes=\DB::table('venue_followers')->where('venue_id',$venue->id)->count();
								?>
								
								<li>
									<a href="{{ url('') }}/venue/{{ $venue->slug }}">
									<div class="col-sm-3 col-xs-3 p-0">
										@if ($venue->image)
										<img width="90" src="{{ url('') }}/upload/images/{{ $venue->image }}">
										@else
										<img width="90" src="{{ url('') }}/images/no-user-Image.png">
										@endif
										

									</div>
									<div class="col-sm-9 col-xs-9">
										<h3>{{ $venue->name }}</h3>
										<p>{{ $venue->location }}</p>
										@if ($followes>0)
										<span>{{ $followes }} followers</span>
										@endif
										
									</div>
								</a>
								</li>
								@endforeach
								
							</ul>
						</div>
					</div>
			</div>
		</div>
	</div>
	</section>-->
	<!--BASIC-STATUS-SECTION-END-->
	<div class="clearfix"></div>
	<!--add-section-->
	{{-- <section class="add">
		<div class="container">
			<div class="img-add-here">
				<img src="images/add-img.jpg">
			</div>
		</div>
	</section> --}}
	<!--add-section-end-->
	<!--APP-STORE-SECTION-START-->
	<div class="clearfix"></div>
	<section class="app-store">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="col-sm-6">
						<div class="download-now">
							<h3>DOWNLOAD NOW</h3>
							<h1>Available for  <br>all devices</h1>
						</div>
						<div class="buttons-playstore">
						<a href="{{ @$setting->app_store_link }}" target="_blank">
							<button class="ios"><img src="{{ url('') }}/images/icons/apple.png">Apple Store</button>
						</a>
						<a href="{{ @$setting->play_store_link }}" target="_blank">
							<button class="android"><img src="{{ url('') }}/images/icons/android.png">Play Store</button>
						</a>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="phone-img-boll">
							<img src="{{ url('') }}/images/phone.png">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--APP-STORE-SECTION-START-->
	<div class="clearfix"></div>
	
	

	<script>
            function initAutocomplete() {
      
			var input = document.getElementById('pac-input');
			//var searchBox = new google.maps.places.SearchBox(input);
			var options = {
			// types: ['(cities)'],
			componentRestrictions: {country: 'GB'}//UK only
			};
			var places = new google.maps.places.Autocomplete(input,options);
			google.maps.event.addListener(places, 'place_changed', function () {
                var lat = places.getPlace().geometry.location.lat();
                var lng = places.getPlace().geometry.location.lng();
                    $('#near_latitude').val(lat);
                    $('#near_longitude').val(lng);
                   
    
            });
     
  }
  
		</script>
		
		@endsection