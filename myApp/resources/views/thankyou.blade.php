<?php

use App\Models\MstGame;

$game = MstGame::where('slug', $data['title'])->first();
header("Refresh:10; url=" . url('/games/' . $data['title'] . '/' . $data['id']));

?>
@extends('layouts.usermaster')
@section('content')
<style>
    .section {
        margin-top: 20px;
        ;
    }
</style>
<div class="container section">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <div class="jumbotron text-xs-center text-center">

                <p>
                    <i style="color: #4CAF50;font-size: 70px;" class="fa fa-check" aria-hidden="true"></i>
                </p>
                <h3 class="display-3">Thank You!</h3>
                <p>
                    <h4 style="color: #989898; font-weight: 400;font-size: 19px;"> <?= (isset($data['title'])) ? ucwords($game->title) : '' ?> </h4>
                </p>
                <p style="padding: 20px;">

                    <a href="{{url('/games/'.$data['title'].'/'.$data['id'])}}">
                        <button class="btn btn-primary">
                            View Game
                        </button>
                    </a>
                </p>
                <p style="font-size: 15px;">
                    Thank you for booking you can see this in your joined games in your account. We have also emailed confirmation.
                </p>
                <!-- <p class="lead"><?= (isset($data['message'])) ? $data['message'] : '' ?></p> -->
                <!-- <p class="lead"><strong>Please check your email</strong> for further instructions on how to complete your account setup.</p> -->
                <!-- <hr> -->
                <!-- <a href="{{url('/games/'.$data['title'].'/'.$data['id'])}}">
                    <h4> <?= (isset($data['title'])) ? ucwords($game->title) : '' ?> </h4>
                </a> -->
                <!-- <p class="lead">
                    <a class="btn btn-primary btn-sm" href="https://bootstrapcreative.com/" role="button">Continue to homepage</a>
                </p> -->
            </div>
        </div>
    </div>
</div>
@endsection