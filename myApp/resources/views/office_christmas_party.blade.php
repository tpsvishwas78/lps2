@extends('layouts.master')
@section('content')
<?php
$setting=\DB::table('site_setting')->first();
?>
<div class="clearfix"></div>
	<!--BANNER-SECTION-START-->
	<section class="banner-detail"  style="background: url({{ url('') }}/images/Office-Christmas-Party.jpg);">
       <h1 class="hedding-detail">Office Christmas Party</h1>
    </section>
	<!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
	<!--crousel--and-video-section-start-->
	<section class="corporate-sports-day">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="col-sm-8">
						<div class="left-vide0-section">
							<div class="video-detail">
								<img src="images/office-chrimasassa.jpg">
								<div class="video-play-icon-cp">
									<img src="images/icons/play-video.png">
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="right-side-crousel">
							<!--crousel-->
							<div class="header" id="cover">
						<div class="carousel slide" data-ride="carousel" id="carousel">
						<div class="carousel-inner">
						<div class="item">
						
						<img class="responsive" alt="umpires" src="images/kdakdkd.jpg"></div>
						<div class="item active">
						
						<img class="responsive" alt="baseballd" src="images/office-chrimasassa.jpg">
						</div>
						<div class="item">
						
						<img class="responsive" alt="fallball5" src="images/kdakdkd.jpg">
						</div>
						<div class="item">
						
						<img class="responsive" alt="fallball4" src="images/office-chrimasassa.jpg">
						</div>
						
						</div>
						<div class="clearfix">
						<div class="carousel-link">
						<div class="thumb" data-slide-to="0" data-target="#carousel">
						<img class="responsive" alt="umpires" src="images/kdakdkd.jpg"></div>
						<div class="thumb" data-slide-to="1" data-target="#carousel">
						<img class="responsive" alt="baseballd" src="images/office-chrimasassa.jpg">
						</div>
						<div class="thumb" data-slide-to="2" data-target="#carousel">
						<img class="responsive" alt="fallball5" src="images/kdakdkd.jpg">
						</div>
						<div class="thumb" data-slide-to="3" data-target="#carousel">
						<img class="responsive" alt="fallball4" src="images/office-chrimasassa.jpg">
						</div>
						
						</div>
						</div>
						</div>
						</div>
						</div>
					</div>
					<div class="clearfix"></div>
					
				</div>
			</div>
		</div>
	</section>
<!--crousel--and-video-section-end-->
<section class="faq-christmas-party">
	<div class="container">
		<div class="row">
			<div class="heading-party">
				<h2>FREQUENTLY ASKED QUESTIONS</h2>
				<p>Please remember we're on hand to answer any question you may have, simply call 020 3589 6019</p>
			</div>
			<div class="box-faqs">
				<div class="col-sm-6">
					<div class="box-faq-red">
						<p>Check the numbers in your party and your preferred dates. Over 18's only</p>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="box-faq-black">
						<p>Check availability and make a booking by calling our events team on 02035896019 or:</p>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="box-faq-black">
						<p>Complete the enquiry form which can be found from any of the Christmas party pages - pay a deposit to secure the event.</p>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="box-faq-red">
						<p>Full payment of the balance and menu/drinks options, etc must be received by us no later than 8 weeks prior to your party date.</p>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="box-faq-red">
						<p>Tickets/e-tickets will be sent out to 1 week prior to the event date.</p>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="box-faq-black">
						<p>Tables sit between 8-12 guests, if your group exceed the maximum we will split your group as evenly as possible and set the tables close together.</p>
					</div>
				</div>
				<div class="clearfix"></div>
				<button class="enquiry-now">ENQUIRY NOW</button>
			</div>
		</div>
	</div>
</section>
	<script>
		function initAutocomplete() {
			var input = document.getElementById('pac-input');
			//var searchBox = new google.maps.places.SearchBox(input);
			var options = {
			types: ['(cities)'],
			componentRestrictions: {country: 'GB'}//UK only
			};
			var autocomplete = new google.maps.places.Autocomplete(input,options);
          
        }
	</script>
@endsection