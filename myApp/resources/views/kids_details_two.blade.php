@extends('layouts.master')
@section('content')
<?php
@$setting=\DB::table('site_setting')->first();
@$images=\DB::table('kids_gallery')->where('kid',@$party->id)->get();
@$included=\DB::table('kids_whats_included')->where('kid',@$party->id)->get();
@$blogs=\DB::table('kids_blogs')->where('category_id',@$party->category_id)->get();
?>
<div class="clearfix"></div>
	<!--BANNER-SECTION-START-->
	<section class="banner-detail" style="background: url({{ url('') }}/upload/images/{{ $cat->banner }});">
       <h1 class="hedding-detail">{{  $cat->name }}</h1>
    </section>
	<!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
	<!--crousel--and-video-section-start-->
	<section class="corporate-sports-day">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="col-sm-7">
						<div class="left-vide0-section">
							<div class="video-detail">
								<img src=" {{ url('') }}/upload/images/{{  $party->image }}">
							</div>
						</div>
					</div>
					<div class="col-sm-5">
						<div class="right-side-crousel">
							<!--crousel-->
							<div class="header" id="cover">
									<div class="carousel slide" data-ride="carousel" id="carousel">
											<div class='carousel-inner'>
													<div class='item active'>
													
													<img class='responsive' alt='umpires' src='{{ url('') }}/upload/images/{{ @$party->image }}'/>
												</div>
													@if (@$images)
														
													
													@foreach ($images as $image)
														
													
													<div class='item'>
													
													<img class='responsive' alt='fallball4' src='{{ url('') }}/upload/images/{{ $image->image }}'/>
													</div>
								
													@endforeach
													@endif
													
													</div>
									<div class="clearfix">
											<div class='carousel-link'>
													<div class='thumb' data-slide-to='0' data-target='#carousel'>
													<img class='responsive' alt='umpires' src='{{ url('') }}/upload/images/{{ @$party->image }}'/>
												</div>
												@foreach ($images as $key=> $thumb)
													<div class='thumb' data-slide-to='{{ $key+1 }}' data-target='#carousel'>
													<img class='responsive' alt='baseballd' src='{{ url('') }}/upload/images/{{ $thumb->image }}'/>
													</div>
													@endforeach
													</div>
													
									</div>
									</div>
									</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="childpara">{!! @$party->description !!}</div>
				</div>
				</div>
				</div>
				</div>
			</div>
		</div>
	</section>
<!--crousel--and-video-section-end-->
<!--section-key-facts-->
<section class="sports-challanges">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="head-challange">
                        <h1>{{ @$party->secound_title }} </h1>
                        {!! @$party->secound_description !!}
					</div>
					<div class="tab-design-function">
						 <!--<div class="col-sm-12 p-0">
						 	<h1 class="whats-included">Whats Included</h1>
				        <div class="col-sm-5 pl-0">
				          <ul class="nav nav-tabs tabs-left sideways">
                    
                            @foreach ($included as $key => $include)
                            <li @if ($key==0)
                            class="active"
                            @endif ><a href="#home-{{ $key }}" data-toggle="tab" aria-expanded="false">
                                    <div class="drop-down-icon-filter-corporate"></div>{{ $include->title }}</a>
                                </li>
                            @endforeach
																            
				          </ul>
				        </div>
				        <div class="col-sm-7">
				         <div class="tab-content">
                                @foreach ($included as $key => $include)
                                        
                                   						
				            <div class="tab-pane @if ($key==0)
                             active
                            @endif" id="home-{{ $key }}">
				            	<div class="content-info-lorem">
				            		<p>{{ $include->description }}</p>
				            	</div>
                            </div>
                            @endforeach	
															
							
				          </div>
				        </div>

				        <div class="clearfix"></div>

				      </div>-->
					</div>
					</div>
			</div>
		</div>
	</sectio>
	<div class="clearfix"></div>
<!--section-key-facts--end-->
<section class="serach-result">
		<div class="container">
			<div class="row">
				<!--game-listing-start-->
				<div class="col-sm-12">
                    @if (@$blogs)
                    @foreach ($blogs as $blog)
                    <div class="listing-games">
                            <div class="design-list">
                                <div class="col-sm-9">
                                    <div class="info-game">
                                        <p>{{ $blog->title }}</p>
                                        
                                        <div class="dis-game">{{ $blog->description }}</div>
                                    </div>
                                </div>
                                <div class="col-sm-3 p-0">
                                <div class="game-img">
                                    <img src="{{ url('') }}/upload/images/{{ $blog->image }}">
                                </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    @endif
					
				</div>
				<!--game-listing-end-->
			</div>
		</div>
	</section>
	<script>
		function initAutocomplete() {
			var input = document.getElementById('pac-input');
			//var searchBox = new google.maps.places.SearchBox(input);
			var options = {
			types: ['(cities)'],
			componentRestrictions: {country: 'GB'}//UK only
			};
			var autocomplete = new google.maps.places.Autocomplete(input,options);
          
        }
	</script>
@endsection