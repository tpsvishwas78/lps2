@extends('layouts.usermaster')

@section('content')

<?php
@$location=\DB::table('mst_venue')->where('id',$game->venue_id)->first();
?>

<!--BANNER-SECTION-START-->
<section class="banner-detail">
    <h1 class="hedding-detail">Payments</h1>
 </section>
 <!--BANNER-SECTION-END-->
 <div class="clearfix"></div>
 <!--account-setting-section-start-->
 <section class="account-setting-content">
     <div class="container">
         <div class="row">
             <div class="col-sm-12 p-0">
                 <div class="col-sm-12">
                     <h3 class="text-center">Your booking: {{ @$location->name }} ({{ date('D, M d, Y', strtotime(@$game->game_start_date)) }} {{ @$game->game_start_time }})</h3>
                     
                     <div class="clearfix"></div>

                     <div class="ammount-box">
                         <h2>You have to confirm your payment</h2>
                         
                            <ul class="list-group no-radius text-right box-vailue">
                                    <li class="list-group-item">
                                      <span class="m-l pull-right" style="margin-left: 15px;">£{{ number_format(@$game->price,2) }}</span>
                                      Cost
                                    </li>
                                      <!-- <li class="list-group-item">
                                        <span class="m-l pull-right" style="margin-left: 15px;">
                                            
                                            <?php
                            @$vat=@$game->price*20/100;
                            @$total=@$game->price;
                                            ?>
                                            £{{ number_format(@$vat,2) }}
                                        </span>
                                        VAT (20%)
                                      </li> -->
                                      <li class="list-group-item"><strong>
                                        <span class="m-l pull-right" style="margin-left: 15px;">£{{ number_format(@$total,2) }}</span>
                                        Total</strong>
                                      </li>
                                    
                                  </ul>
                     </div>
                     <div class="clearfix"></div>
                   <div class="payment-accordion" style="width: 100%;margin: auto;">
                       <h2>Your Payment Details</h2>
                       
                           <div class="panel-group" id="accordion">
                               
                             <div class="panel panel-default">
                              
<form action="<?php echo url('charge') ?>" method="post"
        id="bookingForm" enctype="multipart/form-data" autocomplete="off">
      <input type="hidden" value="<?php echo csrf_token(); ?>" name="_token" id="csrf_token">
        <input type="hidden" name="totalprice" value="{{ number_format(@$total,2) }}">
        <input type="hidden" name="game_amount" value="{{ @$game->price }}">
        <input type="hidden" name="vat_amount" value="{{ @$vat }}">
        <input type="hidden" name="game_id" value="{{ @$game->id }}">
        <input type="hidden" name="game_pid" value="{{ @$game->gid }}">
        <input type="hidden" name="game_author_id" value="{{ @$game->user_id }}">
        <input type="hidden" name="game_slug" value="{{ @$game->slug }}">
                               <div class="panel-heading">
                                 <h4 class="panel-title">
                                     <input type='radio' id='r11' name='occupation' value='Working' />
                                     <label for='r11' >
                                       <span> Debit/Credit Card</span>
                                      
                                       <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"></a>
                                     </label>
                                      <ul>
                                           <li><img src="{{ url('') }}/images/icons/visa.png"></li>
                                         <li><img src="{{ url('') }}/images/icons/master.png"></li>
                                       </ul>
                                 </h4>
                               </div>
                               <div id="collapseOne" class="panel-collapse collapse in">
                                 <div class="panel-body">
                                  <div class="edit-content">
                                     <div class="col-sm-12 ">
                                         <div class="form-group">
                                             <label class="control-label">Card Holder Name</label>
                                             <input type="text" name="cardName" data-worldpay="name" class="form-control" autocomplete="off" placeholder="Enter Your Name" required="required">
                                             </div>
                                     </div>
                                     <div class="col-sm-12">
                                         <div class="form-group">
                                             <label class="control-label">Card Number</label>
                                             <input type="text" type="text" size="20" data-worldpay="number"class="form-control" autocomplete="off" placeholder="Enter Your Card Number" required="required">
                                             </div>
                                     </div>
                                     <div class="col-sm-6">
                                         <div class="form-group">
                                             <label class="control-label">Expiration Month</label>
                                             <input type="text" data-worldpay="exp-month" required="required" autocomplete="off" class="form-control" placeholder="MM">
                                             </div>
                                     </div>
                                     <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Expiration Year</label>
                                                <input type="text" data-worldpay="exp-year" required="required" autocomplete="off" class="form-control" placeholder="YYYY">
                                                </div>
                                        </div>
                                         <div class="col-sm-12">
                                         <div class="form-group">
                                             <label class="control-label">CVV</label>
                                             <input placeholder="CVC" type="password" size="4" data-worldpay="cvc" required="required" autocomplete="off" class="form-control" >
                                             <img class="tool-tip-icon" src="{{ url('') }}/images/icons/cvv.png">
                                             </div>
                                     </div>
                                     
                                 </div>
                                 
                                 <div id="paymentErrors"></div>
                                 {{-- <p class="save-details"><img src="{{ url('') }}/images/icons/save-details.png">Save my details for future payments.</p>
                                 <p class="save-details">Is it safe? Yes, all sensitive information are securely stored via Stripe, certified to PCI Service Provider Level 1</p> --}}
                                 <button type="submit" class="make-payment">MAKE PAYMENT</button>
                                 </div>
                               </div>
                            </form>
                             </div>
                             

                             {{-- <div class="panel panel-default">
                               <div class="panel-heading">
                                 <h4 class=panel-title>
                                     <input type='radio' id='r13' name='occupation' value='Not-Working' required />
                                     <label for='r13'>
                                       <span> Paypal</span>
                                      
                                       <a data-toggle="collapse" data-parent="#accordion" href="#collapsethree"></a>
                                     </label>
                                      <ul>
                                           <li><img src="{{ url('') }}/images/icons/pay-pall.png"></li>
                                       </ul>
                                 </h4>
                               </div>
                               <div id="collapsethree" class="panel-collapse collapse">
                                 <div class="panel-body">
                                  <div class="edit-content">
                                     <div class="col-sm-12 ">
                                         <div class="form-group">
                                             <label class="control-label">Card Holder Name</label>
                                             <input type="text" name="regular" class="form-control" placeholder="Enter Your Name">
                                             </div>
                                     </div>
                                     <div class="col-sm-12">
                                         <div class="form-group">
                                             <label class="control-label">Card Number</label>
                                             <input type="text" name="regular" class="form-control" placeholder="Enter Your Card Number">
                                             </div>
                                     </div>
                                     <div class="col-sm-6">
                                         <div class="form-group">
                                             <label class="control-label">Expiration Date</label>
                                             <input type="text" name="regular" class="form-control" placeholder="09/20">
                                             </div>
                                     </div>
                                         <div class="col-sm-6">
                                         <div class="form-group">
                                             <label class="control-label">CVV</label>
                                             <input type="text" name="regular" class="form-control" placeholder="09/20">
                                             <img class="tool-tip-icon" src="{{ url('') }}/images/icons/cvv.png">
                                             </div>
                                     </div>
                                     
                                 </div>
                                 <p class="save-details"><img src="{{ url('') }}/images/icons/save-details.png">Save my details for future payments.</p>
                                 <p class="save-details">Is it safe? Yes, all sensitive information are securely stored via Stripe, certified to PCI Service Provider Level 1</p>
                                 <button class="make-payment">MAKE PAYMENT</button>
                                 </div>
                               </div>
                             </div> --}}

                              
                           </div>
                         
                   </div>
                 </div>
             </div>
         </div>
     </div>
 </section>
 <!--account-setting-section-end-->

 <script type="text/javascript" src="https://cdn.worldpay.com/v1/worldpay.js"></script>
 <script>
     $(function () {
         var worldPayKey = "<?php echo config('worldpay.live.client'); ?>";
         var form = document.getElementById('bookingForm');
         Worldpay.useOwnForm({
             'clientKey': worldPayKey,
             'form': form,
             'reusable': false,
             'callback': function (status, response) {
                 console.log('response',response);
                 console.log('status',status);
                 $('#paymentErrors').html('');
                 if (response.error) {
                     Worldpay.handleError(form, $('#paymentErrors'), response.error);
                     $('#paymentErrors').html(response.error.message);
                     $('input[type="submit"]').prop('disabled', false);
                 } else {
                     var token = response.token;
                     Worldpay.formBuilder(form, 'input', 'hidden', 'token', token);
                     form.submit();
                 }
             }
         });
     });
 </script>

@endsection