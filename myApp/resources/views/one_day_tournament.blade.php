@extends('layouts.master')
@section('content')

<!--BANNER-SECTION-START-->
<section class="banner-detail corporat-banner-one-day">
		<div  class="hedding-children">
       <h1>Sports Tournaments </h1>
       </div>
    </section>
	<!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
	<!--SLIDER-SECTION-START-->
	<section class="slider">
			<div>
			

			<div class="full-width-crousel">
				<div id="owl-demo" class="owl-carousel owl-theme">
						@if(@$sliders)
						@foreach ($sliders as $key=> $slider)
				  <a href="{{ $slider->link }}" class="item">
				  	<div class="col-md-12 col-sm-12 col-xs-12 p-0">
							<img src="{{ url('') }}/upload/sliders/{{ $slider->image }}" class="img-responsive">
							<div class="hover-deta-img-crousel">
								 <h2>{{ $slider->title }}</h2>
								 <p>{{ $slider->description }}</p>
							 </div>
						</div>
					</a>
					@endforeach
					@endif
			
				 
				</div>
			</div>
	</section>
	<!--SLIDER-SECTION-END-->
	<div class="clearfix"></div>
	<!--soft-play-section-start-->
	  <section class="blocks-children">
	  	<div class="container">
	  		<div class="row">
	  	 <div class="col-sm-12">
	  	 	<div class="col-sm-4">
					<a href="{{ url('') }}/corporate-detail">
	  	 		<div class="block-img">
	  	 			<img src="{{ url('') }}/images/oneday-img1.jpg">
	  	 			<div class="hover-deta-img team-build">
						   <h2>Drapers Field</h2>
	  	 				<!--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>-->
	  	 			</div>
				   </div>
					</a>
	  	 	</div>
	  	 	<div class="col-sm-4">
					<a href="{{ url('') }}/corporate-detail">
	  	 		<div class="block-img">
	  	 			<img src="{{ url('') }}/images/oneday-img2.jpg">
	  	 			<div class="hover-deta-img team-build">
	  	 				<h2>Paradise Park</h2>
	  	 				<!--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>-->
	  	 			</div>
				   </div>
					</a>
	  	 	</div>
	  	 	<div class="col-sm-4">
					<a href="{{ url('') }}/corporate-detail">
	  	 		<div class="block-img">
	  	 			<img src="{{ url('') }}/images/oneday-img3.jpg">
	  	 			<div class="hover-deta-img team-build">
	  	 				<h2>West Way Sports centre</h2>
	  	 				<!--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>-->
	  	 			</div>
				   </div>
					</a>
	  	 	</div>
	  	 	<div class="col-sm-4">
					<a href="{{ url('') }}/corporate-detail">
	  	 		<div class="block-img">
	  	 			<img src="{{ url('') }}/images/oneday-img4.jpg">
	  	 			<div class="hover-deta-img team-build">
	  	 				<h2>Rocks Lane Chiswick</h2>
	  	 				<!--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>-->
	  	 			</div>
				   </div>
					</a>
	  	 	</div>
	  	 	<div class="col-sm-4">
					<a href="{{ url('') }}/corporate-detail">
	  	 		<div class="block-img">
	  	 			<img src="{{ url('') }}/images/oneday-img5.jpg">
	  	 			<div class="hover-deta-img team-build">
	  	 				<h2>Archbishop Park</h2>
	  	 				<!--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>-->
	  	 			</div>
				   </div>
					</a>
	  	 	</div><div class="col-sm-4">
					<a href="{{ url('') }}/corporate-detail">
	  	 		<div class="block-img">
	  	 			<img src="{{ url('') }}/images/oneday-img1.jpg">
	  	 			<div class="hover-deta-img team-build">
	  	 				<h2>Drapers Field</h2>
	  	 				<!--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>-->
	  	 			</div>
				   </div>
					</a>
	  	 	</div>
	  	 </div>
	  	</div>
	  	</div>
	  </section>
	<!--soft-play-section-end-->

@endsection