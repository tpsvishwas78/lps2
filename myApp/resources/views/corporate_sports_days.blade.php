@extends('layouts.master')
@section('content')

<!--BANNER-SECTION-START-->
<section class="banner-detail corporat-banner-one-day">
		<div  class="hedding-children">
       <h1>Corporate Sports Days </h1>
       </div>
    </section>
	<!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
	
	<div class="clearfix"></div>
	<!--soft-play-section-start-->
	  <section class="blocks-children">
	  	<div class="container">
	  		<div class="row">
	  	 <div class="col-sm-12">

	  	 	<div class="col-sm-6">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/Wz7qmJix1S8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
               </div>
               <div class="col-sm-6">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/oz0egzRr01k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
               </div>
               <div class="col-sm-6">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/fWwR3O83kw0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
               </div>
               <div class="col-sm-6">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/8JS4HLUGV5s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
               </div>
               <div class="col-sm-6">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/LfSVaa0E61k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
               </div>
               <div class="col-sm-6">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/TkW3XepxveQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
               </div>
               <div class="col-sm-6">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/Q4_MPoULgHg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
               </div>
               <div class="col-sm-6">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/7Xvr1zJ1QX4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	  	 	</div>
	  	 	
	  	 </div>
	  	</div>
	  	</div>
	  </section>
	<!--soft-play-section-end-->

@endsection