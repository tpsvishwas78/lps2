@extends('layouts.master')
@section('content')

<!--BANNER-SECTION-START-->
<section class="banner-detail"  style="background: url({{ url('') }}/images/Childrens-Coaching.jpg);">
		<div  class="hedding-children">
       <h1>Children's Coaching</h1>
       </div>
    </section>
	<!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
	<!--available-at-section-start-->
	<section class="available-at">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="search-fields">
						<form action="{{ url('') }}/children-coaching">
							<div class="select-coaching">
									<select class="getsearchchange">
											<option value="{{ url('') }}/children-coaching">Coaching</option>
											<option value="{{ url('') }}/kids-5-a-side-leagues">Kids 5 a ...</option>
									</select>
							</div>
							<div class="selectcity-town">
								<input type="text" name="search"  id="pac-input" value="{{ @$_GET['search'] }}" placeholder="SEARCH BY TOWN/CITY/POSTCODE">
							</div>
							<button  class="search"><img src="{{ url('') }}/images/icons/search.png">SEARCH </button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--available-at-section-end-->
	<div class="clearfix"></div>
	
	<!--SEARCH-RESULT-SECTION-START-->
	<section class="serach-result">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="map-sports">
							<div id="map"></div>
					</div>
					<div class="filter_vailue">
							<select name="sort_by" id="sort_by" class="selectfilter sportfilter">
									<option value="">Filter Venues</option>
									<option @if (@$_GET['sortby']=='ASC')
										selected
									@endif value="ASC">A-Z</option>
									<option @if (@$_GET['sortby']=='DESC')
									selected
								@endif value="DESC">Z-A</option>
								</select>
						</div>
					<div class="map-toggle-button">
							<p>SHOW MAP</p>
							<label class="switch">
							  <input class="switch-button" type="checkbox">
							  <span class="slider-button round"></span>
							</label>
						</div>
					</div>
				<!--game-listing-start-->
				<div class="col-sm-12">
					{{-- <div class="col-sm-12 p-0">
					<div class="filter_vailue">
						<ul class="list-unstyled">
						    <li class="init">Filter</li>
						    <li data-value="value 1">Option 1</li>
						    <li data-value="value 2">Option 2</li>
						    <li data-value="value 3">Option 3</li>
						</ul>
						<div class="drop-down-icon-filter"></div>
					</div>
					</div> --}}
					<div class="clearfix"></div>

					

					@foreach ($parties as $party)
						
					
					<div class="listing-games">
						<div class="design-list">
							<div class="col-sm-9">
								<div class="info-game">
									<a href="{{ url('') }}/children-detail/{{ $party->slug }}">
										<p>{{ $party->title }}</p>
									</a>
									
									<div class="dis-game"><img src="{{ url('') }}/images/icons/when-where-location.png">{{  $party->venue_address  }}</div>

									<div class="button-group">
											{{-- <button class="enq">SELECT VENUE</button> --}}
										<a href="{{ url('') }}/children-detail/{{ $party->slug }}">
										<button class="more-info">More Info</button>
										</a>
									</div>
								</div>
							</div>
							<div class="col-sm-3 p-0">
							<div class="game-img">
									<a href="{{ url('') }}/children-detail/{{ $party->slug }}">
										@if ($party->feature_image)
                                    <img src="{{url('')}}/upload/images/{{$party->feature_image}}">
                                @else
                                    <img src="{{url('')}}/images/no-user-Image.png">
                                @endif
									</a>
							</div>
							</div>
						</div>
					</div>
					@endforeach
					
					
					
				</div>
				<!--game-listing-end-->
			</div>
			{{-- <div class="load-more-button">
				<button class="load-more">LOAD MORE</button>
			</div> --}}
		</div>
	</section>
	<!--SEARCH-RESULT-SECTION-END-->
	<script>
	$(".sportfilter").change(function(){
                var sortby=$(this).val();
                var search=$('#pac-input').val();
                window.history.pushState({}, '', 'children-coaching?search='+search+'&sortby='+sortby);
                location.reload();
            });

$( ".getsearchchange" ).change(function() {
 
  window.location.href = $(this).val();
});
            function initAutocomplete() {
      var locations = [
          @foreach ($parties as $party)
            ['{{$party->venue_address}}', {{$party->latitude}}, {{$party->longitude}}, 1],
            @endforeach
          
      ];
  
      window.map = new google.maps.Map(document.getElementById('map'), {
          mapTypeId: google.maps.MapTypeId.ROADMAP
	  });
	  
	  var input = document.getElementById('pac-input');
			//var searchBox = new google.maps.places.SearchBox(input);
			var options = {
			// types: ['(cities)'],
			componentRestrictions: {country: 'GB'}//UK only
			};
            var autocomplete = new google.maps.places.Autocomplete(input,options);
            
            var inputlocation = document.getElementById('pac-location');
			//var searchBox = new google.maps.places.SearchBox(input);
			var optionslocation = {
			// types: ['(cities)'],
			componentRestrictions: {country: 'GB'}//UK only
			};
			var autocompletelocation = new google.maps.places.Autocomplete(inputlocation,optionslocation);
  
            

      var infowindow = new google.maps.InfoWindow();
  
      var bounds = new google.maps.LatLngBounds();
  
      for (i = 0; i < locations.length; i++) {
          marker = new google.maps.Marker({
              position: new google.maps.LatLng(locations[i][1], locations[i][2]),
              map: map
          });
  
          bounds.extend(marker.position);
  
          google.maps.event.addListener(marker, 'click', (function (marker, i) {
              return function () {
                  infowindow.setContent(locations[i][0]);
                  infowindow.open(map, marker);
              }
          })(marker, i));
      }
  
      map.fitBounds(bounds);
  
      var listener = google.maps.event.addListener(map, "idle", function () {
          map.setZoom(3);
          google.maps.event.removeListener(listener);
      });
  }
  
        </script>

@endsection