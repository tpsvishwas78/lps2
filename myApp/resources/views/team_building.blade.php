@extends('layouts.master')
@section('content')

<!--BANNER-SECTION-START-->
<section class="banner-detail">
		<div  class="hedding-children">
       <h1>Team Building Days </h1>
       </div>
    </section>
	<!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
	<!--soft-play-section-start-->
	  <section class="blocks-children">
	  	<div class="container">
	  		<div class="row">
	  	 <div class="col-sm-12">
	  	 	<div class="col-sm-4">
					<a href="{{ url('') }}/corporate-detail">
	  	 		<div class="block-img">
	  	 			<img src="{{ url('') }}/images/team-img2.jpg">
	  	 			<div class="hover-deta-img team-build">
	  	 				<h2>Sports Days</h2>
	  	 				<!--<p>None of these really need content mate </p>-->
	  	 			</div>
				   </div>
					</a>
	  	 	</div>
	  	 	<div class="col-sm-4">
					<a href="{{ url('') }}/corporate-detail">
	  	 		<div class="block-img">
	  	 			<img src="{{ url('') }}/images/team-img1.jpg">
	  	 			<div class="hover-deta-img team-build">
	  	 				<h2>Its a knockout Fun days</h2>
	  	 				<!--<p>None of these really need content mate </p>-->
	  	 			</div>
				   </div>
					</a>
	  	 	</div>
	  	 	<div class="col-sm-4">
					<a href="{{ url('') }}/corporate-detail">
	  	 		<div class="block-img">
	  	 			<img src="{{ url('') }}/images/team-img2.jpg">
	  	 			<div class="hover-deta-img team-build">
	  	 				<h2>Sports Days</h2>
	  	 				<!--<p>None of these really need content mate </p>-->
	  	 			</div>
				   </div>
					</a>
	  	 	</div>
	  	 	<div class="col-sm-4">
					<a href="{{ url('') }}/corporate-detail">
	  	 		<div class="block-img">
	  	 			<img src="{{ url('') }}/images/team-img4.jpg">
	  	 			<div class="hover-deta-img team-build">
	  	 				<h2>BUBBLE <br> FOOTBALL</h2>
	  	 				<!--<p>None of these really need content mate </p>-->
	  	 			</div>
				   </div>
					</a>
	  	 	</div>
	  	 	<div class="col-sm-4">
					<a href="{{ url('') }}/corporate-detail">
	  	 		<div class="block-img">
	  	 			<img src="{{ url('') }}/images/team-img3.jpg">
	  	 			<div class="hover-deta-img team-build">
	  	 				<h2>FOOTBALL INFLATABLE EVENTS</h2>
	  	 				<!--<p>None of these really need content mate </p>-->
	  	 			</div>
				   </div>
					</a>
	  	 	</div><div class="col-sm-4">
					<a href="{{ url('') }}/corporate-detail">
	  	 		<div class="block-img">
	  	 			<img src="{{ url('') }}/images/team-img2.jpg">
	  	 			<div class="hover-deta-img team-build">
	  	 				<h2>Sports Days</h2>
	  	 				<!--<p>None of these really need content mate </p>-->
	  	 			</div>
				   </div>
					</a>
	  	 	</div>
	  	 </div>
	  	</div>
	  	</div>
	  </section>
	<!--soft-play-section-end-->

@endsection