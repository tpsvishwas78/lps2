@extends('layouts.master')
@section('content')

<section class="banner-detail">
        <h1 class="hedding-detail">{{ $page->name }}</h1>
     </section>
     <div class="clearfix"></div>

     <section class="account-setting-content">
<div class="container">
        <div class="row">
                <div class="col-sm-12 p-0">
{!! $page->description !!}
        </div>
    </div>
</div>
     </section>
@endsection