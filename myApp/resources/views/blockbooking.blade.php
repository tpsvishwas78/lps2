@extends('layouts.master')
@section('content')

<style>
.error {
    color: red;
}
.form-group {
    margin-bottom: 25px;
    position: relative;
    min-height: 80px;
}
.comment input.form-control {
    margin-bottom: 10px !important;
}
</style>

<!--BANNER-SECTION-START-->
<section class="banner-detail">
		<div  class="hedding-children">
       <h1>Block booking <br>sports facilities </h1>
       </div>
    </section>
	<!--BANNER-SECTION-END-->
	<div class="clearfix"></div> 
	<!--available-at-section-start-->
	<section class="available-at">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="search-fields">
							<form action="{{ url('') }}/children-coaching">
							<div class="select-coaching">
								<select class="getsearchchange">
									<option value="{{ url('') }}/children-coaching">Coaching</option>
									<option value="{{ url('') }}/kids-5-a-side-leagues">Kids 5 a ...</option>
							</select>
							</div>
							<div class="selectcity-town">
									<input type="text" name="search"  id="pac-input" value="{{ @$_GET['search'] }}" placeholder="SEARCH BY TOWN/CITY/POSTCODE">
								</div>
							<button  class="search"><img src="{{ url('') }}/images/icons/search.png">SEARCH </button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--available-at-section-end-->
	<div class="clearfix"></div>
	<!--BOOKING-SECTION-START-->
	<section class="form-contact">
		<div class="container">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<div class="heading-contact-form">
						<h1>Block Booking</h1>
						<p>If you wish to book one of our facilities weekly or more regulary and have a team already then please fill out the form below and we will come back to you with our availability within 24 hours.</p>
					</div>
					@if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
                @php
                    Session::forget('success');
				@endphp
				</div>
				@endif
					<div class="contact-us-form">
						<form class="comment" id="comment" action="{{ url('') }}/submitblockboking" method="POST" enctype="multipart/form-data">
							@csrf
							<div class="col-sm-6 p-0">
									<div class="form-group">
										<label class="control-label">First Name</label>
										<input type="text" name="first_name"  class="form-control"  placeholder="Enter Your First  Name" value="{{ old('first_name') }}">
										@if ($errors->has('first_name'))
								<span class="text-danger">{{ $errors->first('first_name') }}</span>
							@endif
										</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="control-label">Last Name</label>
										<input type="text" name="last_name" class="form-control"  placeholder="Enter Your Last Name" value="{{ old('last_name') }}">
										@if ($errors->has('last_name'))
								<span class="text-danger">{{ $errors->first('last_name') }}</span>
							@endif
										</div>
								</div>
								<div class="col-sm-6 p-0">
									<div class="form-group">
										<label class="control-label">Your Email</label>
										<input type="text" name="email" class="form-control"  placeholder="Enter Your Email" value="{{ old('email') }}">
										@if ($errors->has('email'))
										<span class="text-danger">{{ $errors->first('email') }}</span>
									@endif	
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="control-label">Mobile Number</label>
										<input type="text" name="mobile_number" class="form-control"  placeholder="Enter Your Mobile Number" value="{{ old('mobile_number') }}">
										@if ($errors->has('mobile_number'))
										<span class="text-danger">{{ $errors->first('mobile_number') }}</span>
									@endif
										</div>
								</div>
								<div class="col-sm-6 p-0">
									<div class="form-group">
										<label class="control-label">Ideal Location</label>
										<input type="text" name="venue_name" class="form-control"  placeholder="Select Venue" autocomplete="off" id="venue1" style="margin-bottom: 0px;">
										<input type="hidden" id="venue_id" name="venue">
										@if ($errors->has('venue'))
										<span class="text-danger">{{ $errors->first('venue') }}</span>
									@endif
									<div id="venueList">
									</div>
										</div>
								</div>
						<div class="col-sm-6">
							
								<div class="form-group datepicker">
										<label class="control-label">Ideal Day</label>
										<input autocomplete="off" type="text" name="date" class="form-control"  placeholder="Select days of booking" value="{{ old('date') }}">
										@if ($errors->has('date'))
										<span class="text-danger">{{ $errors->first('date') }}</span>
									@endif	
									</div>
						</div>
						<div class="col-sm-6 p-0">
								<div class="form-group timepicker">
									<label class="control-label">Time</label>
									<input type="text" autocomplete="off" name="time" class="form-control"  placeholder="Select Your Time">
									@if ($errors->has('time'))
									<span class="text-danger">{{ $errors->first('time') }}</span>
								@endif
									</div>
							</div>
							<div class="col-sm-6">
									<div class="form-group">
										<label class="control-label">Sport</label>
										<select name="sport" id="sport" class="form-control" >
											<option value="">Select</option>
											@foreach ($sports as $sport)
											<option value="{{ $sport->id }}">{{ $sport->name }}</option>
											@endforeach
											
										</select>
										@if ($errors->has('sport'))
										<span class="text-danger">{{ $errors->first('sport') }}</span>
									@endif	
									</div>
								</div>
						<div class="col-sm-12 pl-0">
							<div class="form-group">
								<label class="control-label">Comments</label>
								<textarea class="form-control textarea" name="comments" style="padding: 15px;" placeholder="Enter Your Comments"></textarea>
								@if ($errors->has('comments'))
										<span class="text-danger">{{ $errors->first('comments') }}</span>
								@endif
							</div>
						</div>
						<div class="captcha contact-captcha">
								{!! NoCaptcha::renderJs() !!}
								{!! NoCaptcha::display() !!}
								@if ($errors->has('g-recaptcha-response'))
								<span class="text-danger">{{ $errors->first('g-recaptcha-response') }}</span>
							@endif
					</div>
					<div style="text-align: center;">
						<button class="submit" type="submit">SUBMIT</button>
					</div>
					</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--BOOKING-SECTION-END-->


	<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.min.js"></script>

	<script>
		// just for the demos, avoids form submit
		jQuery.validator.setDefaults({ 
		//   debug: true,
		  success: "valid"
		});
		$( "#comment" ).validate({
		  rules: {
			email: {
			  required: true,
			  email: true
			},
			mobile_number: {
			  required: true,
			  number: true,
			  minlength: 11,
			  maxlength:11,
			  
			},
			first_name:{
				required: true
			},
			last_name:{
				required: true
			},
			venue_name:{
				required: true
			},
			date:{
				required: true
			},
			time:{
				required: true
			},
			sport:{
				required: true
			},
			comments:{
				required: true
			}
		  },
		  messages: {
				email: "Enter Your Email",
				mobile_number: {
					required: "Enter Your Number"
				}
			}
		});
	</script>
		






	<script>
		$( ".getsearchchange" ).change(function() {
 
 window.location.href = $(this).val();
});
function initAutocomplete() {
      
  
      
	  
	  var input = document.getElementById('pac-input');
			//var searchBox = new google.maps.places.SearchBox(input);
			var options = {
			// types: ['(cities)'],
			componentRestrictions: {country: 'GB'}//UK only
			};
            var autocomplete = new google.maps.places.Autocomplete(input,options);
            
            var inputlocation = document.getElementById('pac-location');
			//var searchBox = new google.maps.places.SearchBox(input);
			var optionslocation = {
			// types: ['(cities)'],
			componentRestrictions: {country: 'GB'}//UK only
			};
			var autocompletelocation = new google.maps.places.Autocomplete(inputlocation,optionslocation);
  
            

      var infowindow = new google.maps.InfoWindow();
  
      var bounds = new google.maps.LatLngBounds();
  
      for (i = 0; i < locations.length; i++) {
          marker = new google.maps.Marker({
              position: new google.maps.LatLng(locations[i][1], locations[i][2]),
              map: map
          });
  
          bounds.extend(marker.position);
  
          google.maps.event.addListener(marker, 'click', (function (marker, i) {
              return function () {
                  infowindow.setContent(locations[i][0]);
                  infowindow.open(map, marker);
              }
          })(marker, i));
      }
  
      map.fitBounds(bounds);
  
      var listener = google.maps.event.addListener(map, "idle", function () {
          map.setZoom(3);
          google.maps.event.removeListener(listener);
      });
  }
		$(document).ready(function(){
				
				$('#venue').keyup(function(){ 
					   var query = $(this).val();
					   //console.log('query',query);
					   if(query != '')
					   {
						var _token = $('input[name="_token"]').val();
						$.ajax({
						 url:"{{ url('') }}/venue/autocomplete",
						 method:"POST",
						 data:{query:query, _token:_token},
						 success:function(data){
							 //console.log('data',data);
									$('#venueList').fadeIn();  
								   $('#venueList').html(data);
						 }
						});
					   }
				   });
			   
				   $(document).on('click', '.dropdown-menu li', function(){  
					   $('#venue').val($(this).text());
					  
					   var vid=$(this).attr('vid');
					 
					   $('#venue_id').val(vid);
					   $('#venueList').fadeOut();
					 
				   });  
			   
			   });
		</script>
@endsection