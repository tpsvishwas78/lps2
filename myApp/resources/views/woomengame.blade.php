<?php 
use App\Models\MstVenue;

?>

@extends('layouts.master')
@section('content')
<?php
@$user=Auth::user();
@$sportbanner=\DB::table('mst_sports')->where('id',@$_GET['sport'])->first();
if(@$sportbanner->image){
    $banner=url('').'/upload/images/'.@$sportbanner->image;
}else{
    $banner=url('').'/images/1567664854.jpg';
}
?>
<style>
.gm-style-iw {
width: 300px; 
min-height: 180px;

}
.gm-style-iw-d{
max-height: 180px !important;
overflow:unset !important;
}
</style>
<div class="clearfix"></div>
		<!--BANNER-SECTION-START-->
<section class="banner-detail" style="background: url({{ $banner }}); background-size: cover;">
        <h1 class="hedding-detail" >Womens Only</h1>
        </section>
        <!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
	<!--SEARCH-SECTION-START-->
	<section class="search">
		<nav class="navbar navbar-light bg-light justify-content-between">
			<div class="container">
				<div class="col-sm-12">
			<a class="navbar-brand">FIND YOUR SPORT</a>
			<form class="form-inline" action="{{ url('') }}/womengames">
                <input class="form-control mr-sm-2" type="text" name="search" placeholder="TOWN/CITY/POSTCODE" aria-label="Search" id="pac-input" value="{{ @$_GET['search'] }}">
                <input class="form-control mr-sm-2" type="hidden" name="near_latitude" id="near_latitude" value="{{ @$_GET['near_latitude'] }}">
				<input class="form-control mr-sm-2" type="hidden" name="near_longitude" id="near_longitude" value="{{ @$_GET['near_longitude'] }}">
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit"><img src="{{ url('') }}/images/icons/search.png">Search Games</button>
			</form>
		</div>
		</nav>
	</section>
	<!--SEARCH-SECTION-END-->
    <div class="clearfix"></div>
    
    <!--SEARCH-RESULT-SECTION-START-->
	<section class="serach-result">
            <div class="section-headding">
                @if ( @$_GET['search'])
                <h1>Search Result Of <span>{{ @$_GET['search'] }}</span></h1>
                @endif
                
            </div>

    <div class="container">
        <div class="row">
            <div class="map-sports">
                    <div id="map"></div>
            </div>

            <div class="date-and-filter">
                    <div class="select-date datefilterwomen">
                        <div class="custom-date-design">
                            <img src="{{url('')}}/images/icons/cal.png">
                            <input type="text" name="date" id="date" class="datebox" placeholder="Select dates" date="{{ @$_GET['date'] }}" value="{{ @$_GET['date'] }}" autocomplete="off">
                            <img src="{{url('')}}/images/icons/drop-down-icon.png">
                        </div>
                    </div>
                    <div class="filter_vailue">
                        <select name="sports" id="sports" class="selectfilter sportfilter">
                            <option value="">Filter Sport</option>
                            @foreach ($sports as $sport)
                                <option @if (@$_GET['sport']==$sport->id)
                                    selected
                                @endif value="{{ $sport->id }}">{{ $sport->name }}</option>
                            @endforeach
                        </select>
                        
                    </div>
                </div>
                <div class="map-toggle-button">
                    <p>SHOW MAP</p>
                    <label class="switch">
                      <input class="switch-button" type="checkbox">
                      <span class="slider-button round"></span>
                    </label>
                </div>

                <div class="clearfix"></div>
					  <div class="listing-games" id="load-data">
					
						 
						@if (isset($games[0]->id))
                        @foreach ($games as $game)
                       <?php
                       if($game->game_start_date){
                        @$day=date('d',strtotime($game->game_start_date));
                        @$month=date('M',strtotime($game->game_start_date));
                       }else{
                        @$day=date('d',strtotime($game->game_date));
                         @$month=date('M',strtotime($game->game_date));
                       }
                    
                    @$location=\DB::table('mst_venue')->where('id',$game->venue_id)->first();
                    @$spots=\DB::table('team_spots')->where('game_id',$game->id)->where('status',1)->count();

						@$followes=\DB::table('venue_followers')->where('venue_id',$game->venue_id)->count();
						@$mefollow=\DB::table('venue_followers')->where('user_id',$user->id)->where('venue_id',$game->venue_id)->count();
                       ?>
						<div class="design-list">
							
							<div class="col-sm-2 col-xs-3 p-0">
								<div class="date-game">
									<h2>{{$day}}<span>{{$month}}</span></h2>
								</div>
							</div>
							<div class="col-sm-7 col-xs-9">
								<div class="info-game">
										<a href="{{ url('') }}/games/{{ $game->slug }}/{{ $game->rid }}">
									<p><img src="{{url('')}}/images/icons/tenis-ball.png">{{ $game->title }}</p>
								</a>
									<ul class="time-and-place">
										<li><img src="{{url('')}}/images/icons/watch-icon.png">{{$game->game_start_time}} - {{$game->game_finish_time}}</li>
                                    <li title="{{ $location->name }}"><img src="{{url('')}}/images/icons/location-icon.png">{!! str_limit($location->name, $limit = 50, $end = '') !!}</li>
									</ul>
									<div class="follow-venue">
										<ul>
											 <li class="price-book">
                                                 @if ($game->payment==1 || $game->payment==2)
												 @if ($game->currency=='USD')
												 ${{ $game->price }} 
											 @else
											 £{{ $game->price }} 
											 @endif
                                                 @else
                                                     Free
                                                 @endif
                                                 &nbsp;&nbsp;
                                                </li>
                                                

                                             {{-- <li class="number-player">{{ $spots }}/{{ $game->team_limit+$game->team_limit }}</li> --}}
                                             


											 <li class="follow-button">
                                        @csrf
                                        <input type="hidden" value="{{ @$user->id }}" id="userid">
                                        <?php
										
										 $venue = MstVenue::where('id',$game->venue_id )->first();
										?>
                                        @if ($mefollow > 0)
                                         <a href="{{ url('') }}/venue/{{ $venue->slug }}">
											<button class="follow setfollow">
													View Sessions
											</button>
										</a>
										<!-- <button id="follow{{ $game->venue_id }}" follow="Following" vid="{{ $game->venue_id }}" class="follow setfollow">Following Venue</button> -->
                                        @else
                                         <a href="{{ url('') }}/venue/{{ $venue->slug }}">
											<button class="follow setfollow">
													View Sessions
											</button>
										</a>
										<!-- <button id="follow{{ $game->venue_id }}" follow="Follow" vid="{{ $game->venue_id }}" class="follow setfollow">Follow Venue</button> -->
										@endif
										
										@if ($followes>0)
                                        <!-- <span><span id="followcount{{ $game->venue_id }}">{{ $followes }}</span> followers</span> -->
                                        @endif
										
											 </li>
											 
										</ul>
									</div>
									<div class="dis-game">{!! str_limit($game->description, $limit = 45, $end = '') !!} </div>
								</div>
							</div>
							<div class="col-sm-3 col-xs-12 p-0">
							<div class="game-img"><a href="{{ url('') }}/games/{{ $game->slug }}/{{ $game->rid }}">
                                @if ($game->featured_img)
                                    <img src="{{url('')}}/upload/images/{{$game->featured_img}}">
                                @else
                                    <img src="{{url('')}}/images/no-user-Image.png">
                                @endif
								</a>
							</div>
							</div>
						
                        </div>
                        <div class="clearfix"></div>
						@endforeach
						@else
						<div class="alert alert-warning m-sm">
							We couldn't find any games based on your requirements.
							You can try the following options:
							<ul>
							
							<li>
							<div class="text-black">Change the search terms</div>
							</li>
							</ul>
							</div>
						@endif
                    @if ($gamescount>10)
                     <div class="load-more-button" id="remove-row">
                            <button id="btn-more" data-id="2" class="load-more">LOAD MORE</button>
                     </div>
                    @endif
                    
						
					
                    </div>
                    
                    
                        

        </div>
    </div>
            
    </section>
    @if (Route::has('login'))
    @auth


    @else
    
    <section class="form-contact">
            <div class="container">
                <div class="row">
                        @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                            @php
                                Session::forget('success');
                            @endphp
                            </div>
                            @endif
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="heading-contact-form">
                            <h1>Register your interest form</h1>
                            <p>Please fill out the below form - we aim to respond within 24 hours </p>
                        </div>
                        <div class="contact-us-form">
                            <form class="comment" method="POST" action="{{ url('') }}/submitIntrest">
                                @csrf
                            <div class="col-sm-6 p-0">
                                <div class="form-group">
                                    <label class="control-label">First Name</label>
                                    <input type="text" name="first_name" class="form-control" placeholder="Enter Your First  Name">
                                    @if ($errors->has('first_name'))
                            <span class="text-danger">{{ $errors->first('first_name') }}</span>
                        @endif
                                    </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Last Name</label>
                                    <input type="text" name="last_name" class="form-control" placeholder="Enter Your Last Name">
                                    @if ($errors->has('last_name'))
                            <span class="text-danger">{{ $errors->first('last_name') }}</span>
                        @endif
                                    </div>
                            </div>
                            <div class="col-sm-6 p-0">
                                <div class="form-group">
                                    <label class="control-label">Your Email</label>
                                    <input type="text" name="email" class="form-control" placeholder="Enter Your Email">
                                    @if ($errors->has('email'))
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        @endif
                                    </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Mobile Number</label>
                                    <input type="text" name="mobile_number" class="form-control" placeholder="Enter Your Mobile Number">
                                    @if ($errors->has('mobile_number'))
                            <span class="text-danger">{{ $errors->first('mobile_number') }}</span>
                        @endif
                                    </div>
                            </div>
                            <div class="col-sm-12 pl-0">
                                    <div class="form-group">
                                        <label class="control-label">Location</label>
                                        <input type="text" name="location" id="pac-location" class="form-control" placeholder="Enter Your Location">
                                        @if ($errors->has('location'))
                                <span class="text-danger">{{ $errors->first('location') }}</span>
                            @endif
                                        </div>
                                </div>
                            <div class="col-sm-12 pl-0">
                                <div class="form-group">
                                    <label class="control-label">	Your Message</label>
                                    <textarea name="message" class="form-control textarea" placeholder="Enter Your Message"></textarea>
                                    @if ($errors->has('message'))
                            <span class="text-danger">{{ $errors->first('message') }}</span>
                        @endif
                                    </div>
                            </div>
                            <div class="captcha contact-captcha">
                            {!! NoCaptcha::renderJs() !!}
                            {!! NoCaptcha::display() !!}
                            @if ($errors->has('g-recaptcha-response'))
                            <span class="text-danger">{{ $errors->first('g-recaptcha-response') }}</span>
                        @endif
                        </div>
                        <div style="text-align: center;">
                            <button class="submit">SUBMIT</button>
                        </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @endauth
    @endif
  <!--CONTACT-US-SECTION-END-->
  <script>
        $(document).ready(function(){
           $(document).on('click','#btn-more',function(){
               var id = $(this).data('id');
               //alert(id);
               $("#btn-more").html("Loading....");
               $.ajax({
                   url : '{{ url("/woomenloadmore") }}',
                   method : "POST",
                   data : {page:id, _token:"{{csrf_token()}}",search:"{{ @$_GET['search'] }}" , date:"{{ @$_GET['date'] }}" ,sport:"{{ @$_GET['sport'] }}",near_latitude:"{{ @$_GET['near_latitude'] }}",near_longitude:"{{ @$_GET['near_longitude'] }}" },
                   dataType : "text",
                   success : function (data)
                   {
                       //console.log('data',data);
                      if(data != '') 
                      {
                          $('#remove-row').remove();
                          //$("#btn-more").html("Load More");
                          $('#load-data').append(data);
                          var page=parseFloat(id)+parseFloat(1);
                            $('#btn-more').data('id',page);
                      }
                      else
                      {
                          //$('#btn-more').html("No Data");
                          $('#remove-row').remove();
                      }
                      
                      
                   }
               });
           });  
        }); 
        </script> 

    <script>
  
            $(".sportfilter").change(function(){
                var sport=$(this).val();
                var date=$('#date').attr('date');
                var search=$('#pac-input').val();
                var near_latitude=$('#near_latitude').val();
                var near_longitude=$('#near_longitude').val();
                if(date){
                    date=date;
                }else{
                    date='';
                }
                window.history.pushState({}, '', 'womengames?search='+search+'&date='+date+'&sport='+sport+'&near_latitude='+near_latitude+'&near_longitude='+near_longitude);
                location.reload();
            });
        
                    $(".setfollow").click(function(){
                        
                        var userid=$('#userid').val();
                        if(userid){
                            var follow=$(this).attr('follow');
                        var vid=$(this).attr('vid');
                    
                        var _token = $('input[name="_token"]').val();
                                     $.ajax({
                                      url:"{{ url('') }}/venue/follow",
                                      method:"POST",
                                      dataType:'Json',
                                      data:{follow:follow,id:vid, _token:_token},
                                      success:function(data){
                                          //console.log(data);
                                          $('#follow'+vid).html(data.follow + ' Venue');
                                          $('#follow'+vid).attr('follow',data.follow);
                                          $('#followcount'+vid).html(data.followes);
                                                
                                      }
                                     });
                        }else{
                            alert('Need to login first!');
                        }
                        
                      
                    });
                    </script>

    <script>
            function initAutocomplete() {
                
      var locations = [
          @foreach ($allgames as $key => $allgame)
          <?php 
          
          $location=\DB::table('mst_venue')->where('id',$allgame->venue_id)->first();
          $vurl=url('').'/venue/'.$location->slug;
          $gurl=url('').'/games/'.$allgame->slug.'/'.$allgame->rid;
           $geSession = MstVenue::getSession($location->id);

          $html='<p class="popvenuename"><a target="_blank" href="'.$vurl.'">'.$location->name.'</a></p>';
          $html .='<p class="popgamename"><a target="_blank" href="'.$vurl.'">'.$location->postcode.'</a></p>';
           foreach($geSession as $ses){
            $html .='<p class="popgamename"><a target="_blank" href="'.$vurl.'">'.$ses->game_start_time .' '.date('D d M', strtotime($ses->game_start_date)).' '.$ses->title.'</a></p>';
          }
          $html .='<p  class="popseemore"><a target="_blank" href="'.$vurl.'">See all upcoming games</a></p>';
          ?>
          
            [`<?php echo $html; ?>`, {{ $location->latitude }}, {{$location->longitude}}, {{ $key }}],
            @endforeach
          
      ];  
  
      window.map = new google.maps.Map(document.getElementById('map'), {
          mapTypeId: google.maps.MapTypeId.ROADMAP
	  });
	  
	  var input = document.getElementById('pac-input');
			//var searchBox = new google.maps.places.SearchBox(input);
			var options = {
			// types: ['(cities)'],
			componentRestrictions: {country: 'GB'}//UK only
			};
            var places = new google.maps.places.Autocomplete(input,options);
            google.maps.event.addListener(places, 'place_changed', function () {
                var lat = places.getPlace().geometry.location.lat();
                var lng = places.getPlace().geometry.location.lng();
                    $('#near_latitude').val(lat);
                    $('#near_longitude').val(lng);
                   
    
            });
            
            var inputlocation = document.getElementById('pac-location');
			//var searchBox = new google.maps.places.SearchBox(input);
			var optionslocation = {
			// types: ['(cities)'],
			componentRestrictions: {country: 'GB'}//UK only
			};
			var autocompletelocation = new google.maps.places.Autocomplete(inputlocation,optionslocation);
  
      var infowindow = new google.maps.InfoWindow();
  
      var bounds = new google.maps.LatLngBounds();
  
      for (i = 0; i < locations.length; i++) {
          marker = new google.maps.Marker({
              position: new google.maps.LatLng(locations[i][1], locations[i][2]),
              map: map
          });
  
          bounds.extend(marker.position);
  
          google.maps.event.addListener(marker, 'click', (function (marker, i) {
              return function () {
                  infowindow.setContent(locations[i][0]);
                  infowindow.open(map, marker);
              }
          })(marker, i));
      }
  
      map.fitBounds(bounds);
  
      var listener = google.maps.event.addListener(map, "idle", function () {
          map.setZoom(12);
          google.maps.event.removeListener(listener);
      });
  }
  
        </script>
@endsection