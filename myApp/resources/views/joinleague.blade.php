@extends('layouts.master')
@section('content')
<?php
@$user=Auth::user();
if(@$joinleague->banner){
    $banner=url('').'/upload/images/'.@$joinleague->banner;
}else{
    $banner=url('').'/images/detail-page-baner.jpg';
}
?>
<div class="clearfix"></div>
	<!--BANNER-SECTION-START-->
<section class="banner-detail" style="background: url({{ $banner }}); background-size: cover;">
        <h1 class="hedding-detail" >{{ @$joinleague->banner_title }}</h1>
        </section>
        <!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
	<!--SEARCH-SECTION-START-->
	<section class="search">
		<nav class="navbar navbar-light bg-light justify-content-between">
			<div class="container">
				<div class="col-sm-12">
			<a class="navbar-brand">FIND YOUR SPORT</a>
			<form class="form-inline" action="{{ url('') }}/joinleague">
				<input class="form-control mr-sm-2" type="text" name="search" placeholder="TOWN/CITY/POSTCODE" aria-label="Search" id="pac-input" value="{{ @$_GET['search'] }}">
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit"><img src="{{ url('') }}/images/icons/search.png">Search Games</button>
			</form>
		</div>
		</nav>
	</section>
	<!--SEARCH-SECTION-END-->
    <div class="clearfix"></div>
    
    <!--SEARCH-RESULT-SECTION-START-->
	<section class="serach-result">
            {{-- <div class="section-headding">
                @if ( @$_GET['search'])
                <h1>Search Result Of <span>{{ @$_GET['search'] }}</span></h1>
                @endif
                
            </div> --}}

    <div class="container">
        <div class="row">
            <div class="map-sports" style="display: block;">
              <div class="col-sm-12">
                    <div id="map"></div>
                  </div>
            </div>



            {{-- <div class="date-and-filter">
                    <div class="select-date datefilter">
                        <div class="custom-date-design">
                            <img src="{{url('')}}/images/icons/cal.png">
                            <input type="text" name="date" id="date" class="datebox" placeholder="Select dates" date="{{ @$_GET['date'] }}" value="{{ @$_GET['date'] }}" autocomplete="off">
                            <img src="{{url('')}}/images/icons/drop-down-icon.png">
                        </div>
                    </div>
                    <div class="filter_vailue">
                        <select name="sports" id="sports" class="selectfilter sportfilter">
                            <option value="">Filter Sport</option>
                            @foreach ($sports as $sport)
                                <option @if (@$_GET['sport']==$sport->id)
                                    selected
                                @endif value="{{ $sport->id }}">{{ $sport->name }}</option>
                            @endforeach
                        </select>
                        
                    </div>
                </div> --}}
                <div class="map-toggle-button maptoggle">
                    <p>SHOW MAP</p>
                    <label class="switch">
                      <input class="switch-button" checked type="checkbox">
                      <span class="slider-button round"></span>
                    </label>
                </div>
<!--DESCRIPTION-SECTION-START-->
<section class="description descriptiontitle">
    <div class="container">
        <div class="row">
                <div class="col-sm-12">
                    <div class="description-left">
                         <h2>{{ @$joinleague->title }}</h2>
                <p></p>
                        <h3>Description</h3>
                        {!! @$joinleague->description !!}
                        <div class="invite-friend">
                            <button class="your-friend btn btn-primary" data-toggle="modal" data-target="#modal-invite-friend">+ Invite Your Friend</button>
                            <div class="detail-share-buttons">
                                <ul>
                                    <li>
                                            <a target="_blank" href="https://facebook.com/sharer/sharer.php?u={{ url('') }}/joinleague"><img src="{{ url('') }}/images/icons/facebook.png"></a>
                                        
                                    </li>
                                    <li>
                                        
                                        <a target="_blank" href="https://twitter.com/share?url={{ url('') }}/joinleague&amp;text={{ @$joinleague->title }}"><img src="{{ url('') }}/images/icons/Twitter.png"></a>
                                    </li>
                                    <li>
                                            <a target="_self" href="whatsapp://send?text={{ url('') }}/joinleague"><img src="{{ url('') }}/images/icons/whatsapp.png"></a>
                                        
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" onclick="setClipboard('{{ url('') }}/joinleague');">
                                        <img src="{{ url('') }}/images/icons/link.png">
                                    </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    
                    </div>
                </div>
                
                    </div>
                </div>
        
</section>
<!-- The modal -->
<div class="modal fade" id="modal-invite-friend" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog invite-friend-model" role="document">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title" id="modalLabel">Invite Your Friend</h4>
    <p>Invite your friend and get 1 session free at selected locations. </p>
    </div>
    
    <div class="modal-body">
    <form class="form-invite" action="{{ url('') }}/sendinvitation" method="POST">
        @csrf
        <input type="email" required name="email" placeholder="Add your friend email address">
        <input type="hidden" name="gameurl" value="{{ url()->current() }}">
        <input type="hidden" name="gametitle" value="JOIN A LEAGUE">
        <input type="hidden" name="gamevenue" value="JOIN A LEAGUE">
        <button type="submit" class="invitefriend">INVITE FRIEND</button>
    </form>
    </div>

    </div>
    </div>
    </div>
<!--DESCRIPTION-SECTION-END-->
                <div class="clearfix"></div>
					  <div class="listing-games"  id="load-data">
					
						 <div class="col-sm-12"> 
						@if (isset($games[0]->id))
                        @foreach ($games as $game)
                       <?php
                            @$day=date('d',strtotime($game->game_date));
                            @$month=date('M',strtotime($game->game_date));
							@$location=\DB::table('mst_venue')->where('id',$game->venue_id)->first();
							@$spots=\DB::table('team_spots')->where('game_id',$game->id)->where('status',1)->count();

						@$followes=\DB::table('venue_followers')->where('venue_id',$game->venue_id)->count();
						@$mefollow=\DB::table('venue_followers')->where('user_id',$user->id)->where('venue_id',$game->venue_id)->count();
                       ?>
						<div class="design-list">
							
							<div class="col-sm-2 col-xs-3 p-0">
								<div class="date-game">
									<h2>{{$day}}<span>{{$month}}</span></h2>
								</div>
							</div>
							<div class="col-sm-7 col-xs-9">
								<div class="info-game">
										<a href="{{ url('') }}/games/{{ $game->slug }}">
									<p><img src="{{url('')}}/images/icons/tenis-ball.png">{{ str_limit($game->title, $limit = 20, $end = '...')}}</p>
								</a>
									<ul class="time-and-place">
										<li><img src="{{url('')}}/images/icons/watch-icon.png">{{$game->game_start_time}} - {{$game->game_finish_time}}</li>
                                    <li title="{{ $location->name }}"><img src="{{url('')}}/images/icons/location-icon.png">{!! str_limit($location->name, $limit = 20, $end = '...') !!}</li>
									</ul>
									<div class="follow-venue">
										<ul>
											 <li class="price-book">
                                                 @if ($game->payment==1)
												 @if ($game->currency=='USD')
												 ${{ $game->price }} 
											 @else
											 £{{ $game->price }} 
											 @endif
                                                 @else
                                                     Free
                                                 @endif
                                                 &nbsp;&nbsp;
                                                </li>
											 {{-- <li class="number-player">{{ $spots }}/{{ $game->team_limit+$game->team_limit }}</li> --}}

											 <li class="follow-button">
                                        @csrf
                                        <input type="hidden" value="{{ @$user->id }}" id="userid">
										@if ($mefollow > 0)
										<button id="follow{{ $game->venue_id }}" follow="Following" vid="{{ $game->venue_id }}" class="follow setfollow">Following Venue</button>
										@else
										<button id="follow{{ $game->venue_id }}" follow="Follow" vid="{{ $game->venue_id }}" class="follow setfollow">Follow Venue</button>
										@endif
										
										
										@if ($followes>0)
                                        <span><span id="followcount{{ $game->venue_id }}">{{ $followes }}</span> followers</span>
                                        @endif
											 </li>
											 
										</ul>
									</div>
									<div class="dis-game">{!! str_limit($game->description, $limit = 45, $end = '...') !!} </div>
								</div>
							</div>
							<div class="col-sm-3 col-xs-12 p-0">
							<div class="game-img">
                                @if ($game->featured_img)
                                    <img src="{{url('')}}/upload/images/{{$game->featured_img}}">
                                @else
                                    <img src="{{url('')}}/images/no-user-Image.png">
                                @endif
								
							</div>
							</div>
						
                        </div>
						@endforeach
						@else
						<div class="alert alert-warning m-sm">
							We couldn't find any games based on your requirements.
							You can try the following options:
							<ul>
							
							<li>
							<div class="text-black">Change the search terms</div>
							</li>
							</ul>
							</div>
                        @endif
                        
                        @if ($gamescount>10)
                        <div class="load-more-button" id="remove-row">
                                <button id="btn-more" data-id="{{ @$game->id }}" class="load-more">LOAD MORE</button>
                            </div>
                        @endif
                        
						
					
					</div>

        </div>
    </div>
            </div>
    </section>
    <div class="clearfix"></div>
    <section class="amazing-vanue">
      <div class="container">
        <div class="row">

            @if (@$allblocks)
            @foreach ($allblocks as $allblock)
            <div class="col-sm-6">
                    <div class="amazing-content-section">
                      <h2 class="amazing-heading">{{ $allblock->title }}</h2>
                      <div class="img-amazing">
                        <img src="{{ url('') }}/upload/images/{{ $allblock->image }}">
                      </div>
                      <p>{{ $allblock->description }}</p>
                    </div>
                  </div>
            @endforeach
            @endif
            
          </div>
        </div>
    </section>

  <script>
        $(document).ready(function(){
           $(document).on('click','#btn-more',function(){
               var id = $(this).data('id');
               $("#btn-more").html("Loading....");
               $.ajax({
                   url : '{{ url("/searchgameloadmore") }}',
                   method : "POST",
                   data : {id:id, _token:"{{csrf_token()}}",search:"{{ @$_GET['search'] }}" , date:"{{ @$_GET['date'] }}" ,sport:"{{ @$_GET['sport'] }}" },
                   dataType : "text",
                   success : function (data)
                   {
                       //console.log('data',data);
                      if(data != '') 
                      {
                          $('#remove-row').remove();
                          //$("#btn-more").html("Load More");
                          $('#load-data').append(data);
                      }
                      else
                      {
                          //$('#btn-more').html("No Data");
                          $('#remove-row').remove();
                      }
                   }
               });
           });  
        }); 
        </script> 

    <script>
  function setClipboard(value) {
    var tempInput = document.createElement("input");
    tempInput.style = "position: absolute; left: -1000px; top: -1000px";
    tempInput.value = value;
    document.body.appendChild(tempInput);
    tempInput.select();
    document.execCommand("copy");
    document.body.removeChild(tempInput);
}
            $(".sportfilter").change(function(){
                var sport=$(this).val();
                var date=$('#date').attr('date');
                var search=$('#pac-input').val();
                if(date){
                    date=date;
                }else{
                    date='';
                }
                window.history.pushState({}, '', 'searchgame?search='+search+'&date='+date+'&sport='+sport);
                location.reload();
            });
        
                    $(".setfollow").click(function(){
                        
                        var userid=$('#userid').val();
                        if(userid){
                            var follow=$(this).attr('follow');
                        var vid=$(this).attr('vid');
                    
                        var _token = $('input[name="_token"]').val();
                                     $.ajax({
                                      url:"{{ url('') }}/venue/follow",
                                      method:"POST",
                                      dataType:'Json',
                                      data:{follow:follow,id:vid, _token:_token},
                                      success:function(data){
                                          //console.log(data);
                                          $('#follow'+vid).html(data.follow + ' Venue');
                                          $('#follow'+vid).attr('follow',data.follow);
                                          $('#followcount'+vid).html(data.followes);
                                                
                                      }
                                     });
                        }else{
                            alert('Need to login first!');
                        }
                        
                      
                    });
                    </script>

    <script>
            function initAutocomplete() {
      var locations = [
          @foreach ($games as $game)
          <?php  
          $location=\DB::table('mst_venue')->where('id',$game->venue_id)->first();
          ?>
            ['{{$location->name}}', {{$location->latitude}}, {{$location->longitude}}, 1],
            @endforeach
          
      ];
  
      window.map = new google.maps.Map(document.getElementById('map'), {
          mapTypeId: google.maps.MapTypeId.ROADMAP
	  });
	  
	  var input = document.getElementById('pac-input');
			//var searchBox = new google.maps.places.SearchBox(input);
			var options = {
			types: ['(cities)'],
			componentRestrictions: {country: 'GB'}//UK only
			};
            var autocomplete = new google.maps.places.Autocomplete(input,options);
            
            var inputlocation = document.getElementById('pac-location');
			//var searchBox = new google.maps.places.SearchBox(input);
			var optionslocation = {
			types: ['(cities)'],
			componentRestrictions: {country: 'GB'}//UK only
			};
			var autocompletelocation = new google.maps.places.Autocomplete(inputlocation,optionslocation);
  
            

      var infowindow = new google.maps.InfoWindow();
  
      var bounds = new google.maps.LatLngBounds();
  
      for (i = 0; i < locations.length; i++) {
          marker = new google.maps.Marker({
              position: new google.maps.LatLng(locations[i][1], locations[i][2]),
              map: map
          });
  
          bounds.extend(marker.position);
  
          google.maps.event.addListener(marker, 'click', (function (marker, i) {
              return function () {
                  infowindow.setContent(locations[i][0]);
                  infowindow.open(map, marker);
              }
          })(marker, i));
      }
  
      map.fitBounds(bounds);
  
      var listener = google.maps.event.addListener(map, "idle", function () {
          map.setZoom(3);
          google.maps.event.removeListener(listener);
      });
  }
  
        </script>
@endsection