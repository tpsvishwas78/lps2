@extends('layouts.usermaster')

@section('content')
<?php
$user=Auth::user();
?>
<div class="clearfix"></div>
	<!--BANNER-SECTION-START-->
	<section class="banner-detail">
       <h1 class="hedding-detail">Profile</h1>
    </section>
	<!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
	<!--account-setting-section-start-->
	<section class="account-setting-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 p-0">
                        @include('layouts.sidebar')
					<div class="col-sm-8">
					  <div class="content-profile">
					  	<div class="col-sm-4">
					  		<div class="edit-profile-img-profilepage">
                                    @if ($user->profile_img && $user->profile_img!='null')
                                    <img src="{{ url('') }}/upload/images/{{$user->profile_img}}" class="w-50 rounded-circle" alt="{{$user->first_name}}">
                                    @else
                                    <img src="{{ url('') }}/images/avtar.png" class="w-50 rounded-circle" alt="{{$user->first_name}}">
                                    @endif
					  		</div>
					  		<a href="{{ url('') }}/profile/edit" class="edit-your-profile"><img src="{{ url('') }}/images/icons/edit.png">EDIT PROFILE</a>
					  	</div>
					  	<div class="col-sm-8">
					  		<div class="contact-information">
					  			<h2 class="name">{{$user->first_name}} {{$user->last_name}}</h2>
					  			<p class="email">@ {{$user->username}}</p>
					  			<div class="information-profile">
					  				<h2>Contact Information</h2>
					  				<div class="border-right"></div>
					  				<ul>
					  					<li><strong>Email Address :</strong><span>{{$user->email}}</span></li>
					  					<li><strong>Mobile Number :</strong><span>{{$user->phone_number}}</span></li>
					  					<li><strong>Address:</strong><span>@if ($user->address!='null')
												{{ $user->address }}											  
										  @endif</span></li>
					  				</ul>
					  			</div>
					  			<div class="information-profile">
					  				<h2>Basic Information</h2>
					  				<div class="border-right"></div>
					  				<ul>
					  					<li><strong>Birthday :</strong><span>
											  @if ($user->dob)
											  {{date('M d Y', strtotime($user->dob))}}
											  @endif
											</span></li>
					  					<li><strong>Gender :</strong><span>
											@if ($user->gender)
												@if ($user->gender==1)
													Male
												@else
													Female
												@endif
											@endif
											  
											</span></li>
					  					<li><strong>Nationality:</strong><span>{{@$nationality->name}}</span></li>
					  				</ul>
					  			</div>
					  			<div class="information-profile">
					  				<h2>Area Of Interest</h2>
					  				<div class="border-right"></div>
					  				<ul>
					  					<li><span>
										@foreach ($areas as $item)
											{{$item->name}},
										@endforeach	  
										</span></li>
					  				</ul>
					  			</div>
					  		</div>
					  	</div>
					  </div>
				</div>
			</div>
		</div>
	</section>
	<!--account-setting-section-end-->
@endsection
