<?php
@$setting=\DB::table('site_setting')->first();
if(@$setting->website_title){
  $title=@$setting->website_title;
}else{
  $title='Lets play sports - play football, netball and sports near you';
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>{{ $title }}</title>
	<!-- META TAGS -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- FAV ICON(BROWSER TAB ICON) -->
	
	<!-- GOOGLE FONT -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
	<!-- FONTAWESOME ICONS -->
	<link rel="stylesheet" href="{{ url('') }}/css/font-awesome.min.css">
    <!-- ALL CSS FILES -->
    <link href="{{ url('') }}/css/semantic.min.css" rel="stylesheet">
    <link href="{{ url('') }}/css/bootstrap.min.css" rel="stylesheet">
    
	<!-- RESPONSIVE.CSS ONLY FOR MOBILE AND TABLET VIEWS -->
	<link href="{{ url('') }}/css/style.css" rel="stylesheet">
	<link href="{{ url('') }}/css/responsive.css" rel="stylesheet">
	<script src="{{ url('') }}/js/jquery.min.js"></script>
    

</head>

<body>
        @include('layouts.userheader')
        @yield('content')
        @include('layouts.footer')
        

    <!--SCRIPT FILES-->
	{{-- <script src="{{ url('') }}/js/jquery.min.js"></script> --}}
	<script src="{{ url('') }}/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="{{ url('') }}/js/custom.js"></script>
    <script src="{{ url('') }}/js/semantic.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCinqHaojRbM2o13DIpNjZV5OIovI5kyrs&libraries=places&callback=initAutocomplete"
    async defer></script>
    <script>
        var dateToday = new Date();
        $('.datepicker').calendar({
          type: 'date',
          minDate: dateToday,
        });
        $('.profiledatepicker').calendar({
          type: 'date',
        });
		$('.timepicker').calendar({
          type: 'time'
        });
            $('.rangestart').calendar({
            type: 'time',
            endCalendar: $('.rangeend')
            });
            $('.rangeend').calendar({
            type: 'time',
            startCalendar: $('.rangestart')
            });
        $('.datefilter').calendar({
          type: 'date',
          minDate: dateToday,
          onChange: function (date, text) {
            var year = date.getFullYear();
            var month = date.getMonth() + 1;
            var day = date.getDate();
            if (month < 10) {
                month = '0' + month;
            }
            if (day < 10) {
                day = '0' + day;
            }

            // everything combined
           var mydate = year + '-' + month + '-' + day;
           $('#date').attr('date',mydate);
           
           var sport=$('#sports').val();
           var gender=$('#gender').val();
           var search=$('#pac-input').val();
           var near_latitude=$('#near_latitude').val();
                var near_longitude=$('#near_longitude').val();
           window.history.pushState({}, '', 'games?search='+search+'&date='+mydate+'&sport='+sport+'&gender='+gender+'&near_latitude='+near_latitude+'&near_longitude='+near_longitude);
           location.reload();
           
  },
        });
    </script>

<script>
//         function initAutocomplete() {
  
//   var input = document.getElementById('header-search-input');
//       var searchBox = new google.maps.places.SearchBox(input);
 
// }

    </script>

<script>
    $('.readnotify').click(function(){ 
                 var _token = $('input[name="_token"]').val();
                 $.ajax({
                  url:"{{ url('') }}/notifyread",
                  method:"POST",
                  data:{_token:_token},
                  success:function(data){
                      
                  }
                 });
            });
</script>

</body>
</html>