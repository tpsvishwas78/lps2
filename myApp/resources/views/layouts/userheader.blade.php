<?php

use App\Models\MstSport;

$user = Auth::user();
$notifications = $user->notifications->take(5);
@$setting = \DB::table('site_setting')->first();
$sports = MstSport::where('status', 1)->orderBy('sport_order', 'ASC')->get();

?>
<style>
	.dropdown-item {
		display: block;
		font-size: 1.4rem;
		text-decoration: none;
		color: #fff;
		padding: 3px 0px 5px 10px;
</style>
<!--HEADER-dashboard-SECTION-START-->
<section class="dashboard-header">
	<nav class="navbar navbar-inverse navbar-static-top">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar3">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a href="{{ url('') }}" class="navbar-brand userlogo">
				<img src="{{ url('') }}/upload/images/{{ @$setting->header_logo }}">
			</a>

			<nav class="search-bar-dashboard">
				<nav class="navbar navbar-default">

					<div class="collapse navbar-collapse" id="example-1">


						<ul class="navbar-nav menu-header  mt-2 mt-lg-0">
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="{{ url('') }}/searchgame" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Choose your sport
								</a>
								<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
									@foreach ($sports as $sport)
									<a class="dropdown-item" href="{{ url('') }}/searchgame?search=&date=&gender=&sport={{ $sport->id  }}">
										{{ $sport->name }}
									</a>
									@endforeach
								</div>
							</li>
						</ul>
					</div>
				</nav>
		</div>
		<!-- <div class="search-bar-dashboard"> -->

		<!-- <form action="{{ url('') }}/searchgame">
			<input type="text" name="search" id="header-search-input" placeholder="Search Your Games"> -->
		<!--<button style="    background: #000;
			border-color: #000;" type="submit"><img src="{{ url('') }}/images/icons/header-search-icon.png"></button>-->
		<!-- </form> -->
		<!-- </div> -->

		</div>
		<div id="navbar3" class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-right dash-nav-bar">
				<li class="active"><a href="{{url('dashboard')}}">Home</a></li>
				<li><a href="{{ url('') }}/advertise-your-session">Advertise Your Session</a></li>
				<li><a href="{{url('')}}/allgames">All Games</a></li>
				<li><a href="{{url('')}}/venues">Venues</a></li>
				<li><a href="#">Teams</a></li>
				<li><a href="{{ url('') }}/payment">Payments</a></li>
				<li class="icon-img">
					@csrf
					<a href="#" class="dropdown-toggle readnotify" data-toggle="dropdown" role="button" aria-expanded="false">
						<img src="{{ url('') }}/images/icons/dash-head-icon1.png">
						@if ($user->unreadNotifications->count() > 0)
						<span class="notification-blink">{{ $user->unreadNotifications->count() }}</span>
						@endif

					</a>
					<ul class="dropdown-menu notifications notifyul" role="menu">
						@foreach ($notifications as $notification)
						<?php
						@$game = \DB::table('mst_games')->where('id', $notification->data['game_id'])->first();
						@$gameuser = \DB::table('users')->where('id', $notification->data['user_id'])->first();
						@$notifydate = Carbon\Carbon::parse($notification->created_at);
						@$repeat = \DB::table('repeat_games')->where('id', $notification->data['game_cid'])->first();
						$now = Carbon\Carbon::now();


						$diff = $notifydate->diffForHumans($now);
						?>
						<li>
							<div class="content-notifications">
								<div class="profile-pic">
									@if ($gameuser->profile_img && $gameuser->profile_img!='null')
									<img src="{{ url('') }}/upload/images/{{$gameuser->profile_img}}" class="w-50 rounded-circle" alt="{{$gameuser->first_name}}">
									@else
									<img src="{{ url('') }}/images/avtar.png" class="w-50 rounded-circle" alt="{{$gameuser->first_name}}">
									@endif
								</div>
								<p><strong>{{ @$gameuser->first_name }} {{ @$gameuser->last_name }}</strong> {{ $notification->data['message'] }} <span>{{ date('D, d M Y',strtotime(@$repeat->game_start_date)) }}</span> <a href="{{ url('') }}/games/{{ @$game->slug }}/{{ @$notification->data['game_cid'] }}">{{ @$game->title }}</a></p>
								<p>{{ $diff  }}</p>

							</div>
						</li>
						@endforeach
						<li class="see-all"><a href="#">See All Notification</a></li>
					</ul>
				</li>
				<li class="icon-img">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						<img src="{{ url('') }}/images/icons/dash-head-icon2.png">
					</a>
					<ul class="dropdown-menu notifications" role="menu">
						<li>
							<a href="#">
								<div class="content-notifications">
									<div class="profile-pic">
										<img src="{{ url('') }}/images/james.png">
									</div>
									<p><strong>John Doe </strong>New games on Let’s Play Sports in the Somerset area!</p>
								</div>
							</a>
						</li>
						<li>
							<a href="#">
								<div class="content-notifications">
									<div class="profile-pic">
										<img src="{{ url('') }}/images/james.png">
									</div>
									<p><strong>John Doe </strong>New games on Let’s Play Sports in the Somerset area!</p>
								</div>
							</a>
						</li>
						<li>
							<a href="#">
								<div class="content-notifications">
									<div class="profile-pic">
										<img src="{{ url('') }}/images/james.png">
									</div>
									<p><strong>John Doe </strong>New games on Let’s Play Sports in the Somerset area!</p>
								</div>
							</a>
						</li>
						<li>
							<a href="#">
								<div class="content-notifications">
									<div class="profile-pic">
										<img src="{{ url('') }}/images/james.png">
									</div>
									<p><strong>John Doe </strong>New games on Let’s Play Sports in the Somerset area!</p>
								</div>
							</a>
						</li>
						<li>
							<a href="#">
								<div class="content-notifications">
									<div class="profile-pic">
										<img src="{{ url('') }}/images/james.png">
									</div>
									<p><strong>John Doe </strong>New games on Let’s Play Sports in the Somerset area!</p>
								</div>
							</a>
						</li>
						<li class="see-all"><a href="#">See All Message</a></li>
					</ul>
				</li>
				<li class="icon-img">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">
						<img src="{{ url('') }}/images/icons/dash-head-icon3.png">
					</a>
					<div class="dropdown-menu notifications" role="menu">
						<div class="drop-head">
							<h1>Helpful information</h1>
						</div>
						<ul>
							<li><a href="{{ url('') }}/pages/about-us"><img src="{{ url('') }}/images/icons/about.png">About us</a></li>
							<li><a href="{{ url('') }}/pages/terms-conditions"><img src="{{ url('') }}/images/icons/tnc.png">Terms &amp; Conditions</a></li>
							<li><a href="{{ url('') }}/pages/privacy-policy"><img src="{{ url('') }}/images/icons/privacy.png">Privacy Policy</a></li>
							<li><a href="{{ url('') }}/contactus"><img src="{{ url('') }}/images/icons/mail.png">Contact us</a></li>
						</ul>
					</div>
				</li>
				<li class="dropdown profile-name">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						<div class="profile-img">
							@if ($user->profile_img && $user->profile_img!='null')
							<img src="{{ url('') }}/upload/images/{{$user->profile_img}}" class="w-50 rounded-circle" alt="{{$user->first_name}}">
							@else
							<img src="{{ url('') }}/images/avtar.png" class="w-50 rounded-circle" alt="{{$user->first_name}}">
							@endif
						</div>{{$user->first_name}} <span class="caret"></span>
					</a>

					<ul class="dropdown-menu" role="menu">
						<li>
							<a href="{{url('/dashboard')}}">Dashboard</a>
						</li>
						<li>
							<a href="{{url('')}}/profile/edit">Settings</a>
						</li>
						<li>
							<a href="{{url('')}}/profile">Profile</a>
						</li>
						<li class="divider"></li>
						<li>
							<a href="{{ route('logout') }}" onclick="event.preventDefault();
			document.getElementById('logout-form').submit();">
								Logout
							</a>
						</li>
						<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
							@csrf
						</form>
					</ul>
				</li>
				<!--  <li class="dropdown">
					<a  href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						Choose your sport
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu" role="menu">
						
				
						@foreach ($sports as $sport)
						<li>
						<a class="dropdown-item" href="{{ url('') }}/searchgame?search=&date=&gender=&sport={{ $sport->id  }}">
							{{ $sport->name }}
						</a>
						</li>
						@endforeach
					
					</ul>
				</li> -->
			</ul>
		</div>
	</nav>
</section>
<!--HEADER-SECTION-END-->