@extends('layouts.master')
@section('content')

<?php

@$user_id=Auth::user()->id;
@$images=\DB::table('game_images')->where('game_id',$game->gid)->get();
@$location=\DB::table('mst_venue')->where('id',$game->venue_id)->first();

@$spots=\DB::table('team_spots')->where('game_id',$game->id)->where('status',1)->count();

@$lightspots=\DB::table('team_spots')->where('game_id',$game->id)->where('team',1)->where('status',1)->count();
@$darkspots=\DB::table('team_spots')->where('game_id',$game->id)->where('team',2)->where('status',1)->count();
@$lightmembers=\DB::table('team_spots')->where('game_id',$game->id)->where('team',1)->where('status',1)->get();

@$darkmembers=\DB::table('team_spots')->where('game_id',$game->id)->where('team',2)->where('status',1)->get();

@$organizer=\DB::table('users')->where('id',$game->user_id)->first();

@$isingame=\DB::table('team_spots')->where('game_id',$game->id)->where('user_id',$user_id)->where('status',1)->count();

@$totalspots=@$game->team_limit;

@$spotleft= @$game->team_limit-$lightspots - $darkspots;

@$lightspotleft=@($game->team_limit / 2)-$lightspots;
@$darkspotleft=@($game->team_limit / 2)-$darkspots; 

@$commonVideo=\DB::table('common_video')->first();

?>
<div class="clearfix"></div>
<!--BANNER-SECTION-START-->
<section class="banner-detail">
   <h1 class="hedding-detail">{{ @$game->title }}</h1>
</section>
<!--BANNER-SECTION-END-->
<div class="clearfix"></div>

        {{-- @if(Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
            @php
                Session::forget('success');
            @endphp
            </div>
            @endif --}}
<!--DETAIL-CONTENT-SECTION-START-->
<section class="search-detail">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-8">
                    <div class="left-detail-section">
                        <!--crousel-start-->
                        <div class='header' id='cover'>
                    <div class='carousel slide' data-ride='carousel' id='carousel'>
                    <div class='carousel-inner'>
                    <div class='item active'>
                    <!--<div class='banner-detail-info'>
                    <ul>
                        <li>
                            <h3>00 <span>HOURS</span></h3>
                        </li>
                        <li>
                            <h3>00 <span>MINUTES</span></h3>
                        </li>
                        <li>
                            <h3>00 <span>SECONDS</span></h3>
                        </li>
                    </ul>
                    </div>-->
                    <img class='responsive' alt='umpires' src='{{ url('') }}/upload/images/{{ @$game->featured_img }}'/>
                </div>
                    @if (@$images)
                        
                    
                    @foreach ($images as $image)
                        
                    
                    <div class='item'>
                     <!--<div class='banner-detail-info'>
                    <ul>
                        <li>
                            <h3>00 <span>HOURS</span></h3>
                        </li>
                        <li>
                            <h3>00 <span>MINUTES</span></h3>
                        </li>
                        <li>
                            <h3>00 <span>SECONDS</span></h3>
                        </li>
                    </ul>
                    </div>-->
                    <img class='responsive' alt='fallball4' src='{{ url('') }}/upload/images/{{ $image->image }}'/>
                    </div>

                    @endforeach
                    @endif
                    
                    </div>
                    <div class='clearfix'>
                    <div class='carousel-link'>
                    <div class='thumb' data-slide-to='0' data-target='#carousel'>
                    <img class='responsive' alt='umpires' src='{{ url('') }}/upload/images/{{ @$game->featured_img }}'/>
                </div>
                @foreach ($images as $key=> $thumb)
                    <div class='thumb' data-slide-to='{{ $key+1 }}' data-target='#carousel'>
                    <img class='responsive' alt='baseballd' src='{{ url('') }}/upload/images/{{ $thumb->image }}'/>
                    </div>
                    @endforeach
                   
                   
                    
                    </div>
                    </div>
                    </div>
                    </div>
                    <!--crousel-end-->
                </div>

            </div>
            <div class="col-sm-4">
                    <div class="when-and-where">
                        @if (@$user_id==@$game->user_id)
                        @if (@$game->game_start_date>=date('Y-m-d'))
                        <p class="text-right">
                                <a href="{{ url('') }}/startgame/{{ @$game->gid }}" class="join-game">
                                    EDIT GAME
                                </a>
                        </p>
                        @endif
                        @endif
                        
                        
                        <h2>When & Where</h2>
                        <ul>
                            <li>
                                <div class="col-sm-1 col-xs-2 p-0">
                                    <img src="{{ url('') }}/images/icons/when-where-watch.png">
                                </div>
                                <div class="col-sm-11 col-xs-10">
                                    <h4>Start Time / Start Date</h4>
                                    <p>{{ @$game->game_start_time }} / {{ date('D, M d, Y', strtotime(@$game->game_start_date)) }}</p>
                                </div>
                            </li>
                            <li>
                                <div class="col-sm-1 col-xs-2 p-0">
                                    <img src="{{ url('') }}/images/icons/when-where-location.png">
                                </div>
                                <div class="col-sm-11 col-xs-10">
                                    <h4>Venue Name</h4>
                                    <p>{{ @$location->name }}</p>
                                </div>
                            </li>
                            <li>
                                <div class="col-sm-1 col-xs-2 p-0">
                                    <img src="{{ url('') }}/images/icons/when-where-location.png">
                                </div>
                                <div class="col-sm-11 col-xs-10">
                                    <h4>Address</h4>
                                    
                                    <p>{{ @$location->location }}</p>
                                </div>
                            </li>
                            <li>
                                <div class="col-sm-1 col-xs-2 p-0">
                                    <img src="{{ url('') }}/images/icons/when-where-online.png">
                                </div>
                                <div class="col-sm-11 col-xs-10">
                                    @if (@$game->payment==1)
                                    <h4>Online</h4>
                                    <p class="online-text">
                                        @if ($game->currency=='USD')
                                            ${{ $game->price }} /per person
                                        @else
                                        £{{ $game->price }} /per person
                                        @endif
                                        </p>
                                    @elseif(@$game->payment==2)
                                    <h4>Cash</h4>
                                    <p class="online-text">
                                            @if ($game->currency=='USD')
                                            ${{ $game->price }} /per person
                                        @else
                                        £{{ $game->price }} /per person
                                        @endif    
                                    </p>
                                    @else
                                    <h4>Free</h4>
                                    @endif

                                    
                                    <!-- <a href="{{ url('') }}/register-your-interest">
											<button class="pay-now">
													Register Your Interest 
											</button>
										</a> -->
                                </div>
                            </li>
                            <!--<li>
                                <div class="col-sm-1 col-xs-2 p-0">
                                    <img src="{{ url('') }}/images/icons/when-where-member.png">
                                </div>
                                <div class="col-sm-11 col-xs-10">
                                    <p class="number-player">{{ @$spots }}/{{ @$game->team_limit*2 }}</p>
                                </div>
                            </li>-->
                        </ul>
                        @if (Route::has('login'))
                      @auth
                        @if (@$user_id != @$game->user_id)
                        
                        @if (@$game->game_start_date >= date('Y-m-d'))
                        @if (@$game->payment==1)
                        @if ($isingame==0)
                        <a href="{{ url('') }}/paynow/{{ @$game->slug }}/{{ $game->id }}">
                        <button class="pay-now">PAY NOW</button>
                        </a>
                        {{-- <a href="{{ url('') }}/register-your-interest">
                            <button class="pay-now">Register Your Interest </button>
                            </a> --}}
                        {{-- <a href="{{ url('') }}/register-your-intrest">
                            <button class="register-intrest">Register Your Interest</button>
                            </a> --}}
                        @endif
                        @endif
                        @endif

                        @endif
                        @else
                    <a class="nav-link" href="javascript:void(0)" onclick="return setC()"> <button class="pay-now">PAY NOW</button>  </a>
                 
                    @endauth
                    @endif
                       {{-- <a href="{{ url('') }}/register-your-interest">
                        <button class="pay-now">Register Your Interest</button>
                        </a> --}}
                        
                        <div class="dounload-app-here">
                            <h2>Download our app here</h2>
                            <button class="ios"><img src="{{ url('') }}/images/icons/apple.png">Apple Store</button>
                        <button class="android"><img src="{{ url('') }}/images/icons/android.png">Play Store</button>
                        </div>
                    </div>
                </div>
        </div>
</section>
<!--DETAIL-CONTENT-SECTION-END-->
<div class="clearfix"></div>
<!--6pm 8-a-side Drapers-SECTION-START-->
<!--<section class="a-side">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-12">
               
            </div>
        </div>
    </div>
    </div>
</section>-->
<!--6pm 8-a-side Drapers-SECTION-END-->
<div class="clearfix"></div>
<!--DESCRIPTION-SECTION-START-->
<section class="description">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-8">
                    <div class="description-left">
                         <h2>{{ @$game->game_start_time }}
                            @if (@$game->sport_id!=2 && @$game->sport_id!=14 && @$game->sport_id!=6 && @$game->sport_id!=5 && @$game->sport_id!=7 && @$game->sport_id!=4 && @$game->sport_id!=8 && @$game->sport_id!=9 && @$game->sport_id!=15)
                                {{ @$game->team_limit / 2 }}-a-side  
                            @endif
                             

                             {{ @$location->name }} </h2>
                <p></p>
                        <h3>Description</h3>
                        <p>{!! @$game->description !!}</p>
                        <div class="invite-friend">
                            <button class="your-friend btn btn-primary" data-toggle="modal" data-target="#modal-invite-friend">+ Invite Your Friend</button>
                            <div class="detail-share-buttons">
                                <ul>
                                    <li>
                                            <a target="_blank" href="https://facebook.com/sharer/sharer.php?u={{ url('') }}/games/{{ @$game->slug }}/{{ @$game->id }}"><img src="{{ url('') }}/images/icons/facebook.png"></a>
                                        
                                    </li>
                                    <li>
                                        
                                        <a target="_blank" href="https://twitter.com/share?url={{ url('') }}/games/{{ @$game->slug }}/{{ @$game->id }}&amp;text={{ @$game->title }}"><img src="{{ url('') }}/images/icons/Twitter.png"></a>
                                    </li>
                                    <li>
                                            <a target="_self" href="whatsapp://send?text={{ url('') }}/games/{{ @$game->slug }}/{{ @$game->id }}"><img src="{{ url('') }}/images/icons/whatsapp.png"></a>
                                        
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" onclick="setClipboard('{{ url('') }}/games/{{ @$game->slug }}/{{ @$game->id }}');">
                                        <img src="{{ url('') }}/images/icons/link.png">
                                    </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="post-by">
                            <div class="col-sm-6">
                                <div class="col-sm-3 col-xs-3 p-0">
                                    <div class="profile-img-post">
                                        @if (@$organizer->profile_img && $organizer->profile_img!='null')
						<img src="{{ url('') }}/upload/images/{{$organizer->profile_img}}" class="w-50 rounded-circle" alt="{{$organizer->first_name}}">
						@else
						<img src="{{ url('') }}/images/avtar.png" class="w-50 rounded-circle" alt="{{ @$organizer->first_name }}">
						@endif
                                    </div>
                                </div>
                                <div class="col-sm-9 col-xs-9 p-0">
                                    <div class="profile-name-post">
                                        <p class="player-name">{{ @$organizer->first_name }} {{ @$organizer->last_name }}</p>
                                         <p class="player-about">Game Organizer</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    {{-- <img src="{{ url('') }}/images/game-fun.png" alt="" srcset=""> --}}
                    <div id="map"></div>
                    <a target="_blank" href="{{ url('') }}/venue/{{ @$location->slug }}">
                        <button style="margin-top:20px;"  class="pay-now">View More Sessions</button>
                        </a>
                {{-- </div>
                <div class="col-sm-4"> --}}
                        
                    <div class="right-team-spot" style="margin-top: 20px; display:none;">
                            @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                                @php
                                    Session::forget('success');
                                @endphp
                                </div>
                                @endif
                        <h2>Will you attend? {{ @$spotleft }} spots left</h2>
                        @if (@$game->game_start_date >= date('Y-m-d'))
                            
                        
                        @if (Route::has('login'))
                        @auth

                            @if ($isingame>0)
                            <form action="{{ url('') }}/leavegame" method="POST">
                                @csrf
                                <input type="hidden" name="game_id" value="{{ @$game->gid }}">
                                <input type="hidden" name="game_cid" value="{{ @$game->id }}">
                                <button type="submit" class="leave-game">LEAVE GAME</button>
                            </form>
                            @else
                                {{-- <form action="{{ url('') }}/joingame" method="POST">
                                    @csrf
                                    <input type="hidden" name="game_id" value="{{ @$game->id }}"> --}}
                                    <a href="{{ url('') }}/paynow/{{ @$game->slug }}/{{ $game->id }}">
                                        <button type="submit" class="join-game">JOIN GAME</button>
                                    </a>
                                {{-- </form> --}}
                            @endif
                        
                        @else
                        <a href="javascript:void(0)" onclick="return setC()" class="join-game">JOIN GAME</a>
                        @endauth
                        @endif

                        @endif

                        

                      
                        <div class="team-players">
                                <div class="col-sm-6 col-xs-6">
                                        <h5 class="font-bold text-black text-right">
                                                Light Tees
                                                ({{ @$lightspots }}/{{ round(@$game->team_limit / 2) }})
                                                </h5>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                        <h5 class="font-bold text-black text-left">
                                                Dark Tees
                                                ({{ @$darkspots }}/{{ round(@$game->team_limit /2)}})
                                                </h5>
                                    </div>
                             <div class="col-sm-6 col-xs-6">  
                                 
                            @foreach ($lightmembers as $key =>  $lightmember)Open Spot
                            <?php
                                $teamMember=\DB::table('users')->where('id',$lightmember->user_id)->first();
                            ?>
                           
                            <div class="col-sm-12 col-xs-12 p-0">
                                <div class="plyer-team">
                            
                                <div class="col-sm-8 col-xs-8 p-0">
                                    <?php
                                    $fullname=$teamMember->first_name.' '.$teamMember->last_name;
                                    ?>
                                <p class="player-name">
                                        {{ str_limit($fullname, $limit = 8, $end = '...') }}
                                </p>
                                {{-- <p class="player-about">Midfielder</p> --}}
                            </div>
                            <div class="col-sm-4 col-xs-4 p-0">
                                    <div class="player-img">
                                        
                                            @if ($teamMember->profile_img && $teamMember->profile_img!='null')
                                            <img src="{{ url('') }}/upload/images/{{$teamMember->profile_img}}" class="w-50 rounded-circle" alt="{{$teamMember->first_name}}">
                                            @else
                                            <img src="{{ url('') }}/images/avtar.png" class="w-50 rounded-circle" alt="{{$teamMember->first_name}}">
                                            @endif
                                    </div>
                                </div>
                            </div>
                            </div>
                            @endforeach
                        
                        
                            @if ($lightspotleft>0)
                            @for ($i = 0; $i < $lightspotleft; $i++)
                            
                            {{-- Open Spot --}}
                            <div class="col-sm-12 col-xs-12 p-0">
                               <div class="plyer-team">
                               <div class="col-sm-8 col-xs-8 p-0">
                               <p class="player-name">Open Spot</p>
                               <p class="player-about">Join Game</p>
                              </div>
                              <div class="col-sm-4 col-xs-4 p-0">
                               <div class="player-img">
                                   <img src="{{ url('') }}/images/icons/open-spot.png">
                               </div>
                               </div>
                               </div>
                           </div>
                           {{-- Open Spot --}}
   
                            @endfor
                            @endif
                        </div>

                        <div class="col-sm-6 col-xs-6"> 
                            @foreach ($darkmembers as $key =>  $darkmember)
                            <?php
                                $teamMember=\DB::table('users')->where('id',$darkmember->user_id)->first();
                            ?>
                                <div class="col-sm-12 col-xs-12 p-0">
                                <div class="plyer-team">
                                    <div class="col-sm-4 col-xs-4 p-0">
                                        <div class="player-img">
                                            
                                                @if ($teamMember->profile_img && $teamMember->profile_img!='null')
                                                <img src="{{ url('') }}/upload/images/{{$teamMember->profile_img}}" class="w-50 rounded-circle" alt="{{$teamMember->first_name}}">
                                                @else
                                                <img src="{{ url('') }}/images/avtar.png" class="w-50 rounded-circle" alt="{{$teamMember->first_name}}">
                                                @endif
                                        </div>
                                    </div>
                                <div class="col-sm-8 col-xs-8 p-0">
                                    <?php
                                    $fullname=$teamMember->first_name.' '.$teamMember->last_name;
                                    ?>
                                <p class="player-name">
                                        {{ str_limit($fullname, $limit = 8, $end = '...') }}
                                </p>
                                {{-- <p class="player-about">Midfielder</p> --}}
                            </div>
                            
                            </div>
                            </div>
                            @endforeach
                            

                            

                            @if ($darkspotleft>0)
                            @for ($j = 0; $j < $darkspotleft; $j++)
                            
                            {{-- Open Spot --}}
                            <div class="col-sm-12 col-xs-12 p-0">
                               <div class="plyer-team">
                                    <div class="col-sm-4 col-xs-4 p-0">
                                            <div class="player-img">
                                                <img src="{{ url('') }}/images/icons/open-spot.png">
                                            </div>
                                            </div>
                               <div class="col-sm-8 col-xs-8 p-0">
                               <p class="player-name">Open Spot</p>
                               <p class="player-about">Join Game</p>
                              </div>
                              
                               </div>
                           </div>
                           {{-- Open Spot --}}
   
                            @endfor
                            @endif
                        </div>
                            
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--DESCRIPTION-SECTION-END-->
<div class="clearfix"></div>
<!--VIDEO-SECTION-START-->
{{-- <section class="detail-video">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="video-detail">
                        <video width="100%" height="auto" controls>
                                <source src="{{ url('') }}/upload/video/{{ $commonVideo->video }}" type="video/mp4">
                                <source src="{{ url('') }}/upload/video/{{ $commonVideo->video }}" type="video/ogg">
                              Your browser does not support the video tag.
                              </video>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
            <p class="video-info">{{ @$commonVideo->description }}</p>
            </div>
        </div>
    </div>
</section> --}}
<!--VIDEO-SECTION-END-->
<div class="clearfix"></div>
<!--COMMENT-SECTION-START-->
{{-- <section class="comments">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="hedding-comments">
                    <h1>Comments</h1>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Quisq</p>
                </div>
                    
                    @foreach ($comments as $comment)
                   <//?php
                    $commentuser=\DB::table('users')->where('id',$comment->user_id)->first();
                    $replies=\DB::table('game_comments')->where('comment_id',$comment->id)->get();
                   ?>
                    <div class="comment-section">
                        <div class="col-sm-1">
                            <div class="profile-img-comment img-pro">
                                @if (@$commentuser && @$commentuser->profile_img!='null')
						<img src="{{ url('') }}/upload/images/{{$commentuser->profile_img}}" class="w-50 rounded-circle" alt="{{$commentuser->first_name}}">
						@else
						<img src="{{ url('') }}/images/avtar.png" class="w-50 rounded-circle">
						@endif
                            </div>
                        </div>
                        <div class="col-sm-11">
                            <div class="comment-box">
                            <div class="name-date-comment">
                                <h3>{{ $comment->name }}</h3>
                                <p>{{ date('M d, Y',strtotime($comment->created_at)) }}</p>
                            </div>
                            <p>{{ $comment->comment }}</p>
                            <a class="reply-comment" commentid="{{ $comment->id }}" href="javascript:void(0);">Reply</a>
                            
                        </div>
                        @foreach ($replies as $reply)
                            
                        <//?php
                            $replyuser=\DB::table('users')->where('id',$reply->user_id)->first();
                        ?>
                        <div class="comment-section">
                                <div class="col-sm-1">
                                    <div class="profile-img-comment img-pro">
                                            @if (@$replyuser && @$replyuser->profile_img!='null')
                                            <img src="{{ url('') }}/upload/images/{{$replyuser->profile_img}}" class="w-50 rounded-circle" alt="{{$replyuser->first_name}}">
                                            @else
                                            <img src="{{ url('') }}/images/avtar.png" class="w-50 rounded-circle">
                                            @endif
                                    </div>
                                </div>
                                <div class="col-sm-11">
                                    <div class="comment-box">
                                            <div class="name-date-comment">
                                                    <h3>{{ $reply->name }}</h3>
                                                    <p>{{ date('M d, Y',strtotime($reply->created_at)) }}</p>
                                                </div>
                                                <p>{{ $reply->comment }}</p>
                                </div>
                                </div> 
                            </div>
                            @endforeach
                        
                        </div> 
                    </div>
                    @endforeach

                    <div class="clearfix"></div>

                    <div class="hedding-comments comment-form">
                    <h1>Leave a comment</h1>
                    <p>Your email address will not be published. Required fields are marked*</p>
                </div>
                
                @if(Session::has('success'))
        <div class="alert alert-success">
            
            {{ Session::get('success') }}
            @php
                Session::forget('success');
            @endphp
            </div>
            @endif
                <form class="comment" id="comment-from" action="{{ url('') }}/gamecomment" method="POST">
                    @csrf
                    <div class="col-sm-6 p-0">
                        <div class="form-group">
                            <label class="control-label">Your Name </label>
                            <input type="text" name="name" class="form-control" placeholder="Enter Your  Name*" required>
                            @if ($errors->has('name'))
            <span class="text-danger">{{ $errors->first('name') }}</span>
        @endif
                            </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Your Email </label>
                            <input type="text" name="email" class="form-control" placeholder="Enter Your  Email*" required>
                            @if ($errors->has('email'))
            <span class="text-danger">{{ $errors->first('email') }}</span>
        @endif
                            </div>
                    </div>
                    <div class="col-sm-12 pl-0">
                        <div class="form-group">
                            <label class="control-label">	Your Comment </label>
                            <textarea class="form-control textarea" name="comment" placeholder="Enter Your Comment*" required></textarea>
                        @if ($errors->has('comment'))
                            <span class="text-danger">{{ $errors->first('comment') }}</span>
                        @endif

                        </div>
                    </div>
                    <div class="col-sm-12 p-0">
                        <input type="hidden" value="{{ @$game->id }}" name="game_id">
                        <input type="hidden" name="comment_id" id="commentid">
                    <input type="hidden" value="{{ @$user_id }}" name="userid" id="userid">
                    <button class="submit" type="submit" id="commentBtn">SUBMIT</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section> --}}
<!--COMMENT-SECTION-END-->

<!-- The modal -->
<div class="modal fade" id="modal-invite-friend" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog invite-friend-model" role="document">
		<div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">&times;</span>
		</button>
		<h4 class="modal-title" id="modalLabel">Invite Your Friend</h4>
		<p>Invite your friend and get 1 session free at selected locations. </p>
        </div>
        
		<div class="modal-body">
		<form class="form-invite" action="{{ url('') }}/sendinvitation" method="POST">
            @csrf
            <input type="email" required name="email" placeholder="Add your friend email address">
            <input type="hidden" name="gameurl" value="{{ url()->current() }}">
            <input type="hidden" name="gametitle" value="{{ @$game->title }}">
            <input type="hidden" name="gamevenue" value="{{ @$location->name }}">
			<button type="submit" class="invitefriend">INVITE FRIEND</button>
		</form>
		</div>
	
		</div>
		</div>
		</div>

        <script>
            function initAutocomplete() {
                var lattt=parseFloat({{ @$location->latitude }});
                var long=parseFloat({{ @$location->longitude }});
                var html ='<a target="_blank" href="http://maps.google.co.uk/maps?daddr='+lattt+','+long+'">';
                    html +='<p>{{ @$location->name }} </p>';
                html +='<p>{{  @$location->address }}  </p>';
                html +='<p>{{  @$location->postcode }} </p> ';
                html +='</a>';
                var locations = [
          
            [html, lattt, long, 1],
            
          
      ];
          var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: lattt, lng: long},
            zoom: 13,
            mapTypeId: 'roadmap'
          });
          var infowindow = new google.maps.InfoWindow();
  
      var bounds = new google.maps.LatLngBounds();
		  //var marker = new google.maps.Marker({map: map, position: {lat: lattt, lng: long}});
          marker = new google.maps.Marker({
              position: new google.maps.LatLng(lattt, long),
              map: map
          });
  
          bounds.extend(marker.position);
  
          google.maps.event.addListener(marker, 'click', (function (marker, i) {
              return function () {
                  infowindow.setContent(locations[0][0]);
                  infowindow.open(map, marker);
              }
          })(marker, 0));
        }
        function setClipboard(value) {
    var tempInput = document.createElement("input");
    tempInput.style = "position: absolute; left: -1000px; top: -1000px";
    tempInput.value = value;
    document.body.appendChild(tempInput);
    tempInput.select();
    document.execCommand("copy");
    document.body.removeChild(tempInput);
}


$(".reply-comment").click(function(){
   $('#commentid').val($(this).attr('commentid'));
   
     $('html, body').animate({
         scrollTop: $(".comment-form").offset().top
     }, 1500);
});
// $("#commentBtn").click(function(){
//     var formdata=$("#comment-from").serialize();
//     $.ajax({
//             url:"{{ url('') }}/gamecomment",
//             method:"POST",
//             dataType:'Json',
//             data:formdata,
//             success:function(data){
//                 console.log(data);
//             }
//             });
// });
        </script>

<script>
function setC(){
  <?php if(\Route::current()->getName() == 'gamedetail'){?>
  $.post('<?= URL::to('/setcookie/') ?>',{Url:'<?= Request::fullUrl() ?>',"_token": "{{ csrf_token() }}",} ,function(data, status){
   if(data.status =='success'){
    window.location.href = '<?= URL::to('/login') ?>';
   }
  });
<?php }else{ ?>
  window.location.href = '<?= URL::to('/login') ?>';
<?php } ?>

}

</script>
@endsection