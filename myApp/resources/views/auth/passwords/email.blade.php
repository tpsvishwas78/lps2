<?php
@$setting=\DB::table('site_setting')->first();
?>
@extends('layouts.master')

@section('content')
<!--SIGN-IN-PAGE-START-->
<section class="sign-in">
        @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
		<div class="left-pannel">
			<img src="{{ url('') }}/images/sign-in.jpg">
		</div>
		<div class="right-pannel">
			<div class="signin-top-section">
					<a href="{{ url('') }}">
				<img class="back-arrow-sign" src="{{ url('') }}/images/icons/back-arrow.png">
					</a>
				<div class="sign-in-logo">
						<a href="{{ url('') }}">
							<img src="{{ url('') }}/upload/images/{{ @$setting->header_logo }}">
						</a>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="sign-in-form">
				<h1>Forgot Password</h1>
				<p class="tag-line-heading">Enter Your email address to reset your password</p>
                <form method="POST" action="{{ route('password.email') }}">
                        @csrf
					<div class="form-group">
					<label class="control-label">Email</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus  placeholder="Enter Your Email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
					</div>
					
					<div class="sinin-buttons">
						<button type="submit" class="sign-in-here">RESET PASSWORD</button>
					</div>
				</form>
			</div>
		</div>
	</section>
	<!--SIGN-IN-PAGE-END-->
@endsection
