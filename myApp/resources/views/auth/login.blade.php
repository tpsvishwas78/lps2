<?php
@$setting=\DB::table('site_setting')->first();
?>
@extends('layouts.master')

@section('content')
    <!--SIGN-IN-PAGE-START-->
	<section class="sign-in" ng-controller='loginController'>
            <div class="left-pannel">
                <img src="{{ url('') }}/images/sign-in.jpg">
            </div>
            <div class="right-pannel">
                <div class="signin-top-section">
                        <a href="{{ url('') }}">
                    <img class="back-arrow-sign" src="{{ url('') }}/images/icons/back-arrow.png">
                        </a>
                        
                    <div class="sign-in-logo">
                            <a href="{{ url('') }}">
                                <img src="{{ url('') }}/upload/images/{{ @$setting->header_logo }}">
                            </a>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="sign-in-form">
                    <h1>Sign in</h1>
                    <form method="POST" action="{{ route('login') }}">
                            @csrf
                        <div class="form-group">
                        <label class="control-label">Email</label>
                        <input type="email" name="email" id="email" class="form-control @error('email') is-invalid @enderror" placeholder="Enter Your Email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="form-group">
                        <label class="control-label">Password</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Enter Your Password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="sinin-buttons">
                            <button class="sign-in-here">SIGN IN</button>
                            <span class="or">or</span>
                            <a href="{{ url('auth/facebook') }}" class="sign-in-fb"><img src="{{ url('') }}/images/icons/fb.png">Sign in With Facebook</a>
                        </div>
                    </form>
                    <div class="accont-create">
                        <p class="no-account">Don’t have an account? <a href="{{ route('register') }}">Sign Up here?</a></p>
                        <p class="forgot-pass"><a href="{{ route('password.request') }}">Forgot Password? Reset</a></p>
                    </div>
                </div>
            </div>
        </section>
        <!--SIGN-IN-PAGE-END-->

@endsection

<script>
app.controller('loginController', function($scope, CALLAPI){
            $scope.email = '';
            $scope.password = '';
            $scope.login = function(){
                isError = false;
                $scope.emailError = '';
                $scope.passwordError = '';
                if($scope.email == undefined){ $scope.emailError = 'Please enter email address'; isError = true; }
                if($scope.password == undefined){ $scope.passwordError = 'Please enter password'; isError = true; }
                
                if(isError == false)
                {                  
                  CALLAPI.post('login', {email:$scope.email, password:$scope.password})
                  .then(function(results){
                    console.log(results);
                      if(results.status == 'error')
                      {
                        $scope.loginError = results.message;
                      }else{
                        window.location = '<?php echo url('');?>/login-success/'+results.token;
                      }
                  });
                }
            }
        });
</script>