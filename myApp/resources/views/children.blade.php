@extends('layouts.master')
@section('content')

<!--BANNER-SECTION-START-->
<section class="banner-detail" style="background: url({{ url('') }}/images/Kid-parties-banner.jpg);background-size: cover !important;">
		<div  class="hedding-children">
       <h1>Kids parties</h1>
       {{-- <h3>AT lets play sports BIRMINGHAM INDOOR</h3> --}}
       </div>
    </section>
	<!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
	<!--available-at-section-start-->
	{{-- <section class="available-at">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h2 class="available-hedding">Available At <br>BIRMINGHAM INDOOR</h2>
				</div>
			</div>
		</div>
	</section> --}}
	<!--available-at-section-end-->
	<div class="clearfix"></div>
	<!--SLIDER-SECTION-START-->
	{{-- <section class="slider">
			<div>
			

			<div class="full-width-crousel">
				<div id="owl-demo" class="owl-carousel owl-theme">
						@if(@$sliders)
						@foreach ($sliders as $key=> $slider)
						</?php
		if ($slider->link=='' || $slider->link=='#') {
			$link=url('')."/register-your-intrest";
		} else {
			$link=$slider->link;
		}
		
						?>
				  <a href="{{ $link }}" class="item">
				  	<div class="col-md-12 col-sm-12 col-xs-12 p-0">
							<img src="{{ url('') }}/upload/sliders/{{ $slider->image }}" class="img-responsive">
							<div class="hover-deta-img-crousel">
								 <h2>{{ $slider->title }}</h2>
								 <p>{{ $slider->description }}</p>
							 </div>
						</div>
					</a>
					@endforeach
					@endif
			
				 
				</div>
			</div>
	</section> --}}
	<!--SLIDER-SECTION-END-->
	<div class="clearfix"></div>
	<!--soft-play-section-start-->
	  <section class="blocks-children">
	  	<div class="container">
	  		<div class="row">
	  	 <div class="col-sm-12">
			   @foreach ($parties as $party)
			   <div class="col-sm-6">
					<a href="{{ url('') }}/kids/{{ $party->slug }}" class="block-img">
						<img src="{{ url('') }}/upload/images/{{ $party->image }}">
						<div class="hover-deta-img">
							<h2>{{ $party->name }}</h2>
							<p>{{ $party->description }}</p>
						</div>
					</a>
				</div>
			   @endforeach
	  	 	
	  	 	
	  	 	
	  	 	
	  	 </div>
	  	</div>
	  	</div>
	  </section>
	<!--soft-play-section-end-->

@endsection