@extends('layouts.master')
@section('content')
<?php
$setting=\DB::table('site_setting')->first();
@$images=\DB::table('corporate_images')->where('cp_id',@$detail->id)->get();
?>

	<!--BANNER-SECTION-START-->
	<section class="banner-detail"  style="background: url({{ url('') }}/upload/images/{{ @$detail->banner }});">
		<div  class="hedding-children">
		<h1>{{ @$detail->title }}</h1>
	   <p style="color: #fff;">{{ @$detail->banner_description }}</p>
		</div>
    </section>
	<!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
	<!--crousel--and-video-section-start-->
	<section class="corporate-sports-day">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="col-sm-7">
						<div class="left-vide0-section">
							<div class="video-detail">
                                    {!! @$detail->main_video !!}
							</div>
						</div>
					</div>
					<div class="col-sm-5">
						<div class="right-side-crousel">
							<!--crousel-->
							<div class="header" id="cover">
									<div class="carousel slide" data-ride="carousel" id="carousel">
											<div class='carousel-inner'>
													<div class='item active'>
													
													<img class='responsive' alt='umpires' src='{{ url('') }}/upload/images/{{ @$detail->feature_image }}'/>
												</div>
													@if (@$images)
														
													
													@foreach ($images as $image)
														
													
													<div class='item'>
													
													<img class='responsive' alt='fallball4' src='{{ url('') }}/upload/images/{{ $image->image }}'/>
													</div>
								
													@endforeach
													@endif
													
													</div>
									<div class="clearfix">
											<div class='carousel-link'>
													<div class='thumb' data-slide-to='0' data-target='#carousel'>
													<img class='responsive' alt='umpires' src='{{ url('') }}/upload/images/{{ @$detail->feature_image }}'/>
												</div>
												@foreach ($images as $key=> $thumb)
													<div class='thumb' data-slide-to='{{ $key+1 }}' data-target='#carousel'>
													<img class='responsive' alt='baseballd' src='{{ url('') }}/upload/images/{{ $thumb->image }}'/>
													</div>
													@endforeach
													</div>
													
									</div>
									</div>
									</div>
						</div>
					</div>
					<div class="clearfix"></div>
					
				</div>
			</div>
		</div>
	</section>
<!--crousel--and-video-section-end-->
<section class="faq-christmas-party">
	<div class="container">
		<div class="row">
			<div class="heading-party col-md-12">
				<h2>{{ @$detail->sec_title }}</h2>
				<p>{!! @$detail->sec_description !!}</p>
			</div>
            <a href="{{ url('') }}/register-your-interest">
                <button class="enquiry-now">ENQUIRE NOW</button>
                </a>
			<div class="box-faqs">
				<?php
				$random=array(2,3,5,4,6,7,9,10,12,13,15,16,18,19,21,22,24,25,27,28,30,31,33,34,36,37,39,40,42,43,45,46,48,49,51,52,54,55,57,58,60,61,63,64,66,67,69,70,72,73,75,76,78,79,81,82,84,85,87,88,90,91,93,94,96,97,99);
				?>
                @foreach ($faqs as $key => $faq)
                    @if ($random[$key] % 2 == 0)
                    <div class="col-sm-6">
                            <div class="box-faq-red">
                                <p>{{ $faq->title }}</p>
                            </div>
                        </div>
                    @else
                    <div class="col-sm-6">
                            <div class="box-faq-black">
                                <p>{{ $faq->title }}</p>
                            </div>
                        </div>
                    @endif
                    
                @endforeach
				
                
				
                
                <div class="clearfix"></div>
                
			</div>
		</div>
	</div>
</section>
	
@endsection