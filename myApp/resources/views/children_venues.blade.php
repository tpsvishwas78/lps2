@extends('layouts.master')
@section('content')

<!--BANNER-SECTION-START-->
<section class="banner-detail" style="background: url({{ url('') }}/images/children-venues-banner-image.jpg);background-size: cover !important;">
		<div  class="hedding-children">
       <h1>childrens party venues</h1>
       {{-- <h3>AT lets play sports BIRMINGHAM INDOOR</h3> --}}
       </div>
    </section>
	<!--BANNER-SECTION-END-->
	<div class="clearfix"></div>
	<!--available-at-section-start-->
	{{-- <section class="available-at">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h2 class="available-hedding">Available At <br>BIRMINGHAM INDOOR</h2>
				</div>
			</div>
		</div>
	</section> --}}
	<!--available-at-section-end-->
	<div class="clearfix"></div>
	<!--SLIDER-SECTION-START-->
	{{-- <section class="slider">
			<div>
			

			<div class="full-width-crousel">
				<div id="owl-demo" class="owl-carousel owl-theme">
						@if(@$sliders)
						@foreach ($sliders as $key=> $slider)
						</?php
		if ($slider->link=='' || $slider->link=='#') {
			$link=url('')."/register-your-intrest";
		} else {
			$link=$slider->link;
		}
		
						?>
				  <a href="{{ $link }}" class="item">
				  	<div class="col-md-12 col-sm-12 col-xs-12 p-0">
							<img src="{{ url('') }}/upload/sliders/{{ $slider->image }}" class="img-responsive">
							<div class="hover-deta-img-crousel">
								 <h2>{{ $slider->title }}</h2>
								 <p>{{ $slider->description }}</p>
							 </div>
						</div>
					</a>
					@endforeach
					@endif
			
				 
				</div>
			</div>
	</section> --}}
	<!--SLIDER-SECTION-END-->

	<section class="search">
			<nav class="navbar navbar-light bg-light justify-content-between">
				<div class="container">
					<div class="col-sm-12">
						
				<a class="navbar-brand">FIND YOUR VENUE</a>
				<form class="form-inline" action="{{ url('') }}/children-venues">
					<input class="form-control mr-sm-2" type="text" name="search" placeholder="TOWN/CITY/POSTCODE" aria-label="Search" id="pac-input" value="{{ @$_GET['search'] }}">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit"><img src="{{ url('') }}/images/icons/search.png">Search Games</button>
				</form>
			</div>
			</nav>
		</section>
	<div class="clearfix"></div>
	<!--SEARCH-RESULT-SECTION-START-->
	<section class="serach-result">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					
					<div class="map-sports">
							<div id="map"></div>
					</div>
					{{-- <div class="filter_vailue">
							<select name="sort_by" id="sort_by" class="selectfilter sportfilter">
								<option value="">Filter Venues</option>
									<option @if (@$_GET['sortby']=='ASC')
										selected
									@endif value="ASC">A-Z</option>
									<option @if (@$_GET['sortby']=='DESC')
									selected
								@endif value="DESC">Z-A</option>
							</select>
						</div> --}}
					<div class="map-toggle-button">
							<p>SHOW MAP</p>
							<label class="switch">
							  <input class="switch-button" type="checkbox">
							  <span class="slider-button round"></span>
							</label>
						</div>
					</div>
				<!--game-listing-start-->
				<div class="col-sm-12">
					@foreach ($parties as $party)
						
					
					<div class="listing-games">
						<div class="design-list">
							<div class="col-sm-9">
								<div class="info-game">
									<a target="_blank" href="{{ url('') }}/children-detail/{{ $party->slug }}">
										<p>{{ $party->title }}</p>
									</a>
									
									<div class="dis-game">{{ $party->short_description }}</div>

									<div class="button-group">
											<a href="{{ url('') }}/children/register-your-interest">
										<button class="enq">Enquire</button>
											</a>
										<a target="_blank" href="{{ url('') }}/children-detail/{{ $party->slug }}">
										<button class="more-info">More Info</button>
										</a>
									</div>
								</div>
							</div>
							<div class="col-sm-3 p-0">
							<div class="game-img">
									<a target="_blank" href="{{ url('') }}/children-detail/{{ $party->slug }}">
										@if ($party->feature_image)
                                    <img src="{{url('')}}/upload/images/{{$party->feature_image}}">
                                @else
                                    <img src="{{url('')}}/images/no-user-Image.png">
                                @endif
									</a>
							</div>
							</div>
						</div>
					</div>
					@endforeach
					
				</div>
				<!--game-listing-end-->
			</div>

		</div>
	</section>
	<!--SEARCH-RESULT-SECTION-END-->
	<script>
$(".sportfilter").change(function(){
                var sortby=$(this).val();
                var search=$('#pac-input').val();
                window.history.pushState({}, '', 'children-venues?search='+search+'&sortby='+sortby);
                location.reload();
            });


            function initAutocomplete() {
      var locations = [
          @foreach ($parties as $party)
            ['{{$party->venue_address}}', {{$party->latitude}}, {{$party->longitude}}, 1],
            @endforeach
          
      ];
  
      window.map = new google.maps.Map(document.getElementById('map'), {
          mapTypeId: google.maps.MapTypeId.ROADMAP
	  });
	  
	  var input = document.getElementById('pac-input');
			//var searchBox = new google.maps.places.SearchBox(input);
			var options = {
			// types: ['(cities)'],
			componentRestrictions: {country: 'GB'}//UK only
			};
            var autocomplete = new google.maps.places.Autocomplete(input,options);
            
            var inputlocation = document.getElementById('pac-location');
			//var searchBox = new google.maps.places.SearchBox(input);
			var optionslocation = {
			// types: ['(cities)'],
			componentRestrictions: {country: 'GB'}//UK only
			};
			var autocompletelocation = new google.maps.places.Autocomplete(inputlocation,optionslocation);
  
            

      var infowindow = new google.maps.InfoWindow();
  
      var bounds = new google.maps.LatLngBounds();
  
      for (i = 0; i < locations.length; i++) {
          marker = new google.maps.Marker({
              position: new google.maps.LatLng(locations[i][1], locations[i][2]),
              map: map
          });
  
          bounds.extend(marker.position);
  
          google.maps.event.addListener(marker, 'click', (function (marker, i) {
              return function () {
                  infowindow.setContent(locations[i][0]);
                  infowindow.open(map, marker);
              }
          })(marker, i));
      }
  
      map.fitBounds(bounds);
  
      var listener = google.maps.event.addListener(map, "idle", function () {
          map.setZoom(12);
          google.maps.event.removeListener(listener);
      });
  }
  
        </script>

@endsection