@extends('admin.layouts.master')
@section('content')


<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
    <div class="mr-auto">
    <h1>Add Slider</h1>
    </div>
    </div>
</header>
<!--END PAGE HEADER -->
<section class="page-content container-fluid">

<div class="row">
    <div class="col-md-12">
<div class="card">
        
        <form  class="form-horizontal" action="{{ url('admin/submitHomeSlider') }}" method="POST" enctype="multipart/form-data">
            @csrf
    <div class="card-body">
            @if(Session::has('success'))

            <div class="alert alert-success">
    
                {{ Session::get('success') }}
    
                @php
    
                    Session::forget('success');
    
                @endphp
    
            </div>
    
            @endif
            <div class="form-body">

                    <div class="form-group row">
                            <label class="control-label text-right col-md-3">Image</label>
                            <div class="col-md-5">
                            <input class="form-control"  type="file" name="image" 
                            @if (@$edit->image)
                                
                            @else
                                required
                            @endif>

                            @if (@$edit->image)
                            <a target="_blank" href="{{url('')}}/upload/sliders/{{@$edit->image}}">Click here and see slider image</a>
                            @endif

                            
                            </div>
                        </div>

                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Title</label>
                    <div class="col-md-5">
                        <input type="text" placeholder="Title" id="title" name="title" class="form-control" value="{{ old('title') ? old('title') : @$edit->title }}">
                        @if ($errors->has('title'))
                        <span class="text-danger">{{ $errors->first('title') }}</span>
                    @endif
                    </div>
                </div>
                <div class="form-group row">
                        <label class="control-label text-right col-md-3">Description</label>
                        <div class="col-md-5">
                            <input type="text" placeholder="Description" id="description" name="description" class="form-control" value="{{ old('description') ? old('description') : @$edit->description }}">
                            @if ($errors->has('description'))
                            <span class="text-danger">{{ $errors->first('description') }}</span>
                        @endif
                        </div>
                    </div>
                    <div class="form-group row">
                            <label class="control-label text-right col-md-3">Link</label>
                            <div class="col-md-5">
                                <input type="text" placeholder="Link" id="link" name="link" class="form-control" value="{{ old('link') ? old('link') : @$edit->link }}">
                                @if ($errors->has('link'))
                                <span class="text-danger">{{ $errors->first('link') }}</span>
                            @endif
                            </div>
                        </div>
                        

                            <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Status</label>
                                    <div class="col-md-5">
                                    <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio"   id="customRadioInline3" name="status" class="custom-control-input" value="1" 
                                            @if (@$edit)
                                                @if (@$edit->status==1)
                                                checked
                                                @endif
                                            @else
                                            checked
                                            @endif>
                                            <label class="custom-control-label" for="customRadioInline3">Active</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadioInline4" name="status" class="custom-control-input" value="0"
                                                @if (@$edit && @$edit->status==0)
                                                checked
                                                @endif>
                                                <label class="custom-control-label" for="customRadioInline4">Deactive</label>
                                            </div>
                                    </div>
                                    @if ($errors->has('status'))
                                    <span class="text-danger">{{ $errors->first('status') }}</span>
                                @endif
                                </div>

                </div>
            
        </div>
        <div class="card-footer bg-light">
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="offset-sm-3 col-md-5">
                                <input type="hidden" name="id" value="{{@$edit->id}}">
                                <button type="submit" class="btn btn-primary btn-rounded">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>
</div>
</section>
@endsection