@extends('admin.layouts.master')
@section('content')


<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
    <div class="mr-auto">
    <h1>Edit Featured Blocks</h1>
    </div>
    </div>
</header>
<!--END PAGE HEADER -->

<section class="page-content container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                        @if(Session::has('success'))

                        <div class="alert alert-success">
                
                            {{ Session::get('success') }}
                
                            @php
                
                                Session::forget('success');
                
                            @endphp
                
                        </div>
                
                        @endif
                    <div class="card-body">
                        <table id="bs4-table" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($alljoinleagueblock as $joinleagueblock)
                                
                                <tr>
                                        <td>
                                                @if ($joinleagueblock->image)
                                                <img width="70" src="{{ url('') }}/upload/images/{{ $joinleagueblock->image }}" alt=""> 
                                             @else
                                             <img width="70" src="{{ url('') }}/images/no-user-Image.png" alt="">  
                                             @endif
                                        </td>
                                    <td>{{$joinleagueblock->title}}</td>
                                    <td>
                                    <a href="{{ url('admin/addjoinleagueblock') }}/{{ $joinleagueblock->id }}"><i class="zmdi zmdi-edit zmdi-hc-fw"></i></a>
                                         <a href="{{ url('admin/joinleagueblockdelete') }}/{{ $joinleagueblock->id }}" onClick="return confirm('Are you sure you want to Delete?');"><i class="zmdi zmdi-delete zmdi-hc-fw"></i></a></td>
                                </tr>
                                @endforeach
                            </tbody>
                            
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection