@extends('admin.layouts.master')
@section('content')


<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
    <div class="mr-auto">
    <h1>Manage All Payments</h1>
    </div> 
    </div>
</header>
<!--END PAGE HEADER -->

<section class="page-content container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                        @if(Session::has('success'))

                        <div class="alert alert-success">
                
                            {{ Session::get('success') }}
                
                            @php
                
                                Session::forget('success');
                
                            @endphp
                
                        </div>
                
                        @endif
                        
                        
                        <br>
                        <div class="form-body">
                        <form  class="form-horizontal" action="{{ url('admin/payments') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Sport</label>
                                    <div class="col-md-5">
                                        
                                    <select name="sports" id="sports" class="selectfilter sportfilter">
                                        <option value="">Filter Sport</option>
                                        @foreach ($sports as $sport)
                                            <option @if (@$_POST['sports']==$sport->id)
                                                selected
                                            @endif value="{{ $sport->id }}">{{ $sport->name }}</option>
                                        @endforeach
                                    </select>

                                        @if ($errors->has('name'))
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Date</label>
                                        <div class="col-md-5 datepicker">
                                            <input type="text" name="date"
                                            placeholder="Enter start end date" autocomplete="off" class="form-control" value="{{ @$_POST['date'] }}">
                                            @if ($errors->has('date'))
                                            <span class="text-danger">{{ $errors->first('date') }}</span>
                                        @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Gender</label>
                                        <div class="col-md-5">                                        
                        <select name="gender" id="gender" class="selectfilter genderfilter">
                                <option value="">Filter By Gender</option>
                                    <option {{ (@$_POST['gender'] == '2') ? 'selected' : '' }} value="2">Mens</option>
                                    <option {{ (@$_POST['gender'] == '3') ? 'selected' : '' }} value="3">Womens</option>
                                    <option {{ (@$_POST['gender'] == '1') ? 'selected' : '' }} value="1">Mixed</option>
                            </select>

                                            @if ($errors->has('address'))
                                            <span class="text-danger">{{ $errors->first('address') }}</span>
                                        @endif
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="offset-sm-3 col-md-5">
                                            <input type="hidden" name="id" value="{{@$edit->id}}">
                                            <button type="submit" class="btn btn-primary btn-rounded">Submit</button>
                                        </div>
                                    </div>
                     
        
                        </form>
                        </div>
                        <hr>
                    <div class="card-body">
                        <table id="bs4-table" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sport</th>
                                    <th>Game Name</th>
                                    <th>Author Name</th>
                                    <th>User Name</th>
                                    <th>Venue</th>
                                    <th>Price/Payment Status</th>
                                    <th>Start Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($games as $game)
                                <?php
@$repeat=\DB::table('repeat_games')->where('game_id',$game->id)->orderBy('game_start_date','DESC')->first();
                                ?>
                                <tr>
                                    <td>{{$game->sport_name}}</td>
                                    <td>{{$game->title}}</td>
                                    <td>{{$game->author_name}}</td>
                                    <td>{{$game->buyer_name}}</td>
                                    <td>{{ $game->address }}</td>
                                    <td>
                                            @if ($game->payment==3)
                                                Free
                                            @else
                                            @if ($game->currency=='USD')
                                            ${{ $game->price }} 
                                            @else
                                            £{{ $game->price }}
                                            @endif
                                            @endif
                                            / {{ $game->payment_status }}
                                        </td>
                                    <td>{{$game->game_start_date}}</td>
                                
                                    <td>
                                    <a target="_blank" href="{{ url('') }}/games/{{ $game->slug }}/{{ @$repeat->id }}"><i class="zmdi zmdi-eye zmdi-hc-fw"></i></a>
                                         
                                        
                                        </td>
                                </tr>
                                @endforeach
                            </tbody>
                            
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
     var dateToday = new Date();
        $('.datepicker').calendar({
          type: 'date',
          minDate: dateToday,
        });

    </script>

@endsection