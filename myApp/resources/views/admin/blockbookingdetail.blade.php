@extends('admin.layouts.master')
@section('content')


<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
    <div class="mr-auto">
    <h1>Block Booking Details</h1>
    </div>
    </div>
</header>
<!--END PAGE HEADER -->
<?php
//$location=\DB::table('mst_venue')->where('id',$booking->venue)->first();
$sport=\DB::table('mst_sports')->where('id',$booking->sport_id)->first();
?>
<section class="page-content container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                        
                    <div class="card-body">
                            <ul class="list-reset p-t-10">
                                    <li class="p-b-10">
                                        <span class="w-150 d-inline-block">First Name</span>
                                        <span>{{ $booking->first_name }}</span>
                                    </li>
                                    <li class="p-b-10">
                                            <span class="w-150 d-inline-block">Last Name</span>
                                            <span>{{ $booking->last_name }}</span>
                                    </li>
                                    <li class="p-b-10">
                                            <span class="w-150 d-inline-block">Email</span>
                                            <span>{{ $booking->email }}</span>
                                    </li>
                                    <li class="p-b-10">
                                            <span class="w-150 d-inline-block">Phone Number</span>
                                            <span>{{ $booking->phone_number }}</span>
                                    </li>
                                    <li class="p-b-10">
                                            <span class="w-150 d-inline-block">Venue</span>
                                            <span>{{ $booking->venue }}</span>
                                    </li>
                                    <li class="p-b-10">
                                            <span class="w-150 d-inline-block">Sport</span>
                                            <span>{{ $sport->name }}</span>
                                    </li>
                                    
                                    <li class="p-b-10">
                                            <span class="w-150 d-inline-block">Ideal Booking Date</span>
                                            <span>{{ date('M d, Y',strtotime($booking->ideal_days)) }}</span>
                                    </li>
                                    <li class="p-b-10">
                                            <span class="w-150 d-inline-block">Time</span>
                                            <span>{{ $booking->time }}</span>
                                    </li>
                                    <li class="p-b-10">
                                            <span class="w-150 d-inline-block">Comments</span>
                                            <span>{{ $booking->comments }}</span>
                                    </li>
                                    
                                    
                                </ul>


                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection