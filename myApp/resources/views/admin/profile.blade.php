@extends('admin.layouts.master')
@section('content')
<div class="content container">
<header class="page-header">
<div class="d-flex align-items-center">
    <div class="mr-auto">
        <h1 class="separator">My Account</h1>
        
    </div>
</div>
</header>
<section class="page-content container-fluid">
<div class="row">
    <div class="col">
        <div class="card">
                @if(Session::has('success'))

                <div class="alert alert-success">
        
                    {{ Session::get('success') }}
        
                    @php
        
                        Session::forget('success');
        
                    @endphp
        
                </div>
        
                @endif

                @if(Session::has('warning'))

                <div class="alert alert-warning">
        
                    {{ Session::get('warning') }}
        
                    @php
        
                        Session::forget('warning');
        
                    @endphp
        
                </div>
        
                @endif
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 col-lg-3">
                        <div class="nav flex-column nav-pills" id="my-account-tabs" role="tablist" aria-orientation="vertical">
                            <a class="nav-link active" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="true">Profile</a>
                            <a class="nav-link" id="v-pills-payment-tab" data-toggle="pill" href="#v-pills-payment" role="tab" aria-controls="v-pills-payment" aria-selected="false">Change Password</a>
                            
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-9">
                        <div class="tab-content" id="my-account-tabsContent">
                            <div class="tab-pane fade show active" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                <h4 class="card-heading p-b-20">Profile</h4>
                            <form method="POST" action="{{url('admin/updateprofile')}}" enctype="multipart/form-data">
                                @csrf
                                    <div class="form-group">
                                        
                                        @if ($userdata->profile_img)
                                        <img src="{{ url('') }}/upload/images/{{$userdata->profile_img}}" class="w-50 rounded-circle" alt="{{$userdata->first_name}}">
                                        @else
                                        <img src="{{ url('') }}/images/avtar.png" class="w-50 rounded-circle" alt="{{$userdata->first_name}}">
                                        @endif
                                        
                                        <div class="file-upload">
                                            <label for="upload" class="btn btn-primary m-b-0 m-l-5 m-r-5">Upload a new picture</label>
                                            <input id="upload" class="file-upload__input" type="file" name="image">
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="first_name">First name</label>
                                    <input type="text" class="form-control" name="first_name" id="first_name" autocomplete="name" placeholder="Enter your first name" value="{{$userdata->first_name}}">
                                    @if ($errors->has('first_name'))
                                    <span class="text-danger">{{ $errors->first('first_name') }}</span>
                                @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="last_name">Last name</label>
                                        <input type="text" class="form-control" name="last_name" id="last_name" autocomplete="name" placeholder="Enter your last name" value="{{$userdata->last_name}}">
                                        @if ($errors->has('last_name'))
                                        <span class="text-danger">{{ $errors->first('last_name') }}</span>
                                    @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email</label>
                                        <input type="email" class="form-control" name="email" autocomplete="email" id="exampleInputEmail1" placeholder="Enter email"  value="{{$userdata->email}}">
                                        @if ($errors->has('email'))
                                        <span class="text-danger">{{ $errors->first('email') }}</span>
                                    @endif
                                    </div>

                                    <div class="form-group">
                                        <label for="Username">Username</label>
                                        <input type="text" class="form-control" name="username" autocomplete="Username" id="Username" placeholder="Username" value="{{$userdata->username}}">
                                        @if ($errors->has('username'))
                                        <span class="text-danger">{{ $errors->first('username') }}</span>
                                    @endif
                                    </div>
                                    
                                    <button type="submit" class="btn btn-primary">Update Profile</button>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="v-pills-payment" role="tabpanel" aria-labelledby="v-pills-payment-tab">
                                <h4 class="card-heading p-b-20">Change Password</h4>
                                <form method="POST" action="{{url('admin/changepassword')}}">
                                    @csrf
                                        <div class="form-group">
                                            <label for="old_password">Old Password</label>
                                            <input type="password" class="form-control" id="old_password" placeholder="Enter Old Password" name="old_password">
                                            @if ($errors->has('old_password'))
                                        <span class="text-danger">{{ $errors->first('old_password') }}</span>
                                    @endif
                                        </div>

                                        <div class="form-group">
                                                <label for="new_password">New Password</label>
                                                <input type="password" class="form-control" id="new_password" placeholder="Enter New Password" name="new_password">
                                                @if ($errors->has('new_password'))
                                                <span class="text-danger">{{ $errors->first('new_password') }}</span>
                                            @endif
                                            </div>
                                            <div class="form-group">
                                                    <label for="confirm_password">Confirm Password</label>
                                                    <input type="password" class="form-control" id="confirm_password" placeholder="Enter Confirm Password" name="confirm_password">
                                                    @if ($errors->has('confirm_password'))
                                                <span class="text-danger">{{ $errors->first('confirm_password') }}</span>
                                            @endif
                                                </div>
                                        
                                        <button type="submit" class="btn btn-primary">Change Password</button>
                                    </form>
                                </div>
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>

@endsection