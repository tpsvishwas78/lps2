@extends('admin.layouts.master')
@section('content')


<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
    <div class="mr-auto">
    <h1>Manage Common Video</h1>
    </div>
    </div>
</header>
<!--END PAGE HEADER -->
<section class="page-content container-fluid">

<div class="row">
    <div class="col-md-12">
<div class="card">
        
        <form  class="form-horizontal" action="{{ url('admin/submitCommonVideo') }}" method="POST" enctype="multipart/form-data">
            @csrf
    <div class="card-body">
            @if(Session::has('success'))

            <div class="alert alert-success">
    
                {{ Session::get('success') }}
    
                @php
    
                    Session::forget('success');
    
                @endphp
    
            </div>
    
            @endif
            <div class="form-body">
                
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Video</label>
                    <div class="col-md-5">
                        <input type="file" id="video" name="video" class="form-control">
                        @if ($errors->has('video'))
                        <span class="text-danger">{{ $errors->first('video') }}</span>
                    @endif

                    @if (@$edit->video)
                    <video width="300" height="auto" controls>
                            <source src="{{ url('') }}/upload/video/{{ $edit->video }}" type="video/mp4">
                            <source src="{{ url('') }}/upload/video/{{ $edit->video }}" type="video/ogg">
                          Your browser does not support the video tag.
                          </video>
                    @endif
                    </div>
                    
                    
                </div>

                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Description</label>
                    <div class="col-md-5">
                    <textarea class="form-control" rows="5" name="description">{{@$edit->description}}</textarea>
                    </div>
                </div>

                <div class="form-group row">
                        <label class="control-label text-right col-md-3">Status</label>
                        <div class="col-md-5">
                        <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio"   id="customRadioInline3" name="status" class="custom-control-input" value="1" 
                                @if (@$edit)
                                    @if (@$edit->status==1)
                                    checked
                                    @endif
                                @else
                                checked
                                @endif>
                                <label class="custom-control-label" for="customRadioInline3">Active</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadioInline4" name="status" class="custom-control-input" value="0" @if (@$edit) @if (@$edit->status==0)
                                    checked
                                    @endif @endif>
                                    <label class="custom-control-label" for="customRadioInline4">Deactive</label>
                                </div>
                        </div>
                        @if ($errors->has('status'))
                        <span class="text-danger">{{ $errors->first('status') }}</span>
                    @endif
                    </div>
 
                </div>
            
        </div>
        <div class="card-footer bg-light">
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="offset-sm-3 col-md-5">
                                <input type="hidden" name="id" value="{{@$edit->id}}">
                                <button type="submit" class="btn btn-primary btn-rounded">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>
</div>
</section>


@endsection