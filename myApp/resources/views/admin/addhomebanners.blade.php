@extends('admin.layouts.master')
@section('content')


<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
    <div class="mr-auto">
    <h1>Add New Slider</h1>
    </div>
    </div>
</header>
<!--END PAGE HEADER -->
<section class="page-content container-fluid">

<div class="row">
    <div class="col-md-12">
<div class="card">
        
        <form  class="form-horizontal" action="{{ url('admin/submitHomeBanner') }}" method="POST" enctype="multipart/form-data">
            @csrf
    <div class="card-body">
            @if(Session::has('success'))

            <div class="alert alert-success">
    
                {{ Session::get('success') }}
    
                @php
    
                    Session::forget('success');
    
                @endphp
    
            </div>
    
            @endif
            <div class="form-body">

                    <div class="form-group row">
                            <label class="control-label text-right col-md-3">Banner Type</label>
                            <div class="col-md-5">
                            <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadioInline1" name="banner_type" class="custom-control-input" value="1" @if (@$edit->banner_type==1)
                                        checked
                                    @endif>
                                    <label class="custom-control-label" for="customRadioInline1">Image</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadioInline2" name="banner_type" class="custom-control-input" value="2" @if (@$edit->banner_type==2)
                                        checked
                                    @endif>
                                        <label class="custom-control-label" for="customRadioInline2">Video</label>
                                    </div>
                            </div>
                            @if ($errors->has('banner_type'))
                        <span class="text-danger">{{ $errors->first('banner_type') }}</span>
                    @endif
                        </div>
                        
                
                    <div class="form-group row">
                            <label class="control-label text-right col-md-3">Banner</label>
                            <div class="col-md-5">
                            <input class="form-control"  type="file" name="banner" 
                            @if (@$edit->banner)
                                
                            @else
                                required
                            @endif>

                            @if (@$edit->banner)
                            <a target="_blank" href="{{url('')}}/upload/banners/{{@$edit->banner}}">Click here and see banner</a>
                            @endif

                            @if ($errors->has('banner'))
                        <span class="text-danger">{{ $errors->first('banner') }}</span>
                    @endif
                            </div>
                        </div>

                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Title One</label>
                    <div class="col-md-5">
                        <input type="text" placeholder="Title One" id="title_one" name="title_one" class="form-control" value="{{ old('title_one') ? old('title_one') : @$edit->title_one }}">
                        @if ($errors->has('title_one'))
                        <span class="text-danger">{{ $errors->first('title_one') }}</span>
                    @endif
                    </div>
                </div>
                <div class="form-group row">
                        <label class="control-label text-right col-md-3">Title Two</label>
                        <div class="col-md-5">
                            <input type="text" placeholder="Title Two" id="title_two" name="title_two" class="form-control" value="{{ old('title_two') ? old('title_two') : @$edit->title_two }}">
                            @if ($errors->has('title_two'))
                            <span class="text-danger">{{ $errors->first('title_two') }}</span>
                        @endif
                        </div>
                    </div>
                    <div class="form-group row">
                            <label class="control-label text-right col-md-3">Title Three</label>
                            <div class="col-md-5">
                                <input type="text" placeholder="Title Three" id="title_three" name="title_three" class="form-control" value="{{ old('title_three') ? old('title_three') : @$edit->title_three }}">
                                @if ($errors->has('title_three'))
                                <span class="text-danger">{{ $errors->first('title_three') }}</span>
                            @endif
                            </div>
                        </div>
                        <div class="form-group row">
                                <label class="control-label text-right col-md-3">Button Link</label>
                                <div class="col-md-5">
                                    <input type="text" placeholder="Button Link" id="button_link" name="button_link" class="form-control" value="{{ old('button_link') ? old('button_link') : @$edit->button_link }}">
                                    @if ($errors->has('button_link'))
                                    <span class="text-danger">{{ $errors->first('button_link') }}</span>
                                @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Status</label>
                                    <div class="col-md-5">
                                    <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio"   id="customRadioInline3" name="status" class="custom-control-input" value="1" 
                                            @if (@$edit)
                                                @if (@$edit->status==1)
                                                checked
                                                @endif
                                            @else
                                            checked
                                            @endif>
                                            <label class="custom-control-label" for="customRadioInline3">Active</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadioInline4" name="status" class="custom-control-input" value="0" @if (@$edit->status==0)
                                                checked
                                                @endif>
                                                <label class="custom-control-label" for="customRadioInline4">Deactive</label>
                                            </div>
                                    </div>
                                    @if ($errors->has('status'))
                                    <span class="text-danger">{{ $errors->first('status') }}</span>
                                @endif
                                </div>

                </div>
            
        </div>
        <div class="card-footer bg-light">
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="offset-sm-3 col-md-5">
                                <input type="hidden" name="id" value="{{@$edit->id}}">
                                <button type="submit" class="btn btn-primary btn-rounded">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>
</div>
</section>
@endsection