@extends('admin.layouts.master')
@section('content')
<?php
@$images=\DB::table('kids_gallery')->where('kid',@$edit->id)->get();
@$videos=\DB::table('kids_whats_included')->where('kid',@$edit->id)->get();
?>
<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
    <div class="mr-auto">
    <h1>Add Kid Party Details</h1>
    </div>
    </div>
</header>
<!--END PAGE HEADER -->
<section class="page-content container-fluid">

<div class="row">
    <div class="col-md-12">
<div class="card">
        
        <form  class="form-horizontal" action="{{ url('admin/submitKidsDetailLayoutTwo') }}" method="POST" enctype="multipart/form-data">
            @csrf
    <div class="card-body">
            @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
                @php
                    Session::forget('success');
                @endphp
            </div>
            @endif

            <div class="form-body">
                    <div class="form-group row">
                            <label class="control-label text-right col-md-3">Select Category</label>
                            <div class="col-md-5">
                                <select name="category" id="category" class="form-control" >
                                    <option value="">select</option>
                                    @foreach ($allcorporatecatgory as $allcorporatecat)
                                    <option @if (@$edit->category_id==$allcorporatecat->id) selected @endif value="{{ $allcorporatecat->id }}">{{ $allcorporatecat->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('category'))
                                <span class="text-danger">{{ $errors->first('category') }}</span>
                                @endif
                            </div>
                        </div>                  
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Title</label>
                    <div class="col-md-5">
                        <input type="text" placeholder="Title" id="title" name="title" class="form-control" autocomplete="city-name" value="{{ old('title') ? old('title') : @$edit->title }}">
                        @if ($errors->has('title'))
                        <span class="text-danger">{{ $errors->first('title') }}</span>
                        @endif
                    </div>
                </div> 
                <div class="form-group row">
                        <label class="control-label text-right col-md-3">Feature Image</label>
                        <div class="col-md-5">
                        <input class="form-control"  type="file" name="image" >
                        @if(isset($edit->image))
                            <img src="{{ url('upload/images') }}/{{ $edit->image }}" alt="" width="115">
                        @endif
                        @if ($errors->has('image'))
                            <span class="text-danger">{{ $errors->first('image') }}</span>
                        @endif
                        </div>
                    </div>              
                
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Description</label>
                    <div class="col-md-9">
                    <textarea class="form-control cke_wrapper" id="editor1" name="description">{{ @$edit->description }}</textarea>
                    @if ($errors->has('description'))
                        <span class="text-danger">{{ $errors->first('description') }}</span>
                    @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Secound Title</label>
                    <div class="col-md-5">
                        <input type="text" placeholder="Secound Title" id="secound_title" name="secound_title" class="form-control" autocomplete="city-name" value="{{ old('secound_title') ? old('secound_title') : @$edit->secound_title }}">
                        @if ($errors->has('secound_title'))
                        <span class="text-danger">{{ $errors->first('secound_title') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Secound Description</label>
                    <div class="col-md-9">
                    <textarea class="form-control cke_wrapper" id="editor2" name="secound_description">{{ @$edit->secound_description }}</textarea>
                    @if ($errors->has('secound_description'))
                        <span class="text-danger">{{ $errors->first('secound_description') }}</span>
                    @endif
                    </div>
                </div>
                
                    
                    <div class="form-group row">
                            <label class="control-label text-right col-md-3">Gallery Images</label>
                            <div class="col-md-5">
                                    <div class="controls">
               
                                            <div class="entry input-group col-xs-3">
                                              
                                           
                                              <input class="btn btn-default" name="gallary[]" type="file">
                                              <span class="input-group-btn">
                                            <button class="btn btn-success btn-add" type="button">
                                                              <span class="zmdi zmdi-plus zmdi-hc-fw"></span>
                                              </button>
                                              </span>
                                            </div>
                                         
                                        </div>
                        @if (@$images)
                            @foreach (@$images as $image)
                            <p>
                            <img width="100" src="{{ url('') }}/upload/images/{{ $image->image }}" alt="">
                            <a href="javascript:void(0);"><i id="{{ $image->id }}" class="zmdi zmdi-delete zmdi-hc-fw removeimg"></i></a>
                            </p>
                            @endforeach
                        @endif
                        
                                        
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">Whats Included</label>
                            <div class="col-md-9">
                                    <div class="videocontrols">
               
                                            <div class="entry-video input-group col-xs-3">
                                              
                                                <input class="form-control" placeholder="Title" name="videotitle[]" type="text">
                                                <textarea placeholder="Description" class="form-control" name="videos[]"></textarea>
                                              <span class="input-group-btn">
                                            <button class="btn btn-success btn-add-video" type="button">
                                                              <span class="zmdi zmdi-plus zmdi-hc-fw"></span>
                                              </button>
                                              </span>
                                            </div>
                                    </div>
                            </div>
                        </div>
                        @if (@$videos)
                        @foreach ($videos as $video)
                        <div class="form-group row myvideo">
                            <div class="col-md-12">
                                <div class="row">
                                <div class="col-md-2">{{ $video->title }}</div>
                                <div class="col-md-7">{!! $video->description !!}</div>
                                <div class="col-md-3"><a href="javascript:void(0);"><i id="{{ $video->id }}" class="zmdi zmdi-delete zmdi-hc-fw removevideo"></i></a></div>
                            </div>
                            </div>
                        </div>
                        @endforeach
                        @endif

                
 
                </div>
            
        </div>
        <div class="card-footer bg-light">
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="offset-sm-3 col-md-5">
                                <input type="hidden" name="id" value="{{@$edit->id}}">
                                <button type="submit" class="btn btn-primary btn-rounded">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>
</div>

</section>



<script src="{{ url('') }}/admin/vendor/ckeditor/ckeditor.js"></script>

<script>
    CKEDITOR.replace( 'editor1' );
    CKEDITOR.replace( 'editor2' );
    //CKEDITOR.replace( 'editor3' );
    $(function()
{
    $(document).on('click', '.removeimg', function(e)
    {

         var id=$(this).attr('id');
         $(this).closest('p').remove();
        var _token = $('input[name="_token"]').val();
                $.ajax({
                 url:"{{ url('') }}/admin/deletekidsgalleryimage",
                 method:"POST",
                 data:{id:id, _token:_token},
                 success:function(data){
                    
                 }
                });
        
    });
    $(document).on('click', '.removevideo', function(e)
    {

         var id=$(this).attr('id');
         $(this).closest('.myvideo').remove();
        var _token = $('input[name="_token"]').val();
                $.ajax({
                 url:"{{ url('') }}/admin/deletekidsvideo",
                 method:"POST",
                 data:{id:id, _token:_token},
                 success:function(data){
                    
                 }
                });
        
    });
    $(document).on('click', '.btn-add', function(e)
    {
        e.preventDefault();

        var controlForm = $('.controls:first'),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="zmdi zmdi-minus zmdi-hc-fw"></span>');
    }).on('click', '.btn-remove', function(e)
    {
      $(this).parents('.entry:first').remove();

		e.preventDefault();
		return false;
	});
});

$(function()
{
    $(document).on('click', '.btn-add-video', function(e)
    {
        e.preventDefault();

        var controlForm = $('.videocontrols:first'),
            currentEntry = $(this).parents('.entry-video:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry-video:not(:last) .btn-add-video')
            .removeClass('btn-add-video').addClass('btn-remove-video')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="zmdi zmdi-minus zmdi-hc-fw"></span>');
    }).on('click', '.btn-remove-video', function(e)
    {
      $(this).parents('.entry-video:first').remove();

		e.preventDefault();
		return false;
	});
});

</script>

@endsection