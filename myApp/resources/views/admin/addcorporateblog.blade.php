@extends('admin.layouts.master')
@section('content')


<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
    <div class="mr-auto">
    <h1>Add New Featured Block</h1>
    </div>
    </div>
</header>
<!--END PAGE HEADER -->
<section class="page-content container-fluid">

<div class="row">
    <div class="col-md-12">
<div class="card">
        
        <form  class="form-horizontal" action="{{ url('admin/submitCorporateblog') }}" method="POST" enctype="multipart/form-data">
            @csrf
    <div class="card-body">
            @if(Session::has('success'))

            <div class="alert alert-success">
    
                {{ Session::get('success') }}
    
                @php
    
                    Session::forget('success');
    
                @endphp
    
            </div>
    
            @endif
            <div class="form-body">
                    <div class="form-group row">
                            <label class="control-label text-right col-md-3">Select Category</label>
                            <div class="col-md-5">
                                <select name="category" id="category" class="form-control" >
                                    <option value="">select</option>
                                    @foreach ($allcorporatecatgory as $allcorporatecat)
                                    <option @if (@$edit->category_id==$allcorporatecat->id) selected @endif value="{{ $allcorporatecat->id }}">{{ $allcorporatecat->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('category'))
                                <span class="text-danger">{{ $errors->first('category') }}</span>
                                @endif
                            </div>
                        </div>  
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Name</label>
                    <div class="col-md-5">
                        <input type="text" placeholder="Name" id="name" name="name" class="form-control" autocomplete="city-name" value="{{ old('name') ? old('name') : @$edit->title }}">
                        @if ($errors->has('name'))
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    @endif
                    </div>
                </div>
                <div class="form-group row">
                        <label class="control-label text-right col-md-3">Image</label>
                        <div class="col-md-5">
                        <input class="form-control"  type="file" name="image" >
                        @if(isset($edit->image))
                            <img src="{{ url('upload/images') }}/{{ $edit->image }}" alt="" width="115">
                        @endif
                        @if ($errors->has('image'))
                            <span class="text-danger">{{ $errors->first('image') }}</span>
                        @endif
                        </div>
                    </div>
                    <div class="form-group row">
                            <label class="control-label text-right col-md-3">Description</label>
                            <div class="col-md-5">
                                <textarea class="form-control" name="description" id="description" cols="30" rows="10">{{ old('description') ? old('description') : @$edit->description }}</textarea>
                                @if ($errors->has('description'))
                                <span class="text-danger">{{ $errors->first('description') }}</span>
                            @endif
                            </div>
                        </div>
 
                </div>
            
        </div>
        <div class="card-footer bg-light">
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="offset-sm-3 col-md-5">
                                <input type="hidden" name="id" value="{{@$edit->id}}">
                                <button type="submit" class="btn btn-primary btn-rounded">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>
</div>
</section>


@endsection