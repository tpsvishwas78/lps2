@extends('admin.layouts.master')
@section('content')


<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
    <div class="mr-auto">
    <h1>Manage Edit Slider</h1>
    </div>
    </div>
</header>
<!--END PAGE HEADER -->

<section class="page-content container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                        @if(Session::has('success'))

                        <div class="alert alert-success">
                
                            {{ Session::get('success') }}
                
                            @php
                
                                Session::forget('success');
                
                            @endphp
                
                        </div>
                
                        @endif
                    <div class="card-body">
                        <table id="bs4-table" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Banner Title</th>
                                    <th>Banner Type</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($banners as $banner)
                                <tr>
                                <td>{{$banner->title_one}}</td>
                                    <td>
                                    @if ($banner->banner_type==1)
                                        Image
                                    @else
                                        Video
                                    @endif
                                    </td>
                                <td>
                                    @if ($banner->status==1)
                                    <a onClick="return confirm('Are you sure you want to Deactive?');" style="color:#fff; padding-top:9px;" href="{{ url('admin/bannerstatus') }}/{{ $banner->id }}" class="btn btn-danger btn-sm">Deactive Now</a>
                                    @else
                                    <a onClick="return confirm('Are you sure you want to Active?');" style="color:#fff; padding-top:9px;" href="{{ url('admin/bannerstatus') }}/{{ $banner->id }}" class="btn btn-success btn-sm">Active Now</a>
                                    @endif
                                    
                                </td>
                                    <td>
                                    <a href="{{ url('admin/addhomebanners') }}/{{ $banner->id }}"><i class="zmdi zmdi-edit zmdi-hc-fw"></i></a>
                                         <a href="{{ url('admin/bannerdelete') }}/{{ $banner->id }}" onClick="return confirm('Are you sure you want to Delete?');"><i class="zmdi zmdi-delete zmdi-hc-fw"></i></a></td>
                                </tr>
                                @endforeach
                            </tbody>
                            
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection