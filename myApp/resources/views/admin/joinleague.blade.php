@extends('admin.layouts.master')
@section('content')

<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
    <div class="mr-auto">
    <h1>Edit Join A League</h1>
    </div>
    </div>
</header>
<!--END PAGE HEADER -->
<section class="page-content container-fluid">

<div class="row">
    <div class="col-md-12">
<div class="card">
        
        <form  class="form-horizontal" action="{{ url('admin/submitJoinLeague') }}" method="POST" enctype="multipart/form-data">
            @csrf
    <div class="card-body">
            @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
                @php
                    Session::forget('success');
                @endphp
            </div>
            @endif

            <div class="form-body">
                    
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Banner Title</label>
                    <div class="col-md-5">
                        <input type="text" placeholder="Banner Title" id="banner_title" name="banner_title" class="form-control" autocomplete="city-name" value="{{ old('banner_title') ? old('banner_title') : @$edit->banner_title }}">
                        @if ($errors->has('banner_title'))
                        <span class="text-danger">{{ $errors->first('banner_title') }}</span>
                        @endif
                    </div>
                </div> 
                
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Banner Image</label>
                    <div class="col-md-5">
                    <input class="form-control"  type="file" name="banner" >
                    @if(isset($edit->banner))
                        <img src="{{ url('upload/images') }}/{{ $edit->banner }}" alt="" width="115">
                    @endif
                    @if ($errors->has('banner'))
                        <span class="text-danger">{{ $errors->first('banner') }}</span>
                    @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Title</label>
                    <div class="col-md-5">
                        <input type="text" placeholder="Title" id="title" name="title" class="form-control" autocomplete="city-name" value="{{ old('title') ? old('title') : @$edit->title }}">
                        @if ($errors->has('title'))
                        <span class="text-danger">{{ $errors->first('title') }}</span>
                        @endif
                    </div>
                </div> 

            
                <div class="form-group row" id='mydescription'>
                    <label class="control-label text-right col-md-3">Description</label>
                    <div class="col-md-9">
                    <textarea class="form-control cke_wrapper" id="editor1" name="description">{{ @$edit->description }}</textarea>
                    @if ($errors->has('description'))
                        <span class="text-danger">{{ $errors->first('description') }}</span>
                    @endif
                    </div>
                </div>

                </div>
            
        </div>
        <div class="card-footer bg-light">
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="offset-sm-3 col-md-5">
                                <input type="hidden" name="id" value="{{@$edit->id}}">
                                <button type="submit" class="btn btn-primary btn-rounded">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>
</div>

</section>



<script src="{{ url('') }}/admin/vendor/ckeditor/ckeditor.js"></script>

<script>
    CKEDITOR.replace( 'editor1' );
</script>

@endsection