@extends('admin.layouts.master')
@section('content')


<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
    <div class="mr-auto">
    <h1>Manage Booking</h1>
    </div>
    </div>
</header>
<!--END PAGE HEADER -->

<section class="page-content container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                        @if(Session::has('success'))

                        <div class="alert alert-success">
                
                            {{ Session::get('success') }}
                
                            @php
                
                                Session::forget('success');
                
                            @endphp
                
                        </div>
                
                        @endif
                    <div class="card-body">
                        <table id="bs4-table" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Venue</th>
                                    <th>Booking Date</th>
                                    <th>Added Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($bookings as $booking)
                                <?php
                                $location=\DB::table('mst_venue')->where('id',$booking->venue_id)->first();
                                ?>
                                <tr>
                                <td>{{ $booking->first_name }} {{ $booking->last_name }}</td>
                                    <td>{{ $location->name }}</td>
                                    <td>{{ date('M d, Y',strtotime($booking->date)) }}</td>
                                    <td>{{$booking->created_at}}</td>
                                <td>
                                    <a style="color:#fff; padding-top:9px;" href="{{ url('admin/bookingdetail') }}/{{ $booking->id }}" class="btn btn-danger btn-sm"><i class="zmdi zmdi-eye zmdi-hc-fw"></i> View</a>
                                    
                                </td>
                                    
                                </tr>
                                @endforeach
                            </tbody>
                            
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection