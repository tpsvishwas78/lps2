@extends('admin.layouts.master')
@section('content')


<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
    <div class="mr-auto">
    <h1>Manage Site Setting</h1>
    </div>
    </div>
</header>
<!--END PAGE HEADER -->
<section class="page-content container-fluid">

<div class="row">
    <div class="col-md-12">
<div class="card">
        
        <form  class="form-horizontal" action="{{ url('admin/submitSiteSetting') }}" method="POST" enctype="multipart/form-data">
            @csrf
    <div class="card-body">
            @if(Session::has('success'))

            <div class="alert alert-success">
    
                {{ Session::get('success') }}
    
                @php
    
                    Session::forget('success');
    
                @endphp
    
            </div>
    
            @endif
            <div class="form-body">

                    <div class="form-group row">
                            <label class="control-label text-right col-md-3">Website Name</label>
                            <div class="col-md-5">
                                <input type="text" placeholder="Website Name" id="website_name" name="website_name" class="form-control" value="{{ @$edit->website_name }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">Website Title</label>
                            <div class="col-md-5">
                                <input type="text" placeholder="Website Title" id="website_title" name="website_title" class="form-control" value="{{ @$edit->website_title }}">
                            </div>
                        </div>

                    <div class="form-group row">
                            <label class="control-label text-right col-md-3">Header Logo</label>
                            <div class="col-md-5">
                            <input class="form-control"  type="file" name="header_logo">
                            @if (@$edit->header_logo)
                                <img width="100px" src="{{ url('') }}/upload/images/{{ @$edit->header_logo }}" alt="">
                            @endif
                            </div>
                    </div>
                        <div class="form-group row">
                                <label class="control-label text-right col-md-3">Footer Logo</label>
                                <div class="col-md-5">
                                <input class="form-control"  type="file" name="footer_logo">
                                @if (@$edit->footer_logo)
                                <img width="100px" src="{{ url('') }}/upload/images/{{ @$edit->footer_logo }}" alt="">
                            @endif
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="control-label text-right col-md-3">Favicon Icon</label>
                                <div class="col-md-5">
                                <input class="form-control"  type="file" name="favicon_icon">
                                @if (@$edit->favicon_icon)
                                <img width="50px" src="{{ url('') }}/upload/images/{{ @$edit->favicon_icon }}" alt="">
                            @endif
                                </div>
                        </div>

                <div class="form-group row">
                    <label class="control-label text-right col-md-3">App Store Url</label>
                    <div class="col-md-5">
                        <input type="text" placeholder="App Store Url" id="app_store_link" name="app_store_link" class="form-control" value="{{ @$edit->app_store_link }}">
                       
                    </div>
                </div>
                <div class="form-group row">
                        <label class="control-label text-right col-md-3">Play Store Url</label>
                        <div class="col-md-5">
                            <input type="text" placeholder="Play Store Url" id="play_store_link" name="play_store_link" class="form-control" value="{{ @$edit->play_store_link }}">
                            
                        </div>
                    </div>
                    <div class="form-group row">
                            <label class="control-label text-right col-md-3">Facebook Url</label>
                            <div class="col-md-5">
                                <input type="text" placeholder="Facebook Url" id="facebook_url" name="facebook_url" class="form-control" value="{{ @$edit->facebook_url }}">
                               
                            </div>
                        </div>
                        <div class="form-group row">
                                <label class="control-label text-right col-md-3">Twitter Url</label>
                                <div class="col-md-5">
                                    <input type="text" placeholder="Twitter Url" id="twitter_url" name="twitter_url" class="form-control" value="{{ @$edit->twitter_url }}">
                                   
                                </div>
                            </div>
                            <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Instagram Url</label>
                                    <div class="col-md-5">
                                        <input type="text" placeholder="Instagram Url" id="instagram_url" name="instagram_url" class="form-control" value="{{ @$edit->instagram_url }}">
                                       
                                    </div>
                                </div>
                                <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Website Email</label>
                                        <div class="col-md-5">
                                            <input type="text" placeholder="Website Email" id="website_email" name="website_email" class="form-control" value="{{ @$edit->website_email }}">
                                           
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                            <label class="control-label text-right col-md-3">Sending Mail Email</label>
                                            <div class="col-md-5">
                                                <input type="text" placeholder="Sending Mail Email" id="sendmail_email" name="sendmail_email" class="form-control" value="{{ @$edit->sendmail_email }}">
                                               
                                            </div>
                                        </div>
                        <div class="form-group row">
                                <label class="control-label text-right col-md-3">Phone Number</label>
                                <div class="col-md-5">
                                    <input type="text" placeholder="Phone Number" id="phone_number" name="phone_number" class="form-control" value="{{ @$edit->phone_number }}">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label text-right col-md-3">City</label>
                                <div class="col-md-5">
                                    <input type="text" placeholder="City" id="city" name="city" class="form-control" value="{{ @$edit->city }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Address</label>
                                    <div class="col-md-5">
                                        <input type="text" placeholder="Address" id="address" name="address" class="form-control" value="{{ @$edit->address }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Latitude</label>
                                        <div class="col-md-5">
                                            <input type="text" placeholder="Latitude" id="latitude" name="latitude" class="form-control" value="{{ @$edit->latitude }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                            <label class="control-label text-right col-md-3">Longitude</label>
                                            <div class="col-md-5">
                                                <input type="text" placeholder="Longitude" id="longitude" name="longitude" class="form-control" value="{{ @$edit->longitude }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Copyright</label>
                                                <div class="col-md-5">
                                                    <input type="text" placeholder="Copyright" id="copyright" name="copyright" class="form-control" value="{{ @$edit->copyright }}">
                                                </div>
                                        </div>

                        

                </div>
            
        </div>
        <div class="card-footer bg-light">
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="offset-sm-3 col-md-5">
                                <input type="hidden" name="id" value="{{@$edit->id}}">
                                <button type="submit" class="btn btn-primary btn-rounded">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>
</div>
</section>
@endsection