@extends('admin.layouts.master')
@section('content')

<?php
@$images=\DB::table('game_images')->where('game_id',$edit->id)->get();
?>
<!--START PAGE HEADER -->
<header class="page-header">
    <div class="d-flex align-items-center">
    <div class="mr-auto">
    <h1>Add Game</h1>
    </div>
    </div>
</header>
<!--END PAGE HEADER -->
<section class="page-content container-fluid">

<div class="row">
    <div class="col-md-12">
<div class="card">
        
        <form  class="form-horizontal" action="{{ url('admin/creategame') }}" method="POST" enctype="multipart/form-data">
            @csrf
    <div class="card-body">
            @if(Session::has('success'))

            <div class="alert alert-success">
    
                {{ Session::get('success') }}
    
                @php
    
                    Session::forget('success');
    
                @endphp
    
            </div>
    
            @endif
            <div class="form-body">
                    <div class="form-group row">
                            <label class="control-label text-right col-md-3">Sport</label>
                            <div class="col-md-5">
                               <select required name="sport" id="sport" class="form-control">
                                   <option value="">Select</option>
                                   @foreach ($sports as $sport)
                                   <option @if (@$edit)
                                   @if ($edit->sport_id==$sport->id)
                                   selected
                               @endif
                                   @endif  value="{{ $sport->id }}">{{ $sport->name }}</option>
                                   @endforeach
                                   
                               </select>
                                @if ($errors->has('sport'))
                                <span class="text-danger">{{ $errors->first('sport') }}</span>
                            @endif
                            </div>
                        </div>
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Title</label>
                    <div class="col-md-5">
                        <input type="text" required placeholder="Title" id="title" name="title" class="form-control" autocomplete="city-name" value="{{ old('title') ? old('title') : @$edit->title }}">
                        @if ($errors->has('title'))
                        <span class="text-danger">{{ $errors->first('title') }}</span>
                    @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Featured Image</label>
                    <div class="col-md-5">
                        <input type="file"  name="featured_img" class="form-control">
                       @if (@$edit->featured_img)
                           <img src="{{ url('') }}/upload/images/{{ @$edit->featured_img }}" alt="" width="200">
                       @endif
                    </div>
                </div>
                <div class="form-group row">
                        <label class="control-label text-right col-md-3">Description</label>
                        <div class="col-md-9">
                            <textarea id="editor1" class="cke_wrapper" name="description">{{ @$edit->description }}</textarea>
                            @if ($errors->has('description'))
                            <span class="text-danger">{{ $errors->first('description') }}</span>
                        @endif
                        </div>
                </div>
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Date</label>
                    <div class="col-md-5 datepicker">
                        <input type="text" required placeholder="Enter Your date" id="game_date" name="game_date" class="form-control" autocomplete="off" value="{{ old('game_date') ? old('game_date') : @$edit->game_date }}">
                        @if ($errors->has('game_date'))
                        <span class="text-danger">{{ $errors->first('game_date') }}</span>
                    @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Start Time</label>
                    <div class="col-md-5 timepicker">
                        <input type="text" required placeholder="Enter Your Start Time" id="game_start_time" name="game_start_time" class="form-control" autocomplete="off" value="{{ old('game_start_time') ? old('game_start_time') : @$edit->game_start_time }}">
                        @if ($errors->has('game_start_time'))
                        <span class="text-danger">{{ $errors->first('game_start_time') }}</span>
                    @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Finish Time</label>
                    <div class="col-md-5 timepicker">
                        <input type="text" placeholder="Enter Your Finish Time" id="game_finish_time" name="game_finish_time" required class="form-control" autocomplete="off" value="{{ old('game_finish_time') ? old('game_finish_time') : @$edit->game_finish_time }}">
                        @if ($errors->has('game_finish_time'))
                        <span class="text-danger">{{ $errors->first('game_finish_time') }}</span>
                    @endif
                    </div>
                </div>
                <div class="form-group row can-not-find">
                        <label class="control-label text-right col-md-3"></label>
                        <div class="col-md-5 intrest-checkbox">
                            <input class="styled-checkbox" @if (@$edit->is_repeat > 0)
                                checked
                            @endif id="styled-checkbox-repeat" name="repeat_check" type="checkbox" value="1">
                            <label for="styled-checkbox-repeat">Repeat event</label>
                        </div>
                    </div>
                    <div  id="repeat_option" @if (@$edit->is_repeat > 0)
                        style="display:block;"
                    @else style="display:none;" @endif>
                    <div class="form-group row">
                            <label class="control-label text-right col-md-3"></label>
                        <div class="col-md-5">
                            <select class="form-control select" name="is_repeat" id="is_repeat">
                                <option @if (@$edit->is_repeat==1) selected @endif value="1">Every week</option>
                                {{-- <option @if (@$edit->is_repeat==2) selected @endif value="2">Every 2 weeks</option>
                                <option @if (@$edit->is_repeat==3) selected @endif value="3">Every month</option> --}}
                            </select>
                           
                            </div>
                    </div>
                    <div class="form-group row">
                            <label class="control-label text-right col-md-3">Select which days</label>
                        <div class="col-md-5">
                            <div class="btn-group ">
                                <label class="btn btn-default">
                                    <input type="checkbox" @if (@in_array(1,unserialize(@$edit->days)))
                                        checked
                                    @endif name="days[]" value="1">Mon
                                </label>
                                <label class="btn btn-default">
                                    <input @if (@in_array(2,unserialize($edit->days)))
                                    checked
                                @endif type="checkbox" name="days[]" value="2"> Tue
                                </label>
                                <label class="btn btn-default">
                                    <input @if (@in_array(3,unserialize($edit->days)))
                                    checked
                                @endif type="checkbox" name="days[]" value="3"> Wed
                                </label>
                                <label class="btn btn-default">
                                    <input @if (@in_array(4,unserialize($edit->days)))
                                    checked
                                @endif type="checkbox" name="days[]" value="4"> Thur
                                </label>
                                <label class="btn btn-default">
                                    <input @if (@in_array(5,unserialize($edit->days)))
                                    checked
                                @endif type="checkbox" name="days[]" value="5"> Fri
                                </label>
                                <label class="btn btn-default">
                                    <input @if (@in_array(6,unserialize($edit->days)))
                                    checked
                                @endif type="checkbox" name="days[]" value="6"> Sat
                                </label>
                                <label class="btn btn-default">
                                    <input @if (@in_array(7,unserialize($edit->days)))
                                    checked
                                @endif type="checkbox" name="days[]" value="7"> Sun
                                </label>
                            </div>
                           
                            </div>
                    </div>
                
                    <div class="form-group row">
                            <label class="control-label text-right col-md-3">Set an end date</label>
                        <div class="col-md-5 datepicker">
                            <input type="text" name="end_date" value="{{ @$edit->end_date }}" class="form-control" placeholder="Enter Your end date" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Venue</label>
                    <div class="col-md-5">
                        <?php
                        if(@$edit->venue_id){
                            $venue=\DB::table('mst_venue')->where('id',$edit->venue_id)->first();
                        }

                        ?>
                        <input type="text" required placeholder="Enter Venue" id="venue" name="venue" class="form-control" autocomplete="off" value="{{ @$venue->name }}">
                        <input type="hidden" id="venue_id" name="venue_id" value="{{ @$edit->venue_id }}">
                        <input type="hidden" id="latitude" name="latitude">
                        <input type="hidden" id="longitude" name="longitude">
                        @if ($errors->has('venue'))
                        <span class="text-danger">{{ $errors->first('venue') }}</span>
                    @endif
                    <div id="venueList"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label text-right col-md-3"></label>
                    <div class="col-md-5">
                        <div class="can-not-find">
                            <p>Cannot find what you're looking for? <a target="_blank" href="{{ url('') }}/admin/addvenue">Add it</a></p>
                            <div class="intrest-checkbox">
                                <input class="styled-checkbox" id="styled-checkbox-10" name="is_private" type="checkbox" value="1" @if (@$edit->is_private==1)
                                    checked
                                @endif>
                                <label for="styled-checkbox-10">Private (Only invited players can join)</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Players Required (Even Number)</label>
                    <div class="col-md-5">
                        <select required class="form-control select" name="team_limit" id="team_limit">
                            <option value="">Select</option>
                            <?php 
                $k=0;
            for ($i=1; $i <= 20; $i++) { 
                    $k = $i *2;
                ?>
                 <option @if (@$edit->team_limit==$k) selected @endif value="{{$k}}">{{$k}}</option>
            <?php  } ?>
            <!-- <option @if (@$edit->team_limit==1) selected @endif value="1">1</option>
            <option @if (@$edit->team_limit==2) selected @endif value="2">2</option>
            <option @if (@$edit->team_limit==3) selected @endif value="3">3</option>
            <option @if (@$edit->team_limit==4) selected @endif value="4">4</option>
            <option @if (@$edit->team_limit==5) selected @endif value="5">5</option>
            <option @if (@$edit->team_limit==6) selected @endif value="6">6</option>
            <option @if (@$edit->team_limit==7) selected @endif value="7">7</option>
            <option @if (@$edit->team_limit==8) selected @endif value="8">8</option>
            <option @if (@$edit->team_limit==9) selected @endif value="9">9</option>
            <option @if (@$edit->team_limit==10) selected @endif value="10">10</option>
            <option @if (@$edit->team_limit==11) selected @endif value="11">11</option>
            <option @if (@$edit->team_limit==12) selected @endif value="12">12</option>
            <option @if (@$edit->team_limit==13) selected @endif value="13">13</option>
            <option @if (@$edit->team_limit==14) selected @endif value="14">14</option>
            <option @if (@$edit->team_limit==15) selected @endif value="15">15</option>
            <option @if (@$edit->team_limit==16) selected @endif value="16">16</option>
            <option @if (@$edit->team_limit==17) selected @endif value="17">17</option>
            <option @if (@$edit->team_limit==18) selected @endif value="18">18</option>
            <option @if (@$edit->team_limit==19) selected @endif value="19">19</option>
            <option @if (@$edit->team_limit==20) selected @endif value="20">20</option>
            <option @if (@$edit->team_limit==21) selected @endif value="21">21</option>
            <option @if (@$edit->team_limit==22) selected @endif value="22">22</option> -->
                        </select>
                        @if ($errors->has('team_limit'))
                        <span class="text-danger">{{ $errors->first('team_limit') }}</span>
                    @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Gender options</label>
                    <div class="col-md-5">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input @if (@$edit->gender==1)
                            checked
                        @endif @if (empty(@$edit))
                        checked
                        @endif type="radio" id="gender1" name="gender" class="custom-control-input" value="1">
                            <label class="custom-control-label" for="gender1">Mixed</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input @if (@$edit->gender==3)
                            checked
                        @endif type="radio" id="gender2" class="custom-control-input" name="gender" value="3">
                            <label class="custom-control-label" for="gender2">Women</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input @if (@$edit->gender==2)
                            checked
                        @endif type="radio" id="gender3" class="custom-control-input" name="gender" value="2">
                            <label class="custom-control-label" for="gender3">Men</label>
                        </div>
                        @if ($errors->has('gender'))
                        <span class="text-danger">{{ $errors->first('gender') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label text-right col-md-3">Payment</label>
                    <div class="col-md-5">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input @if (@$edit->payment==1)
                            checked
                        @endif type="radio" id="payment1" name="payment" class="custom-control-input" value="1" checked>
                            <label class="custom-control-label" for="payment1">Online</label>
                        </div>
                         <div class="custom-control custom-radio custom-control-inline">
                            <input @if (@$edit->payment==2)
                            checked
                        @endif type="radio" id="payment2" class="custom-control-input" name="payment" value="2">
                            <label class="custom-control-label" for="payment2">Cash</label>
                        </div>
                        {{--<div class="custom-control custom-radio custom-control-inline">
                            <input @if (@$edit->payment==3)
                            checked
                        @endif type="radio" id="payment3" class="custom-control-input" name="payment" value="3">
                            <label class="custom-control-label" for="payment3">Free</label>
                        </div> --}}
                        @if ($errors->has('payment'))
                        <span class="text-danger">{{ $errors->first('payment') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row mainpayment" >
                    <label class="control-label text-right col-md-3">Price per player</label>
                    <div class="col-md-5">
                        <div class="input-group payment-box" >
                            <input value="{{ @$edit->price }}" type="text" class="form-control" name="price">
                            <span class="input-group-btn">
                              <select class="btn" name="currency">
                                    <option @if (@$edit->currency=='GBP') selected @endif value="GBP">GBP</option>
                                    {{-- <option @if (@$edit->currency=='EUR') selected @endif value="EUR">EUR</option>
                                    <option @if (@$edit->currency=='USD') selected @endif value="USD">USD</option>  --}}
                              </select>
                            </span>
                          </div>
                          <div class="refund-fields">
                            <div class="form-group boolean optional game_refund_cancelled">
                                <div class="checkbox">
                                    <label>
                                        <input @if (@$edit->is_refund==1)
                                            checked
                                        @endif class="boolean optional" type="checkbox" value="1" name="is_refund" id="is_refund"> Refund if the game is cancelled
                                    </label>
                                </div>
                            </div>
                            <div class="form-group boolean optional game_refund_changed_rsvp">
                                <div class="checkbox">
                                    <label>
                                        <input class="boolean optional" @if (@$edit->refund_changed_rsvp==1)
                                            checked
                                        @endif type="checkbox" value="1" name="refund_changed_rsvp" id="refund_changed_rsvp"> Refund if RSVP changed 
                                        <input value="{{ @$edit->refund_days }}" class="numeric integer optional" type="text" name="refund_days" id="refund_days"> days before game
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                        <label class="control-label text-right col-md-3">Venue Images</label>
                        <div class="col-md-5">
                                <div class="controls">
           
                                        <div class="entry input-group col-xs-3">
                                          
                                       
                                          <input class="btn btn-default" name="gallary[]" type="file">
                                          <span class="input-group-btn">
                                        <button class="btn btn-success btn-add" type="button">
                                                          <span class="zmdi zmdi-plus zmdi-hc-fw"></span>
                                          </button>
                                          </span>
                                        </div>
                                     
                                    </div>
                                    @if (@$images)
            @foreach (@$images as $image)
                    <p>
                    <img width="100" src="{{ url('') }}/upload/images/{{ $image->image }}" alt="">
                    {{-- <a href="{{ url('/admin/gamedeletegalleryimage') }}/{{ $image->id }}"><i class="zmdi zmdi-delete zmdi-hc-fw removeimg"></i></a> --}}

                    <a href="javascript:void(0);"><i id="{{ $image->id }}" class="zmdi zmdi-delete zmdi-hc-fw removeimg"></i></a> 
                    </p>
            @endforeach
            @endif
                        </div>
                    </div>

                </div>
            
        </div>
        <div class="card-footer bg-light">
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="offset-sm-3 col-md-5">
                                <input type="hidden" name="id" value="{{@$edit->id}}">
                                <button type="submit" class="btn btn-primary btn-rounded">CREATE GAME</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>
</div>
</section>
<script src="{{ url('') }}/admin/vendor/ckeditor/ckeditor.js"></script>

<script>
    
    $(document).on('click', '.removeimg', function(e)
    {

         var id=$(this).attr('id');
         $(this).closest('p').remove();
        var _token = $('input[name="_token"]').val();
                $.ajax({
                 url:"{{ url('') }}/admin/gamedeletegalleryimage",
                 method:"POST",
                 data:{id:id, _token:_token},
                 success:function(data){
                    
                 }
                });
        
    });
    $(document).on('click', '#styled-checkbox-repeat', function(e)
    {
        var check=$('input[name="repeat_check"]:checked').val();
        if(check){
            $('#repeat_option').show();
        }else{
            $('#repeat_option').hide();
        }
        
    });
    var dateToday = new Date();
        $('.datepicker').calendar({
          type: 'date',
          minDate: dateToday,
        });
		$('.timepicker').calendar({
          type: 'time'
        });

    var payment1=$('#payment1').val();
     $(document).on('click', '#payment1', function(e)
    {
        $('.mainpayment').show();
        $('.payment-box').show();
        $('.help-block').show();
        $('.refund-fields').show();
    });
    $(document).on('click', '#payment2', function(e)
    {
        $('.mainpayment').show();
        $('.payment-box').show();
        $('.help-block').hide();
        $('.refund-fields').hide();
    });
    $(document).on('click', '#payment3', function(e)
    {
        $('.mainpayment').hide();
        $('.payment-box').hide();
        $('.help-block').hide();
        $('.refund-fields').hide();
    });

    $(function()
{
    $(document).on('click', '.btn-add', function(e)
    {
        e.preventDefault();

        var controlForm = $('.controls:first'),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="zmdi zmdi-minus zmdi-hc-fw"></span>');
    }).on('click', '.btn-remove', function(e)
    {
      $(this).parents('.entry:first').remove();

		e.preventDefault();
		return false;
	});
});
$(document).ready(function(){
        
        $('#venue').keyup(function(){
               var query = $(this).val();
               //console.log('query',query);
               if(query != '')
               {
                var _token = $('input[name="_token"]').val();
                $.ajax({
                 url:"{{ url('') }}/venue/autocomplete",
                 method:"POST",
                 data:{query:query, _token:_token},
                 success:function(data){
                     //console.log('data',data);
                            $('#venueList').fadeIn();
                           $('#venueList').html(data);
                 }
                });
               }
           });
       
           $(document).on('click', '.dropdown-menu li', function(){  
               $('#venue').val($(this).text());
               var lat=$(this).attr('lat');
               var lng=$(this).attr('lng');
               var vid=$(this).attr('vid');
               $('#latitude').val(lat);
               $('#longitude').val(lng);
               $('#venue_id').val(vid);
               $('#venueList').fadeOut();
           });  
       
       });

        CKEDITOR.replace( 'editor1' );
    </script>
@endsection