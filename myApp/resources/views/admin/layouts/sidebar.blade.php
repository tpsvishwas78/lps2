<?php
@$setting=\DB::table('site_setting')->first();
?>
<!-- START MENU SIDEBAR WRAPPER -->
<aside class="sidebar sidebar-left">
        <div class="sidebar-content">
        <div class="aside-toolbar">
        <ul class="site-logo">
        <li>
        <!-- START LOGO -->
        <a href="{{ url('admin/dashboard') }}">
        <div class="logo">
        <img src="{{ url('') }}/upload/images/{{ @$setting->header_logo }}">
        </div>
        </a>
        <!-- END LOGO -->
        </li>
        </ul>
        {{-- <ul class="header-controls">
        <li class="nav-item">
        <button type="button" class="btn btn-link btn-menu" data-toggle-state="mini-sidebar">
        <i class="la la-dot-circle-o"></i>
        </button>
        </li>
        </ul> --}}
        </div>
        <nav class="main-menu">
        <ul class="nav metismenu">
        <li class="sidebar-header"><span>NAVIGATION</span></li>
        <li class="nav-dropdown active">
        <a href="{{ url('admin/dashboard') }}" aria-expanded="false"><i class="icon dripicons-meter"></i><span>Dashboard</span></a>
        </li>
        <li class="nav-dropdown">
                        <a href="{{ url('admin/sitesetting') }}" aria-expanded="false"><i class="zmdi zmdi-settings zmdi-hc-fw"></i><span>Manage Site Setting</span></a>
                        </li>
        <li class="nav-dropdown">
        <a class="has-arrow" href="#" aria-expanded="false"><i class="la la-users"></i><span>Manage Users </span></a>
        <ul class="collapse nav-sub" aria-expanded="false">
        <li><a href="{{url('admin/allusers')}}" class="sub-menu"><span>All Users</span></a></li>
        </ul>
        </li>
        <li class="nav-dropdown">
                        <a class="has-arrow" href="#" aria-expanded="false"><i class="la la-image"></i><span>Manage Home Page</span></a>
                        <ul class="collapse nav-sub" aria-expanded="false">
                        <li><a href="{{url('admin/addhomebanners')}}" class="sub-menu"><span>Add New Slider</span></a></li>
                        <li><a href="{{url('admin/allhomebanners')}}" class="sub-menu"><span>Edit Slider</span></a></li>
                        <li><a href="{{url('admin/homeaboutus')}}" class="sub-menu"><span>Edit About Us</span></a></li>
                        <li><a href="{{url('admin/addbasicfeatures')}}" class="sub-menu"><span>Add New Why Join Us Feature</span></a></li>
                        <li><a href="{{url('admin/allbasicfeatures')}}" class="sub-menu"><span>Edit Why Join Us Features</span></a></li>
                        </ul>
                </li>
                <li class="nav-dropdown">
                        <a class="has-arrow" href="#" aria-expanded="false"><i class="la la-sliders"></i><span>Manage Corporate</span></a>
                        <ul class="collapse nav-sub" aria-expanded="false">
                        <li><a href="{{url('admin/addcorporatecatgory')}}" class="sub-menu"><span>Add New Event</span></a></li>
                        <li><a href="{{url('admin/allcorporatecatgory')}}" class="sub-menu"><span>Edit Events</span></a></li>
                        <li><a href="{{url('admin/addcorporateblog')}}" class="sub-menu"><span>Add New Featured Block</span></a></li>
                        <li><a href="{{url('admin/allcorporateblog')}}" class="sub-menu"><span>Edit Featured Blocks</span></a></li>
                        <li><a href="{{url('admin/addcorporatedetails')}}" class="sub-menu"><span>Add Event Details</span></a></li>
                        <li><a href="{{url('admin/allcorporatedetails')}}" class="sub-menu"><span>Edit Events Details</span></a></li>
                        </ul>
                </li>
                <li class="nav-dropdown">
                        <a class="has-arrow" href="#" aria-expanded="false"><i class="la la-sliders"></i><span>Manage Sports Turnaments</span></a>
                        <ul class="collapse nav-sub" aria-expanded="false">
                        <li><a href="{{url('admin/addsportsturnamentscatgory')}}" class="sub-menu"><span>Add New Event</span></a></li>
                        <li><a href="{{url('admin/allsportsturnamentscatgory')}}" class="sub-menu"><span>Edit Events</span></a></li>
                        <li><a href="{{url('admin/addsportsturnamentsblog')}}" class="sub-menu"><span>Add New Featured Block</span></a></li>
                        <li><a href="{{url('admin/allsportsturnamentsblog')}}" class="sub-menu"><span>Edit Featured Blocks</span></a></li>
                        <li><a href="{{url('admin/addsportsturnamentsdetails')}}" class="sub-menu"><span>Add Event Details</span></a></li>
                        <li><a href="{{url('admin/allsportsturnamentsdetails')}}" class="sub-menu"><span>Edit Events Details</span></a></li>
                        </ul>
                </li>
                <li class="nav-dropdown">
                                <a class="has-arrow" href="#" aria-expanded="false"><i class="la la-play-circle-o"></i><span>Manage Children</span></a>
                <ul class="collapse nav-sub" aria-expanded="false">
                        <li><a href="{{url('admin/add-children-party-category')}}" class="sub-menu"><span>Add New Kid Party Section</span></a></li>
                        <li><a href="{{url('admin/all-children-party-category')}}" class="sub-menu"><span>Edit Kid Parties Section</span></a></li>
                        <li><a href="{{url('admin/addkidsblog')}}" class="sub-menu"><span>Add New Featured Block</span></a></li>
                        <li><a href="{{url('admin/allkidsblog')}}" class="sub-menu"><span>Edit Featured Blocks</span></a></li>
                        <li><a href="{{url('admin/choose-kids-party-layout')}}" class="sub-menu"><span>Add New Parties Details</span></a></li>
                        <li><a href="{{url('admin/all-kids-party-details')}}" class="sub-menu"><span>Edit Parties Details</span></a></li>
                        <li><a href="{{url('admin/add-children-party')}}" class="sub-menu"><span>Add New Party Venue </span></a>
                        <li><a href="{{url('admin/all-children-party')}}" class="sub-menu"><span>Edit Party Venues</span></a>
                        </li>
                </ul>
                </li>
                <li class="nav-dropdown">
                        <a class="has-arrow" href="#" aria-expanded="false"><i class="la la-sliders"></i><span>Manage Join A League</span></a>
                        <ul class="collapse nav-sub" aria-expanded="false">
                        <li><a href="{{url('admin/joinleague')}}" class="sub-menu"><span>Edit Join A League</span></a></li>
                        <li><a href="{{url('admin/addjoinleagueblock')}}" class="sub-menu"><span>Add New Featured Block</span></a></li>
                        <li><a href="{{url('admin/alljoinleagueblock')}}" class="sub-menu"><span>Edit Featured Blocks</span></a></li>
                       
                        </ul>
                </li>
                <li class="nav-dropdown">
                        <a class="has-arrow" href="#" aria-expanded="false"><i class="la la-sliders"></i><span>Manage Games</span></a>
                        <ul class="collapse nav-sub" aria-expanded="false">
                        <li><a href="{{url('admin/addgame')}}" class="sub-menu"><span>Add Game</span></a></li>
                        <li><a href="{{url('admin/alladmingame')}}" class="sub-menu"><span>All Admin Games</span></a></li>
                        <li><a href="{{url('admin/allusergame')}}" class="sub-menu"><span>All User Games</span></a></li>
                        </ul>
                </li>
                <li class="nav-dropdown">
                                <a class="has-arrow" href="#" aria-expanded="false"><i class="la la-sliders"></i><span>Manage Carousel Slider </span></a>
                                <ul class="collapse nav-sub" aria-expanded="false">
                                <li><a href="{{url('admin/addslider')}}" class="sub-menu"><span>Add New Slide</span></a></li>
                                <li><a href="{{url('admin/allsliders')}}" class="sub-menu"><span>Edit Slide</span></a></li>
                                </ul>
                        </li>
                        <li class="nav-dropdown">
                                        <a href="{{ url('admin/commonvideo') }}" aria-expanded="false"><i class="icon dripicons-meter"></i><span>Manage Common Video</span></a>
                                        </li>
                        <li class="nav-dropdown">
                                <a class="has-arrow" href="#" aria-expanded="false"><i class="la la-sliders"></i><span>Manage Menus </span></a>
                                <ul class="collapse nav-sub" aria-expanded="false">
                                <li><a href="{{url('admin/addmenu')}}" class="sub-menu"><span>Add New Menu</span></a></li>
                                <li><a href="{{url('admin/allmenu')}}" class="sub-menu"><span>Edit Menus</span></a></li>
                                </ul>
                        </li>
                        <li class="nav-dropdown">
                                        <a class="has-arrow" href="#" aria-expanded="false"><i class="la la-sliders"></i><span>Manage Pages </span></a>
                                        <ul class="collapse nav-sub" aria-expanded="false">
                                        <li><a href="{{url('admin/addpage')}}" class="sub-menu"><span>Add New Page</span></a></li>
                                        <li><a href="{{url('admin/allpage')}}" class="sub-menu"><span>Edit Pages</span></a></li>
                                        </ul>
                                </li>
       
        <li class="nav-dropdown">
                <a class="has-arrow" href="#" aria-expanded="false"><i class="la la-map-marker"></i><span>Manage Venue’s</span></a>
                <ul class="collapse nav-sub" aria-expanded="false">
                <li><a href="{{url('admin/addvenue')}}" class="sub-menu"><span>Add New Venue</span></a></li>
                <li><a href="{{url('admin/allvenues')}}" class="sub-menu"><span>Edit New Venue’s</span></a></li>
                </ul>
        </li>
        <li class="nav-dropdown">
                        <a class="has-arrow" href="#" aria-expanded="false"><i class="la la-location-arrow"></i><span>Manage Intrest Area</span></a>
                        <ul class="collapse nav-sub" aria-expanded="false">
                        <li><a href="{{url('admin/addarea')}}" class="sub-menu"><span>Add New Interest Area </span></a></li>
                        <li><a href="{{url('admin/allarea')}}" class="sub-menu"><span>Edit Interest Area’s</span></a></li>
                        </ul>
        </li>
        <li class="nav-dropdown">
                        <a class="has-arrow" href="#" aria-expanded="false"><i class="la la-play-circle-o"></i><span>Manage Sports</span></a>
                        <ul class="collapse nav-sub" aria-expanded="false">
                        <li><a href="{{url('admin/addsport')}}" class="sub-menu"><span>Add New Sport</span></a></li>
                        <li><a href="{{url('admin/allsports')}}" class="sub-menu"><span>Edit Sports</span></a></li>
                        </ul>
        </li>
        <li class="nav-dropdown">
                <a class="has-arrow" href="#" aria-expanded="false"><i class="la la-play-circle-o"></i><span>Manage Venue Facilities</span></a>
                <ul class="collapse nav-sub" aria-expanded="false">
                <li><a href="{{url('admin/addfacilities')}}" class="sub-menu"><span>Add Facilities</span></a></li>
                <li><a href="{{url('admin/allfacilities')}}" class="sub-menu"><span>All Facilities</span></a></li>
                </ul>
</li>

        

        <li class="nav-dropdown">
                        <a class="has-arrow" href="#" aria-expanded="false"><i class="la la-play-circle-o"></i><span>Manage Advertise Forms</span></a>
                        <ul class="collapse nav-sub" aria-expanded="false">
                        <li><a href="{{url('admin/listbooking')}}" class="sub-menu"><span>Bookings</span></a></li>
                        <li><a href="{{url('admin/listsession')}}" class="sub-menu"><span>Sessions</span></a></li>
                        
                        </ul>
        </li>
        <li class="nav-dropdown">
                        <a href="{{ url('admin/manageblockbooking') }}" aria-expanded="false"><i class="zmdi zmdi-settings zmdi-hc-fw"></i><span>Manage Block Booking</span></a>
                        </li>
        <li class="nav-dropdown">
                        <a href="{{ url('admin/managegetinvolved') }}" aria-expanded="false"><i class="zmdi zmdi-settings zmdi-hc-fw"></i><span>Manage Get Involved</span></a>
                        </li>
        <li class="nav-dropdown">
                        <a href="{{ url('admin/payments') }}" aria-expanded="false"><i class="zmdi zmdi-money zmdi-hc-fw"></i><span>Manage Payments</span></a>
        </li>
        
        </ul>
        </nav>
        </div>
        </aside>
        <!-- END MENU SIDEBAR WRAPPER -->