<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Admin | Dashboard</title>
<!-- ================== GOOGLE FONTS ==================-->
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500" rel="stylesheet">
<!-- ======================= GLOBAL VENDOR STYLES ========================-->
<link rel="stylesheet" href="{{ url('') }}/admin/css/vendor/bootstrap.css">
<link rel="stylesheet" href="{{ url('') }}/admin/vendor/metismenu/dist/metisMenu.css">
<link rel="stylesheet" href="{{ url('') }}/admin/vendor/switchery-npm/index.css">
<link rel="stylesheet" href="{{ url('') }}/admin/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
<!-- ======================= LINE AWESOME ICONS ===========================-->
<link rel="stylesheet" href="{{ url('') }}/admin/css/icons/line-awesome.min.css">
<link rel="stylesheet" href="{{ url('') }}/admin/css/icons/simple-line-icons.css">
<!-- ======================= DRIP ICONS ===================================-->
<link rel="stylesheet" href="{{ url('') }}/admin/css/icons/dripicons.min.css">
<!-- ======================= MATERIAL DESIGN ICONIC FONTS =================-->
<link rel="stylesheet" href="{{ url('') }}/admin/css/icons/material-design-iconic-font.min.css">
<link href="{{ url('') }}/css/semantic.min.css" rel="stylesheet" type="text/css" />
<!-- ======================= PAGE VENDOR STYLES ===========================-->
<link rel="stylesheet" href="{{ url('') }}/admin/vendor/datatables.net-bs4/css/dataTables.bootstrap4.css">
<!-- ======================= GLOBAL COMMON STYLES ============================-->
<link rel="stylesheet" href="{{ url('') }}/admin/css/common/main.bundle.css">
<!-- ======================= LAYOUT TYPE ===========================-->
<link rel="stylesheet" href="{{ url('') }}/admin/css/layouts/vertical/core/main.css">
<!-- ======================= MENU TYPE ===========================-->
<link rel="stylesheet" href="{{ url('') }}/admin/css/layouts/vertical/menu-type/default.css">
<!-- ======================= THEME COLOR STYLES ===========================-->
<link rel="stylesheet" href="{{ url('') }}/admin/css/layouts/vertical/themes/theme-j.css">
<script src="{{ url('') }}/admin/vendor/modernizr/modernizr.custom.js"></script>
<script src="{{ url('') }}/admin/vendor/jquery/dist/jquery.min.js"></script>
<script src="{{ url('') }}/js/semantic.min.js"></script>
</head>
<body>
    <!-- START APP WRAPPER -->
<div id="app">
        @include('admin.layouts.sidebar')
        
<div class="content-wrapper">

<!-- END TOP TOOLBAR WRAPPER -->
<div class="content">
        @include('admin.layouts.header')
        
        @yield('content')
<!--END PAGE CONTENT -->
</div>
<!-- END SIDEBAR QUICK PANNEL WRAPPER -->
</div>
</div>
   <!-- END CONTENT WRAPPER -->

<!-- ================== GLOBAL VENDOR SCRIPTS ==================-->
{{--  <script src="{{ url('') }}/admin/vendor/modernizr/modernizr.custom.js"></script>
<script src="{{ url('') }}/admin/vendor/jquery/dist/jquery.min.js"></script>  --}}
<script src="{{ url('') }}/admin/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="{{ url('') }}/admin/vendor/js-storage/js.storage.js"></script>
<script src="{{ url('') }}/admin/vendor/js-cookie/src/js.cookie.js"></script>
<script src="{{ url('') }}/admin/vendor/pace/pace.js"></script>
<script src="{{ url('') }}/admin/vendor/metismenu/dist/metisMenu.js"></script>
<script src="{{ url('') }}/admin/vendor/switchery-npm/index.js"></script>
<script src="{{ url('') }}/admin/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- ================== PAGE LEVEL VENDOR SCRIPTS ==================-->
<script src="{{ url('') }}/admin/vendor/countup.js/dist/countUp.min.js"></script>
<script src="{{ url('') }}/admin/vendor/chart.js/dist/Chart.bundle.min.js"></script>
<script src="{{ url('') }}/admin/vendor/flot/jquery.flot.js"></script>
<script src="{{ url('') }}/admin/vendor/jquery.flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
<script src="{{ url('') }}/admin/vendor/flot/jquery.flot.resize.js"></script>
<script src="{{ url('') }}/admin/vendor/flot/jquery.flot.time.js"></script>
<script src="{{ url('') }}/admin/vendor/flot.curvedlines/curvedLines.js"></script>
<script src="{{ url('') }}/admin/vendor/datatables.net/js/jquery.dataTables.js"></script>
<script src="{{ url('') }}/admin/vendor/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>
<!-- ================== GLOBAL APP SCRIPTS ==================-->
<script src="{{ url('') }}/admin/js/global/app.js"></script>
<script src="{{ url('') }}/admin/js/components/datatables-init.js"></script>
<!-- ================== PAGE LEVEL SCRIPTS ==================-->
<script src="{{ url('') }}/admin/js/components/countUp-init.js"></script>
<script src="{{ url('') }}/admin/js/cards/counter-group.js"></script>
<script src="{{ url('') }}/admin/js/cards/recent-transactions.js"></script>
<script src="{{ url('') }}/admin/js/cards/monthly-budget.js"></script>
<script src="{{ url('') }}/admin/js/cards/users-chart.js"></script>
<script src="{{ url('') }}/admin/js/cards/bounce-rate-chart.js"></script>
<script src="{{ url('') }}/admin/js/cards/session-duration-chart.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCinqHaojRbM2o13DIpNjZV5OIovI5kyrs&libraries=places"></script>


<script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
          var options = {
			// types: ['(cities)'],
			componentRestrictions: {country: 'GB'}//UK only
			};
            var places = new google.maps.places.Autocomplete(document.getElementById('txtPlaces'),options);
            google.maps.event.addListener(places, 'place_changed', function () {
                var lat = places.getPlace().geometry.location.lat();
                var lng = places.getPlace().geometry.location.lng();
                var address = places.getPlace().formatted_address;
                var name = places.getPlace().name;
                    $('#latitude').val(lat);
                    $('#longitude').val(lng);
                    $('#address').val(address);
                    $('#name').val(name);
                   var place = places.getPlace();
                   for (var i = 0; i < place.address_components.length; i++) {
      for (var j = 0; j < place.address_components[i].types.length; j++) {
        if (place.address_components[i].types[j] == "postal_code") {
          $('#postcode').val(place.address_components[i].long_name);
        }
      }
    }
    
            });
        });

        var str = window.location.href,
    delimiter = '/',
    start = 5,
    tokens = str.split(delimiter, 6);//.slice(start);
    //console.log(tokens);
    URLs = tokens.join(delimiter);

    //console.log(URLs);

  $(".sub-menu").each(function(idx, val) {    
    if($(this).attr('href') == URLs)
    {
      $(this).parent().parent().addClass('in');
      $(this).parent().parent().parent().addClass('active');
      $(this).parent().parent().parent().parent().className;
      $(this).parent().addClass('active'); 
    }
  });
    </script>
</body>
</html>
