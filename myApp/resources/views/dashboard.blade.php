<?php

use App\Models\MstVenue;

?>

@extends('layouts.usermaster')

@section('content')
<?php
@$user = Auth::user();

@$location = \DB::table('user_location')->where('user_id', @$user->id)->first();

?>

<div class="clearfix"></div>
<!--BANNER-SECTION-START-->
<section class="banner-detail">
	<h1 class="hedding-detail">My Games</h1>
</section>
<!--BANNER-SECTION-END-->
<div class="clearfix"></div>
<!--account-setting-section-start-->
<section class="account-setting-content">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 p-0">
				@include('layouts.sidebar')
				<div class="col-sm-8">
				@if ($message = Session::get('success'))
				<div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button>	
						<strong>{{ $message }}</strong>
				</div>
				@endif
				@if ($message = Session::get('error'))
				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button>	
						<strong>{{ $message }}</strong>
				</div>
				@endif
					<div class="welcome-back">
						<h2>Welcome, {{$user->first_name}} {{$user->last_name}} </h2>
						@if ( @$location->location )
						<p><img src="{{url('')}}/images/icons/red-location.png">{{ @$location->location }}</p>
						@endif

					</div>
					<div class="listing-games">
						<!-- <p class="somerset">New games on Let’s Play Sports in the Somerset area! </p> -->
						<p class="somerset">Welcome to letsplaysports </p>
						@if (\Request::getRequestUri() == '/join_games')
							<h3>Joined Games</h3>												
						@endif

						{{-- my join games --}}
						@foreach ($joingames as $joingame)
						<?php

						@$game = \DB::table('mst_games')->where('id', $joingame->game_id)->first();

						if (@$game->game_start_date) {
							$day = date('d', strtotime(@$game->game_start_date));
							$month = date('M', strtotime(@$game->game_start_date));
						} else {
							$day = date('d', strtotime(@$game->game_date));
							$month = date('M', strtotime(@$game->game_date));
						}

						@$location = \DB::table('mst_venue')->where('id', @$game->venue_id)->first();
						$spots = \DB::table('team_spots')->where('game_id', @$game->id)->where('status', 1)->count();

						$followes = \DB::table('venue_followers')->where('venue_id', @$game->venue_id)->count();
						$mefollow = \DB::table('venue_followers')->where('user_id', $user->id)->where('venue_id', @$game->venue_id)->count();
						?>
						@if(@$game)
						@if (@$game->game_start_date < date('Y-m-d')) @if (@$game->user_id!=$user->id)

							<div class="design-list">

								<div class="col-sm-2 col-xs-3 p-0">
									<div class="date-game">
										<h2>{{$day}}<span>{{$month}}</span></h2>
									</div>
								</div>

								<div class="col-sm-7 col-xs-9">
									<div class="info-game">
										<a href="{{ url('') }}/games/{{ @$game->slug }}/{{ @$game->rid }}">
											<p><img src="{{url('')}}/images/icons/tenis-ball.png">

												{{ @$game->title }}</p>
										</a>

										<ul class="time-and-place">
											<li><img src="{{url('')}}/images/icons/watch-icon.png">{{ @$game->game_start_time}} - {{ @$game->game_finish_time}}</li>
											<li title="{{ @$location->name }}"><img src="{{url('')}}/images/icons/location-icon.png">{{ str_limit(@$location->name, $limit = 20, $end = '...') }}</li>
										</ul>
										<div class="follow-venue">
											<ul>
												<li class="price-book">
													@if (@$game->payment==1 || @$game->payment==2)
													@if (@$game->currency=='USD')
													${{ @$game->price }}
													@else
													£{{ @$game->price }}
													@endif
													@else
													Free
													@endif

												</li>
												<li class="number-player">{{ $spots }}/{{ ($game->team_limit) }}</li>

												<li class="follow-button">
													@csrf
													@if ($mefollow > 0)

													<!-- <button id="follow{{ @$game->venue_id }}" follow="Following" vid="{{ @$game->venue_id }}" class="follow setfollow">Following Venue</button> -->
													@else
													<!-- <button id="follow{{ @$game->venue_id }}" follow="Follow" vid="{{ @$game->venue_id }}" class="follow setfollow">Follow Venue</button> -->
													@endif
													<?php

													$venue = MstVenue::where('id', $game->venue_id)->first();
													?>
													<a href="{{ url('') }}/venue/{{ $venue->slug }}">
														<button class="follow setfollow">
															View Sessions
														</button>
													</a>


													<!-- <span>
														<span id="followcount{{ @$game->venue_id }}">{{$followes}}</span> followers</span> -->
												</li>
												<li class="actionBtn">
										<a href="{{ url('startgame') }}/{{ $game->id }}">
										<button class="follow setfollow">Edit</button>
										</a>
                                    <a href="{{ url('gamedelete') }}/{{ $game->id }}" onClick="return confirm('Are you sure you want to Delete?');">
									<button class="follow setfollow">Delete</button>
									</a>
                                       

										</li>

											</ul>
										</div>
										<div class="dis-game">{!! str_limit(@$game->description, $limit = 45, $end = '...') !!} </div>
									</div>
								</div>
								<div class="col-sm-3 col-xs-12 p-0">
									<div class="game-img">
										<a href="{{ url('') }}/games/{{ @$game->slug }}/{{ @$game->rid }}">
											@if (@$game->featured_img)
											<img src="{{url('')}}/upload/images/{{ @$game->featured_img}}">
											@else
											<img src="{{url('')}}/images/no-user-Image.png">
											@endif
										</a>
									</div>
								</div>

							</div>

							@endif
							@endif
							@endif
							@endforeach

							{{-- my added games --}}

							@foreach ($games as $game)
							<?php
							if (@$game->game_start_date) {
								$day = date('d', strtotime(@$game->game_start_date));
								$month = date('M', strtotime(@$game->game_start_date));
							} else {
								$day = date('d', strtotime(@$game->game_date));
								$month = date('M', strtotime(@$game->game_date));
							}
							@$location = \DB::table('mst_venue')->where('id', @$game->venue_id)->first();
							$spots = \DB::table('team_spots')->where('game_id', @$game->id)->where('status', 1)->count();

							$followes = \DB::table('venue_followers')->where('venue_id', @$game->venue_id)->count();
							$mefollow = \DB::table('venue_followers')->where('user_id', $user->id)->where('venue_id', @$game->venue_id)->count();
							?>

							@if (@$game->game_start_date < date('Y-m-d')) <div class="design-list">

								<div class="col-sm-2 p-0">
									<div class="date-game">
										<h2>{{$day}}<span>{{$month}}</span></h2>
									</div>
								</div>
								<div class="col-sm-7">
									<div class="info-game">
										<a href="{{ url('') }}/games/{{ @$game->slug }}/{{ @$game->rid }}">
											<p><img src="{{url('')}}/images/icons/tenis-ball.png">{{ @$game->title }}</p>
										</a>
										<ul class="time-and-place">
											<li><img src="{{url('')}}/images/icons/watch-icon.png">{{ @$game->game_start_time}} - {{ @$game->game_finish_time}}</li>
											<li title="{{ @$location->name }}"><img src="{{url('')}}/images/icons/location-icon.png">{!! str_limit(@$location->name, $limit = 20, $end = '...') !!}</li>
										</ul>
										<div class="follow-venue">
											<ul>
												<li class="price-book">
													@if (@$game->payment==1 || @$game->payment==2)
													@if (@$game->currency=='USD')
													${{ @$game->price }}
													@else
													£{{ @$game->price }}
													@endif
													@else
													Free
													@endif

												</li>
												<li class="number-player">{{ $spots }}/{{ ($game->team_limit ) }}</li>

												<li class="follow-button">
													@csrf
													@if ($mefollow > 0)
													<!-- <button id="follow{{ @$game->venue_id }}" follow="Following" vid="{{ @$game->venue_id }}" class="follow setfollow">Following Venue</button> -->
													@else
													<!-- <button id="follow{{ @$game->venue_id }}" follow="Follow" vid="{{ @$game->venue_id }}" class="follow setfollow">Follow Venue</button> -->
													@endif
													<?php

													$venue = MstVenue::where('id', $game->venue_id)->first();
													?>
													<a href="{{ url('') }}/venue/{{ $venue->slug }}">
														<button class="follow setfollow">
															View Sessions
														</button>
													</a>
													@if ($followes>0)
													<!-- <span><span id="followcount{{ @$game->venue_id }}">{{ $followes }}</span> followers</span> -->
													@endif
												</li>
												<li class="actionBtn">
										<a href="{{ url('startgame/') }}/{{ $game->id }}">
										<button class="follow setfollow">Edit </button>
										</a>
                                    <a href="{{ url('gamedelete') }}/{{ $game->id }}" onClick="return confirm('Are you sure you want to Delete?');">
									<button class="follow setfollow">Delete</button>
									</a>
                                       

										</li>
											</ul>
										</div>
										<div class="dis-game">{!! str_limit(@$game->description, $limit = 45, $end = '...') !!} </div>
									</div>
								</div>
								<div class="col-sm-3 p-0">
									<div class="game-img">
										<a href="{{ url('') }}/games/{{ @$game->slug }}/{{ @$game->rid }}">
											@if (@$game->featured_img)
											<img src="{{url('')}}/upload/images/{{  @$game->featured_img}}">
											@else
											<img src="{{url('')}}/images/no-user-Image.png">
											@endif
										</a>
									</div>
								</div>
					</div>
					@endif
					@endforeach

				</div>


				{{-- upcoming games --}}
				<div class="listing-games">
					@if ($type == 'join_games')
						@if (\Request::getRequestUri() !== '/join_games')
							<h3>Joined Games</h3>												
						@endif
					@else
						<h3>Upcoming Games</h3>
					@endif

					{{-- my join games --}}
					@foreach ($joingames as $joingame)
					<?php
					@$game = \DB::table('mst_games')->where('id', $joingame->game_id)->first();
					if (@$game->game_start_date) {
						$day = date('d', strtotime(@$game->game_start_date));
						$month = date('M', strtotime(@$game->game_start_date));
					} else {
						$day = date('d', strtotime(@$game->game_date));
						$month = date('M', strtotime(@$game->game_date));
					}
					@$location = \DB::table('mst_venue')->where('id', @$game->venue_id)->first();
					$spots = \DB::table('team_spots')->where('game_id', @$game->id)->where('status', 1)->count();

					$followes = \DB::table('venue_followers')->where('venue_id', @$game->venue_id)->count();
					$mefollow = \DB::table('venue_followers')->where('user_id', $user->id)->where('venue_id', @$game->venue_id)->count();
					?>
					@if(@$game)
					@if (@$game->game_start_date >= date('Y-m-d'))
					@if (@$game->user_id!=$user->id)

					<div class="design-list">

						<div class="col-sm-2 p-0">
							<div class="date-game">
								<h2>{{$day}}<span>{{$month}}</span></h2>
							</div>
						</div>
						<div class="col-sm-7">
							<div class="info-game">
								<a href="{{ url('') }}/games/{{ @$game->slug }}/{{ @$game->rid }}">
									<p><img src="{{url('')}}/images/icons/tenis-ball.png">

										{{ @$game->title }}</p>
								</a>
								<ul class="time-and-place">
									<li><img src="{{url('')}}/images/icons/watch-icon.png">{{$game->game_start_time}} - {{$game->game_finish_time}}</li>
									<li title="{{ @$location->name }}"><img src="{{url('')}}/images/icons/location-icon.png">{{ str_limit(@$location->name, $limit = 20, $end = '...') }}</li>
								</ul>
								<div class="follow-venue">
									<ul>
										<li class="price-book">
											@if ($game->payment==1 || $game->payment==2)
											@if ($game->currency=='USD')
											${{ $game->price }}
											@else
											£{{ $game->price }}
											@endif
											@else
											Free
											@endif

										</li>
										<li class="number-player">{{ $spots }}/{{ ($game->team_limit ) }}</li>

										<li class="follow-button">
											@csrf
											@if ($mefollow > 0)
											<!-- <button id="follow{{ $game->venue_id }}" follow="Following" vid="{{ $game->venue_id }}" class="follow setfollow">Following Venue</button> -->
											@else
											<!-- <button id="follow{{ $game->venue_id }}" follow="Follow" vid="{{ $game->venue_id }}" class="follow setfollow">Follow Venue</button> -->
											@endif
											<?php

											$venue = MstVenue::where('id', $game->venue_id)->first();
											?>
											<a href="{{ url('') }}/venue/{{ $venue->slug }}">
												<button class="follow setfollow">
													View Sessions
												</button>
											</a>

											{{-- <span><span id="followcount{{ $game->venue_id }}">{{$followes}}</span> followers</span> --}}
										</li>
										<li class="actionBtn">
										<a href="{{ url('startgame') }}/{{ $game->id }}">
										<button class="follow setfollow">Edit<button>
										</a>
                                    <a href="{{ url('gamedelete') }}/{{ $game->id }}" onClick="return confirm('Are you sure you want to Delete?');">
									<button class="follow setfollow">Delete</button>
									</a>
                                       

										</li>

									</ul>
								</div>
								<div class="dis-game">{!! str_limit($game->description, $limit = 45, $end = '...') !!} </div>
							</div>
						</div>
						<div class="col-sm-3 p-0">
							<div class="game-img">
								<a href="{{ url('') }}/games/{{ $game->slug }}/{{ $game->rid }}">
									@if ($game->featured_img)
									<img src="{{url('')}}/upload/images/{{$game->featured_img}}">
									@else
									<img src="{{url('')}}/images/no-user-Image.png">
									@endif
								</a>
							</div>
						</div>

					</div>
					@endif
					@endif
					@endif
					@endforeach

					{{-- my added games --}}
					@foreach ($games as $game)
					<?php
					if (@$game->game_start_date) {
						$day = date('d', strtotime(@$game->game_start_date));
						$month = date('M', strtotime(@$game->game_start_date));
					} else {
						$day = date('d', strtotime(@$game->game_date));
						$month = date('M', strtotime(@$game->game_date));
					}
					@$location = \DB::table('mst_venue')->where('id', $game->venue_id)->first();
					$spots = \DB::table('team_spots')->where('game_id', $game->id)->where('status', 1)->count();

					$followes = \DB::table('venue_followers')->where('venue_id', $game->venue_id)->count();
					$mefollow = \DB::table('venue_followers')->where('user_id', $user->id)->where('venue_id', $game->venue_id)->count();
					?>
					@if (@$game->game_start_date >= date('Y-m-d'))
					<div class="design-list">

						<div class="col-sm-2 p-0">
							<div class="date-game">
								<h2>{{$day}}<span>{{$month}}</span></h2>
							</div>
						</div>
						<div class="col-sm-7">
							<div class="info-game">
								<a href="{{ url('') }}/games/{{ $game->slug }}/{{ $game->rid }}">
									<p><img src="{{url('')}}/images/icons/tenis-ball.png">{{ $game->title }}</p>
								</a>
								<ul class="time-and-place">
									<li><img src="{{url('')}}/images/icons/watch-icon.png">{{$game->game_start_time}} - {{$game->game_finish_time}}</li>
									<li title="{{ @$location->name }}"><img src="{{url('')}}/images/icons/location-icon.png">{!! str_limit(@$location->name, $limit = 20, $end = '...') !!}</li>
								</ul>
								<div class="follow-venue">
									<ul>
										<li class="price-book">
											@if ($game->payment==1 || $game->payment==2)
											@if ($game->currency=='USD')
											${{ $game->price }}
											@else
											£{{ $game->price }}
											@endif
											@else
											Free
											@endif

										</li>
										<li class="number-player">{{ $spots }}/{{ ($game->team_limit) }}</li>

										<li class="follow-button">
											@csrf
											@if ($mefollow > 0)
											<!-- <button id="follow{{ $game->venue_id }}" follow="Following" vid="{{ $game->venue_id }}" class="follow setfollow">Following Venue</button> -->
											@else
											<!-- <button id="follow{{ $game->venue_id }}" follow="Follow" vid="{{ $game->venue_id }}" class="follow setfollow">Follow Venue</button> -->
											@endif
											<?php

													$venue = MstVenue::where('id', $game->venue_id)->first();
													?>
													<a href="{{ url('') }}/venue/{{ $venue->slug }}">
														<button class="follow setfollow">
															View Sessions
														</button>
													</a>

											{{-- @if ($followes>0)
											<span><span id="followcount{{ $game->venue_id }}">{{ $followes }}</span> followers</span>
											@endif --}}
										</li>
										<li class="actionBtn">
										<a href="{{ url('startgame') }}/{{ $game->id }}">
										<button class="follow setfollow">
										Edit
										</button>
										</a>
                                    <a href="{{ url('gamedelete') }}/{{ $game->id }}" onClick="return confirm('Are you sure you want to Delete?');">
									
									<button class="follow setfollow">
									Delete
									</button>
									</a>
                                       

										</li>
									</ul>
								</div>
								<div class="dis-game">{!! str_limit($game->description, $limit = 45, $end = '...') !!} </div>
							</div>
						</div>
						<div class="col-sm-3 p-0">
							<div class="game-img">
								@if ($game->featured_img)
								<img src="{{url('')}}/upload/images/{{$game->featured_img}}">
								@else
								<img src="{{url('')}}/images/no-user-Image.png">
								@endif

							</div>
						</div>

					</div>
					@endif
					@endforeach

				</div>
				{{-- upcoming games --}}






























			</div>
		</div>
	</div>
	</div>
</section>
<!--account-setting-section-end-->
<script>
	$(document).ready(function() {
		$(document).on('click', '#btn-more', function() {
			var id = $(this).data('id');
			$("#btn-more").html("Loading....");
			$.ajax({
				url: '{{ url("/usergameloadmore") }}',
				method: "POST",
				data: {
					page: id,
					_token: "{{csrf_token()}}",
					search: "{{ @$_GET['search'] }}",
					date: "{{ @$_GET['date'] }}",
					sport: "{{ @$_GET['sport'] }}"
				},
				dataType: "text",
				success: function(data) {
					//console.log('data',data);
					if (data != '') {
						$('#remove-row').remove();
						//$("#btn-more").html("Load More");
						$('#load-data').append(data);
						var page = parseFloat(id) + parseFloat(1);
						$('#btn-more').data('id', page);
					} else {
						//$('#btn-more').html("No Data");
						$('#remove-row').remove();
					}
				}
			});
		});
	});
</script>
<script>
	$(".setfollow").click(function() {
		var follow = $(this).attr('follow');
		var vid = $(this).attr('vid');
		console.log('follow', follow);

		var _token = $('input[name="_token"]').val();
		$.ajax({
			url: "{{ url('') }}/venue/follow",
			method: "POST",
			dataType: 'Json',
			data: {
				follow: follow,
				id: vid,
				_token: _token
			},
			success: function(data) {
				console.log(data);
				$('#follow' + vid).html(data.follow + ' Venue');
				$('#follow' + vid).attr('follow', data.follow);
				$('#followcount' + vid).html(data.followes);

			}
		});

	});
</script>
@endsection