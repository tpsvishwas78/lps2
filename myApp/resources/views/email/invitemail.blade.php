Hi,

You have invited from <b>Let's Play Sports</b>, there are starting an new game for you.

<p>Game Title : {{ $invitedata['gametitle'] }}</p>
<p>Game Url : {{ $invitedata['gameurl'] }}</p>
<p>Game Venue : {{ $invitedata['gamevenue'] }}</p>

Please click the game url and join this game.