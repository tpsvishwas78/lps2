﻿$(document).ready(function(){
$('.carousel[data-type="multi"] .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  for (var i=0;i<2;i++) {
    next=next.next();
    if (!next.length) {
        next = $(this).siblings(':first');
  	}
    
    next.children(':first-child').clone().appendTo($(this));
  }
});

$(".list-unstyled").on("click", ".init", function() {
    $(this).closest(".list-unstyled").children('li:not(.init)').toggle();
});

var allOptions = $(".list-unstyled").children('li:not(.init)');
$(".list-unstyled").on("click", "li:not(.init)", function() {
    allOptions.removeClass('selected');
    $(this).addClass('selected');
    $(".list-unstyled").children('.init').html($(this).html());
    allOptions.toggle();
});
 $(function () {
        $(".switch-button").click(function () {
            if ($(this).is(":checked")) {
                $(".map-sports").show();
            } else {
                $(".map-sports").hide();
            }
        });
    });
 /*detail-crousel*/
 /* from css tricks */
$(function() {
  $("a[href*=#]:not([href=#])").click(function() {
    if (location.pathname.replace(/^\//,"") == this.pathname.replace(/^\//,"") || location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $("[name=" + this.hash.slice(1) +"]");
      if (target.length) {
        $("html,body").animate({
          scrollTop: target.offset().top
        }, 2000);
        return false;
      }
    }
  });
});
/*detail-crousel-end*/

/*custom-dropdown*/

$(".dropdown-area dt a").on('click', function() {
  $(".dropdown-area dd ul").slideToggle('fast');
});

$(".dropdown-area dd ul li a").on('click', function() {
  $(".dropdown-area dd ul").hide();
});

function getSelectedValue(id) {
  return $("#" + id).find("dt a span.value").html();
}

$(document).bind('click', function(e) {
  var $clicked = $(e.target);
  if (!$clicked.parents().hasClass("dropdown-area")) $(".dropdown-area dd ul").hide();
});

$('.mutliSelect input[type="checkbox"]').on('click', function() {

  var title = $(this).closest('.mutliSelect').find('input[type="checkbox"]').val(),
    title = $(this).attr('title') + ",";

  if ($(this).is(':checked')) {
    var html = '<span title="' + title + '">' + title + '</span>';
    $('.multiSel').append(html);
    $(".hida").hide();
  } else {
    $('span[title="' + title + '"]').remove();
    var ret = $(".hida");
    $('.dropdown-area dt a').append(ret);

  }
});
$('#r11').on('click', function(){
  $(this).parent().find('a').trigger('click')
})

$('#r12').on('click', function(){
  $(this).parent().find('a').trigger('click')
})
$('#r13').on('click', function(){
  $(this).parent().find('a').trigger('click')
})
$('#r14').on('click', function(){
  $(this).parent().find('a').trigger('click')
})

});
$(document).ready(function() {
 $('.owl-carousel').owlCarousel({
      itemsDesktop : [5000,4],
      itemsDesktopSmall : [1199,3],
      itemsTablet : [899,2],
      itemsMobile : [599,1],
      navigation : true,
      navigationText : ['<span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-chevron-circle-left fa-stack-1x fa-inverse"></i></span>','<span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-chevron-circle-right fa-stack-1x fa-inverse"></i></span>'],
 

})
});
$(document).ready(function() {

    
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.profile-pic').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    

    $(".file-upload").on('change', function(){
        readURL(this);
    });
    
    $(".upload-button").on('click', function() {
       $(".file-upload").click();
    });
    $('.select-coaching option').each(function () {
  var text = $(this).text();
  if (text.length > 18) {
    text = text.substring(0, 13) + '...';
    $(this).text(text);
  }
});
});
/* ========================================== 
scrollTop() >= 300
Should be equal the the height of the header
========================================== */

$(window).scroll(function(){
    if ($(window).scrollTop() >= 200) {
        $('.header_section').addClass('fixed-header');
    }
    else {
        $('.header_section').removeClass('fixed-header');
    }
});
$(window).scroll(function(){
    if ($(window).scrollTop() >= 200) {
        $('.dashboard-header').addClass('fixed-header');
    }
    else {
        $('.dashboard-header').removeClass('fixed-header');
    }
});

// jQuery(function($) {
 
//     $('.navbar .dropdown').click(function() {
//       $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();

//     }, function() {
//       $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();

//     });

//     $('.navbar .dropdown > a').click(function() {
//       location.href = this.href;
//     });

  
// });



jQuery(function($) {
 
  $('.navbar .dropdown').click(function(e) {
    $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideToggle();
    e.preventDefault();
  }, function() {
    //$(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();

  });

  $('.navbar .dropdown > a').click(function() {
    location.href = this.href;
  });


});