-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 19, 2019 at 09:49 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lps`
--

-- --------------------------------------------------------

--
-- Table structure for table `advertise_session`
--

CREATE TABLE `advertise_session` (
  `id` bigint(127) NOT NULL,
  `user_id` bigint(127) NOT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `venue_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sport_id` int(11) NOT NULL,
  `no_of_player` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_recurring_booking` tinyint(1) NOT NULL,
  `message` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `signature` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `basic_features`
--

CREATE TABLE `basic_features` (
  `id` int(11) NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `basic_features`
--

INSERT INTO `basic_features` (`id`, `title`, `status`) VALUES
(2, 'Your first session is free, just so you know you can trust us to deliver a fantastic session.', 1),
(3, 'Play sports whenever you want, socialize and have fun.', 1),
(5, 'Access to thousands of sports and locations across the UK.', 1),
(6, 'Have your own regular game? No problem create your own listing and find players near you.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `block_booking`
--

CREATE TABLE `block_booking` (
  `id` bigint(127) NOT NULL,
  `user_id` bigint(127) DEFAULT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `venue` int(11) DEFAULT NULL,
  `ideal_days` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sport_id` int(11) DEFAULT NULL,
  `comments` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `children_party`
--

CREATE TABLE `children_party` (
  `id` bigint(127) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `category` int(11) NOT NULL COMMENT '1 venues, 2 coaching, 3 kids',
  `venue_title` varchar(255) DEFAULT NULL,
  `venue_address` varchar(255) DEFAULT NULL,
  `postcode` varchar(100) DEFAULT NULL,
  `latitude` varchar(100) DEFAULT NULL,
  `longitude` varchar(100) DEFAULT NULL,
  `description` mediumtext DEFAULT NULL,
  `short_description` varchar(255) DEFAULT NULL,
  `video` varchar(200) DEFAULT NULL,
  `feature_image` varchar(100) DEFAULT NULL,
  `timing_description` text DEFAULT NULL,
  `opening_hours` text DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `children_party`
--

INSERT INTO `children_party` (`id`, `title`, `slug`, `category`, `venue_title`, `venue_address`, `postcode`, `latitude`, `longitude`, `description`, `short_description`, `video`, `feature_image`, `timing_description`, `opening_hours`, `status`, `created_at`, `updated_at`) VALUES
(6, 'Football Party', 'football-party-6', 1, 'Birmingham Indoor', 'Draperstown, Magherafelt, UK', 'BT45', '54.79351669999999', '-6.78426479999996', '<p>Experience a series of fun games and challenges for 90 minutes, before the children move inside for food and presentations</p>\r\n\r\n<ul>\r\n	<li>90 minutes pitch hire</li>\r\n	<li>30 minutes function hire or reserved bar seating</li>\r\n	<li>FA qualified football coach</li>\r\n	<li>Invitations for all attendees</li>\r\n	<li>Bag and waterbottle for all attendees</li>\r\n	<li>Party food - fresh pizza and unlimited soft drinks</li>\r\n	<li>A trophy for the birthday child</li>\r\n</ul>', 'Experience A Series Of Fun Games And Challenges For 90 Minutes, Before The Children Move Inside For Food And Presentations', '1567843193.mp4', '1567776148.jpg', '<p>Training Date and Time: Tuesdays 4:15-5:30pm | Training Location: Chobham Academy, E15 | Match Day: Sundays (kick offs between 10-2pm)Match Location Home: Weavers Fields, E2 | Match Location Away: Dependant on away team</p>', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>Monday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Tuesday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Wednesdau</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Thursday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Friday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Saturday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Sunday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', 1, '2019-09-06 12:22:28', '2019-09-13 06:36:46'),
(7, 'Champions Experience', 'champions-experience-7', 1, 'Birmingham Indoor', 'Birmingham, UK', '462042', '52.48624299999999', '-1.8904009999999971', '<p>Experience a series of fun games and challenges for 90 minutes, before the children move inside for food and presentations</p>\r\n\r\n<ul>\r\n	<li>90 minutes pitch hire</li>\r\n	<li>30 minutes function hire or reserved bar seating</li>\r\n	<li>FA qualified football coach</li>\r\n	<li>Invitations for all attendees</li>\r\n	<li>Bag and waterbottle for all attendees</li>\r\n	<li>Party food - fresh pizza and unlimited soft drinks</li>\r\n	<li>A trophy for the birthday child</li>\r\n</ul>', 'Experience A Series Of Fun Games And Challenges For 90 Minutes, Before The Children Move Inside For Food And Presentations', '', '1567776486.jpg', '<p>Training Date and Time: Tuesdays 4:15-5:30pm | Training Location: Chobham Academy, E15 | Match Day: Sundays (kick offs between 10-2pm)Match Location Home: Weavers Fields, E2 | Match Location Away: Dependant on away team</p>', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>Monday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Tuesday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Wednesdau</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Thursday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Friday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Saturday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Sunday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', 1, '2019-09-06 12:28:06', '2019-09-13 06:37:00'),
(8, 'Football Party', 'football-party-8', 2, 'Birmingham Indoor', 'London Colney, St Albans, UK', 'AL2', '51.7225824', '-0.2993176999999605', '<p>Experience a series of fun games and challenges for 90 minutes, before the children move inside for food and presentations</p>\r\n\r\n<ul>\r\n	<li>90 minutes pitch hire</li>\r\n	<li>30 minutes function hire or reserved bar seating</li>\r\n	<li>FA qualified football coach</li>\r\n	<li>Invitations for all attendees</li>\r\n	<li>Bag and waterbottle for all attendees</li>\r\n	<li>Party food - fresh pizza and unlimited soft drinks</li>\r\n	<li>A trophy for the birthday child</li>\r\n</ul>', 'Experience A Series Of Fun Games And Challenges For 90 Minutes, Before The Children Move Inside For Food And Presentations', '', '1567776774.jpg', '<p>Training Date and Time: Tuesdays 4:15-5:30pm | Training Location: Chobham Academy, E15 | Match Day: Sundays (kick offs between 10-2pm)Match Location Home: Weavers Fields, E2 | Match Location Away: Dependant on away team</p>', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>Monday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Tuesday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Wednesdau</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Thursday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Friday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Saturday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Sunday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', 1, '2019-09-06 12:32:54', '2019-09-13 06:37:40'),
(9, 'Champions Experience', 'champions-experience-9', 3, 'Birmingham Indoor', 'Andoversford, Cheltenham, UK', 'GL54', '51.8740306', '-1.9674386000000368', '<p>Experience a series of fun games and challenges for 90 minutes, before the children move inside for food and presentations</p>\r\n\r\n<ul>\r\n	<li>90 minutes pitch hire</li>\r\n	<li>30 minutes function hire or reserved bar seating</li>\r\n	<li>FA qualified football coach</li>\r\n	<li>Invitations for all attendees</li>\r\n	<li>Bag and waterbottle for all attendees</li>\r\n	<li>Party food - fresh pizza and unlimited soft drinks</li>\r\n	<li>A trophy for the birthday child</li>\r\n</ul>', 'Experience A Series Of Fun Games And Challenges For 90 Minutes, Before The Children Move Inside For Food And Presentations', '', '1567776857.jpg', '<p>Training Date and Time: Tuesdays 4:15-5:30pm | Training Location: Chobham Academy, E15 | Match Day: Sundays (kick offs between 10-2pm)Match Location Home: Weavers Fields, E2 | Match Location Away: Dependant on away team</p>', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>Monday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Tuesday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Wednesdau</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Thursday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Friday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Saturday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Sunday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', 1, '2019-09-06 12:34:17', '2019-09-13 06:37:59');

-- --------------------------------------------------------

--
-- Table structure for table `children_party_category`
--

CREATE TABLE `children_party_category` (
  `id` bigint(127) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `image` varchar(200) NOT NULL,
  `banner` varchar(100) DEFAULT NULL,
  `layout` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `children_party_category`
--

INSERT INTO `children_party_category` (`id`, `name`, `slug`, `description`, `image`, `banner`, `layout`, `status`, `created_at`, `updated_at`) VALUES
(1, 'SOFT PLAY HIRE', 'soft-play-hire', 'VIEW OUR SOFT PLAY OPTIONS FROM AGES 2 +', '1567754096.jpg', '1568618855.jpg', 1, 1, '2019-08-16 21:20:12', '2019-09-16 01:57:35'),
(3, 'INFLATABLE PARTIES', 'inflatable-parties', 'From Rodeo Bulls to assault courses we have a huge range of inflatables for every occasion', '1567754117.jpg', '1568618839.jpg', 1, 1, '2019-08-17 00:14:26', '2019-09-16 01:57:19'),
(4, 'SPORTS PARTIES', 'sports-parties', 'ORGANSING YOUR CHILDS PARTY DOES NOT NEED TO BE STRESSFUL', '1567753864.jpg', '1568618866.jpg', 2, 1, '2019-09-06 05:13:19', '2019-09-16 01:57:46'),
(5, 'THEMED PARTIES', 'themed-parties', 'From football themed events to disney princess and peppa pig we can tailor make a party to suit you', '1567751287.jpg', '1568618876.jpg', 2, 1, '2019-09-06 05:28:07', '2019-09-16 01:57:56');

-- --------------------------------------------------------

--
-- Table structure for table `common_video`
--

CREATE TABLE `common_video` (
  `id` int(11) NOT NULL,
  `video` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `common_video`
--

INSERT INTO `common_video` (`id`, `video`, `description`, `status`) VALUES
(1, '1566468736.mp4', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Quisque eu libero. Sed sit amet metus. Cras ante nisi, ornare sit amet, pretium eget, tempus suscipit, lectus. Vivamus fermentum est vitae felis. Pellentesque habitant morbi tristique senectus et netus.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `corporate_blogs`
--

CREATE TABLE `corporate_blogs` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `corporate_blogs`
--

INSERT INTO `corporate_blogs` (`id`, `category_id`, `title`, `slug`, `description`, `image`, `created_at`, `updated_at`) VALUES
(2, 2, 'Location', 'location-2', 'We are affiliated with Locations acorss the Uk and host any style tournament (5,6,7,11 a side) for as many people as you require. All you have to do is tell us where you wish the event to take place and the ideal times and we will give you a list of suitable options', '1568361704.jpg', '2019-09-13 08:18:09', '2019-09-13 02:48:09'),
(3, 2, 'Equipment', 'equipment-3', 'Dont want to carry anything to the venue? Dont worry we will have everything ready for you from balls to bibs and any extras you may require for your event.s', '1568361721.jpg', '2019-09-13 08:18:16', '2019-09-13 02:48:16'),
(4, 2, 'Trophies', 'trophies-4', 'Our packs include Trophies for winners and runners up along with medals. You can choose from standard 10\" trophies to large 22\" inch trophies so you can really spoil the winners and there is even a plate for the losers.', '1568361741.jpg', '2019-09-13 08:18:22', '2019-09-13 02:48:22'),
(5, 3, 'Event Management', 'event-management-5', 'Not sure how to run the event and organise games? Well don\'t worry we can post and create fixtures for you and post them online 48 hours before kick off so your teams know exactly what time theyre playing and where. This also adds vital build up to what could be a long awaited tournament. On the day if you need a dedicated event manager to ensure smooth operation alongside referees then fear not, we can get anyone anywhere and make sure that all the bad boys and girls are kept in line. Download our brochure here', '1568361764.png', '2019-09-13 08:18:32', '2019-09-13 02:48:32');

-- --------------------------------------------------------

--
-- Table structure for table `corporate_category`
--

CREATE TABLE `corporate_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `corporate_category`
--

INSERT INTO `corporate_category` (`id`, `name`, `slug`, `image`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Corporate Sports Days', 'corporate-sports-days', '1568356338.jpg', 1, '2019-09-13 06:32:18', '2019-09-13 01:02:18'),
(3, 'Inflatable Fun Days', 'inflatable-fun-days', '1568356396.jpg', 1, '2019-09-13 06:33:16', '2019-09-13 01:03:16'),
(4, 'Its A Knockout Fun Days', 'its-a-knockout-fun-days', '1568356409.jpg', 1, '2019-09-13 06:33:29', '2019-09-13 01:03:29'),
(5, 'Office Summer Party', 'office-summer-party', '1568356417.jpg', 1, '2019-09-13 06:33:37', '2019-09-13 01:03:37'),
(6, 'Office Christmas Party', 'office-christmas-party', '1568356429.jpg', 1, '2019-09-13 06:33:49', '2019-09-13 01:03:49');

-- --------------------------------------------------------

--
-- Table structure for table `corporate_details`
--

CREATE TABLE `corporate_details` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `feature_image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `main_video` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `sec_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sec_description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `layout` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `corporate_details`
--

INSERT INTO `corporate_details` (`id`, `title`, `feature_image`, `banner`, `category_id`, `main_video`, `description`, `sec_title`, `sec_description`, `layout`, `created_at`, `updated_at`) VALUES
(2, 'SOFT PLAY HIRE', '1568295166.jpg', '1568291493.jpg', 2, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/Wz7qmJix1S8\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '<p>dadasds</p>', 'adasdadasdas', '<p>asdasdas</p>', 1, '2019-09-12 13:32:46', '2019-09-16 05:45:02'),
(3, 'sfsdfs', '', '1568291617.jpg', 4, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/Wz7qmJix1S8\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '<p>sfsdf</p>', 'adasdadasdas', '<p>sdfsdf</p>', 2, '2019-09-12 07:03:37', '2019-09-16 05:44:48'),
(5, 'Office Summer Party', '1568295412.jpg', '1568295412.png', 5, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/Wz7qmJix1S8\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '<p>Fun filled, fast paced action for your corporate event. We have a huge range of activities we can include within your sports day package. Please email us on info@letsplaysports.co.uk or call 020 3589 6019 for a quote.</p>', 'Sports Day Challanges', '<p>Not sure how to run the event and organise games? Well don&#39;t worry we can post and create fixtures for you and post them online 48 hours before kick off so your teams know exactly what time theyre playing and where</p>', 2, '2019-09-12 08:06:52', '2019-09-17 01:57:34'),
(6, 'SOFT PLAY HIRE', '1568638640.jpg', '1568638092.jpg', 6, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/Wz7qmJix1S8\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', NULL, 'FAQ Test', '<p>sdadas sfsdass asfas</p>', 2, '2019-09-16 06:05:43', '2019-09-16 07:40:54');

-- --------------------------------------------------------

--
-- Table structure for table `corporate_faq`
--

CREATE TABLE `corporate_faq` (
  `id` int(11) NOT NULL,
  `cp_id` int(11) DEFAULT NULL,
  `title` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `corporate_faq`
--

INSERT INTO `corporate_faq` (`id`, `cp_id`, `title`) VALUES
(11, 6, 'Check the numbers in your party and your preferred dates. Over 18\'s only'),
(12, 6, 'Check the numbers in your party and your preferred dates. Over 18\'s only'),
(13, 6, 'Check the numbers in your party and your preferred dates. Over 18\'s only'),
(14, 6, 'Check the numbers in your party and your preferred dates. Over 18\'s only'),
(15, 6, 'Check the numbers in your party and your preferred dates. Over 18\'s only'),
(16, 6, 'Check the numbers in your party and your preferred dates. Over 18\'s only'),
(17, 6, 'Check the numbers in your party and your preferred dates. Over 18\'s only'),
(18, 6, 'Check the numbers in your party and your preferred dates. Over 18\'s only'),
(19, 6, 'Check the numbers in your party and your preferred dates. Over 18\'s only'),
(20, 6, 'Check the numbers in your party and your preferred dates. Over 18\'s only'),
(21, 6, 'Check the numbers in your party and your preferred dates. Over 18\'s only'),
(22, 6, 'Check the numbers in your party and your preferred dates. Over 18\'s only'),
(23, 5, 'Check the numbers in your party and your preferred dates. Over 18\'s only'),
(24, 5, 'Check the numbers in your party and your preferred dates. Over 18\'s only'),
(25, 5, 'Check the numbers in your party and your preferred dates. Over 18\'s only'),
(26, 5, 'Check the numbers in your party and your preferred dates. Over 18\'s only');

-- --------------------------------------------------------

--
-- Table structure for table `corporate_images`
--

CREATE TABLE `corporate_images` (
  `id` bigint(127) NOT NULL,
  `cp_id` bigint(127) NOT NULL,
  `image` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `corporate_images`
--

INSERT INTO `corporate_images` (`id`, `cp_id`, `image`) VALUES
(1, 1, 'child-page-img-1.jpg'),
(2, 1, 'child-page-img-3.jpg'),
(3, 2, 'child-page-img-1.jpg'),
(4, 2, 'child-page-img-3.jpg'),
(5, 3, 'child-page-img-3.jpg'),
(6, 3, 'detail-video.jpg'),
(7, 4, 'child-page-img-3.jpg'),
(9, 6, 'about-img.jpg'),
(10, 6, 'img1.jpg'),
(11, 6, 'child-page-img-3.jpg'),
(12, 6, 'child-page-img-1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `corporate_videos`
--

CREATE TABLE `corporate_videos` (
  `id` int(11) NOT NULL,
  `cp_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `corporate_videos`
--

INSERT INTO `corporate_videos` (`id`, `cp_id`, `title`, `video`) VALUES
(1, 4, 'sdfsdfsd', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/Wz7qmJix1S8\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(2, 4, 'fsdfdfsdfsdfsdf', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/Wz7qmJix1S8\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>');

-- --------------------------------------------------------

--
-- Table structure for table `cp_images`
--

CREATE TABLE `cp_images` (
  `id` bigint(127) NOT NULL,
  `cp_id` bigint(127) NOT NULL,
  `image` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_images`
--

INSERT INTO `cp_images` (`id`, `cp_id`, `image`) VALUES
(1, 1, 'about-img.jpg'),
(2, 1, 'child-page-img-2.jpg'),
(3, 2, 'about-img.jpg'),
(4, 3, 'about-img.jpg'),
(5, 6, 'about-img.jpg'),
(6, 6, 'child-page-img-2.jpg'),
(7, 6, 'detail-crousel-img.jpg'),
(8, 6, 'detail-video.jpg'),
(9, 7, 'child-page-img-1.jpg'),
(10, 7, 'corporate-banner.jpg'),
(11, 7, 'img3.jpg'),
(12, 7, 'img-list6.png'),
(13, 8, 'detail-video.jpg'),
(14, 8, 'img-list6.png'),
(15, 8, 'oneday-img1.jpg'),
(16, 8, 'oneday-img2.jpg'),
(17, 9, 'oneday-img2.jpg'),
(18, 9, 'vanue-video-img.png'),
(19, 9, 'team-img4.jpg'),
(20, 9, 'team-img1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `cp_venue_facility`
--

CREATE TABLE `cp_venue_facility` (
  `id` bigint(127) NOT NULL,
  `cp_id` bigint(127) NOT NULL,
  `vf_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_venue_facility`
--

INSERT INTO `cp_venue_facility` (`id`, `cp_id`, `vf_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 2, 1),
(7, 2, 2),
(8, 2, 3),
(9, 3, 1),
(10, 3, 2),
(11, 3, 3),
(12, 4, 1),
(13, 4, 2),
(14, 4, 3),
(15, 4, 4),
(16, 4, 5),
(17, 4, 6),
(18, 4, 7),
(19, 5, 1),
(20, 5, 2),
(21, 5, 3),
(22, 5, 4),
(23, 5, 5),
(24, 5, 6),
(25, 5, 7),
(61, 6, 1),
(62, 6, 2),
(63, 6, 3),
(64, 6, 4),
(65, 6, 5),
(66, 6, 6),
(67, 6, 7),
(68, 7, 1),
(69, 7, 2),
(70, 7, 3),
(71, 7, 4),
(72, 7, 5),
(73, 7, 6),
(74, 7, 7),
(75, 8, 1),
(76, 8, 2),
(77, 8, 3),
(78, 8, 4),
(79, 8, 5),
(80, 8, 6),
(81, 8, 7),
(82, 9, 1),
(83, 9, 2),
(84, 9, 3),
(85, 9, 4),
(86, 9, 5),
(87, 9, 6),
(88, 9, 7);

-- --------------------------------------------------------

--
-- Table structure for table `game_comments`
--

CREATE TABLE `game_comments` (
  `id` bigint(127) NOT NULL,
  `user_id` bigint(127) DEFAULT NULL,
  `game_id` bigint(127) NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `comment_id` bigint(127) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `game_comments`
--

INSERT INTO `game_comments` (`id`, `user_id`, `game_id`, `comment`, `name`, `email`, `comment_id`, `created_at`, `updated_at`) VALUES
(1, NULL, 2, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Quisque eu libero. Sed sit amet metus. Cras ante nisi, ornare sit amet, pretium eget, tempus suscipit, lectus. Vivamus fermentum est vitae felis. Pellentesque habitant morbi tristique senectus et netus.', 'shirram chaurasiya', 'shriram@gmail.com', NULL, '2019-08-19 08:30:59', '2019-08-19 02:41:32'),
(2, NULL, 2, 'sdasdsd', 'shirram chaurasiya', 'shriram@gmail.com', NULL, '2019-08-19 08:31:01', '2019-08-19 02:52:17'),
(3, NULL, 2, 'asdasda', 'shirram chaurasiya', 'shriram@gmail.com', NULL, '2019-08-19 08:31:03', '2019-08-19 02:54:43'),
(4, 12, 2, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Quisque eu libero. Sed sit amet metus. Cras ante nisi, ornare sit amet, pretium eget, tempus suscipit, lectus. Vivamus fermentum est vitae felis. Pellentesque habitant morbi tristique senectus et netus.', 'jay kumar', 'ajay@gmail.com', NULL, '2019-08-19 03:00:45', '2019-08-19 03:00:45'),
(5, 12, 2, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Quisque eu libero. Sed sit amet metus. Cras ante nisi, ornare sit amet, pretium eget, tempus suscipit, lectus. Vivamus fermentum est vitae felis. Pellentesque habitant morbi tristique senectus et netus.', 'shirram chaurasiya', 'shriram@gmail.com', NULL, '2019-08-19 04:44:41', '2019-08-19 04:44:41'),
(6, 6, 2, 'Pellentesque habitant morbi tristique senectus et netus.', 'tapas', 'tpsvishwas78@gmail.com', 1, '2019-08-19 04:45:42', '2019-08-19 04:45:42'),
(7, 3, 5, 'asdasd', 'shirram chaurasiya', 'shriram@gmail.com', NULL, '2019-08-30 05:15:04', '2019-08-30 05:15:04'),
(8, 3, 5, 'asd', 'shirram chaurasiya', 'shriram@gmail.com', 7, '2019-08-30 05:16:22', '2019-08-30 05:16:22');

-- --------------------------------------------------------

--
-- Table structure for table `game_images`
--

CREATE TABLE `game_images` (
  `id` bigint(127) NOT NULL,
  `game_id` bigint(127) NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `game_images`
--

INSERT INTO `game_images` (`id`, `game_id`, `image`) VALUES
(2, 3, 'child-page-img-1.jpg'),
(3, 4, 'child-page-img-1.jpg'),
(4, 5, 'child-page-img-1.jpg'),
(5, 6, 'child-page-img-1.jpg'),
(6, 7, 'photo-1540747913346-19e32dc3e97e.jpg'),
(7, 8, 'NETBALL.jpg'),
(8, 8, 'womens netball 2.jpg'),
(9, 8, 'womens netball 3.jpg'),
(10, 8, 'womens netball 4.jpg'),
(11, 9, 'child-page-img-1.jpg'),
(12, 9, 'team-img2.jpg'),
(13, 10, '9829_Fifa-world-cup-2018-Russia-football-field-HD-wallpaper.jpg'),
(14, 10, 'download.jpg'),
(15, 10, 'pexels-photo-399187.jpeg'),
(16, 11, '1.jpg'),
(17, 12, 'womens football 2.jpg'),
(18, 12, 'womens football 3.jpg'),
(20, 13, 'womens football 3.jpg'),
(21, 13, 'womens football 2.jpg'),
(22, 13, 'womens football.jpg'),
(23, 12, 'womens football 2.jpg'),
(24, 14, 'main image.jpg'),
(25, 14, '1567497888.jpg'),
(26, 14, 'womens netball 3.jpg'),
(27, 15, '828265588.jpg'),
(28, 15, 'image.jpg'),
(29, 15, 'Torneo_de_clasificación_WRWC_2014_-_Italia_vs_España_-_18.jpg'),
(30, 16, 'small-img2.jpg'),
(31, 16, 'team-img1.jpg'),
(32, 17, 'child-page-img-3.jpg'),
(33, 17, 'child-page-img-4.jpg'),
(34, 18, 'img-list6.png'),
(35, 18, 'detail-crousel-img.jpg'),
(36, 19, 'img-list6.png'),
(37, 19, 'img1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `game_payment`
--

CREATE TABLE `game_payment` (
  `id` bigint(127) NOT NULL,
  `user_id` bigint(127) DEFAULT NULL,
  `game_author_id` bigint(127) DEFAULT NULL,
  `game_id` bigint(127) DEFAULT NULL,
  `order_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paid_amount` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `game_amount` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vat_amount` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commission_amount` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_status` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_order_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `game_payment`
--

INSERT INTO `game_payment` (`id`, `user_id`, `game_author_id`, `game_id`, `order_code`, `paid_amount`, `game_amount`, `vat_amount`, `commission_amount`, `currency`, `payment_status`, `payment_type`, `customer_order_code`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 7, '49163a24-4b6b-4e5f-b09f-dad987a300fe', '2.4', '2', '0.4', '0.2', 'GBP', 'SUCCESS', 'VISA_CREDIT', '1567412687', '2019-09-02 02:54:48', '2019-09-02 02:54:48'),
(2, 1, 2, 7, '9ead34a4-e40d-4647-97d2-684a723266f7', '2.4', '2', '0.4', '0.2', 'GBP', 'SUCCESS', 'VISA_CREDIT', '1567412777', '2019-09-02 02:56:18', '2019-09-02 02:56:18'),
(3, 1, 2, 7, '4181fd3b-a970-4768-95ec-1cdcca15af5e', '2.4', '2', '0.4', '0.2', 'GBP', 'SUCCESS', 'VISA_CREDIT', '1567412816', '2019-09-02 02:56:57', '2019-09-02 02:56:57'),
(4, 1, 2, 7, '295b99af-d2e6-4e43-a131-f673df1ce939', '2.4', '2', '0.4', '0.2', 'GBP', 'SUCCESS', 'VISA_CREDIT', '1567415977', '2019-09-02 03:49:38', '2019-09-02 03:49:38'),
(5, 1, 2, 7, '716e5788-1a2f-4b4b-8421-e945762a3c7b', '2.4', '2', '0.4', '0.2', 'GBP', 'SUCCESS', 'VISA_CREDIT', '1567416045', '2019-09-02 03:50:46', '2019-09-02 03:50:46'),
(6, 1, 2, 7, '93c7ec3f-6f10-4f9c-a83d-31d85f4c6629', '2.4', '2', '0.4', '0.2', 'GBP', 'SUCCESS', 'VISA_CREDIT', '1567416714', '2019-09-02 04:01:54', '2019-09-02 04:01:54'),
(7, 1, 2, 7, 'a49f24f7-d5f9-41d9-9678-03ade1b99ad4', '2.4', '2', '0.4', '0.2', 'GBP', 'SUCCESS', 'VISA_CREDIT', '1567425851', '2019-09-02 06:34:12', '2019-09-02 06:34:12'),
(8, 2, 1, 8, '8ee79acb-6381-4dfb-9b49-ca8178063b1b', '6', '5', '1', '0.5', 'GBP', 'SUCCESS', 'VISA_CREDIT', '1567499859', '2019-09-03 07:37:39', '2019-09-03 07:37:39'),
(9, 8, 11, 10, '6660a29e-d198-47f9-b43c-07d607638273', '180', '150', '30', '15', 'GBP', 'SUCCESS', 'VISA_CREDIT', '1567681148', '2019-09-05 09:59:08', '2019-09-05 09:59:08');

-- --------------------------------------------------------

--
-- Table structure for table `get_involved`
--

CREATE TABLE `get_involved` (
  `id` bigint(127) NOT NULL,
  `user_id` bigint(127) DEFAULT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `venue` int(11) DEFAULT NULL,
  `available_days` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sport_id` int(11) DEFAULT NULL,
  `comments` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `home_about_us`
--

CREATE TABLE `home_about_us` (
  `id` int(11) NOT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `home_about_us`
--

INSERT INTO `home_about_us` (`id`, `video`, `description`, `status`) VALUES
(1, '1565606547.mp4', 'Letsplaysports has 1 goal and 1 goal only and that is to connect individuals, teams, children, parents with easy access sports wherever they want. Whether you have moved to a new area and looking for a game of rugby to having a group of mates looking to play against another group at a central location. Our aim is to find- connect - play. So what are you waiting for. Lets play sports.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `home_banners`
--

CREATE TABLE `home_banners` (
  `id` int(11) NOT NULL,
  `banner` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_one` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_two` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_three` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `button_link` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '1 image, 2 video',
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `home_banners`
--

INSERT INTO `home_banners` (`id`, `banner`, `title_one`, `title_two`, `title_three`, `button_link`, `banner_type`, `status`, `created_at`, `updated_at`) VALUES
(1, '1567596052.jpg', 'SEARCH AND PLAY TODAY', 'FIND - PLAY - CONNECT', 'YOUR PITCH YOUR GAME', NULL, '1', 1, '2019-09-04 11:20:52', '2019-09-04 10:20:52'),
(3, '1567596062.jpg', 'SEARCH AND PLAY TODAY', 'FIND - PLAY - CONNECT', 'YOUR PITCH YOUR GAME', NULL, '1', 1, '2019-09-04 11:21:02', '2019-09-04 10:21:02'),
(4, '1567596071.jpg', 'SEARCH AND PLAY TODAY', 'FIND - PLAY - CONNECT', 'YOUR PITCH YOUR GAME', NULL, '1', 1, '2019-09-04 11:21:11', '2019-09-04 10:21:11');

-- --------------------------------------------------------

--
-- Table structure for table `invite_friend`
--

CREATE TABLE `invite_friend` (
  `id` bigint(127) NOT NULL,
  `user_id` bigint(127) NOT NULL,
  `invite_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `join_a_league`
--

CREATE TABLE `join_a_league` (
  `id` int(11) NOT NULL,
  `banner_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `join_a_league`
--

INSERT INTO `join_a_league` (`id`, `banner_title`, `banner`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 'JOIN A LEAGUE', '1568877216.jpg', '7-a-side footbal in london', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Quisque eu libero. Sed sit amet metus. Cras ante nisi, ornare sit amet, pretium eget, tempus suscipit, lectus. Vivamus fermentum est vitae felis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas commodo arcu ut odio. Curabitur tempus mi in erat. Praesent molestie magna placerat tellus. Nullam mauris. Nullam vel ipsum quis mi tincidunt vulputate. Cras mattis, lorem id condimentum tincidunt, quam dolor lobortis ligula, tincidunt lacinia ipsum est sit amet ante. Morbi sodales pellentesque neque. Etiam ac odio. In non orci vitae odio tempus facilisis. Pellentesque ut neque. Nullam enim augue, tincidunt eget, fermentum at, luctus nec, tellus. Pellentesque et, imperdiet vitae, mi. Sed nisl velit, viverra non, vehicula vel, dictum non, erat. Sed hendrerit, mauris id fringilla rutrum, pede ligula laoreet nunc, sit amet placerat ipsum risus tincidunt diam. Donec volutpat. Sed hendrerit, mauris id fringilla rutrum, pede ligula laoreet nunc, sit amet placerat ipsum risus tincidunt diam. Donec volutpat. Quisque elementum, tortor in consectetuer ultrices, mi diam sollicitudin nibh, eu scelerisque purus tortor vel elit. Ut suscipit eleifend odio. Proin dignissim mauris ut lacus. Suspendisse in erat. Proin semper facilisis tellus. Vivamus gravida rhoncus massa. ed nisl velit, viverra non, vehicula vel, dictum non, erat. Sed hendrerit, mauris id fringilla rutrum, pede ligula laoreet nunc, sit amet placerat ipsum risus tincidunt diam. Donec volutpat.</p>', '2019-09-19 01:43:36', '2019-09-19 01:43:57');

-- --------------------------------------------------------

--
-- Table structure for table `join_league_block`
--

CREATE TABLE `join_league_block` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `join_league_block`
--

INSERT INTO `join_league_block` (`id`, `title`, `description`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Amazing vanues', 'Superb central location the best playing surfaces competitive. social & fun Divisions to suit all abilities online fixtures. tables & results superior quality mitre & refs', '1568878435.png', '2019-09-19 02:03:55', '2019-09-19 02:03:55'),
(2, 'Flexible & convenient', 'Fixture request and re-arrangement convenient regular invoicing - perfecr for corporate teams top scorer competition & prizes follow the banter on our facebook & twitter pages', '1568878457.jpg', '2019-09-19 02:04:17', '2019-09-19 02:04:17');

-- --------------------------------------------------------

--
-- Table structure for table `kids_blogs`
--

CREATE TABLE `kids_blogs` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kids_blogs`
--

INSERT INTO `kids_blogs` (`id`, `category_id`, `title`, `slug`, `description`, `image`, `created_at`, `updated_at`) VALUES
(2, 4, 'Lorem Ipsum', 'lorem-ipsum', 'sdfsdfsdfsdf ddfsdfsdf', '1568637684.jpg', '2019-09-16 07:11:24', '2019-09-16 07:11:24');

-- --------------------------------------------------------

--
-- Table structure for table `kids_details`
--

CREATE TABLE `kids_details` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secound_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secound_description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kids_details`
--

INSERT INTO `kids_details` (`id`, `category_id`, `title`, `description`, `image`, `secound_title`, `secound_description`, `status`, `created_at`, `updated_at`) VALUES
(3, 5, 'sfsdf', 'sdfsdf', '1568383948.jpg', NULL, NULL, 1, '2019-09-13 08:42:28', '2019-09-13 08:42:28'),
(6, 1, 'Lorem Ipsum', 'sdfsdfsdf', '1568621114.jpg', NULL, NULL, 1, '2019-09-16 02:35:14', '2019-09-16 02:35:14'),
(8, 4, 'test', '<p>saadsdasd</p>', '1568627966.jpg', 'dfdgdfsdfsf', '<p>sdfsdfsdfsdfsdf</p>', 1, '2019-09-16 04:21:58', '2019-09-16 04:29:26');

-- --------------------------------------------------------

--
-- Table structure for table `kids_gallery`
--

CREATE TABLE `kids_gallery` (
  `id` int(11) NOT NULL,
  `kid` int(11) NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kids_gallery`
--

INSERT INTO `kids_gallery` (`id`, `kid`, `image`) VALUES
(1, 7, 'child-page-img-1.jpg'),
(2, 7, 'about-img.jpg'),
(3, 8, 'child-page-img-1.jpg'),
(4, 8, 'about-img.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `kids_whats_included`
--

CREATE TABLE `kids_whats_included` (
  `id` int(11) NOT NULL,
  `kid` int(11) DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kids_whats_included`
--

INSERT INTO `kids_whats_included` (`id`, `kid`, `title`, `description`) VALUES
(1, 8, 'sdfsdf', 'sdfsdf1'),
(2, 8, 'sdf', 'sdfsdf2'),
(3, 8, 'sdfsdfsdf', 'sdfsdf');

-- --------------------------------------------------------

--
-- Table structure for table `looking_venue_players`
--

CREATE TABLE `looking_venue_players` (
  `id` bigint(127) NOT NULL,
  `user_id` bigint(127) NOT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `venue_id` int(11) NOT NULL,
  `is_recurring_booking` tinyint(1) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `slug`, `status`) VALUES
(2, 'PRIVACY POLICY', 'privacy-policy', 1),
(3, 'TERMS & CONDITIONS', 'terms-conditions', 1),
(4, 'ABOUT US', 'about-us', 1);

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` bigint(127) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `receiver_id` bigint(127) NOT NULL,
  `sender_id` bigint(127) NOT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(49, '2014_10_12_000000_create_users_table', 1),
(50, '2014_10_12_100000_create_password_resets_table', 1),
(51, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(52, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(53, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(54, '2016_06_01_000004_create_oauth_clients_table', 1),
(55, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(56, '2019_08_28_105045_create_notifications_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mst_area_intrest`
--

CREATE TABLE `mst_area_intrest` (
  `id` smallint(5) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_area_intrest`
--

INSERT INTO `mst_area_intrest` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Northumberlund', 'asdasda', 1, '2019-08-05 14:20:16', '2019-08-05 08:50:16'),
(3, 'west yorkshire', NULL, 1, '2019-08-05 08:49:14', '2019-08-05 08:49:14'),
(4, 'shropshire', NULL, 1, '2019-08-05 08:49:18', '2019-08-05 08:49:18'),
(5, 'kent', NULL, 1, '2019-08-05 08:49:21', '2019-08-05 08:49:21'),
(6, 'cumbria', NULL, 1, '2019-08-05 08:49:25', '2019-08-05 08:49:25'),
(7, 'south yorks', NULL, 1, '2019-08-05 08:49:28', '2019-08-05 08:49:28'),
(8, 'west midlands', NULL, 1, '2019-08-05 08:49:33', '2019-08-05 08:49:33'),
(9, 'somerset', NULL, 1, '2019-08-05 08:49:36', '2019-08-05 08:49:36'),
(10, 'durham', NULL, 1, '2019-08-05 08:49:40', '2019-08-05 08:49:40'),
(11, 'derbs', NULL, 1, '2019-08-05 08:49:43', '2019-08-05 08:49:43'),
(12, 'worcs warks', NULL, 1, '2019-08-05 08:49:47', '2019-08-05 08:49:47'),
(13, 'oxaon', NULL, 1, '2019-08-05 08:49:50', '2019-08-05 08:49:50'),
(14, 'tyne & wear', NULL, 1, '2019-08-05 08:49:53', '2019-08-05 08:49:53'),
(15, 'staffs', NULL, 1, '2019-08-05 08:49:57', '2019-08-05 08:49:57'),
(16, 'herts', NULL, 1, '2019-08-05 08:50:00', '2019-08-05 08:50:00'),
(17, 'hampshire', NULL, 1, '2019-08-05 08:50:04', '2019-08-05 08:50:04');

-- --------------------------------------------------------

--
-- Table structure for table `mst_games`
--

CREATE TABLE `mst_games` (
  `id` bigint(127) NOT NULL,
  `user_id` bigint(127) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `featured_img` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `game_date` date NOT NULL,
  `game_start_time` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `game_finish_time` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `venue_id` int(11) NOT NULL,
  `sport_id` bigint(127) DEFAULT NULL,
  `is_private` tinyint(1) DEFAULT NULL,
  `team_limit` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `gender` tinyint(3) NOT NULL COMMENT '1 Co-ed, 2 male, 3 female',
  `payment` tinyint(3) NOT NULL COMMENT '1 Online, 2 cash, 3 free',
  `price` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_refund` tinyint(1) DEFAULT NULL,
  `refund_changed_rsvp` tinyint(3) DEFAULT NULL,
  `refund_days` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_admin` int(11) DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_games`
--

INSERT INTO `mst_games` (`id`, `user_id`, `title`, `slug`, `featured_img`, `description`, `game_date`, `game_start_time`, `game_finish_time`, `venue_id`, `sport_id`, `is_private`, `team_limit`, `gender`, `payment`, `price`, `currency`, `is_refund`, `refund_changed_rsvp`, `refund_days`, `is_admin`, `status`, `created_at`, `updated_at`) VALUES
(2, 1, 'Play football whenever you want, socialize and have fun.', 'play-football-whenever-you-want-socialize-and-have-fun', '1567087171.jpg', 'fsdfsdf', '2019-08-29', '9:00 AM', '2:00 PM', 3, 3, NULL, '6', 2, 1, '11', 'GBP', NULL, NULL, NULL, 0, 1, '2019-08-29 08:29:31', '2019-08-29 08:29:31'),
(8, 1, 'Womens Netball Session', 'womens-netball-session', '1567497888.jpg', 'Womens Netball Session every day', '2019-09-30', '1:00 PM', '5:00 PM', 12, 2, NULL, '6', 3, 1, '5', 'GBP', NULL, NULL, NULL, 0, 1, '2019-09-05 13:12:58', '2019-09-05 12:12:58'),
(9, 1, 'Mens Football 5 a side session', 'mens-football-5-a-side-session-9', '1567498124.jpg', 'Mens Football 5 A Side Session', '2019-09-30', '2:00 PM', '6:00 PM', 13, 3, NULL, '5', 2, 1, '5', 'GBP', NULL, NULL, NULL, 0, 1, '2019-09-03 07:08:44', '2019-09-03 07:08:44'),
(11, 12, 'Americano Rugby Club', 'americano-rugby-club-11', '1567844583.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2019-09-24', '9:00 AM', '6:00 PM', 7, 5, NULL, '7', 2, 1, '150', 'GBP', 1, 1, '1', 0, 1, '2019-09-07 07:23:03', '2019-09-07 07:23:03'),
(12, 4, 'London bridge perfect for 5 a side football', 'london-bridge-perfect-for-5-a-side-football', '1568052093.jpg', '<p>A beautiful new 4g pitch in London bridge perfect for 5 a side football.&nbsp; Start time is 8pm and the facility is completely private but there are sample benches and park space locally if you wish to warm up.&nbsp;&nbsp;Balls and bibs are provided so all you need to do is turn up.</p>', '2019-09-30', '8:00 PM', '9:00 PM', 13, 10, NULL, '5', 3, 1, '6', 'GBP', NULL, NULL, NULL, 1, 1, '2019-09-10 07:36:36', '2019-09-10 06:36:36'),
(13, 4, 'Football St Faiths 4g pitch Wandsworth TOWN', 'football-st-faiths-4g-pitch-wandsworth-town', '1568052808.jpg', '<p>Located in the heart of wandsworth town this fantastic 4g pitch is 30 seconds away from wandsworth town station and also accessible from clapham junction. Parking is provided and toilet facilities are on site. The pitch is european 4G with the taller 3m * 2m futsal goals and run offs allowing for some break in play but also for a more technical game.&nbsp;</p>', '2019-09-25', '7:00 PM', '8:00 PM', 11, 10, NULL, '5', 3, 1, '6', 'GBP', NULL, NULL, NULL, 1, 1, '2019-09-10 07:36:22', '2019-09-10 06:36:22'),
(14, 4, 'Netball St Faiths 4g pitch Wandsworth TOWN', 'netball-st-faiths-4g-pitch-wandsworth-town', '1568053163.jpg', '<p>Located in the heart of wandsworth town this fantastic new&nbsp;pitch is 30 seconds away from wandsworth town station and also accessible from clapham junction. Parking is provided and toilet facilities are on site. The pitch is multi sport and allows for a good level of netball to be played. Beginners all welcome as we dont take our selves too seriously and this is meant as a fun and social group. Balls and bibs are provided</p>', '2019-09-26', '7:00 PM', '8:00 PM', 11, 2, NULL, '5', 3, 1, '6', 'GBP', NULL, NULL, NULL, 1, 1, '2019-09-09 17:19:23', '2019-09-09 17:19:23'),
(15, 4, 'Tag Rugby (mixed)', 'tag-rugby-mixed', '1568053546.jpg', '<p>Played on grass our fantastic tag rugby session is a perfect way to stay fit. Played on pitch 5 or 6 in the heart of clapham its easy to access and also on traditional grass. Changing rooms are available although do close at 330 therefore we do advise to come changed. Men and women are allowed.</p>', '2019-09-29', '3:00 PM', '5:00 PM', 14, 5, NULL, '5', 3, 1, '6', 'GBP', NULL, NULL, NULL, 1, 1, '2019-09-09 17:25:46', '2019-09-09 17:25:46'),
(16, 4, 'Football Harris Academy Bermondsey', 'football-harris-academy-bermondsey', '1568053817.jpg', '<p>Played in the heart of Southwark accessible from London bridge, Borough and Bermondsey stations this fantastic 4G pitch hosts our new Friday sessions. Turn up and play with showers and changing facilities all provided. Parking is free after 5pm and we normally play between 5 - 7 a side depending on numbers. Balls and bibs are provided so all you need to do is turn up.</p>', '2019-09-27', '6:00 PM', '8:00 PM', 15, 10, NULL, '5', 2, 1, '5', 'GBP', NULL, NULL, NULL, 1, 1, '2019-09-10 07:36:09', '2019-09-10 06:36:09'),
(17, 4, 'Football Snowsfields School', 'football-snowsfields-school', '1568116364.jpg', '<p>A beautiful new 4g pitch in London bridge perfect for 5 a side football.&nbsp; Start time is 8pm and the facility is completely private but there are ample benches and park space locally if you wish to warm up.&nbsp;&nbsp;Balls and bibs are provided so all you need to do is turn up.</p>', '2019-09-23', '8:00 PM', '9:00 PM', 13, 10, NULL, '5', 2, 1, '6', 'GBP', NULL, NULL, NULL, 1, 1, '2019-09-10 11:52:44', '2019-09-10 10:52:44'),
(18, 4, 'Football St Faiths 4g pitch Wandsworth TOWN', 'football-st-faiths-4g-pitch-wandsworth-town', '1568116429.jpg', '<p>Located in the heart of wandsworth town this fantastic 4g pitch is 30 seconds away from wandsworth town station and also accessible from clapham junction. Parking is provided and toilet facilities are on site. The pitch is european 4G with the taller 3m * 2m futsal goals and run offs allowing for some break in play but also for a more technical game.&nbsp;</p>', '2019-09-25', '8:00 PM', '9:00 PM', 11, 10, NULL, '5', 2, 1, '6', 'GBP', NULL, NULL, NULL, 1, 1, '2019-09-10 11:53:49', '2019-09-10 10:53:49'),
(19, 4, 'Football City of London Academy', 'football-city-of-london-academy', '1568116711.png', '<p>Located a short distance from bermondsey and south bermondsey this is our &quot;corporate league&quot; with referees. The atmosphere is fun and friendly with a competitive edge and for intermediates. The league is for individuals and corporate teams working or living locally so a good chance to meet new people and just have fun and stay fit.</p>', '2019-09-30', '8:00 PM', '10:00 PM', 16, 10, NULL, '5', 2, 1, '5', 'GBP', NULL, NULL, NULL, 1, 1, '2019-09-12 09:58:36', '2019-09-12 04:28:36');

-- --------------------------------------------------------

--
-- Table structure for table `mst_nationality`
--

CREATE TABLE `mst_nationality` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_nationality`
--

INSERT INTO `mst_nationality` (`id`, `name`, `status`) VALUES
(1, 'Indian', 1),
(2, 'England', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mst_notification`
--

CREATE TABLE `mst_notification` (
  `id` bigint(127) NOT NULL,
  `user_to_notify` bigint(127) DEFAULT NULL,
  `user_who_fired_event` bigint(127) DEFAULT NULL,
  `notification_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '1 added game following venue, 2 comment added game, 3 comment my game, ',
  `notification_id` bigint(127) DEFAULT NULL,
  `notification_date` timestamp NULL DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mst_sports`
--

CREATE TABLE `mst_sports` (
  `id` mediumint(5) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sport_order` int(11) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_sports`
--

INSERT INTO `mst_sports` (`id`, `name`, `description`, `image`, `sport_order`, `status`) VALUES
(2, 'Netball', NULL, '1567664599.jpg', 6, 1),
(3, 'American Football', NULL, '1567664830.jpg', 5, 1),
(4, 'Tennis', NULL, '1567664854.jpg', 12, 1),
(5, 'Rugby', NULL, '1567664872.jpg', 10, 1),
(6, 'Hockey', NULL, '1567664884.jpg', 9, 1),
(7, 'Tag Rugby', NULL, '1567664898.jpg', 11, 1),
(8, 'Badminton', NULL, '1567664912.jpg', 13, 1),
(9, 'Ultimate Frisbee', NULL, '1567664939.jpg', 14, 1),
(10, 'Football', NULL, '1567677368.jpg', 1, 1),
(11, '5 a side football', NULL, '1567677422.jpg', 2, 1),
(12, '7 a side football', NULL, '1567677469.jpg', 3, 1),
(13, '11 a side football', NULL, '1567677490.jpg', 4, 1),
(14, 'Cricket', NULL, '1567677651.jpg', 8, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mst_venue`
--

CREATE TABLE `mst_venue` (
  `id` int(11) NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postcode` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_created_by_user` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_venue`
--

INSERT INTO `mst_venue` (`id`, `location`, `image`, `name`, `slug`, `postcode`, `address`, `description`, `latitude`, `longitude`, `is_created_by_user`, `status`, `created_at`, `updated_at`) VALUES
(3, 'Blackpool, UK', '1566297482.jpg', 'Blackpool', 'blackpool-3', '462011', 'Blackpool, UK', 'Paradise Park is a stand alone 7 a side football pitch located in North London. It is between 3 tube stations (Highbury & Islington, Holloway Road, Caledonian Road) so it\'s ideal for people working in Kings Cross area. It’s one of the best spots in the city for day time games with quality football and easy access.\r\n\r\nIf you are looking for a casual kickabout or a competitive league type game you can get both in this pitch. Get your friends together and come to play social 7-a-side football in the heart of Islington. With dozens of football games to play every week, Paradise Park is one of your top football pitches to pick from.\r\n\r\nFacilities\r\n7 a side 3G artificial grass pitch\r\nChanging rooms, showers not available at the venue\r\nStreet Pay & Display parking available\r\nGetting there\r\nWalk: 5-7 minutes from Highbury & Islington, Caledonian Rd., Holloway Rd. stations\r\nUnderground: Highbury & Islington (Victoria Ln, London Overground), Caledonian Rd. (Piccadilly Ln.)\r\nBuses: 153, 43, 263, 271, 393\r\nMore about Footy Addicts\r\nFooty Addicts is a social platform that connects football players in a local area, eliminating the barriers to play the sport we all love. People use Footy Addicts to play football, organise games, socialise, keep fit and have fun!\r\nWe believe that players should have the freedom to play football, anytime, anyplace.', '53.8175053', '-3.035674800000038', NULL, 1, '2019-08-31 11:49:42', '2019-08-31 10:49:42'),
(4, 'Hull, UK', '1565332958.jpg', 'Kingston upon Hull', 'kingston-upon-hull-4', '461001', 'Hull, UK', NULL, '53.76762360000001', '-0.3274198000000297', NULL, 1, '2019-08-31 11:50:54', '2019-08-31 10:50:54'),
(5, 'Inverness, UK', '1565332966.jpg', 'Inverness', 'inverness-5', '462011', 'Inverness, UK', NULL, '57.477773', '-4.224721000000045', NULL, 1, '2019-08-31 11:51:05', '2019-08-31 10:51:05'),
(6, 'London, UK', NULL, 'Haggerston Park', 'haggerston-park-6', '5032', 'London, UK', NULL, '51.5073509', '-0.12775829999998223', NULL, 1, '2019-08-31 11:51:27', '2019-08-31 10:51:27'),
(7, 'Manchester, UK', NULL, 'Manchester', 'manchester-7', '446600', 'Manchester, UK', NULL, '53.4807593', '-2.2426305000000184', NULL, 1, '2019-08-31 11:51:38', '2019-08-31 10:51:38'),
(8, 'Portsmouth, UK', '1565332848.jpg', 'Portsmouth', 'portsmouth-8', '446600', 'Portsmouth, UK', NULL, '50.8197675', '-1.0879769000000579', NULL, 1, '2019-08-31 11:51:50', '2019-08-31 10:51:50'),
(9, 'Glasgow, UK', '', 'Glasgow', 'glasgow-9', '462011', 'Glasgow, UK', NULL, '55.864237', '-4.251805999999988', NULL, 1, '2019-08-31 11:49:52', '2019-08-31 10:49:52'),
(10, 'John o\' Groats, UK', '', 'John o\' Groats', 'john-o-groats-10', '461001', 'John o\' Groats, UK', NULL, '58.6373368', '-3.0688996999999745', NULL, 1, '2019-08-31 11:51:16', '2019-08-31 10:51:16'),
(11, 'Wandsworth, London, UK', '1567372155.jpg', 'St Faiths 4G sports pitch', 'st-faiths-4g-sports-pitch-11', 'SW18 1AE', 'London Borough of Wandsworth, London, UK', 'Fantastic facility suitable for football and netball. Toilets are available and parking on entry. Also a fantastic venue to host childrens parties. Indoor facilities are also available on request.', '51.4570716', '-0.1817823999999746', NULL, 1, '2019-09-01 20:09:15', '2019-09-01 20:09:15'),
(12, 'Somerton, UK', '', 'Somerton', 'somerton-12', 'TA11', 'Somerton TA11, UK', NULL, '51.0551109', '-2.733788000000004', NULL, 1, '2019-09-03 07:03:49', '2019-09-03 07:03:49'),
(13, 'Snodland, UK', '', 'Snowsfield School, London Bridge', 'snowsfield-school-london-bridge-13', 'SE1 3TD', 'Snodland, UK', NULL, '51.3313', '0.445067999999992', NULL, 1, '2019-09-03 07:07:53', '2019-09-03 07:07:53'),
(14, 'Clapham, Lancaster, UK', '', 'Clapham', 'clapham-14', 'LA2 8DP', 'Clapham, Lancaster LA2 8DP, UK', NULL, '54.11773899999999', '-2.3925409999999374', NULL, 1, '2019-09-09 17:23:01', '2019-09-09 17:23:01'),
(15, 'Southwark, UK', '', 'Harris Academy Bermondsey', 'harris-academy-bermondsey-15', 'SE1 3RA', 'Southwark, London SE1 3RA, UK', NULL, '51.502781', '-0.0877379999999448', NULL, 1, '2019-09-09 17:29:14', '2019-09-09 17:29:14'),
(16, 'City of London, Anglia, UK', '', 'City of London Academy', 'city-of-london-academy-16', 'SE1 5LA', 'City of London, UK', NULL, '51.5123443', '-0.09098519999997734', NULL, 1, '2019-09-09 17:36:34', '2019-09-09 17:36:34');

-- --------------------------------------------------------

--
-- Table structure for table `mst_venue_facility`
--

CREATE TABLE `mst_venue_facility` (
  `id` int(10) NOT NULL,
  `facility` varchar(255) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_venue_facility`
--

INSERT INTO `mst_venue_facility` (`id`, `facility`, `icon`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Private Event Space', '1567775832.png', 1, NULL, '2019-09-06 12:17:12'),
(2, 'Free Parking and Wifi', '1567775841.png', 1, NULL, '2019-09-06 12:17:21'),
(3, 'Chaning Rooms', '1567775851.png', 1, NULL, '2019-09-06 12:17:31'),
(4, '3 x 5-A-Side 3G Pitches', '1567775861.png', 1, NULL, '2019-09-06 12:17:41'),
(5, '7-A-Side 3G Pitch', '1567775873.png', 1, NULL, '2019-09-06 12:17:53'),
(6, '4 x Futsal Pitches', '1567775883.png', 1, NULL, '2019-09-06 12:18:03'),
(7, 'Licensed Bar with BT Sport', '1567775893.png', 1, NULL, '2019-09-06 12:18:13');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('1ea7fe7c-b804-45b5-be02-f8ede7229f2e', 'App\\Notifications\\GameNotify', 'App\\User', 1, '{\"game_id\":3,\"user_id\":1,\"message\":\" has left your game\"}', '2019-09-03 07:11:27', '2019-09-03 06:23:57', '2019-09-03 07:11:27'),
('38b87f2e-ba79-4c54-abcc-b88364960755', 'App\\Notifications\\GameNotify', 'App\\User', 2, '{\"game_id\":1,\"user_id\":1,\"message\":\"created a new game on a venue you are following\"}', '2019-08-30 01:59:40', '2019-08-30 01:02:31', '2019-08-30 01:59:40'),
('49fd0945-9402-4c2c-9ea7-a3ca9f40abbe', 'App\\Notifications\\GameNotify', 'App\\User', 2, '{\"game_id\":6,\"user_id\":1,\"message\":\"created a new game on a venue you are following\"}', '2019-08-30 04:58:32', '2019-08-30 04:51:52', '2019-08-30 04:58:32'),
('616d8966-e9d2-4a94-89ed-8ec7e689cf96', 'App\\Notifications\\GameNotify', 'App\\User', 1, '{\"game_id\":8,\"user_id\":2,\"message\":\" has joined your created game\"}', '2019-09-05 12:25:48', '2019-09-03 07:37:52', '2019-09-05 12:25:48'),
('8319323a-d8c4-42d3-ab27-09955468f496', 'App\\Notifications\\GameNotify', 'App\\User', 2, '{\"game_id\":1,\"user_id\":1,\"message\":\"created a new game on a venue you are following\"}', '2019-08-30 02:24:52', '2019-08-29 02:24:20', '2019-08-30 02:24:52'),
('c83d6db5-ce37-4d44-948f-44d7ca17e5bd', 'App\\Notifications\\GameNotify', 'App\\User', 11, '{\"game_id\":10,\"user_id\":8,\"message\":\" has joined your created game\"}', '2019-09-05 10:00:35', '2019-09-05 09:59:19', '2019-09-05 10:00:35'),
('cb0a1f0b-387e-4203-8f5b-2b4efb8281ad', 'App\\Notifications\\GameNotify', 'App\\User', 3, '{\"game_id\":6,\"user_id\":1,\"message\":\"created a new game on a venue you are following\"}', '2019-08-30 04:58:21', '2019-08-30 04:51:58', '2019-08-30 04:58:21'),
('ce17fb5d-d262-4777-8621-40cc3a0bdbd8', 'App\\Notifications\\GameNotify', 'App\\User', 8, '{\"game_id\":10,\"user_id\":8,\"message\":\" You have joined a game \"}', NULL, '2019-09-05 09:59:29', '2019-09-05 09:59:29'),
('ed09d639-3385-4093-bf86-dee1f0bf6fc8', 'App\\Notifications\\GameNotify', 'App\\User', 2, '{\"game_id\":8,\"user_id\":2,\"message\":\" You have joined a game \"}', '2019-09-03 07:40:05', '2019-09-03 07:38:02', '2019-09-03 07:40:05');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('166a5d455d29cad73abec026b5404772a19502dfcd6ca45806a1f88cbd99275c9635618af21f4cf0', 6, 3, 'letesplayapp', '[]', 0, '2019-09-09 06:09:08', '2019-09-09 06:09:08', '2020-09-09 07:09:08'),
('1caf07c2ef675826701c9861379bb0a47824aa652564abcab0b7b8105025a963fce1b9cecaaab24e', 4, 3, 'letesplayapp', '[]', 0, '2019-09-06 04:48:18', '2019-09-06 04:48:18', '2020-09-06 05:48:18'),
('749275a8c1838534e2b0119f99e518e3dc0480cc25e6d6c14202cbc8954329ce254babc6ff122e5f', 6, 3, 'letesplayapp', '[]', 1, '2019-09-07 14:29:25', '2019-09-07 14:29:25', '2020-09-07 15:29:25');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', '8ogU6HNUN4FdPWZKrceeuWjq46zbE5AqhmkF2VbW', 'http://localhost', 1, 0, 0, '2019-08-30 12:17:37', '2019-08-30 12:17:37'),
(2, NULL, 'Laravel Password Grant Client', '0GRb63KbCbpTiYApWq70gyDtoKXcsoqeNbtmVTzl', 'http://localhost', 0, 1, 0, '2019-08-30 12:17:37', '2019-08-30 12:17:37'),
(3, NULL, 'Laravel Personal Access Client', 'qV3AuWpBxJhZkX7daXuuAXaM1Cvsic82GzA5dkmp', 'http://localhost', 1, 0, 0, '2019-08-31 11:23:55', '2019-08-31 11:23:55'),
(4, NULL, 'Laravel Password Grant Client', 'UOtrTqr4dxcZ3xRcDcF24gQXS1z9MXdQKmFKDv98', 'http://localhost', 0, 1, 0, '2019-08-31 11:23:55', '2019-08-31 11:23:55');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-08-30 12:17:37', '2019-08-30 12:17:37'),
(2, 3, '2019-08-31 11:23:55', '2019-08-31 11:23:55');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `name`, `description`, `menu_id`) VALUES
(1, 'PRIVACY POLICY', '<h2><strong>Privacy Policy</strong></h2>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>This Policy applies as between you, the User of this Website and Footy Addicts the owner and provider of this Website. This Policy applies to our use of any and all Data collected by us in relation to your use of the Website.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>\r\n	<p>Definitions and Interpretation</p>\r\n	In this Policy the following terms shall have the following meanings:\r\n\r\n	<p>&nbsp;</p>\r\n\r\n	<p><strong>&ldquo;Data&rdquo;</strong></p>\r\n\r\n	<p>means collectively all information that you submit to the Footy Addicts via the Website. This definition shall, where applicable, incorporate the definitions provided in the Data Protection Act 1998;</p>\r\n	&nbsp;\r\n\r\n	<p><strong>&ldquo;Cookie&rdquo;</strong></p>\r\n\r\n	<p>means a small text file placed on your computer by this Website when you visit certain parts of the Website and/or when you use certain features of the Website. Details of the cookies used by this Website are set out in Clause 12;</p>\r\n	&nbsp;\r\n\r\n	<p><strong>&ldquo;Footy Addicts&rdquo;</strong></p>\r\n\r\n	<p>means Footy Addicts of 120 Baker Street, 3rd Floor, London, United Kingdom, W1U 6TU</p>\r\n	&nbsp;\r\n\r\n	<p><strong>&ldquo;UK and EU Cookie Law&rdquo;</strong></p>\r\n\r\n	<p>means the Privacy and Electronic Communications (EC Directive) Regulations 2003 as amended by the Privacy and Electronic Communications (EC Directive) (Amendment) Regulations 2011;</p>\r\n	&nbsp;\r\n\r\n	<p><strong>&ldquo;User&rdquo;</strong></p>\r\n\r\n	<p>means any third party that accesses the Website and is not employed by Footy Addicts and acting in the course of their employment; and</p>\r\n	&nbsp;\r\n\r\n	<p><strong>&ldquo;Website&rdquo;</strong></p>\r\n\r\n	<p>means the website that you are currently using (www .footyaddicts.com) and any sub-domains of this site (e.g. subdomain www.footyaddicts.net &amp; www.footyaddicts.co.uk.) unless expressly excluded by their own terms and conditions.</p>\r\n	</li>\r\n	<li>\r\n	<p>Scope of this Policy</p>\r\n	This Policy applies only to the actions of Footy Addicts and Users with respect to this Website. It does not extend to any websites that can be accessed from this Website including, but not limited to, any links we may provide to social media websites.</li>\r\n	<li>\r\n	<p>Data Collected</p>\r\n	Without limitation, any of the following Data may be collected by this Website from time to time:\r\n\r\n	<ol>\r\n		<li>name;</li>\r\n		<li>date of birth;</li>\r\n		<li>gender;</li>\r\n		<li>job title;</li>\r\n		<li>profession;</li>\r\n		<li>contact information such as email addresses and telephone numbers;</li>\r\n		<li>demographic information such as post code, preferences and interests;</li>\r\n		<li>financial information such as credit / debit card numbers;</li>\r\n		<li>IP address (automatically collected);</li>\r\n		<li>web browser type and version (automatically collected);</li>\r\n		<li>operating system (automatically collected);</li>\r\n		<li>a list of URLs starting with a referring site, your activity on this Website, and the site you exit to (automatically collected); and</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Our Use of Data</p>\r\n\r\n	<ol>\r\n		<li>Any personal Data you submit will be retained by Footy Addicts for 6 months.</li>\r\n		<li>Unless we are obliged or permitted by law to do so, and subject to Clause 5, your Data will not be disclosed to third parties. This does not include our affiliates and / or other companies within our group.</li>\r\n		<li>All personal Data is stored securely in accordance with the principles of the Data Protection Act 1998. Fore more details on security see Clause 11 below.</li>\r\n		<li>Any or all of the above Data may be required by us from time to time in order to provide you with the best possible service and experience when using our Website. Specifically, Data may be used by us for the following reasons:\r\n		<ol>\r\n			<li>internal record keeping;</li>\r\n			<li>improvement of our products / services;</li>\r\n			<li>transmission by email of promotional materials that may be of interest to you;</li>\r\n			<li>contact for market research purposes which may be done using email, telephone, fax or mail. Such information may be used to customise or update the Website.</li>\r\n		</ol>\r\n		</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Third Party Websites and Services</p>\r\n\r\n	<ol>\r\n		<li>Footy Addicts may, from time to time, employ the services of other parties for dealing with matters that may include, but are not limited to, payment processing, delivery of purchased items, search engine facilities, advertising and marketing. The providers of such services do not have access to certain personal Data provided by Users of this Website.</li>\r\n		<li>Any Data used by such parties is used only to the extent required by them to perform the services that Footy Addicts requests. Any use for other purposes is strictly prohibited. Furthermore, any Data that is processed by third parties shall be processed within the terms of this Policy and in accordance with the Data Protection Act 1998.</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Links to Other Websites</p>\r\n	This Website may, from time to time, provide links to other websites. Footy Addicts has no control over such websites and is in no way responsible for the content thereof. This Policy does not extend to your use of such websites. Users are advised to read the privacy policy or statement of other websites prior to using them.</li>\r\n	<li>\r\n	<p>Changes of Business Ownership and Control</p>\r\n\r\n	<ol>\r\n		<li>Footy Addicts may, from time to time, expand or reduce our business and this may involve the sale and/or the transfer of control of all or part of Footy Addicts. Data provided by Users will, where it is relevant to any part of our business so transferred, be transferred along with that part and the new owner or newly controlling party will, under the terms of this Policy, be permitted to use the Data for the purposes for which it was originally supplied to us.</li>\r\n		<li>In the event that any Data submitted by Users is to be transferred in such a manner, you will not be contacted in advance and informed of the changes. When contacted you not be given the choice to have your Data deleted or withheld from the new owner or controller.</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Controlling Use of Your Data</p>\r\n\r\n	<ol>\r\n		<li>Wherever you are required to submit Data, you will be given options to restrict our use of that Data. This may include the following:\r\n		<ol>\r\n			<li>use of Data for direct marketing purposes; and</li>\r\n			<li>sharing Data with third parties.</li>\r\n		</ol>\r\n		</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Your Right to Withhold Information</p>\r\n\r\n	<ol>\r\n		<li>You may access certain areas of the Website without providing any Data at all. However, to use all features and functions available on the Website you may be required to submit certain Data.</li>\r\n		<li>You may restrict your internet browser&rsquo;s use of Cookies. For more information see Clause 12.</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Accessing your own Data</p>\r\n	You have the right to ask for a copy of any of your personal Data held by Footy Addicts (where such data is held) on payment of a small fee which will not exceed &pound;100.00 GBP.</li>\r\n	<li>\r\n	<p>Security</p>\r\n	Data security is of great importance to Footy Addicts and to protect your Data we have put in place suitable physical, electronic and managerial procedures to safeguard and secure Data collected via this Website.</li>\r\n	<li>\r\n	<p>Cookies</p>\r\n\r\n	<ol>\r\n		<li>This Website may place and access certain first party Cookies on your computer. First party cookies are those placed directly by Footy Addicts via this Website and are used only by Footy Addicts. Footy Addicts uses Cookies to improve your experience of using the Website and to improve our range of products and services. Footy Addicts has carefully chosen these Cookies and has taken steps to ensure that your privacy is protected and respected at all times.</li>\r\n		<li>By using this Website you may receive certain third party Cookies on your computer. Third party cookies are those placed by websites and/or parties other than Footy Addicts for the purposes of tracking the success of their application or customising the application for you. We cannot access these cookies, nor can the third parties access the data in our cookies. For example, when you share an article using a social media sharing button on our site, the social network on which you are sharing will record that you have done this.</li>\r\n		<li>All Cookies used by this Website are used in accordance with current UK and EU Cookie Law.</li>\r\n		<li>Before any Cookies are placed on your computer, subject to sub-Clause 12.5, you will be presented with either a pop-up or a message bar requesting your consent to set those Cookies. By giving your consent to the placing of Cookies you are enabling Footy Addicts to provide the best possible experience and service to you. You may, if you wish, deny consent to the placing of Cookies; however certain features of the Website may not function fully or as intended. You will be given the opportunity to allow only first party Cookies and block third party Cookies.</li>\r\n		<li>Certain features of the Website depend upon Cookies to function. UK and EU Cookie Law deems these Cookies to be &ldquo;strictly necessary&rdquo;. These cookies enable services you have specifically asked for. No consent is required for the use of these cookies. For example, when you subscribe to one of our products or register as a user, we use session cookies to signal that you are logged in so that you are able to use community features of the Website, and persistent cookies to provide automatic login. (need to check this one with my developers) Your consent will not be sought to place these Cookies. You may still block these cookies by changing your internet browser&rsquo;s settings as detailed below.</li>\r\n		<li>You can choose to enable or disable Cookies in your internet browser. Most internet browsers also enable you to choose whether you wish to disable all cookies or only third party cookies. By default, most internet browsers accept Cookies but this can be changed. For further details, please consult the help menu in your internet browser.</li>\r\n		<li>You can choose to delete Cookies at any time however you may lose any information that enables you to access the Website more quickly and efficiently including, but not limited to, personalisation settings.</li>\r\n		<li>It is recommended that you ensure that your internet browser is up-to-date and that you consult the help and guidance provided by the developer of your internet browser if you are unsure about adjusting your privacy settings.</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Changes to this Policy</p>\r\n	Footy Addicts reserves the right to change this Policy as we may deem necessary from time to time or as may be required by law. Any changes will be immediately posted on the Website and you are deemed to have accepted the terms of the Policy on your first use of the Website following the alterations.</li>\r\n</ol>', 2),
(2, 'TERMS & CONDITIONS', '<h2><strong>Terms and Conditions</strong></h2>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>This agreement applies as between you, the User of this Website and Footy Addicts, the owner(s) of this Website. Your agreement to comply with and be bound by these Terms and Conditions is deemed to occur upon your first use of the Website. If you do not agree to be bound by these Terms and Conditions, you should stop using the Website immediately.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Footy Addicts is a social platform (the &ldquo;Platform&rdquo;) that helps football enthusiasts (&ldquo;Users&rdquo;) all around London find people to play with and organise football games.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Footy Addicts brings the football community together. Users can search for games by venue or date, interact with the rest of the group, give feedback &amp; share real time information.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>\r\n	<p>Definitions and Interpretation</p>\r\n	In this Agreement the following terms shall have the following meanings:\r\n\r\n	<p>&nbsp;</p>\r\n\r\n	<p><strong>&ldquo;Content&rdquo;</strong></p>\r\n\r\n	<p>means any text, graphics, images, audio, video, software, data compilations and any other form of information capable of being stored in a computer that appears on or forms part of this Website;</p>\r\n	&nbsp;\r\n\r\n	<p><strong>&ldquo;Footy Addicts&rdquo;</strong></p>\r\n\r\n	<p>Means Footy Addicts Limited trading as Footy Addicts 120 Baker Street, 3rd Floor, London, United Kingdom, W1U 6TU.</p>\r\n	&nbsp;\r\n\r\n	<p><strong>&ldquo;Service&rdquo;</strong></p>\r\n\r\n	<p>means collectively any online facilities, tools, services or information that Footy Addicts makes available through the Website either now or in the future;</p>\r\n	&nbsp;\r\n\r\n	<p><strong>&ldquo;System&rdquo;</strong></p>\r\n\r\n	<p>means any online communications infrastructure that Footy Addicts makes available through the Website either now or in the future. This includes, but is not limited to, web-based email, message boards, live chat facilities and email links;</p>\r\n	&nbsp;\r\n\r\n	<p><strong>&ldquo;User&rdquo; / &ldquo;Users&rdquo;</strong></p>\r\n\r\n	<p>means any third party that accesses the Website and is not employed by Footy Addicts and acting in the course of their employment;</p>\r\n	&nbsp;\r\n\r\n	<p><strong>&ldquo;Website&rdquo; /&ldquo;Platform&rdquo;</strong></p>\r\n\r\n	<p>means the website that you are currently using http://www.footyaddicts.com and any sub-domains of this site including www.footyaddicts.net and www.footyaddicts.co.uk and any other sub-domains unless expressly excluded by their own terms and conditions.</p>\r\n	</li>\r\n	<li>\r\n	<p>Membership</p>\r\n\r\n	<ol>\r\n		<li>By registering to use our Website, you represent and warrant that you are at least 16 years of age.</li>\r\n		<li>When you complete our registration process you will create a password that will enable you to access our Website.</li>\r\n		<li>You are solely responsible for maintaining the confidentiality of your password and agree not to share your password or let anyone else access your account. You will immediately notify Footy Addicts of any unauthorised use of your password or any other security breach relative to your account. You agree that Footy Addicts will not be liable for any loss or damage arising from your failure to comply with this section.</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Restrictions</p>\r\n\r\n	<ol>\r\n		<li>Persons under the age of 16 should not use this Website.</li>\r\n		<li>This site has certain areas that only members of Footy Addicts can access.&nbsp; If you are not a member or if your membership expires or otherwise ends you will not be able to access these areas.</li>\r\n		<li>Members should not allow their membership details to be passed to another person in order to allow them unauthorised access to members&#39; only areas or members&#39; only services available on this website.&nbsp; Members who fail to observe this requirement may place their membership at risk.</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Intellectual Property</p>\r\n\r\n	<ol>\r\n		<li>Subject to the exceptions in Clause 5 of these Terms and Conditions, all Content included on the Website, unless uploaded by Users, including, but not limited to, text, graphics, logos, icons, images, sound clips, video clips, data compilations, page layout, underlying code and software is the property of Footy Addicts, or our affiliates. By continuing to use the Website you acknowledge that such material is protected by applicable United Kingdom and International intellectual property and other laws.</li>\r\n		<li>Subject to Clause 5 of these Terms and Conditions you may print, reproduce, copy, distribute, store or in any other fashion re-use Content from the Website for personal purposes only unless otherwise indicated on the Website or unless given express written permission to do so by Footy Addicts. Personal use includes, but is not limited to, recreational use, social use, and use in education as a student or teacher. Specifically you agree that:\r\n		<ol>\r\n			<li>you will not use the Content of the Website for commercial purposes; and</li>\r\n			<li>you will not systematically copy Content from the Website with a view to creating or compiling any form of comprehensive collection, compilation, directory or database unless given express written permission to do so by Footy Addicts.</li>\r\n		</ol>\r\n		</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Third Party Intellectual Property</p>\r\n	Where expressly indicated, certain Content and the Intellectual Property Rights subsisting therein belongs to other parties. This Content, unless expressly stated to be so, is not covered by any permission granted by Clause 4 of these Terms and Conditions to use Content from the Website. Any such Content will be accompanied by a notice providing the contact details of the owner and any separate use policy that may be relevant.</li>\r\n	<li>\r\n	<p>Fair Use of Intellectual Property</p>\r\n	Material from the Website may be re-used without written permission where any of the exceptions detailed in Chapter III of the Copyright Designs and Patents Act 1988 apply.</li>\r\n	<li>\r\n	<p>Links to Other Websites</p>\r\n\r\n	<p>This Website may contain links to other sites. Unless expressly stated, these sites are not under the control of Footy Addicts or that of our affiliates. We assume no responsibility for the content of such websites and disclaim liability for any and all forms of loss or damage arising out of the use of them. The inclusion of a link to another site on this Website does not imply any endorsement of the sites themselves or of those in control of them.</p>\r\n\r\n	<p>As a member of Footy Addicts, you may link your account with other third party social web services including, without limitation to, Facebook and Twitter, which are subject to their own Terms and Conditions. If you decide to link your account with other third-party social web services, we may receive the personal information you have provided to those third party social web services (including, without limitation to, your name, profile picture, email address and all other information you have chosen to provide). You understand and agree that this personal information will be collected by Footy Addicts and will appear in your player profile.</p>\r\n	</li>\r\n	<li>\r\n	<p>Links to this Website</p>\r\n	Deep-linking to the Site for any purpose, (i.e. including a link to a Footy Addicts webpage other than the Footy Addicts home page) unless expressly authorised in writing by Footy Addicts or for the purpose of promoting your profile or a Group on Footy Addicts via a reputable social networking website (e.g. facebook, twitter, flickr etc) is prohibited. To find out more please contact us by email at&nbsp;info@footyaddicts.com</li>\r\n	<li>\r\n	<p>Use of Communications Facilities and Content Submission</p>\r\n\r\n	<ol>\r\n		<li>You are solely responsible for all information you post to the Website. You acknowledge that your public information may be accessible to the general public not only via the Website but also via third party websites related to Footy Addicts.</li>\r\n		<li>When using the Footy Addicts website and when submitting Content to the Website you should do so in accordance with the following rules:\r\n		<ol>\r\n			<li>you must not use obscene or vulgar language;</li>\r\n			<li>you must not submit Content that is unlawful or otherwise objectionable. This includes, but is not limited to, Content that is abusive, threatening, harassing, defamatory, ageist, sexist or racist;</li>\r\n			<li>you must not submit Content that is intended to promote or incite violence;</li>\r\n			<li>it is advised that posts on message boards, chat facilities or similar and communications with Footy Addicts are made using the English language as we may be unable to respond to enquiries submitted in any other languages;</li>\r\n			<li>content submissions are required to be made using the English language. Content in any other language may be removed at our sole discretion;</li>\r\n			<li>you must not post links to other websites containing any of the above types of Content;</li>\r\n			<li>the means by which you identify yourself must not violate these Terms and Conditions or any applicable laws;</li>\r\n			<li>you must not engage in any form of commercial advertising. This does not prohibit references to businesses for non-promotional purposes including references where advertising may be incidental;</li>\r\n			<li>you must not impersonate other people, particularly employees and representatives of Footy Addicts or our affiliates;</li>\r\n			<li>you must not submit material that may contain viruses or any other software or instructions that may damage or disrupt other software, computer hardware or communications networks; and</li>\r\n			<li>you must not use our System for unauthorised mass-communication such as &ldquo;spam&rdquo; or &ldquo;junk mail&rdquo;.</li>\r\n		</ol>\r\n		</li>\r\n		<li>You acknowledge that Footy Addicts reserves the right to monitor any and all communications made to us or using our System.</li>\r\n		<li>In order to use the Footy Addicts website and any other communication facility that may be added in the future or to submit Content, you are required to submit certain personal details. By continuing to use this Website you represent and warrant that:\r\n		<ol>\r\n			<li>any information you submit is accurate and truthful; and</li>\r\n			<li>you will keep this information accurate and up-to-date.</li>\r\n		</ol>\r\n		</li>\r\n		<li>By submitting Content you warrant and represent that you are the author of such Content or that you have acquired all of the appropriate rights and / or permissions to use the Content in this fashion. Further, you waive all moral rights in the Content to be named as its author and grant Footy Addicts a licence to modify the Content as necessary for its inclusion on the Website. Footy Addicts accepts no responsibility or liability for any infringement of third party rights by such Content.</li>\r\n		<li>Unless a User informs Footy Addicts otherwise, in advance of posting, in writing, and Footy Addicts agrees to any terms or restrictions, all Content submitted is for publication on the Website and other such uses as Footy Addicts may deem appropriate under a royalty-free, perpetual basis.</li>\r\n		<li>Footy Addicts will not be liable in any way or under any circumstances for any loss or damage that you may incur as a result of such Content, nor for any errors or omissions in the Content. Use of and reliance upon such Content is entirely at your own risk.</li>\r\n		<li>Content submitted by Users is not screened by Footy Addicts prior to appearing online. We retain the right to exercise our sole discretion to remove or relocate any Content as we deem appropriate without the consent of the author. We shall be under no obligation to exercise such discretion. If you wish to enquire as to the removal of Content, please submit your query to&nbsp;info@footyaddicts.com.&nbsp;This does not constitute an undertaking to explain our actions.</li>\r\n		<li>You acknowledge that Footy Addicts may retain copies of any and all communications made to us or using our System.</li>\r\n		<li>You acknowledge that any information you send to us through our System or post on the Footy Addicts website may be modified by us in any way and you hereby waive your moral right to be identified as the author of such information. Any restrictions you may wish to place upon our use of such information must be communicated to us in advance and we reserve the right to reject such terms and associated information.</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Termination and Suspension</p>\r\n	In the event that any of the provisions of sub-Clause 9.2, above, are not followed, Footy Addicts reserves the right to suspend or terminate your access to the Service. Any Users banned in this way must not attempt to use the Website under any other name or by using the access credentials of another User, with or without the permission of that User.</li>\r\n	<li>\r\n	<p>Privacy</p>\r\n\r\n	<ol>\r\n		<li>Use of the Website is also governed by our&nbsp;<a href=\"https://footyaddicts.com/pages/privacy\">Privacy Policy</a>&nbsp;which is incorporated into these terms and conditions by this reference. To view the Privacy Policy, please click on the link above.</li>\r\n		<li>The Website places the cookies onto your computer or device. These cookies are used for the purposes described herein. Full details of the cookies used by the Website and your legal rights with respect to them are included in our&nbsp;<a href=\"https://footyaddicts.com/pages/privacy\">Privacy Policy</a></li>\r\n		<li>By accepting these terms and conditions, you are giving consent to Footy Addicts to place cookies on your computer or device. Please read the information contained in the Privacy Policy prior to acceptance.</li>\r\n		<li>If you wish to opt-out of our placing cookies onto your computer or device, please see our&nbsp;<a href=\"https://footyaddicts.com/pages/privacy\">Privacy Policy</a>&nbsp;You may also wish to delete cookies which have already been placed. For instructions on how to do this, please consult your internet browser&rsquo;s help menu.</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Disclaimers</p>\r\n\r\n	<ol>\r\n		<li>Footy Addicts makes no warranty or representation that the Website will meet your requirements, that it will be of satisfactory quality, that it will be fit for a particular purpose, that it will not infringe the rights of third parties, that it will be compatible with all systems, that it will be secure and that all information provided will be accurate. We make no guarantee of any specific results from the use of our Services.</li>\r\n		<li>No part of this Website is intended to constitute advice and the Content of this Website should not be relied upon when making any decisions or taking any action of any kind.</li>\r\n		<li>The information on this Website is not designed with commercial purposes in mind. Commercial use of the Content of this Website is forbidden under sub-Clause 4.2.1 of these Terms and Conditions. Any such use constitutes a breach of these Terms and Conditions and Footy Addicts makes no representation or warranty that this Content is suitable for use in commercial situations or that it constitutes accurate data and / or advice on which business decisions can be based.</li>\r\n		<li>Whilst every effort has been made to ensure that all descriptions of services available from Footy Addicts correspond to the actual services available, Footy Addicts is not responsible for any variations from these descriptions.</li>\r\n		<li>Whilst Footy Addicts uses reasonable endeavours to ensure that the Website is secure and free of errors, viruses and other malware, all Users are advised to take responsibility for their own security, that of their personal details and their computers.</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Availability of the Website and Modifications</p>\r\n\r\n	<ol>\r\n		<li>The Service is provided &ldquo;as is&rdquo; and on an &ldquo;as available&rdquo; basis. We give no warranty that the Service will be free of defects and / or faults. To the maximum extent permitted by the law we provide no warranties (express or implied) of fitness for a particular purpose, accuracy of information, compatibility and satisfactory quality.</li>\r\n		<li>Footy Addicts accepts no liability for any disruption or non-availability of the Website resulting from external causes including, but not limited to, ISP equipment failure, host equipment failure, communications network failure, power failure, natural events, acts of war or legal restrictions and censorship.</li>\r\n		<li>Footy Addicts reserves the right to alter, suspend or discontinue any part (or the whole of) the Website including, but not limited to, the products and/or services available. These Terms and Conditions shall continue to apply to any modified version of the Website unless it is expressly stated otherwise.</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Liability</p>\r\n\r\n	<ol>\r\n		<li>To the maximum extent permitted by law, Footy Addicts accepts no liability for any direct or indirect loss or damage, foreseeable or otherwise, including any indirect, consequential, special or exemplary damages arising from the use of the Website or any information contained therein. Users should be aware that they use the Website and its Content at their own risk.</li>\r\n		<li>Nothing in these Terms and Conditions excludes or restricts Footy Addicts&rsquo; liability for death or personal injury resulting from any negligence or fraud on the part of Footy Addicts.</li>\r\n		<li>Whilst every effort has been made to ensure that these Terms and Conditions adhere strictly with the relevant provisions of the Unfair Contract Terms Act 1977, in the event that any of these terms are found to be unlawful, invalid or otherwise unenforceable, that term is to be deemed severed from these Terms and Conditions and shall not affect the validity and enforceability of the remaining terms and conditions. This term shall apply only within jurisdictions where a particular term is illegal.</li>\r\n		<li>In addition, no advice or information (oral or written) obtained by the User from Footy Addicts shall create any warranty.</li>\r\n		<li>You understand and agree that you download or otherwise obtain material or data through the use of our Website at your own discretion and risk and that you will be solely responsible for any damages to your computer system or loss of data that results from the download of such material or data.</li>\r\n		<li>Your use of our Website is at your sole risk. Our Website is provided to you &quot;as is&quot; and on an &quot;as available&quot; basis. We specifically disclaim all warranties and conditions of any kind, whether express, implied or statutory, including but not limited to the implied warranties of merchantability, fitness for a particular purpose and non-infringement. We disclaim any warranties regarding the security, reliability, timeliness, and performance of our Website. We disclaim any warranties for any information or advice obtained through our Website. We disclaim any warranties for services or goods received through or advertised on our Website or received through any links provided by our Website, as well as for any information or advice received through any links provided through our Website.</li>\r\n		<li>Your correspondence or business dealings with, or participation in promotions of, marketing partners or other third parties found on our Website or through our Website, including payment and delivery of related goods or services, and any other terms, conditions, warranties or representations associated with such dealings, are solely between you and such marketing partner or other third party. You agree that Footy Addicts shall not be responsible or liable for any loss or damage of any sort incurred as the result of any such dealings or as the result of the presence of such marketing partners or other third parties on our Website or located through the use of our Website.</li>\r\n		<li>Footy Addicts provide tools that enable Users to arrange physical meetings or events. Footy Addicts do not supervise these meetings or events and are not involved in any way with the actions of any individuals at these meetings or events. Footy Addicts do not supervise or control the meetings or events organised between Users on the Website.</li>\r\n		<li>Footy Addicts do not endorse the venues or venue owners arranged on the Website between Users. In addition, Footy Addicts do not attempt to confirm, and do not confirm, venue owner&#39;s purported identity. The User is responsible for determining the identity and suitability of others whom you contact via the Website.</li>\r\n		<li>You agree that in no event shall Footy Addicts be liable for any direct, indirect, incidental, special, consequential or exemplary damages, including but not limited to, damages for loss of profits, goodwill, use, data or other intangible losses (even if Footy Addicts has been advised of the possibility of such damages), arising out of or in connection with our Website or this Agreement or the inability to use our Website (however arising, including negligence), arising out of or in connection with Third Party Transactions or arising out of or in connection with your use of our Website or transportation to or from Footy Addicts Gatherings , attendance at Footy Addicts Gatherings, participation in or exclusion from Footy Addicts Groups and the actions of you or others at Footy Addicts Gatherings. Our liability to you or any third parties in any circumstance is limited to the greater of (a) the amount of fees, if any, you pay to us in the twelve (12) months prior to the action giving rise to liability, and (b) &pound;100.</li>\r\n		<li>Some jurisdictions do not allow the exclusion or limitation of certain warranties or of incidental or consequential damages. Accordingly, some of the limitations in this Section 14 may not apply to you.</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Indemnity</p>\r\n	You agree to indemnify and hold us and our officers, directors, shareholders, agents, employees, consultants, affiliates, subsidiaries and third-party partners harmless from any claim or demand, including reasonable legal fees, made by any third party due to or arising out of your breach of your representations and warranties or this Agreement or the documents it incorporates by reference, your use of our Website, Your Information, your violation of any law, statute, ordinance or regulation or the rights of a third party, your participation in a Footy Addicts Group, or your participation as an Organiser or Creator or Host or in Footy Addicts Meetings(whether the claim or demand is due to or arising out of your transportation to or from, attendance at, or the actions of you or other users at Footy Addicts Gatherings). Without limiting the foregoing, you, as an Organiser or Creator or Host, agree to indemnify and hold us and our officers, directors, shareholders, agents, employees, consultants, affiliates, subsidiaries and third-party partners harmless from any claim or demand, including reasonable attorneys&#39; fees, made by any Footy Addicts Group or third party due to or arising out of your actions as an Organiser or Creator or Host, including your use of money paid to you by members of your Footy Addicts Group.</li>\r\n	<li>\r\n	<p>No Waiver</p>\r\n	In the event that any party to these Terms and Conditions fails to exercise any right or remedy contained herein, this shall not be construed as a waiver of that right or remedy.</li>\r\n	<li>\r\n	<p>Previous Terms and Conditions</p>\r\n	In the event of any conflict between these Terms and Conditions and any prior versions thereof, the provisions of these Terms and Conditions shall prevail unless it is expressly stated otherwise.</li>\r\n	<li>\r\n	<p>Third Party Rights</p>\r\n	Nothing in these Terms and Conditions shall confer any rights upon any third party. The agreement created by these Terms and Conditions is between you and Footy Addicts.</li>\r\n	<li>\r\n	<p>Communications</p>\r\n\r\n	<ol>\r\n		<li>All notices / communications shall be given to us either by post to our Premises (see address above) or by email to (email address). Such notice will be deemed received 3 days after posting if sent by first class post, the day of sending if the email is received in full on a business day and on the next business day if the email is sent on a weekend or public holiday.</li>\r\n		<li>Footy Addicts may from time to time send you information about our products and/or services. We may also send you information regarding your account activity and purchases, as well as updates about the Website and Service as well as other promotional offers. You can always opt-out of our promotional e-mails at any time by clicking the unsubscribe link at the bottom of any of such e-mail correspondence; or you can opt-out of any marketing emails by managing your notifications, which is available as part of your Footy Addicts account.</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Law and Jurisdiction</p>\r\n	These Terms and Conditions and the relationship between you and Footy Addicts shall be governed by and construed in accordance with the Law of England and Wales and Footy Addicts and you agree to submit to the exclusive jurisdiction of the Courts of England and Wales.</li>\r\n</ol>', 3),
(3, 'About us', '<h2><strong>About</strong></h2>\r\n\r\n<h3>The fastest, simplest way to share your passion about football</h3>\r\n\r\n<p>Footy Addicts is a social platform that connects football enthusiasts all around Great Britain. People use Footy Addicts to play football, organize games, socialize, keep fit and have fun!</p>\r\n\r\n<p>Footy Addicts believe that players should have the freedom to play football, anytime, anyplace.</p>\r\n\r\n<p>All levels of players are welcome to Footy Addicts,&nbsp;<a href=\"https://footyaddicts.com/users/sign_up\">sign up</a>&nbsp;for free and find the game that suits you. Didn&rsquo;t find one? Why don&rsquo;t you&nbsp;<a href=\"https://footyaddicts.com/football-games/new\">start your own</a>&nbsp;game and invite your friends?</p>\r\n\r\n<p>Footy Addicts develops around the community needs, so your&nbsp;<a href=\"https://footyaddicts.uservoice.com/forums/128329-moving-forward\" target=\"_blank\">feedback</a>&nbsp;is vital.&nbsp;<a href=\"https://footyaddicts.uservoice.com/forums/128329-moving-forward\" target=\"_blank\">Tell us how Footy Addicts can improve</a></p>\r\n\r\n<p><em>Football games, everywhere, anytime, for everybody &hellip;</em></p>\r\n\r\n<h3>Footy Addicts for Players</h3>\r\n\r\n<p>Footy Addicts brings the football community together. Players can&nbsp;<a href=\"https://footyaddicts.com/football-games\">search for games</a>&nbsp;by venue or date, interact with the rest of the group, give feedback &amp; share real time information</p>\r\n\r\n<h3>Footy Addicts for Hosts</h3>\r\n\r\n<p>Footy Addicts makes games organization easy. Easy to set up date, time, venue, number of players. Real time attendance list, so you know who is coming and who is not. Comment before and after the game, asking questions or giving answers. Why don&rsquo;t you give it a go?</p>\r\n\r\n<h3>Footy Addicts &amp; Partnerships</h3>\r\n\r\n<p>We will always look for people who are passionate about football, love the game &amp; share their passion with the community. If you believe that you want to be more active in our community feel free to&nbsp;<a href=\"javascript:void(0)\">Contact us</a></p>\r\n\r\n<p>We do appreciate your free time, so it&rsquo;s up to you how much of it you want to spend.</p>\r\n\r\n<p>Just drop as line and we can figure out what suits you best together.</p>', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('seoisys@gmail.com', '$2y$10$CkUoaRuxCpR612wtOi.Xbua/rf478n7Z7BIfSY0ohvmHA0vSAsTH2', '2019-09-03 10:37:05');

-- --------------------------------------------------------

--
-- Table structure for table `pitch_hire`
--

CREATE TABLE `pitch_hire` (
  `id` bigint(127) NOT NULL,
  `user_id` bigint(127) NOT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `time` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `sport_id` int(11) NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_block_booking` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quick_links_slider`
--

CREATE TABLE `quick_links_slider` (
  `id` int(11) NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `quick_links_slider`
--

INSERT INTO `quick_links_slider` (`id`, `image`, `title`, `description`, `link`, `status`, `created_at`, `updated_at`) VALUES
(2, '1567679964.jpg', 'Join A League', 'Find a 5,6 or 7-a-side football league near you', 'http://localhost/lps/joinleague', 1, '2019-09-18 12:44:28', '2019-09-18 07:14:28'),
(3, '1568033305.jpg', 'Corporate Events', 'Book your Company fun day or tournament', '#', 1, '2019-09-09 12:48:25', '2019-09-09 11:48:25'),
(4, '1567680067.jpg', 'Kids Parties', 'Football and fun, everything a party should be.', '#', 1, '2019-09-05 15:17:30', '2019-09-05 14:17:30'),
(5, '1567602541.jpg', 'She can play', 'Womens only Sessions for all Sports', 'https://webchoice-test.uk/lps/woomengame', 1, '2019-09-09 12:51:49', '2019-09-09 11:51:49'),
(6, '1565684005.jpg', 'SOFT PLAY HIRE', 'View our soft play options from ages 2 +', '#', 1, '2019-09-05 11:50:11', '2019-09-05 10:50:11');

-- --------------------------------------------------------

--
-- Table structure for table `site_setting`
--

CREATE TABLE `site_setting` (
  `id` int(11) NOT NULL,
  `website_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `header_logo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `footer_logo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `favicon_icon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_store_link` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `play_store_link` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_url` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_url` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram_url` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `copyright` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `website_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sendmail_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_setting`
--

INSERT INTO `site_setting` (`id`, `website_name`, `website_title`, `header_logo`, `footer_logo`, `favicon_icon`, `app_store_link`, `play_store_link`, `facebook_url`, `twitter_url`, `instagram_url`, `copyright`, `website_email`, `sendmail_email`, `phone_number`, `city`, `address`, `latitude`, `longitude`) VALUES
(1, 'Let\'s Play Sports', 'Lets Play Sports - Play Football, Netball And Sports Near You', '1565617120.png', '1565617167.png', '1565617167.png', '#', '#', '#', '#', '#', '©2019 Let’s Play Sport. All Right Reserved. <a href=\"https://webdesignchoice.co.uk/\" target=\"_blank\">Web Development </a>by Web Choice', 'info@letsplaysports.co.uk', 'tpsvishwas78@gmail.com', '020 3589 6019', 'London', '24 viadict street,Holborn,EC1A 2BN', '50.945709', '-2.628690');

-- --------------------------------------------------------

--
-- Table structure for table `sportsturnaments_blogs`
--

CREATE TABLE `sportsturnaments_blogs` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sportsturnaments_blogs`
--

INSERT INTO `sportsturnaments_blogs` (`id`, `category_id`, `title`, `slug`, `description`, `image`, `created_at`, `updated_at`) VALUES
(2, 2, 'Location', 'location', 'We are affiliated with Locations acorss the Uk and host any style tournament (5,6,7,11 a side) for as many people as you require. All you have to do is tell us where you wish the event to take place and the ideal times and we will give you a list of suitable options', '1568703096.jpg', '2019-09-17 01:21:36', '2019-09-17 01:21:36');

-- --------------------------------------------------------

--
-- Table structure for table `sportsturnaments_category`
--

CREATE TABLE `sportsturnaments_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sportsturnaments_category`
--

INSERT INTO `sportsturnaments_category` (`id`, `name`, `slug`, `image`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Drapers Field', 'drapers-field', '1568702870.jpg', 1, '2019-09-17 01:17:50', '2019-09-17 01:17:50'),
(3, 'Paradise Park', 'paradise-park', '1568702892.jpg', 1, '2019-09-17 01:18:12', '2019-09-17 01:18:12'),
(4, 'West Way Sports centre', 'west-way-sports-centre', '1568702928.jpg', 1, '2019-09-17 01:18:48', '2019-09-17 01:18:48'),
(5, 'Rocks Lane Chiswick', 'rocks-lane-chiswick', '1568702949.jpg', 1, '2019-09-17 01:19:09', '2019-09-17 01:19:09'),
(6, 'Archbishop Park', 'archbishop-park', '1568702988.png', 1, '2019-09-17 01:19:48', '2019-09-17 01:19:48'),
(7, 'Drapers Field', 'drapers-field', '1568703016.jpg', 1, '2019-09-17 01:20:16', '2019-09-17 01:20:16');

-- --------------------------------------------------------

--
-- Table structure for table `sportsturnaments_details`
--

CREATE TABLE `sportsturnaments_details` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `feature_image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `main_video` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `sec_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sec_description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `layout` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sportsturnaments_details`
--

INSERT INTO `sportsturnaments_details` (`id`, `title`, `feature_image`, `banner`, `category_id`, `main_video`, `description`, `sec_title`, `sec_description`, `layout`, `created_at`, `updated_at`) VALUES
(1, 'Drapers Field', '1568705336.jpg', '1568705037.jpg', 2, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/Wz7qmJix1S8\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '<p>adasdadasdas</p>', 'sdfsdfsdf', '<p>sdfsdfsdfsdf</p>', 1, '2019-09-17 01:23:42', '2019-09-17 02:06:41'),
(2, 'Paradise Park', '1568705355.jpg', '1568705077.jpg', 3, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/Wz7qmJix1S8\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', NULL, 'sfsdf', '<p>sdfsdf</p>', 2, '2019-09-17 01:24:58', '2019-09-17 01:59:15');

-- --------------------------------------------------------

--
-- Table structure for table `sportsturnaments_faq`
--

CREATE TABLE `sportsturnaments_faq` (
  `id` int(11) NOT NULL,
  `cp_id` int(11) DEFAULT NULL,
  `title` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sportsturnaments_faq`
--

INSERT INTO `sportsturnaments_faq` (`id`, `cp_id`, `title`) VALUES
(5, 2, 'Check the numbers in your party and your preferred dates. Over 18\'s only'),
(6, 2, 'Check the numbers in your party and your preferred dates. Over 18\'s only'),
(7, 2, 'Check the numbers in your party and your preferred dates. Over 18\'s only'),
(8, 2, 'Check the numbers in your party and your preferred dates. Over 18\'s only');

-- --------------------------------------------------------

--
-- Table structure for table `sportsturnaments_images`
--

CREATE TABLE `sportsturnaments_images` (
  `id` bigint(127) NOT NULL,
  `cp_id` bigint(127) NOT NULL,
  `image` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sportsturnaments_images`
--

INSERT INTO `sportsturnaments_images` (`id`, `cp_id`, `image`) VALUES
(1, 1, 'about-img.jpg'),
(2, 1, 'corporate-banner.jpg'),
(3, 2, 'about-img.jpg'),
(4, 2, 'detail-video.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `sportsturnaments_videos`
--

CREATE TABLE `sportsturnaments_videos` (
  `id` int(11) NOT NULL,
  `cp_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sportsturnaments_videos`
--

INSERT INTO `sportsturnaments_videos` (`id`, `cp_id`, `title`, `video`) VALUES
(1, 1, 'asda', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/Wz7qmJix1S8\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(2, 1, 'asd', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/Wz7qmJix1S8\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(3, 1, 'asd', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/Wz7qmJix1S8\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>');

-- --------------------------------------------------------

--
-- Table structure for table `team_spots`
--

CREATE TABLE `team_spots` (
  `id` bigint(127) NOT NULL,
  `game_id` bigint(127) NOT NULL,
  `user_id` bigint(127) NOT NULL,
  `team` int(11) DEFAULT NULL COMMENT '1 Light Tees, 2 Dark Tees',
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `team_spots`
--

INSERT INTO `team_spots` (`id`, `game_id`, `user_id`, `team`, `status`) VALUES
(3, 3, 1, 1, 0),
(4, 4, 1, 1, 1),
(5, 5, 1, 1, 1),
(7, 5, 3, 2, 0),
(8, 7, 8, 1, 1),
(9, 7, 1, 2, 1),
(10, 8, 1, 1, 1),
(11, 9, 1, 1, 1),
(12, 8, 2, 2, 1),
(13, 10, 11, 1, 1),
(14, 10, 8, 2, 1),
(15, 11, 12, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `dob` date DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_img` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `gender` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1 male, 2 female',
  `nationality` smallint(6) NOT NULL DEFAULT 0,
  `facebook_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `nf_comment` tinyint(4) NOT NULL DEFAULT 0,
  `nf_game` tinyint(4) NOT NULL DEFAULT 0,
  `nf_attendance` tinyint(4) NOT NULL DEFAULT 0,
  `nf_newsletter` tinyint(4) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `user_role` tinyint(4) NOT NULL COMMENT '1 Admin, 2 User, 3 Company, 4 Advertiser',
  `is_cancel` tinyint(4) NOT NULL DEFAULT 0,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activation_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `api_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `first_name`, `last_name`, `phone_number`, `username`, `dob`, `address`, `profile_img`, `gender`, `nationality`, `facebook_id`, `nf_comment`, `nf_game`, `nf_attendance`, `nf_newsletter`, `status`, `user_role`, `is_cancel`, `active`, `remember_token`, `activation_token`, `api_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'null', 'tpsvishwas78@gmail.com', '2019-08-28 18:30:00', '$2y$10$E7O4mxVnhr0B5iw9XVu66uBtgfiatzGcgfgjNZx6o1iK0G01q1.fK', 'Cris', 'Moires', '12345678900', 'tapas-vishwas', NULL, NULL, 'null', 0, 0, 'null', 0, 0, 0, 0, 0, 2, 0, 0, NULL, 'null', 'null', '2019-08-29 08:02:14', '2019-08-30 18:30:51', NULL),
(2, 'null', 'tapas@agnitotechnologies.com', '2019-08-28 18:30:00', '$2y$10$epcG4Sdkw6i3K/vnT8HF0uojJH2njXkxjAjR3FjTTwIGDB0Qoo.oK', 'Neerav', 'Kumar', '12345678900', 'neerav-kumar', NULL, NULL, 'null', 0, 0, 'null', 0, 0, 0, 0, 0, 2, 0, 0, NULL, 'null', 'null', '2019-08-29 08:05:43', '2019-08-29 08:05:43', NULL),
(3, 'null', 'shriram@gmail.com', '2019-08-29 18:30:00', '$2y$10$epcG4Sdkw6i3K/vnT8HF0uojJH2njXkxjAjR3FjTTwIGDB0Qoo.oK', 'shirram', 'chaurasiya', '12345678900', 'shirram-chaurasiya', NULL, NULL, 'null', 0, 0, 'null', 0, 0, 0, 0, 0, 2, 0, 0, NULL, 'null', 'null', '2019-08-30 03:00:34', '2019-08-30 03:00:34', NULL),
(4, 'admin', 'admin@gmail.com', NULL, '$2y$10$HjzNoo9w6q0JoA/TddZ7neXoTAckgfVN4W3WJOgxhajYVRFtW5rhO', 'admin', 'admin', '12345678900', 'admin', NULL, 'Test', '1564827952.png', 0, 0, 'null', 0, 0, 0, 0, 1, 1, 0, 1, NULL, 'null', 'null', NULL, '2019-09-06 05:16:39', NULL),
(6, 'null', 'Johndoe@gmail.com', '2019-08-30 23:00:00', '$2y$10$9Dr7Wxv93UUhstBGO427iuMVGc0Z7glvQySUoaFIrk9YV0L9XkWRa', 'John', 'doe', '12345678900', 'john-doe-5', NULL, NULL, 'null', 0, 0, 'null', 1, 1, 1, 1, 1, 2, 0, 1, NULL, 'null', 'null', '2019-08-31 06:29:50', '2019-09-09 06:15:19', NULL),
(7, 'null', 'siddharthkekre@gmail.com', '2019-08-30 23:00:00', '$2y$10$kOSnL9Tr.l02yU3MZsRdYuThTHmKyr6oBP1Af8UKpB4TaQvkh8j5a', 'John', 'Doe', '12345678987', 'john-doe', NULL, NULL, 'null', 0, 0, 'null', 0, 0, 0, 0, 0, 2, 0, 0, NULL, 'null', 'null', '2019-08-31 06:35:12', '2019-08-31 10:46:48', NULL),
(8, 'null', 'fati@simplemail.top', '2019-08-31 06:45:53', '$2y$10$B9QvA/BH1UpKxGJutfaAuOKwddH2C/VuQm68ci7JxC.BtZOvrr8Pm', 'Alex', 'Martin', '14151658789', 'alex-martin', '1989-05-18', NULL, '1567237943.png', 1, 2, 'null', 0, 0, 0, 0, 0, 2, 0, 0, NULL, 'null', 'null', '2019-08-31 06:41:19', '2019-08-31 07:34:54', NULL),
(9, 'null', 'seoisys@gmail.com', '2019-09-02 19:52:59', '$2y$10$pdoqCrpNDXFJIXmZMnrPXutC7Quc2TwX0UqkmA4oCLJLr7gL//nQS', 'Bobby', 'Grosse', '98930155211', 'bobby-grosse', NULL, NULL, 'null', 0, 0, 'null', 0, 0, 0, 0, 0, 2, 0, 0, NULL, 'null', 'null', '2019-09-02 19:52:21', '2019-09-02 19:52:59', NULL),
(10, 'null', 'varaye@simplemail.top', '2019-09-03 06:53:48', '$2y$10$OnyFV6y.AGjoOTGx.2nOq.DOm/OrA6tG9RnUceePa8.yI9EZNMayG', 'John', 'Jacobs', '48579685215', 'siddharthkekre', NULL, NULL, 'null', 0, 0, 'null', 0, 0, 0, 0, 0, 2, 0, 0, NULL, 'null', 'null', '2019-09-03 06:50:20', '2019-09-03 06:54:52', NULL),
(11, 'null', 'carljohnson@click-mail.top', '2019-09-05 09:35:54', '$2y$10$.NESr2LzUsrHTCfyYxcCxe9kZ2NNhASoGBmMipmZFXDEck2gZxDFe', 'Carl', 'Johnson', '78549685214', 'carl-johnson', NULL, NULL, 'null', 0, 0, 'null', 0, 0, 0, 0, 0, 2, 0, 0, NULL, 'null', 'null', '2019-09-05 09:34:50', '2019-09-05 09:48:07', NULL),
(12, 'null', 'mowe@mail-search.com', '2019-09-07 06:06:28', '$2y$10$v95aDd3Ce1TLNqimRbTAc.iDCKQYCv7SfODZK77pTMlVatqxCTIgm', 'Emma', 'Jacob', '78965894587', 'emma-jacob', NULL, NULL, '1567844757.jpg', 0, 0, 'null', 0, 0, 0, 0, 0, 2, 0, 0, NULL, 'null', 'null', '2019-09-07 06:01:59', '2019-09-07 07:25:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_area_intrest`
--

CREATE TABLE `user_area_intrest` (
  `id` bigint(127) NOT NULL,
  `user_id` bigint(127) NOT NULL,
  `area_intrest_id` smallint(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_area_intrest`
--

INSERT INTO `user_area_intrest` (`id`, `user_id`, `area_intrest_id`) VALUES
(9, 8, 2),
(10, 8, 3),
(11, 8, 4),
(12, 8, 7);

-- --------------------------------------------------------

--
-- Table structure for table `user_location`
--

CREATE TABLE `user_location` (
  `id` bigint(127) NOT NULL,
  `user_id` bigint(127) NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_location`
--

INSERT INTO `user_location` (`id`, `user_id`, `location`, `address`, `latitude`, `longitude`) VALUES
(1, 2, 'London, UK', 'London, UK', '51.5073509', '-0.12775829999998223'),
(2, 12, 'Manchester, UK', 'Manchester, UK', '53.4807593', '-2.2426305000000184');

-- --------------------------------------------------------

--
-- Table structure for table `user_sports`
--

CREATE TABLE `user_sports` (
  `id` bigint(11) NOT NULL,
  `user_id` bigint(11) NOT NULL,
  `sport_id` smallint(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_sports`
--

INSERT INTO `user_sports` (`id`, `user_id`, `sport_id`) VALUES
(3, 8, 5);

-- --------------------------------------------------------

--
-- Table structure for table `venue_followers`
--

CREATE TABLE `venue_followers` (
  `id` bigint(127) NOT NULL,
  `user_id` bigint(127) NOT NULL,
  `venue_id` bigint(127) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `venue_followers`
--

INSERT INTO `venue_followers` (`id`, `user_id`, `venue_id`) VALUES
(3, 2, 3),
(4, 3, 3),
(5, 11, 6);

-- --------------------------------------------------------

--
-- Table structure for table `your_booking`
--

CREATE TABLE `your_booking` (
  `id` bigint(127) NOT NULL,
  `user_id` bigint(127) NOT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `venue_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `recurring_booking` tinyint(1) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `advertise_session`
--
ALTER TABLE `advertise_session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `basic_features`
--
ALTER TABLE `basic_features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `block_booking`
--
ALTER TABLE `block_booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `children_party`
--
ALTER TABLE `children_party`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `children_party_category`
--
ALTER TABLE `children_party_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `common_video`
--
ALTER TABLE `common_video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `corporate_blogs`
--
ALTER TABLE `corporate_blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `corporate_category`
--
ALTER TABLE `corporate_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `corporate_details`
--
ALTER TABLE `corporate_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `corporate_faq`
--
ALTER TABLE `corporate_faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `corporate_images`
--
ALTER TABLE `corporate_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `corporate_videos`
--
ALTER TABLE `corporate_videos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_images`
--
ALTER TABLE `cp_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_venue_facility`
--
ALTER TABLE `cp_venue_facility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `game_comments`
--
ALTER TABLE `game_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `game_images`
--
ALTER TABLE `game_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `game_payment`
--
ALTER TABLE `game_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `get_involved`
--
ALTER TABLE `get_involved`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_about_us`
--
ALTER TABLE `home_about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_banners`
--
ALTER TABLE `home_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invite_friend`
--
ALTER TABLE `invite_friend`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `join_a_league`
--
ALTER TABLE `join_a_league`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `join_league_block`
--
ALTER TABLE `join_league_block`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kids_blogs`
--
ALTER TABLE `kids_blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kids_details`
--
ALTER TABLE `kids_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kids_gallery`
--
ALTER TABLE `kids_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kids_whats_included`
--
ALTER TABLE `kids_whats_included`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `looking_venue_players`
--
ALTER TABLE `looking_venue_players`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_area_intrest`
--
ALTER TABLE `mst_area_intrest`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_games`
--
ALTER TABLE `mst_games`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_nationality`
--
ALTER TABLE `mst_nationality`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_notification`
--
ALTER TABLE `mst_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_sports`
--
ALTER TABLE `mst_sports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_venue`
--
ALTER TABLE `mst_venue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_venue_facility`
--
ALTER TABLE `mst_venue_facility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pitch_hire`
--
ALTER TABLE `pitch_hire`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quick_links_slider`
--
ALTER TABLE `quick_links_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_setting`
--
ALTER TABLE `site_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sportsturnaments_blogs`
--
ALTER TABLE `sportsturnaments_blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sportsturnaments_category`
--
ALTER TABLE `sportsturnaments_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sportsturnaments_details`
--
ALTER TABLE `sportsturnaments_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sportsturnaments_faq`
--
ALTER TABLE `sportsturnaments_faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sportsturnaments_images`
--
ALTER TABLE `sportsturnaments_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sportsturnaments_videos`
--
ALTER TABLE `sportsturnaments_videos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team_spots`
--
ALTER TABLE `team_spots`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_area_intrest`
--
ALTER TABLE `user_area_intrest`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_location`
--
ALTER TABLE `user_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_sports`
--
ALTER TABLE `user_sports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `venue_followers`
--
ALTER TABLE `venue_followers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `your_booking`
--
ALTER TABLE `your_booking`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `advertise_session`
--
ALTER TABLE `advertise_session`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `basic_features`
--
ALTER TABLE `basic_features`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `block_booking`
--
ALTER TABLE `block_booking`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `children_party`
--
ALTER TABLE `children_party`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `children_party_category`
--
ALTER TABLE `children_party_category`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `common_video`
--
ALTER TABLE `common_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `corporate_blogs`
--
ALTER TABLE `corporate_blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `corporate_category`
--
ALTER TABLE `corporate_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `corporate_details`
--
ALTER TABLE `corporate_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `corporate_faq`
--
ALTER TABLE `corporate_faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `corporate_images`
--
ALTER TABLE `corporate_images`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `corporate_videos`
--
ALTER TABLE `corporate_videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cp_images`
--
ALTER TABLE `cp_images`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `cp_venue_facility`
--
ALTER TABLE `cp_venue_facility`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `game_comments`
--
ALTER TABLE `game_comments`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `game_images`
--
ALTER TABLE `game_images`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `game_payment`
--
ALTER TABLE `game_payment`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `get_involved`
--
ALTER TABLE `get_involved`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `home_about_us`
--
ALTER TABLE `home_about_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `home_banners`
--
ALTER TABLE `home_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `invite_friend`
--
ALTER TABLE `invite_friend`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `join_a_league`
--
ALTER TABLE `join_a_league`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `join_league_block`
--
ALTER TABLE `join_league_block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `kids_blogs`
--
ALTER TABLE `kids_blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kids_details`
--
ALTER TABLE `kids_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `kids_gallery`
--
ALTER TABLE `kids_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kids_whats_included`
--
ALTER TABLE `kids_whats_included`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `looking_venue_players`
--
ALTER TABLE `looking_venue_players`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `mst_area_intrest`
--
ALTER TABLE `mst_area_intrest`
  MODIFY `id` smallint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `mst_games`
--
ALTER TABLE `mst_games`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `mst_nationality`
--
ALTER TABLE `mst_nationality`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mst_notification`
--
ALTER TABLE `mst_notification`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mst_sports`
--
ALTER TABLE `mst_sports`
  MODIFY `id` mediumint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `mst_venue`
--
ALTER TABLE `mst_venue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `mst_venue_facility`
--
ALTER TABLE `mst_venue_facility`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pitch_hire`
--
ALTER TABLE `pitch_hire`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `quick_links_slider`
--
ALTER TABLE `quick_links_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `site_setting`
--
ALTER TABLE `site_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sportsturnaments_blogs`
--
ALTER TABLE `sportsturnaments_blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sportsturnaments_category`
--
ALTER TABLE `sportsturnaments_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `sportsturnaments_details`
--
ALTER TABLE `sportsturnaments_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sportsturnaments_faq`
--
ALTER TABLE `sportsturnaments_faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sportsturnaments_images`
--
ALTER TABLE `sportsturnaments_images`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sportsturnaments_videos`
--
ALTER TABLE `sportsturnaments_videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `team_spots`
--
ALTER TABLE `team_spots`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `user_area_intrest`
--
ALTER TABLE `user_area_intrest`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `user_location`
--
ALTER TABLE `user_location`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_sports`
--
ALTER TABLE `user_sports`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `venue_followers`
--
ALTER TABLE `venue_followers`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `your_booking`
--
ALTER TABLE `your_booking`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
