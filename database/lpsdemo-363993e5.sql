-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: shareddb-f.hosting.stackcp.net
-- Generation Time: Oct 01, 2019 at 02:40 PM
-- Server version: 10.2.25-MariaDB-log
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lpsdemo-363993e5`
--

-- --------------------------------------------------------

--
-- Table structure for table `advertise_session`
--

CREATE TABLE `advertise_session` (
  `id` bigint(127) NOT NULL,
  `user_id` bigint(127) NOT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `venue_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sport_id` int(11) NOT NULL,
  `no_of_player` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_recurring_booking` tinyint(1) NOT NULL,
  `message` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `signature` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `basic_features`
--

CREATE TABLE `basic_features` (
  `id` int(11) NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `basic_features`
--

INSERT INTO `basic_features` (`id`, `title`, `status`) VALUES
(2, 'Your first session is free, just so you know you can trust us to deliver a fantastic session.', 1),
(3, 'Play sports whenever you want, socialize and have fun.', 1),
(5, 'Access to thousands of sports and locations across the UK.', 1),
(6, 'Have your own regular game? No problem create your own listing and find players near you.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `block_booking`
--

CREATE TABLE `block_booking` (
  `id` bigint(127) NOT NULL,
  `user_id` bigint(127) DEFAULT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `venue` int(11) DEFAULT NULL,
  `ideal_days` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sport_id` int(11) DEFAULT NULL,
  `comments` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `children_party`
--

CREATE TABLE `children_party` (
  `id` bigint(127) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `category` int(11) NOT NULL COMMENT '1 venues, 2 coaching, 3 kids',
  `venue_title` varchar(255) DEFAULT NULL,
  `venue_address` varchar(255) DEFAULT NULL,
  `postcode` varchar(100) DEFAULT NULL,
  `latitude` varchar(100) DEFAULT NULL,
  `longitude` varchar(100) DEFAULT NULL,
  `box_title` varchar(255) DEFAULT NULL,
  `description` mediumtext DEFAULT NULL,
  `short_description` varchar(255) DEFAULT NULL,
  `video` varchar(200) DEFAULT NULL,
  `feature_image` varchar(100) DEFAULT NULL,
  `timing_description` text DEFAULT NULL,
  `opening_hours` text DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `children_party`
--

INSERT INTO `children_party` (`id`, `title`, `slug`, `category`, `venue_title`, `venue_address`, `postcode`, `latitude`, `longitude`, `box_title`, `description`, `short_description`, `video`, `feature_image`, `timing_description`, `opening_hours`, `status`, `created_at`, `updated_at`) VALUES
(10, 'Camden Childrens Parties', 'camden-childrens-parties-10', 1, 'Castlehaven Sports Pitch', '21 Castlehaven road, NW1 8RU', 'nw1 8ru', '51.542812', '-0.144604', 'Ideas to add to your party', '<p>Should you require any extras for your event we can provide:</p>\r\n\r\n<p>Photobooth</p>\r\n\r\n<p>Catering (Hot and cold food) and Cake</p>\r\n\r\n<p>Entertainment</p>\r\n\r\n<p>Coaches and Equipment</p>\r\n\r\n<p>Transport to and from the facility</p>\r\n\r\n<p>Party Bags</p>\r\n\r\n<p>Indoor Venue decoration</p>', 'Join us for a fantastic party at our beautiful camden space. Featuring a fantastic 9 a side multi sport pitch suitable for up to 60 kids and private indoor spaces.', '1569493110.jpg', '1569493287.jpg', '<p>A stones throw away from Camden road, Camden Town and Kentish Town west, we can host:</p>\r\n\r\n<p>Themed Parties</p>\r\n\r\n<p>Inflatable Parties</p>\r\n\r\n<p>Sports Parties.</p>\r\n\r\n<p>With coaches that can be provided along with equipment we will ensure your party is perfectly planned and organised. Showers and changing facilities are also available should you require them. An indoor function room is also available for after party food and snacks</p>', '<p>For booking times please ensure you email us on info@letsplaysports.co.uk. We can accomodate for half term, end of term and some afternoon events but traditionally our birthday parties fall on Friday evenings, Saturday or Sundays.</p>', 1, '2019-09-26 08:28:13', '2019-09-28 13:05:34'),
(11, 'St Johns Wood Kids Parties', 'st-johns-wood-kids-parties-11', 1, 'Harris Academy St Johns Wood', 'Marlborough Hill, London NW8 0NL', 'NW8 0NL', '51.537835', '-0.176685', 'Ideas to add to your party', '<p>Should you require any extras for your event we can provide:</p>\r\n\r\n<p>Photobooth</p>\r\n\r\n<p>Catering (Hot and cold food) and Cake</p>\r\n\r\n<p>Entertainment</p>\r\n\r\n<p>Coaches and Equipment</p>\r\n\r\n<p>Transport to and from the facility</p>\r\n\r\n<p>Party Bags</p>\r\n\r\n<p>Indoor Venue decoration</p>', 'Located in the heart of London with fantastic facilities and very easy access this is a premium site for your childs event.', '', '1569495206.jpg', '<p>A stones throw away from St johns wood and Swiss Cottage, we can host:</p>\r\n\r\n<p>Themed Parties</p>\r\n\r\n<p>Inflatable Parties</p>\r\n\r\n<p>Sports Parties.</p>\r\n\r\n<p>With coaches that can be provided along with equipment we will ensure your party is perfectly planned and organised. Showers and changing facilities are also available should you require them. An indoor function room is also available for after party food and snacks</p>', '<p>..</p>', 1, '2019-09-26 09:53:26', '2019-09-27 11:59:04'),
(14, 'Edgware Kids Parties', 'edgware-kids-parties-14', 1, 'St James Catholic High School', 'St James Catholic High School Great Strand  NW9 5PE', 'NW9 5PE', '51.600791', '-0.240933', 'Ideas to add to your party', '<p>Should you require any extras for your event we can provide:</p>\r\n\r\n<p>Photobooth</p>\r\n\r\n<p>Catering (Hot and cold food) and Cake</p>\r\n\r\n<p>Entertainment</p>\r\n\r\n<p>Coaches and Equipment</p>\r\n\r\n<p>Transport to and from the facility</p>\r\n\r\n<p>Party Bags</p>\r\n\r\n<p>Indoor Venue decoration</p>\r\n\r\n<p>&nbsp;</p>', 'One of our biggest and most premium london venues. With great access to local underground stations and motorway this is a fantastic location.', '', '1569497106.jpg', '<p>A stones throw away from Edgware, Burnt Oak and Colindale&nbsp;we can host:</p>\r\n\r\n<p>Themed Parties</p>\r\n\r\n<p>Inflatable Parties</p>\r\n\r\n<p>Sports Parties.</p>\r\n\r\n<p>With coaches that can be provided along with equipment we will ensure your party is perfectly planned and organised. Showers and changing facilities are also available should you require them. An indoor function room is also available for after party food and snacks</p>', '<p>...</p>', 1, '2019-09-26 10:25:06', '2019-09-27 12:54:30'),
(15, 'Finchley Christ College Kids Parties', 'finchley-christ-college-kids-parties-15', 1, 'Christ\'s College', 'Christ\'s College, East End Road, Finchley  N2 0SE', 'N2 0SE', '51.590982', '-0.187262', 'Ideas to add to your party', '<p>Should you require any extras for your event we can provide:</p>\r\n\r\n<p>Photobooth</p>\r\n\r\n<p>Catering (Hot and cold food) and Cake</p>\r\n\r\n<p>Entertainment</p>\r\n\r\n<p>Coaches and Equipment</p>\r\n\r\n<p>Transport to and from the facility</p>\r\n\r\n<p>Party Bags</p>\r\n\r\n<p>Indoor Venue decoration</p>', 'One of North Londons premier kids party venues. 3G facilities with easy access and parking.', '', '1569498274.png', '<p>A stones throw away from Finchley, Highgate and Archway&nbsp;we can host:</p>\r\n\r\n<p>Themed Parties</p>\r\n\r\n<p>Inflatable Parties</p>\r\n\r\n<p>Sports Parties.</p>\r\n\r\n<p>With coaches that can be provided along with equipment we will ensure your party is perfectly planned and organised. Showers and changing facilities are also available should you require them. An indoor function room is also available for after party food and snacks</p>\r\n\r\n<p>&nbsp;</p>', '<p>..</p>', 1, '2019-09-26 10:44:34', '2019-09-28 05:39:53'),
(16, 'White City Tiger Turf Kids Parties', 'white-city-tiger-turf-kids-parties-16', 1, 'White City Tiger Turf', 'Burlington Danes Academy White City, London  W12 0EA', 'W12 0EA', '51.518954', '-0.229718', 'Ideas to add to your party', '<p>Should you require any extras for your event we can provide:</p>\r\n\r\n<p>Photobooth</p>\r\n\r\n<p>Catering (Hot and cold food) and Cake</p>\r\n\r\n<p>Entertainment</p>\r\n\r\n<p>Coaches and Equipment</p>\r\n\r\n<p>Transport to and from the facility</p>\r\n\r\n<p>Party Bags</p>\r\n\r\n<p>Indoor Venue decoration</p>', 'Kids Parties in west london at our White City venue. Featuring a fantastic 11 A SIDE 3g pitch, grass area and indoor spaces', '', '1569498790.jpg', '<p>A stones throw away from Shepherd&#39;s Bush, Chiswick&nbsp;and Hammersmith&nbsp;we can host:</p>\r\n\r\n<p>Themed Parties</p>\r\n\r\n<p>Inflatable Parties</p>\r\n\r\n<p>Sports Parties.</p>\r\n\r\n<p>With coaches that can be provided along with equipment we will ensure your party is perfectly planned and organised. Showers and changing facilities are also available should you require them. An indoor function room is also available for after party food and snacks</p>', '<p>..</p>', 1, '2019-09-26 10:53:10', '2019-09-28 05:39:59'),
(17, 'Hammersmith Academy Kids Parties', 'hammersmith-academy-kids-parties-17', 1, 'Hammersmith Academy', '25 Cathnor Road, Shepherd\'s Bush, London, W12 9JD', 'W12 9JD', '51.502110', '-0.236140', 'Ideas to add to your party', '<p>Should you require any extras for your event we can provide:</p>\r\n\r\n<p>Photobooth</p>\r\n\r\n<p>Catering (Hot and cold food) and Cake</p>\r\n\r\n<p>Entertainment</p>\r\n\r\n<p>Coaches and Equipment</p>\r\n\r\n<p>Transport to and from the facility</p>\r\n\r\n<p>Party Bags</p>\r\n\r\n<p>Indoor Venue decoration</p>', 'A hidden gem inbetween Hammersmith and Shepherds Bush. Indoor and outdoor possibilities for your fantastic party.', '', '1569499485.jpg', '<p>A stones throw away from Hammersmith, Chiswick and Barons Court&nbsp;we can host:</p>\r\n\r\n<p>Themed Parties</p>\r\n\r\n<p>Inflatable Parties</p>\r\n\r\n<p>Sports Parties.</p>\r\n\r\n<p>With coaches that can be provided along with equipment we will ensure your party is perfectly planned and organised. Showers and changing facilities are also available should you require them. An indoor function room is also available for after party food and snacks</p>', '<p>...</p>', 1, '2019-09-26 11:04:45', '2019-09-28 05:40:06'),
(18, 'Rocks Lane Chiswick Kids Parties', 'rocks-lane-chiswick-kids-parties-18', 1, 'Rocks Lane Chiswick', 'Rocks Lane Multi Sports Centre, 60 Chiswick Common Road W4 1RZ', 'W4 1RZ', '51.493802', '-0.259706', 'Ideas to add to your party', '<p>Should you require any extras for your event we can provide:</p>\r\n\r\n<p>Photobooth</p>\r\n\r\n<p>Catering (Hot and cold food) and Cake</p>\r\n\r\n<p>Entertainment</p>\r\n\r\n<p>Coaches and Equipment</p>\r\n\r\n<p>Transport to and from the facility</p>\r\n\r\n<p>Party Bags</p>\r\n\r\n<p>Indoor Venue decoration</p>', 'One of the most popular facilities in west London with a fantastic new canteen and soft play space.', '', '1569500472.jpg', '<p>A stones throw away from Chiswick, Hammersmith and Shepherd Bush&nbsp;we can host:</p>\r\n\r\n<p>Themed Parties</p>\r\n\r\n<p>Inflatable Parties</p>\r\n\r\n<p>Sports Parties.</p>\r\n\r\n<p>With coaches that can be provided along with equipment we will ensure your party is perfectly planned and organised. Showers and changing facilities are also available should you require them. An indoor function room is also available for after party food and snacks</p>', '<p>...</p>', 1, '2019-09-26 11:21:12', '2019-09-28 05:40:12'),
(19, 'Rocks Lane Barnes Kids Parties', 'rocks-lane-barnes-kids-parties-19', 1, 'Rocks Lane Barnes', 'Rocks Lane Multi Sports Centre, Rocks Lane, Opposite Ranelagh Avenue, Barnes, London, SW13 0BY', 'SW13 0BY', '51.493802', '-0.259706', 'Ideas to add to your party', '<p>Should you require any extras for your event we can provide:</p>\r\n\r\n<p>Photobooth</p>\r\n\r\n<p>Catering (Hot and cold food) and Cake</p>\r\n\r\n<p>Entertainment</p>\r\n\r\n<p>Coaches and Equipment</p>\r\n\r\n<p>Transport to and from the facility</p>\r\n\r\n<p>Party Bags</p>\r\n\r\n<p>Indoor Venue decoration</p>', 'Join us for a fantastic party at our beautiful Barnes space. Featuring multiple 3g pitches perfect for football or multisport.', '', '1569503028.jpeg', '<p>A stones throw away from Barnes, Chiswick, Fulham, Putney and Richmond&nbsp;we can host:</p>\r\n\r\n<p>Themed Parties</p>\r\n\r\n<p>Inflatable Parties</p>\r\n\r\n<p>Sports Parties.</p>\r\n\r\n<p>With coaches that can be provided along with equipment we will ensure your party is perfectly planned and organised. Showers and changing facilities are also available should you require them. An indoor function room is also available for after party food and snacks</p>', '<p>..</p>', 1, '2019-09-26 12:03:48', '2019-09-28 05:40:18'),
(20, 'Ark Elvin Kids Parties', 'ark-elvin-kids-parties-20', 1, 'Ark Elvin Academy', 'Cecil Ave, London, Wembley', 'HA9 7DU', '51.552593', '-0.289657', 'Ideas to add to your party', '<p>Should you require any extras for your event we can provide:</p>\r\n\r\n<p>Photobooth</p>\r\n\r\n<p>Catering (Hot and cold food) and Cake</p>\r\n\r\n<p>Entertainment</p>\r\n\r\n<p>Coaches and Equipment</p>\r\n\r\n<p>Transport to and from the facility</p>\r\n\r\n<p>Party Bags</p>\r\n\r\n<p>Indoor Venue decoration</p>', 'A fantastic facility in the heart of Wembley with new 3G pitch and indoor spaces available.', '', '1569504942.jpg', '<p>A stones throw away from Wembley, Alperton and Harrow:</p>\r\n\r\n<p>Themed Parties</p>\r\n\r\n<p>Inflatable Parties</p>\r\n\r\n<p>Sports Parties.</p>\r\n\r\n<p>With coaches that can be provided along with equipment we will ensure your party is perfectly planned and organised. Showers and changing facilities are also available should you require them. An indoor function room is also available for after party food and snacks</p>', '<p>...</p>', 1, '2019-09-26 12:35:42', '2019-09-28 05:40:23'),
(21, 'Fulham Eel Brook Kids Parties', 'fulham-eel-brook-kids-parties-21', 1, 'Fulham Eel Brook Common', 'Eel Brook common- Walham Green, Musgrave Crescent, Fulham, London  SW6 4QE', 'SW6 4QE', '51.480160', '-0.196320', 'Ideas to add to your party', '<p>Should you require any extras for your event we can provide:</p>\r\n\r\n<p>Photobooth</p>\r\n\r\n<p>Catering (Hot and cold food) and Cake</p>\r\n\r\n<p>Entertainment</p>\r\n\r\n<p>Coaches and Equipment</p>\r\n\r\n<p>Transport to and from the facility</p>\r\n\r\n<p>Party Bags</p>\r\n\r\n<p>&nbsp;</p>', 'Join us for a fantastic party at our beautiful Fulham space. Featuring a fantastic 5 a side multi sport pitch suitable for up to 20 kids.', '', '1569511656.jpg', '<p>A stones throw away from Fulham, Chelsea, Putney and Hammersmith&nbsp;we can host:</p>\r\n\r\n<p>Themed Parties</p>\r\n\r\n<p>Inflatable Parties</p>\r\n\r\n<p>Sports Parties.</p>\r\n\r\n<p>With coaches that can be provided along with equipment we will ensure your party is perfectly planned and organised. Showers and changing facilities are also available should you require them. An indoor function room is also available for after party food and snacks</p>', '<p>...</p>', 1, '2019-09-26 14:27:36', '2019-09-28 05:40:29'),
(22, 'Waterloo Kids Parties', 'waterloo-kids-parties-22', 1, 'Waterloo Tiger Turf', '1 Coral Street, Waterloo, London  SE1 7BE', 'SE1 7BE', '51.500893', '-0.108803', 'Ideas to add to your party', '<p>Should you require any extras for your event we can provide:</p>\r\n\r\n<p>Photobooth</p>\r\n\r\n<p>Catering (Hot and cold food) and Cake</p>\r\n\r\n<p>Entertainment</p>\r\n\r\n<p>Coaches and Equipment</p>\r\n\r\n<p>Transport to and from the facility</p>\r\n\r\n<p>Party Bags</p>\r\n\r\n<p>&nbsp;</p>', 'You could not get more central than this location. Easy to get to and from with great access and new facilities.', '', '1569512458.jpg', '<p>A stones throw away from Waterloo, Vauxhall, Westminister and Lambeth North we can host:</p>\r\n\r\n<p>Themed Parties</p>\r\n\r\n<p>Inflatable Parties</p>\r\n\r\n<p>Sports Parties.</p>\r\n\r\n<p>With coaches that can be provided along with equipment we will ensure your party is perfectly planned and organised. Showers and changing facilities are also available should you require them. An indoor function room is also available for after party food and snacks</p>', '<p>..</p>', 1, '2019-09-26 14:40:58', '2019-09-30 09:27:30'),
(23, 'Vauxhall Kids Parties', 'vauxhall-kids-parties-23', 1, 'Vauxhall Rhino Turf', 'Lollard Street, Vauxhall, London', 'SE11 6PX', '51.492880', '-0.114934', 'Ideas to add to your party', '<p>Should you require any extras for your event we can provide:</p>\r\n\r\n<p>Photobooth</p>\r\n\r\n<p>Catering (Hot and cold food) and Cake</p>\r\n\r\n<p>Entertainment</p>\r\n\r\n<p>Coaches and Equipment</p>\r\n\r\n<p>Transport to and from the facility</p>\r\n\r\n<p>Party Bags</p>\r\n\r\n<p>Indoor Venue decoration</p>', 'Join us for a fantastic party at our beautiful Vauxhall space. Featuring indoor areas and a fully functional playground - suitable for up to 50 kids.', '', '1569513107.jpg', '<p>A stones throw away from Vauxhall, Waterloo, Brixton and Stockwell we can host:</p>\r\n\r\n<p>Themed Parties</p>\r\n\r\n<p>Inflatable Parties</p>\r\n\r\n<p>Sports Parties.</p>\r\n\r\n<p>With coaches that can be provided along with equipment we will ensure your party is perfectly planned and organised. Showers and changing facilities are also available should you require them. An indoor function room is also available for after party food and snacks</p>', '<p>..</p>', 1, '2019-09-26 14:51:47', '2019-09-28 05:41:17'),
(24, 'Brixton Kids Parties', 'brixton-kids-parties-24', 1, 'Brixton Max Roach', 'Max Roach Playground Enter Via Villa Rd Brixton', 'SW9 7YA', '51.467279', '-0.111346', 'Ideas to add to your party', '<p>Should you require any extras for your event we can provide:</p>\r\n\r\n<p>Photobooth</p>\r\n\r\n<p>Catering (Hot and cold food) and Cake</p>\r\n\r\n<p>Entertainment</p>\r\n\r\n<p>Coaches and Equipment</p>\r\n\r\n<p>Transport to and from the facility</p>\r\n\r\n<p>Party Bags</p>\r\n\r\n<p>Indoor Venue decoration</p>', 'A fantastic playspace with a fully functional playground and indoor area. Suitable for up to 25 children.', '', '1569513774.jpg', '<p>A stones throw away from Brixton, Stockwell, Kennington and Clapham&nbsp;we can host:</p>\r\n\r\n<p>Themed Parties</p>\r\n\r\n<p>Inflatable Parties</p>\r\n\r\n<p>Sports Parties.</p>\r\n\r\n<p>With coaches that can be provided along with equipment we will ensure your party is perfectly planned and organised. Showers and changing facilities are also available should you require them. An indoor function room is also available for after party food and snacks</p>', '<p>...</p>', 1, '2019-09-26 15:02:54', '2019-09-28 05:41:23'),
(25, 'Stockwell Kids Parties', 'stockwell-kids-parties-25', 1, 'Horizons Fitness Centre', 'Horizons Fitness Centre, Liberty Street, Stockwell, London', 'SW9 0RP', '51.475611', '-0.116601', 'Ideas to add to your party', '<p>Should you require any extras for your event we can provide:</p>\r\n\r\n<p>Photobooth</p>\r\n\r\n<p>Catering (Hot and cold food) and Cake</p>\r\n\r\n<p>Entertainment</p>\r\n\r\n<p>Coaches and Equipment</p>\r\n\r\n<p>Transport to and from the facility</p>\r\n\r\n<p>Party Bags</p>\r\n\r\n<p>Indoor Venue decoration</p>', 'A private and small facility perfect for kids parties of up to 20 children.', '', '1569514229.jpg', '<p>A stones throw away from Stockwell, Brixton, Clapham and Elephant &amp; Castle, we can host:</p>\r\n\r\n<p>Themed Parties</p>\r\n\r\n<p>Inflatable Parties</p>\r\n\r\n<p>Sports Parties.</p>\r\n\r\n<p>With coaches that can be provided along with equipment we will ensure your party is perfectly planned and organised. Showers and changing facilities are also available should you require them. An indoor function room is also available for after party food and snacks</p>', '<p>.....</p>', 1, '2019-09-26 15:10:29', '2019-09-28 05:41:28'),
(26, 'North Greenwich Kids Parties', 'north-greenwich-kids-parties-26', 1, 'North Greenwich Millenium Academy', '50 John Harrison Way, London', 'SE10 0BG', '51.493809', '0.010992', 'Ideas to add to your party', '<p>Should you require any extras for your event we can provide:</p>\r\n\r\n<p>Photobooth</p>\r\n\r\n<p>Catering (Hot and cold food) and Cake</p>\r\n\r\n<p>Entertainment</p>\r\n\r\n<p>Coaches and Equipment</p>\r\n\r\n<p>Transport to and from the facility</p>\r\n\r\n<p>Party Bags</p>\r\n\r\n<p>Indoor Venue decoration</p>', 'A beautiful space in a quiet location. Perfect for kids parties.', '', '1569514582.jpg', '<p>A stones throw away from North Greenwich, Canada Water, Canary Wharf and Canning Town&nbsp;we can host:</p>\r\n\r\n<p>Themed Parties</p>\r\n\r\n<p>Inflatable Parties</p>\r\n\r\n<p>Sports Parties.</p>\r\n\r\n<p>With coaches that can be provided along with equipment we will ensure your party is perfectly planned and organised. Showers and changing facilities are also available should you require them. An indoor function room is also available for after party food and snacks</p>', '<p>...</p>', 1, '2019-09-26 15:16:22', '2019-09-28 05:41:33'),
(27, 'Canada Water Kids Parties', 'canada-water-kids-parties-27', 1, 'Canada Water Tiger Turf', 'Docklands Settlement, Salter Road, Surrey Quays, London', 'SE16 5AA', '51.499199', '-0.035145', 'Ideas to add to your party', '<p>Should you require any extras for your event we can provide:</p>\r\n\r\n<p>Photobooth</p>\r\n\r\n<p>Catering (Hot and cold food) and Cake</p>\r\n\r\n<p>Entertainment</p>\r\n\r\n<p>Coaches and Equipment</p>\r\n\r\n<p>Transport to and from the facility</p>\r\n\r\n<p>Party Bags</p>\r\n\r\n<p>Indoor Venue decoration</p>', 'Join us for a fantastic party at our beautiful Canada Water space. Featuring a fantastic 9 a side multi sport pitch suitable for up to 60 kids and private indoor spaces.', '', '1569515162.jpg', '<p>A stones throw away from Canada Water, Bermondsey London Bridge and Canary Wharf&nbsp;we can host:</p>\r\n\r\n<p>Themed Parties</p>\r\n\r\n<p>Inflatable Parties</p>\r\n\r\n<p>Sports Parties.</p>\r\n\r\n<p>With coaches that can be provided along with equipment we will ensure your party is perfectly planned and organised. Showers and changing facilities are also available should you require them. An indoor function room is also available for after party food and snacks</p>', '<p>...</p>', 1, '2019-09-26 15:26:02', '2019-09-28 05:41:40'),
(28, 'Wandsworth Kids Parties', 'wandsworth-kids-parties-28', 1, 'St Faiths School', 'Alma Road, East Hill,', 'SW18 1AE', '51.459357', '-0.186451', 'Ideas to add to your party', '<p>Should you require any extras for your event we can provide:</p>\r\n\r\n<p>Photobooth</p>\r\n\r\n<p>Catering (Hot and cold food) and Cake</p>\r\n\r\n<p>Entertainment</p>\r\n\r\n<p>Coaches and Equipment</p>\r\n\r\n<p>Transport to and from the facility</p>\r\n\r\n<p>Party Bags</p>\r\n\r\n<p>Indoor Venue decoration</p>', 'Join us for a fantastic party at our beautiful Wandsworth space. Featuring a fantastic a new multi sport pitch suitable for up to 20 kids and private indoor spaces.', '', '1569515785.jpg', '<p>A stones throw away from Wandsworth, Fulham, Chelsea and Clapham we can host:</p>\r\n\r\n<p>Themed Parties</p>\r\n\r\n<p>Inflatable Parties</p>\r\n\r\n<p>Sports Parties.</p>\r\n\r\n<p>With coaches that can be provided along with equipment we will ensure your party is perfectly planned and organised. Showers and changing facilities are also available should you require them. An indoor function room is also available for after party food and snacks</p>', '<p>..</p>', 1, '2019-09-26 15:36:25', '2019-09-28 05:41:46'),
(29, 'London bridge and Bermondsey Kids Parties', 'london-bridge-and-bermondsey-kids-parties-29', 1, 'Harris Academy Bermondsey', 'Southwark Park road', 'SE16 3TZ', '51.493259', '-0.070900', 'Ideas to add to your party', '<p>Should you require any extras for your event we can provide:</p>\r\n\r\n<p>Photobooth</p>\r\n\r\n<p>Catering (Hot and cold food) and Cake</p>\r\n\r\n<p>Entertainment</p>\r\n\r\n<p>Coaches and Equipment</p>\r\n\r\n<p>Transport to and from the facility</p>\r\n\r\n<p>Party Bags</p>\r\n\r\n<p>Indoor Venue decoration</p>', 'One of our premium venues. Featuring a fantastic 9 a side 4G pitch and indoor hall suitable for up to 40 kids.', '', '1569516559.jpg', '<p>A stones throw away from Bermondsey, Canada Water and&nbsp;London Brdge&nbsp;we can host:</p>\r\n\r\n<p>Themed Parties</p>\r\n\r\n<p>Inflatable Parties</p>\r\n\r\n<p>Sports Parties.</p>\r\n\r\n<p>With coaches that can be provided along with equipment we will ensure your party is perfectly planned and organised. Showers and changing facilities are also available should you require them. An indoor function room is also available for after party food and snacks</p>', '<p>..</p>', 1, '2019-09-26 15:49:19', '2019-09-28 05:41:52'),
(30, 'Ealing Kids Parties', 'ealing-kids-parties-30', 1, 'Lammas Park', 'Culmington Road, Ealing, Greater London, Ealing', 'W13 9NJ', '51.505975', '-0.309187', 'Ideas to add to your party', '<p>Should you require any extras for your event we can provide:</p>\r\n\r\n<p>Photobooth</p>\r\n\r\n<p>Catering (Hot and cold food) and Cake</p>\r\n\r\n<p>Entertainment</p>\r\n\r\n<p>Coaches and Equipment</p>\r\n\r\n<p>Transport to and from the facility</p>\r\n\r\n<p>Party Bags</p>\r\n\r\n<p>Indoor Venue decoration</p>', 'Ealings top kids party destination. New pitches and a fantastic indoor space, what more could you ask for.', '', '1569576292.jpg', '<p>A stones throw away from Ealing, Acton and Sheperds Bush we can host:</p>\r\n\r\n<p>Themed Parties</p>\r\n\r\n<p>Inflatable Parties</p>\r\n\r\n<p>Sports Parties.</p>\r\n\r\n<p>With coaches that can be provided along with equipment we will ensure your party is perfectly planned and organised. Showers and changing facilities are also available should you require them. An indoor function room is also available for after party food and snacks</p>', '<p>...</p>', 1, '2019-09-27 08:24:52', '2019-09-28 05:41:57'),
(31, 'Stratford Kids Parties', 'stratford-kids-parties-31', 1, 'Chobham Academy', 'Chobham Academy- Temple Mills Lane, Stratford, London', 'E15 2EG', '51.544663', '-0.008979', 'Ideas to add to your party', '<p>Should you require any extras for your event we can provide:</p>\r\n\r\n<p>Photobooth</p>\r\n\r\n<p>Catering (Hot and cold food) and Cake</p>\r\n\r\n<p>Entertainment</p>\r\n\r\n<p>Coaches and Equipment</p>\r\n\r\n<p>Transport to and from the facility</p>\r\n\r\n<p>Party Bags</p>\r\n\r\n<p>Indoor Venue decoration</p>', 'One of the biggest venues in London with excellent premium spaces built by the 2012 legacy.', '', '1569577257.jpg', '<p>A stones throw away from&nbsp;Stratford, Mile End Forest Gate and Bow Road we can host:</p>\r\n\r\n<p>Themed Parties</p>\r\n\r\n<p>Inflatable Parties</p>\r\n\r\n<p>Sports Parties.</p>\r\n\r\n<p>With coaches that can be provided along with equipment we will ensure your party is perfectly planned and organised. Showers and changing facilities are also available should you require them. An indoor function room is also available for after party food and snacks</p>\r\n\r\n<p>&nbsp;</p>', '<p>...</p>', 1, '2019-09-27 08:40:57', '2019-09-28 05:42:02'),
(32, 'Battersea Kids Parties', 'battersea-kids-parties-32', 1, 'Battersea Harris Academy', '401 Battersea Park road Enter from the main road,  Dagnall street', 'SW11 5PA', '51.463211', '-0.153134', 'Ideas to add to your party', '<p>Should you require any extras for your event we can provide:</p>\r\n\r\n<p>Photobooth</p>\r\n\r\n<p>Catering (Hot and cold food) and Cake</p>\r\n\r\n<p>Entertainment</p>\r\n\r\n<p>Coaches and Equipment</p>\r\n\r\n<p>Transport to and from the facility</p>\r\n\r\n<p>Party Bags</p>\r\n\r\n<p>Indoor Venue decoration</p>', 'Join us for a fantastic party at our beautiful Battersea space. Featuring a fantastic 9 a side multi sport pitch suitable for up to 60 kids and private indoor spaces.', '', '1569578226.jpg', '<p>A stones throw away from Battersea, Chelsea, Putney Bridge and Fulham&nbsp;we can host:</p>\r\n\r\n<p>Themed Parties</p>\r\n\r\n<p>Inflatable Parties</p>\r\n\r\n<p>Sports Parties.</p>\r\n\r\n<p>With coaches that can be provided along with equipment we will ensure your party is perfectly planned and organised. Showers and changing facilities are also available should you require them. An indoor function room is also available for after party food and snacks</p>', '<p>...</p>', 1, '2019-09-27 08:57:06', '2019-09-28 05:42:20'),
(33, 'test', 'test-32', 1, 'test', 'Telford, UK', NULL, NULL, NULL, 'ZXcZXc', '<p>Experience a series of fun games and challenges for 90 minutes, before the children move inside for food and presentations</p>\r\n\r\n<ul>\r\n	<li>point 1</li>\r\n	<li>point 2</li>\r\n	<li>point 3 ...</li>\r\n</ul>', 'xcXZcXcXZc', '1569679771.jpg', '1569679771.jpg', '<p>Training Date and Time: Tuesdays 4:15-5:30pm | Training Location: Chobham Academy, E15 | Match Day: Sundays (kick offs between 10-2pm) Match Location Home: Weavers Fields, E2 | Match Location Away: Dependant on away team</p>', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>Monday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Tuesday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Wednesdau</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Thursday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Friday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Saturday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Sunday</td>\r\n			<td>09:00 - 23:00</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', 1, '2019-09-28 13:09:31', '2019-09-28 13:09:31');

-- --------------------------------------------------------

--
-- Table structure for table `children_party_category`
--

CREATE TABLE `children_party_category` (
  `id` bigint(127) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `image` varchar(200) NOT NULL,
  `banner` varchar(100) DEFAULT NULL,
  `layout` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `children_party_category`
--

INSERT INTO `children_party_category` (`id`, `name`, `slug`, `description`, `image`, `banner`, `layout`, `status`, `created_at`, `updated_at`) VALUES
(1, 'SOFT PLAY HIRE', 'soft-play-hire', 'VIEW OUR SOFT PLAY OPTIONS FROM AGES 2 +', '1568987119.jpg', '1568987454.jpg', 1, 1, '2019-08-16 21:20:12', '2019-09-20 12:50:54'),
(3, 'INFLATABLE PARTIES', 'inflatable-parties', 'From Rodeo Bulls to assault courses we have a huge range of inflatables for every occasion', '1567754117.jpg', '1568641188.jpg', 1, 1, '2019-08-17 00:14:26', '2019-09-16 12:39:48'),
(4, 'SPORTS PARTIES', 'sports-parties', 'ORGANSING YOUR CHILDS PARTY DOES NOT NEED TO BE STRESSFUL', '1567753864.jpg', '1568987299.jpg', 2, 1, '2019-09-06 05:13:19', '2019-09-20 12:48:19'),
(5, 'THEMED PARTIES', 'themed-parties', 'From football themed events to disney princess and peppa pig we can tailor make a party to suit you', '1568987101.jpg', '1568641231.jpg', 2, 1, '2019-09-06 05:28:07', '2019-09-20 12:45:02');

-- --------------------------------------------------------

--
-- Table structure for table `common_video`
--

CREATE TABLE `common_video` (
  `id` int(11) NOT NULL,
  `video` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `common_video`
--

INSERT INTO `common_video` (`id`, `video`, `description`, `status`) VALUES
(1, '1566468736.mp4', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Quisque eu libero. Sed sit amet metus. Cras ante nisi, ornare sit amet, pretium eget, tempus suscipit, lectus. Vivamus fermentum est vitae felis. Pellentesque habitant morbi tristique senectus et netus.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `corporate_blogs`
--

CREATE TABLE `corporate_blogs` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `corporate_blogs`
--

INSERT INTO `corporate_blogs` (`id`, `category_id`, `title`, `slug`, `description`, `image`, `created_at`, `updated_at`) VALUES
(2, 2, 'Locations', 'locations-2', 'With exclusive agreements with venues across the UK we are able to find the perfect location for you. Whether its something as prime and central as Hyde Park to something in a rural grass field in Solihull.', '1568386450.jpg', '2019-09-18 12:57:57', '2019-09-18 11:57:57'),
(3, 2, 'Management and staffing', 'management-and-staffing-3', 'With a dedicated MC running your event you will never be left out in the cold. We always have a lead organiser and will provide private contact details for your lead 1 week prior to your event to ensure every desire is met.  Our organiser will bring a team suitable for your scale event and ensure all of his or her team are aware of your requirements. So all you need to do is turn up and enjoy.', '1568387206.jpg', '2019-09-18 13:00:56', '2019-09-18 12:00:56'),
(4, 2, 'Trophies and extras', 'trophies-and-extras-4', 'Our packs include Trophies for winners along with medals. You can also choose from extras such as Photo booths, photography, DJ, PA, Catering and staging. Additional trophies can also be purchased', '1568387575.jpg', '2019-09-18 13:01:23', '2019-09-18 12:01:23'),
(5, 2, 'Equipment', 'equipment-5', 'All equipment for your event is provided. So you do not need to bring anything. If you would like us to assist you with food and drinks this can also be discussed in addition to your event.', '1568387762.jpg', '2019-09-18 13:01:52', '2019-09-18 12:01:52'),
(9, 3, 'Locations', 'locations-9', 'With exclusive agreements with venues across the UK we are able to find the perfect location for you. Whether its something as prime and central as Hyde Park to something in a rural grass field in Solihull. Inflatables traditionally need larger spaces and we have a network of venues which we can recommend as part of the package.', '1568387836.jpg', '2019-09-18 13:00:03', '2019-09-18 12:00:03'),
(10, 3, 'Management and staffing', 'management-and-staffing-10', 'With a dedicated MC running your event you will never be left out in the cold. We always have a lead organiser and will provide private contact details for your lead 1 week prior to your event to ensure every desire is met.  Our organiser will bring a team suitable for your scale event and ensure all of his or her team are aware of your requirements. So all you need to do is turn up and enjoy. Each inflatable station will be manned and all our staff will wear uniform.', '1568387911.jpg', '2019-09-18 12:58:45', '2019-09-18 11:58:45'),
(11, 3, 'Trophies and extras', 'trophies-and-extras-11', 'Our packs include Trophies for winners along with medals. You can also choose from extras such as Photo booths, photography, DJ, PA, Catering and staging. Additional trophies can also be purchased', '1568387937.jpg', '2019-09-18 12:59:05', '2019-09-18 11:59:05'),
(12, 3, 'Equipment', 'equipment-12', 'All equipment for your event is provided. So you do not need to bring anything. If you would like us to assist you with food and drinks this can also be discussed in addition to your event.', '1568387982.jpg', '2019-09-18 12:59:55', '2019-09-18 11:59:55'),
(13, 4, 'Locations', 'locations-13', 'With exclusive agreements with venues across the UK we are able to find the perfect location for you. Whether its something as prime and central as Hyde Park to something in a rural grass field in Solihull. Inflatables traditionally need larger spaces and we have a network of venues which we can recommend as part of the package.', '1568448103.jpg', '2019-09-18 13:00:12', '2019-09-18 12:00:12'),
(14, 4, 'Management and staffing', 'management-and-staffing-14', 'With a dedicated MC running your event you will never be left out in the cold. We always have a lead organiser and will provide private contact details for your lead 1 week prior to your event to ensure every desire is met.  Our organiser will bring a team suitable for your scale event and ensure all of his or her team are aware of your requirements. So all you need to do is turn up and enjoy.', '1568448136.jpg', '2019-09-18 13:01:04', '2019-09-18 12:01:04'),
(15, 4, 'Trophies and extras', 'trophies-and-extras-14', 'Our packs include Trophies for winners and runners up along with medals. You can choose from standard 10\" trophies to large 22\" inch trophies so you can really spoil the winners and there is even a plate for the losers.', '1568448168.jpg', '2019-09-14 07:02:48', '2019-09-14 07:02:48'),
(16, 4, 'Equipment', 'equipment-16', 'All equipment for your event is provided. So you do not need to bring anything. If you would like us to assist you with food and drinks this can also be discussed in addition to your event.', '1568448209.jpg', '2019-09-18 13:02:04', '2019-09-18 12:02:04'),
(17, 5, 'Locations', 'locations-17', 'With exclusive agreements with venues across the UK we are able to find the perfect location for you. Whether its something as prime and central as Hyde Park to something in a rural grass field in Solihull. Inflatables traditionally need larger spaces and we have a network of venues which we can recommend as part of the package.', '1568448342.jpg', '2019-09-18 13:00:22', '2019-09-18 12:00:22'),
(18, 5, 'Management And Staffing', 'management-and-staffing-18', 'With a dedicated MC running your event you will never be left out in the cold. We always have a lead organiser and will provide private contact details for your lead 1 week prior to your event to ensure every desire is met.  Our organiser will bring a team suitable for your scale event and ensure all of his or her team are aware of your requirements. So all you need to do is turn up and enjoy.', '1568448370.jpg', '2019-09-18 13:01:15', '2019-09-18 12:01:15'),
(19, 5, 'Trophies And Extras', 'trophies-and-extras-19', 'Our packs include Trophies for winners along with medals. You can also choose from extras such as Photo booths, photography, DJ, PA, Catering and staging. Additional trophies can also be purchased', '1568448391.jpg', '2019-09-18 13:01:44', '2019-09-18 12:01:44'),
(20, 5, 'Equipment', 'equipment-20', 'All equipment for your event is provided. So you do not need to bring anything. If you would like us to assist you with food and drinks this can also be discussed in addition to your event.', '1568448412.jpg', '2019-09-18 13:02:13', '2019-09-18 12:02:13'),
(21, 2, 'Catering', 'catering-21', 'Catering can be provided on request. All dietary requirements can be catered for. Our traditional package includes: \r\n\r\nBeef Burgers, Chicken Burgers, Hot Dogs, Veggie Burgers and Halloumi Wraps\r\nPotato Salad, Coleslaw (home made), Salad\r\nDrinks (soft and alcoholic) \r\n\r\nWe can also include Pizza, Fries, Wedges and many other items. Dessert options can also be added on.', '1568811988.jpg', '2019-09-18 13:06:28', '2019-09-18 12:06:28'),
(22, 3, 'Catering', 'catering-22', 'Catering can be provided on request. All dietary requirements can be catered for. Our traditional package includes: \r\n\r\nBeef Burgers, Chicken Burgers, Hot Dogs, Veggie Burgers and Halloumi Wraps\r\nPotato Salad, Coleslaw (home made), Salad\r\nDrinks (soft and alcoholic) \r\n\r\nWe can also include Pizza, Fries, Wedges and many other items. Dessert options can also be added on.', '1568812019.jpg', '2019-09-18 13:06:59', '2019-09-18 12:06:59'),
(23, 4, 'Catering', 'catering-23', 'Catering can be provided on request. All dietary requirements can be catered for. Our traditional package includes: \r\n\r\nBeef Burgers, Chicken Burgers, Hot Dogs, Veggie Burgers and Halloumi Wraps\r\nPotato Salad, Coleslaw (home made), Salad\r\nDrinks (soft and alcoholic) \r\n\r\nWe can also include Pizza, Fries, Wedges and many other items. Dessert options can also be added on.', '1568812050.jpg', '2019-09-18 13:07:30', '2019-09-18 12:07:30'),
(24, 5, 'Catering', 'catering-24', 'Catering can be provided on request. All dietary requirements can be catered for. Our traditional package includes: \r\n\r\nBeef Burgers, Chicken Burgers, Hot Dogs, Veggie Burgers and Halloumi Wraps\r\nPotato Salad, Coleslaw (home made), Salad\r\nDrinks (soft and alcoholic) \r\n\r\nWe can also include Pizza, Fries, Wedges and many other items. Dessert options can also be added on.', '1568812076.jpg', '2019-09-18 13:07:56', '2019-09-18 12:07:56'),
(25, 2, 'Music, Marquee and Staging', 'music-marquee-and-staging-25', 'We can add on extras to your event to truly add the icing on the cake. With a network of entertainers we can add on DJ\'s, PA system, Staging, or Performers.\r\n\r\nIf you need indoor areas or conference rooms prior to your event or after the event this can also be arranged.', '1568812309.jpg', '2019-09-18 13:11:49', '2019-09-18 12:11:49'),
(26, 3, 'Music, Marquee and Staging', 'music-marquee-and-staging-25', 'We can add on extras to your event to truly add the icing on the cake. With a network of entertainers we can add on DJ\'s, PA system, Staging, or Performers.\r\n\r\nIf you need indoor areas or conference rooms prior to your event or after the event this can also be arranged.', '1568812412.jpg', '2019-09-18 12:13:32', '2019-09-18 12:13:32'),
(27, 4, 'Music, Marquee and Staging', 'music-marquee-and-staging-26', 'We can add on extras to your event to truly add the icing on the cake. With a network of entertainers we can add on DJ\'s, PA system, Staging, or Performers.\r\n\r\nIf you need indoor areas or conference rooms prior to your event or after the event this can also be arranged.', '1568812427.jpg', '2019-09-18 12:13:47', '2019-09-18 12:13:47'),
(28, 5, 'Music, Marquee and Staging', 'music-marquee-and-staging-27', 'We can add on extras to your event to truly add the icing on the cake. With a network of entertainers we can add on DJ\'s, PA system, Staging, or Performers.\r\n\r\nIf you need indoor areas or conference rooms prior to your event or after the event this can also be arranged.', '1568812441.jpg', '2019-09-18 12:14:01', '2019-09-18 12:14:01');

-- --------------------------------------------------------

--
-- Table structure for table `corporate_category`
--

CREATE TABLE `corporate_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `corporate_category`
--

INSERT INTO `corporate_category` (`id`, `name`, `slug`, `image`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Corporate Sports Days', 'corporate-sports-days', '1568801189.jpg', 1, '2019-09-18 10:06:29', '2019-09-18 09:06:29'),
(3, 'Inflatable Fun Days', 'inflatable-fun-days', '1568812870.jpg', 1, '2019-09-18 13:21:10', '2019-09-18 12:21:10'),
(4, 'Its A Knockout Fun Days', 'its-a-knockout-fun-days', '1568356787.jpg', 1, '2019-09-13 06:39:47', '2019-09-13 05:39:47'),
(5, 'Office Summer Party', 'office-summer-party', '1568456643.jpg', 1, '2019-09-14 10:24:03', '2019-09-14 09:24:03'),
(6, 'Office Christmas Party', 'office-christmas-party', '1568456693.jpg', 1, '2019-09-14 10:24:53', '2019-09-14 09:24:53');

-- --------------------------------------------------------

--
-- Table structure for table `corporate_details`
--

CREATE TABLE `corporate_details` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `feature_image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `main_video` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner_description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `sec_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sec_description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `layout` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `corporate_details`
--

INSERT INTO `corporate_details` (`id`, `title`, `feature_image`, `banner`, `category_id`, `main_video`, `banner_description`, `description`, `sec_title`, `sec_description`, `layout`, `created_at`, `updated_at`) VALUES
(1, 'corporate sports days', '1568802264.jpg', '1568384828.jpg', 2, '<iframe width=\"630\" height=\"370\" src=\"https://www.youtube.com/embed/HZFiquIvvSo\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', NULL, '<p>Fun filled, fast paced action for your corporate event. We have a huge range of activities we can include within your sports day package. We can cater for any numbers of participants from 20 to 2000 and can select a location that meets your requirements. In addition to providing a fantastic event we can also organise:</p>\r\n\r\n<p>Pre Drinks</p>\r\n\r\n<p>Inflatable and Photobooths</p>\r\n\r\n<p>PA, Staging and DJ</p>\r\n\r\n<p>Catering</p>\r\n\r\n<p>Post drinks and after party venue</p>\r\n\r\n<p>Please email us on info@letsplaysports.co.uk or call 020 3589 6019 for a quote.</p>', 'Sports Day Challanges', '<p>Not sure how to run the event and organise games? Well don&#39;t worry with every event we have a dedicated MC and event staff that will ensure you enjoy your day out. We can arrange a suitable location for your event or liase with a location you may have selected and get everything set up ready for your arrival, so all you have to do is turn up and enjoy.&nbsp;</p>\r\n\r\n<p>Some of our challenges include:</p>\r\n\r\n<p>Blindfold egg and spoon race</p>\r\n\r\n<p>3 legged race</p>\r\n\r\n<p>Sack Race</p>\r\n\r\n<p>Space Hopper Challenge</p>\r\n\r\n<p>Over and Under / Tunnell Ball</p>\r\n\r\n<p>Soccer Skillz</p>\r\n\r\n<p>Relay race</p>\r\n\r\n<p>Volleyball</p>\r\n\r\n<p>Dodgeball&nbsp;</p>\r\n\r\n<p>Tug of war</p>\r\n\r\n<p>Rounders (normally an event on its own)</p>\r\n\r\n<p>Event run time is normally 2-3 hours subject to participants. For a more accurate quote please email info@letsplaysports.co.uk or call us on 020 3589 6019</p>', 1, '2019-09-13 14:27:08', '2019-09-28 13:49:18'),
(2, 'inflatable fun days', '1568813106.jpg', '1569142658.jpg', 3, '<iframe width=\"630\" height=\"370\" src=\"https://www.youtube.com/embed/Wz7qmJix1S8\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', NULL, '<p>Fun filled, fast paced action for your corporate event. We have a huge range of activities we can include within your inflatable fun day. Some of our top inflatables include:&nbsp;</p>\r\n\r\n<p>- Assault Courses</p>\r\n\r\n<p>- Big Slide</p>\r\n\r\n<p>- Wrecking Ball</p>\r\n\r\n<p>- Sumo Suits</p>\r\n\r\n<p>- Hungry Hippos</p>\r\n\r\n<p>- Bungee Run</p>\r\n\r\n<p>&nbsp;</p>', 'Inflatable Fun day overview', '<p>We can tailor make an inflatable package to suit you from 1 inflatable to 20 inflatables. If only a few inflatables selected we recommend combining with some sports day races to provide a fun filled day. If over 6&nbsp;inflatables selected an event structured around the inflatables can be created.Our&nbsp;members of staff will run the entire event along with his or her assistants and each inflatable will be manned and all participants will get a chance to rotate around. Inflatables will be set up prior to arrival to ensure a wow factor.</p>', 1, '2019-09-13 14:17:23', '2019-09-28 14:04:57'),
(3, 'Its A Knockout Fun Days', '1569320388.jpg', '1568384846.jpg', 4, '<iframe width=\"630\" height=\"370\" src=\"https://www.youtube.com/embed/uHFZsSHCJRo\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', NULL, '<p>Its a knockout features fun filled inflatables and can combine sports day challenges along side this event. Infaltables and sports day races can be preselected to make up your fantastic event. All events can be customised to suit your budgets,</p>', 'Sports Day Challanges', '<p>Not sure how to run the event and organise games? Well don&#39;t worry we run absolutely everything for you. A typical knockout would feature:</p>\r\n\r\n<p>Assault Courses</p>\r\n\r\n<p>Slides</p>\r\n\r\n<p>Bungee Run</p>\r\n\r\n<p>Last Man Standing</p>\r\n\r\n<p>Wrecking Ball</p>\r\n\r\n<p>Hungry Hippos</p>\r\n\r\n<p>Sumo Suits</p>\r\n\r\n<p>Tug of war</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>More events can be added and customised to suit your needs such as Blaster, Relay, Duck n Dive, Water Slides and so on.</p>', 1, '2019-09-13 14:27:26', '2019-09-28 14:10:35'),
(4, 'office summer party', '1568385482.jpg', '1568457325.jpg', 5, '<iframe width=\"630\" height=\"370\" src=\"https://www.youtube.com/embed/TmBLjlgNRMk\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', NULL, '<p>Fun filled, fast paced action for your corporate event. We have a huge range of activities we can include within your sports day package. Please email us on info@letsplaysports.co.uk or call 020 3589 6019 for a quote.</p>', 'Your Summer Party', '<p>Your Summer party starts right here whether its a sports themed event or just a venue you need to host a summer beach inspired party.</p>\r\n\r\n<p>Its not just sports and fun days we cater for we can provide venue, DJ, PA system, staging and entertainers and ensure your entire summer party goes off without a glitch.&nbsp;</p>\r\n\r\n<p>With exclusive venues across the UK from Hyde Park to the Sports Direct Arena we can accomodate any request no matter how wild it might be.</p>', 2, '2019-09-14 10:35:25', '2019-09-28 05:42:34'),
(6, 'office christmas party', '1568451003.jpg', '1568451133.jpg', 6, '<iframe width=\"630\" height=\"370\" src=\"https://www.youtube.com/embed/2_-XhoeUJek\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', NULL, '<p>Fun filled, fast paced action for your corporate event. We have a huge range of activities we can include within your sports day package. Please email us on info@letsplaysports.co.uk or call 020 3589 6019 for a quote.</p>', 'Your office christmas Party', '<p>Dont be fooled by the name letsplaysports just remember the letsplay part :)&nbsp;</p>\r\n\r\n<p>We can arrange&nbsp;your christmas party from organising the venue to selecting entertainers and ensuring travel to the venue. So you do not have to lift a finger.&nbsp;</p>', 2, '2019-09-14 09:05:32', '2019-09-24 13:16:33');

-- --------------------------------------------------------

--
-- Table structure for table `corporate_faq`
--

CREATE TABLE `corporate_faq` (
  `id` int(11) NOT NULL,
  `cp_id` int(11) DEFAULT NULL,
  `title` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `corporate_faq`
--

INSERT INTO `corporate_faq` (`id`, `cp_id`, `title`) VALUES
(23, 1, NULL),
(24, 2, NULL),
(25, 3, NULL),
(27, 5, NULL),
(43, 4, 'Catering options'),
(44, 4, 'Exclusive Venues catered to your needs'),
(45, 4, 'DJ, Music, PA and Staging all part of the package'),
(46, 4, 'Full event management and staffing'),
(47, 4, 'Tailor made event from Sports day themes to Beach parties'),
(48, 4, 'Packages from 50 - 20000 participants'),
(49, 4, 'Event licensing and applications all taken care off if appropriate'),
(50, 4, 'Inflatables and Marquee options'),
(51, 6, 'Venue selection and viewing through video or in person'),
(52, 6, 'Extensive catering options'),
(53, 6, 'Entertainment from DJs to street performers'),
(54, 6, 'Custom built event to suit your budgets'),
(55, 6, 'Hire of our exclusive inflatable dome for christmas prizes'),
(56, 6, 'Hire of our exclusive inflatable dome for christmas prizes'),
(57, 6, 'Full event management and organisation'),
(58, 6, 'Full event management and organisation');

-- --------------------------------------------------------

--
-- Table structure for table `corporate_images`
--

CREATE TABLE `corporate_images` (
  `id` bigint(127) NOT NULL,
  `cp_id` bigint(127) NOT NULL,
  `image` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `corporate_images`
--

INSERT INTO `corporate_images` (`id`, `cp_id`, `image`) VALUES
(9, 2, 'child-page-img-2.jpg'),
(21, 5, 'child-page-img-1.jpg'),
(22, 5, 'child-page-img-3.jpg'),
(23, 5, 'child-page-img-4.jpg'),
(24, 5, 'inflatable fun dummy.jpg'),
(25, 6, 'office-Christmas-party.jpg'),
(26, 6, 'office-summer-party (2).jpg'),
(27, 6, 'office-Christmas-party.jpg'),
(28, 6, 'office-Christmas-party.jpg'),
(41, 1, 'WATER 1.jpg'),
(43, 2, '20190913_161031.jpg'),
(44, 2, '20190913_160954.jpg'),
(45, 2, '20190913_160115.jpg'),
(47, 3, 'knockout1.jpg'),
(48, 3, 'knockout2.jpg'),
(49, 3, 'knockout3.webp'),
(50, 3, 'CORP1.jpg'),
(51, 3, 'knockout main.jpg'),
(52, 3, 'FUN DAY.jpg'),
(53, 4, 'office 1.jpg'),
(54, 4, 'office 2.jpg'),
(55, 4, 'office 3.jpg'),
(56, 4, 'dj-summer-beach-party_3.jpg'),
(57, 1, '20190913_164036.jpg'),
(58, 1, 'egg and spoon.jpg'),
(59, 1, 'sack race 1.jpg'),
(60, 1, 'SPACE 1.jpg'),
(61, 1, 'twister.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `corporate_videos`
--

CREATE TABLE `corporate_videos` (
  `id` int(11) NOT NULL,
  `cp_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `corporate_videos`
--

INSERT INTO `corporate_videos` (`id`, `cp_id`, `title`, `video`) VALUES
(39, 5, 'Bungee Run', '<iframe width=\"100%\" height=\"350\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(40, 5, 'Assault Course', '<iframe width=\"100%\" height=\"350\" src=\"https://www.youtube.com/embed/fWwR3O83kw0\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(41, 5, 'Wrecking Ball', '<iframe width=\"100%\" height=\"350\" src=\"https://www.youtube.com/embed/8JS4HLUGV5s\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(42, 5, 'Gladiator Duel', '<iframe width=\"894\" height=\"506\" src=\"https://www.youtube.com/embed/LfSVaa0E61k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(43, 5, 'Sumo Suits', '<iframe width=\"100%\" height=\"350\" src=\"https://www.youtube.com/embed/TkW3XepxveQ\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(44, 5, 'Last Man standing', '<iframe width=\"899\" height=\"506\" src=\"https://www.youtube.com/embed/Q4_MPoULgHg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(45, 5, 'Dash and grab', '<iframe width=\"920\" height=\"506\" src=\"https://www.youtube.com/embed/7Xvr1zJ1QX4\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(63, 1, 'Blindfold egg and spoon', '<iframe width=\"640\" height=\"360\" src=\"https://www.youtube.com/embed/d3IDI8i2TuM\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(64, 1, 'Relay Race', '<iframe width=\"727\" height=\"409\" src=\"https://www.youtube.com/embed/FbKp6HHJhtQ\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(70, 1, 'Tug of war', '<iframe width=\"727\" height=\"409\" src=\"https://www.youtube.com/embed/ymitnthrS5c\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(71, 1, 'Sack Race', '<iframe width=\"727\" height=\"409\" src=\"https://www.youtube.com/embed/_wgrwMLlT-4\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(84, 1, 'Space Hopper', '<iframe width=\"670\" height=\"370\" src=\"https://www.youtube.com/embed/W9LuCtYLQqw\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(85, 1, 'Tunnell Ball', '<iframe width=\"640\" height=\"360\" src=\"https://www.youtube.com/embed/cPnvWi7jZcU\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(86, 1, '3 legged race', '<iframe width=\"640\" height=\"360\" src=\"https://www.youtube.com/embed/BUv_fqrUoKs\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(87, 1, 'Rounders Preview', '<iframe width=\"640\" height=\"360\" src=\"https://www.youtube.com/embed/XQxduGWxk3s\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(88, 2, 'Bungee Run', '<iframe width=\"670\" height=\"340\" src=\"https://www.youtube.com/embed/MbMzp6rIJ0M\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(89, 2, 'Assault Course and Slide', '<iframe width=\"670\" height=\"340\" src=\"https://www.youtube.com/embed/h1k3N4DqdhY\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(90, 2, 'Last Man Standing, Dash n Grab and Wrecking Ball', '<iframe width=\"670\" height=\"340\" src=\"https://www.youtube.com/embed/80Mi5jmXq-c\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(91, 2, 'Hungry Hippos', '<iframe width=\"670\" height=\"340\" src=\"https://www.youtube.com/embed/VEJXvGXKQpQ\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(92, 2, 'Sumo Suits', '<iframe width=\"670\" height=\"340\" src=\"https://www.youtube.com/embed/oRmk0SSqXJY\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(93, 2, 'Event set up example - centre stage', '<iframe width=\"670\" height=\"340\" src=\"https://www.youtube.com/embed/ySv9FXMrbF0\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(94, 3, 'Tug of war', '<iframe width=\"670\" height=\"340\" src=\"https://www.youtube.com/embed/M2kQm49DI38\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(95, 3, 'Assault Course and Slide', '<iframe width=\"670\" height=\"340\" src=\"https://www.youtube.com/embed/FrFBAsHz6tA\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(96, 3, 'Piggy Back challenge', '<iframe width=\"670\" height=\"340\" src=\"https://www.youtube.com/embed/jw3MInMejDA\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(97, 3, 'Wrecking Ball', '<iframe width=\"670\" height=\"340\" src=\"https://www.youtube.com/embed/as3PE9qa1es\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(98, 3, 'Bungee Run', '<iframe width=\"670\" height=\"340\" src=\"https://www.youtube.com/embed/pjmZ0SgEmuo\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(99, 3, 'Sumo Suits', '<iframe width=\"670\" height=\"340\" src=\"https://www.youtube.com/embed/oRmk0SSqXJY\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(100, 3, '3 inflatable knockout', '<iframe width=\"670\" height=\"340\" src=\"https://www.youtube.com/embed/80Mi5jmXq-c\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>');

-- --------------------------------------------------------

--
-- Table structure for table `cp_images`
--

CREATE TABLE `cp_images` (
  `id` bigint(127) NOT NULL,
  `cp_id` bigint(127) NOT NULL,
  `image` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_images`
--

INSERT INTO `cp_images` (`id`, `cp_id`, `image`) VALUES
(1, 1, 'about-img.jpg'),
(2, 1, 'child-page-img-2.jpg'),
(3, 2, 'about-img.jpg'),
(4, 3, 'about-img.jpg'),
(5, 6, 'about-img.jpg'),
(6, 6, 'child-page-img-2.jpg'),
(7, 6, 'detail-crousel-img.jpg'),
(9, 7, 'child-page-img-1.jpg'),
(10, 7, 'corporate-banner.jpg'),
(15, 8, 'oneday-img1.jpg'),
(16, 8, 'oneday-img2.jpg'),
(17, 9, 'oneday-img2.jpg'),
(18, 9, 'vanue-video-img.png'),
(20, 9, 'team-img1.jpg'),
(21, 6, 'football-party-rugby.jpg'),
(22, 7, 'champions-experience-thumb.jpg'),
(24, 8, 'children-coaching-thumb1.jpg'),
(25, 8, 'football-party-thumb2.jpg'),
(26, 9, 'champions-experience-thumb2.jpg'),
(33, 11, 'st johs wood 1.jpg'),
(34, 11, 'st johns 2.jpeg'),
(35, 11, 'st johs wood 3.jpg'),
(36, 11, 'st johns 4.jpg'),
(37, 12, 'Join-A-League.jpg'),
(38, 12, '1567602525.jpg'),
(39, 14, 'colindale and edgware 1.jpg'),
(40, 14, 'edgware 3.jpg'),
(41, 14, 'edgware 4.jpg'),
(42, 14, 'edgware 5.jpg'),
(43, 15, 'christ college 1.jpg'),
(44, 15, 'christ college 4.jpg'),
(45, 15, 'christ college 3.jpg'),
(46, 15, 'christ college 5.jpg'),
(47, 15, 'chris college 2.jpg'),
(48, 16, 'white city tiger turf 2.jpg'),
(49, 16, 'white city 5.jpg'),
(50, 16, 'white city tiger turf 1.jpg'),
(51, 16, 'white city 3.jpeg'),
(52, 17, 'hammersmith 2.jpg'),
(53, 17, 'Hammersmith-Academy-Sports-Hall.jpg'),
(54, 18, 'chisiwck 2.jpg'),
(58, 19, 'chiswick 7.jpg'),
(59, 19, 'barnes 3.jpg'),
(60, 20, 'wembley 2.jpg'),
(61, 20, 'wembley 3.jpg'),
(62, 20, 'wembley 4.jpg'),
(63, 20, 'wembley.jpg'),
(64, 20, 'wembley 5.jpg'),
(65, 21, 'fulham 3.jpg'),
(66, 21, 'fulham.jpg'),
(67, 21, 'fulham 2.jpg'),
(68, 22, 'waterloo 3.jpg'),
(69, 22, 'waterloo 2.jpg'),
(70, 22, 'waterloo.jpg'),
(71, 17, 'kid1.jpg'),
(72, 17, 'BUBBLE1.png'),
(73, 18, 'kids4.jpg'),
(74, 18, 'kids1.jpeg'),
(79, 24, 'IMG_2321001.jpg'),
(80, 24, 'IMG-20181128-WA0007.jpg'),
(81, 24, 'IMG_2321001.jpg'),
(82, 24, 'IMG_2324.jpg'),
(83, 25, 'stockwell 2.jpg'),
(84, 25, 'stockwell.jpg'),
(85, 25, 'stockwell 3.jpg'),
(86, 26, 'north greenwich 2.jpg'),
(87, 26, 'north greenwich.jpg'),
(88, 27, 'Canada Water 4.jpg'),
(89, 27, 'Canada Water.jpg'),
(90, 27, 'Canada Water 3.jpg'),
(91, 27, 'Canada Water 2.jpg'),
(92, 28, 'IMG_1084.jpg'),
(93, 28, 'IMG_0641.jpg'),
(94, 28, 'IMG_0643.jpg'),
(95, 29, 'bermondsey.jpg'),
(96, 30, 'lammas park 4.jpg'),
(97, 30, 'lammas park 3.jpg'),
(98, 30, 'lammas park 2.jpg'),
(99, 31, 'stratford 2.jpg'),
(100, 31, 'stratford 3.jpg'),
(101, 31, 'stratford 4.jpg'),
(102, 32, 'battersea 5.jpg'),
(103, 32, 'battersea 2.jpg'),
(104, 32, 'battersea.jpg'),
(105, 32, 'battersea 4.jpg'),
(107, 21, '1.jpg'),
(108, 21, '2.jpg'),
(109, 28, '3.jpg'),
(110, 28, '4.jpg'),
(111, 24, '5.jpg'),
(112, 24, '6.jpg'),
(113, 22, '7.jpg'),
(114, 22, '8.jpg'),
(115, 25, '9.jpg'),
(116, 25, '10.jpg'),
(117, 23, '11.jpg'),
(118, 23, '12.jpg'),
(119, 31, '13.jpg'),
(120, 31, '14.png'),
(121, 23, 'vauxhall.jpg'),
(122, 23, 'vauxhall 11.jpg'),
(123, 32, '1.jpg'),
(124, 32, '2.jpg'),
(125, 29, '3.jpg'),
(126, 29, '4.jpg'),
(127, 27, '5.jpg'),
(128, 27, '6.jpg'),
(129, 26, '8.jpg'),
(130, 26, '7.jpg'),
(131, 30, '9.jpg'),
(132, 30, '10.jpg'),
(133, 10, 'CAMDEN 1.jpg'),
(134, 10, 'CAMDEN 2.jpg'),
(135, 10, 'camden 3.jpg'),
(136, 33, 'CAMDEN 1.jpg'),
(137, 33, 'CAMDEN 2.jpg'),
(138, 33, 'camden 3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `cp_venue_facility`
--

CREATE TABLE `cp_venue_facility` (
  `id` bigint(127) NOT NULL,
  `cp_id` bigint(127) NOT NULL,
  `vf_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_venue_facility`
--

INSERT INTO `cp_venue_facility` (`id`, `cp_id`, `vf_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 2, 1),
(7, 2, 2),
(8, 2, 3),
(9, 3, 1),
(10, 3, 2),
(11, 3, 3),
(12, 4, 1),
(13, 4, 2),
(14, 4, 3),
(15, 4, 4),
(16, 4, 5),
(17, 4, 6),
(18, 4, 7),
(19, 5, 1),
(20, 5, 2),
(21, 5, 3),
(22, 5, 4),
(23, 5, 5),
(24, 5, 6),
(25, 5, 7),
(138, 7, 1),
(139, 7, 2),
(140, 7, 3),
(141, 7, 4),
(142, 7, 5),
(143, 7, 6),
(144, 7, 7),
(152, 9, 1),
(153, 9, 2),
(154, 9, 3),
(155, 9, 4),
(156, 9, 5),
(157, 9, 6),
(158, 9, 7),
(177, 8, 1),
(178, 8, 2),
(179, 8, 3),
(180, 8, 4),
(181, 8, 5),
(182, 8, 6),
(183, 8, 7),
(242, 12, 1),
(243, 12, 2),
(244, 12, 3),
(245, 12, 4),
(246, 12, 5),
(247, 12, 7),
(248, 6, 1),
(249, 6, 2),
(250, 6, 3),
(251, 13, 1),
(252, 13, 6),
(434, 11, 1),
(435, 11, 2),
(436, 11, 9),
(437, 11, 10),
(548, 14, 1),
(549, 14, 2),
(550, 14, 3),
(551, 14, 4),
(552, 14, 5),
(553, 14, 9),
(554, 14, 10),
(624, 15, 1),
(625, 15, 2),
(626, 15, 3),
(627, 15, 5),
(628, 15, 9),
(629, 15, 10),
(630, 16, 1),
(631, 16, 2),
(632, 16, 3),
(633, 16, 5),
(634, 16, 9),
(635, 16, 10),
(636, 17, 1),
(637, 17, 2),
(638, 17, 3),
(639, 17, 9),
(640, 17, 10),
(641, 18, 1),
(642, 18, 3),
(643, 18, 4),
(644, 18, 9),
(645, 19, 1),
(646, 19, 2),
(647, 19, 3),
(648, 19, 4),
(649, 19, 9),
(650, 20, 1),
(651, 20, 2),
(652, 20, 3),
(653, 20, 4),
(654, 20, 5),
(655, 20, 7),
(656, 20, 9),
(657, 20, 10),
(658, 20, 11),
(659, 21, 1),
(660, 21, 2),
(661, 21, 9),
(666, 23, 1),
(667, 23, 2),
(668, 23, 3),
(669, 23, 7),
(670, 23, 9),
(671, 23, 10),
(672, 24, 1),
(673, 24, 2),
(674, 24, 7),
(675, 24, 9),
(676, 24, 10),
(677, 25, 1),
(678, 25, 3),
(679, 25, 5),
(680, 25, 9),
(681, 25, 10),
(682, 26, 1),
(683, 26, 3),
(684, 26, 9),
(685, 26, 10),
(686, 27, 1),
(687, 27, 3),
(688, 27, 7),
(689, 27, 10),
(690, 28, 1),
(691, 28, 2),
(692, 28, 7),
(693, 28, 10),
(694, 29, 1),
(695, 29, 2),
(696, 29, 3),
(697, 29, 4),
(698, 29, 5),
(699, 29, 7),
(700, 29, 9),
(701, 29, 10),
(702, 30, 1),
(703, 30, 3),
(704, 30, 7),
(705, 30, 9),
(706, 30, 10),
(707, 31, 1),
(708, 31, 2),
(709, 31, 3),
(710, 31, 4),
(711, 31, 5),
(712, 31, 7),
(713, 31, 9),
(714, 31, 10),
(715, 32, 1),
(716, 32, 2),
(717, 32, 3),
(718, 32, 5),
(719, 32, 7),
(720, 32, 9),
(721, 32, 10),
(722, 10, 1),
(723, 10, 2),
(724, 10, 3),
(725, 10, 9),
(726, 10, 10),
(727, 33, 1),
(728, 33, 2),
(729, 33, 3),
(730, 33, 4),
(731, 33, 5),
(732, 33, 6),
(733, 33, 9),
(734, 22, 1),
(735, 22, 7),
(736, 22, 9),
(737, 22, 10);

-- --------------------------------------------------------

--
-- Table structure for table `game_comments`
--

CREATE TABLE `game_comments` (
  `id` bigint(127) NOT NULL,
  `user_id` bigint(127) DEFAULT NULL,
  `game_id` bigint(127) NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `comment_id` bigint(127) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `game_comments`
--

INSERT INTO `game_comments` (`id`, `user_id`, `game_id`, `comment`, `name`, `email`, `comment_id`, `created_at`, `updated_at`) VALUES
(1, NULL, 2, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Quisque eu libero. Sed sit amet metus. Cras ante nisi, ornare sit amet, pretium eget, tempus suscipit, lectus. Vivamus fermentum est vitae felis. Pellentesque habitant morbi tristique senectus et netus.', 'shirram chaurasiya', 'shriram@gmail.com', NULL, '2019-08-19 08:30:59', '2019-08-19 02:41:32'),
(2, NULL, 2, 'sdasdsd', 'shirram chaurasiya', 'shriram@gmail.com', NULL, '2019-08-19 08:31:01', '2019-08-19 02:52:17'),
(3, NULL, 2, 'asdasda', 'shirram chaurasiya', 'shriram@gmail.com', NULL, '2019-08-19 08:31:03', '2019-08-19 02:54:43'),
(4, 12, 2, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Quisque eu libero. Sed sit amet metus. Cras ante nisi, ornare sit amet, pretium eget, tempus suscipit, lectus. Vivamus fermentum est vitae felis. Pellentesque habitant morbi tristique senectus et netus.', 'jay kumar', 'ajay@gmail.com', NULL, '2019-08-19 03:00:45', '2019-08-19 03:00:45'),
(5, 12, 2, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Quisque eu libero. Sed sit amet metus. Cras ante nisi, ornare sit amet, pretium eget, tempus suscipit, lectus. Vivamus fermentum est vitae felis. Pellentesque habitant morbi tristique senectus et netus.', 'shirram chaurasiya', 'shriram@gmail.com', NULL, '2019-08-19 04:44:41', '2019-08-19 04:44:41'),
(6, 6, 2, 'Pellentesque habitant morbi tristique senectus et netus.', 'tapas', 'tpsvishwas78@gmail.com', 1, '2019-08-19 04:45:42', '2019-08-19 04:45:42'),
(7, 3, 5, 'asdasd', 'shirram chaurasiya', 'shriram@gmail.com', NULL, '2019-08-30 05:15:04', '2019-08-30 05:15:04'),
(8, 3, 5, 'asd', 'shirram chaurasiya', 'shriram@gmail.com', 7, '2019-08-30 05:16:22', '2019-08-30 05:16:22'),
(9, 6, 8, 'ji', 'John', 'Johndoe@gmail.com', NULL, '2019-09-17 11:27:26', '2019-09-17 11:27:26');

-- --------------------------------------------------------

--
-- Table structure for table `game_images`
--

CREATE TABLE `game_images` (
  `id` bigint(127) NOT NULL,
  `game_id` bigint(127) NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `game_images`
--

INSERT INTO `game_images` (`id`, `game_id`, `image`) VALUES
(2, 3, 'child-page-img-1.jpg'),
(3, 4, 'child-page-img-1.jpg'),
(4, 5, 'child-page-img-1.jpg'),
(5, 6, 'child-page-img-1.jpg'),
(6, 7, 'photo-1540747913346-19e32dc3e97e.jpg'),
(7, 8, 'NETBALL.jpg'),
(8, 8, 'womens netball 2.jpg'),
(9, 8, 'womens netball 3.jpg'),
(10, 8, 'womens netball 4.jpg'),
(11, 9, 'child-page-img-1.jpg'),
(13, 10, '9829_Fifa-world-cup-2018-Russia-football-field-HD-wallpaper.jpg'),
(14, 10, 'download.jpg'),
(15, 10, 'pexels-photo-399187.jpeg'),
(16, 11, '1.jpg'),
(17, 12, 'womens football 2.jpg'),
(18, 12, 'womens football 3.jpg'),
(20, 13, 'womens football 3.jpg'),
(21, 13, 'womens football 2.jpg'),
(22, 13, 'womens football.jpg'),
(23, 12, 'womens football 2.jpg'),
(24, 14, 'main image.jpg'),
(25, 14, '1567497888.jpg'),
(26, 14, 'womens netball 3.jpg'),
(27, 15, '828265588.jpg'),
(28, 15, 'image.jpg'),
(29, 15, 'Torneo_de_clasificación_WRWC_2014_-_Italia_vs_España_-_18.jpg'),
(34, 18, 'img-list6.png'),
(35, 18, 'detail-crousel-img.jpg'),
(36, 19, 'img-list6.png'),
(37, 19, 'img1.jpg'),
(38, 9, 'team-img2.jpg'),
(42, 16, 'football-harris-academy-thumb.jpg'),
(43, 16, 'football-harris-academy-thumb2.jpg'),
(44, 20, 'blue and white front 1.jpg'),
(45, 21, '831_media.jpg'),
(46, 21, '1567776148.jpg'),
(47, 22, 'football-party-thumb2.jpg'),
(48, 25, '1567677422.jpg'),
(49, 28, 'blue1.jpg'),
(50, 28, 'BADMINTONO1.jpg'),
(51, 28, 'BADMINTON2.jpg'),
(52, 29, '1567602525.jpg'),
(53, 30, 'american-football-1920x379.jpg'),
(55, 32, 'netball2.jpg'),
(56, 32, 'netball3.jpg'),
(57, 32, 'netball4.jpg'),
(58, 32, 'netball1.jpg'),
(59, 33, 'child-page-img-1.jpg'),
(60, 35, 'Vauxhall 5.JPG'),
(61, 35, 'Vauxhall 9.jpg'),
(62, 35, 'Vauxhall 4.JPG'),
(63, 35, 'Vauxhall 1.JPG'),
(65, 36, '1567602525.jpg'),
(66, 17, 'child-page-img-1.jpg'),
(67, 17, 'Join-A-League.jpg'),
(68, 37, 'BRIXTON.jpg'),
(76, 39, 'edecd323-009f-440b-ad5e-ecab5b805b74.jpg'),
(78, 38, '3816ccc5-c661-4b87-b0c6-e1c81a6265c6.jpg'),
(79, 41, 'child-page-img-1.jpg'),
(80, 42, 'camden 2.jpg'),
(83, 43, 'westbourne.png'),
(85, 43, 'wb3.jpg'),
(86, 44, 'wchap.jpg'),
(87, 44, 'wc 3.jpg'),
(88, 45, 'wloo 2.jpg'),
(89, 45, 'wloo.jpg'),
(90, 46, 'vauxhall 2.jpg'),
(91, 46, 'vauxhall 3.jpg'),
(92, 47, 'w city indoor.jpg'),
(93, 47, 'w city fk.jpg'),
(94, 48, 'w city indoor.jpg'),
(95, 48, 'w city fk.jpg'),
(96, 49, 'w city play.jpg'),
(97, 49, 'w city 2.jpg'),
(98, 50, 'w city outdoor.jpg'),
(99, 50, 'w city 2.jpg'),
(100, 50, 'Picture1-2.png'),
(105, 17, 'snows 2.jpg'),
(106, 17, 'snows.jpg'),
(109, 38, 'north greenwich mens and womens.jpg'),
(110, 38, 'womens football north greenwich or west p if womens.jpg'),
(111, 46, 'mixed image.jpg'),
(112, 45, 'waterloo (1).jpg'),
(113, 45, 'waterloo (2).jpg'),
(114, 43, 'westbourne park pitch 1.jpg'),
(115, 43, 'westbourne park 2.jpg'),
(116, 49, 'womens north greenwich and west p if you have west p dont care about watershed.jpg'),
(118, 39, 'mens greenwich.jpg'),
(119, 39, 'mens greenwich2.jpg'),
(120, 51, 'canning town 3.jpg'),
(121, 51, 'mixed image.jpg'),
(122, 51, 'CANNING TOWN MAIN.jpg'),
(123, 51, 'CANNING TOWN MAIN 2.jpg'),
(124, 44, 'women 2.jpg'),
(127, 37, 'IMG-20181126-WA0013.jpg'),
(131, 42, 'CAMDEN 2.jpg'),
(132, 42, 'cca1.png');

-- --------------------------------------------------------

--
-- Table structure for table `game_payment`
--

CREATE TABLE `game_payment` (
  `id` bigint(127) NOT NULL,
  `user_id` bigint(127) DEFAULT NULL,
  `game_author_id` bigint(127) DEFAULT NULL,
  `game_id` bigint(127) DEFAULT NULL,
  `order_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paid_amount` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `game_amount` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vat_amount` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commission_amount` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_status` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_order_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `game_payment`
--

INSERT INTO `game_payment` (`id`, `user_id`, `game_author_id`, `game_id`, `order_code`, `paid_amount`, `game_amount`, `vat_amount`, `commission_amount`, `currency`, `payment_status`, `payment_type`, `customer_order_code`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 7, '49163a24-4b6b-4e5f-b09f-dad987a300fe', '2.4', '2', '0.4', '0.2', 'GBP', 'SUCCESS', 'VISA_CREDIT', '1567412687', '2019-09-02 02:54:48', '2019-09-02 02:54:48'),
(2, 1, 2, 7, '9ead34a4-e40d-4647-97d2-684a723266f7', '2.4', '2', '0.4', '0.2', 'GBP', 'SUCCESS', 'VISA_CREDIT', '1567412777', '2019-09-02 02:56:18', '2019-09-02 02:56:18'),
(3, 1, 2, 7, '4181fd3b-a970-4768-95ec-1cdcca15af5e', '2.4', '2', '0.4', '0.2', 'GBP', 'SUCCESS', 'VISA_CREDIT', '1567412816', '2019-09-02 02:56:57', '2019-09-02 02:56:57'),
(4, 1, 2, 7, '295b99af-d2e6-4e43-a131-f673df1ce939', '2.4', '2', '0.4', '0.2', 'GBP', 'SUCCESS', 'VISA_CREDIT', '1567415977', '2019-09-02 03:49:38', '2019-09-02 03:49:38'),
(5, 1, 2, 7, '716e5788-1a2f-4b4b-8421-e945762a3c7b', '2.4', '2', '0.4', '0.2', 'GBP', 'SUCCESS', 'VISA_CREDIT', '1567416045', '2019-09-02 03:50:46', '2019-09-02 03:50:46'),
(6, 1, 2, 7, '93c7ec3f-6f10-4f9c-a83d-31d85f4c6629', '2.4', '2', '0.4', '0.2', 'GBP', 'SUCCESS', 'VISA_CREDIT', '1567416714', '2019-09-02 04:01:54', '2019-09-02 04:01:54'),
(7, 1, 2, 7, 'a49f24f7-d5f9-41d9-9678-03ade1b99ad4', '2.4', '2', '0.4', '0.2', 'GBP', 'SUCCESS', 'VISA_CREDIT', '1567425851', '2019-09-02 06:34:12', '2019-09-02 06:34:12'),
(8, 2, 1, 8, '8ee79acb-6381-4dfb-9b49-ca8178063b1b', '6', '5', '1', '0.5', 'GBP', 'SUCCESS', 'VISA_CREDIT', '1567499859', '2019-09-03 07:37:39', '2019-09-03 07:37:39'),
(9, 8, 11, 10, '6660a29e-d198-47f9-b43c-07d607638273', '180', '150', '30', '15', 'GBP', 'SUCCESS', 'VISA_CREDIT', '1567681148', '2019-09-05 09:59:08', '2019-09-05 09:59:08');

-- --------------------------------------------------------

--
-- Table structure for table `get_involved`
--

CREATE TABLE `get_involved` (
  `id` bigint(127) NOT NULL,
  `user_id` bigint(127) DEFAULT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `venue` int(11) DEFAULT NULL,
  `available_days` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sport_id` int(11) DEFAULT NULL,
  `comments` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `home_about_us`
--

CREATE TABLE `home_about_us` (
  `id` int(11) NOT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `home_about_us`
--

INSERT INTO `home_about_us` (`id`, `video`, `description`, `status`) VALUES
(1, '1565606547.mp4', 'Letsplaysports is a platform where users can connect with all sports across the UK. If you have just moved to a new area and looking for a casual game of Rugby or if you have 2-3 mates looking for a regular game of 5 a side, letsplaysports will have something for you. Make sure you download our app for easier access.\r\nYou can also list your own sessions, so if you have a regular game and need players then make sure you advertise your session.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `home_banners`
--

CREATE TABLE `home_banners` (
  `id` int(11) NOT NULL,
  `banner` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_one` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_two` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_three` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `button_link` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '1 image, 2 video',
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `home_banners`
--

INSERT INTO `home_banners` (`id`, `banner`, `title_one`, `title_two`, `title_three`, `button_link`, `banner_type`, `status`, `created_at`, `updated_at`) VALUES
(1, '1567596052.jpg', 'SEARCH AND PLAY TODAY', 'FIND - PLAY - CONNECT', 'YOUR PITCH YOUR GAME', 'https://webchoice-test.uk/lps/contactus', '1', 1, '2019-09-11 10:22:17', '2019-09-11 09:22:18'),
(3, '1567596062.jpg', 'SEARCH AND PLAY TODAY', 'FIND - PLAY - CONNECT', 'YOUR PITCH YOUR GAME', 'https://webchoice-test.uk/lps/contactus', '1', 1, '2019-09-11 10:22:25', '2019-09-11 09:22:25'),
(4, '1567596071.jpg', 'SEARCH AND PLAY TODAY', 'FIND - PLAY - CONNECT', 'YOUR PITCH YOUR GAME', 'https://webchoice-test.uk/lps/contactus', '1', 1, '2019-09-11 10:22:33', '2019-09-11 09:22:34');

-- --------------------------------------------------------

--
-- Table structure for table `invite_friend`
--

CREATE TABLE `invite_friend` (
  `id` bigint(127) NOT NULL,
  `user_id` bigint(127) NOT NULL,
  `invite_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `join_a_league`
--

CREATE TABLE `join_a_league` (
  `id` int(11) NOT NULL,
  `banner_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `join_a_league`
--

INSERT INTO `join_a_league` (`id`, `banner_title`, `banner`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 'JOIN A LEAGUE', '1568877216.jpg', '7-a-side footbal in london', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Quisque eu libero. Sed sit amet metus. Cras ante nisi, ornare sit amet, pretium eget, tempus suscipit, lectus. Vivamus fermentum est vitae felis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas commodo arcu ut odio. Curabitur tempus mi in erat. Praesent molestie magna placerat tellus. Nullam mauris. Nullam vel ipsum quis mi tincidunt vulputate. Cras mattis, lorem id condimentum tincidunt, quam dolor lobortis ligula, tincidunt lacinia ipsum est sit amet ante. Morbi sodales pellentesque neque. Etiam ac odio. In non orci vitae odio tempus facilisis. Pellentesque ut neque. Nullam enim augue, tincidunt eget, fermentum at, luctus nec, tellus. Pellentesque et, imperdiet vitae, mi. Sed nisl velit, viverra non, vehicula vel, dictum non, erat. Sed hendrerit, mauris id fringilla rutrum, pede ligula laoreet nunc, sit amet placerat ipsum risus tincidunt diam. Donec volutpat. Sed hendrerit, mauris id fringilla rutrum, pede ligula laoreet nunc, sit amet placerat ipsum risus tincidunt diam. Donec volutpat. Quisque elementum, tortor in consectetuer ultrices, mi diam sollicitudin nibh, eu scelerisque purus tortor vel elit. Ut suscipit eleifend odio. Proin dignissim mauris ut lacus. Suspendisse in erat. Proin semper facilisis tellus. Vivamus gravida rhoncus massa. ed nisl velit, viverra non, vehicula vel, dictum non, erat. Sed hendrerit, mauris id fringilla rutrum, pede ligula laoreet nunc, sit amet placerat ipsum risus tincidunt diam. Donec volutpat.</p>', '2019-09-19 01:43:36', '2019-09-19 07:13:09');

-- --------------------------------------------------------

--
-- Table structure for table `join_league_block`
--

CREATE TABLE `join_league_block` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `join_league_block`
--

INSERT INTO `join_league_block` (`id`, `title`, `description`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Amazing vanues', 'Superb central location the best playing surfaces competitive. social & fun Divisions to suit all abilities online fixtures. tables & results superior quality mitre & refs', '1568878435.png', '2019-09-19 02:03:55', '2019-09-19 02:03:55'),
(2, 'Flexible & convenient', 'Fixture request and re-arrangement convenient regular invoicing - perfecr for corporate teams top scorer competition & prizes follow the banter on our facebook & twitter pages', '1568878457.jpg', '2019-09-19 08:13:21', '2019-09-19 07:13:21');

-- --------------------------------------------------------

--
-- Table structure for table `kids_blogs`
--

CREATE TABLE `kids_blogs` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kids_blogs`
--

INSERT INTO `kids_blogs` (`id`, `category_id`, `title`, `slug`, `description`, `image`, `created_at`, `updated_at`) VALUES
(1, 4, 'Venue', 'venue-1', 'We have a whole host of venues from recognised sporting facilities to private indoor function rooms and community halls. So getting a location for your party will be a piece of cake.', '1568643699.jpg', '2019-09-24 12:52:05', '2019-09-24 11:52:05'),
(2, 4, 'Management And Staffing', 'management-and-staffing-2', 'All of our parties come with DBS checked staff who manage the entire event. Actors may also form part of our crew if you have a themed party. Subject to size of the party we also have a dedicated organiser for your event and point of contact on email to ensure your party experience is smooth and pleasurable.', '1569329865.jpg', '2019-09-24 12:57:44', '2019-09-24 11:57:45'),
(3, 4, 'Trophies And Extras', 'trophies-and-extras-3', 'Our packs include Trophies for birthday boy/girl. Medals can also be provided. \r\nCatering can also be provided on request.', '1568643762.jpg', '2019-09-24 13:02:20', '2019-09-24 12:02:20'),
(4, 5, 'Venue', 'venue-4', 'We have a whole host of venues from recognised sporting facilities to private indoor function rooms and community halls. So getting a location for your party will be a piece of cake.', '1569329550.jpg', '2019-09-24 12:52:30', '2019-09-24 11:52:30'),
(5, 5, 'Catering', 'catering-5', 'Catering can be provided to suit your desired theme.', '1568643825.jpg', '2019-09-24 13:03:02', '2019-09-24 12:03:02'),
(6, 4, 'Equipment', 'equipment-6', 'Equipment, Balls, Bibs and a coach is provided for all events.', '1569330229.jpg', '2019-09-24 13:03:49', '2019-09-24 12:03:49'),
(7, 5, 'Event Overview', 'event-overview-7', 'A full event overview will be provided prior to making any payment. So you can ensure your party is in the exact format you like.', '1568643973.jpg', '2019-09-24 13:05:07', '2019-09-24 12:05:07'),
(8, 5, 'Management And Staffing', 'management-and-staffing-8', 'All of our parties come with DBS checked staff who manage the entire event. Actors may also form part of our crew if you have a themed party. Subject to size of the party we also have a dedicated organiser for your event and point of contact on email to ensure your party experience is smooth and pleasurable.', '1569329848.jpg', '2019-09-24 12:57:28', '2019-09-24 11:57:28');

-- --------------------------------------------------------

--
-- Table structure for table `kids_details`
--

CREATE TABLE `kids_details` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secound_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secound_description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kids_details`
--

INSERT INTO `kids_details` (`id`, `category_id`, `title`, `description`, `image`, `secound_title`, `secound_description`, `status`, `created_at`, `updated_at`) VALUES
(4, 1, 'Under 3 soft play', '<p>Perfect for under 5&#39;s we have a range of soft play equipment along side toys, dolls and IPAD sets to keep your children entertained. With bright colours and everything set up prior to arrival you can ensure a fantastic soft play session whether its for a party or just for yourselves.</p>\r\n\r\n<p>Prices range from &pound;75-&pound;200 + Delivery. Staff can also be hired (DBS checked) for an extra fee and venues can be supplied.</p>', '1569316840.jpg', NULL, NULL, 1, '2019-09-14 09:03:29', '2019-09-27 12:38:30'),
(5, 1, '3- 5  years old', '<p>An epic adventure unfolds with bright coloured soft play items for ages 3-5. Featuring slides, balls ponds and customisable themes from circus parties to peppa pig you will be guaranteed a good time. Prices vary subject to children and range from &pound;90-&pound;400 + delivery subject to your specification. Staff and venue can also be provided subject to location</p>', '1569317306.jpg', NULL, NULL, 1, '2019-09-14 09:04:07', '2019-09-26 11:37:22'),
(6, 1, '5- 10 years old', '<p>Slides, ball ponds, obstacles all feature as part of the soft play challenge for 5-10 year olds. With customisable options to suit the numbers of children you can add on and take off anything you wish. Prices vary depending on numbers in attendance. &pound;120-&pound;1200 + Delivery. Staff (DBS checked) can be provided along with a venue if required.</p>', '1569317626.jpg', NULL, NULL, 1, '2019-09-14 09:04:53', '2019-09-26 11:37:34'),
(7, 1, '10-14 year olds', '<p>Soft play and inflatables make the perfect combination for your childrens party. Suitable for ages 10-14 we have fun soft play equipment as well as more challenging inflatables that can be added on to your package. Prices vary subject to numbers attending and range from &pound;200 - &pound;2000 + VAT subject to your specification. Staff (DBS checked) can be provided along with venue if required.</p>', '1569317861.jpg', NULL, NULL, 1, '2019-09-14 09:05:18', '2019-09-26 11:37:42'),
(8, 1, '15 + inflatable ideas', '<p>From aged 15 + we advise inflatables from big slides to sumo suits and assault couruse. Soft play is also possible however based on popular demand our inflatable packages are more popular and well recieved for this age group. Prices range from &pound;500- &pound;5000 subject to specification. Staff (DBS) are provided with all inflatables and Venue can also be proovided if required.</p>', '1568455544.jpg', NULL, NULL, 1, '2019-09-14 09:05:44', '2019-09-26 11:37:49'),
(9, 1, 'Lorem Ipsum', '<p>Soft play and inflatables make the perfect combination for your childrens party. Suitable for ages 10-14 we have fun soft play equipment as well as more challenging inflatables that can be added on to your package. Prices vary subject to numbers attending and range from &pound;200 - &pound;2000 + VAT subject to your specification. Staff (DBS checked) can be provided along with venue if required.</p>\r\n\r\n<p>Soft play and inflatables make the perfect combination for your childrens party. Suitable for ages 10-14 we have fun soft play equipment as well as more challenging inflatables that can be added on to your package. Prices vary subject to numbers attending and range from &pound;200 - &pound;2000 + VAT subject to your specification. Staff (DBS checked) can be provided along with venue if required.</p>\r\n\r\n<p>Soft play and inflatables make the perfect combination for your childrens party. Suitable for ages 10-14 we have fun soft play equipment as well as more challenging inflatables that can be added on to your package. Prices vary subject to numbers attending and range from &pound;200 - &pound;2000 + VAT subject to your specification. Staff (DBS checked) can be provided along with venue if required.</p>', '1568455569.jpg', NULL, NULL, 0, '2019-09-14 09:06:09', '2019-09-26 13:46:48'),
(10, 1, 'Lorem Ipsum', 'Dummy Content', '1568455601.jpg', NULL, NULL, 0, '2019-09-14 09:06:41', '2019-09-24 08:41:05'),
(11, 1, 'Lorem Ipsum', 'Here is your 5-6 lines Dummy Content.', '1568455632.jpg', NULL, NULL, 0, '2019-09-14 09:07:12', '2019-09-24 08:41:18'),
(12, 1, 'Lorem Ipsum', 'Dummy Content', '1568455652.jpg', NULL, NULL, 0, '2019-09-14 09:07:32', '2019-09-24 08:41:12'),
(13, 1, 'Lorem Ipsum', 'Dummy content', '1568455673.jpg', NULL, NULL, 0, '2019-09-14 09:07:53', '2019-09-24 08:41:24'),
(14, 1, 'Lorem Ipsum', 'Dummy Content is simple', '1568455699.jpg', NULL, NULL, 0, '2019-09-14 09:08:19', '2019-09-26 12:17:50'),
(15, 1, 'Lorem Ipsum', 'Dummy Content is simple', '1568455714.jpg', NULL, NULL, 0, '2019-09-14 09:08:34', '2019-09-24 08:41:42'),
(16, 3, 'Bouncy Castle Hire', '<p>Our most popular package consists of bouncy castle hire for ages between 2-12. Starting from &pound;150 + vat and delivery we can cater for any party. Staff (DBS checked) and venue can be provided on request</p>', '1569325091.png', NULL, NULL, 1, '2019-09-14 09:09:13', '2019-09-26 11:38:25'),
(17, 3, 'Assault Courses', '<p>Our Assault courses range from 40ft to 200ft. Assault courses vary subject to age and a bespoke package can be put together based on your requirements. Staff (DBS checked) can be provided and venue can be provided on request.</p>', '1569325257.gif', NULL, NULL, 1, '2019-09-14 09:09:34', '2019-09-26 11:39:03'),
(18, 3, 'Football Themed inflatables', '<p>Human Table Football and Football Darts. A dream inflatable football party. Hire 1 or both in our football package from &pound;150 + delivery. Staff (DBS checked) included and venue can be provided on request</p>', '1569326045.jpg', NULL, NULL, 1, '2019-09-14 09:09:52', '2019-09-26 11:38:55'),
(19, 3, 'Bubble Football / Zorb Ball', '<p>Zorb Ball has many themes. From football themed &quot;bubble football&quot; to Bulldog and Tag this inflatable bubble has everyone in hysterics. Suitable for 6 years and over. Prices from &pound;150 + VAT Staff (DBS checked) are provided and venue can be made available on request.</p>', '1569326224.png', NULL, NULL, 1, '2019-09-14 09:10:08', '2019-09-26 11:39:08'),
(20, 3, 'Lorem Ipsum', 'Dummy Content is simple', '1568455827.jpg', NULL, NULL, 0, '2019-09-14 09:10:27', '2019-09-24 08:42:25'),
(21, 3, 'Lorem Ipsum', 'Dummy Content is simple', '1568455841.jpg', NULL, NULL, 0, '2019-09-14 09:10:41', '2019-09-24 08:42:36'),
(33, 4, 'SPORTS PARTIES', '<p>Leave the organisation of your party to us. With DBS checked coaches available to coach and referee all sports we are confident to provide you everything you need. Not only can we manage your event but we can also provide a suitable venue for all attendees.</p>', '1568719292.jpg', 'Format of party', '<p>Our parties generally follow the same format.</p>\r\n\r\n<p>- Introduction to coach and welcome. Fun ice breaker game. (Heads or tails)&nbsp;</p>\r\n\r\n<p>- Coach begins fun games and provides all balls and bibs</p>\r\n\r\n<p>- Coach organises a more official game between all players</p>\r\n\r\n<p>- Game ends. Birthday boy or girl gets trophy and medal. All sing Happy birthday.</p>\r\n\r\n<p>- Party moves to canteen or food area for cake and snacks / meal (can be provided)</p>\r\n\r\n<p>- Pack up and finish. Party bags taken away (can be provided)</p>', 1, '2019-09-16 13:11:07', '2019-09-30 08:27:32'),
(34, 5, 'THEMED PARTIES', '<p>Looking to host something special for your kids party. We tailor make your childs party to any theme you wish. Nothing is out of reach.</p>', '1569329425.jpg', 'Popular themes', '<p>We can create any themed parties, our most popular events are:</p>\r\n\r\n<p>Spider Man Party</p>\r\n\r\n<p>Batman Party</p>\r\n\r\n<p>Peppa Pig Party&nbsp;</p>\r\n\r\n<p>Football Themed party (club the child supports)</p>\r\n\r\n<p>Princess and Cinderella Party</p>\r\n\r\n<p>Frozen Party</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Prices are subject to location and numbers. Catering can be provided on request. The package includes full decor of the venue (prices vary subject to size) along with actors if appropriate (in costumes). A venue can be provided if required. All staff are DBS checked.&nbsp;&nbsp;</p>', 1, '2019-09-16 13:17:18', '2019-09-24 11:50:25');

-- --------------------------------------------------------

--
-- Table structure for table `kids_gallery`
--

CREATE TABLE `kids_gallery` (
  `id` int(11) NOT NULL,
  `kid` int(11) NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kids_gallery`
--

INSERT INTO `kids_gallery` (`id`, `kid`, `image`) VALUES
(2, 33, 'img4.jpg'),
(8, 33, 'image 1.jpg'),
(9, 33, 'image 2.jpg'),
(10, 33, 'kidsports.jpg'),
(11, 34, 'theme1.jpg'),
(12, 34, 'theme2.jpg'),
(13, 34, 'theme3.jpg'),
(15, 34, 'how-to-throw-the-ultimate-jungle-party.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `kids_whats_included`
--

CREATE TABLE `kids_whats_included` (
  `id` int(11) NOT NULL,
  `kid` int(11) DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kids_whats_included`
--

INSERT INTO `kids_whats_included` (`id`, `kid`, `title`, `description`) VALUES
(1, 33, 'Lorem ipsum', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.Lorem ipsum dolor sit amet, consectetuer adipiscing elit.'),
(2, 33, 'Lorem ipsum', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.Lorem ipsum dolor sit amet, consectetuer adipiscing elit.'),
(3, 33, 'Lorem ipsum', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.Lorem ipsum dolor sit amet, consectetuer adipiscing elit.'),
(4, 33, 'Lorem ipsum', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.Lorem ipsum dolor sit amet, consectetuer adipiscing elit.'),
(5, 33, 'Lorem ipsum', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.Lorem ipsum dolor sit amet, consectetuer adipiscing elit.'),
(6, 34, 'Lorem Ipsum Included', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.Lorem ipsum dolor sit amet, consectetuer adipiscing elit.'),
(7, 34, 'Lorem Ipsum Included', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.Lorem ipsum dolor sit amet, consectetuer adipiscing elit.'),
(8, 34, 'Lorem Ipsum Included', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.Lorem ipsum dolor sit amet, consectetuer adipiscing elit.'),
(9, 34, 'Lorem Ipsum Included', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.Lorem ipsum dolor sit amet, consectetuer adipiscing elit.'),
(10, 34, 'Lorem Ipsum Included', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.Lorem ipsum dolor sit amet, consectetuer adipiscing elit.'),
(11, 34, 'Lorem Ipsum Included', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.Lorem ipsum dolor sit amet, consectetuer adipiscing elit.'),
(12, 33, 'test', 'asas'),
(13, 33, 'asas', 'asassasasas');

-- --------------------------------------------------------

--
-- Table structure for table `looking_venue_players`
--

CREATE TABLE `looking_venue_players` (
  `id` bigint(127) NOT NULL,
  `user_id` bigint(127) NOT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `venue_id` int(11) NOT NULL,
  `is_recurring_booking` tinyint(1) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `slug`, `status`) VALUES
(2, 'PRIVACY POLICY', 'privacy-policy', 1),
(3, 'TERMS & CONDITIONS', 'terms-conditions', 1),
(4, 'ABOUT US', 'about-us', 1);

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` bigint(127) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `receiver_id` bigint(127) NOT NULL,
  `sender_id` bigint(127) NOT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(49, '2014_10_12_000000_create_users_table', 1),
(50, '2014_10_12_100000_create_password_resets_table', 1),
(51, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(52, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(53, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(54, '2016_06_01_000004_create_oauth_clients_table', 1),
(55, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(56, '2019_08_28_105045_create_notifications_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mst_area_intrest`
--

CREATE TABLE `mst_area_intrest` (
  `id` smallint(5) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_area_intrest`
--

INSERT INTO `mst_area_intrest` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Northumberlund', 'asdasda', 1, '2019-08-05 14:20:16', '2019-08-05 08:50:16'),
(3, 'west yorkshire', NULL, 1, '2019-08-05 08:49:14', '2019-08-05 08:49:14'),
(4, 'shropshire', NULL, 1, '2019-08-05 08:49:18', '2019-08-05 08:49:18'),
(5, 'kent', NULL, 1, '2019-08-05 08:49:21', '2019-08-05 08:49:21'),
(6, 'cumbria', NULL, 1, '2019-08-05 08:49:25', '2019-08-05 08:49:25'),
(7, 'south yorks', NULL, 1, '2019-08-05 08:49:28', '2019-08-05 08:49:28'),
(8, 'west midlands', NULL, 1, '2019-08-05 08:49:33', '2019-08-05 08:49:33'),
(9, 'somerset', NULL, 1, '2019-08-05 08:49:36', '2019-08-05 08:49:36'),
(10, 'durham', NULL, 1, '2019-08-05 08:49:40', '2019-08-05 08:49:40'),
(11, 'derbs', NULL, 1, '2019-08-05 08:49:43', '2019-08-05 08:49:43'),
(12, 'worcs warks', NULL, 1, '2019-08-05 08:49:47', '2019-08-05 08:49:47'),
(13, 'oxaon', NULL, 1, '2019-08-05 08:49:50', '2019-08-05 08:49:50'),
(14, 'tyne & wear', NULL, 1, '2019-08-05 08:49:53', '2019-08-05 08:49:53'),
(15, 'staffs', NULL, 1, '2019-08-05 08:49:57', '2019-08-05 08:49:57'),
(16, 'herts', NULL, 1, '2019-08-05 08:50:00', '2019-08-05 08:50:00'),
(17, 'hampshire', NULL, 1, '2019-08-05 08:50:04', '2019-08-05 08:50:04');

-- --------------------------------------------------------

--
-- Table structure for table `mst_games`
--

CREATE TABLE `mst_games` (
  `id` bigint(127) NOT NULL,
  `user_id` bigint(127) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `featured_img` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `game_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `game_start_time` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `game_finish_time` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `venue_id` int(11) NOT NULL,
  `sport_id` bigint(127) DEFAULT NULL,
  `is_private` tinyint(1) DEFAULT NULL,
  `is_repeat` tinyint(1) NOT NULL DEFAULT 0 COMMENT '1 Every week, 2 Every 2 weeks, 3 Every month',
  `days` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `team_limit` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `gender` tinyint(3) NOT NULL COMMENT '1 Co-ed, 2 male, 3 female',
  `payment` tinyint(3) NOT NULL COMMENT '1 Online, 2 cash, 3 free',
  `price` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_refund` tinyint(1) DEFAULT NULL,
  `refund_changed_rsvp` tinyint(3) DEFAULT NULL,
  `refund_days` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_admin` int(11) DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 2 COMMENT '0 deactive ,1 active, 2 Pending Approval',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_games`
--

INSERT INTO `mst_games` (`id`, `user_id`, `title`, `slug`, `featured_img`, `description`, `game_date`, `end_date`, `game_start_time`, `game_finish_time`, `venue_id`, `sport_id`, `is_private`, `is_repeat`, `days`, `team_limit`, `gender`, `payment`, `price`, `currency`, `is_refund`, `refund_changed_rsvp`, `refund_days`, `is_admin`, `status`, `created_at`, `updated_at`) VALUES
(12, 4, 'London bridge 5 a side football', 'london-bridge-5-a-side-football', '1569876337.jpg', '<p>A beautiful new 4g pitch in London bridge perfect for 5 a side football.&nbsp; Start time is 8pm and the facility is completely private but there are sample benches and park space locally if you wish to warm up.&nbsp;&nbsp;Balls and bibs are provided so all you need to do is turn up.</p>', '2019-09-30', '1970-01-01', '8:00 PM', '9:00 PM', 13, 10, NULL, 0, 'N;', '5', 3, 1, '6', 'GBP', NULL, NULL, NULL, 1, 1, '2019-09-30 20:45:36', '2019-09-30 19:45:37'),
(13, 4, 'Wandsworth Football', 'wandsworth-football', '1568052808.jpg', '<p>Located in the heart of wandsworth town this fantastic 4g pitch is 30 seconds away from wandsworth town station and also accessible from clapham junction. Parking is provided and toilet facilities are on site. The pitch is european 4G with the taller 3m * 2m futsal goals and run offs allowing for some break in play but also for a more technical game.&nbsp;</p>', '2019-09-25', NULL, '7:00 PM', '8:00 PM', 11, 10, NULL, 0, NULL, '5', 2, 1, '6', 'GBP', NULL, NULL, NULL, 1, 1, '2019-09-12 13:27:03', '2019-09-12 12:27:03'),
(14, 4, 'Netball St Faiths 4g pitch Wandsworth TOWN', 'netball-st-faiths-4g-pitch-wandsworth-town', '1568053163.jpg', '<p>Located in the heart of wandsworth town this fantastic new&nbsp;pitch is 30 seconds away from wandsworth town station and also accessible from clapham junction. Parking is provided and toilet facilities are on site. The pitch is multi sport and allows for a good level of netball to be played. Beginners all welcome as we dont take our selves too seriously and this is meant as a fun and social group. Balls and bibs are provided</p>', '2019-09-26', NULL, '7:00 PM', '8:00 PM', 11, 2, NULL, 0, NULL, '5', 3, 1, '6', 'GBP', NULL, NULL, NULL, 1, 1, '2019-09-09 17:19:23', '2019-09-09 17:19:23'),
(15, 4, 'Tag Rugby (mixed)', 'tag-rugby-mixed', '1568053546.jpg', '<p>Played on grass our fantastic tag rugby session is a perfect way to stay fit. Played on pitch 5 or 6 in the heart of clapham its easy to access and also on traditional grass. Changing rooms are available although do close at 330 therefore we do advise to come changed. Men and women are allowed.</p>', '2019-09-29', NULL, '3:00 PM', '5:00 PM', 14, 5, NULL, 0, NULL, '5', 3, 1, '6', 'GBP', NULL, NULL, NULL, 1, 1, '2019-09-09 17:25:46', '2019-09-09 17:25:46'),
(17, 4, 'Football Snowsfields School', 'football-snowsfields-school', '1568197680.jpg', '<p>A beautiful new 4g pitch in London bridge perfect for 5 a side football.&nbsp; Start time is 8pm and the facility is completely private but there are ample benches and park space locally if you wish to warm up.&nbsp;&nbsp;Balls and bibs are provided so all you need to do is turn up.</p>', '2019-09-30', '2019-09-30', '8:00 PM', '9:00 PM', 13, 10, NULL, 0, 'N;', '5', 2, 1, '6', 'GBP', NULL, NULL, NULL, 1, 1, '2019-09-30 20:41:57', '2019-09-30 19:41:58'),
(18, 4, 'Football St Faiths 4g pitch Wandsworth TOWN', 'football-st-faiths-4g-pitch-wandsworth-town', '1568116429.jpg', '<p>Located in the heart of wandsworth town this fantastic 4g pitch is 30 seconds away from wandsworth town station and also accessible from clapham junction. Parking is provided and toilet facilities are on site. The pitch is european 4G with the taller 3m * 2m futsal goals and run offs allowing for some break in play but also for a more technical game.&nbsp;</p>', '2019-09-25', NULL, '8:00 PM', '9:00 PM', 11, 10, NULL, 0, NULL, '5', 2, 1, '6', 'GBP', NULL, NULL, NULL, 1, 1, '2019-09-10 11:53:49', '2019-09-10 10:53:49'),
(20, 4, 'Womens Football Bermondsey Fridays', 'womens-football-bermondsey-fridays-20', '1568970103.jpg', 'Every Friday 630pm 6 a side football on 4G pitch', '2019-09-20', NULL, '6:30 PM', '8:00 PM', 13, 11, NULL, 0, NULL, '6', 3, 1, '6', 'GBP', 1, 1, NULL, 0, 1, '2019-09-20 08:01:43', '2019-09-20 08:01:43'),
(21, 4, 'UK Test Tennis', 'uk-test-tennis-21', '1569065076.jpg', 'adsfadf asdfadsf asdfadsaf', '2019-09-24', NULL, '12:00 AM', '2:05 AM', 17, 4, NULL, 0, NULL, '7', 2, 1, '20', 'GBP', NULL, NULL, NULL, 0, 0, '2019-09-28 14:41:58', '2019-09-28 13:41:58'),
(22, 4, 'Test title for tennis game', 'test-title-for-tennis-game-22', '1569076022.jpg', 'This is the test description for this game.', '2019-09-27', NULL, '4:45 PM', '5:35 PM', 18, 4, NULL, 0, NULL, '6', 2, 1, '10', 'GBP', NULL, NULL, NULL, 0, 2, '2019-09-27 07:35:27', '2019-09-27 06:35:13'),
(28, 4, 'London bridge badminton Sessions', 'london-bridge-badminton-sessions', '1569576069.jpg', '<p>Join our newly formed Badminton Club playing in London Bridge, Waterloo and other venues across London. Ensure you register your interest to be kept up to date with weekend and weekday sessions.&nbsp;</p>', '2019-09-28', '2019-11-30', '12:00 PM', '1:00 PM', 13, 8, NULL, 1, 'a:1:{i:0;s:1:\"6\";}', '10', 2, 1, '7.50', 'GBP', 1, NULL, NULL, 1, 1, '2019-09-27 09:21:10', '2019-09-27 08:21:10'),
(32, 4, 'Womens Netball Euston', 'womens-netball-euston', '1569584252.jpg', '<p>Womens netball every Thursday, fun and friendly balls and bibs provided.</p>', '2019-10-10', '2019-12-12', '7:00 PM', '8:00 PM', 32, 2, NULL, 1, 'a:1:{i:0;s:1:\"4\";}', '8', 3, 1, '6', 'GBP', 1, NULL, NULL, 1, 1, '2019-09-29 08:00:54', '2019-09-29 07:00:54'),
(35, 4, 'Vauxhall Football Mens', 'vauxhall-football-mens', '1569584922.JPG', '<p>7 a side football played at the fantastic vauxhall location, located in between Lambeth north and Vauxhall (also accessible from Waterloo)</p>\r\n\r\n<p>Mens session for &pound;6 played in a fun and friendly atmosphere</p>\r\n\r\n<p>Balls and bibs provided.&nbsp;</p>\r\n\r\n<p>Toilets, showers and changing available.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>- Vauxhall underground station (10 mins walk)</p>\r\n\r\n<p>- Lambeth North underground station&nbsp;(10 mins walk)</p>', '2019-10-06', '2019-12-15', '1:00 PM', '2:00 PM', 34, 12, NULL, 1, 'a:1:{i:0;s:1:\"7\";}', '7', 1, 1, '6', 'GBP', 1, NULL, NULL, 1, 1, '2019-09-27 11:50:33', '2019-09-27 10:50:33'),
(37, 4, 'Brixton Mens 5 a side Football', 'brixton-mens-5-a-side-football', '1569769831.jpg', '<p><strong>Match length</strong>: 1 hour</p>\r\n\r\n<p><strong>Meet time</strong>: 10 minutes before kick off</p>\r\n\r\n<p><strong>Surface:</strong> 4G , moulds and blades are ok to use</p>\r\n\r\n<p><strong>Equipment</strong>: Balls and bibs will be provided and a site manager will be available with any questions.</p>\r\n\r\n<p><strong>Facilities:</strong> Toilets and a changing block are available but no showers.</p>\r\n\r\n<p><img src=\"https://www.hireapitch.com/images/tube.png\" />&nbsp;Brixton (5 mins walk)</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>General play.</strong></p>\r\n\r\n<p>All our games are played in a fun and friendly atmosphere with payments made prior to the game, this ensures that people actually show up and play. All no shows will be marked and should you consistently no show we can no longer accept you in our session.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Match Rules</strong><br />\r\n- You are not allowed in the Area. Goalkeeper also must remain in his area.<br />\r\n- Back pass to keeper allowed<br />\r\n- Every player gets to be Goalie after every 5-7&nbsp;minutes Goalie Rotation<br />\r\n- No aggressive behaviour, be in mood to have fun and a good game !!<br />\r\n- Everyone must play fair, respect their opponents and fellow players</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Directions</strong></p>\r\n\r\n<p>The pitch is located via an entrance on &quot;villa road&quot; there will be a green gate which should be open, you can also access the facility via wiltshire road and come from the front reception. Brixton underground station is very close. The venue is just off brixton road with buses 3, 59, 133,415 and 159 stopping here.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>', '2019-10-03', '2019-12-19', '8:00 PM', '9:00 PM', 35, 11, NULL, 1, 'a:1:{i:0;s:1:\"4\";}', '5', 2, 1, '6', 'GBP', 1, NULL, NULL, 1, 1, '2019-10-01 13:21:08', '2019-10-01 12:21:08'),
(38, 4, 'North Greewich Women\'s Football', 'north-greewich-womens-football', '1569877190.jpg', '<p>&nbsp;5 a side football session&nbsp;held in our popular North Greenwich location&nbsp;at Millenium Academy,.</p>\r\n\r\n<p>Women&#39;s&nbsp;session for &pound;6 played in an fun and energetic environment.&nbsp;</p>\r\n\r\n<p>Balls and bibs provided.&nbsp;</p>\r\n\r\n<p>Toilets&nbsp;and changing available.</p>\r\n\r\n<p><img src=\"https://www.hireapitch.com/images/tube.png\" />&nbsp;North Greenwich (10 mins walk)</p>\r\n\r\n<p><img src=\"https://www.hireapitch.com/images/rail.png\" />&nbsp;Westcombe Park / Charlton (20 mins walk)</p>', '2019-10-01', '2019-12-17', '8:00 PM', '9:00 PM', 36, 11, NULL, 1, 'a:1:{i:0;s:1:\"2\";}', '5', 3, 1, '6', 'GBP', 1, NULL, NULL, 1, 1, '2019-09-30 20:59:50', '2019-09-30 19:59:50'),
(39, 4, 'North Greewich Men\'s Football', 'north-greewich-mens-football', '1569873930.jpg', '<p>5 a side football session&nbsp;held in our&nbsp;popular North Greenwich location&nbsp;at Millenium Academy, known for brilliant it&#39;s 5, 6 and 7 a side facilities.&nbsp;</p>\r\n\r\n<p>Men&#39;s&nbsp;session for &pound;6 played in an relaxed and friendly atmosphere</p>\r\n\r\n<p>Balls and bibs provided.&nbsp;</p>\r\n\r\n<p>Toilets&nbsp;and changing available.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img src=\"https://www.hireapitch.com/images/tube.png\" />&nbsp;North Greenwich (10 mins walk)</p>\r\n\r\n<p><img src=\"https://www.hireapitch.com/images/rail.png\" />&nbsp;Westcombe Park / Charlton (20 mins walk)</p>\r\n\r\n<p>&nbsp;</p>', '2019-10-01', '2019-12-17', '8:00 PM', '9:00 PM', 36, 11, NULL, 1, 'a:1:{i:0;s:1:\"2\";}', '5', 2, 1, '6', 'GBP', 1, NULL, NULL, 1, 1, '2019-09-30 20:59:30', '2019-09-30 19:59:30'),
(40, 1, 'first game woomen', 'first-game-woomen-40', '1569824934.jpg', 'sdfsf', '2019-09-30', '1970-01-01', '10:00 AM', '11:00 AM', 17, 4, NULL, 1, NULL, '8', 3, 1, '5', 'GBP', NULL, NULL, NULL, 0, 2, '2019-09-30 05:28:54', '2019-09-30 05:28:54'),
(41, 21, 'Test for tag rugby', 'test-for-tag-rugby-41', '1569842157.jpg', 'adsfasdfasdf asdfadfas sdfasferer vfgfgrtr', '2019-10-03', '2019-10-09', '8:00 AM', '9:00 AM', 16, 7, NULL, 1, NULL, '17', 3, 1, '10', 'GBP', 1, NULL, NULL, 0, 0, '2019-10-01 11:35:06', '2019-10-01 10:35:06'),
(42, 4, 'Camden 9 a side Mixed Football', 'camden-9-a-side-mixed-football', '1569865779.jpg', '<p><strong>Match length</strong>: 1 hour</p>\r\n\r\n<p><strong>Meet time</strong>: 10 minutes before kick off</p>\r\n\r\n<p><strong>Surface:</strong>&nbsp;New astrotuf&nbsp;, moulds and blades are ok to use but trainers and astroturf recommended</p>\r\n\r\n<p><strong>Equipment</strong>: Balls and bibs will be provided and a site manager will be available with any questions.</p>\r\n\r\n<p><strong>Facilities:</strong> Toilets and a changing block with showers.</p>\r\n\r\n<p><img src=\"https://www.hireapitch.com/images/tube.png\" />&nbsp;Camden Road (4 mins walk)</p>\r\n\r\n<p><img src=\"https://www.hireapitch.com/images/tube.png\" />&nbsp;Camden Town (6&nbsp;mins walk)</p>\r\n\r\n<p><img src=\"https://www.hireapitch.com/images/tube.png\" />&nbsp;Kentish Town west (12&nbsp;mins walk)</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>General play.</strong></p>\r\n\r\n<p>All our games are played in a fun and friendly atmosphere with payments made prior to the game, this ensures that people actually show up and play. All no shows will be marked and should you consistently no show we can no longer accept you in our session.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Match Rules</strong><br />\r\n- You are allowed in the Area.&nbsp;<br />\r\n- Back pass to keeper allowed<br />\r\n- Every player gets to be Goalie after every 5-7&nbsp;minutes Goalie Rotation<br />\r\n- No aggression or slide tackling</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Directions</strong></p>\r\n\r\n<p>The pitch is located via an entrance on &quot;castlehaven road&quot; the pitch cannot be missed as its a fantastically big pitch. If you struggle to find it from camden road or camden town then Buses 24,27,31,168 will put you 2 mins walk from the venue.</p>\r\n\r\n<p>&nbsp;</p>', '2019-10-02', '2019-12-18', '9:00 PM', '10:00 PM', 37, 10, NULL, 1, 'a:1:{i:0;s:1:\"3\";}', '9', 1, 1, '6', 'GBP', 1, NULL, NULL, 1, 1, '2019-10-01 13:28:27', '2019-10-01 12:28:28'),
(43, 4, 'Westbourne Park 5 a side Football', 'westbourne-park-5-a-side-football', '1569866544.jpg', '<p>5 a side men&#39;s football at our delightfully spacious Westbourne Park Astro Turf facility. Come to enjoy some recreational football at this Beautful facility&nbsp;and make some new acquaintances!</p>\r\n\r\n<p>Balls and bibs provided.&nbsp;</p>\r\n\r\n<p><img src=\"https://www.hireapitch.com/images/tube.png\" />&nbsp;Westbourne Park (10 mins walk)</p>', '2019-10-07', '2019-12-23', '6:00 PM', '7:00 PM', 38, 11, NULL, 1, 'a:1:{i:0;s:1:\"1\";}', '5', 2, 1, '6', 'GBP', 1, NULL, NULL, 1, 1, '2019-09-30 20:47:44', '2019-09-30 19:47:45'),
(44, 4, 'Whitechapel 5 a side Football Session', 'whitechapel-5-a-side-football-session', '1569877395.jpg', '<p>It&#39;s ladies night!</p>\r\n\r\n<p>Enjoy a night of 5 a side football every Thursday at out Whitechapel Location in the heart of the east end at our 4G Pitch in John Scurr Primary! Whether making friends, getting out of the house or blowing off steam after work, we&#39;ve got you covered!</p>\r\n\r\n<p>Balls and bibs provided.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>On-street Parking Available</p>', '2019-10-03', '2019-12-19', '7:00 PM', '8:00 PM', 39, 11, NULL, 1, 'a:1:{i:0;s:1:\"4\";}', '5', 3, 1, '6', 'GBP', 1, NULL, NULL, 1, 1, '2019-09-30 21:03:15', '2019-09-30 20:03:15'),
(45, 4, 'Waterloo 5 a side Football', 'waterloo-5-a-side-football', '1569867748.jpg', '<p>Enjoy Men&#39;s 5 a side at our Prestigious Waterloo Wolf Turf complete with changing facilities. Get the most out of your weekends by attending our hour long session of non-competetive recreational football - and who knows? You may make some friends along the way. Whether it&#39;s to make new friends in the area or get some excercise in - join us every sunday.</p>\r\n\r\n<p>Balls and bibs provided.&nbsp;</p>\r\n\r\n<p><img src=\"https://www.hireapitch.com/images/tube.png\" />&nbsp;Waterloo- 2 mins walk</p>\r\n\r\n<p><img src=\"https://www.hireapitch.com/images/tube.png\" />&nbsp;Lambeth north- 5 mins walk</p>\r\n\r\n<p><img src=\"https://www.hireapitch.com/images/tube.png\" />&nbsp;Southwark- 7 mins walk</p>', '2019-10-06', '2019-12-22', '6:00 PM', '7:00 PM', 41, 11, NULL, 1, 'a:1:{i:0;s:1:\"7\";}', '5', 2, 1, '6', 'GBP', 1, NULL, NULL, 1, 1, '2019-09-30 20:47:29', '2019-09-30 19:47:29'),
(46, 4, 'Vauxhall 7 a side Sessions', 'vauxhall-7-a-side-sessions', '1569868055.jpg', '<p>Enjoy 7&nbsp;a side football&nbsp;at our Vauxhall Rhino Turf location in the south east of London - venue&nbsp;complete with changing facilities. Mix up your sunday afternoons and&nbsp;attending our hour long session of relaxed&nbsp;recreational football -&nbsp; all genders and abilities welcome.</p>\r\n\r\n<p>Whether it&#39;s to make new friends in the area or get some excercise in - join us every sunday afternoon fro 1-2pm.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img src=\"https://www.hireapitch.com/images/tube.png\" />&nbsp;Vauxhall Tube (10 mins walk)</p>\r\n\r\n<p><img src=\"https://www.hireapitch.com/images/tube.png\" />&nbsp;Lambeth North (10 mins walk)</p>\r\n\r\n<p><img src=\"https://www.hireapitch.com/images/tube.png\" />&nbsp;Waterloo (17 mins walk)</p>\r\n\r\n<p><img src=\"https://www.hireapitch.com/images/tube.png\" />&nbsp;Westminster (23 mins walk)</p>\r\n\r\n<p>&nbsp;</p>', '2019-10-06', '2019-12-29', '1:00 PM', '2:00 PM', 34, 12, NULL, 1, 'a:1:{i:0;s:1:\"7\";}', '7', 1, 1, '6', 'GBP', 1, NULL, NULL, 1, 1, '2019-09-30 20:47:02', '2019-09-30 19:47:03'),
(47, 4, 'White City Basketball Session - Women\'s', 'white-city-basketball-session-womens', '1569868657.jpg', '<p>Are you a basketball lover but can&#39;t find a casual playing enviroment? Then join us on Saturday&#39;s every week for recreational basketball at Burlington Danes Academy! Located in London&#39;s vibrant White City this indoor facility is compelete with a number of faccilities including shoers, changing and toilets.</p>\r\n\r\n<p>Balls and bibs provided.&nbsp;</p>\r\n\r\n<p><img src=\"https://www.hireapitch.com/images/tube.png\" />&nbsp;White City underground- 7 minutes walk<br />\r\n<img src=\"https://www.hireapitch.com/images/tube.png\" />&nbsp;East Acton underground- 15 minutes walk<br />\r\n<img src=\"https://www.hireapitch.com/images/tube.png\" />&nbsp;Wood Lane- underground- 10 minutes walk</p>', '2019-10-05', '2019-12-21', '1:00 PM', '2:30 PM', 40, 15, NULL, 1, 'a:1:{i:0;s:1:\"6\";}', '5', 3, 1, '6', 'GBP', 1, NULL, NULL, 1, 1, '2019-09-30 20:07:02', '2019-09-30 19:07:02'),
(48, 4, 'White City Basketball Session - Men\'s', 'white-city-basketball-session-mens', '1569868661.jpg', '<p>Are you a basketball lover but can&#39;t find a casual playing enviroment? Then join us on Saturday&#39;s every week for recreational basketball at Burlington Danes Academy! Located in London&#39;s vibrant White City this indoor facility is compelete with a number of faccilities including shoers, changing and toilets.</p>\r\n\r\n<p>Balls and bibs provided.&nbsp;</p>\r\n\r\n<p><img src=\"https://www.hireapitch.com/images/tube.png\" />&nbsp;White City underground- 7 minutes walk<br />\r\n<img src=\"https://www.hireapitch.com/images/tube.png\" />&nbsp;East Acton underground- 15 minutes walk<br />\r\n<img src=\"https://www.hireapitch.com/images/tube.png\" />&nbsp;Wood Lane- underground- 10 minutes walk</p>', '2019-10-05', '2019-12-21', '2:30 PM', '4:00 PM', 40, 15, NULL, 1, 'a:1:{i:0;s:1:\"6\";}', '5', 2, 1, '6', 'GBP', 1, NULL, NULL, 1, 1, '2019-09-30 20:49:43', '2019-09-30 19:49:44'),
(49, 4, 'White City 7 a side football - Women\'s', 'white-city-7-a-side-football-womens', '1569869481.jpg', '<p>Need to get your hours fitness in? Looking to Join up with other like-minded individuls? Or Simply looking for something to do on the weekends?</p>\r\n\r\n<p>Join us for some energetic 7 a side Women&#39;s football at Burlington Danes Academy every Saturday!</p>\r\n\r\n<p>Located in London&#39;s popular western area of White City, known for it&#39;s outdoor sports faciities, this&nbsp;venue offers changing, showers and toilets.&nbsp;</p>\r\n\r\n<p>Balls and bibs provided.&nbsp;</p>\r\n\r\n<p><img src=\"https://www.hireapitch.com/images/tube.png\" />&nbsp;&nbsp;White City underground- (7 minutes walk)<br />\r\n<img src=\"https://www.hireapitch.com/images/tube.png\" />&nbsp;East Acton underground- (15 minutes walk)<br />\r\n<img src=\"https://www.hireapitch.com/images/tube.png\" />&nbsp;Wood Lane- underground- (10 minutes walk)</p>', '2019-10-05', '2019-09-30', '3:00 PM', '4:00 PM', 40, 12, NULL, 1, 'a:1:{i:0;s:1:\"6\";}', '7', 3, 1, '6', 'GBP', 1, NULL, NULL, 1, 1, '2019-09-30 20:48:04', '2019-09-30 19:48:04'),
(50, 4, 'White City 7 a side football  - Men\'s', 'white-city-7-a-side-football-mens', '1569869737.jpg', '<p>Need to get your hours fitness in? Looking to Join up with other like-minded individuls? Or Simply looking for something to do on the weekends?</p>\r\n\r\n<p>Join us for some energetic 7 a side Men&#39;s football at Burlington Danes Academy every Saturday!</p>\r\n\r\n<p>Located in London&#39;s popular western area of White City, known for it&#39;s outdoor sports faciities, this&nbsp;venue offers changing, showers and toilets.&nbsp;</p>\r\n\r\n<p>Balls and bibs provided.&nbsp;</p>\r\n\r\n<p><img src=\"https://www.hireapitch.com/images/tube.png\" />&nbsp;&nbsp;White City underground- (7 minutes walk)<br />\r\n<img src=\"https://www.hireapitch.com/images/tube.png\" />&nbsp;East Acton underground- (15 minutes walk)<br />\r\n<img src=\"https://www.hireapitch.com/images/tube.png\" />&nbsp;Wood Lane- underground- (10 minutes walk)</p>', '2019-10-05', '2019-12-21', '3:00 PM', '4:00 PM', 40, 12, NULL, 1, 'a:1:{i:0;s:1:\"6\";}', '7', 2, 1, '6', 'GBP', 1, NULL, NULL, 1, 1, '2019-09-30 20:49:31', '2019-09-30 19:49:32'),
(51, 4, 'Canning Town 9 a side Football', 'canning-town-9-a-side-football', '1569876856.jpg', '<p><strong>Match length</strong>: 1 hour</p>\r\n\r\n<p><strong>Meet time</strong>: 10 minutes before kick off</p>\r\n\r\n<p><strong>Surface:</strong>&nbsp;New 3G, moulds and blades are fine.</p>\r\n\r\n<p><strong>Equipment</strong>: Balls and bibs will be provided and a site manager will be available with any questions.</p>\r\n\r\n<p><strong>Facilities:</strong>&nbsp;Toilets and a changing block with showers.</p>\r\n\r\n<p><img src=\"https://www.hireapitch.com/images/tube.png\" />&nbsp;Canning town (8-10&nbsp;mins walk)</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>General play.</strong></p>\r\n\r\n<p>All our games are played in a fun and friendly atmosphere with payments made prior to the game, this ensures that people actually show up and play. All no shows will be marked and should you consistently no show we can no longer accept you in our session.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Match Rules</strong><br />\r\n- You are allowed in the Area.&nbsp;<br />\r\n- Back pass to keeper allowed<br />\r\n- Every player gets to be Goalie after every 5-7&nbsp;minutes Goalie Rotation<br />\r\n- No aggression or slide tackling</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Directions</strong></p>\r\n\r\n<p>The pitch is located via an entrance on &quot;barking road&quot; the pitch cannot be missed as its a fantastically big pitch. If you struggle to find it then Buses 5,115,276, 300, 330&nbsp;will put you at the venue.</p>', '2019-10-01', '2019-12-17', '8:00 PM', '9:00 PM', 44, 10, NULL, 1, 'a:1:{i:0;s:1:\"2\";}', '9', 1, 1, '6', 'GBP', 1, NULL, NULL, 1, 1, '2019-10-01 13:34:21', '2019-10-01 12:34:21');

-- --------------------------------------------------------

--
-- Table structure for table `mst_nationality`
--

CREATE TABLE `mst_nationality` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_nationality`
--

INSERT INTO `mst_nationality` (`id`, `name`, `status`) VALUES
(1, 'Indian', 1),
(2, 'England', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mst_notification`
--

CREATE TABLE `mst_notification` (
  `id` bigint(127) NOT NULL,
  `user_to_notify` bigint(127) DEFAULT NULL,
  `user_who_fired_event` bigint(127) DEFAULT NULL,
  `notification_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '1 added game following venue, 2 comment added game, 3 comment my game, ',
  `notification_id` bigint(127) DEFAULT NULL,
  `notification_date` timestamp NULL DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mst_sports`
--

CREATE TABLE `mst_sports` (
  `id` mediumint(5) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sport_order` int(11) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_sports`
--

INSERT INTO `mst_sports` (`id`, `name`, `description`, `image`, `sport_order`, `status`) VALUES
(2, 'Netball', NULL, '1567664599.jpg', 6, 1),
(3, 'American Football', NULL, '1567664830.jpg', 5, 1),
(4, 'Tennis', NULL, '1567664854.jpg', 12, 1),
(5, 'Rugby', NULL, '1567664872.jpg', 10, 1),
(6, 'Hockey', NULL, '1567664884.jpg', 9, 1),
(7, 'Tag Rugby', NULL, '1567664898.jpg', 11, 1),
(8, 'Badminton', NULL, '1567664912.jpg', 13, 1),
(9, 'Ultimate Frisbee', NULL, '1567664939.jpg', 14, 1),
(10, 'Football', NULL, '1567677368.jpg', 1, 1),
(11, '5 a side football', NULL, '1567677422.jpg', 2, 1),
(12, '7 a side football', NULL, '1567677469.jpg', 3, 1),
(13, '11 a side football', NULL, '1567677490.jpg', 4, 1),
(14, 'Cricket', NULL, '1568196951.jpg', 8, 1),
(15, 'Basketball', '5 per side', '1569774787.jpg', 15, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mst_venue`
--

CREATE TABLE `mst_venue` (
  `id` int(11) NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postcode` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_created_by_user` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_venue`
--

INSERT INTO `mst_venue` (`id`, `location`, `image`, `name`, `slug`, `postcode`, `address`, `description`, `latitude`, `longitude`, `is_created_by_user`, `status`, `created_at`, `updated_at`) VALUES
(11, 'Alma road, London Borough of Wandsworth, London, UK SW18 1AE', '', 'St Faiths 4G Pitch Wandsworth', 'st-faiths-4g-pitch-wandsworth-11', 'SW18 1AE', 'St Faiths 4g pitch Wandsworth TOWN', 'Located in the heart of wandsworth town this fantastic 4g pitch is 30 seconds away from wandsworth town station and also accessible from clapham junction. Parking is provided and toilet facilities are on site. The pitch is european 4G with the taller 3m * 2m futsal goals and run offs allowing for some break in play but also for a more technical game.', '51.4570716', '-0.1817823999999746', NULL, 1, '2019-10-01 11:42:38', '2019-10-01 10:42:38'),
(13, 'Tyers Gate', '', 'Snowsfields School a', 'snowsfields-school-a-13', 'SE1 3TD', 'Snodland, UK', 'A beautiful new 4g pitch in London bridge perfect for 5 a side football.  Start time is 8pm and the facility is completely private but there are ample benches and park space locally if you wish to warm up.  Balls and bibs are provided so all you need to do is turn up.', '51.3313', '0.445067999999992', NULL, 1, '2019-10-01 12:34:46', '2019-10-01 11:34:47'),
(15, 'Southwark, UK', '', 'Southwark Park Road', 'southwark-park-road-15', 'SE16 5AZ', 'Southwark, London SE1 3RA, UK', 'Played in the heart of Southwark accessible from London bridge, Borough and Bermondsey stations this fantastic 4G pitch hosts our new Friday sessions. Turn up and play with showers and changing facilities all provided. Parking is free after 5pm and we normally play between 5 - 7 a side depending on numbers. Balls and bibs are provided so all you need to do is turn up.', '51.502781', '-0.0877379999999448', NULL, 1, '2019-09-11 16:13:59', '2019-09-11 15:13:59'),
(16, 'City of London, Anglia, UK', '', 'City of London Academy', 'city-of-london-academy-16', 'SE1 5LA', 'City of London, UK', NULL, '51.5123443', '-0.09098519999997734', NULL, 1, '2019-09-09 17:36:34', '2019-09-09 17:36:34'),
(17, 'London Borough of Wandsworth, London, UK', '', 'London Borough of Wandsworth', 'london-borough-of-wandsworth-17', 'SW11 1HS', 'London Borough of Wandsworth, London, UK', NULL, '51.4570716', '-0.1817823999999746', NULL, 1, '2019-09-11 14:39:21', '2019-09-11 13:39:21'),
(18, 'London Borough of Lambeth, London, UK', '', 'London Borough of Lambeth', 'london-borough-of-lambeth-18', 'SW4 9DE', 'London Borough of Lambeth, London, UK', NULL, '51.4571477', '-0.12306809999995494', NULL, 1, '2019-09-12 07:26:34', '2019-09-12 07:26:34'),
(32, '134 Chalton Street, Euston, London, UK, NW1 1RX', '1569576506.jpg', 'Somerstown Sports Centre Euston', 'somerstown-sports-centre-euston-32', 'NW1 1RX', 'London, UK', 'Fantastic court for netball and football with full changing and shower facilities. Indoor facilities are also available on request.', '51.5073509', '-0.12775829999998223', NULL, 1, '2019-10-01 11:54:37', '2019-10-01 10:54:37'),
(34, 'Lollard Street, Vauxhall, London Borough of Lambeth, London, UK SE11 6PX', '1569584379.JPG', 'Vauxhall Rhino Turf', 'vauxhall-rhino-turf-34', 'SE11 6PX', 'London Borough of Lambeth, London, UK', 'Outdoor sports pitches suitable for football and netball. Showers and changing available,', '51.4571477', '-0.12306809999995494', NULL, 1, '2019-10-01 11:25:36', '2019-10-01 10:25:37'),
(35, 'Max Roach Playground, Enter Via Villa Rd, Brixton, London Borough of Lambeth, London, UK SW9 7YA', '1569769668.jpg', 'Brixton', 'brixton-35', 'SW9 7YA', 'London Borough of Lambeth, London, UK', '4G Pitch in the Lambeth / Southwark Area\r\n\r\nVenue complete with ample changing rooms and European Goals', '51.4571477', '-0.12306809999995494', NULL, 1, '2019-10-01 10:40:29', '2019-10-01 09:40:29'),
(36, '50 John Harrison Way,Royal Borough of Greenwich, London, UK SE10 0BG', '1569770821.jpg', 'North Greenwich Millennium Academy', 'north-greenwich-millennium-academy-36', 'SE10 0BG', 'Royal Borough of Greenwich, UK', 'Spacious, caged pitch located in the Royal Borough of Greenwich offering both 5 a side and 7 a side facilities. \r\n\r\n10 minutes walk from North Greenwich Underground Station.\r\n\r\nOff street parking, Toilets and changing available.', '51.4834627', '0.05862019999995027', NULL, 1, '2019-10-01 10:36:01', '2019-10-01 09:36:02'),
(37, '21 Castlehaven Road, London Borough of Camden, London, UK NW1 8RU', '1569771960.jpg', 'Camden Rhino Turf', 'camden-rhino-turf-37', 'NW1 8RU', 'London Borough of Camden, London, UK', 'Brilliant 5/9 a side pitch in the heart of London and Camden Town, the AstroTurf pitch easily accessible from a number of underground stations. \r\n\r\nCamden Rd (5 mins walk)\r\nKentish Town (10 mins walk)\r\nCamden Town (10 mins walk)\r\n\r\nChanging, Showers and toilets available.\r\nPay and Display on-street parking available', '51.55170589999999', '-0.15882550000003448', NULL, 1, '2019-10-01 13:30:56', '2019-10-01 12:30:56'),
(38, 'Emslie Hornimans Pleasance, Southern Row, Westbourne Park, Royal Borough of Kensington and Chelsea, London, UK W10 5BD', '1569772313.png', 'Westbourne Park AstroTurf', 'westbourne-park-astroturf-38', 'W10 5BD', 'Royal Borough of Kensington and Chelsea, London, UK', 'Extremely spacious Astro Turf Pitch in the Royal Borough of Kensington and Chelsea, London, UK - located in North Kensington.\r\n\r\nFacility includes shower, changing area and Parking.  \r\n\r\n\r\nWestbourne Park Underground (10 mins walk)', '51.4990805', '-0.19382529999995768', NULL, 1, '2019-10-01 11:46:58', '2019-10-01 10:46:59'),
(39, 'Enter via Coopers Close, Bethnal Green, London Borough of Tower Hamlets, London, UK E1 4AX', '1569773037.jpg', 'Whitechapel John Scurr', 'whitechapel-john-scurr-39', 'E1 4AX', 'London Borough of Tower Hamlets, UK', 'Cosy pitch in London\'s Vibrant east end. Boasts a gated 5 a side with a 4G surface located between Whitechapel and Bethnal Green  within John Scurr Primary School.\r\n\r\nAccessible from both Stepney Green and Bethnal Green underground stations\r\nFacilities:  On - Street Parking', '51.52026069999999', '-0.0293395999999575', NULL, 1, '2019-10-01 10:52:35', '2019-10-01 09:52:35'),
(40, 'White City, London Borough of Hammersmith and Fulham, London, UK, W12 0EA', '1569773440.jpg', 'Burlington Danes Academy', 'burlington-danes-academy-40', 'W12 0EA', 'London Borough of Hammersmith and Fulham, London, UK', 'Extremely popular location in the Shepherds bush area. \r\n\r\nOffers both indoor and outdoor facilities:\r\n\r\nOutdoor facilities 7/11 a side pitch with a well-maintained 3G surface\r\n\r\nIndoor facilities offer an extensive amount of facilities for a variation of sporting activities including netball, basketball, 7/11 aside football, volleyball and much more. \r\n\r\nFacilities include:  Showers, Changing Rooms And Parking \r\n\r\nMutiple buses available form Shepherds Bush, Hammersmith, Paddington and other zone 1\r\nWhite City underground- 7 minutes walk\r\nEast Acton underground- 15 minutes walk\r\nWood Lane- underground- 10 minutes walk\r\nWestfield shopping centre 15 minutes away\r\nParking free and available on site', '51.4990156', '-0.22915000000000418', NULL, 1, '2019-10-01 12:32:53', '2019-10-01 11:32:54'),
(41, '1 Coral Street, Waterloo, London Borough of Lambeth, London, UK SE1 7BE', '1569773684.jpg', 'Waterloo Wolf Turf', 'waterloo-wolf-turf-41', 'SE1 7BE', 'London Borough of Lambeth, London, UK', 'An urban 5 a side facility located between Waterloo and Newington. Pitch offers Well Kept facilities perfect for both casual and competitive games.  \r\n \r\nWaterloo (2 mins walk)\r\nLambeth north Underground  (5 mins walk)\r\n Southwark Underground ( 7 mins walk )', '51.4571477', '-0.12306809999995494', NULL, 1, '2019-10-01 11:31:57', '2019-10-01 10:31:58'),
(42, 'London Borough of Lambeth, London, UK', '1569774083.jpg', 'Vauxhall Rhino Turf', 'vauxhall-rhino-turf-42', 'SE11 6PX', 'Lollard Street, Vauxhall, London', 'Beautiful and vibrant 5/7 a side facility located in the Vauxhall area between Lambeth and Pimlico - located within the Lollard Street Adventure Playground. Beautifully maintained astro pitch surface.\r\n\r\n\r\nFacilities will soon offer changing and showers alongside toilets. \r\n\r\n\r\nVauxhall Tube (10 mins walk)\r\n Lambeth North (10 mins walk)\r\n Waterloo (17 mins walk)\r\n Westminster (23 mins walk)', '51.4571477', '-0.12306809999995494', NULL, 1, '2019-09-29 15:21:23', '2019-09-29 15:21:23'),
(43, 'Perivale, London, UK', NULL, 'london park', 'london-park', '12312', 'Perivale, London, UK', NULL, '51.532222', '-0.3244770000000017', NULL, 1, '2019-09-30 10:19:34', '2019-09-30 10:19:34'),
(44, 'Rokeby School, Barking Road, Canning Town, London Borough of Newham, UK E16 4DD', '1569876263.jpg', 'Rokeby School Football Pitch', 'rokeby-school-football-pitch-44', 'E16 4DD', 'London Borough of Newham, UK', 'Spacious 9 and 5 a side pitch in London\'s east district', '51.5255162', '0.035216300000001866', NULL, 1, '2019-10-01 10:37:49', '2019-10-01 09:37:50');

-- --------------------------------------------------------

--
-- Table structure for table `mst_venue_facility`
--

CREATE TABLE `mst_venue_facility` (
  `id` int(10) NOT NULL,
  `facility` varchar(255) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_venue_facility`
--

INSERT INTO `mst_venue_facility` (`id`, `facility`, `icon`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Private Event Space', '1567775832.png', 1, NULL, '2019-09-06 12:17:12'),
(2, 'Free Parking', '1567775841.png', 1, NULL, '2019-09-24 14:40:12'),
(3, 'Changing Room', '1567775851.png', 1, NULL, '2019-09-26 18:52:07'),
(4, '11 a side 3g pitch', '1567775861.png', 1, NULL, '2019-09-27 04:45:25'),
(5, '7-A-Side 3G Pitch', '1567775873.png', 1, NULL, '2019-09-06 12:17:53'),
(6, '4 x Futsal Pitches', '1567775883.png', 1, NULL, '2019-09-06 12:18:03'),
(7, '5 a side pitch', '1567775893.png', 1, NULL, '2019-09-27 04:45:45'),
(9, 'Outdoor Sports Pitch', '1569339083.png', 1, '2019-09-24 14:28:03', '2019-09-24 14:31:23'),
(10, 'Indoor Areas For Food And Drink', '1569339515.png', 1, '2019-09-24 14:38:35', '2019-09-24 14:38:35'),
(11, 'Grass Area', '1569563193.png', 1, '2019-09-27 04:46:33', '2019-09-27 04:46:33');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('1ea7fe7c-b804-45b5-be02-f8ede7229f2e', 'App\\Notifications\\GameNotify', 'App\\User', 1, '{\"game_id\":3,\"user_id\":1,\"message\":\" has left your game\"}', '2019-09-03 07:11:27', '2019-09-03 06:23:57', '2019-09-03 07:11:27'),
('38b87f2e-ba79-4c54-abcc-b88364960755', 'App\\Notifications\\GameNotify', 'App\\User', 2, '{\"game_id\":1,\"user_id\":1,\"message\":\"created a new game on a venue you are following\"}', '2019-08-30 01:59:40', '2019-08-30 01:02:31', '2019-08-30 01:59:40'),
('49fd0945-9402-4c2c-9ea7-a3ca9f40abbe', 'App\\Notifications\\GameNotify', 'App\\User', 2, '{\"game_id\":6,\"user_id\":1,\"message\":\"created a new game on a venue you are following\"}', '2019-08-30 04:58:32', '2019-08-30 04:51:52', '2019-08-30 04:58:32'),
('616d8966-e9d2-4a94-89ed-8ec7e689cf96', 'App\\Notifications\\GameNotify', 'App\\User', 1, '{\"game_id\":8,\"user_id\":2,\"message\":\" has joined your created game\"}', '2019-09-05 12:25:48', '2019-09-03 07:37:52', '2019-09-05 12:25:48'),
('8319323a-d8c4-42d3-ab27-09955468f496', 'App\\Notifications\\GameNotify', 'App\\User', 2, '{\"game_id\":1,\"user_id\":1,\"message\":\"created a new game on a venue you are following\"}', '2019-08-30 02:24:52', '2019-08-29 02:24:20', '2019-08-30 02:24:52'),
('b4187bc0-7b89-4a74-96f7-f8a23357d79f', 'App\\Notifications\\GameNotify', 'App\\User', 17, '{\"game_id\":20,\"user_id\":4,\"message\":\"created a new game on a venue you are following\"}', NULL, '2019-09-20 08:01:48', '2019-09-20 08:01:48'),
('c83d6db5-ce37-4d44-948f-44d7ca17e5bd', 'App\\Notifications\\GameNotify', 'App\\User', 11, '{\"game_id\":10,\"user_id\":8,\"message\":\" has joined your created game\"}', '2019-09-05 10:00:35', '2019-09-05 09:59:19', '2019-09-05 10:00:35'),
('cb0a1f0b-387e-4203-8f5b-2b4efb8281ad', 'App\\Notifications\\GameNotify', 'App\\User', 3, '{\"game_id\":6,\"user_id\":1,\"message\":\"created a new game on a venue you are following\"}', '2019-08-30 04:58:21', '2019-08-30 04:51:58', '2019-08-30 04:58:21'),
('ce17fb5d-d262-4777-8621-40cc3a0bdbd8', 'App\\Notifications\\GameNotify', 'App\\User', 8, '{\"game_id\":10,\"user_id\":8,\"message\":\" You have joined a game \"}', NULL, '2019-09-05 09:59:29', '2019-09-05 09:59:29'),
('ed09d639-3385-4093-bf86-dee1f0bf6fc8', 'App\\Notifications\\GameNotify', 'App\\User', 2, '{\"game_id\":8,\"user_id\":2,\"message\":\" You have joined a game \"}', '2019-09-03 07:40:05', '2019-09-03 07:38:02', '2019-09-03 07:40:05');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('09abb17b55bc5dd4539219438ae011b48b1dc22d11c53c8561febeefd2fb93f13261737e3005154d', 6, 3, 'letesplayapp', '[]', 1, '2019-09-19 14:22:53', '2019-09-19 14:22:53', '2020-09-19 15:22:53'),
('109ef064d0887a0db8e83cbbdb6fb9ac2e7d5ab82f98c80f29ca76ee52ebeb0ba89c3601911dfdcb', 6, 3, 'letesplayapp', '[]', 0, '2019-09-17 11:17:38', '2019-09-17 11:17:38', '2020-09-17 12:17:38'),
('166a5d455d29cad73abec026b5404772a19502dfcd6ca45806a1f88cbd99275c9635618af21f4cf0', 6, 3, 'letesplayapp', '[]', 0, '2019-09-09 06:09:08', '2019-09-09 06:09:08', '2020-09-09 07:09:08'),
('1c8889b1e91fd1fe332bfd612291587d9212b6fa521a48a50b31b1f417ea0ee3e157eb31d92aa0ac', 6, 3, 'letesplayapp', '[]', 0, '2019-09-20 08:23:19', '2019-09-20 08:23:19', '2020-09-20 09:23:19'),
('1caf07c2ef675826701c9861379bb0a47824aa652564abcab0b7b8105025a963fce1b9cecaaab24e', 4, 3, 'letesplayapp', '[]', 0, '2019-09-06 04:48:18', '2019-09-06 04:48:18', '2020-09-06 05:48:18'),
('1f76949dfbc424f72a80f3890fd9b81a79adaf7ea6563e8189828dcfa3b4bc4347ec022b0cdd2967', 6, 3, 'letesplayapp', '[]', 1, '2019-09-19 14:54:18', '2019-09-19 14:54:18', '2020-09-19 15:54:18'),
('27efc13af2424cc3c7eff246824115a2c09c3f43d5dcb10715ba66a9434c574501a2adeb79b3868c', 4, 3, 'letesplayapp', '[]', 1, '2019-09-19 11:06:17', '2019-09-19 11:06:17', '2020-09-19 12:06:17'),
('2db7da23ee379d32512bc5a516d4b8a0af47a81aafa9db79646edf9a9289e0c651a33a158e0718df', 4, 3, 'letesplayapp', '[]', 1, '2019-09-18 12:57:00', '2019-09-18 12:57:00', '2020-09-18 13:57:00'),
('57de2f5e350f50e492e4153994e610ccc8e6d3023de91819f132e4ad287b6e0a1332a1f5d83f4c3e', 6, 3, 'letesplayapp', '[]', 0, '2019-09-28 16:51:02', '2019-09-28 16:51:02', '2020-09-28 17:51:02'),
('64a71f4c6893f9627f59e8c74ba1adf2c7e02af5143ee9de834720bba174b2a46b3ef633eaa671af', 6, 3, 'letesplayapp', '[]', 1, '2019-09-20 08:21:49', '2019-09-20 08:21:49', '2020-09-20 09:21:49'),
('69a4446e3c6574f58aaf9226892b5db01b7f2b4f97bb3592f83c405507abf04af0e5bf8f34b6af59', 6, 3, 'letesplayapp', '[]', 1, '2019-09-17 11:24:22', '2019-09-17 11:24:22', '2020-09-17 12:24:22'),
('749275a8c1838534e2b0119f99e518e3dc0480cc25e6d6c14202cbc8954329ce254babc6ff122e5f', 6, 3, 'letesplayapp', '[]', 1, '2019-09-07 14:29:25', '2019-09-07 14:29:25', '2020-09-07 15:29:25'),
('81f7b691faafaf8f20a7777e05ceb8473b2dd1a5391e24d58d6269777ed2bfde8f78681b6641be4d', 6, 3, 'letesplayapp', '[]', 0, '2019-09-18 10:05:54', '2019-09-18 10:05:54', '2020-09-18 11:05:54'),
('9a4c92681cd56c0dc1f2b2cd17cc9ac3339ca1c90e74309202fa2c443bfec8cedd70aebe1e87ecdd', 6, 3, 'letesplayapp', '[]', 0, '2019-09-29 14:01:34', '2019-09-29 14:01:34', '2020-09-29 15:01:34'),
('ac712a4569904b753e8eb7c5ed237d88081803d9f175298279452103996c1c3b7d5d11e46f196c38', 6, 3, 'letesplayapp', '[]', 1, '2019-09-28 16:06:48', '2019-09-28 16:06:48', '2020-09-28 17:06:48'),
('b07e1bbb2c90a08f4cd652867ffb23ac71011c05cc481feac0343bb521df8d1b1fecac5e0cf33b1a', 6, 3, 'letesplayapp', '[]', 0, '2019-09-30 11:15:08', '2019-09-30 11:15:08', '2020-09-30 12:15:08'),
('b0d01e3599491b43629c08e13838bf07d4072de38963a7e6e3ae543dd9ac5f57078199b62905bfa8', 6, 3, 'letesplayapp', '[]', 0, '2019-09-18 08:35:37', '2019-09-18 08:35:37', '2020-09-18 09:35:37'),
('c2461d1c0ee41a2a023eebe317fb146a5cbf64bec8640cd95228f44c18df440a2b86de206e8b9740', 6, 3, 'letesplayapp', '[]', 0, '2019-09-19 15:02:27', '2019-09-19 15:02:27', '2020-09-19 16:02:27'),
('c90bf3f5bfa89f3530a41df2f65911bf1934f4066f71e5623164461251f3d7a4739369a7f3b4d453', 6, 3, 'letesplayapp', '[]', 1, '2019-09-18 02:28:56', '2019-09-18 02:28:56', '2020-09-18 03:28:56'),
('d3ac6ce3c4af1498a42661a693eb9e9fd9686b7a675d497092f38871d917e05d30a32b0b32685bd8', 6, 3, 'letesplayapp', '[]', 1, '2019-09-19 14:48:44', '2019-09-19 14:48:44', '2020-09-19 15:48:44'),
('dd49a37839fc8800052ba9ba55113afe99f7b3521fd1b2270493930200bb8925b83c79dc8766d68b', 6, 3, 'letesplayapp', '[]', 0, '2019-09-30 11:45:01', '2019-09-30 11:45:01', '2020-09-30 12:45:01'),
('f65aa4b823d0ae16ef94c5f122fcbc173bda38aff9b8e7d5ca57b0b0fe125baf7f74006970cdba92', 6, 3, 'letesplayapp', '[]', 0, '2019-09-17 13:13:56', '2019-09-17 13:13:56', '2020-09-17 14:13:56'),
('fb6d4fb23e6916bdb233926ec1498f79cedf780105ed5203c50b8c09279a14eae43d74b61af7d855', 6, 3, 'letesplayapp', '[]', 0, '2019-09-20 08:23:26', '2019-09-20 08:23:26', '2020-09-20 09:23:26');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', '8ogU6HNUN4FdPWZKrceeuWjq46zbE5AqhmkF2VbW', 'http://localhost', 1, 0, 0, '2019-08-30 12:17:37', '2019-08-30 12:17:37'),
(2, NULL, 'Laravel Password Grant Client', '0GRb63KbCbpTiYApWq70gyDtoKXcsoqeNbtmVTzl', 'http://localhost', 0, 1, 0, '2019-08-30 12:17:37', '2019-08-30 12:17:37'),
(3, NULL, 'Laravel Personal Access Client', 'qV3AuWpBxJhZkX7daXuuAXaM1Cvsic82GzA5dkmp', 'http://localhost', 1, 0, 0, '2019-08-31 11:23:55', '2019-08-31 11:23:55'),
(4, NULL, 'Laravel Password Grant Client', 'UOtrTqr4dxcZ3xRcDcF24gQXS1z9MXdQKmFKDv98', 'http://localhost', 0, 1, 0, '2019-08-31 11:23:55', '2019-08-31 11:23:55');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-08-30 12:17:37', '2019-08-30 12:17:37'),
(2, 3, '2019-08-31 11:23:55', '2019-08-31 11:23:55');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `name`, `description`, `menu_id`) VALUES
(1, 'PRIVACY POLICY', '<h2><strong>Privacy Policy</strong></h2>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>This Policy applies as between you, the User of this Website and Footy Addicts the owner and provider of this Website. This Policy applies to our use of any and all Data collected by us in relation to your use of the Website.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>\r\n	<p>Definitions and Interpretation</p>\r\n	In this Policy the following terms shall have the following meanings:\r\n\r\n	<p>&nbsp;</p>\r\n\r\n	<p><strong>&ldquo;Data&rdquo;</strong></p>\r\n\r\n	<p>means collectively all information that you submit to the Footy Addicts via the Website. This definition shall, where applicable, incorporate the definitions provided in the Data Protection Act 1998;</p>\r\n	&nbsp;\r\n\r\n	<p><strong>&ldquo;Cookie&rdquo;</strong></p>\r\n\r\n	<p>means a small text file placed on your computer by this Website when you visit certain parts of the Website and/or when you use certain features of the Website. Details of the cookies used by this Website are set out in Clause 12;</p>\r\n	&nbsp;\r\n\r\n	<p><strong>&ldquo;Footy Addicts&rdquo;</strong></p>\r\n\r\n	<p>means Footy Addicts of 120 Baker Street, 3rd Floor, London, United Kingdom, W1U 6TU</p>\r\n	&nbsp;\r\n\r\n	<p><strong>&ldquo;UK and EU Cookie Law&rdquo;</strong></p>\r\n\r\n	<p>means the Privacy and Electronic Communications (EC Directive) Regulations 2003 as amended by the Privacy and Electronic Communications (EC Directive) (Amendment) Regulations 2011;</p>\r\n	&nbsp;\r\n\r\n	<p><strong>&ldquo;User&rdquo;</strong></p>\r\n\r\n	<p>means any third party that accesses the Website and is not employed by Footy Addicts and acting in the course of their employment; and</p>\r\n	&nbsp;\r\n\r\n	<p><strong>&ldquo;Website&rdquo;</strong></p>\r\n\r\n	<p>means the website that you are currently using (www .footyaddicts.com) and any sub-domains of this site (e.g. subdomain www.footyaddicts.net &amp; www.footyaddicts.co.uk.) unless expressly excluded by their own terms and conditions.</p>\r\n	</li>\r\n	<li>\r\n	<p>Scope of this Policy</p>\r\n	This Policy applies only to the actions of Footy Addicts and Users with respect to this Website. It does not extend to any websites that can be accessed from this Website including, but not limited to, any links we may provide to social media websites.</li>\r\n	<li>\r\n	<p>Data Collected</p>\r\n	Without limitation, any of the following Data may be collected by this Website from time to time:\r\n\r\n	<ol>\r\n		<li>name;</li>\r\n		<li>date of birth;</li>\r\n		<li>gender;</li>\r\n		<li>job title;</li>\r\n		<li>profession;</li>\r\n		<li>contact information such as email addresses and telephone numbers;</li>\r\n		<li>demographic information such as post code, preferences and interests;</li>\r\n		<li>financial information such as credit / debit card numbers;</li>\r\n		<li>IP address (automatically collected);</li>\r\n		<li>web browser type and version (automatically collected);</li>\r\n		<li>operating system (automatically collected);</li>\r\n		<li>a list of URLs starting with a referring site, your activity on this Website, and the site you exit to (automatically collected); and</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Our Use of Data</p>\r\n\r\n	<ol>\r\n		<li>Any personal Data you submit will be retained by Footy Addicts for 6 months.</li>\r\n		<li>Unless we are obliged or permitted by law to do so, and subject to Clause 5, your Data will not be disclosed to third parties. This does not include our affiliates and / or other companies within our group.</li>\r\n		<li>All personal Data is stored securely in accordance with the principles of the Data Protection Act 1998. Fore more details on security see Clause 11 below.</li>\r\n		<li>Any or all of the above Data may be required by us from time to time in order to provide you with the best possible service and experience when using our Website. Specifically, Data may be used by us for the following reasons:\r\n		<ol>\r\n			<li>internal record keeping;</li>\r\n			<li>improvement of our products / services;</li>\r\n			<li>transmission by email of promotional materials that may be of interest to you;</li>\r\n			<li>contact for market research purposes which may be done using email, telephone, fax or mail. Such information may be used to customise or update the Website.</li>\r\n		</ol>\r\n		</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Third Party Websites and Services</p>\r\n\r\n	<ol>\r\n		<li>Footy Addicts may, from time to time, employ the services of other parties for dealing with matters that may include, but are not limited to, payment processing, delivery of purchased items, search engine facilities, advertising and marketing. The providers of such services do not have access to certain personal Data provided by Users of this Website.</li>\r\n		<li>Any Data used by such parties is used only to the extent required by them to perform the services that Footy Addicts requests. Any use for other purposes is strictly prohibited. Furthermore, any Data that is processed by third parties shall be processed within the terms of this Policy and in accordance with the Data Protection Act 1998.</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Links to Other Websites</p>\r\n	This Website may, from time to time, provide links to other websites. Footy Addicts has no control over such websites and is in no way responsible for the content thereof. This Policy does not extend to your use of such websites. Users are advised to read the privacy policy or statement of other websites prior to using them.</li>\r\n	<li>\r\n	<p>Changes of Business Ownership and Control</p>\r\n\r\n	<ol>\r\n		<li>Footy Addicts may, from time to time, expand or reduce our business and this may involve the sale and/or the transfer of control of all or part of Footy Addicts. Data provided by Users will, where it is relevant to any part of our business so transferred, be transferred along with that part and the new owner or newly controlling party will, under the terms of this Policy, be permitted to use the Data for the purposes for which it was originally supplied to us.</li>\r\n		<li>In the event that any Data submitted by Users is to be transferred in such a manner, you will not be contacted in advance and informed of the changes. When contacted you not be given the choice to have your Data deleted or withheld from the new owner or controller.</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Controlling Use of Your Data</p>\r\n\r\n	<ol>\r\n		<li>Wherever you are required to submit Data, you will be given options to restrict our use of that Data. This may include the following:\r\n		<ol>\r\n			<li>use of Data for direct marketing purposes; and</li>\r\n			<li>sharing Data with third parties.</li>\r\n		</ol>\r\n		</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Your Right to Withhold Information</p>\r\n\r\n	<ol>\r\n		<li>You may access certain areas of the Website without providing any Data at all. However, to use all features and functions available on the Website you may be required to submit certain Data.</li>\r\n		<li>You may restrict your internet browser&rsquo;s use of Cookies. For more information see Clause 12.</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Accessing your own Data</p>\r\n	You have the right to ask for a copy of any of your personal Data held by Footy Addicts (where such data is held) on payment of a small fee which will not exceed &pound;100.00 GBP.</li>\r\n	<li>\r\n	<p>Security</p>\r\n	Data security is of great importance to Footy Addicts and to protect your Data we have put in place suitable physical, electronic and managerial procedures to safeguard and secure Data collected via this Website.</li>\r\n	<li>\r\n	<p>Cookies</p>\r\n\r\n	<ol>\r\n		<li>This Website may place and access certain first party Cookies on your computer. First party cookies are those placed directly by Footy Addicts via this Website and are used only by Footy Addicts. Footy Addicts uses Cookies to improve your experience of using the Website and to improve our range of products and services. Footy Addicts has carefully chosen these Cookies and has taken steps to ensure that your privacy is protected and respected at all times.</li>\r\n		<li>By using this Website you may receive certain third party Cookies on your computer. Third party cookies are those placed by websites and/or parties other than Footy Addicts for the purposes of tracking the success of their application or customising the application for you. We cannot access these cookies, nor can the third parties access the data in our cookies. For example, when you share an article using a social media sharing button on our site, the social network on which you are sharing will record that you have done this.</li>\r\n		<li>All Cookies used by this Website are used in accordance with current UK and EU Cookie Law.</li>\r\n		<li>Before any Cookies are placed on your computer, subject to sub-Clause 12.5, you will be presented with either a pop-up or a message bar requesting your consent to set those Cookies. By giving your consent to the placing of Cookies you are enabling Footy Addicts to provide the best possible experience and service to you. You may, if you wish, deny consent to the placing of Cookies; however certain features of the Website may not function fully or as intended. You will be given the opportunity to allow only first party Cookies and block third party Cookies.</li>\r\n		<li>Certain features of the Website depend upon Cookies to function. UK and EU Cookie Law deems these Cookies to be &ldquo;strictly necessary&rdquo;. These cookies enable services you have specifically asked for. No consent is required for the use of these cookies. For example, when you subscribe to one of our products or register as a user, we use session cookies to signal that you are logged in so that you are able to use community features of the Website, and persistent cookies to provide automatic login. (need to check this one with my developers) Your consent will not be sought to place these Cookies. You may still block these cookies by changing your internet browser&rsquo;s settings as detailed below.</li>\r\n		<li>You can choose to enable or disable Cookies in your internet browser. Most internet browsers also enable you to choose whether you wish to disable all cookies or only third party cookies. By default, most internet browsers accept Cookies but this can be changed. For further details, please consult the help menu in your internet browser.</li>\r\n		<li>You can choose to delete Cookies at any time however you may lose any information that enables you to access the Website more quickly and efficiently including, but not limited to, personalisation settings.</li>\r\n		<li>It is recommended that you ensure that your internet browser is up-to-date and that you consult the help and guidance provided by the developer of your internet browser if you are unsure about adjusting your privacy settings.</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Changes to this Policy</p>\r\n	Footy Addicts reserves the right to change this Policy as we may deem necessary from time to time or as may be required by law. Any changes will be immediately posted on the Website and you are deemed to have accepted the terms of the Policy on your first use of the Website following the alterations.</li>\r\n</ol>', 2),
(2, 'TERMS & CONDITIONS', '<h2><strong>Terms and Conditions</strong></h2>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>This agreement applies as between you, the User of this Website and Footy Addicts, the owner(s) of this Website. Your agreement to comply with and be bound by these Terms and Conditions is deemed to occur upon your first use of the Website. If you do not agree to be bound by these Terms and Conditions, you should stop using the Website immediately.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Footy Addicts is a social platform (the &ldquo;Platform&rdquo;) that helps football enthusiasts (&ldquo;Users&rdquo;) all around London find people to play with and organise football games.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Footy Addicts brings the football community together. Users can search for games by venue or date, interact with the rest of the group, give feedback &amp; share real time information.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>\r\n	<p>Definitions and Interpretation</p>\r\n	In this Agreement the following terms shall have the following meanings:\r\n\r\n	<p>&nbsp;</p>\r\n\r\n	<p><strong>&ldquo;Content&rdquo;</strong></p>\r\n\r\n	<p>means any text, graphics, images, audio, video, software, data compilations and any other form of information capable of being stored in a computer that appears on or forms part of this Website;</p>\r\n	&nbsp;\r\n\r\n	<p><strong>&ldquo;Footy Addicts&rdquo;</strong></p>\r\n\r\n	<p>Means Footy Addicts Limited trading as Footy Addicts 120 Baker Street, 3rd Floor, London, United Kingdom, W1U 6TU.</p>\r\n	&nbsp;\r\n\r\n	<p><strong>&ldquo;Service&rdquo;</strong></p>\r\n\r\n	<p>means collectively any online facilities, tools, services or information that Footy Addicts makes available through the Website either now or in the future;</p>\r\n	&nbsp;\r\n\r\n	<p><strong>&ldquo;System&rdquo;</strong></p>\r\n\r\n	<p>means any online communications infrastructure that Footy Addicts makes available through the Website either now or in the future. This includes, but is not limited to, web-based email, message boards, live chat facilities and email links;</p>\r\n	&nbsp;\r\n\r\n	<p><strong>&ldquo;User&rdquo; / &ldquo;Users&rdquo;</strong></p>\r\n\r\n	<p>means any third party that accesses the Website and is not employed by Footy Addicts and acting in the course of their employment;</p>\r\n	&nbsp;\r\n\r\n	<p><strong>&ldquo;Website&rdquo; /&ldquo;Platform&rdquo;</strong></p>\r\n\r\n	<p>means the website that you are currently using http://www.footyaddicts.com and any sub-domains of this site including www.footyaddicts.net and www.footyaddicts.co.uk and any other sub-domains unless expressly excluded by their own terms and conditions.</p>\r\n	</li>\r\n	<li>\r\n	<p>Membership</p>\r\n\r\n	<ol>\r\n		<li>By registering to use our Website, you represent and warrant that you are at least 16 years of age.</li>\r\n		<li>When you complete our registration process you will create a password that will enable you to access our Website.</li>\r\n		<li>You are solely responsible for maintaining the confidentiality of your password and agree not to share your password or let anyone else access your account. You will immediately notify Footy Addicts of any unauthorised use of your password or any other security breach relative to your account. You agree that Footy Addicts will not be liable for any loss or damage arising from your failure to comply with this section.</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Restrictions</p>\r\n\r\n	<ol>\r\n		<li>Persons under the age of 16 should not use this Website.</li>\r\n		<li>This site has certain areas that only members of Footy Addicts can access.&nbsp; If you are not a member or if your membership expires or otherwise ends you will not be able to access these areas.</li>\r\n		<li>Members should not allow their membership details to be passed to another person in order to allow them unauthorised access to members&#39; only areas or members&#39; only services available on this website.&nbsp; Members who fail to observe this requirement may place their membership at risk.</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Intellectual Property</p>\r\n\r\n	<ol>\r\n		<li>Subject to the exceptions in Clause 5 of these Terms and Conditions, all Content included on the Website, unless uploaded by Users, including, but not limited to, text, graphics, logos, icons, images, sound clips, video clips, data compilations, page layout, underlying code and software is the property of Footy Addicts, or our affiliates. By continuing to use the Website you acknowledge that such material is protected by applicable United Kingdom and International intellectual property and other laws.</li>\r\n		<li>Subject to Clause 5 of these Terms and Conditions you may print, reproduce, copy, distribute, store or in any other fashion re-use Content from the Website for personal purposes only unless otherwise indicated on the Website or unless given express written permission to do so by Footy Addicts. Personal use includes, but is not limited to, recreational use, social use, and use in education as a student or teacher. Specifically you agree that:\r\n		<ol>\r\n			<li>you will not use the Content of the Website for commercial purposes; and</li>\r\n			<li>you will not systematically copy Content from the Website with a view to creating or compiling any form of comprehensive collection, compilation, directory or database unless given express written permission to do so by Footy Addicts.</li>\r\n		</ol>\r\n		</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Third Party Intellectual Property</p>\r\n	Where expressly indicated, certain Content and the Intellectual Property Rights subsisting therein belongs to other parties. This Content, unless expressly stated to be so, is not covered by any permission granted by Clause 4 of these Terms and Conditions to use Content from the Website. Any such Content will be accompanied by a notice providing the contact details of the owner and any separate use policy that may be relevant.</li>\r\n	<li>\r\n	<p>Fair Use of Intellectual Property</p>\r\n	Material from the Website may be re-used without written permission where any of the exceptions detailed in Chapter III of the Copyright Designs and Patents Act 1988 apply.</li>\r\n	<li>\r\n	<p>Links to Other Websites</p>\r\n\r\n	<p>This Website may contain links to other sites. Unless expressly stated, these sites are not under the control of Footy Addicts or that of our affiliates. We assume no responsibility for the content of such websites and disclaim liability for any and all forms of loss or damage arising out of the use of them. The inclusion of a link to another site on this Website does not imply any endorsement of the sites themselves or of those in control of them.</p>\r\n\r\n	<p>As a member of Footy Addicts, you may link your account with other third party social web services including, without limitation to, Facebook and Twitter, which are subject to their own Terms and Conditions. If you decide to link your account with other third-party social web services, we may receive the personal information you have provided to those third party social web services (including, without limitation to, your name, profile picture, email address and all other information you have chosen to provide). You understand and agree that this personal information will be collected by Footy Addicts and will appear in your player profile.</p>\r\n	</li>\r\n	<li>\r\n	<p>Links to this Website</p>\r\n	Deep-linking to the Site for any purpose, (i.e. including a link to a Footy Addicts webpage other than the Footy Addicts home page) unless expressly authorised in writing by Footy Addicts or for the purpose of promoting your profile or a Group on Footy Addicts via a reputable social networking website (e.g. facebook, twitter, flickr etc) is prohibited. To find out more please contact us by email at&nbsp;info@footyaddicts.com</li>\r\n	<li>\r\n	<p>Use of Communications Facilities and Content Submission</p>\r\n\r\n	<ol>\r\n		<li>You are solely responsible for all information you post to the Website. You acknowledge that your public information may be accessible to the general public not only via the Website but also via third party websites related to Footy Addicts.</li>\r\n		<li>When using the Footy Addicts website and when submitting Content to the Website you should do so in accordance with the following rules:\r\n		<ol>\r\n			<li>you must not use obscene or vulgar language;</li>\r\n			<li>you must not submit Content that is unlawful or otherwise objectionable. This includes, but is not limited to, Content that is abusive, threatening, harassing, defamatory, ageist, sexist or racist;</li>\r\n			<li>you must not submit Content that is intended to promote or incite violence;</li>\r\n			<li>it is advised that posts on message boards, chat facilities or similar and communications with Footy Addicts are made using the English language as we may be unable to respond to enquiries submitted in any other languages;</li>\r\n			<li>content submissions are required to be made using the English language. Content in any other language may be removed at our sole discretion;</li>\r\n			<li>you must not post links to other websites containing any of the above types of Content;</li>\r\n			<li>the means by which you identify yourself must not violate these Terms and Conditions or any applicable laws;</li>\r\n			<li>you must not engage in any form of commercial advertising. This does not prohibit references to businesses for non-promotional purposes including references where advertising may be incidental;</li>\r\n			<li>you must not impersonate other people, particularly employees and representatives of Footy Addicts or our affiliates;</li>\r\n			<li>you must not submit material that may contain viruses or any other software or instructions that may damage or disrupt other software, computer hardware or communications networks; and</li>\r\n			<li>you must not use our System for unauthorised mass-communication such as &ldquo;spam&rdquo; or &ldquo;junk mail&rdquo;.</li>\r\n		</ol>\r\n		</li>\r\n		<li>You acknowledge that Footy Addicts reserves the right to monitor any and all communications made to us or using our System.</li>\r\n		<li>In order to use the Footy Addicts website and any other communication facility that may be added in the future or to submit Content, you are required to submit certain personal details. By continuing to use this Website you represent and warrant that:\r\n		<ol>\r\n			<li>any information you submit is accurate and truthful; and</li>\r\n			<li>you will keep this information accurate and up-to-date.</li>\r\n		</ol>\r\n		</li>\r\n		<li>By submitting Content you warrant and represent that you are the author of such Content or that you have acquired all of the appropriate rights and / or permissions to use the Content in this fashion. Further, you waive all moral rights in the Content to be named as its author and grant Footy Addicts a licence to modify the Content as necessary for its inclusion on the Website. Footy Addicts accepts no responsibility or liability for any infringement of third party rights by such Content.</li>\r\n		<li>Unless a User informs Footy Addicts otherwise, in advance of posting, in writing, and Footy Addicts agrees to any terms or restrictions, all Content submitted is for publication on the Website and other such uses as Footy Addicts may deem appropriate under a royalty-free, perpetual basis.</li>\r\n		<li>Footy Addicts will not be liable in any way or under any circumstances for any loss or damage that you may incur as a result of such Content, nor for any errors or omissions in the Content. Use of and reliance upon such Content is entirely at your own risk.</li>\r\n		<li>Content submitted by Users is not screened by Footy Addicts prior to appearing online. We retain the right to exercise our sole discretion to remove or relocate any Content as we deem appropriate without the consent of the author. We shall be under no obligation to exercise such discretion. If you wish to enquire as to the removal of Content, please submit your query to&nbsp;info@footyaddicts.com.&nbsp;This does not constitute an undertaking to explain our actions.</li>\r\n		<li>You acknowledge that Footy Addicts may retain copies of any and all communications made to us or using our System.</li>\r\n		<li>You acknowledge that any information you send to us through our System or post on the Footy Addicts website may be modified by us in any way and you hereby waive your moral right to be identified as the author of such information. Any restrictions you may wish to place upon our use of such information must be communicated to us in advance and we reserve the right to reject such terms and associated information.</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Termination and Suspension</p>\r\n	In the event that any of the provisions of sub-Clause 9.2, above, are not followed, Footy Addicts reserves the right to suspend or terminate your access to the Service. Any Users banned in this way must not attempt to use the Website under any other name or by using the access credentials of another User, with or without the permission of that User.</li>\r\n	<li>\r\n	<p>Privacy</p>\r\n\r\n	<ol>\r\n		<li>Use of the Website is also governed by our&nbsp;<a href=\"https://footyaddicts.com/pages/privacy\">Privacy Policy</a>&nbsp;which is incorporated into these terms and conditions by this reference. To view the Privacy Policy, please click on the link above.</li>\r\n		<li>The Website places the cookies onto your computer or device. These cookies are used for the purposes described herein. Full details of the cookies used by the Website and your legal rights with respect to them are included in our&nbsp;<a href=\"https://footyaddicts.com/pages/privacy\">Privacy Policy</a></li>\r\n		<li>By accepting these terms and conditions, you are giving consent to Footy Addicts to place cookies on your computer or device. Please read the information contained in the Privacy Policy prior to acceptance.</li>\r\n		<li>If you wish to opt-out of our placing cookies onto your computer or device, please see our&nbsp;<a href=\"https://footyaddicts.com/pages/privacy\">Privacy Policy</a>&nbsp;You may also wish to delete cookies which have already been placed. For instructions on how to do this, please consult your internet browser&rsquo;s help menu.</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Disclaimers</p>\r\n\r\n	<ol>\r\n		<li>Footy Addicts makes no warranty or representation that the Website will meet your requirements, that it will be of satisfactory quality, that it will be fit for a particular purpose, that it will not infringe the rights of third parties, that it will be compatible with all systems, that it will be secure and that all information provided will be accurate. We make no guarantee of any specific results from the use of our Services.</li>\r\n		<li>No part of this Website is intended to constitute advice and the Content of this Website should not be relied upon when making any decisions or taking any action of any kind.</li>\r\n		<li>The information on this Website is not designed with commercial purposes in mind. Commercial use of the Content of this Website is forbidden under sub-Clause 4.2.1 of these Terms and Conditions. Any such use constitutes a breach of these Terms and Conditions and Footy Addicts makes no representation or warranty that this Content is suitable for use in commercial situations or that it constitutes accurate data and / or advice on which business decisions can be based.</li>\r\n		<li>Whilst every effort has been made to ensure that all descriptions of services available from Footy Addicts correspond to the actual services available, Footy Addicts is not responsible for any variations from these descriptions.</li>\r\n		<li>Whilst Footy Addicts uses reasonable endeavours to ensure that the Website is secure and free of errors, viruses and other malware, all Users are advised to take responsibility for their own security, that of their personal details and their computers.</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Availability of the Website and Modifications</p>\r\n\r\n	<ol>\r\n		<li>The Service is provided &ldquo;as is&rdquo; and on an &ldquo;as available&rdquo; basis. We give no warranty that the Service will be free of defects and / or faults. To the maximum extent permitted by the law we provide no warranties (express or implied) of fitness for a particular purpose, accuracy of information, compatibility and satisfactory quality.</li>\r\n		<li>Footy Addicts accepts no liability for any disruption or non-availability of the Website resulting from external causes including, but not limited to, ISP equipment failure, host equipment failure, communications network failure, power failure, natural events, acts of war or legal restrictions and censorship.</li>\r\n		<li>Footy Addicts reserves the right to alter, suspend or discontinue any part (or the whole of) the Website including, but not limited to, the products and/or services available. These Terms and Conditions shall continue to apply to any modified version of the Website unless it is expressly stated otherwise.</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Liability</p>\r\n\r\n	<ol>\r\n		<li>To the maximum extent permitted by law, Footy Addicts accepts no liability for any direct or indirect loss or damage, foreseeable or otherwise, including any indirect, consequential, special or exemplary damages arising from the use of the Website or any information contained therein. Users should be aware that they use the Website and its Content at their own risk.</li>\r\n		<li>Nothing in these Terms and Conditions excludes or restricts Footy Addicts&rsquo; liability for death or personal injury resulting from any negligence or fraud on the part of Footy Addicts.</li>\r\n		<li>Whilst every effort has been made to ensure that these Terms and Conditions adhere strictly with the relevant provisions of the Unfair Contract Terms Act 1977, in the event that any of these terms are found to be unlawful, invalid or otherwise unenforceable, that term is to be deemed severed from these Terms and Conditions and shall not affect the validity and enforceability of the remaining terms and conditions. This term shall apply only within jurisdictions where a particular term is illegal.</li>\r\n		<li>In addition, no advice or information (oral or written) obtained by the User from Footy Addicts shall create any warranty.</li>\r\n		<li>You understand and agree that you download or otherwise obtain material or data through the use of our Website at your own discretion and risk and that you will be solely responsible for any damages to your computer system or loss of data that results from the download of such material or data.</li>\r\n		<li>Your use of our Website is at your sole risk. Our Website is provided to you &quot;as is&quot; and on an &quot;as available&quot; basis. We specifically disclaim all warranties and conditions of any kind, whether express, implied or statutory, including but not limited to the implied warranties of merchantability, fitness for a particular purpose and non-infringement. We disclaim any warranties regarding the security, reliability, timeliness, and performance of our Website. We disclaim any warranties for any information or advice obtained through our Website. We disclaim any warranties for services or goods received through or advertised on our Website or received through any links provided by our Website, as well as for any information or advice received through any links provided through our Website.</li>\r\n		<li>Your correspondence or business dealings with, or participation in promotions of, marketing partners or other third parties found on our Website or through our Website, including payment and delivery of related goods or services, and any other terms, conditions, warranties or representations associated with such dealings, are solely between you and such marketing partner or other third party. You agree that Footy Addicts shall not be responsible or liable for any loss or damage of any sort incurred as the result of any such dealings or as the result of the presence of such marketing partners or other third parties on our Website or located through the use of our Website.</li>\r\n		<li>Footy Addicts provide tools that enable Users to arrange physical meetings or events. Footy Addicts do not supervise these meetings or events and are not involved in any way with the actions of any individuals at these meetings or events. Footy Addicts do not supervise or control the meetings or events organised between Users on the Website.</li>\r\n		<li>Footy Addicts do not endorse the venues or venue owners arranged on the Website between Users. In addition, Footy Addicts do not attempt to confirm, and do not confirm, venue owner&#39;s purported identity. The User is responsible for determining the identity and suitability of others whom you contact via the Website.</li>\r\n		<li>You agree that in no event shall Footy Addicts be liable for any direct, indirect, incidental, special, consequential or exemplary damages, including but not limited to, damages for loss of profits, goodwill, use, data or other intangible losses (even if Footy Addicts has been advised of the possibility of such damages), arising out of or in connection with our Website or this Agreement or the inability to use our Website (however arising, including negligence), arising out of or in connection with Third Party Transactions or arising out of or in connection with your use of our Website or transportation to or from Footy Addicts Gatherings , attendance at Footy Addicts Gatherings, participation in or exclusion from Footy Addicts Groups and the actions of you or others at Footy Addicts Gatherings. Our liability to you or any third parties in any circumstance is limited to the greater of (a) the amount of fees, if any, you pay to us in the twelve (12) months prior to the action giving rise to liability, and (b) &pound;100.</li>\r\n		<li>Some jurisdictions do not allow the exclusion or limitation of certain warranties or of incidental or consequential damages. Accordingly, some of the limitations in this Section 14 may not apply to you.</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Indemnity</p>\r\n	You agree to indemnify and hold us and our officers, directors, shareholders, agents, employees, consultants, affiliates, subsidiaries and third-party partners harmless from any claim or demand, including reasonable legal fees, made by any third party due to or arising out of your breach of your representations and warranties or this Agreement or the documents it incorporates by reference, your use of our Website, Your Information, your violation of any law, statute, ordinance or regulation or the rights of a third party, your participation in a Footy Addicts Group, or your participation as an Organiser or Creator or Host or in Footy Addicts Meetings(whether the claim or demand is due to or arising out of your transportation to or from, attendance at, or the actions of you or other users at Footy Addicts Gatherings). Without limiting the foregoing, you, as an Organiser or Creator or Host, agree to indemnify and hold us and our officers, directors, shareholders, agents, employees, consultants, affiliates, subsidiaries and third-party partners harmless from any claim or demand, including reasonable attorneys&#39; fees, made by any Footy Addicts Group or third party due to or arising out of your actions as an Organiser or Creator or Host, including your use of money paid to you by members of your Footy Addicts Group.</li>\r\n	<li>\r\n	<p>No Waiver</p>\r\n	In the event that any party to these Terms and Conditions fails to exercise any right or remedy contained herein, this shall not be construed as a waiver of that right or remedy.</li>\r\n	<li>\r\n	<p>Previous Terms and Conditions</p>\r\n	In the event of any conflict between these Terms and Conditions and any prior versions thereof, the provisions of these Terms and Conditions shall prevail unless it is expressly stated otherwise.</li>\r\n	<li>\r\n	<p>Third Party Rights</p>\r\n	Nothing in these Terms and Conditions shall confer any rights upon any third party. The agreement created by these Terms and Conditions is between you and Footy Addicts.</li>\r\n	<li>\r\n	<p>Communications</p>\r\n\r\n	<ol>\r\n		<li>All notices / communications shall be given to us either by post to our Premises (see address above) or by email to (email address). Such notice will be deemed received 3 days after posting if sent by first class post, the day of sending if the email is received in full on a business day and on the next business day if the email is sent on a weekend or public holiday.</li>\r\n		<li>Footy Addicts may from time to time send you information about our products and/or services. We may also send you information regarding your account activity and purchases, as well as updates about the Website and Service as well as other promotional offers. You can always opt-out of our promotional e-mails at any time by clicking the unsubscribe link at the bottom of any of such e-mail correspondence; or you can opt-out of any marketing emails by managing your notifications, which is available as part of your Footy Addicts account.</li>\r\n	</ol>\r\n	</li>\r\n	<li>\r\n	<p>Law and Jurisdiction</p>\r\n	These Terms and Conditions and the relationship between you and Footy Addicts shall be governed by and construed in accordance with the Law of England and Wales and Footy Addicts and you agree to submit to the exclusive jurisdiction of the Courts of England and Wales.</li>\r\n</ol>', 3),
(3, 'About us', '<h2><strong>About</strong></h2>\r\n\r\n<h3>The fastest, simplest way to share your passion about football</h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Footy Addicts is a social platform that connects football enthusiasts all around Great Britain. People use Footy Addicts to play football, organize games, socialize, keep fit and have fun!</p>\r\n\r\n<p>Footy Addicts believe that players should have the freedom to play football, anytime, anyplace.</p>\r\n\r\n<p>All levels of players are welcome to Footy Addicts,&nbsp;<a href=\"https://footyaddicts.com/users/sign_up\">sign up</a>&nbsp;for free and find the game that suits you. Didn&rsquo;t find one? Why don&rsquo;t you&nbsp;<a href=\"https://footyaddicts.com/football-games/new\">start your own</a>&nbsp;game and invite your friends?</p>\r\n\r\n<p>Footy Addicts develops around the community needs, so your&nbsp;<a href=\"https://footyaddicts.uservoice.com/forums/128329-moving-forward\" target=\"_blank\">feedback</a>&nbsp;is vital.&nbsp;<a href=\"https://footyaddicts.uservoice.com/forums/128329-moving-forward\" target=\"_blank\">Tell us how Footy Addicts can improve</a></p>\r\n\r\n<p><em>Football games, everywhere, anytime, for everybody &hellip;</em></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>Footy Addicts for Players</h3>\r\n\r\n<p>Footy Addicts brings the football community together. Players can&nbsp;<a href=\"https://footyaddicts.com/football-games\">search for games</a>&nbsp;by venue or date, interact with the rest of the group, give feedback &amp; share real time information</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>Footy Addicts for Hosts</h3>\r\n\r\n<p>Footy Addicts makes games organization easy. Easy to set up date, time, venue, number of players. Real time attendance list, so you know who is coming and who is not. Comment before and after the game, asking questions or giving answers. Why don&rsquo;t you give it a go?</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>Footy Addicts &amp; Partnerships</h3>\r\n\r\n<p>We will always look for people who are passionate about football, love the game &amp; share their passion with the community. If you believe that you want to be more active in our community feel free to&nbsp;<a href=\"javascript:void(0)\">Contact us</a></p>\r\n\r\n<p>We do appreciate your free time, so it&rsquo;s up to you how much of it you want to spend.</p>\r\n\r\n<p>Just drop as line and we can figure out what suits you best together. email us at: <a href=\"mailto:info@letsplaysports.com\" style=\"color: #c80000;\">info@letsplaysports.com</a></p>', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('seoisys@gmail.com', '$2y$10$CkUoaRuxCpR612wtOi.Xbua/rf478n7Z7BIfSY0ohvmHA0vSAsTH2', '2019-09-03 10:37:05');

-- --------------------------------------------------------

--
-- Table structure for table `pitch_hire`
--

CREATE TABLE `pitch_hire` (
  `id` bigint(127) NOT NULL,
  `user_id` bigint(127) NOT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `time` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `sport_id` int(11) NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_block_booking` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quick_links_slider`
--

CREATE TABLE `quick_links_slider` (
  `id` int(11) NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `quick_links_slider`
--

INSERT INTO `quick_links_slider` (`id`, `image`, `title`, `description`, `link`, `status`, `created_at`, `updated_at`) VALUES
(2, '1567679964.jpg', 'Join A League', 'Find a 5,6 or 7-a-side football league near you', '#', 1, '2019-09-21 16:48:41', '2019-09-21 15:48:41'),
(3, '1568033305.jpg', 'Corporate Events', 'Book your Company fun day or tournament', 'https://webchoice-test.uk/lps/corporate', 1, '2019-09-18 10:16:57', '2019-09-18 09:16:57'),
(4, '1567680067.jpg', 'Kids Parties', 'Football and fun, everything a party should be.', 'https://webchoice-test.uk/lps/kids-parties', 1, '2019-09-18 10:17:12', '2019-09-18 09:17:12'),
(5, '1567602541.jpg', 'She can play', 'Womens only Sessions for all Sports', 'https://webchoice-test.uk/lps/woomengame', 1, '2019-09-09 12:51:49', '2019-09-09 11:51:49'),
(6, '1565684005.jpg', 'SOFT PLAY HIRE', 'View our soft play options from ages 2 +', 'https://webchoice-test.uk/lps/kids/soft-play-hire', 1, '2019-09-18 10:17:42', '2019-09-18 09:17:42');

-- --------------------------------------------------------

--
-- Table structure for table `repeat_games`
--

CREATE TABLE `repeat_games` (
  `id` bigint(127) NOT NULL,
  `game_id` bigint(127) DEFAULT NULL,
  `game_start_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `repeat_games`
--

INSERT INTO `repeat_games` (`id`, `game_id`, `game_start_date`) VALUES
(3, 12, '2019-10-06'),
(4, 13, '2019-10-07'),
(5, 14, '2019-10-13'),
(6, 15, '2019-10-14'),
(8, 17, '2019-10-21'),
(9, 18, '2019-10-27'),
(11, 20, '2019-10-20'),
(12, 21, '2019-10-21'),
(13, 22, '2019-10-27'),
(307, 28, '2019-09-28'),
(308, 28, '2019-10-05'),
(309, 28, '2019-10-12'),
(310, 28, '2019-10-19'),
(311, 28, '2019-10-26'),
(312, 28, '2019-11-02'),
(313, 28, '2019-11-09'),
(314, 28, '2019-11-16'),
(315, 28, '2019-11-23'),
(316, 28, '2019-11-30'),
(381, 35, '2019-10-06'),
(382, 35, '2019-10-13'),
(383, 35, '2019-10-20'),
(384, 35, '2019-10-27'),
(385, 35, '2019-11-03'),
(386, 35, '2019-11-10'),
(387, 35, '2019-11-17'),
(388, 35, '2019-11-24'),
(389, 35, '2019-12-01'),
(390, 35, '2019-12-08'),
(391, 35, '2019-12-15'),
(392, 17, '2019-09-27'),
(420, 17, '2019-09-28'),
(421, 32, '2019-10-10'),
(422, 32, '2019-10-17'),
(423, 32, '2019-10-24'),
(424, 32, '2019-10-31'),
(425, 32, '2019-11-07'),
(426, 32, '2019-11-14'),
(427, 32, '2019-11-21'),
(428, 32, '2019-11-28'),
(429, 32, '2019-12-05'),
(430, 32, '2019-12-12'),
(647, 40, '2019-09-30'),
(648, 41, '2019-10-07'),
(649, 41, '2019-10-08'),
(650, 41, '2019-10-09'),
(868, 47, '2019-10-05'),
(869, 47, '2019-10-12'),
(870, 47, '2019-10-19'),
(871, 47, '2019-10-26'),
(872, 47, '2019-11-02'),
(873, 47, '2019-11-09'),
(874, 47, '2019-11-16'),
(875, 47, '2019-11-23'),
(876, 47, '2019-11-30'),
(877, 47, '2019-12-07'),
(878, 47, '2019-12-14'),
(879, 47, '2019-12-21'),
(916, 17, '2019-09-30'),
(917, 12, '2019-09-30'),
(942, 46, '2019-10-06'),
(943, 46, '2019-10-13'),
(944, 46, '2019-10-20'),
(945, 46, '2019-10-27'),
(946, 46, '2019-11-03'),
(947, 46, '2019-11-10'),
(948, 46, '2019-11-17'),
(949, 46, '2019-11-24'),
(950, 46, '2019-12-01'),
(951, 46, '2019-12-08'),
(952, 46, '2019-12-15'),
(953, 46, '2019-12-22'),
(954, 46, '2019-12-29'),
(955, 45, '2019-10-06'),
(956, 45, '2019-10-13'),
(957, 45, '2019-10-20'),
(958, 45, '2019-10-27'),
(959, 45, '2019-11-03'),
(960, 45, '2019-11-10'),
(961, 45, '2019-11-17'),
(962, 45, '2019-11-24'),
(963, 45, '2019-12-01'),
(964, 45, '2019-12-08'),
(965, 45, '2019-12-15'),
(966, 45, '2019-12-22'),
(967, 43, '2019-10-07'),
(968, 43, '2019-10-14'),
(969, 43, '2019-10-21'),
(970, 43, '2019-10-28'),
(971, 43, '2019-11-04'),
(972, 43, '2019-11-11'),
(973, 43, '2019-11-18'),
(974, 43, '2019-11-25'),
(975, 43, '2019-12-02'),
(976, 43, '2019-12-09'),
(977, 43, '2019-12-16'),
(978, 43, '2019-12-23'),
(979, 50, '2019-10-05'),
(980, 50, '2019-10-12'),
(981, 50, '2019-10-19'),
(982, 50, '2019-10-26'),
(983, 50, '2019-11-02'),
(984, 50, '2019-11-09'),
(985, 50, '2019-11-16'),
(986, 50, '2019-11-23'),
(987, 50, '2019-11-30'),
(988, 50, '2019-12-07'),
(989, 50, '2019-12-14'),
(990, 50, '2019-12-21'),
(991, 48, '2019-10-05'),
(992, 48, '2019-10-12'),
(993, 48, '2019-10-19'),
(994, 48, '2019-10-26'),
(995, 48, '2019-11-02'),
(996, 48, '2019-11-09'),
(997, 48, '2019-11-16'),
(998, 48, '2019-11-23'),
(999, 48, '2019-11-30'),
(1000, 48, '2019-12-07'),
(1001, 48, '2019-12-14'),
(1002, 48, '2019-12-21'),
(1051, 39, '2019-10-01'),
(1052, 39, '2019-10-08'),
(1053, 39, '2019-10-15'),
(1054, 39, '2019-10-22'),
(1055, 39, '2019-10-29'),
(1056, 39, '2019-11-05'),
(1057, 39, '2019-11-12'),
(1058, 39, '2019-11-19'),
(1059, 39, '2019-11-26'),
(1060, 39, '2019-12-03'),
(1061, 39, '2019-12-10'),
(1062, 39, '2019-12-17'),
(1063, 38, '2019-10-01'),
(1064, 38, '2019-10-08'),
(1065, 38, '2019-10-15'),
(1066, 38, '2019-10-22'),
(1067, 38, '2019-10-29'),
(1068, 38, '2019-11-05'),
(1069, 38, '2019-11-12'),
(1070, 38, '2019-11-19'),
(1071, 38, '2019-11-26'),
(1072, 38, '2019-12-03'),
(1073, 38, '2019-12-10'),
(1074, 38, '2019-12-17'),
(1087, 44, '2019-10-03'),
(1088, 44, '2019-10-10'),
(1089, 44, '2019-10-17'),
(1090, 44, '2019-10-24'),
(1091, 44, '2019-10-31'),
(1092, 44, '2019-11-07'),
(1093, 44, '2019-11-14'),
(1094, 44, '2019-11-21'),
(1095, 44, '2019-11-28'),
(1096, 44, '2019-12-05'),
(1097, 44, '2019-12-12'),
(1098, 44, '2019-12-19'),
(1147, 37, '2019-10-03'),
(1148, 37, '2019-10-10'),
(1149, 37, '2019-10-17'),
(1150, 37, '2019-10-24'),
(1151, 37, '2019-10-31'),
(1152, 37, '2019-11-07'),
(1153, 37, '2019-11-14'),
(1154, 37, '2019-11-21'),
(1155, 37, '2019-11-28'),
(1156, 37, '2019-12-05'),
(1157, 37, '2019-12-12'),
(1158, 37, '2019-12-19'),
(1183, 42, '2019-10-02'),
(1184, 42, '2019-10-09'),
(1185, 42, '2019-10-16'),
(1186, 42, '2019-10-23'),
(1187, 42, '2019-10-30'),
(1188, 42, '2019-11-06'),
(1189, 42, '2019-11-13'),
(1190, 42, '2019-11-20'),
(1191, 42, '2019-11-27'),
(1192, 42, '2019-12-04'),
(1193, 42, '2019-12-11'),
(1194, 42, '2019-12-18'),
(1195, 51, '2019-10-01'),
(1196, 51, '2019-10-08'),
(1197, 51, '2019-10-15'),
(1198, 51, '2019-10-22'),
(1199, 51, '2019-10-29'),
(1200, 51, '2019-11-05'),
(1201, 51, '2019-11-12'),
(1202, 51, '2019-11-19'),
(1203, 51, '2019-11-26'),
(1204, 51, '2019-12-03'),
(1205, 51, '2019-12-10'),
(1206, 51, '2019-12-17');

-- --------------------------------------------------------

--
-- Table structure for table `site_setting`
--

CREATE TABLE `site_setting` (
  `id` int(11) NOT NULL,
  `website_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `header_logo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `footer_logo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `favicon_icon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_store_link` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `play_store_link` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_url` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_url` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram_url` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `copyright` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `website_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sendmail_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_setting`
--

INSERT INTO `site_setting` (`id`, `website_name`, `website_title`, `header_logo`, `footer_logo`, `favicon_icon`, `app_store_link`, `play_store_link`, `facebook_url`, `twitter_url`, `instagram_url`, `copyright`, `website_email`, `sendmail_email`, `phone_number`, `city`, `address`, `latitude`, `longitude`) VALUES
(1, 'Let\'s Play Sports', 'Lets Play Sports - Play Football, Netball And Sports Near You', '1569222077.png', '1565617167.png', '1565617167.png', '#', '#', 'https://www.facebook.com/Lets-Play-Sports-100193351370893/', 'https://twitter.com/LetsPlaySports1', 'https://www.instagram.com/lets_playsports/', '©2019 Let’s Play Sport. All Right Reserved. <a href=\"https://webdesignchoice.co.uk/\" target=\"_blank\">Web Development </a>by Web Choice', 'info@letsplaysports.co.uk', 'info@letsplaysports.co.uk', '020 3589 6019', 'London', '24 Viaduct Street,  Holborn,  EC1A 2BN', '51.517115', '-0.120066');

-- --------------------------------------------------------

--
-- Table structure for table `sportsturnaments_blogs`
--

CREATE TABLE `sportsturnaments_blogs` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sportsturnaments_blogs`
--

INSERT INTO `sportsturnaments_blogs` (`id`, `category_id`, `title`, `slug`, `description`, `image`, `created_at`, `updated_at`) VALUES
(2, 2, 'Location', 'location-2', 'We are affiliated with Locations acorss the Uk and host any style tournament (5,6,7,11 a side) for as many people as you require. All you have to do is tell us where you wish the event to take place and the ideal times and we will give you a list of suitable options', '1568703096.jpg', '2019-09-17 08:02:02', '2019-09-17 07:02:02'),
(3, 2, 'Management And Staffing', 'management-and-staffing-2', 'Dont want to carry anything to the venue? Dont worry we will have everything ready for you from balls to bibs and any extras you may require for your events.', '1568721333.jpg', '2019-09-17 10:55:33', '2019-09-17 10:55:33'),
(4, 2, 'Trophies And Extras', 'trophies-and-extras-3', 'Our packs include Trophies for winners and runners up along with medals. You can choose from standard 10\" trophies to large 22\" inch trophies so you can really spoil the winners and there is even a plate for the losers.', '1568721356.jpg', '2019-09-17 10:55:56', '2019-09-17 10:55:56'),
(5, 2, 'Equipment', 'equipment-4', 'Not sure how to run the event and organise games? Well don\'t worry we can post and create fixtures for you and post them online 48 hours before kick off so your teams know exactly what time theyre playing and where. This also adds vital build up to what could be a long awaited tournament. On the day if you need a dedicated event manager to ensure smooth operation alongside referees then fear not, we can get anyone anywhere and make sure that all the bad boys and girls are kept in line. Download our brochure here', '1568721404.jpg', '2019-09-17 10:56:44', '2019-09-17 10:56:44'),
(6, 3, 'Locations', 'locations-5', 'We are affiliated with Locations acorss the Uk and host any style tournament (5,6,7,11 a side) for as many people as you require. All you have to do is tell us where you wish the event to take place and the ideal times and we will give you a list of suitable options', '1568721474.jpg', '2019-09-17 10:57:54', '2019-09-17 10:57:54'),
(7, 3, 'Management And Staffing', 'management-and-staffing-6', 'Dont want to carry anything to the venue? Dont worry we will have everything ready for you from balls to bibs and any extras you may require for your events.', '1568722647.jpg', '2019-09-17 11:17:27', '2019-09-17 11:17:27'),
(8, 3, 'Trophies And Extras', 'trophies-and-extras-7', 'Our packs include Trophies for winners and runners up along with medals. You can choose from standard 10\" trophies to large 22\" inch trophies so you can really spoil the winners and there is even a plate for the losers.', '1568722664.jpg', '2019-09-17 11:17:44', '2019-09-17 11:17:44'),
(9, 3, 'Equipment', 'equipment-8', 'Not sure how to run the event and organise games? Well don\'t worry we can post and create fixtures for you and post them online 48 hours before kick off so your teams know exactly what time theyre playing and where. This also adds vital build up to what could be a long awaited tournament. On the day if you need a dedicated event manager to ensure smooth operation alongside referees then fear not, we can get anyone anywhere and make sure that all the bad boys and girls are kept in line. Download our brochure here', '1568722681.jpg', '2019-09-17 11:18:01', '2019-09-17 11:18:01'),
(10, 4, 'Locations', 'locations-9', 'We are affiliated with Locations acorss the Uk and host any style tournament (5,6,7,11 a side) for as many people as you require. All you have to do is tell us where you wish the event to take place and the ideal times and we will give you a list of suitable options', '1568723103.jpg', '2019-09-17 11:25:03', '2019-09-17 11:25:03'),
(11, 4, 'Management And Staffing', 'management-and-staffing-10', 'Dont want to carry anything to the venue? Dont worry we will have everything ready for you from balls to bibs and any extras you may require for your events.', '1568723135.jpg', '2019-09-17 11:25:35', '2019-09-17 11:25:35'),
(12, 4, 'Trophies And Extras', 'trophies-and-extras-11', 'Our packs include Trophies for winners and runners up along with medals. You can choose from standard 10\" trophies to large 22\" inch trophies so you can really spoil the winners and there is even a plate for the losers.', '1568723155.jpg', '2019-09-17 11:25:55', '2019-09-17 11:25:55'),
(13, 4, 'Equipment', 'equipment-12', 'Not sure how to run the event and organise games? Well don\'t worry we can post and create fixtures for you and post them online 48 hours before kick off so your teams know exactly what time theyre playing and where. This also adds vital build up to what could be a long awaited tournament. On the day if you need a dedicated event manager to ensure smooth operation alongside referees then fear not, we can get anyone anywhere and make sure that all the bad boys and girls are kept in line. Download our brochure here', '1568723170.jpg', '2019-09-17 11:26:10', '2019-09-17 11:26:10'),
(14, 5, 'Locations', 'locations-13', 'We are affiliated with Locations acorss the Uk and host any style tournament (5,6,7,11 a side) for as many people as you require. All you have to do is tell us where you wish the event to take place and the ideal times and we will give you a list of suitable options', '1568723218.jpg', '2019-09-17 11:26:58', '2019-09-17 11:26:58'),
(15, 5, 'Management And Staffing', 'management-and-staffing-14', 'Dont want to carry anything to the venue? Dont worry we will have everything ready for you from balls to bibs and any extras you may require for your events.', '1568723245.jpg', '2019-09-17 11:27:25', '2019-09-17 11:27:25'),
(16, 5, 'Trophies And Extras', 'trophies-and-extras-15', 'Our packs include Trophies for winners and runners up along with medals. You can choose from standard 10\" trophies to large 22\" inch trophies so you can really spoil the winners and there is even a plate for the losers.', '1568723275.jpg', '2019-09-17 11:27:55', '2019-09-17 11:27:55'),
(17, 6, 'Locations', 'locations-16', 'We are affiliated with Locations acorss the Uk and host any style tournament (5,6,7,11 a side) for as many people as you require. All you have to do is tell us where you wish the event to take place and the ideal times and we will give you a list of suitable options', '1568723355.jpg', '2019-09-17 11:29:15', '2019-09-17 11:29:15'),
(18, 6, 'Management And Staffing', 'management-and-staffing-17', 'Dont want to carry anything to the venue? Dont worry we will have everything ready for you from balls to bibs and any extras you may require for your events.', '1568723374.jpg', '2019-09-17 11:29:34', '2019-09-17 11:29:34'),
(19, 6, 'Trophies And Extras', 'trophies-and-extras-18', 'Our packs include Trophies for winners and runners up along with medals. You can choose from standard 10\" trophies to large 22\" inch trophies so you can really spoil the winners and there is even a plate for the losers.', '1568723393.jpg', '2019-09-17 11:29:53', '2019-09-17 11:29:53'),
(20, 6, 'Equipment', 'equipment-19', 'Not sure how to run the event and organise games? Well don\'t worry we can post and create fixtures for you and post them online 48 hours before kick off so your teams know exactly what time theyre playing and where. This also adds vital build up to what could be a long awaited tournament. On the day if you need a dedicated event manager to ensure smooth operation alongside referees then fear not, we can get anyone anywhere and make sure that all the bad boys and girls are kept in line. Download our brochure here', '1568723415.jpg', '2019-09-17 11:30:15', '2019-09-17 11:30:15');

-- --------------------------------------------------------

--
-- Table structure for table `sportsturnaments_category`
--

CREATE TABLE `sportsturnaments_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sportsturnaments_category`
--

INSERT INTO `sportsturnaments_category` (`id`, `name`, `slug`, `image`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Drapers Field', 'drapers-field', '1568702870.jpg', 1, '2019-09-17 01:17:50', '2019-09-17 01:17:50'),
(3, 'Paradise Park', 'paradise-park', '1568702892.jpg', 1, '2019-09-17 01:18:12', '2019-09-17 01:18:12'),
(4, 'West Way Sports centre', 'west-way-sports-centre', '1568702928.jpg', 1, '2019-09-17 01:18:48', '2019-09-17 01:18:48'),
(5, 'Rocks Lane Chiswick', 'rocks-lane-chiswick', '1568702949.jpg', 1, '2019-09-17 01:19:09', '2019-09-17 01:19:09'),
(6, 'Archbishop Park', 'archbishop-park', '1568702988.png', 1, '2019-09-17 01:19:48', '2019-09-17 01:19:48'),
(7, 'Drapers Field', 'drapers-field', '1568703016.jpg', 1, '2019-09-17 08:01:52', '2019-09-17 07:01:52');

-- --------------------------------------------------------

--
-- Table structure for table `sportsturnaments_details`
--

CREATE TABLE `sportsturnaments_details` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `feature_image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `main_video` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `sec_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sec_description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `layout` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sportsturnaments_details`
--

INSERT INTO `sportsturnaments_details` (`id`, `title`, `feature_image`, `banner`, `category_id`, `main_video`, `description`, `sec_title`, `sec_description`, `layout`, `created_at`, `updated_at`) VALUES
(1, 'Drapers Field', '1568705336.jpg', '1568705037.jpg', 2, '<iframe width=\"630\" height=\"370\" src=\"https://www.youtube.com/embed/Wz7qmJix1S8\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '<p>Fun filled, fast paced action for your corporate event. We have a huge range of activities we can include within your sports day package. Please email us on info@letsplaysports.co.uk or call 020 3589 6019 for a quote.</p>', 'Drapers Field', '<p>Not sure how to run the event and organise games? Well don&#39;t worry we can post and create fixtures for you and post them online 48 hours before kick off so your teams know exactly what time theyre playing and where</p>', 1, '2019-09-17 01:23:42', '2019-09-17 11:13:50'),
(2, 'Paradise Park', '1568705355.jpg', '1568705077.jpg', 3, '<iframe width=\"630\" height=\"370\" src=\"https://www.youtube.com/embed/Wz7qmJix1S8\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '<p>Fun filled, fast paced action for your corporate event. We have a huge range of activities we can include within your sports day package. Please email us on info@letsplaysports.co.uk or call 020 3589 6019 for a quote.</p>', 'Paradise Park', '<p>Fun filled, fast paced action for your corporate event. We have a huge range of activities we can include within your sports day package. Please email us on info@letsplaysports.co.uk or call 020 3589 6019 for a quote.</p>', 1, '2019-09-17 01:24:58', '2019-09-17 11:14:39'),
(3, 'West Way Sports Center', '1568720258.jpg', '1568720615.jpg', 4, '<iframe width=\"630\" height=\"370\" src=\"https://www.youtube.com/embed/Wz7qmJix1S8\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '<p>Fun filled, fast paced action for your corporate event. We have a huge range of activities we can include within your sports day package. Please email us on info@letsplaysports.co.uk or call 020 3589 6019 for a quote.</p>', 'West Way Sports Center', '<p>Not sure how to run the event and organise games? Well don&#39;t worry we can post and create fixtures for you and post them online 48 hours before kick off so your teams know exactly what time theyre playing and where</p>', 1, '2019-09-17 10:37:38', '2019-09-17 11:15:17'),
(4, 'Rock Lane Chiswick', '1568720912.jpg', '1568720912.jpg', 5, '<iframe width=\"630\" height=\"370\" src=\"https://www.youtube.com/embed/Wz7qmJix1S8\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '<p>Fun filled, fast paced action for your corporate event. We have a huge range of activities we can include within your sports day package. Please email us on info@letsplaysports.co.uk or call 020 3589 6019 for a quote.</p>', 'Rock Lane Chiswick', '<p>Not sure how to run the event and organise games? Well don&#39;t worry we can post and create fixtures for you and post them online 48 hours before kick off so your teams know exactly what time theyre playing and where</p>', 1, '2019-09-17 10:48:32', '2019-09-17 11:16:01'),
(5, 'Archbishop Park', '1568721157.jpg', '1568721195.jpg', 6, '<iframe width=\"630\" height=\"370\" src=\"https://www.youtube.com/embed/Wz7qmJix1S8\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '<p>Fun filled, fast paced action for your corporate event. We have a huge range of activities we can include within your sports day package. Please email us on info@letsplaysports.co.uk or call 020 3589 6019 for a quote.</p>', 'Archbishop Park', '<p>Not sure how to run the event and organise games? Well don&#39;t worry we can post and create fixtures for you and post them online 48 hours before kick off so your teams know exactly what time theyre playing and where</p>', 1, '2019-09-17 10:52:37', '2019-09-17 11:16:39');

-- --------------------------------------------------------

--
-- Table structure for table `sportsturnaments_faq`
--

CREATE TABLE `sportsturnaments_faq` (
  `id` int(11) NOT NULL,
  `cp_id` int(11) DEFAULT NULL,
  `title` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sportsturnaments_images`
--

CREATE TABLE `sportsturnaments_images` (
  `id` bigint(127) NOT NULL,
  `cp_id` bigint(127) NOT NULL,
  `image` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sportsturnaments_images`
--

INSERT INTO `sportsturnaments_images` (`id`, `cp_id`, `image`) VALUES
(1, 1, 'about-img.jpg'),
(2, 1, 'corporate-banner.jpg'),
(3, 2, 'about-img.jpg'),
(4, 2, 'detail-video.jpg'),
(6, 3, 'Join-A-League.jpg'),
(7, 3, 'detail-video.jpg'),
(8, 3, 'football-harris-academy-thumb.jpg'),
(9, 4, 'detail-video.jpg'),
(10, 4, 'football-harris-academy-thumb.jpg'),
(11, 4, 'champions-experience-thumb2.jpg'),
(12, 4, 'Join-A-League.jpg'),
(13, 5, 'detail-video.jpg'),
(14, 5, 'football-harris-academy-thumb.jpg'),
(15, 5, 'champions-experience-thumb.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `sportsturnaments_videos`
--

CREATE TABLE `sportsturnaments_videos` (
  `id` int(11) NOT NULL,
  `cp_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sportsturnaments_videos`
--

INSERT INTO `sportsturnaments_videos` (`id`, `cp_id`, `title`, `video`) VALUES
(4, 1, 'Bungee Run', '<iframe width=\"100%\" height=\"400\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(5, 1, 'Assault Course', '<iframe width=\"100%\" height=\"400\" src=\"https://www.youtube.com/embed/fWwR3O83kw0\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(6, 1, 'Wrecking Ball', '<iframe width=\"100%\" height=\"400\" src=\"https://www.youtube.com/embed/fWwR3O83kw0\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(7, 1, 'Gladiator Duel', '<iframe width=\"100%\" height=\"400\" src=\"https://www.youtube.com/embed/fWwR3O83kw0\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(8, 1, 'Sumo Suits', '<iframe width=\"100%\" height=\"400\" src=\"https://www.youtube.com/embed/fWwR3O83kw0\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(9, 1, 'Last Man standing', '<iframe width=\"100%\" height=\"400\" src=\"https://www.youtube.com/embed/fWwR3O83kw0\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(10, 1, 'Dash and grab', '<iframe width=\"100%\" height=\"400\" src=\"https://www.youtube.com/embed/fWwR3O83kw0\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(11, 3, 'Bungee Run', '<iframe width=\"100%\" height=\"350\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(12, 3, 'Assault Course', '<iframe width=\"100%\" height=\"350\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(13, 3, 'Wrecking Ball', '<iframe width=\"100%\" height=\"350\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(14, 3, 'Gladiator Duel', '<iframe width=\"100%\" height=\"350\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(15, 3, 'Sumo Suits', '<iframe width=\"100%\" height=\"350\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(16, 3, 'Last Man standing', '<iframe width=\"100%\" height=\"350\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(17, 3, 'Dash and grab', '<iframe width=\"100%\" height=\"350\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(18, 4, 'Bungee Run', '<iframe width=\"100%\" height=\"350\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(19, 4, 'Assault Course', '<iframe width=\"100%\" height=\"350\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(20, 4, 'Wrecking Ball', '<iframe width=\"100%\" height=\"350\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(21, 4, 'Gladiator Duel', '<iframe width=\"100%\" height=\"350\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(22, 4, 'Sumo Suits', '<iframe width=\"100%\" height=\"350\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(23, 4, 'Last Man standing', '<iframe width=\"100%\" height=\"350\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(24, 4, 'Dash and grab', '<iframe width=\"100%\" height=\"350\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(25, 5, 'Bungee Run', '<iframe width=\"100%\" height=\"400\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(26, 5, 'Assault Course', '<iframe width=\"100%\" height=\"400\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(27, 5, 'Wrecking Ball', '<iframe width=\"100%\" height=\"400\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(28, 5, 'Gladiator Duel', '<iframe width=\"100%\" height=\"400\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(29, 5, 'Sumo Suits', '<iframe width=\"100%\" height=\"400\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(30, 5, 'Last Man standing', '<iframe width=\"100%\" height=\"400\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(31, 5, 'Dash and grab', '<iframe width=\"100%\" height=\"400\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(32, 2, 'Bungee Run', '<iframe width=\"100%\" height=\"400\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(33, 2, 'Assault Course', '<iframe width=\"100%\" height=\"400\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(34, 2, 'Wrecking Ball', '<iframe width=\"100%\" height=\"400\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(35, 2, 'Gladiator Duel', '<iframe width=\"100%\" height=\"400\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(36, 2, 'Sumo Suits', '<iframe width=\"100%\" height=\"400\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(37, 2, 'Last Man standing', '<iframe width=\"100%\" height=\"400\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(38, 2, 'Dash and grab', '<iframe width=\"100%\" height=\"400\" src=\"https://www.youtube.com/embed/oz0egzRr01k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>');

-- --------------------------------------------------------

--
-- Table structure for table `team_spots`
--

CREATE TABLE `team_spots` (
  `id` bigint(127) NOT NULL,
  `game_id` bigint(127) NOT NULL,
  `user_id` bigint(127) NOT NULL,
  `team` int(11) DEFAULT NULL COMMENT '1 Light Tees, 2 Dark Tees',
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `team_spots`
--

INSERT INTO `team_spots` (`id`, `game_id`, `user_id`, `team`, `status`) VALUES
(3, 3, 1, 1, 0),
(4, 4, 1, 1, 1),
(5, 5, 1, 1, 1),
(7, 5, 3, 2, 0),
(8, 7, 8, 1, 1),
(9, 7, 1, 2, 1),
(10, 8, 1, 1, 1),
(11, 9, 1, 1, 1),
(12, 8, 2, 2, 1),
(13, 10, 11, 1, 1),
(14, 10, 8, 2, 1),
(15, 11, 12, 1, 1),
(16, 20, 4, 1, 1),
(17, 21, 4, 1, 1),
(18, 22, 4, 1, 1),
(19, 40, 1, 1, 1),
(20, 41, 21, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `dob` date DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_img` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `gender` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1 male, 2 female',
  `nationality` smallint(6) NOT NULL DEFAULT 0,
  `facebook_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `nf_comment` tinyint(4) NOT NULL DEFAULT 0,
  `nf_game` tinyint(4) NOT NULL DEFAULT 0,
  `nf_attendance` tinyint(4) NOT NULL DEFAULT 0,
  `nf_newsletter` tinyint(4) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `user_role` tinyint(4) NOT NULL COMMENT '1 Admin, 2 User, 3 Company, 4 Advertiser',
  `is_cancel` tinyint(4) NOT NULL DEFAULT 0,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activation_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `api_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `first_name`, `last_name`, `phone_number`, `username`, `dob`, `address`, `profile_img`, `gender`, `nationality`, `facebook_id`, `nf_comment`, `nf_game`, `nf_attendance`, `nf_newsletter`, `status`, `user_role`, `is_cancel`, `active`, `remember_token`, `activation_token`, `api_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'null', 'tpsvishwas78@gmail.com', '2019-08-28 18:30:00', '$2y$10$E7O4mxVnhr0B5iw9XVu66uBtgfiatzGcgfgjNZx6o1iK0G01q1.fK', 'Cris', 'Moires', '12345678900', 'tapas-vishwas', NULL, NULL, 'null', 0, 0, 'null', 0, 0, 0, 0, 1, 2, 0, 0, NULL, 'null', 'null', '2019-08-29 08:02:14', '2019-08-30 18:30:51', NULL),
(2, 'null', 'tapas@agnitotechnologies.com', '2019-08-28 18:30:00', '$2y$10$epcG4Sdkw6i3K/vnT8HF0uojJH2njXkxjAjR3FjTTwIGDB0Qoo.oK', 'Mark', 'Doe', '12345678900', 'neerav-kumar', NULL, NULL, 'null', 0, 0, 'null', 0, 0, 0, 0, 1, 2, 0, 0, NULL, 'null', 'null', '2019-08-29 08:05:43', '2019-09-19 15:06:23', NULL),
(3, 'null', 'shriram@gmail.com', '2019-08-29 18:30:00', '$2y$10$epcG4Sdkw6i3K/vnT8HF0uojJH2njXkxjAjR3FjTTwIGDB0Qoo.oK', 'Jonson', 'Jacod', '12345678900', 'shirram-chaurasiya', NULL, NULL, 'null', 0, 0, 'null', 0, 0, 0, 0, 0, 2, 0, 0, NULL, 'null', 'null', '2019-08-30 03:00:34', '2019-08-30 03:00:34', NULL),
(4, 'admin', 'admin@gmail.com', '2019-09-19 23:00:00', '$2y$10$HjzNoo9w6q0JoA/TddZ7neXoTAckgfVN4W3WJOgxhajYVRFtW5rhO', 'admin', 'admin', '12345678900', 'admin', NULL, 'Test', '1564827952.png', 0, 0, 'null', 0, 0, 0, 0, 1, 1, 0, 1, NULL, 'null', 'null', NULL, '2019-09-06 05:16:39', NULL),
(6, 'null', 'Johndoe@gmail.com', '2019-08-30 23:00:00', '$2y$10$9Dr7Wxv93UUhstBGO427iuMVGc0Z7glvQySUoaFIrk9YV0L9XkWRa', 'John', 'doe', '12345678900', 'john-doe-5', NULL, NULL, 'null', 0, 0, 'null', 0, 1, 1, 1, 1, 2, 0, 1, NULL, 'null', 'null', '2019-08-31 06:29:50', '2019-09-17 11:52:09', NULL),
(7, 'null', 'siddharthkekre@gmail.com', '2019-08-30 23:00:00', '$2y$10$kOSnL9Tr.l02yU3MZsRdYuThTHmKyr6oBP1Af8UKpB4TaQvkh8j5a', 'John', 'Doe', '12345678987', 'john-doe', NULL, NULL, 'null', 0, 0, 'null', 0, 0, 0, 0, 0, 2, 0, 0, NULL, 'null', 'null', '2019-08-31 06:35:12', '2019-08-31 10:46:48', NULL),
(8, 'null', 'fati@simplemail.top', '2019-08-31 06:45:53', '$2y$10$B9QvA/BH1UpKxGJutfaAuOKwddH2C/VuQm68ci7JxC.BtZOvrr8Pm', 'Alex', 'Martin', '14151658789', 'alex-martin', '1989-05-18', NULL, '1567237943.png', 1, 2, 'null', 0, 0, 0, 0, 0, 2, 0, 0, NULL, 'null', 'null', '2019-08-31 06:41:19', '2019-08-31 07:34:54', NULL),
(9, 'null', 'seoisys@gmail.com', '2019-09-02 19:52:59', '$2y$10$pdoqCrpNDXFJIXmZMnrPXutC7Quc2TwX0UqkmA4oCLJLr7gL//nQS', 'Bobby', 'Grosse', '98930155211', 'bobby-grosse', NULL, NULL, 'null', 0, 0, 'null', 0, 0, 0, 0, 0, 2, 0, 0, NULL, 'null', 'null', '2019-09-02 19:52:21', '2019-09-02 19:52:59', NULL),
(10, 'null', 'varaye@simplemail.top', '2019-09-03 06:53:48', '$2y$10$OnyFV6y.AGjoOTGx.2nOq.DOm/OrA6tG9RnUceePa8.yI9EZNMayG', 'John', 'Jacobs', '48579685215', 'siddharthkekre', NULL, NULL, 'null', 0, 0, 'null', 0, 0, 0, 0, 0, 2, 0, 0, NULL, 'null', 'null', '2019-09-03 06:50:20', '2019-09-03 06:54:52', NULL),
(11, 'null', 'carljohnson@click-mail.top', '2019-09-05 09:35:54', '$2y$10$.NESr2LzUsrHTCfyYxcCxe9kZ2NNhASoGBmMipmZFXDEck2gZxDFe', 'Carl', 'Johnson', '78549685214', 'carl-johnson', NULL, NULL, 'null', 0, 0, 'null', 0, 0, 0, 0, 0, 2, 0, 0, NULL, 'null', 'null', '2019-09-05 09:34:50', '2019-09-05 09:48:07', NULL),
(12, 'null', 'mowe@mail-search.com', '2019-09-07 06:06:28', '$2y$10$v95aDd3Ce1TLNqimRbTAc.iDCKQYCv7SfODZK77pTMlVatqxCTIgm', 'Emma', 'Jacob', '78965894587', 'emma-jacob', NULL, NULL, '1567844757.jpg', 0, 0, 'null', 0, 0, 0, 0, 0, 2, 0, 0, NULL, 'null', 'null', '2019-09-07 06:01:59', '2019-09-07 07:25:57', NULL),
(17, 'null', 'mrvishwas9229@gmail.com', '2019-09-11 10:06:37', NULL, 'Alen', 'Jax', 'null', 'er-tapas-vishwas', NULL, NULL, '1568199997.png', 0, 0, '1345914195584839', 0, 0, 0, 0, 0, 2, 0, 0, NULL, 'null', 'null', '2019-09-11 10:06:37', '2019-09-11 10:06:37', NULL),
(18, 'null', 'geogatedproject27@gmail.com', '2019-09-20 13:29:24', NULL, 'Amber', 'Leaf', 'null', 'amber-leaf', NULL, NULL, '1568989763.png', 0, 0, '10150072001242708', 0, 0, 0, 0, 0, 2, 0, 0, NULL, 'null', 'null', '2019-09-20 13:29:24', '2019-09-20 13:29:24', NULL),
(19, 'null', 'djvolumes@gmail.com', NULL, '$2y$10$JvAJnXQ/Zfer07g.HoSafuvm6G4r5T1PDN/ZurKa5DkqZOXgRYdRi', 'Dan', 'Joe', '08871287816', 'dan-joe', NULL, NULL, 'null', 0, 0, 'null', 0, 0, 0, 0, 0, 2, 0, 1, NULL, 'null', 'null', '2019-09-21 13:53:29', '2019-09-21 13:58:51', NULL),
(20, 'null', 'dheerajksang@gmail.com', '2019-09-27 10:01:02', '$2y$10$eV1CI4lOYjxNhyXJkD6nPuCHNwheySgx4F5afomCLYaHtiNaPybb.', 'Harry', 'Wats', '08871287816', 'harry-wats', NULL, NULL, 'null', 0, 0, 'null', 0, 0, 0, 0, 0, 2, 0, 1, NULL, 'null', 'null', '2019-09-27 09:58:59', '2019-09-27 10:01:02', NULL),
(21, 'null', 'dheerajxm@gmail.com', '2019-09-27 13:38:25', '$2y$10$4JUuDf7Z4uimt6RqFyhk8OANldIE8d5hEUJgZyuTV2pRMHNuFfKia', 'Tom', 'Michal', '07788451247', 'tom-michal', NULL, NULL, 'null', 0, 0, 'null', 0, 0, 0, 0, 0, 2, 0, 1, NULL, 'null', 'null', '2019-09-27 13:36:16', '2019-09-27 13:38:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_area_intrest`
--

CREATE TABLE `user_area_intrest` (
  `id` bigint(127) NOT NULL,
  `user_id` bigint(127) NOT NULL,
  `area_intrest_id` smallint(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_area_intrest`
--

INSERT INTO `user_area_intrest` (`id`, `user_id`, `area_intrest_id`) VALUES
(9, 8, 2),
(10, 8, 3),
(11, 8, 4),
(12, 8, 7);

-- --------------------------------------------------------

--
-- Table structure for table `user_location`
--

CREATE TABLE `user_location` (
  `id` bigint(127) NOT NULL,
  `user_id` bigint(127) NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_location`
--

INSERT INTO `user_location` (`id`, `user_id`, `location`, `address`, `latitude`, `longitude`) VALUES
(1, 2, 'London, UK', 'London, UK', '51.5073509', '-0.12775829999998223'),
(2, 12, 'Manchester, UK', 'Manchester, UK', '53.4807593', '-2.2426305000000184');

-- --------------------------------------------------------

--
-- Table structure for table `user_sports`
--

CREATE TABLE `user_sports` (
  `id` bigint(11) NOT NULL,
  `user_id` bigint(11) NOT NULL,
  `sport_id` smallint(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_sports`
--

INSERT INTO `user_sports` (`id`, `user_id`, `sport_id`) VALUES
(3, 8, 5);

-- --------------------------------------------------------

--
-- Table structure for table `venue_followers`
--

CREATE TABLE `venue_followers` (
  `id` bigint(127) NOT NULL,
  `user_id` bigint(127) NOT NULL,
  `venue_id` bigint(127) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `venue_followers`
--

INSERT INTO `venue_followers` (`id`, `user_id`, `venue_id`) VALUES
(3, 2, 3),
(4, 3, 3),
(5, 11, 6),
(8, 17, 13),
(9, 17, 12);

-- --------------------------------------------------------

--
-- Table structure for table `your_booking`
--

CREATE TABLE `your_booking` (
  `id` bigint(127) NOT NULL,
  `user_id` bigint(127) NOT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `venue_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `recurring_booking` tinyint(1) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `advertise_session`
--
ALTER TABLE `advertise_session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `basic_features`
--
ALTER TABLE `basic_features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `block_booking`
--
ALTER TABLE `block_booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `children_party`
--
ALTER TABLE `children_party`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `children_party_category`
--
ALTER TABLE `children_party_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `common_video`
--
ALTER TABLE `common_video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `corporate_blogs`
--
ALTER TABLE `corporate_blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `corporate_category`
--
ALTER TABLE `corporate_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `corporate_details`
--
ALTER TABLE `corporate_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `corporate_faq`
--
ALTER TABLE `corporate_faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `corporate_images`
--
ALTER TABLE `corporate_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `corporate_videos`
--
ALTER TABLE `corporate_videos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_images`
--
ALTER TABLE `cp_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_venue_facility`
--
ALTER TABLE `cp_venue_facility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `game_comments`
--
ALTER TABLE `game_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `game_images`
--
ALTER TABLE `game_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `game_payment`
--
ALTER TABLE `game_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `get_involved`
--
ALTER TABLE `get_involved`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_about_us`
--
ALTER TABLE `home_about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_banners`
--
ALTER TABLE `home_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invite_friend`
--
ALTER TABLE `invite_friend`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `join_a_league`
--
ALTER TABLE `join_a_league`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `join_league_block`
--
ALTER TABLE `join_league_block`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kids_blogs`
--
ALTER TABLE `kids_blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kids_details`
--
ALTER TABLE `kids_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kids_gallery`
--
ALTER TABLE `kids_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kids_whats_included`
--
ALTER TABLE `kids_whats_included`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `looking_venue_players`
--
ALTER TABLE `looking_venue_players`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_area_intrest`
--
ALTER TABLE `mst_area_intrest`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_games`
--
ALTER TABLE `mst_games`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_nationality`
--
ALTER TABLE `mst_nationality`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_notification`
--
ALTER TABLE `mst_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_sports`
--
ALTER TABLE `mst_sports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_venue`
--
ALTER TABLE `mst_venue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_venue_facility`
--
ALTER TABLE `mst_venue_facility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pitch_hire`
--
ALTER TABLE `pitch_hire`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quick_links_slider`
--
ALTER TABLE `quick_links_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `repeat_games`
--
ALTER TABLE `repeat_games`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_setting`
--
ALTER TABLE `site_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sportsturnaments_blogs`
--
ALTER TABLE `sportsturnaments_blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sportsturnaments_category`
--
ALTER TABLE `sportsturnaments_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sportsturnaments_details`
--
ALTER TABLE `sportsturnaments_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sportsturnaments_faq`
--
ALTER TABLE `sportsturnaments_faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sportsturnaments_images`
--
ALTER TABLE `sportsturnaments_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sportsturnaments_videos`
--
ALTER TABLE `sportsturnaments_videos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team_spots`
--
ALTER TABLE `team_spots`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_area_intrest`
--
ALTER TABLE `user_area_intrest`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_location`
--
ALTER TABLE `user_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_sports`
--
ALTER TABLE `user_sports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `venue_followers`
--
ALTER TABLE `venue_followers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `your_booking`
--
ALTER TABLE `your_booking`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `advertise_session`
--
ALTER TABLE `advertise_session`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `basic_features`
--
ALTER TABLE `basic_features`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `block_booking`
--
ALTER TABLE `block_booking`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `children_party`
--
ALTER TABLE `children_party`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `children_party_category`
--
ALTER TABLE `children_party_category`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `common_video`
--
ALTER TABLE `common_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `corporate_blogs`
--
ALTER TABLE `corporate_blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `corporate_category`
--
ALTER TABLE `corporate_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `corporate_details`
--
ALTER TABLE `corporate_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `corporate_faq`
--
ALTER TABLE `corporate_faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `corporate_images`
--
ALTER TABLE `corporate_images`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `corporate_videos`
--
ALTER TABLE `corporate_videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `cp_images`
--
ALTER TABLE `cp_images`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT for table `cp_venue_facility`
--
ALTER TABLE `cp_venue_facility`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=738;

--
-- AUTO_INCREMENT for table `game_comments`
--
ALTER TABLE `game_comments`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `game_images`
--
ALTER TABLE `game_images`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;

--
-- AUTO_INCREMENT for table `game_payment`
--
ALTER TABLE `game_payment`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `get_involved`
--
ALTER TABLE `get_involved`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `home_about_us`
--
ALTER TABLE `home_about_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `home_banners`
--
ALTER TABLE `home_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `invite_friend`
--
ALTER TABLE `invite_friend`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `join_a_league`
--
ALTER TABLE `join_a_league`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `join_league_block`
--
ALTER TABLE `join_league_block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `kids_blogs`
--
ALTER TABLE `kids_blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `kids_details`
--
ALTER TABLE `kids_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `kids_gallery`
--
ALTER TABLE `kids_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `kids_whats_included`
--
ALTER TABLE `kids_whats_included`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `looking_venue_players`
--
ALTER TABLE `looking_venue_players`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `mst_area_intrest`
--
ALTER TABLE `mst_area_intrest`
  MODIFY `id` smallint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `mst_games`
--
ALTER TABLE `mst_games`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `mst_nationality`
--
ALTER TABLE `mst_nationality`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mst_notification`
--
ALTER TABLE `mst_notification`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mst_sports`
--
ALTER TABLE `mst_sports`
  MODIFY `id` mediumint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `mst_venue`
--
ALTER TABLE `mst_venue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `mst_venue_facility`
--
ALTER TABLE `mst_venue_facility`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pitch_hire`
--
ALTER TABLE `pitch_hire`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `quick_links_slider`
--
ALTER TABLE `quick_links_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `repeat_games`
--
ALTER TABLE `repeat_games`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1207;

--
-- AUTO_INCREMENT for table `site_setting`
--
ALTER TABLE `site_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sportsturnaments_blogs`
--
ALTER TABLE `sportsturnaments_blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `sportsturnaments_category`
--
ALTER TABLE `sportsturnaments_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sportsturnaments_details`
--
ALTER TABLE `sportsturnaments_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sportsturnaments_faq`
--
ALTER TABLE `sportsturnaments_faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sportsturnaments_images`
--
ALTER TABLE `sportsturnaments_images`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `sportsturnaments_videos`
--
ALTER TABLE `sportsturnaments_videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `team_spots`
--
ALTER TABLE `team_spots`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `user_area_intrest`
--
ALTER TABLE `user_area_intrest`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `user_location`
--
ALTER TABLE `user_location`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_sports`
--
ALTER TABLE `user_sports`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `venue_followers`
--
ALTER TABLE `venue_followers`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `your_booking`
--
ALTER TABLE `your_booking`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
